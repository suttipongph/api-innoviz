﻿using IdentityModel.Client;
using Innoviz.SmartApp.BatchProcessing.Job;
using Innoviz.SmartApp.BatchProcessing.Service;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.BatchProcessing;
using Innoviz.SmartApp.Data.InterfaceWithOtherSystems;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModels;
using Innoviz.SmartApp.WebApi;
using Innoviz.SmartApp.WebApi.AuthPolicy;
using Innoviz.SmartApp.WebApi.Middleware;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Quartz;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.BatchProcessing
{
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) {

            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            string connectionString = Configuration.GetConnectionString("SmartApp");
            int connectionTimeout = Convert.ToInt32(Configuration["CommandTimeout:Timeout"]);

            services.AddDbContext<QuartzDbContext>(options =>
                options.UseSqlServer(connectionString, sql => {
                    sql.MigrationsAssembly(migrationsAssembly);
                    sql.CommandTimeout(connectionTimeout);
                }), 
                ServiceLifetime.Transient
            );
            services.AddDbContext<SmartAppContext>(options =>
                options.UseSqlServer(connectionString, sql => {
                    sql.MigrationsAssembly(migrationsAssembly);
                    sql.CommandTimeout(connectionTimeout);
                }),
                ServiceLifetime.Transient
            );

            var interfaceAXMigrationAssembly = typeof(InterfaceAXDbContext).GetTypeInfo().Assembly.GetName().Name;
            string interfaceWithAXConnectionString = Configuration.GetConnectionString("AXInterface");
            services.AddDbContext<InterfaceAXDbContext>(options =>
                options.UseSqlServer(interfaceWithAXConnectionString, sql => {
                    sql.MigrationsAssembly(interfaceAXMigrationAssembly);
                    sql.CommandTimeout(connectionTimeout);
                }),
                ServiceLifetime.Transient
            );

            services.AddControllers()
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            services.AddScoped<IBatchLogService, BatchLogService>();

            services.UseQuartz(connectionString, typeof(ApiJob));

            string apiAuthority = Configuration["JWT:Authority"];
            string apiAudience = Configuration["JWT:Audience"];
            string apiUserAudience = Configuration["JWT:UserAudience"];
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer("system",options => {
                    options.Authority = apiAuthority;
                    options.RequireHttpsMetadata = false;
                    options.Audience = apiAudience;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ClockSkew = TimeSpan.FromSeconds(10)
                    };
                })
                .AddJwtBearer("user", options => {
                    options.Authority = apiAuthority;
                    options.RequireHttpsMetadata = false;
                    options.Audience = apiUserAudience;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ClockSkew = TimeSpan.FromSeconds(10)
                    };
                });

            //services.AddAuthorization(options => {
            //    // use custom policy
            //    options.AddPolicy("roles_required", policy =>
            //                        policy.Requirements.Add(new CustomRolesRequirement()));
            //});

            ////auth policy
            //services.AddScoped<IAuthorizationHandler, CustomRolesRequirementHandler>();

            services.AddTransient<ISysTransactionLogService, SysTransactionLogService>();

            //services.AddHostedService<SmartAppWebApiControllerDiscoHostedService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
                              IWebHostEnvironment env,
                              ILoggerFactory loggerFactory) {

            loggerFactory.AddSerilog();

            InitializeDatabase(app, env);

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }
            app.UseMiddleware<RequestResponseLoggingMiddleware>();
            app.UseMiddleware<WebApiErrorHandler>();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                name: "api-quartz",
                pattern: "api/quartz/{controller}");
            });

            // start scheduler
            app.ApplicationServices.GetService<IScheduler>().Start(new System.Threading.CancellationToken()).Wait();
            
        }
        private void InitializeDatabase(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetRequiredService<QuartzDbContext>().Database.Migrate();

                var smartAppContext = serviceScope.ServiceProvider.GetRequiredService<SmartAppContext>();

                if (smartAppContext.SysMessage.Count() != 0 &&
                    smartAppContext.SysDuplicateDetection.Count() != 0)
                {
                    InitializeSystemStaticData(smartAppContext);
                }
            }
        }
        
        private void InitializeStoredProcedureAndFunction()
        {
            try
            {
                string connectionString = Configuration.GetConnectionString("SmartApp");
                string spFnFolder = "./StoredProceduresAndFunctions";
                SharedService sharedService = new SharedService();
                sharedService.InitializeStoredProceduresAndFunctions(spFnFolder, connectionString);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void InitializeSystemStaticData(SmartAppContext context)
        {
            try
            {
                // ob groupIds
                //var siteObFeatureGroupIds = SysFeatureTableData.GetOBGroupId();
                //SystemStaticData.SetOBFeatureGroupIds(siteObFeatureGroupIds);

                // sysMessage
                if (!SystemStaticData.HasStaticSysMessageData())
                {
                    ISysMessageService sysMessageService = new SysMessageService(context);
                    var dbSysMessage = sysMessageService.GetSysMessageNoTracking();
                    List<StaticSysMessageData> messages = new List<StaticSysMessageData>();
                    foreach (var item in dbSysMessage)
                    {
                        messages.Add(item.ToStaticSysMessageData());
                    }
                    SystemStaticData.SetSysMessageData(messages);
                }

                // sysDuplicate
                if (!SystemStaticData.HasStaticSysDuplicateData())
                {
                    ISysDuplicateDetectionService sysDuplicateDetectionService = new SysDuplicateDetectionService(context);
                    List<SysDuplicateDetection> duplicateDetections = sysDuplicateDetectionService.GetSysDuplicateDetectionNoTracking();
                    SystemStaticData.SetSysDuplicateDetectionData((duplicateDetections.ToStaticSysDuplicateDetections()).ToList());
                }

                // dropdown lookup table names
                if (!SystemStaticData.HasStaticDropdownLookup())
                {
                    ISysAccessRightService accessRightService = new SysAccessRightService(context);
                    SystemStaticData.SetDropdownLookupData(accessRightService.GetTableNamesForControllerEntityMapping());
                }

                // modelType lookup by model name
                if(!SystemStaticData.HasStaticModelTypeLookup())
                {
                    ISharedService sharedService = new SharedService();
                    SystemStaticData.SetModelTypeLookupData(sharedService.InitializeModelTypeLookupStaticData());
                }
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e);
                throw e;
            }
        }
        private void InitializeSysMessage(SmartAppContext context)
        {
            try
            {
                string text = File.ReadAllText("./StaticData/SysMessage.json");

                ISysMessageService sysMessageService = new SysMessageService(context);
                sysMessageService.InitializeSysMessage(text);
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e);
                throw e;
            }
        }
        private void InitializeSysRoleTable(SmartAppContext context, string username)
        {
            try
            {
                ISysAccessRightService accessRightService = new SysAccessRightService(context);
                accessRightService.InitializeDevelopmentSysRoleTable(username);
                accessRightService.InitializeDevelopmentSysScopeRole(username);
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e);
                throw e;
            }
        }
        private void InitializeSysDuplicateDetection(SmartAppContext context, string username)
        {
            try
            {
                string text = File.ReadAllText("./StaticData/SysDuplicateDetection.json");
                ISysDuplicateDetectionService sysDuplicateDetectionService = new SysDuplicateDetectionService(context);
                sysDuplicateDetectionService.InitializeSysDuplicateDetection(text, username);
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e);
                throw e;
            }
        }
        private void InitializeSysLabelReport(SmartAppContext context)
        {
            try
            {
                string sysLabelReportjson = File.ReadAllText("./StaticData/SysLabelReport.json");
                List<SysLabelReport> sysLabelReports = JsonConvert.DeserializeObject<List<SysLabelReport>>(sysLabelReportjson);

                ISysLabelReportService labelReportService = new SysLabelReportService(context);
                //labelReportService.InitializeSysLabelReport(sysLabelReports);
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e);
                throw e;
            }
        }
        private void InitializeSysLabelReport_CUSTOM(SmartAppContext context)
        {
            try
            {
                string sysLabelReportCustomjson = File.ReadAllText("./StaticData/SysLabelReport_CUSTOM.json");
                List<SysLabelReport_CUSTOM> sysLabelReport_CUSTOMs = JsonConvert.DeserializeObject<List<SysLabelReport_CUSTOM>>(sysLabelReportCustomjson);

                ISysLabelReportService labelReportService = new SysLabelReportService(context);
                //labelReportService.InitializeSysLabelReport_CUSTOM(sysLabelReport_CUSTOMs);
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e);
                throw e;
            }
        }
        private void InitializeDocumentProcessStatus(SmartAppContext context, string username)
        {
            try
            {
                IDocumentService documentService = new DocumentService(context);
                string text = File.ReadAllText("./StaticData/DocumentProcessStatus.json");
                string text_custom = File.ReadAllText("./StaticData/DocumentProcessStatus_CUSTOM.json");
                documentService.InitializeDocumentProcessStatus(text, text_custom, username);
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e);
                throw e;
            }
        }
        private void InitializeSysEnumTable(SmartAppContext context, string username)
        {
            try
            {
                string sysEnumTablejson = File.ReadAllText("./StaticData/SysEnumTable.json");
                string sysEnumTableCustomjson = File.ReadAllText("./StaticData/SysEnumTable_CUSTOM.json");

                ISysEnumTableService enumTableService = new SysEnumTableService(context);
                enumTableService.InitializeSysEnumTable(sysEnumTablejson, sysEnumTableCustomjson, username);
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e);
                throw e;
            }
        }

        private void InitializeSysFeatureGroupControllerMapping(SmartAppContext context, string username, bool isDevelopment)
        {
            try
            {
                string sysFeatureGroupMenujson = File.ReadAllText("./StaticData/SysFeatureGroup_Menu_Mappings.json");
                var cemViews = SystemStaticData.GetSysControllerEntityMappingData()
                                    .Select(s => new SysControllerEntityMappingView
                                    {
                                        ModelName = s.ModelName,
                                        SysControllerTable_RouteAttribute = s.SysControllerTable_RouteAttribute,
                                        SysControllerTable_ControllerName = s.SysControllerTable_ControllerName
                                    }).ToList();
                ISysAccessRightService accessRightService = new SysAccessRightService(context);
                //accessRightService.InitializeSysFeatureGroupControllerMapping(SysFeatureTableData.GetAddFeatureControllerMapping(),
                //                                                                SysFeatureTableData.GetOBGroupId(), sysFeatureGroupMenujson,
                //                                                                cemViews, username, isDevelopment);
                SystemStaticData.SetSysControllerEntityMappingData(null);
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e);
                throw e;
            }
        }
        private void InitializeMigrationTable(SmartAppContext context, string username)
        {
            try
            {
                string migrationTablejson = File.ReadAllText("./StaticData/MigrationTable.json");
                string migrationTableCustomjson = File.ReadAllText("./StaticData/MigrationTable_CUSTOM.json");

                IMigrationTableService migrationTableService = new MigrationTableService(context);
                migrationTableService.InitializeMigrationTable(migrationTablejson, migrationTableCustomjson, username);
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e);
                throw e;
            }
        }
        private void LogErrorAtStartup(Exception e)
        {
            Console.WriteLine("#Error at WebApi Startup");
            var msgList = e.Data[ExceptionDataKey.MessageList];
            if (msgList != null)
            {
                var list = (List<string>)msgList;
                if (list.Count() != 0)
                {
                    foreach (var item in list)
                    {
                        Console.WriteLine(item);
                    }
                }
            }
            Console.WriteLine(e.Data[ExceptionDataKey.StackTrace].ToString());
        }

    }
}
