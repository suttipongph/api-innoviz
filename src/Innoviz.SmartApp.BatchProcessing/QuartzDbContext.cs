﻿using Innoviz.SmartApp.BatchProcessing.Model.QuartzModels;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Innoviz.SmartApp.BatchProcessing
{
    public class QuartzDbContext : DbContext
    {
        private SystemParameter systemParameter;

        public QuartzDbContext(DbContextOptions<QuartzDbContext> options)
            : base(options) {

        }

        public virtual DbSet<QRTZ_BLOB_TRIGGERS> QRTZ_BLOB_TRIGGERS { get; set; }
        public virtual DbSet<QRTZ_CALENDARS> QRTZ_CALENDARS { get; set; }
        public virtual DbSet<QRTZ_CRON_TRIGGERS> QRTZ_CRON_TRIGGERS { get; set; }
        public virtual DbSet<QRTZ_FIRED_TRIGGERS> QRTZ_FIRED_TRIGGERS { get; set; }
        public virtual DbSet<QRTZ_JOB_DETAILS> QRTZ_JOB_DETAILS { get; set; }
        public virtual DbSet<QRTZ_LOCKS> QRTZ_LOCKS { get; set; }
        public virtual DbSet<QRTZ_PAUSED_TRIGGER_GRPS> QRTZ_PAUSED_TRIGGER_GRPS { get; set; }
        public virtual DbSet<QRTZ_SCHEDULER_STATE> QRTZ_SCHEDULER_STATE { get; set; }
        public virtual DbSet<QRTZ_SIMPLE_TRIGGERS> QRTZ_SIMPLE_TRIGGERS { get; set; }
        public virtual DbSet<QRTZ_SIMPROP_TRIGGERS> QRTZ_SIMPROP_TRIGGERS { get; set; }
        public virtual DbSet<QRTZ_TRIGGERS> QRTZ_TRIGGERS { get; set; }
        public virtual DbSet<BatchJob> BatchJob { get; set; }
        public virtual DbSet<BatchTrigger> BatchTrigger { get; set; }
        public virtual DbSet<BatchInstanceHistory> BatchInstanceHistory { get; set; }
        public virtual DbSet<BatchInstanceLog> BatchInstanceLog { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<QRTZ_BLOB_TRIGGERS>(entity => {
                entity.HasKey(e => new { e.SCHED_NAME, e.TRIGGER_NAME, e.TRIGGER_GROUP });
            });

            modelBuilder.Entity<QRTZ_CALENDARS>(entity => {
                entity.HasKey(e => new { e.SCHED_NAME, e.CALENDAR_NAME });
            });

            modelBuilder.Entity<QRTZ_CRON_TRIGGERS>(entity => {
                entity.HasKey(e => new { e.SCHED_NAME, e.TRIGGER_NAME, e.TRIGGER_GROUP });

                entity.HasOne(d => d.QRTZ_TRIGGERS)
                    .WithOne(p => p.QRTZ_CRON_TRIGGERS)
                    .HasForeignKey<QRTZ_CRON_TRIGGERS>(d => new { d.SCHED_NAME, d.TRIGGER_NAME, d.TRIGGER_GROUP })
                    .HasConstraintName("FK_QRTZ_CRON_TRIGGERS_QRTZ_TRIGGERS");
            });

            modelBuilder.Entity<QRTZ_FIRED_TRIGGERS>(entity => {
                entity.HasKey(e => new { e.SCHED_NAME, e.ENTRY_ID });

                entity.HasIndex(e => new { e.SCHED_NAME, e.INSTANCE_NAME })
                    .HasName("IDX_QRTZ_FT_TRIG_INST_NAME");

                entity.HasIndex(e => new { e.SCHED_NAME, e.JOB_GROUP })
                    .HasName("IDX_QRTZ_FT_JG");

                entity.HasIndex(e => new { e.SCHED_NAME, e.TRIGGER_GROUP })
                    .HasName("IDX_QRTZ_FT_TG");

                entity.HasIndex(e => new { e.SCHED_NAME, e.INSTANCE_NAME, e.REQUESTS_RECOVERY })
                    .HasName("IDX_QRTZ_FT_INST_JOB_REQ_RCVRY");

                entity.HasIndex(e => new { e.SCHED_NAME, e.JOB_NAME, e.JOB_GROUP })
                    .HasName("IDX_QRTZ_FT_J_G");

                entity.HasIndex(e => new { e.SCHED_NAME, e.TRIGGER_NAME, e.TRIGGER_GROUP })
                    .HasName("IDX_QRTZ_FT_T_G");
            });

            modelBuilder.Entity<QRTZ_JOB_DETAILS>(entity => {
                entity.HasKey(e => new { e.SCHED_NAME, e.JOB_NAME, e.JOB_GROUP });
            });

            modelBuilder.Entity<QRTZ_LOCKS>(entity => {
                entity.HasKey(e => new { e.SCHED_NAME, e.LOCK_NAME });
            });

            modelBuilder.Entity<QRTZ_PAUSED_TRIGGER_GRPS>(entity => {
                entity.HasKey(e => new { e.SCHED_NAME, e.TRIGGER_GROUP });
            });

            modelBuilder.Entity<QRTZ_SCHEDULER_STATE>(entity => {
                entity.HasKey(e => new { e.SCHED_NAME, e.INSTANCE_NAME });
            });

            modelBuilder.Entity<QRTZ_SIMPLE_TRIGGERS>(entity => {
                entity.HasKey(e => new { e.SCHED_NAME, e.TRIGGER_NAME, e.TRIGGER_GROUP });

                entity.HasOne(d => d.QRTZ_TRIGGERS)
                    .WithOne(p => p.QRTZ_SIMPLE_TRIGGERS)
                    .HasForeignKey<QRTZ_SIMPLE_TRIGGERS>(d => new { d.SCHED_NAME, d.TRIGGER_NAME, d.TRIGGER_GROUP })
                    .HasConstraintName("FK_QRTZ_SIMPLE_TRIGGERS_QRTZ_TRIGGERS");
            });

            modelBuilder.Entity<QRTZ_SIMPROP_TRIGGERS>(entity => {
                entity.HasKey(e => new { e.SCHED_NAME, e.TRIGGER_NAME, e.TRIGGER_GROUP });

                entity.HasOne(d => d.QRTZ_TRIGGERS)
                    .WithOne(p => p.QRTZ_SIMPROP_TRIGGERS)
                    .HasForeignKey<QRTZ_SIMPROP_TRIGGERS>(d => new { d.SCHED_NAME, d.TRIGGER_NAME, d.TRIGGER_GROUP })
                    .HasConstraintName("FK_QRTZ_SIMPROP_TRIGGERS_QRTZ_TRIGGERS");
            });

            modelBuilder.Entity<QRTZ_TRIGGERS>(entity => {
                entity.HasKey(e => new { e.SCHED_NAME, e.TRIGGER_NAME, e.TRIGGER_GROUP });

                entity.HasIndex(e => new { e.SCHED_NAME, e.CALENDAR_NAME })
                    .HasName("IDX_QRTZ_T_C");

                entity.HasIndex(e => new { e.SCHED_NAME, e.JOB_GROUP })
                    .HasName("IDX_QRTZ_T_JG");

                entity.HasIndex(e => new { e.SCHED_NAME, e.NEXT_FIRE_TIME })
                    .HasName("IDX_QRTZ_T_NEXT_FIRE_TIME");

                entity.HasIndex(e => new { e.SCHED_NAME, e.TRIGGER_GROUP })
                    .HasName("IDX_QRTZ_T_G");

                entity.HasIndex(e => new { e.SCHED_NAME, e.TRIGGER_STATE })
                    .HasName("IDX_QRTZ_T_STATE");

                entity.HasIndex(e => new { e.SCHED_NAME, e.JOB_NAME, e.JOB_GROUP })
                    .HasName("IDX_QRTZ_T_J");

                entity.HasIndex(e => new { e.SCHED_NAME, e.MISFIRE_INSTR, e.NEXT_FIRE_TIME })
                    .HasName("IDX_QRTZ_T_NFT_MISFIRE");

                entity.HasIndex(e => new { e.SCHED_NAME, e.TRIGGER_GROUP, e.TRIGGER_STATE })
                    .HasName("IDX_QRTZ_T_N_G_STATE");

                entity.HasIndex(e => new { e.SCHED_NAME, e.TRIGGER_STATE, e.NEXT_FIRE_TIME })
                    .HasName("IDX_QRTZ_T_NFT_ST");

                entity.HasIndex(e => new { e.SCHED_NAME, e.MISFIRE_INSTR, e.NEXT_FIRE_TIME, e.TRIGGER_STATE })
                    .HasName("IDX_QRTZ_T_NFT_ST_MISFIRE");

                entity.HasIndex(e => new { e.SCHED_NAME, e.TRIGGER_NAME, e.TRIGGER_GROUP, e.TRIGGER_STATE })
                    .HasName("IDX_QRTZ_T_N_STATE");

                entity.HasIndex(e => new { e.SCHED_NAME, e.MISFIRE_INSTR, e.NEXT_FIRE_TIME, e.TRIGGER_GROUP, e.TRIGGER_STATE })
                    .HasName("IDX_QRTZ_T_NFT_ST_MISFIRE_GRP");

                entity.HasOne(d => d.QRTZ_JOB_DETAILS)
                    .WithMany(p => p.QRTZ_TRIGGERS)
                    .HasForeignKey(d => new { d.SCHED_NAME, d.JOB_NAME, d.JOB_GROUP })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QRTZ_TRIGGERS_QRTZ_JOB_DETAILS");
            });
            modelBuilder.Entity<BatchInstanceHistory>(entity => {
                entity.Property(e => e.InstanceHistoryId).ValueGeneratedNever();

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.BatchInstanceHistory)
                    .HasForeignKey(d => d.JobId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BatchInstanceHistoryBatchJob");

                entity.HasOne(d => d.Trigger)
                    .WithMany(p => p.BatchInstanceHistory)
                    .HasForeignKey(d => d.TriggerId)
                    .HasConstraintName("FK_BatchInstanceHistoryBatchTrigger");
            });

            modelBuilder.Entity<BatchInstanceLog>(entity => {
                entity.HasOne(d => d.InstanceHistory)
                    .WithMany(p => p.BatchInstanceLog)
                    .HasForeignKey(d => d.InstanceHistoryId)
                    .HasConstraintName("FK_BatchInstanceLogBatchInstancHistory");
            });

            modelBuilder.Entity<BatchJob>(entity => {
                entity.Property(e => e.JobId).ValueGeneratedNever();
            });

            modelBuilder.Entity<BatchTrigger>(entity => {
                entity.Property(e => e.TriggerId).ValueGeneratedNever();

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.BatchTrigger)
                    .HasForeignKey(d => d.JobId)
                    .HasConstraintName("FK_BatchTriggerBatchJob");
            });

        }
        public void SetSystemParameter(SystemParameter input)
        {
            systemParameter = input;
        }
        public SystemParameter GetSystemParameter()
        {
            return systemParameter;
        }

        public string GetUserName()
        {
            // check from SystemParameter property
            if (systemParameter != null && systemParameter.UserName != null)
            {
                return systemParameter.UserName;
            }
            else
            {
                throw new NullReferenceException("System Parameter [UserName] cannot be null.");
            }
        }

    }
}
