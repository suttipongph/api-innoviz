﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Innoviz.SmartApp.BatchProcessing.Migrations
{
    public partial class _25apr19 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QRTZ_BLOB_TRIGGERS",
                columns: table => new
                {
                    SCHED_NAME = table.Column<string>(maxLength: 120, nullable: false),
                    TRIGGER_NAME = table.Column<string>(maxLength: 150, nullable: false),
                    TRIGGER_GROUP = table.Column<string>(maxLength: 150, nullable: false),
                    BLOB_DATA = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QRTZ_BLOB_TRIGGERS", x => new { x.SCHED_NAME, x.TRIGGER_NAME, x.TRIGGER_GROUP });
                });

            migrationBuilder.CreateTable(
                name: "QRTZ_CALENDARS",
                columns: table => new
                {
                    SCHED_NAME = table.Column<string>(maxLength: 120, nullable: false),
                    CALENDAR_NAME = table.Column<string>(maxLength: 200, nullable: false),
                    CALENDAR = table.Column<byte[]>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QRTZ_CALENDARS", x => new { x.SCHED_NAME, x.CALENDAR_NAME });
                });

            migrationBuilder.CreateTable(
                name: "QRTZ_FIRED_TRIGGERS",
                columns: table => new
                {
                    SCHED_NAME = table.Column<string>(maxLength: 120, nullable: false),
                    ENTRY_ID = table.Column<string>(maxLength: 140, nullable: false),
                    TRIGGER_NAME = table.Column<string>(maxLength: 150, nullable: false),
                    TRIGGER_GROUP = table.Column<string>(maxLength: 150, nullable: false),
                    INSTANCE_NAME = table.Column<string>(maxLength: 200, nullable: false),
                    FIRED_TIME = table.Column<long>(nullable: false),
                    SCHED_TIME = table.Column<long>(nullable: false),
                    PRIORITY = table.Column<int>(nullable: false),
                    STATE = table.Column<string>(maxLength: 16, nullable: false),
                    JOB_NAME = table.Column<string>(maxLength: 150, nullable: true),
                    JOB_GROUP = table.Column<string>(maxLength: 150, nullable: true),
                    IS_NONCONCURRENT = table.Column<bool>(nullable: true),
                    REQUESTS_RECOVERY = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QRTZ_FIRED_TRIGGERS", x => new { x.SCHED_NAME, x.ENTRY_ID });
                });

            migrationBuilder.CreateTable(
                name: "QRTZ_JOB_DETAILS",
                columns: table => new
                {
                    SCHED_NAME = table.Column<string>(maxLength: 120, nullable: false),
                    JOB_NAME = table.Column<string>(maxLength: 150, nullable: false),
                    JOB_GROUP = table.Column<string>(maxLength: 150, nullable: false),
                    DESCRIPTION = table.Column<string>(maxLength: 250, nullable: true),
                    JOB_CLASS_NAME = table.Column<string>(maxLength: 250, nullable: false),
                    IS_DURABLE = table.Column<bool>(nullable: false),
                    IS_NONCONCURRENT = table.Column<bool>(nullable: false),
                    IS_UPDATE_DATA = table.Column<bool>(nullable: false),
                    REQUESTS_RECOVERY = table.Column<bool>(nullable: false),
                    JOB_DATA = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QRTZ_JOB_DETAILS", x => new { x.SCHED_NAME, x.JOB_NAME, x.JOB_GROUP });
                });

            migrationBuilder.CreateTable(
                name: "QRTZ_LOCKS",
                columns: table => new
                {
                    SCHED_NAME = table.Column<string>(maxLength: 120, nullable: false),
                    LOCK_NAME = table.Column<string>(maxLength: 40, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QRTZ_LOCKS", x => new { x.SCHED_NAME, x.LOCK_NAME });
                });

            migrationBuilder.CreateTable(
                name: "QRTZ_PAUSED_TRIGGER_GRPS",
                columns: table => new
                {
                    SCHED_NAME = table.Column<string>(maxLength: 120, nullable: false),
                    TRIGGER_GROUP = table.Column<string>(maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QRTZ_PAUSED_TRIGGER_GRPS", x => new { x.SCHED_NAME, x.TRIGGER_GROUP });
                });

            migrationBuilder.CreateTable(
                name: "QRTZ_SCHEDULER_STATE",
                columns: table => new
                {
                    SCHED_NAME = table.Column<string>(maxLength: 120, nullable: false),
                    INSTANCE_NAME = table.Column<string>(maxLength: 200, nullable: false),
                    LAST_CHECKIN_TIME = table.Column<long>(nullable: false),
                    CHECKIN_INTERVAL = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QRTZ_SCHEDULER_STATE", x => new { x.SCHED_NAME, x.INSTANCE_NAME });
                });

            migrationBuilder.CreateTable(
                name: "QRTZ_TRIGGERS",
                columns: table => new
                {
                    SCHED_NAME = table.Column<string>(maxLength: 120, nullable: false),
                    TRIGGER_NAME = table.Column<string>(maxLength: 150, nullable: false),
                    TRIGGER_GROUP = table.Column<string>(maxLength: 150, nullable: false),
                    JOB_NAME = table.Column<string>(maxLength: 150, nullable: false),
                    JOB_GROUP = table.Column<string>(maxLength: 150, nullable: false),
                    DESCRIPTION = table.Column<string>(maxLength: 250, nullable: true),
                    NEXT_FIRE_TIME = table.Column<long>(nullable: true),
                    PREV_FIRE_TIME = table.Column<long>(nullable: true),
                    PRIORITY = table.Column<int>(nullable: true),
                    TRIGGER_STATE = table.Column<string>(maxLength: 16, nullable: false),
                    TRIGGER_TYPE = table.Column<string>(maxLength: 8, nullable: false),
                    START_TIME = table.Column<long>(nullable: false),
                    END_TIME = table.Column<long>(nullable: true),
                    CALENDAR_NAME = table.Column<string>(maxLength: 200, nullable: true),
                    MISFIRE_INSTR = table.Column<int>(nullable: true),
                    JOB_DATA = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QRTZ_TRIGGERS", x => new { x.SCHED_NAME, x.TRIGGER_NAME, x.TRIGGER_GROUP });
                    table.ForeignKey(
                        name: "FK_QRTZ_TRIGGERS_QRTZ_JOB_DETAILS",
                        columns: x => new { x.SCHED_NAME, x.JOB_NAME, x.JOB_GROUP },
                        principalTable: "QRTZ_JOB_DETAILS",
                        principalColumns: new[] { "SCHED_NAME", "JOB_NAME", "JOB_GROUP" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QRTZ_CRON_TRIGGERS",
                columns: table => new
                {
                    SCHED_NAME = table.Column<string>(maxLength: 120, nullable: false),
                    TRIGGER_NAME = table.Column<string>(maxLength: 150, nullable: false),
                    TRIGGER_GROUP = table.Column<string>(maxLength: 150, nullable: false),
                    CRON_EXPRESSION = table.Column<string>(maxLength: 120, nullable: false),
                    TIME_ZONE_ID = table.Column<string>(maxLength: 80, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QRTZ_CRON_TRIGGERS", x => new { x.SCHED_NAME, x.TRIGGER_NAME, x.TRIGGER_GROUP });
                    table.ForeignKey(
                        name: "FK_QRTZ_CRON_TRIGGERS_QRTZ_TRIGGERS",
                        columns: x => new { x.SCHED_NAME, x.TRIGGER_NAME, x.TRIGGER_GROUP },
                        principalTable: "QRTZ_TRIGGERS",
                        principalColumns: new[] { "SCHED_NAME", "TRIGGER_NAME", "TRIGGER_GROUP" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QRTZ_SIMPLE_TRIGGERS",
                columns: table => new
                {
                    SCHED_NAME = table.Column<string>(maxLength: 120, nullable: false),
                    TRIGGER_NAME = table.Column<string>(maxLength: 150, nullable: false),
                    TRIGGER_GROUP = table.Column<string>(maxLength: 150, nullable: false),
                    REPEAT_COUNT = table.Column<int>(nullable: false),
                    REPEAT_INTERVAL = table.Column<long>(nullable: false),
                    TIMES_TRIGGERED = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QRTZ_SIMPLE_TRIGGERS", x => new { x.SCHED_NAME, x.TRIGGER_NAME, x.TRIGGER_GROUP });
                    table.ForeignKey(
                        name: "FK_QRTZ_SIMPLE_TRIGGERS_QRTZ_TRIGGERS",
                        columns: x => new { x.SCHED_NAME, x.TRIGGER_NAME, x.TRIGGER_GROUP },
                        principalTable: "QRTZ_TRIGGERS",
                        principalColumns: new[] { "SCHED_NAME", "TRIGGER_NAME", "TRIGGER_GROUP" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QRTZ_SIMPROP_TRIGGERS",
                columns: table => new
                {
                    SCHED_NAME = table.Column<string>(maxLength: 120, nullable: false),
                    TRIGGER_NAME = table.Column<string>(maxLength: 150, nullable: false),
                    TRIGGER_GROUP = table.Column<string>(maxLength: 150, nullable: false),
                    STR_PROP_1 = table.Column<string>(maxLength: 512, nullable: true),
                    STR_PROP_2 = table.Column<string>(maxLength: 512, nullable: true),
                    STR_PROP_3 = table.Column<string>(maxLength: 512, nullable: true),
                    INT_PROP_1 = table.Column<int>(nullable: true),
                    INT_PROP_2 = table.Column<int>(nullable: true),
                    LONG_PROP_1 = table.Column<long>(nullable: true),
                    LONG_PROP_2 = table.Column<long>(nullable: true),
                    DEC_PROP_1 = table.Column<decimal>(type: "numeric(13, 4)", nullable: true),
                    DEC_PROP_2 = table.Column<decimal>(type: "numeric(13, 4)", nullable: true),
                    BOOL_PROP_1 = table.Column<bool>(nullable: true),
                    BOOL_PROP_2 = table.Column<bool>(nullable: true),
                    TIME_ZONE_ID = table.Column<string>(maxLength: 80, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QRTZ_SIMPROP_TRIGGERS", x => new { x.SCHED_NAME, x.TRIGGER_NAME, x.TRIGGER_GROUP });
                    table.ForeignKey(
                        name: "FK_QRTZ_SIMPROP_TRIGGERS_QRTZ_TRIGGERS",
                        columns: x => new { x.SCHED_NAME, x.TRIGGER_NAME, x.TRIGGER_GROUP },
                        principalTable: "QRTZ_TRIGGERS",
                        principalColumns: new[] { "SCHED_NAME", "TRIGGER_NAME", "TRIGGER_GROUP" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_FT_TRIG_INST_NAME",
                table: "QRTZ_FIRED_TRIGGERS",
                columns: new[] { "SCHED_NAME", "INSTANCE_NAME" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_FT_JG",
                table: "QRTZ_FIRED_TRIGGERS",
                columns: new[] { "SCHED_NAME", "JOB_GROUP" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_FT_TG",
                table: "QRTZ_FIRED_TRIGGERS",
                columns: new[] { "SCHED_NAME", "TRIGGER_GROUP" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_FT_INST_JOB_REQ_RCVRY",
                table: "QRTZ_FIRED_TRIGGERS",
                columns: new[] { "SCHED_NAME", "INSTANCE_NAME", "REQUESTS_RECOVERY" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_FT_J_G",
                table: "QRTZ_FIRED_TRIGGERS",
                columns: new[] { "SCHED_NAME", "JOB_NAME", "JOB_GROUP" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_FT_T_G",
                table: "QRTZ_FIRED_TRIGGERS",
                columns: new[] { "SCHED_NAME", "TRIGGER_NAME", "TRIGGER_GROUP" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_T_C",
                table: "QRTZ_TRIGGERS",
                columns: new[] { "SCHED_NAME", "CALENDAR_NAME" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_T_JG",
                table: "QRTZ_TRIGGERS",
                columns: new[] { "SCHED_NAME", "JOB_GROUP" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_T_NEXT_FIRE_TIME",
                table: "QRTZ_TRIGGERS",
                columns: new[] { "SCHED_NAME", "NEXT_FIRE_TIME" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_T_G",
                table: "QRTZ_TRIGGERS",
                columns: new[] { "SCHED_NAME", "TRIGGER_GROUP" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_T_STATE",
                table: "QRTZ_TRIGGERS",
                columns: new[] { "SCHED_NAME", "TRIGGER_STATE" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_T_J",
                table: "QRTZ_TRIGGERS",
                columns: new[] { "SCHED_NAME", "JOB_NAME", "JOB_GROUP" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_T_NFT_MISFIRE",
                table: "QRTZ_TRIGGERS",
                columns: new[] { "SCHED_NAME", "MISFIRE_INSTR", "NEXT_FIRE_TIME" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_T_N_G_STATE",
                table: "QRTZ_TRIGGERS",
                columns: new[] { "SCHED_NAME", "TRIGGER_GROUP", "TRIGGER_STATE" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_T_NFT_ST",
                table: "QRTZ_TRIGGERS",
                columns: new[] { "SCHED_NAME", "TRIGGER_STATE", "NEXT_FIRE_TIME" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_T_NFT_ST_MISFIRE",
                table: "QRTZ_TRIGGERS",
                columns: new[] { "SCHED_NAME", "MISFIRE_INSTR", "NEXT_FIRE_TIME", "TRIGGER_STATE" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_T_N_STATE",
                table: "QRTZ_TRIGGERS",
                columns: new[] { "SCHED_NAME", "TRIGGER_NAME", "TRIGGER_GROUP", "TRIGGER_STATE" });

            migrationBuilder.CreateIndex(
                name: "IDX_QRTZ_T_NFT_ST_MISFIRE_GRP",
                table: "QRTZ_TRIGGERS",
                columns: new[] { "SCHED_NAME", "MISFIRE_INSTR", "NEXT_FIRE_TIME", "TRIGGER_GROUP", "TRIGGER_STATE" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QRTZ_BLOB_TRIGGERS");

            migrationBuilder.DropTable(
                name: "QRTZ_CALENDARS");

            migrationBuilder.DropTable(
                name: "QRTZ_CRON_TRIGGERS");

            migrationBuilder.DropTable(
                name: "QRTZ_FIRED_TRIGGERS");

            migrationBuilder.DropTable(
                name: "QRTZ_LOCKS");

            migrationBuilder.DropTable(
                name: "QRTZ_PAUSED_TRIGGER_GRPS");

            migrationBuilder.DropTable(
                name: "QRTZ_SCHEDULER_STATE");

            migrationBuilder.DropTable(
                name: "QRTZ_SIMPLE_TRIGGERS");

            migrationBuilder.DropTable(
                name: "QRTZ_SIMPROP_TRIGGERS");

            migrationBuilder.DropTable(
                name: "QRTZ_TRIGGERS");

            migrationBuilder.DropTable(
                name: "QRTZ_JOB_DETAILS");
        }
    }
}
