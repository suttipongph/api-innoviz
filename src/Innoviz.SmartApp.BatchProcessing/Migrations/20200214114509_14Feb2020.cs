﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Innoviz.SmartApp.BatchProcessing.Migrations
{
    public partial class _14Feb2020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "IntervalCount",
                table: "BatchTrigger",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "IntervalCount",
                table: "BatchTrigger",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
