﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Innoviz.SmartApp.BatchProcessing.Migrations
{
    public partial class _21May19 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BatchJob",
                columns: table => new
                {
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: false),
                    JobId = table.Column<Guid>(nullable: false),
                    GroupName = table.Column<string>(maxLength: 25, nullable: true),
                    ControllerUrl = table.Column<string>(nullable: true),
                    JobData = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 100, nullable: true),
                    JobStatus = table.Column<int>(nullable: true),
                    CompanyGUID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BatchJob", x => x.JobId);
                });

            migrationBuilder.CreateTable(
                name: "BatchTrigger",
                columns: table => new
                {
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: false),
                    TriggerId = table.Column<Guid>(nullable: false),
                    JobId = table.Column<Guid>(nullable: true),
                    CRONExpression = table.Column<string>(nullable: true),
                    RecurringCount = table.Column<int>(nullable: true),
                    StartDateTime = table.Column<DateTime>(nullable: false),
                    EndDateTime = table.Column<DateTime>(nullable: true),
                    IntervalType = table.Column<int>(nullable: true),
                    IntervalCount = table.Column<int>(nullable: true),
                    NextScheduledTime = table.Column<DateTime>(nullable: true),
                    LastExecuted = table.Column<DateTime>(nullable: true),
                    IsFinalized = table.Column<bool>(nullable: false),
                    CompanyGUID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BatchTrigger", x => x.TriggerId);
                    table.ForeignKey(
                        name: "FK_BatchTriggerBatchJob",
                        column: x => x.JobId,
                        principalTable: "BatchJob",
                        principalColumn: "JobId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BatchInstanceHistory",
                columns: table => new
                {
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: false),
                    InstanceHistoryId = table.Column<Guid>(nullable: false),
                    TriggerId = table.Column<Guid>(nullable: true),
                    JobId = table.Column<Guid>(nullable: false),
                    ScheduledTime = table.Column<DateTime>(nullable: true),
                    ActualStartTime = table.Column<DateTime>(nullable: true),
                    InstanceState = table.Column<int>(nullable: true),
                    ParamValues = table.Column<string>(nullable: true),
                    ControllerUrl = table.Column<string>(nullable: true),
                    FinishedDateTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BatchInstanceHistory", x => x.InstanceHistoryId);
                    table.ForeignKey(
                        name: "FK_BatchInstanceHistoryBatchJob",
                        column: x => x.JobId,
                        principalTable: "BatchJob",
                        principalColumn: "JobId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BatchInstanceHistoryBatchTrigger",
                        column: x => x.TriggerId,
                        principalTable: "BatchTrigger",
                        principalColumn: "TriggerId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BatchInstanceLog",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InstanceHistoryId = table.Column<Guid>(nullable: true),
                    ResultStatus = table.Column<int>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    TimeStamp = table.Column<DateTime>(type: "datetime", nullable: false),
                    ItemValues = table.Column<string>(nullable: true),
                    StackTrace = table.Column<string>(nullable: true),
                    Reference = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BatchInstanceLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BatchInstanceLogBatchInstancHistory",
                        column: x => x.InstanceHistoryId,
                        principalTable: "BatchInstanceHistory",
                        principalColumn: "InstanceHistoryId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BatchInstanceHistory_JobId",
                table: "BatchInstanceHistory",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_BatchInstanceHistory_TriggerId",
                table: "BatchInstanceHistory",
                column: "TriggerId");

            migrationBuilder.CreateIndex(
                name: "IX_BatchInstanceLog_InstanceHistoryId",
                table: "BatchInstanceLog",
                column: "InstanceHistoryId");

            migrationBuilder.CreateIndex(
                name: "IX_BatchTrigger_JobId",
                table: "BatchTrigger",
                column: "JobId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BatchInstanceLog");

            migrationBuilder.DropTable(
                name: "BatchInstanceHistory");

            migrationBuilder.DropTable(
                name: "BatchTrigger");

            migrationBuilder.DropTable(
                name: "BatchJob");
        }
    }
}
