﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Innoviz.SmartApp.BatchProcessing.Migrations
{
    public partial class _12oct2020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Owner",
                table: "BatchTrigger",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "OwnerBusinessUnitGUID",
                table: "BatchTrigger",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Owner",
                table: "BatchJob",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "OwnerBusinessUnitGUID",
                table: "BatchJob",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Owner",
                table: "BatchTrigger");

            migrationBuilder.DropColumn(
                name: "OwnerBusinessUnitGUID",
                table: "BatchTrigger");

            migrationBuilder.DropColumn(
                name: "Owner",
                table: "BatchJob");

            migrationBuilder.DropColumn(
                name: "OwnerBusinessUnitGUID",
                table: "BatchJob");
        }
    }
}
