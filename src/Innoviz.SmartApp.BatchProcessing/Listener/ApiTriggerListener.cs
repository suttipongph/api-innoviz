﻿using IdentityModel.Client;
using Innoviz.SmartApp.BatchProcessing.Model;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quartz;
using Serilog.Context;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.BatchProcessing.Listener
{
    public class ApiTriggerListener : ITriggerListener {
        public string Name { get; set; }

        private ILogger<ApiTriggerListener> logger;
        private string updateTriggerUrl;
        private string checkJobStatusUrl;

        private string reqApi;
        private string reqClient;
        private string reqSecret;
        private string authServer;

        private string dateTimeFormat;
        private readonly string listenerEventTemplate = "[{ListenerEvent}]";
        private readonly string jobTemplate = "Job Key:{JobKey}";
        private readonly string jobDetailTemplate = "JobData: {JobData}";
        private readonly string jobRunTime = "Job Runtime: {JobRunTime}";
        private readonly string triggerTemplate = "Trigger Key:{TriggerKey}";
        private readonly string startTimeTemplate = "Start Time:{StartTime:yyyy-MM-dd HH:mm:ss}";
        private readonly string scheduleTimeTemplate = "Schedule Fire Time: {ScheduledFireTime:yyyy-MM-dd HH:mm:ss}";
        private readonly string actualFireTimeTemplate = "Actual Fire Time: {ActualFireTime:yyyy-MM-dd HH:mm:ss}";
        private readonly string nextFireTimeTemplate = "Next Fire time: {NextFireTime:yyyy-MM-dd HH:mm:ss}";
        private readonly string endTimeTemplate = "End time: {EndTime:yyyy-MM-dd HH:mm:ss}";
        private readonly string lastExecutedTimeTemplate = "Last Executed time: {LastExecutedTime:yyyy-MM-dd HH:mm:ss}";

        public ApiTriggerListener(ILogger<ApiTriggerListener> logger, IConfiguration config) {
            this.logger = logger;
            Name = "ApiTriggerListener" + DateTime.Now;
            updateTriggerUrl = config["BatchApi:BaseUrl"] +
                                "Quartz/UpdateNextScheduledTime";
            checkJobStatusUrl = config["BatchApi:BaseUrl"] + "Quartz/checkJobStatus";

            reqApi = config["BatchApi:Scope"];
            reqClient = config["BatchApi:Client"];
            reqSecret = "smartAppS3c1234";
            authServer = config["JWT:Authority"];

            dateTimeFormat = config["AppSetting:DateTimeFormat"];
        }

        public Task TriggerComplete(ITrigger trigger, IJobExecutionContext context, SchedulerInstruction triggerInstructionCode, CancellationToken cancellationToken = default(CancellationToken)) {
            JobKey jobKey = context.JobDetail.Key;
            TimeSpan runtime = context.JobRunTime;
            DateTimeOffset now = DateTimeOffset.ParseExact(DateTime.Now.DateTimeToString(),
                                                           dateTimeFormat, CultureInfo.InvariantCulture);
            DateTime? nextFireTime = trigger.GetFireTimeAfter(now)?.ToLocalTime().DateTime;
            DateTime? lastExecuted = trigger.GetPreviousFireTimeUtc()?.ToLocalTime().DateTime;
            DateTime? scheduledTime = context.ScheduledFireTimeUtc?.ToLocalTime().DateTime;
            DateTime startTime = context.Trigger.StartTimeUtc.ToLocalTime().DateTime;
            DateTime? actualFireTime = context.FireTimeUtc.ToLocalTime().DateTime;
            DateTime? endTime = trigger.EndTimeUtc?.ToLocalTime().DateTime;

            using (LogContext.PushProperty("BatchEvent", "TriggerComplete"))
            using (LogContext.PushProperty("JobKey", jobKey))
            using (LogContext.PushProperty("TriggerKey", trigger.Key))
            using (LogContext.PushProperty("JobRunTime", runtime))
            using (LogContext.PushProperty("StartTime", startTime))
            using (LogContext.PushProperty("EndTime", endTime))
            using (LogContext.PushProperty("LastExecuted", lastExecuted))
            using (LogContext.PushProperty("ScheduledFireTime", scheduledTime))
            using (LogContext.PushProperty("FiredTime", actualFireTime))
            using (LogContext.PushProperty("NextFireTime", nextFireTime))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate = listenerEventTemplate + newLine +
                                     jobTemplate + newLine +
                                     triggerTemplate + newLine +
                                     jobRunTime + newLine +
                                     startTimeTemplate + newLine +
                                     endTimeTemplate + newLine +
                                     lastExecutedTimeTemplate + newLine +
                                     scheduleTimeTemplate + newLine +
                                     actualFireTimeTemplate + newLine +
                                     nextFireTimeTemplate;
                logger.LogInformation(msgTemplate, "TriggerListener: TriggerComplete",
                                      jobKey, trigger.Key, runtime, startTime, endTime, 
                                      lastExecuted, scheduledTime, actualFireTime, nextFireTime);
                return Task.CompletedTask;
            }
            
        }

        public Task TriggerFired(ITrigger trigger, IJobExecutionContext context, CancellationToken cancellationToken = default(CancellationToken)) {
            JobKey jobKey = context.JobDetail.Key;
            //TimeSpan runtime = context.JobRunTime;
            DateTimeOffset now = DateTimeOffset.ParseExact(DateTime.Now.DateTimeToString(),
                                                           dateTimeFormat, CultureInfo.InvariantCulture);
            DateTime? nextFireTime = trigger.GetFireTimeAfter(now)?.ToLocalTime().DateTime;
            DateTime? lastExecuted = trigger.GetPreviousFireTimeUtc()?.ToLocalTime().DateTime;
            DateTime? scheduledTime = context.ScheduledFireTimeUtc?.ToLocalTime().DateTime;
            DateTime startTime = context.Trigger.StartTimeUtc.ToLocalTime().DateTime;
            DateTime? endTime = trigger.EndTimeUtc?.ToLocalTime().DateTime;
            DateTime? actualFireTime = context.FireTimeUtc.ToLocalTime().DateTime;

            using (LogContext.PushProperty("BatchEvent", "TriggerFired"))
            using (LogContext.PushProperty("JobKey", jobKey))
            using (LogContext.PushProperty("TriggerKey", trigger.Key))
            using (LogContext.PushProperty("StartTime", startTime))
            using (LogContext.PushProperty("EndTime", endTime))
            using (LogContext.PushProperty("LastExecuted", lastExecuted))
            using (LogContext.PushProperty("ScheduledFireTime", scheduledTime))
            using (LogContext.PushProperty("FiredTime", actualFireTime))
            using (LogContext.PushProperty("NextFireTime", nextFireTime))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate = listenerEventTemplate + newLine +
                                     jobTemplate + newLine +
                                     triggerTemplate + newLine +
                                     startTimeTemplate + newLine +
                                     endTimeTemplate + newLine +
                                     lastExecutedTimeTemplate + newLine +
                                     scheduleTimeTemplate + newLine +
                                     actualFireTimeTemplate + newLine +
                                     nextFireTimeTemplate;
                logger.LogInformation(msgTemplate, "TriggerListener: TriggerFired",
                                      jobKey, trigger.Key, /*runtime,*/ startTime, endTime, 
                                      lastExecuted, scheduledTime, actualFireTime, nextFireTime);
                return Task.CompletedTask;
            }

        }

        public async Task TriggerMisfired(ITrigger trigger, CancellationToken cancellationToken = default(CancellationToken))
        {
            JobKey jobKey = trigger.JobKey;
            DateTimeOffset now = DateTimeOffset.ParseExact(DateTime.Now.DateTimeToString(),
                                                           dateTimeFormat, CultureInfo.InvariantCulture);
            DateTime? nextFireTime = trigger.GetFireTimeAfter(now)?.ToLocalTime().DateTime;
            DateTime? lastExecuted = trigger.GetPreviousFireTimeUtc()?.ToLocalTime().DateTime;
            DateTime startTime = trigger.StartTimeUtc.ToLocalTime().DateTime;
            DateTime? endTime = trigger.EndTimeUtc?.ToLocalTime().DateTime;

            string jobData = GetJsonJobData(trigger.JobDataMap);

            using (LogContext.PushProperty("BatchEvent", "TriggerMisfired"))
            using (LogContext.PushProperty("JobKey", jobKey))
            using (LogContext.PushProperty("TriggerKey", trigger.Key))
            using (LogContext.PushProperty("StartTime", startTime))
            using (LogContext.PushProperty("EndTime", endTime))
            using (LogContext.PushProperty("LastExecuted", lastExecuted))
            using (LogContext.PushProperty("NextFireTime", nextFireTime))
            using (LogContext.PushProperty("JobData", jobData))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate =
                listenerEventTemplate + newLine +
                jobTemplate + newLine +
                triggerTemplate + newLine +
                startTimeTemplate + newLine +
                endTimeTemplate + newLine +
                lastExecutedTimeTemplate + newLine +
                nextFireTimeTemplate + newLine +
                jobDetailTemplate;

                logger.LogInformation(msgTemplate, "TriggerListener: TriggerMisfired",
                                      jobKey, trigger.Key, startTime, endTime, lastExecuted,
                                      nextFireTime, jobData);

                await UpdateTriggerNextScheduledTime(trigger.Key.Group, trigger.Key.Name, false);
            }
        }

        public async Task<bool> VetoJobExecution(ITrigger trigger, IJobExecutionContext context, CancellationToken cancellationToken = default(CancellationToken)) {
            JobKey jobKey = trigger.JobKey;
            DateTimeOffset now = DateTimeOffset.ParseExact(DateTime.Now.DateTimeToString(),
                                                           dateTimeFormat, CultureInfo.InvariantCulture);
            DateTime? nextFireTime = trigger.GetFireTimeAfter(now)?.ToLocalTime().DateTime;
            DateTime? lastExecuted = trigger.GetPreviousFireTimeUtc()?.ToLocalTime().DateTime;
            DateTime startTime = trigger.StartTimeUtc.ToLocalTime().DateTime;
            DateTime? endTime = trigger.EndTimeUtc?.ToLocalTime().DateTime;

            using (LogContext.PushProperty("BatchEvent", "VetoJobExecution"))
            using (LogContext.PushProperty("JobKey", jobKey))
            using (LogContext.PushProperty("TriggerKey", trigger.Key))
            using (LogContext.PushProperty("StartTime", startTime))
            using (LogContext.PushProperty("EndTime", endTime))
            using (LogContext.PushProperty("LastExecuted", lastExecuted))
            using (LogContext.PushProperty("NextFireTime", nextFireTime))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate =
                listenerEventTemplate + newLine +
                jobTemplate + newLine +
                triggerTemplate + newLine +
                startTimeTemplate + newLine +
                endTimeTemplate + newLine +
                lastExecutedTimeTemplate + newLine +
                nextFireTimeTemplate; 

                logger.LogInformation(msgTemplate, "TriggerListener: VetoJobExecution",
                                      jobKey, trigger.Key, startTime, endTime, lastExecuted,
                                      nextFireTime/*, jobData*/);

                string isRetry = context.JobDetail.JobDataMap.GetString("isRetry");
                if (isRetry == "true")
                {
                    return false;
                }
                BatchTrigger trig = new BatchTrigger();
                trig.NextScheduledTime = nextFireTime;
                trig.JobId = new Guid(jobKey.Name);
                trig.TriggerId = new Guid(trigger.Key.Name);
                trig.CompanyGUID = Guid.Empty;

                string username = SmartAppUtil.GetMachineName();
                trig.ModifiedBy = username;

                bool isJobExecuting = await IsJobExecuting(trig);

                return isJobExecuting;
            }

        }

        private async Task UpdateTriggerNextScheduledTime(string triggerGroup, string triggerKey, bool isTriggerPaused) {
            try {
                using (var httpClient = new HttpClient()) {
                    string url = updateTriggerUrl;
                    string accessToken = await GetToken();

                    string requestId = Guid.NewGuid().GuidNullToString();

                    httpClient.SetBearerToken(accessToken);
                    httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);

                    string username = SmartAppUtil.GetMachineName();

                    TriggerKeyModel trigKey = new TriggerKeyModel()
                    {
                        Group = triggerGroup,
                        Name = triggerKey,
                        IsTriggerPaused = isTriggerPaused,
                        UserName = username
                    };

                    var jsonObject = JsonConvert.SerializeObject(trigKey, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                   
                }
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<bool> IsJobExecuting(BatchTrigger trigger) {
            try {
                using (var httpClient = new HttpClient()) {
                    string url = checkJobStatusUrl;
                    string accessToken = await GetToken();
                    string requestId = Guid.NewGuid().GuidNullToString();

                    httpClient.SetBearerToken(accessToken);
                    httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);

                    var jsonObject = JsonConvert.SerializeObject(trigger, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));
                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            return true;
                        }
                        else
                        {
                            // status != 400 or 500 (non-success status)
                            return true;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        bool result =
                            JsonConvert.DeserializeObject<bool>(jsonContent);
                        return result;
                    }
                    
                }
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<string> GetToken() {
            HttpHelper helper = new HttpHelper();
            return await helper.GetClientCredentialsToken(authServer, reqClient, reqSecret, reqApi);
            DiscoveryDocumentResponse discoRes;
            using (var httpClient = new HttpClient()) {
                var disco = await httpClient.GetDiscoveryDocumentAsync(authServer);
                if (disco.IsError) {
                    SmartAppException ex = new SmartAppException("Error loading Dicovery Document.");
                    ex.AddData(disco.Error);
                    throw SmartAppUtil.AddStackTrace(ex);

                }
                else {
                    discoRes = disco;
                }
            }

            TokenResponse tokenResponse;
            using (var httpClient = new HttpClient()) {
                var response = await httpClient.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
                {
                    Address = discoRes.TokenEndpoint,
                    ClientId = reqClient,
                    ClientSecret = reqSecret,
                    Scope = reqApi,

                });
                if (response.IsError) {
                    SmartAppException ex = new SmartAppException("Error getting token.");
                    ex.AddData(response.Error);
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else {
                    tokenResponse = response;
                }
            }
            return tokenResponse?.AccessToken;

        }
        private string GetJsonJobData(JobDataMap jobData)
        {
            try
            {
                JobDataMap tmp = jobData;
                if (jobData.Count() != 0)
                {
                    foreach (var k in tmp.GetKeys())
                    {
                        string value = (string)tmp[k];
                        if (value != null)
                        {
                            if (value.StartsWith('[') || value.StartsWith('{'))
                            {
                                var json = JToken.Parse(value);
                                tmp[k] = json;
                            }

                        }

                    }
                }
                string data = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                return data;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
