﻿using IdentityModel.Client;
using Innoviz.SmartApp.BatchProcessing.Model;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.WebApi.BatchProcessing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quartz;
using Serilog.Context;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.BatchProcessing.BatchEnum;

namespace Innoviz.SmartApp.BatchProcessing.Listener
{
    public class ApiSchedulerListener : ISchedulerListener {
        private ILogger<ApiSchedulerListener> logger;
        private string updateTriggerFinalizedUrl;
        private string updateScheduledJobUrl;
        private string updateNextScheduledTimeUrl;
        private string updateInstanceStateSchedulerStartedUrl;

        private string webApi;
        private string quartzApi;
        private string reqClient;
        private string reqSecret;
        private string authServer;

        private string dateTimeFormat;
        private readonly string listenerEventTemplate = "[{ListenerEvent}]";
        private readonly string jobTemplate = "Job Key:{JobKey}";
        private readonly string jobDetailTemplate = "JobData: {JobData}";
        private readonly string triggerTemplate = "Trigger Key:{TriggerKey}";
        private readonly string startTimeTemplate = "Start Time:{StartTime:yyyy-MM-dd HH:mm:ss}";
        private readonly string nextFireTimeTemplate = "Next Fire time: {NextFireTime:yyyy-MM-dd HH:mm:ss}";
        private readonly string endTimeTemplate = "End time: {EndTime:yyyy-MM-dd HH:mm:ss}";
        private readonly string lastExecutedTimeTemplate = "Last Executed time: {LastExecutedTime:yyyy-MM-dd HH:mm:ss}";

        public ApiSchedulerListener(ILogger<ApiSchedulerListener> logger, IConfiguration config) {
            this.logger = logger;
            
            updateTriggerFinalizedUrl = config["BatchApi:BaseUrl"] +
                                "Quartz/updateTriggerFinalized";
            
            updateScheduledJobUrl = config["BatchApi:BaseUrl"] +
                                    "Quartz/updateScheduledJob";
            updateNextScheduledTimeUrl = config["BatchApi:BaseUrl"] +
                                "Quartz/UpdateNextScheduledTime";
            updateInstanceStateSchedulerStartedUrl = config["BatchApi:BaseUrl"] +
                                "Quartz/UpdateInstancesSchedulerStarted";

            webApi = config["BusinessApi:Resource"];
            quartzApi = config["BatchApi:Resource"];
            reqClient = config["BatchApi:Client"];
            reqSecret = "smartAppS3c1234";
            authServer = config["JWT:Authority"];

            dateTimeFormat = config["AppSetting:DateTimeFormat"];
        }
        public Task JobAdded(IJobDetail jobDetail, CancellationToken cancellationToken = default(CancellationToken)) {

            JobKey jobKey = jobDetail.Key;
            string jobData = GetJsonJobData(jobDetail.JobDataMap);
            using (LogContext.PushProperty("BatchEvent", "JobAdded"))
            using (LogContext.PushProperty("JobKey", jobKey))
            using (LogContext.PushProperty("JobData", jobData))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate = 
                    listenerEventTemplate + newLine +
                    jobTemplate + newLine +
                    jobDetailTemplate;
                logger.LogInformation(msgTemplate, "SchedulerListener: JobAdded",
                                      jobKey, jobData);
                return Task.CompletedTask;
            }
        }

        public Task JobDeleted(JobKey jobKey, CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "JobDeleted"))
            using (LogContext.PushProperty("JobKey", jobKey))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate =
                    listenerEventTemplate + newLine +
                    jobTemplate;
                logger.LogInformation(msgTemplate, "SchedulerListener: JobDeleted",
                                      jobKey);
                return Task.CompletedTask;
            }
        }

        public Task JobInterrupted(JobKey jobKey, CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "JobInterrupted"))
            using (LogContext.PushProperty("JobKey", jobKey))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate =
                    listenerEventTemplate + newLine +
                    jobTemplate;
                logger.LogInformation(msgTemplate, "SchedulerListener: JobInterrupted",
                                      jobKey);
                return Task.CompletedTask;
            }
            
        }

        public Task JobPaused(JobKey jobKey, CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "JobPaused"))
            using (LogContext.PushProperty("JobKey", jobKey))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate =
                    listenerEventTemplate + newLine +
                    jobTemplate;
                logger.LogInformation(msgTemplate, "SchedulerListener: JobPaused",
                                      jobKey);
                return Task.CompletedTask;
            }
            
        }

        public Task JobResumed(JobKey jobKey, CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "JobResumed"))
            using (LogContext.PushProperty("JobKey", jobKey))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate =
                    listenerEventTemplate + newLine +
                    jobTemplate;
                logger.LogInformation(msgTemplate, "SchedulerListener: JobResumed",
                                      jobKey);
                return Task.CompletedTask;
            }
            
        }

        public async Task JobScheduled(ITrigger trigger, CancellationToken cancellationToken = default(CancellationToken)) {
            var jobKey = trigger.JobKey;
            var triggerKey = trigger.Key;
            var startTime = trigger.StartTimeUtc.ToLocalTime().DateTime;
            var endTime = trigger.EndTimeUtc?.ToLocalTime().DateTime;

            DateTimeOffset now = DateTimeOffset.ParseExact(DateTime.Now.DateTimeToString(),
                                                            dateTimeFormat, CultureInfo.InvariantCulture);
            var nextFireTime = trigger.GetFireTimeAfter(now)?.ToLocalTime().DateTime;

            using (LogContext.PushProperty("BatchEvent", "JobScheduled"))
            using (LogContext.PushProperty("JobKey", jobKey))
            using (LogContext.PushProperty("TriggerKey", triggerKey))
            using (LogContext.PushProperty("StartTime", startTime))
            using (LogContext.PushProperty("EndTime", endTime))
            using (LogContext.PushProperty("NextFireTime", nextFireTime))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate =
                listenerEventTemplate + newLine +
                jobTemplate + newLine +
                triggerTemplate + newLine +
                startTimeTemplate + newLine +
                endTimeTemplate + newLine +
                nextFireTimeTemplate;

                logger.LogInformation(msgTemplate, "SchedulerListener: JobScheduled",
                                      jobKey, triggerKey, startTime, endTime, nextFireTime);

                //update Next scheduled time & job status
                BatchObject batchObject = new BatchObject();
                BatchJob job = new BatchJob();
                BatchTrigger trig = new BatchTrigger();

                job.JobId = new Guid(trigger.JobKey.Name);
                job.JobStatus = Convert.ToInt32(BatchJobStatus.Waiting);

                if (jobKey.Group == "SystemRetry")
                {
                    trig = null;
                    Thread.Sleep(1000);
                }
                else
                {
                    await UpdateTriggerNextScheduledTime(trigger.Key.Group, trigger.Key.Name, false);
                }
            }
            
        }

        public Task JobsPaused(string jobGroup, CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "JobsPaused"))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate = listenerEventTemplate + newLine +
                                    "Job Group: {JobGroup}";
                logger.LogInformation(msgTemplate, "SchedulerListener: JobsPaused",
                                        jobGroup);
                return Task.CompletedTask;
            }
            
        }

        public Task JobsResumed(string jobGroup, CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "JobsResumed"))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate = listenerEventTemplate + newLine +
                                    "Job Group: {JobGroup}";
                logger.LogInformation(msgTemplate, "SchedulerListener: JobsResumed",
                                        jobGroup);
                return Task.CompletedTask;
            }
            
        }

        public Task JobUnscheduled(TriggerKey triggerKey, CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "JobUnscheduled"))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate = listenerEventTemplate + newLine +
                                        triggerTemplate;
                logger.LogInformation(msgTemplate, "SchedulerListener: JobUnscheduled",
                                     triggerKey);
                return Task.CompletedTask;
            }
        }

        public Task SchedulerError(string msg, SchedulerException cause, CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "SchedulerError"))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate = listenerEventTemplate + newLine +
                                        "Error message: {ErrorMessage}";
                logger.LogError(cause, msgTemplate, "SchedulerListener: SchedulerError", msg);
                return Task.CompletedTask;
            }


        }

        public Task SchedulerInStandbyMode(CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "SchedulerInStandbyMode"))
            {
                logger.LogInformation(listenerEventTemplate, "SchedulerListener: SchedulerInStandbyMode");
                return Task.CompletedTask;
            }
            
        }

        public Task SchedulerShutdown(CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "SchedulerShutdown"))
            {
                logger.LogInformation(listenerEventTemplate, "SchedulerListener: SchedulerShutdown");
                return Task.CompletedTask;
            }
        }

        public Task SchedulerShuttingdown(CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "SchedulerShuttingdown"))
            {
                logger.LogInformation(listenerEventTemplate, "SchedulerListener: SchedulerShuttingdown");
                return Task.CompletedTask;
            }
        }

        public Task SchedulerStarted(CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "SchedulerStarted"))
            {
                logger.LogInformation(listenerEventTemplate, "SchedulerListener: SchedulerStarted");

                //intended async call
                UpdateInstanceStateSchedulerStarted(DateTime.Now);

                return Task.CompletedTask;
            }
        }

        public Task SchedulerStarting(CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "SchedulerStarting"))
            {
                logger.LogInformation(listenerEventTemplate, "SchedulerListener: SchedulerStarting");
                return Task.CompletedTask;
            }
        }

        public Task SchedulingDataCleared(CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "SchedulingDataCleared"))
            {
                logger.LogInformation(listenerEventTemplate, "SchedulerListener: SchedulingDataCleared");
                return Task.CompletedTask;
            }
        }

        public async Task TriggerFinalized(ITrigger trigger, CancellationToken cancellationToken = default(CancellationToken)) {
            var jobkey = trigger.JobKey;
            DateTime startTime = trigger.StartTimeUtc.ToLocalTime().DateTime;
            DateTime? endTime = trigger.EndTimeUtc?.ToLocalTime().DateTime;
            DateTime? lastExecuted = trigger.GetPreviousFireTimeUtc()?.ToLocalTime().DateTime;

            using (LogContext.PushProperty("BatchEvent", "TriggerFinalized"))
            using (LogContext.PushProperty("JobKey", jobkey))
            using (LogContext.PushProperty("TriggerKey", trigger.Key))
            using (LogContext.PushProperty("StartTime", startTime))
            using (LogContext.PushProperty("EndTime", endTime))
            using (LogContext.PushProperty("LastExecuted", lastExecuted))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate = listenerEventTemplate + newLine +
                                    jobTemplate + newLine +
                                    triggerTemplate + newLine +
                                    startTimeTemplate + newLine +
                                    endTimeTemplate + newLine +
                                    lastExecutedTimeTemplate;

                logger.LogInformation(msgTemplate, "SchedulerListener: TriggerFinalized",
                                      jobkey, trigger.Key, startTime, endTime, lastExecuted);

                if (jobkey.Group != "SystemRetry")
                {
                    //update finalized trigger
                    BatchTrigger trig = new BatchTrigger();
                    trig.TriggerId = new Guid(trigger.Key.Name);
                    trig.IsFinalized = true;
                    trig.CompanyGUID = Guid.Empty;

                    string username = SmartAppUtil.GetMachineName();
                    trig.ModifiedBy = username;

                    await UpdateTriggerFinalized(trig);
                }

            }
        }

        public async Task TriggerPaused(TriggerKey triggerKey, CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "TriggerPaused"))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate = listenerEventTemplate + newLine +
                                        triggerTemplate;
                logger.LogInformation(msgTemplate, "SchedulerListener: TriggerPaused",
                                     triggerKey);
                await UpdateTriggerNextScheduledTime(triggerKey.Group, triggerKey.Name, true);
            }
        }

        public async Task TriggerResumed(TriggerKey triggerKey, CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "TriggerResumed"))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate = listenerEventTemplate + newLine +
                                        triggerTemplate;
                logger.LogInformation(msgTemplate, "SchedulerListener: TriggerResumed",
                                     triggerKey);
                await UpdateTriggerNextScheduledTime(triggerKey.Group, triggerKey.Name, false);
            }
            
        }

        public Task TriggersPaused(string triggerGroup, CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "TriggersPaused"))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate = listenerEventTemplate + newLine +
                                        "Trigger Group: {TriggerGroup}";
                logger.LogInformation(msgTemplate, "SchedulerListener: TriggersPaused",
                                        triggerGroup);
                return Task.CompletedTask;
            }

        }

        public Task TriggersResumed(string triggerGroup, CancellationToken cancellationToken = default(CancellationToken)) {
            using (LogContext.PushProperty("BatchEvent", "TriggersResumed"))
            {
                string newLine = TextConstants.NewLine;
                string msgTemplate = listenerEventTemplate + newLine +
                                        "Trigger Group: {TriggerGroup}";
                logger.LogInformation(msgTemplate, "SchedulerListener: TriggersResumed",
                                        triggerGroup);
                return Task.CompletedTask;
            }
        }
        
        private async Task UpdateScheduledJob(BatchObject batchObject) {
            try {
                using (var httpClient = new HttpClient()) {
                    string url = updateScheduledJobUrl;

                    string accessToken = await GetToken(webApi);
                    string requestId = Guid.NewGuid().GuidNullToString();

                    httpClient.SetBearerToken(accessToken);
                    httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);

                    var jsonObject = JsonConvert.SerializeObject(batchObject, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                }
            }
            catch (Exception e) {
                e = SmartAppUtil.AddStackTrace(e);
                string newLine = TextConstants.NewLine;
                string msgTemplate = newLine + "#Error updating scheduled job." + newLine;
                LogErrorFromException(e, msgTemplate);
                throw e;
            }
        }

        private async Task UpdateTriggerFinalized(BatchTrigger batchTrigger) {
            try {
                using (var httpClient = new HttpClient()) {
                    string url = updateTriggerFinalizedUrl;

                    string accessToken = await GetToken(quartzApi);
                    string requestId = Guid.NewGuid().GuidNullToString();

                    httpClient.SetBearerToken(accessToken);
                    httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);

                    var jsonObject = JsonConvert.SerializeObject(batchTrigger, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                }
            }
            catch (Exception e) {
                e = SmartAppUtil.AddStackTrace(e);
                string newLine = TextConstants.NewLine;
                string msgTemplate = newLine + "#Error updating trigger finalized." + newLine;
                LogErrorFromException(e, msgTemplate);
                throw e;
            }
        }
        private async Task UpdateTriggerNextScheduledTime(string triggerGroup, string triggerKey, bool isTriggerPaused) {
            try {
                using (var httpClient = new HttpClient()) {
                    string url = updateNextScheduledTimeUrl;
                    string accessToken = await GetToken(quartzApi);
                    string requestId = Guid.NewGuid().GuidNullToString();

                    httpClient.SetBearerToken(accessToken);
                    httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);

                    string username = SmartAppUtil.GetMachineName();

                    TriggerKeyModel trigKey = new TriggerKeyModel()
                    {
                        Group = triggerGroup,
                        Name = triggerKey,
                        IsTriggerPaused = isTriggerPaused,
                        UserName = username
                    };

                    var jsonObject = JsonConvert.SerializeObject(trigKey, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    
                }
            }
            catch (Exception e) {
                e = SmartAppUtil.AddStackTrace(e);
                string newLine = TextConstants.NewLine;
                string msgTemplate = newLine + "#Error updating next scheduled time." + newLine;
                LogErrorFromException(e, msgTemplate);
                throw e;
            }
        }
        private async Task UpdateInstanceStateSchedulerStarted(DateTime startedTime) {
            try {
                using (var httpClient = new HttpClient()) {
                    string url = updateInstanceStateSchedulerStartedUrl;

                    string accessToken = await GetToken(quartzApi);
                    string requestId = Guid.NewGuid().GuidNullToString();

                    httpClient.SetBearerToken(accessToken);
                    httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);

                    DateTimeParameter model = new DateTimeParameter();
                    model.dateTime = startedTime;

                    var jsonObject = JsonConvert.SerializeObject(model, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));
                        

                }
            }
            catch (Exception e) {
                e = SmartAppUtil.AddStackTrace(e);
                string newLine = TextConstants.NewLine;
                string msgTemplate = newLine + "#Error updating value at scheduler start." + newLine;
                LogErrorFromException(e, msgTemplate);
                throw e;
            }
        }
        private async Task<string> GetToken(string reqApi) {
            HttpHelper helper = new HttpHelper();
            //return await helper.GetClientCredentialsToken(authServer, reqClient, reqSecret, reqApi);
            DiscoveryDocumentResponse discoRes;
            using (var httpClient = new HttpClient()) {
                var disco = await httpClient.GetDiscoveryDocumentAsync(authServer);
                if (disco.IsError) {
                    SmartAppException ex = new SmartAppException("Error loading Dicovery Document.");
                    ex.AddData(disco.Error);
                    throw SmartAppUtil.AddStackTrace(ex);

                }
                else {
                    discoRes = disco;
                }
            }

            TokenResponse tokenResponse;
            using (var httpClient = new HttpClient()) {
                var response = await httpClient.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
                {
                    Address = discoRes.TokenEndpoint,
                    ClientId = reqClient,
                    ClientSecret = reqSecret,
                    Scope = reqApi,

                });
                if (response.IsError) {
                    SmartAppException ex = new SmartAppException("Error getting token.");
                    ex.AddData(response.Error);
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else {
                    tokenResponse = response;
                }
            }
            return tokenResponse?.AccessToken;

        }
        private void LogErrorFromException(Exception e, string messageTemplate) {
            string msgTemplate = (messageTemplate != null) ? messageTemplate : "";
            string newLine = TextConstants.NewLine;
            List<string> logItems = new List<string>();

            if (e.Data[ExceptionDataKey.MessageList] != null)
            {
                List<string> messages = (List<string>)e.Data[ExceptionDataKey.MessageList];
                if (messages.Count() != 0)
                {

                    for (int i = 0; i < messages.Count(); i++)
                    {
                        var item = messages[i];
                        msgTemplate += "{Msg" + i + "}";
                        logItems.Add(item);
                        if (i < messages.Count() - 1)
                        {
                            msgTemplate += newLine;
                        }
                    }
                    logger.LogError(msgTemplate, logItems.ToArray<object>());
                }
            }
            
        }

        private string GetJsonJobData(JobDataMap jobData)
        {
            try
            {
                JobDataMap tmp = jobData;
                if(jobData.Count() != 0)
                {
                    foreach (var k in tmp.GetKeys())
                    {
                        string value = (string)tmp[k];
                        if(value != null)
                        {
                            if (value.StartsWith('[') || value.StartsWith('{'))
                            {
                                var json = JToken.Parse(value);
                                tmp[k] = json;
                            }

                        }

                    }
                }
                string data = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                return data;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
