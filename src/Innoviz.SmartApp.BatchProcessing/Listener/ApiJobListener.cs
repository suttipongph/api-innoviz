﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using Serilog.Context;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.BatchProcessing.Listener {
    public class ApiJobListener : IJobListener {

        public string Name { get; set; }

        private ILogger<ApiJobListener> logger;
        private string dateTimeFormat;
        
        private readonly string listenerEventTemplate = "[{ListenerEvent}]";
        private readonly string jobTemplate = "Job Key:{JobKey}";
        private readonly string jobRunTime = "Job Runtime: {JobRunTime}";
        private readonly string triggerTemplate = "Trigger Key:{TriggerKey}";
        private readonly string startTimeTemplate = "Start Time:{StartTime:yyyy-MM-dd HH:mm:ss}";
        private readonly string scheduleTimeTemplate = "Schedule Fire Time: {ScheduledFireTime:yyyy-MM-dd HH:mm:ss}";
        private readonly string nextFireTimeTemplate = "Next Fire time: {NextFireTime:yyyy-MM-dd HH:mm:ss}";

        public ApiJobListener(ILogger<ApiJobListener> logger, IConfiguration config) {
            this.logger = logger;
            Name = "ApiJobListener" + DateTime.Now;
            dateTimeFormat = config["AppSetting:DateTimeFormat"];
        }

        public Task JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken = default(CancellationToken)) {
            JobKey jobKey = context.JobDetail.Key;
            ITrigger trig = context.Trigger;
            var runtime = context.JobRunTime;
            var startTime = trig.StartTimeUtc.ToLocalTime().DateTime;
            var firedTime = context.ScheduledFireTimeUtc?.ToLocalTime().DateTime;

            DateTimeOffset now = DateTimeOffset.ParseExact(DateTime.Now.DateTimeToString(), 
                                                            dateTimeFormat, CultureInfo.InvariantCulture);
            var nextFireTime = trig.GetFireTimeAfter(now)?.ToLocalTime().DateTime;

            using (LogContext.PushProperty("BatchEvent", "JobExecutionVetoed"))
            using (LogContext.PushProperty("JobKey", jobKey))
            using (LogContext.PushProperty("TriggerKey", trig.Key))
            using (LogContext.PushProperty("JobRunTime", runtime))
            using (LogContext.PushProperty("ScheduledFireTime", firedTime))
            using (LogContext.PushProperty("NextFireTime", nextFireTime))
            {
                string newLine = TextConstants.NewLine;
                string messageTemplate = listenerEventTemplate + newLine +
                                     jobTemplate + newLine +
                                     triggerTemplate + newLine +
                                     jobRunTime + newLine +
                                     startTimeTemplate + newLine +
                                     scheduleTimeTemplate + newLine +
                                     nextFireTimeTemplate;
                logger.LogInformation(messageTemplate, "JobListener: JobExecutionVetoed",
                                      jobKey, trig.Key, runtime, startTime, firedTime, nextFireTime);
                return Task.CompletedTask;
            }            
        }
        
        public Task JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken = default(CancellationToken)) {
            JobKey jobKey = context.JobDetail.Key;
            ITrigger trig = context.Trigger;
            var startTime = trig.StartTimeUtc.ToLocalTime().DateTime;
            var firedTime = context.ScheduledFireTimeUtc?.ToLocalTime().DateTime;

            DateTimeOffset now = DateTimeOffset.ParseExact(DateTime.Now.DateTimeToString(),
                                                            dateTimeFormat, CultureInfo.InvariantCulture);
            var nextFireTime = trig.GetFireTimeAfter(now)?.ToLocalTime().DateTime;

            using (LogContext.PushProperty("BatchEvent", "JobToBeExecuted"))
            using (LogContext.PushProperty("JobKey", jobKey))
            using (LogContext.PushProperty("TriggerKey", trig.Key))
            using (LogContext.PushProperty("ScheduledFireTime", firedTime))
            using (LogContext.PushProperty("NextFireTime", nextFireTime))
            {
                string newLine = TextConstants.NewLine;
                string messageTemplate = listenerEventTemplate + newLine +
                                     jobTemplate + newLine +
                                     triggerTemplate + newLine +
                                     startTimeTemplate + newLine +
                                     scheduleTimeTemplate + newLine +
                                     nextFireTimeTemplate;
                logger.LogInformation(messageTemplate, "JobListener: JobToBeExecuted",
                                    jobKey, trig.Key, startTime, firedTime, nextFireTime);
                return Task.CompletedTask;
            }
            
        }
        
        public Task JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException, CancellationToken cancellationToken = default(CancellationToken)) {
            
            JobKey jobKey = context.JobDetail.Key;
            ITrigger trig = context.Trigger;
            var runtime = context.JobRunTime;
            var startTime = trig.StartTimeUtc.ToLocalTime().DateTime;
            var firedTime = context.ScheduledFireTimeUtc?.ToLocalTime().DateTime;

            DateTimeOffset now = DateTimeOffset.ParseExact(DateTime.Now.DateTimeToString(),
                                                            dateTimeFormat, CultureInfo.InvariantCulture);
            var nextFireTime = trig.GetFireTimeAfter(now)?.ToLocalTime().DateTime;

            using (LogContext.PushProperty("BatchEvent", "JobWasExecuted"))
            using (LogContext.PushProperty("JobKey", jobKey))
            using (LogContext.PushProperty("TriggerKey", trig.Key))
            using (LogContext.PushProperty("JobRunTime", runtime))
            using (LogContext.PushProperty("ScheduledFireTime", firedTime))
            using (LogContext.PushProperty("NextFireTime", nextFireTime))
            {
                string newLine = TextConstants.NewLine;
                string messageTemplate = listenerEventTemplate + newLine +
                                     jobTemplate + newLine +
                                     triggerTemplate + newLine +
                                     jobRunTime + newLine +
                                     startTimeTemplate + newLine +
                                     scheduleTimeTemplate + newLine +
                                     nextFireTimeTemplate;
                logger.LogInformation(messageTemplate, "JobListener: JobWasExecuted",
                                    jobKey, trig.Key, runtime, startTime, firedTime, nextFireTime);
                return Task.CompletedTask;
            }
        }
    }
}
