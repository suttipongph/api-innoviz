﻿using Innoviz.SmartApp.BatchProcessing.Service;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.BatchProcessing;
using Innoviz.SmartApp.Data.InterfaceWithOtherSystems;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.WebApi.BatchProcessing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Quartz;
using Serilog.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.BatchProcessing.BatchEnum;

namespace Innoviz.SmartApp.BatchProcessing.Job
{
    public class ApiJob : IJob
    {
        private readonly ILogger<ApiJob> logger;

        private SmartAppContext smrtCtx;
        private QuartzDbContext qrtzCtx;
        private InterfaceAXDbContext axCtx;

        private IBatchLogService batchLogService;
        private ISysTransactionLogService transactionLogService;

        public ApiJob(ILogger<ApiJob> logger, IConfiguration config, 
                        SmartAppContext smrtAppCtx, QuartzDbContext qrtzCtx,
                        InterfaceAXDbContext axCtx,
                        ISysTransactionLogService transactionLogService,
                        IBatchLogService batchlogservice) {
            this.logger = logger;

            smrtCtx = smrtAppCtx;
            this.qrtzCtx = qrtzCtx;
            this.axCtx = axCtx;

            batchLogService = batchlogservice;
            this.transactionLogService = transactionLogService;
        }

        public async Task Execute(IJobExecutionContext context) {
            try {
                #region prepare data for execution
                // preparing data
                JobDataMap dataMap = context.JobDetail.JobDataMap;
                string callUrl = dataMap.GetString("callUrl");
                string jobData = dataMap.GetString("jobData");
                string isRetry = dataMap.GetString("isRetry");
                string companyGUID = dataMap.GetString("companyGUID");
                JobKey jobkey = context.JobDetail.Key;
                IScheduler scheduler = context.Scheduler;

                BatchTrigger batchTrigger;
                BatchJob batchJob;
                BatchInstanceHistory item;
                BatchInstanceObject insObject;

                batchTrigger = JsonConvert.DeserializeObject<BatchTrigger>(dataMap.GetString("batchTrigger"));
                batchJob = JsonConvert.DeserializeObject<BatchJob>(dataMap.GetString("batchJob"));
                batchJob.JobStatus = Convert.ToInt32(BatchJobStatus.Executing);

                Guid newInstanceId = Guid.NewGuid();
                using (LogContext.PushProperty("BatchInstanceId", newInstanceId))
                using (LogContext.PushProperty("BatchJobKey", jobkey))
                using (LogContext.PushProperty("BatchTriggerKey", context.Trigger.Key))
                using (LogContext.PushProperty("BatchEvent", "ApiJob.Excecute"))
                {
                    if (isRetry == "true")
                    {
                        item = new BatchInstanceHistory
                        {
                            InstanceHistoryId = newInstanceId,
                            JobId = new Guid(context.JobDetail.Key.Name),
                            TriggerId = null,
                            ControllerUrl = callUrl,
                            ParamValues = jobData,
                            ScheduledTime = (context.ScheduledFireTimeUtc)?.ToLocalTime().DateTime,
                            ActualStartTime = (context.FireTimeUtc).ToLocalTime().DateTime,
                            InstanceState = Convert.ToInt32(BatchInstanceState.Retrying)
                        };

                        batchTrigger.LastExecuted = item.ActualStartTime;
                        batchTrigger.NextScheduledTime = null;

                        insObject = new BatchInstanceObject()
                        {
                            BatchInstanceHistory = item,
                            BatchJob = batchJob,
                            BatchTrigger = batchTrigger
                        };
                    }
                    else
                    {
                        item = new BatchInstanceHistory
                        {
                            InstanceHistoryId = newInstanceId,
                            JobId = new Guid(context.JobDetail.Key.Name),
                            TriggerId = new Guid(context.Trigger.Key.Name),
                            ControllerUrl = callUrl,
                            ParamValues = jobData,
                            ScheduledTime = (context.ScheduledFireTimeUtc)?.ToLocalTime().DateTime,
                            ActualStartTime = (context.FireTimeUtc).ToLocalTime().DateTime,
                            InstanceState = Convert.ToInt32(BatchInstanceState.Executing)
                        };

                        ITrigger trig = context.Trigger;
                        DateTime? nextScheduledTime = trig.GetNextFireTimeUtc()?.ToLocalTime().DateTime;

                        DateTime? lastExecuted = item.ActualStartTime;

                        batchTrigger.NextScheduledTime = nextScheduledTime;
                        batchTrigger.LastExecuted = lastExecuted;

                        insObject = new BatchInstanceObject
                        {
                            BatchInstanceHistory = item,
                            BatchJob = batchJob,
                            BatchTrigger = batchTrigger,
                            NextScheduledTime = nextScheduledTime,
                            LastExecuted = lastExecuted
                        };

                    }
                    #endregion

                    #region execute job
                    // execute job
                    string username = SmartAppUtil.GetMachineName();
                    string instanceHistoryId = null;

                    IQuartzBatchService quartzBatchService = new QuartzBatchService(qrtzCtx);
                    try
                    {
                        SystemParameter param = GetSysParmValue(smrtCtx, username, companyGUID);
                        param.InstanceHistoryId = insObject.BatchInstanceHistory.InstanceHistoryId.GuidNullToString();
                        instanceHistoryId = param.InstanceHistoryId;

                        qrtzCtx.SetSystemParameter(param);
                        smrtCtx.SetSystemParameter(param);

                        DetachAllEntries(qrtzCtx);
                        insObject = quartzBatchService.CreateJobInstance(insObject, username);

                        List<DbContext> contexts = new List<DbContext>()
                        {
                            (SmartAppDbContext)smrtCtx, axCtx
                        };

                        BatchUtil batchUtil = new BatchUtil();
                        BatchFunctionClassMapping mapping = batchUtil.GetMappingFromCallUrl(callUrl);

                        Type callType = batchUtil.GetCallerClassFromCallUrl(callUrl);
                        dynamic callerInstance = Activator.CreateInstance(callType, new object[] { contexts, transactionLogService, batchLogService });
                        var paramInfo = callType.GetMethod(mapping.MethodName).GetParameters().ToList().FirstOrDefault();

                        object[] paramObjects;
                        if(paramInfo != null)
                        {
                            Type paramType = paramInfo.ParameterType;
                            dynamic paramObject = JsonConvert.DeserializeObject(jobData, paramType);
                            paramObjects = new object[] { paramObject };
                        }
                        else
                        {
                            paramObjects = null;
                        }
                        
                        bool isSuccessResult = false;
                        bool isExpectedError = false;

                        try
                        {
                            Type returnType = callType.GetMethod(mapping.MethodName).ReturnType;

                            if (returnType.BaseType == typeof(Task))
                            {
                                await Task.Run(async () =>
                                {
                                    await callType.GetMethod(mapping.MethodName).Invoke(callerInstance, paramObjects);
                                });
                            }
                            else
                            {
                                await Task.Run(() =>
                                {
                                    callType.GetMethod(mapping.MethodName).Invoke(callerInstance, paramObjects);
                                });
                            }

                            logger.LogInformation("Job finished execution successfully.");
                            isSuccessResult = true;
                        }
                        catch (Exception e)
                        {
                            e = SmartAppUtil.AddStackTrace(e);
                            LogErrorFromJobExecution(e, instanceHistoryId);
                            isSuccessResult = false;
                            isExpectedError = e.InnerException != null &&  e.InnerException.GetType() == typeof(SmartAppException);
                        }
                        DetachAllEntries(qrtzCtx);
                        TriggerKey trigKey = new TriggerKey(insObject.BatchTrigger.TriggerId.GuidNullToString(),
                                                                    insObject.BatchJob.GroupName);

                        if (isSuccessResult || (!isSuccessResult && isExpectedError))
                        {
                            insObject.BatchJob.JobStatus = Convert.ToInt32(BatchJobStatus.Waiting);
                            if (isRetry == "true")
                            {
                                insObject.BatchInstanceHistory.InstanceState = Convert.ToInt32(BatchInstanceState.RetryCompleted);
                                quartzBatchService.UpdateInstanceState(insObject, username);

                                await scheduler.ResumeTrigger(trigKey);
                            }
                            else
                            {
                                insObject.BatchInstanceHistory.InstanceState = Convert.ToInt32(BatchInstanceState.Completed);
                                quartzBatchService.UpdateInstanceState(insObject, username);
                            }
                        }
                        else
                        {
                            //failed status
                            insObject.BatchJob.JobStatus = Convert.ToInt32(BatchJobStatus.Executing);
                            if (isRetry == "true")
                            {
                                insObject.BatchInstanceHistory.InstanceState = Convert.ToInt32(BatchInstanceState.RetryFailed);
                                insObject = quartzBatchService.UpdateInstanceState(insObject, username);
                            }
                            else
                            {
                                insObject.BatchInstanceHistory.InstanceState = Convert.ToInt32(BatchInstanceState.Failed);
                                insObject = quartzBatchService.UpdateInstanceState(insObject, username);

                                await scheduler.PauseTrigger(trigKey);
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        //failed status
                        insObject.BatchJob.JobStatus = Convert.ToInt32(BatchJobStatus.Executing);
                        if (isRetry == "true")
                        {
                            insObject.BatchInstanceHistory.InstanceState = Convert.ToInt32(BatchInstanceState.RetryFailed);
                            insObject = quartzBatchService.UpdateInstanceState(insObject, username);
                        }
                        else
                        {
                            insObject.BatchInstanceHistory.InstanceState = Convert.ToInt32(BatchInstanceState.Failed);
                            insObject = quartzBatchService.UpdateInstanceState(insObject, username);

                            TriggerKey trigKey = new TriggerKey(insObject.BatchTrigger.TriggerId.GuidNullToString(),
                                                                    insObject.BatchJob.GroupName);
                            await scheduler.PauseTrigger(trigKey);
                        }
                        e = SmartAppUtil.AddStackTrace(e);
                        throw;
                    }
                    #endregion
                }

            }
            catch(Exception e) {
                e = SmartAppUtil.AddStackTrace(e);
                LogSystemErrorFromJobExcecution(e, null);

                throw;
            }

        }
        private void DetachAllEntries(QuartzDbContext db) {
            try {
                var changedEntriesCopy = db.ChangeTracker.Entries()
                                            .Where(e => e.State == EntityState.Added ||
                                                        e.State == EntityState.Modified ||
                                                        e.State == EntityState.Deleted)
                                            .ToList();

                foreach (var entry in changedEntriesCopy) {
                    entry.State = EntityState.Detached;
                }
                    
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private void LogErrorFromJobExecution(Exception e, string instanceHistoryId) {
            string newLine = TextConstants.NewLine;
            string msgTemplate =
                newLine + "#JobFinishedExecutionWithError" + newLine +
                "Job finished execution with error: " + e.Message + newLine;

            AddBatchInstanceLogError(e, instanceHistoryId);
            LogErrorFromException(e, msgTemplate);
        }
        private void LogSystemErrorFromJobExcecution(Exception e, string instanceHistoryId) {
            string newLine = TextConstants.NewLine;
            string msgTemplate =
                newLine + "#JobExecutionError" + newLine;

            AddBatchInstanceLogError(e, instanceHistoryId);
            LogErrorFromException(e, msgTemplate);
        }
        private void LogErrorFromException(Exception e, 
                                            string messageTemplate) {
            string msgTemplate = (messageTemplate != null) ? messageTemplate : "";
            string newLine = TextConstants.NewLine;
            List<string> logItems = new List<string>();

            if (e.Data[ExceptionDataKey.MessageList] != null) {
                List<string> messages = (List<string>)e.Data[ExceptionDataKey.MessageList];
                if (messages.Count() != 0) {
                    
                    for(int i=0; i< messages.Count(); i++)
                    {
                        var item = messages[i];
                        msgTemplate += "{Msg" + i + "}";
                        logItems.Add(item);
                        if(i < messages.Count() -1)
                        {
                            msgTemplate += newLine;
                        }
                    }
                    logger.LogError(msgTemplate, logItems.ToArray<object>());
                }
            }
            
        }
        private SystemParameter GetSysParmValue(SmartAppContext context, string username, string companyGUID)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(context);
                ISysUserService userService = new SysUserService(context);
                SysUserTable user = userService.GetSysUserTableByUserNameNoTracking(username);
                SystemParameter result = new SystemParameter
                {
                    AccessLevel = new AccessLevelParm { AccessLevel = (int)AccessLevel.Company },
                    CompanyGUID = companyGUID,
                    BusinessUnitGUID = accessLevelService.GetOwnerBusinessUnitGUIDByUserName(username, companyGUID).GuidNullToString(),
                    UserName = username,
                    UserId = user != null ? user.Id.GuidNullToString() : null,
                };
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void AddBatchInstanceLogError(Exception ex, string instanceHistoryId)
        {
            try
            {
                batchLogService.LogBatchErrors(ex.InnerException ?? ex, instanceHistoryId);
                
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
