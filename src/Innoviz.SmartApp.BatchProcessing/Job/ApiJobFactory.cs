﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Spi;
using System;

namespace Innoviz.SmartApp.BatchProcessing.Job {
    public class ApiJobFactory : IJobFactory {
        private readonly IServiceProvider _serviceProvider;
        protected readonly IServiceScope _scope;

        public ApiJobFactory(IServiceProvider serviceProvider) {
            this._serviceProvider = serviceProvider;
            _scope = serviceProvider.CreateScope();
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler) {
            try {
                IJobDetail jobDetail = bundle.JobDetail;
                Type jobType = jobDetail.JobType;

                return _scope.ServiceProvider.GetService(jobType) as IJob;
            }
            catch (Exception e) {
                throw new SchedulerException($"Problem instantiating class '{bundle.JobDetail.JobType.FullName}'", e);
            }
        }

        public void ReturnJob(IJob job) {
            var disposable = job as IDisposable;
            disposable?.Dispose();
        }
    }
}
