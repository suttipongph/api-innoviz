﻿using Innoviz.SmartApp.BatchProcessing.Job;
using Innoviz.SmartApp.BatchProcessing.Service;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.WebApi.BatchProcessing;
using Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Quartz;
using System;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.BatchProcessing.Controller
{
    [Produces("application/json")]
    [Route("api/quartz/[controller]")]
    public class QuartzController : ControllerBase {
        private BuilderHelper _builderHelper;
        private readonly IScheduler _scheduler;

        private QuartzDbContext db;
        private IQuartzBatchService quartzBatchService;

        private string dateTimeFormat;

        public QuartzController(IScheduler scheduler, IConfiguration configuration, QuartzDbContext context) {
            _scheduler = scheduler;
            _builderHelper = new BuilderHelper();

            db = context;
            quartzBatchService = new QuartzBatchService(db);

            dateTimeFormat = configuration["AppSetting:DateTimeFormat"];
        }
        
        [HttpPost]
        [Authorize(AuthenticationSchemes = "user")]
        [Route("GetQuartzBatchViewList")]
        public ActionResult GetBatchViewList([FromBody]SearchParameter search)
        {
            try
            {
                string companyGUID = GetCompanyFromRequest();
                return Ok(quartzBatchService.GetBatchViewList(search, companyGUID));
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = "user")]
        [Route("GetQuartzBatchHistoryViewList")]
        public ActionResult GetBatchHistoryViewList([FromBody]SearchParameter search)
        {
            try
            {
                return Ok(quartzBatchService.GetBatchHistoryViewList(search));
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = "user")]
        [Route("GetQuartzBatchInstanceLogViewList")]
        public ActionResult GetBatchInstanceLogViewList([FromBody]SearchParameter search)
        {
            try
            {
                return Ok(quartzBatchService.GetBatchInstanceLogViewList(search));
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = "user")]
        [Route("GetQuartzBatchViewItem/{jobId}")]
        public ActionResult GetQuartzBatchViewItem(string jobId) {
            try {
                return Ok(quartzBatchService.GetQuartzBatchViewItem(jobId));
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = "user")]
        [Route("GetQuartzBatchHistoryViewItem/{instanceId}")]
        public ActionResult GetQuartzBatchHistoryViewItem(string instanceId) {
            try {
                return Ok(quartzBatchService.GetQuartzBatchHistoryViewItem(instanceId));
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = "user")]
        [Route("GetQuartzBatchInstanceLogViewItem/{id}")]
        public ActionResult GetQuartzBatchInstanceLogViewItem(int id) {
            try {
                return Ok(quartzBatchService.GetQuartzBatchInstanceLogViewItem(id));
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void SetSystemParameter(string username = null, string ownerBusinessUnitGUID = null, string companyGUID = null)
        {
            try
            {
                SystemParameter sysParm = new SystemParameter
                {
                    CompanyGUID = companyGUID,
                    UserName = username ?? GetUserNameFromToken(),
                    BearerToken = GetBearerTokenFromRequest(),
                    BusinessUnitGUID = ownerBusinessUnitGUID
                };
                db.SetSystemParameter(sysParm);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("scheduleJob")]
        [Authorize(AuthenticationSchemes = "user")]
        public async Task<ActionResult> ScheduleJob([FromBody]BatchObjectView model) { 
            try {
                string username = model.CreatedBy ?? GetUserNameFromToken();
                string companyGUID = GetCompanyFromRequest();
                SetSystemParameter(username, model.OwnerBusinessUnitGUID);

                BatchObject batchObject = 
                    quartzBatchService.CreateJobAndTriggerFromBatchObjectView(model, companyGUID, username);

                try {
                    string groupName = batchObject.BatchJob.GroupName;
                    IJobDetail job = CreateJob(batchObject);
                    ITrigger trigger = CreateTrigger(batchObject.BatchTrigger, groupName);

                    var result = await _scheduler.ScheduleJob(job, trigger);

                }
                catch (Exception e) {
                    // error scheduling job, rollback on BatchJob/BatchTrigger
                    quartzBatchService.RemoveJobAndTrigger(batchObject);
                    throw SmartAppUtil.AddStackTrace(e);
                }
                
                return Ok("Job Scheduled.");
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        [HttpPost]
        [Route("unscheduleJob")]
        [Authorize(AuthenticationSchemes = "user")]
        public async Task<ActionResult> UnscheduleJob([FromBody]BatchView inputModel) {
            try {
                string username = inputModel.ModifiedBy ?? GetUserNameFromToken();
                string companyGUID = GetCompanyFromRequest();
                SetSystemParameter(username, inputModel.OwnerBusinessUnitGUID);
                BatchObject model = quartzBatchService.PrepareJobAndTriggerFromBatchView(inputModel);
                int? origJobStatus = model.BatchJob.JobStatus;

                quartzBatchService.CancelJob(model, username);

                string group = model.BatchJob.GroupName;
                if (group == null) {
                    group = SchedulerConstants.DefaultGroup;
                }
                string triggerId = model.BatchTrigger.TriggerId.ToString().ToLower();

                TriggerKey trigKey = new TriggerKey(triggerId, group);
                var trigger = await _scheduler.GetTrigger(trigKey);
                if(trigger != null) {
                    try
                    {
                        await _scheduler.UnscheduleJob(trigKey);
                    }
                    catch (Exception e)
                    {
                        //roll-back
                        string modifiedBy = SmartAppUtil.GetMachineName();
                        model.BatchJob.JobStatus = origJobStatus;
                        quartzBatchService.UpdateJob(model.BatchJob, modifiedBy);
                        throw SmartAppUtil.AddStackTrace(e);
                    }
                    
                    return Ok("Job unscheduled.");
                }
                else {
                    return Ok("No trigger to unschedule.");
                }
                
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("pauseJob")]
        [Authorize(AuthenticationSchemes = "user")]
        public async Task<ActionResult> PauseJob([FromBody]BatchView inputModel) {
            try {
                string username = inputModel.ModifiedBy ?? GetUserNameFromToken();
                string companyGUID = GetCompanyFromRequest();
                SetSystemParameter(username, inputModel.OwnerBusinessUnitGUID);
                BatchObject model = quartzBatchService.PrepareJobAndTriggerFromBatchView(inputModel);
                int? origJobStatus = model.BatchJob.JobStatus;

                quartzBatchService.HoldJob(model, username);

                string group = model.BatchJob.GroupName;
                if (group == null) {
                    group = SchedulerConstants.DefaultGroup;
                }
                string triggerId = model.BatchTrigger.TriggerId.ToString().ToLower();

                TriggerKey trigKey = new TriggerKey(triggerId, group);
                var trigger = await _scheduler.GetTrigger(trigKey);
                if (trigger != null) {
                    try
                    {
                        await _scheduler.PauseTrigger(trigKey);
                    }
                    catch(Exception e)
                    {
                        //roll-back
                        string modifiedBy = SmartAppUtil.GetMachineName();
                        model.BatchJob.JobStatus = origJobStatus;
                        quartzBatchService.UpdateJob(model.BatchJob, modifiedBy);

                        throw SmartAppUtil.AddStackTrace(e);
                    }
                    
                    return Ok("Job paused." );
                }
                else {
                    return Ok("No trigger to pause.");
                }
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("resumeJob")]
        [Authorize(AuthenticationSchemes = "user")]
        public async Task<ActionResult> ResumeJob([FromBody]BatchView inputModel) {
            try {
                string username = inputModel.ModifiedBy ?? GetUserNameFromToken();
                string companyGUID = GetCompanyFromRequest();
                SetSystemParameter(username, inputModel.OwnerBusinessUnitGUID);
                BatchObject model = quartzBatchService.PrepareJobAndTriggerFromBatchView(inputModel);
                int? origJobStatus = model.BatchJob.JobStatus;

                quartzBatchService.ReleaseJob(model, username);

                string group = model.BatchJob.GroupName;
                if (group == null) {
                    group = SchedulerConstants.DefaultGroup;
                }
                string triggerId = model.BatchTrigger.TriggerId.ToString().ToLower();

                TriggerKey trigKey = new TriggerKey(triggerId, group);
                var trigger = await _scheduler.GetTrigger(trigKey);
                if (trigger != null) {
                    try
                    {
                        await _scheduler.ResumeTrigger(trigKey);
                    }
                    catch(Exception e)
                    {
                        // roll-back 
                        string modifiedBy = SmartAppUtil.GetMachineName();
                        model.BatchJob.JobStatus = origJobStatus;
                        quartzBatchService.UpdateJob(model.BatchJob, modifiedBy);
                        throw SmartAppUtil.AddStackTrace(e);
                    }
                    return Ok("Job resumed.");
                }
                else {
                    return Ok("No trigger to resume.");
                }
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        [HttpPost]
        [Route("UpdateNextScheduledTime")]
        [Authorize(AuthenticationSchemes = "system")]
        public async Task<ActionResult> UpdateNextScheduledTime([FromBody]Model.TriggerKeyModel model) {
            try {
                string triggerkey = model.Name;
                string group = model.Group;
                if(group.Equals(SchedulerConstants.DefaultGroup, StringComparison.InvariantCultureIgnoreCase)) {
                    group = SchedulerConstants.DefaultGroup;
                }
                TriggerKey k = new TriggerKey(triggerkey, group);
                
                BatchTrigger trigger = new BatchTrigger();
                trigger.TriggerId = new Guid(triggerkey);

                if(!model.IsTriggerPaused) {
                    ITrigger trig = await _scheduler.GetTrigger(k);
                    
                    DateTimeOffset now = DateTimeOffset.ParseExact(DateTime.Now.DateTimeToString(), dateTimeFormat, CultureInfo.InvariantCulture);
                    trigger.NextScheduledTime = trig.GetFireTimeAfter(now)?.ToLocalTime().DateTime;
                }
                else {
                    trigger.NextScheduledTime = null;
                }
                SetSystemParameter(model.UserName);
                quartzBatchService.UpdateNextScheduledTime(trigger, model.UserName);
               
                return Ok("trigger updated");
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("updateTriggerFinalized")]
        [Authorize(AuthenticationSchemes = "system")]
        public ActionResult UpdateTriggerFinalized([FromBody]BatchTrigger model) {
            try {
                SetSystemParameter(model.ModifiedBy);
                return Ok(quartzBatchService.UpateTriggerFinalized(model));
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("checkJobStatus")]
        [Authorize(AuthenticationSchemes = "system")]
        public ActionResult IsJobExecuting([FromBody]BatchTrigger model) {
            try {
                SetSystemParameter(model.ModifiedBy);
                return Ok(quartzBatchService.IsJobExecuting(model));
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ValidateRetryInstanceHistory")]
        public ActionResult ValidateRetryInstanceHistory([FromBody]BatchHistoryView model)
        {
            try
            {
                return Ok(quartzBatchService.ValidateRetryInstanceHistory(model));
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("retryJob")]
        [Authorize(AuthenticationSchemes = "user")]
        public async Task<ActionResult> RetryJob([FromBody]BatchHistoryView inputModel) {
            try {
                string username = inputModel.ModifiedBy ?? GetUserNameFromToken();
                string companyGUID = GetCompanyFromRequest();
                SetSystemParameter(username);
                BatchInstanceObject model = quartzBatchService.GetBatchInstanceObjectFromInstanceId(inputModel);
                int? origJobStatus = model.BatchJob.JobStatus;
                int? origInstanceState = model.BatchInstanceHistory.InstanceState;

                quartzBatchService.RetryJobInstance(model, username);

                try {
                    string group = model.BatchJob.GroupName;
                    if(group == null)
                    {
                        group = SchedulerConstants.DefaultGroup;
                    }
                    string jobId = model.BatchJob.JobId.GuidNullToString();
                    IJobDetail job = await _scheduler.GetJobDetail(new JobKey(jobId, group));
                    var jobBuilder = job.GetJobBuilder();

                    string retryGroupName = "SystemRetry";
                    job = jobBuilder
                            .WithIdentity(jobId, retryGroupName)
                            .StoreDurably(true)
                            .UsingJobData("isRetry", "true").Build();

                    await _scheduler.AddJob(job, true);

                    // intended async call
                    TriggerRetry(job);

                }
                catch (Exception e) {
                    //roll-back
                    string modifiedBy = SmartAppUtil.GetMachineName();
                    model.BatchInstanceHistory.InstanceState = origInstanceState;
                    model.BatchJob.JobStatus = origJobStatus;

                    quartzBatchService.UpdateJobAndInstanceHistory(model, modifiedBy);

                    throw SmartAppUtil.AddStackTrace(e);
                }
                
                return Ok("Job triggered.");
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("cancelInstance")]
        [Authorize(AuthenticationSchemes = "user")]
        public async Task<ActionResult> CancelInstance([FromBody]BatchHistoryView inputModel) {
            try {
                string username = inputModel.ModifiedBy ?? GetUserNameFromToken();
                string companyGUID = GetCompanyFromRequest();
                SetSystemParameter(username);
                BatchInstanceObject model = quartzBatchService.GetBatchInstanceObjectFromInstanceId(inputModel);
                int? origJobStatus = model.BatchJob.JobStatus;
                int? origInstanceState = model.BatchInstanceHistory.InstanceState;

                quartzBatchService.CancelInstance(model, username);

                string group = model.BatchJob.GroupName;
                if (group == null) {
                    group = SchedulerConstants.DefaultGroup;
                }
                string triggerId = model.BatchTrigger.TriggerId.ToString().ToLower();

                TriggerKey trigKey = new TriggerKey(triggerId, group);
                var trigger = await _scheduler.GetTrigger(trigKey);
                if (trigger != null) {
                    try
                    {
                        await _scheduler.ResumeTrigger(trigKey);
                    }
                    catch (Exception e)
                    {
                        // roll-back 
                        string modifiedBy = SmartAppUtil.GetMachineName();
                        model.BatchJob.JobStatus = origJobStatus;
                        model.BatchInstanceHistory.InstanceState = origInstanceState;

                        quartzBatchService.UpdateJobAndInstanceHistory(model, modifiedBy);
                        throw SmartAppUtil.AddStackTrace(e);
                    }
                    return Ok("Job resumed.");
                }
                else {
                    return Ok("No trigger to resume.");
                }
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateInstancesSchedulerStarted")]
        public async Task UpdateInstancesSchedulerStarted([FromBody]Model.DateTimeParameter model) {
            try {
                string modifiedBy = SmartAppUtil.GetMachineName();
                SetSystemParameter(modifiedBy);
                await quartzBatchService.UpdateBatchHistoryBatchJobAtSchedulerStarted(model.dateTime, dateTimeFormat, _scheduler);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("cleanUpHistory")]
        [Authorize(AuthenticationSchemes = "user")]
        public ActionResult CleanUpHistory([FromBody]CleanUpBatchHistoryView model) {
            try {
                //SetSystemParameter(username);
                return Ok(quartzBatchService.CleanUpBatchHistory(model));
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("deleteBatchJob")]
        [Authorize(AuthenticationSchemes = "user")]
        public ActionResult DeleteBatchJob([FromBody]BatchView model) {
            try {
                return Ok(quartzBatchService.DeleteBatchJob(model));
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("test1/{jobkey}")]
        public async Task<ActionResult> test(string jobkey) {
            try {
                IJobDetail job = await _scheduler.GetJobDetail(new JobKey("xxx"));
                var jobBuilder = job.GetJobBuilder();
                jobBuilder.UsingJobData("instanceId", "xxxx");
                return Ok("okayy");
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        private async Task TriggerRetry(IJobDetail job) {
            try {
                await _scheduler.TriggerJob(job.Key);

                await _scheduler.DeleteJob(job.Key);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IJobDetail CreateJob(BatchObject batchObject) {
            BatchJob inputJob = batchObject.BatchJob;
            inputJob.BatchTrigger = null;
            inputJob.BatchInstanceHistory = null;
            BatchTrigger inputTrigger = batchObject.BatchTrigger;
            inputTrigger.BatchInstanceHistory = null;
            inputTrigger.Job = null;

            var jobBuilder = JobBuilder.Create<ApiJob>();

            // set job Id
            jobBuilder = _builderHelper.SetJobIdentity(jobBuilder,
                                           inputJob.JobId.ToString().ToLower(),
                                           inputJob.GroupName);
            string batchJob = JsonConvert.SerializeObject(inputJob, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });
            string batchTrigger = JsonConvert.SerializeObject(inputTrigger, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });
            IJobDetail job = jobBuilder.WithDescription(inputJob.Description)
                                   .UsingJobData("callUrl", inputJob.ControllerUrl)
                                   .UsingJobData("jobData", inputJob.JobData)
                                   .UsingJobData("companyGUID", inputJob.CompanyGUID.GuidNullToString())
                                   .UsingJobData("batchJob", batchJob)
                                   .UsingJobData("batchTrigger", batchTrigger)
                                   .Build();
            return job;
        }

        private ITrigger CreateTrigger(BatchTrigger inputTrigger, string groupName) {

            DateTimeOffset startTime = inputTrigger.StartDateTime;
            DateTimeOffset now = DateTime.Now;
            if(startTime < now)
            {
                startTime = DateTime.Now;
            }
            var triggerBuilder = TriggerBuilder.Create()
                                               .StartAt(startTime);
            
            if (inputTrigger.EndDateTime != null) {
                DateTimeOffset endTime = Convert.ToDateTime(inputTrigger.EndDateTime).AddDays(1);
                triggerBuilder = triggerBuilder.EndAt(endTime);
            }

            // set trigger Id
            triggerBuilder = _builderHelper.SetTriggerIdentity(triggerBuilder,
                                                   inputTrigger.TriggerId.ToString().ToLower(),
                                                   inputTrigger.JobId.ToString().ToLower(),
                                                   groupName);

            // set trigger schedule
            triggerBuilder = _builderHelper.SetTriggerSchedule(triggerBuilder,
                                                    inputTrigger);
            
            return triggerBuilder.Build();
        }
        
        private string GetUserNameFromToken() {
            var fromUserClaims = HttpContext.User?.Claims?
                                            .Where(x => x.Type == "username").SingleOrDefault();
            string username = (fromUserClaims != null) ? fromUserClaims.Value : null;
            if(username == null) {
                throw new NullReferenceException();
            }
            else {
                return username;
            }
        }
        private string GetCompanyFromRequest() {
            var request = HttpContext.Request;
            string companyHeader = request.Headers["CompanyHeader"];
            if(companyHeader == null) {
                throw new NullReferenceException();
            }
            else {
                return companyHeader;
            }
        }
        private string GetBearerTokenFromRequest()
        {
            var request = HttpContext.Request;
            string authHeader = request.Headers["Authorization"];
            if (authHeader == null)
            {
                throw new NullReferenceException();
            }
            else
            {
                return authHeader.Split(" ").Last();
            }
        }
    }
}
