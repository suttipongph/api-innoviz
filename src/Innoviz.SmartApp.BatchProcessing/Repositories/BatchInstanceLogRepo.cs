﻿using Innoviz.SmartApp.BatchProcessing;
using Innoviz.SmartApp.BatchProcessing.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.BatchProcessing.ViewModelHandler.Models;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace Innoviz.SmartApp.WebApi.BatchProcessing.BatchRepo
{

    public interface IBatchInstanceLogRepo {

        SearchResult<BatchInstanceLogView> GetListvw(SearchParameter search);
        BatchInstanceLogView GetByIdvw(int id);

        int[] GetLogStatusGroups(Guid instanceHistoryId);

        void Add(BatchInstanceLog item);
        void Add(IEnumerable<BatchInstanceLog> list);
    }

    public class BatchInstanceLogRepo: QuartzBaseRepo<BatchInstanceLog>, IBatchInstanceLogRepo
    {
        public BatchInstanceLogRepo(QuartzDbContext context) : base(context) {

        }
        public override void ValidateAdd(BatchInstanceLog item) {

        }
        public override void ValidateUpdate(BatchInstanceLog item) {

        }
        public override void ValidateRemove(BatchInstanceLog item) {

        }
        
        private IQueryable<BatchInstanceLogViewMap> GetQuery()
        {
            try
            {
                var result = (from batchInstanceLog in Entity
                              select new BatchInstanceLogViewMap
                              {
                                  Id = batchInstanceLog.Id,
                                  Message = batchInstanceLog.Message,
                                  Status = batchInstanceLog.ResultStatus,
                                  Values = batchInstanceLog.ItemValues,
                                  StackTrace = batchInstanceLog.StackTrace,
                                  TimeStamp = batchInstanceLog.TimeStamp,
                                  InstanceHistoryId = batchInstanceLog.InstanceHistoryId,
                                  Reference = batchInstanceLog.Reference,
                                  CreatedBy = "",
                                  ModifiedBy = "",
                                  CreatedDateTime = DateTime.Now,
                                  ModifiedDateTime = DateTime.Now
                              });
                return result;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public SearchResult<BatchInstanceLogView> GetListvw(SearchParameter search)
        {
            try
            {
                var result = new SearchResult<BatchInstanceLogView>();
                var predicate = search.GetSearchPredicate(typeof(BatchInstanceLogViewMap));

                var total = GetQuery()
                               .Where(predicate.Predicates, predicate.Values)
                               .AsNoTracking()
                               .Count();

                var list = GetQuery()
                                .Where(predicate.Predicates, predicate.Values)
                                .OrderBy(predicate.Sorting)
                                .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                .Take(search.Paginator.Rows.GetPaginatorRows(total))
                                .AsNoTracking()
                                .ToMaps<BatchInstanceLogViewMap, BatchInstanceLogView>();

                result = list.SetSearchResult<BatchInstanceLogView>(total, search);
                return result;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public BatchInstanceLogView GetByIdvw(int id) {
            try {
                var result = GetQuery()
                                .Where(item=>item.Id == id)
                                .AsNoTracking()
                                .FirstOrDefault()
                                .ToMap<BatchInstanceLogViewMap, BatchInstanceLogView>();
                return result;
            }
            catch (Exception ex) {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public int[] GetLogStatusGroups(Guid instanceHistoryId) {
            var list = (from instanceLog in Entity.Where(item =>
                                                item.InstanceHistoryId == instanceHistoryId)
                        group instanceLog by instanceLog.ResultStatus into g
                        select g.Key)
                        .ToList()
                        .ToArray();

            return list;

        }
    }
}
