﻿using Innoviz.SmartApp.BatchProcessing.Service;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.BatchProcessing.Repositories
{
    public class QuartzBaseRepo<TEntity> : IDisposable, IRepository<TEntity> where TEntity : class {
        protected QuartzDbContext db;
        protected DbSet<TEntity> Entity;
        public SmartAppException ex;
        
        private bool HasCreatedDateTime;
        private bool HasModifiedDateTime;
        private bool HasCreatedBy;
        private bool HasModifiedBy;


        #region LIT
        private bool HasCompany;
        private bool HasOwner;
        private bool HasOwnerBusinessUnitGUID;

        protected readonly SystemParameter SysParm;
        #endregion LIT

        public QuartzBaseRepo(QuartzDbContext context) {
            db = context;
            Entity = db.Set<TEntity>();

            HasCreatedBy = typeof(TEntity).BaseType.GetProperty("CreatedBy") != null ? true : false;
            HasCreatedDateTime = typeof(TEntity).BaseType.GetProperty("CreatedDateTime") != null ? true : false;
            HasModifiedBy = typeof(TEntity).BaseType.GetProperty("ModifiedBy") != null ? true : false;
            HasModifiedDateTime = typeof(TEntity).BaseType.GetProperty("ModifiedDateTime") != null ? true : false;

            SysParm = db.GetSystemParameter();

            #region LIT
            HasCompany = typeof(TEntity).BaseType.GetProperty("CompanyGUID") != null ? true : false;
            HasOwner = typeof(TEntity).BaseType.GetProperty("Owner") != null;
            HasOwnerBusinessUnitGUID = typeof(TEntity).BaseType.GetProperty("OwnerBusinessUnitGUID") != null;
            #endregion LIT
        }
        public QuartzBaseRepo(QuartzDbContext context, SystemParameter systemParameter) : this(context)
        {
            SysParm = systemParameter;
        }
        public virtual void ValidateAdd(TEntity item)
        {
            try
            {
                CheckAccessLevel(item);
                CheckDuplicate(item);
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }
        public virtual void ValidateAdd(IEnumerable<TEntity> items)
        {
            try
            {
                CheckAccessLevel(items);
                CheckDuplicate(items);
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }
        public virtual void ValidateUpdate(TEntity item)
        {
            try
            {
                CheckAccessLevel(item);
                CheckDuplicate(item);
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }
        public virtual void ValidateUpdate(IEnumerable<TEntity> items)
        {
            try
            {
                CheckAccessLevel(items);
                CheckDuplicate(items);
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }

        public virtual void ValidateRemove(TEntity item)
        {
            try
            {
                CheckAccessLevel(item);
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }
        public virtual void ValidateRemove(IEnumerable<TEntity> items)
        {
            try
            {
                foreach (var item in items)
                {
                    CheckAccessLevel(item);
                }
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }
        public virtual TEntity Find(params object[] keyValues)
        {
            try
            {
                return Entity.Find(keyValues);
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }
        public virtual bool Exist(params object[] keyValues)
        {
            try
            {
                return (Entity.Find(keyValues) != null ? true : false);
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }
        public bool Exist(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                int i = Entity.Where(predicate).Count();


                return (i > 0 ? true : false);
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }
        public virtual IEnumerable<TEntity> GetList()
        {
            try
            {
                return Entity.ToList();
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }

        public virtual IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                return Entity.Where(predicate).ToList();

            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }

        protected virtual IQueryable<TEntity> GetByKey(params object[] keyValues)
        {
            try
            {
                var entity = (TEntity)Activator.CreateInstance(typeof(TEntity));
                var primaryKeys = db.Entry(entity).Metadata.FindPrimaryKey().Properties;

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < keyValues.Count(); i++)
                {
                    string keyName = primaryKeys[i].Name;
                    builder.Append(keyName);
                    builder.Append(" == ");
                    builder.Append("@");
                    builder.Append(i);

                    if (i < keyValues.Count() - 1)
                    {
                        builder.Append(" and ");
                    }
                }

                Console.WriteLine(builder.ToString());

                return Entity.Where(builder.ToString(), keyValues);

            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }

        public virtual void Add(TEntity item)
        {
            try
            {
                ValidateAdd(item);
                if (this.HasCreatedBy && this.HasModifiedBy)
                {
                    db.Entry(item).Property("CreatedBy").CurrentValue = SysParm.UserName;
                    db.Entry(item).Property("ModifiedBy").CurrentValue = SysParm.UserName;
                }
                if (this.HasCreatedDateTime && this.HasModifiedDateTime)
                {
                    db.Entry(item).Property("CreatedDateTime").CurrentValue = DateTime.Now;
                    db.Entry(item).Property("ModifiedDateTime").CurrentValue = DateTime.Now;
                }

                Entity.Add(item);
                db.Entry(item).State = EntityState.Added;

            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }
        public virtual void Add(IEnumerable<TEntity> items)
        {
            try
            {
                foreach (var item in items)
                {
                    ValidateAdd(item);

                    if (this.HasCreatedBy && this.HasModifiedBy)
                    {
                        db.Entry(item).Property("CreatedBy").CurrentValue = SysParm.UserName;
                        db.Entry(item).Property("ModifiedBy").CurrentValue = SysParm.UserName;
                    }
                    if (this.HasCreatedDateTime && this.HasModifiedDateTime)
                    {
                        db.Entry(item).Property("CreatedDateTime").CurrentValue = DateTime.Now;
                        db.Entry(item).Property("ModifiedDateTime").CurrentValue = DateTime.Now;
                    }
                }

                Entity.AddRange(items);

                foreach (var item in items)
                {
                    db.Entry(item).State = EntityState.Added;
                }

            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);

                }
            }
        }
        public void UpdateSystemFields(TEntity item)
        {
            if (this.HasCreatedBy && this.HasModifiedBy)
            {
                db.Entry(item).Property("CreatedBy").CurrentValue = db.Entry(item).Property("CreatedBy").OriginalValue;
                db.Entry(item).Property("ModifiedBy").CurrentValue = SysParm.UserName;
            }
            if (this.HasCreatedDateTime && this.HasModifiedDateTime)
            {
                db.Entry(item).Property("CreatedDateTime").CurrentValue = db.Entry(item).Property("CreatedDateTime").OriginalValue;
                db.Entry(item).Property("ModifiedDateTime").CurrentValue = DateTime.Now;
            }
        }
        public virtual void Update(TEntity item)
        {
            try
            {
                ValidateUpdate(item);
                UpdateSystemFields(item);

                Entity.Attach(item);
                db.Entry(item).State = EntityState.Modified;

            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }
        public virtual void Update(IEnumerable<TEntity> items)
        {
            try
            {
                foreach (var item in items)
                {
                    ValidateUpdate(item);
                    UpdateSystemFields(item);
                }

                Entity.AttachRange(items);

                foreach (var item in items)
                {
                    db.Entry(item).State = EntityState.Modified;
                }

            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }
        public virtual void Remove(TEntity item)
        {
            try
            {
                ValidateRemove(item);

                Entity.Remove(item);
                db.Entry(item).State = EntityState.Deleted;
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }

        }
        public virtual void Remove(IEnumerable<TEntity> items)
        {
            try
            {
                Entity.RemoveRange(items);
                foreach (var item in items)
                {
                    ValidateRemove(item);

                    db.Entry(item).State = EntityState.Deleted;
                }
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }

        }
        public virtual void CheckDuplicate(TEntity item)
        {
            try
            {
                var itemToCheck = new TEntity[] { item };
                CheckDuplicate(itemToCheck);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public virtual void CheckDuplicate(IEnumerable<TEntity> items)
        {
            try
            {
                int itemsCount = items != null ? items.Count() : -1;
                if (itemsCount < 1)
                {
                    return;
                }

                Type modelType = typeof(TEntity);
                string modelName = modelType.Name;
                var sysDuplicateDetectionData = SystemStaticData.GetSysDuplicateDetectionData()
                                                                .Where(a => a.ModelName == modelName
                                                                         && a.InActive == false);
                if (sysDuplicateDetectionData.Count() == 0)
                {
                    return;
                }

                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                string errParams = "";
                var primaryKey = db.Entry(items.FirstOrDefault()).Metadata.FindPrimaryKey().Properties.Select(s => s.PropertyInfo);

                PropertyInfo[] properties = modelType.GetProperties();
                var notExistProps = (from data in sysDuplicateDetectionData
                                     where !(from prop in properties.AsQueryable()
                                             select prop.Name).Contains(data.PropertyName)
                                     select data.PropertyName);
                if (notExistProps.Count() != 0)
                {
                    errParams = string.Join(", ", notExistProps);
                    ex.AddData("ERROR.DUPLICATE_NOT_FOUND", new string[] { errParams, modelName });
                }
                else
                {
                    var groupByRules = sysDuplicateDetectionData.GroupBy(g => g.RuleNum);
                    int pkCount = primaryKey.Count();
                    foreach (var rule in groupByRules)
                    {
                        var checkProperties = (from property in properties
                                               join duplicateData in sysDuplicateDetectionData
                                               on property.Name equals duplicateData.PropertyName
                                               where duplicateData.RuleNum == rule.Key
                                               orderby duplicateData.PropertyName
                                               select property);
                        int checkPropCount = checkProperties.Count();

                        var errorLabels = (from sysDuplicate in sysDuplicateDetectionData
                                           join checkProperty in checkProperties.Where(w => w.Name != "CompanyGUID")
                                           on sysDuplicate.PropertyName equals checkProperty.Name
                                           select sysDuplicate.LabelName ?? checkProperty.Name);

                        errParams = string.Join(",", errorLabels);

                        #region check input list
                        if (itemsCount > 1)
                        {
                            var listCount = items.AsQueryable().GroupBy(checkProperties.GetGroupByPredicate(), "it").Select("new (it.Count() as Count)");
                            if (listCount.Any("Count > 1"))
                            {
                                ex.AddData("ERROR.DUPLICATE_LIST", new string[] { modelName, errParams });
                            }
                        }
                        #endregion
                        #region check in database
                        int take = 300;
                        int startIdx = 0;
                        int endIdx = itemsCount <= take ? itemsCount : take;
                        while (endIdx <= itemsCount)
                        {
                            List<SearchCondition> searchConditions = new List<SearchCondition>();
                            #region loop collection
                            for (int idx = startIdx; idx < endIdx; idx++)
                            {
                                var item = items.ElementAt(idx);
                                // properties to check
                                for (int i = 0; i < checkPropCount; i++)
                                {
                                    var checkProperty = checkProperties.ElementAt(i);
                                    var value = checkProperty.GetValue(item);

                                    SearchCondition searchCondition = new SearchCondition
                                    {
                                        ColumnName = checkProperty.Name,
                                        Operator = (idx == startIdx && i == 0) ? Operators.AND : i == 0 ? Operators.OR : Operators.AND,
                                        Type = checkProperty.PropertyType.Name,
                                        Value = value != null ? value.ToString() : "",
                                        Bracket = (idx == startIdx && i == 0) ? (int)BracketType.DoubleStart :
                                                i == 0 ? (int)BracketType.SingleStart : (int)BracketType.None
                                    };
                                    searchConditions.Add(searchCondition);
                                }
                                // pk
                                for (int i = 0; i < pkCount; i++)
                                {
                                    var key = primaryKey.ElementAt(i);
                                    var value = key.GetValue(item);
                                    if (value != null)
                                    {
                                        SearchCondition searchCondition = new SearchCondition
                                        {
                                            ColumnName = key.Name,
                                            Operator = Operators.AND,
                                            Type = key.PropertyType.Name,
                                            Value = value.ToString(),
                                            EqualityOperator = Operators.NOT_EQUAL,
                                            Bracket = (idx == endIdx - 1 && i == pkCount - 1) ? (int)BracketType.DoubleEnd :
                                                    (i == pkCount - 1) ? (int)BracketType.SingleEnd : (int)BracketType.None
                                        };
                                        searchConditions.Add(searchCondition);
                                    }
                                }
                            }
                            #endregion
                            SearchParameter search = new SearchParameter
                            {
                                Conditions = searchConditions
                            };
                            var predicate = search.GetSearchPredicate(modelType, true);

                            if (Entity.Where(predicate.Predicates, predicate.Values).Count() > 0)
                            {
                                ex.AddData("ERROR.DUPLICATE", errParams);
                                break;
                            }

                            startIdx += take;
                            endIdx += take;
                        }

                        #endregion
                    }
                }

                if (ex.MessageList.Count > 1)
                    throw SmartAppUtil.AddStackTrace(ex);
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(SmartAppException))
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                else
                {
                    var ex = new SmartAppException(e.Message, SmartAppUtil.AddStackTrace(e));
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
        }
        public virtual void CheckAccessLevel(TEntity item)
        {
            try
            {
                if (HasCompany && (HasOwner || HasOwnerBusinessUnitGUID))
                {
                    SysAccessLevelHelper.CheckOwnerBUByAccessLevel(item, SysParm.AccessLevel, SysParm.UserName);
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public virtual void CheckAccessLevel(IEnumerable<TEntity> items)
        {
            try
            {
                if (HasCompany && (HasOwner || HasOwnerBusinessUnitGUID))
                {
                    SysAccessLevelHelper.CheckOwnerBUByAccessLevel(items.ToList(), SysParm.AccessLevel, SysParm.UserName);
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsListViewGetAllRows(int rows)
        {
            try
            {
                return rows == (int)SearchingType.avoid;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Get filter by AccessLevel
        public SearchCondition GetFilterLevelCondition()
        {
            try
            {
                return SysAccessLevelHelper.GetFilterCondByAccessLevel(SysParm.AccessLevel);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SearchPredicate GetFilterLevelPredicate(SearchCondition accessLevelCond, List<SearchCondition> inputConds = null)
        {
            try
            {
                SearchPredicate result = new SearchPredicate();
                List<SearchCondition> conds = new List<SearchCondition>();
                if (inputConds != null && inputConds.Count() != 0)
                {
                    conds.AddRange(inputConds);
                }

                if (accessLevelCond != null)
                {
                    conds.Add(accessLevelCond);
                }

                if (conds.Count() == 0)
                {
                    result.Predicates = TextConstants.PredicateAll;
                    return result;
                }
                else
                {
                    SearchParameter search = new SearchParameter
                    {
                        Conditions = conds
                    };
                    result = search.GetSearchPredicate(typeof(TEntity).GetType(), true);
                    return result;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public SearchPredicate GetFilterLevelPredicate(List<SearchCondition> inputConds = null)
        {
            try
            {
                return GetFilterLevelPredicate(
                        SysAccessLevelHelper.GetFilterCondByAccessLevel(SysParm.AccessLevel),
                        inputConds);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SearchPredicate GetFilterLevelPredicate<T>(SearchParameter search) where T : BaseEntity
        {
            try
            {
                if (search != null)
                {
                    return search.GetPredicate_entity<T>();
                }
                else
                {
                    var result = new SearchPredicate();
                    result.Predicates = TextConstants.PredicateNot;
                    return result;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SearchPredicate GetFilterLevelPredicate<T>(SearchParameter search, string companyGUID) where T : CompanyBaseEntity
        {
            try
            {
                if (search != null)
                {
                    return search.GetPredicate_entity<T>(GetFilterLevelCondition(), companyGUID);
                }
                else
                {
                    var result = new SearchPredicate();
                    result.Predicates = TextConstants.PredicateNot;
                    return result;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        
        public SearchPredicate GetByIdvwFilterLevelPredicate(Guid id)
        {
            try
            {
                var modelType = typeof(TEntity);
                var instance = Activator.CreateInstance(modelType);
                var primaryProp = db.Entry(instance).Metadata.FindPrimaryKey().Properties.FirstOrDefault().PropertyInfo;
                List<SearchCondition> conds = new List<SearchCondition>()
                {
                    new SearchCondition
                    {
                        ColumnName = primaryProp.Name,
                        Operator = Operators.AND,
                        Type = primaryProp.PropertyType.Name,
                        Value = id.GuidNullToString()
                    }
                };
                if (HasCompany && (HasOwner && HasOwnerBusinessUnitGUID))
                {
                    return GetFilterLevelPredicate(conds);

                }
                else
                {
                    SearchParameter search = new SearchParameter { Conditions = conds };
                    return search.GetSearchPredicate(modelType, true);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion Get filter by AccessLevel


        #region Asynchronous
        public async Task<TEntity> FindAsync(params object[] keyValues) {
            return await Task.Run(() => this.Find(keyValues));
        }

        public async Task<bool> ExistAsync(params object[] keyValues) {
            return await Task.Run(() => this.Exist(keyValues));
        }

        public async Task<bool> ExistAsync(Expression<Func<TEntity, bool>> predicate) {
            return await Task.Run(() => this.Exist(predicate));
        }

        public async Task<IEnumerable<TEntity>> GetListAsync() {
            return await Task.Run(() => this.GetList());
        }

        public async Task<IEnumerable<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> predicate) {
            return await Task.Run(() => this.GetList(predicate));
        }

        public async Task AddAsync(TEntity item) {
            await Task.Run(() => this.Add(item));
        }

        public async Task AddAsync(IEnumerable<TEntity> items) {
            await Task.Run(() => this.Add(items));
        }

        public async Task UpdateAsync(TEntity item) {
            await Task.Run(() => this.Update(item));
        }

        public async Task UpdateAsync(IEnumerable<TEntity> items) {
            await Task.Run(() => this.Update(items));
        }

        public async Task RemoveAsync(TEntity item) {
            await Task.Run(() => this.Remove(item));
        }

        public async Task RemoveAsync(IEnumerable<TEntity> items) {
            await Task.Run(() => this.Remove(items));
        }

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing) {
            if (!disposedValue) {
                if (disposing) {
                    // TODO: dispose managed state (managed objects).

                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~BaseRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose() {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }


        #endregion
    }
}
