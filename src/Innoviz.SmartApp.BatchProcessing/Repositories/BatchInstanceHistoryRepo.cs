﻿using Innoviz.SmartApp.BatchProcessing;
using Innoviz.SmartApp.BatchProcessing.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.BatchProcessing.ViewModelHandler.Models;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using static Innoviz.SmartApp.Data.BatchProcessing.BatchEnum;

namespace Innoviz.SmartApp.WebApi.BatchProcessing.BatchRepo
{

    public interface IBatchInstanceHistoryRepo {
        BatchInstanceHistory Find(params object[] args);
        BatchInstanceHistory CreateInstanceHistory(BatchInstanceHistory item);
        void Update(BatchInstanceHistory item);
        void Update(IEnumerable<BatchInstanceHistory> list);

        SearchResult<BatchHistoryView> GetListvw(SearchParameter search);

        BatchHistoryView GetByIdvw(string id);

        bool AnyRunningOrFailedInstances(string jobId);

        IEnumerable<BatchInstanceHistory> UpdateRunningInstancesServerRestarted();

        IEnumerable<BatchInstanceHistory> GetAllRunningInstancesBeforeRestarted(DateTime startedTime);
        BatchInstanceObject GetBatchInstanceObjectFromInstanceId(string instanceId);
    }
    public class BatchInstanceHistoryRepo : QuartzBaseRepo<BatchInstanceHistory>, IBatchInstanceHistoryRepo 
    {
        
        public BatchInstanceHistoryRepo(QuartzDbContext context) : base(context) {

        }
        public override void ValidateAdd(BatchInstanceHistory item) {

        }
        public override void ValidateUpdate(BatchInstanceHistory item) {

        }
        public override void ValidateRemove(BatchInstanceHistory item) {

        }
        private IQueryable<BatchHistoryViewMap> GetQuery()
        {
            try
            {
                var result = (from batchInstanceHistory in Entity
                              select new BatchHistoryViewMap
                              {
                                  InstanceHistoryId = batchInstanceHistory.InstanceHistoryId,
                                  JobId = batchInstanceHistory.JobId,
                                  TriggerId = batchInstanceHistory.TriggerId,
                                  ControllerUrl = batchInstanceHistory.ControllerUrl,
                                  ParamValues = batchInstanceHistory.ParamValues,
                                  InstanceState = batchInstanceHistory.InstanceState,
                                  ScheduledDateTime = batchInstanceHistory.ScheduledTime,
                                  ActualStartDateTime = batchInstanceHistory.ActualStartTime,
                                  FinishedDateTime = batchInstanceHistory.FinishedDateTime,

                                  CreatedBy = batchInstanceHistory.CreatedBy,
                                  CreatedDateTime = batchInstanceHistory.CreatedDateTime,
                                  ModifiedBy = batchInstanceHistory.ModifiedBy,
                                  ModifiedDateTime = batchInstanceHistory.ModifiedDateTime

                              });
                return result;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private IQueryable<BatchHistoryViewMap> GetListBatchHistoryViewQuery()
        {
            try
            {
                int failedStatus = (int)BatchResultStatus.Fail;

                var instLogQ1 = (from instLog in db.Set<BatchInstanceLog>()
                                 group instLog by
                                 new
                                 {
                                     instLog.InstanceHistoryId,
                                     instLog.ResultStatus
                                 } into g
                                 select new 
                                 {
                                     InstanceHistoryId = g.Key.InstanceHistoryId.Value,
                                     ResultStatus = g.Key.ResultStatus,
                                     LogCount = g.Count()
                                 })
                                 .Where(item => item.InstanceHistoryId != null &&
                                                item.ResultStatus == failedStatus &&
                                                item.LogCount > 0);

                var instLogQ2 = (from hist in Entity
                                 join instLog in instLogQ1.DefaultIfEmpty()
                                 on hist.InstanceHistoryId equals instLog.InstanceHistoryId
                                 select hist);
                                 
                var result = (from batchInstanceHistory in Entity
                              join failedLogStatus in instLogQ2
                              on batchInstanceHistory.InstanceHistoryId equals failedLogStatus.InstanceHistoryId into lj1

                              from failedLogStatus in lj1.DefaultIfEmpty()

                              select new BatchHistoryViewMap
                              {
                                  InstanceHistoryId = batchInstanceHistory.InstanceHistoryId,
                                  JobId = batchInstanceHistory.JobId,
                                  TriggerId = batchInstanceHistory.TriggerId,
                                  ControllerUrl = batchInstanceHistory.ControllerUrl,
                                  ParamValues = batchInstanceHistory.ParamValues,
                                  InstanceState = batchInstanceHistory.InstanceState,
                                  ScheduledDateTime = batchInstanceHistory.ScheduledTime,
                                  ActualStartDateTime = batchInstanceHistory.ActualStartTime,
                                  FinishedDateTime = batchInstanceHistory.FinishedDateTime,

                                  AnyFailedLogStatus = failedLogStatus != null ? true : false,

                                  CreatedBy = batchInstanceHistory.CreatedBy,
                                  CreatedDateTime = batchInstanceHistory.CreatedDateTime,
                                  ModifiedBy = batchInstanceHistory.ModifiedBy,
                                  ModifiedDateTime = batchInstanceHistory.ModifiedDateTime
                              });
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public SearchResult<BatchHistoryView> GetListvw(SearchParameter search)
        {
            try
            {
                var result = new SearchResult<BatchHistoryView>();
                //var predicate = search.GetPredicate_entity<BatchInstanceHistory>();
                var predicate = search.GetSearchPredicate(typeof(BatchInstanceHistory));
                var total = GetListBatchHistoryViewQuery()
                            .Where(predicate.Predicates, predicate.Values)
                            .AsNoTracking()
                            .Count();

                var list = GetListBatchHistoryViewQuery()
                            .Where(predicate.Predicates, predicate.Values)
                            .OrderBy(predicate.Sorting)
                            .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                            .Take(search.Paginator.Rows.GetPaginatorRows(total))
                            .AsNoTracking()
                            .ToMaps<BatchHistoryViewMap, BatchHistoryView>();
                
                result = list.SetSearchResult<BatchHistoryView>(total, search);
                return result;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public BatchHistoryView GetByIdvw(string id) {
            try {
                Guid historyId = new Guid(id);
                var result = GetQuery()
                                .Where(item => item.InstanceHistoryId == historyId)
                                .AsNoTracking()
                                .FirstOrDefault()
                                .ToMap<BatchHistoryViewMap, BatchHistoryView>();
                return result;
            }
            catch (Exception ex) {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        
        public BatchInstanceHistory CreateInstanceHistory(BatchInstanceHistory item) {

            try {
                if(item.InstanceHistoryId == Guid.Empty)
                {
                    Guid instanceId = Guid.NewGuid();
                    item.InstanceHistoryId = instanceId;
                }
                base.Add(item);
                return item;
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<BatchInstanceHistory> GetAllRunningInstancesBeforeRestarted(DateTime startedTime) {
            try {
                int running = Convert.ToInt32(BatchInstanceState.Executing);
                int retrying = Convert.ToInt32(BatchInstanceState.Retrying);

                var list = (from instance in Entity.Where(item =>
                                        (item.InstanceState == running ||
                                        item.InstanceState == retrying) &&
                                        item.CreatedDateTime < startedTime)
                            join batchJob in db.Set<BatchJob>()
                            on instance.JobId equals batchJob.JobId into lj1

                            from batchJob in lj1
                            join batchTrigger in db.Set<BatchTrigger>()
                            on instance.TriggerId equals batchTrigger.TriggerId into lj2

                            from batchTrigger in lj2.DefaultIfEmpty()

                            select new BatchInstanceHistory
                            {
                                InstanceHistoryId = instance.InstanceHistoryId,
                                JobId = instance.JobId,
                                TriggerId = instance.TriggerId,
                                ControllerUrl = instance.ControllerUrl,
                                ParamValues = instance.ParamValues,
                                ActualStartTime = instance.ActualStartTime,
                                InstanceState = instance.InstanceState,
                                FinishedDateTime = instance.FinishedDateTime,
                                ScheduledTime = instance.ScheduledTime,
                                CreatedBy = instance.CreatedBy,
                                ModifiedBy = instance.ModifiedBy,
                                CreatedDateTime = instance.CreatedDateTime,
                                ModifiedDateTime = instance.ModifiedDateTime,

                                Job = batchJob,
                                Trigger = batchTrigger

                            }).ToList();
                return list;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<BatchInstanceHistory> UpdateRunningInstancesServerRestarted() {
            try {
                int running = Convert.ToInt32(BatchInstanceState.Executing);
                int retrying = Convert.ToInt32(BatchInstanceState.Retrying);

                IEnumerable<BatchInstanceHistory> runningInstances =
                    Entity.Where(instance =>
                                    (instance.InstanceState == running ||
                                        instance.InstanceState == retrying)).ToList();
                foreach (var item in runningInstances) {
                    if(item.InstanceState == retrying) {
                        item.InstanceState = Convert.ToInt32(BatchInstanceState.RetryFailedServerRestarted);
                    }
                    else {
                        item.InstanceState = Convert.ToInt32(BatchInstanceState.FailedServerRestarted);
                    }
                }
                return runningInstances;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        public BatchInstanceObject GetBatchInstanceObjectFromInstanceId(string instanceId) {
            try {
                Guid guid = new Guid(instanceId);
                var result = (from instance in Entity.Where(item => item.InstanceHistoryId == guid)
                              join job in db.Set<BatchJob>()
                              on instance.JobId equals job.JobId into lj1

                              from job in lj1
                              join trigger in db.Set<BatchTrigger>()
                              on job.JobId equals trigger.JobId into lj2

                              from trigger in lj2

                              select new BatchInstanceObject
                              {
                                  BatchInstanceHistory = new BatchInstanceHistory()
                                  {
                                      InstanceHistoryId = instance.InstanceHistoryId,
                                      JobId = instance.JobId,
                                      TriggerId = instance.TriggerId,
                                      ControllerUrl = instance.ControllerUrl,
                                      ParamValues = instance.ParamValues,
                                      ActualStartTime = instance.ActualStartTime,
                                      InstanceState = instance.InstanceState,
                                      FinishedDateTime = instance.FinishedDateTime,
                                      ScheduledTime = instance.ScheduledTime,
                                      CreatedBy = instance.CreatedBy,
                                      ModifiedBy = instance.ModifiedBy,
                                      CreatedDateTime = instance.CreatedDateTime,
                                      ModifiedDateTime = instance.ModifiedDateTime
                                  },
                                  BatchJob = new BatchJob()
                                  {
                                      JobId = job.JobId,
                                      GroupName = job.GroupName,
                                      ControllerUrl = job.ControllerUrl,
                                      JobStatus = job.JobStatus,
                                      JobData = job.JobData,
                                      CompanyGUID = job.CompanyGUID,
                                      Description = job.Description,
                                      CreatedBy = job.CreatedBy,
                                      CreatedDateTime = job.CreatedDateTime,
                                      ModifiedBy = job.ModifiedBy,
                                      ModifiedDateTime = job.ModifiedDateTime
                                  },
                                  BatchTrigger = new BatchTrigger()
                                  {
                                      CompanyGUID = trigger.CompanyGUID,
                                      CRONExpression = trigger.CRONExpression,
                                      EndDateTime = trigger.EndDateTime,
                                      IntervalCount = trigger.IntervalCount,
                                      IntervalType = trigger.IntervalType,
                                      IsFinalized = trigger.IsFinalized,
                                      JobId = trigger.JobId,
                                      LastExecuted = trigger.LastExecuted,
                                      NextScheduledTime = trigger.NextScheduledTime,
                                      RecurringCount = trigger.RecurringCount,
                                      StartDateTime = trigger.StartDateTime,
                                      TriggerId = trigger.TriggerId,

                                      CreatedBy = trigger.CreatedBy,
                                      CreatedDateTime = trigger.CreatedDateTime,
                                      ModifiedBy = trigger.ModifiedBy,
                                      ModifiedDateTime = trigger.ModifiedDateTime
                                  }
                              }).FirstOrDefault();
                return result;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool AnyRunningOrFailedInstances(string jobId)
        {
            try
            {
                Guid jobGUID = new Guid(jobId);

                int running = Convert.ToInt32(BatchInstanceState.Executing);
                int retrying = Convert.ToInt32(BatchInstanceState.Retrying);
                int failed = Convert.ToInt32(BatchInstanceState.Failed);
                int retryFailed = Convert.ToInt32(BatchInstanceState.RetryFailed);

                IEnumerable<BatchInstanceHistory> runningOrFailedInstances =
                Entity.Where(instance =>

                                instance.JobId == jobGUID &&
                                (instance.InstanceState == running ||
                                 instance.InstanceState == retrying ||
                                 instance.InstanceState == failed || 
                                 instance.InstanceState == retryFailed
                                 ))
                                 .ToList();

                return (runningOrFailedInstances.Count() != 0);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
