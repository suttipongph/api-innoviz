﻿using Innoviz.SmartApp.BatchProcessing;
using Innoviz.SmartApp.BatchProcessing.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.BatchProcessing.ViewModelHandler.Models;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel;
using Innoviz.SmartApp.WebApi.BatchProcessing.ViewModelHandler;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace Innoviz.SmartApp.WebApi.BatchProcessing.BatchRepo
{
    public interface IBatchJobRepo {

        SearchResult<BatchView> GetListvw(SearchParameter search, string companyGUID);

        BatchJob Find(params object[] args);
        void Add(BatchJob item);
        void Update(BatchJob item);
        void Remove(BatchJob item);

        void Update(IEnumerable<BatchJob> items);

        BatchView GetBatchViewByJobId(string jobId);
        BatchObject GetBatchObjectByJobId(string jobId);

        int? GetJobStatus(Guid? jobId);

        bool GetTriggerFinalizedByJobId(string jobId);
    }
    public class BatchJobRepo: QuartzBaseRepo<BatchJob>, IBatchJobRepo
    {
        public BatchJobRepo(QuartzDbContext context) : base(context) {

        }
        public override void ValidateAdd(BatchJob item) {

        }
        public override void ValidateUpdate(BatchJob item) {

        }
        public override void ValidateRemove(BatchJob item) {

        }
        private IQueryable<BatchViewMap> GetBatchViewQuery()
        {
            try
            {
                var result = (from job in Entity
                              join trigger in db.Set<BatchTrigger>()
                              on job.JobId equals trigger.JobId into lj1

                              from trigger in lj1
                              select new BatchViewMap
                              {
                                  TriggerId = trigger.TriggerId,
                                  JobId = job.JobId,
                                  StartDateTime = trigger.StartDateTime,
                                  EndDateTime = trigger.EndDateTime,
                                  NextScheduledDateTime = trigger.NextScheduledTime,
                                  LastExecutedDateTime = trigger.LastExecuted,

                                  IsFinalized = trigger.IsFinalized,
                                  IntervalType = trigger.IntervalType,
                                  IntervalCount = trigger.IntervalCount,
                                  RecurringCount = trigger.RecurringCount,

                                  IsEndOfMonth = trigger.CRONExpression != null ?
                                                        trigger.CRONExpression.Contains("L") : false,

                                  ControllerUrl = job.ControllerUrl,
                                  Description = job.Description,
                                  JobStatus = job.JobStatus,
                                  ParamValues = job.JobData,

                                  CompanyGUID = trigger.CompanyGUID,

                                  CreatedBy = job.CreatedBy,
                                  CreatedDateTime = job.CreatedDateTime,
                                  ModifiedBy = job.ModifiedBy,
                                  ModifiedDateTime = job.ModifiedDateTime
                              });
                return result;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IQueryable<BatchViewMap> GetListBatchViewQuery()
        {
            try
            {
                var q1 = (from instance in db.Set<BatchInstanceHistory>()
                          group instance by instance.JobId into g
                          select new BatchInstanceHistory
                          {
                              JobId = g.Key,
                              ActualStartTime = g.Max(item => item.ActualStartTime)
                          });
                var q2 = (from instance in db.Set<BatchInstanceHistory>()
                          join inst in q1
                          on
                          new { JobId = instance.JobId, ActualStartTime = instance.ActualStartTime }
                          equals
                          new { JobId = inst.JobId, ActualStartTime = inst.ActualStartTime }
                          select instance);

                var listQuery = (from job in Entity
                                 join trigger in db.Set<BatchTrigger>()
                                 on job.JobId equals trigger.JobId into lj1

                                 from trigger in lj1
                                 join instance in q2
                                 on job.JobId equals instance.JobId into lj2

                                 from instance in lj2.DefaultIfEmpty()
                                 select new BatchViewMap
                                 {
                                     TriggerId = trigger.TriggerId,
                                     JobId = job.JobId,
                                     StartDateTime = trigger.StartDateTime,
                                     EndDateTime = trigger.EndDateTime,
                                     NextScheduledDateTime = trigger.NextScheduledTime,
                                     LastExecutedDateTime = trigger.LastExecuted,

                                     IsFinalized = trigger.IsFinalized,
                                     IntervalType = trigger.IntervalType,
                                     IntervalCount = trigger.IntervalCount,
                                     RecurringCount = trigger.RecurringCount,

                                     IsEndOfMonth = trigger.CRONExpression != null ?
                                                        trigger.CRONExpression.Contains("L") : false,

                                     ControllerUrl = job.ControllerUrl,
                                     Description = job.Description,
                                     JobStatus = job.JobStatus,

                                     LastExecutedInstanceState = (instance != null) ? instance.InstanceState : null,

                                     CompanyGUID = trigger.CompanyGUID,

                                     CreatedBy = job.CreatedBy,
                                     CreatedDateTime = job.CreatedDateTime,
                                     ModifiedBy = job.ModifiedBy,
                                     ModifiedDateTime = job.ModifiedDateTime
                                 });
                return listQuery;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SearchResult<BatchView> GetListvw(SearchParameter search, string companyGUID)
        {
            try
            {
                var result = new SearchResult<BatchView>();

                //var predicate = search.GetPredicate_entity<BatchJob>(companyGUID);
                //var predicate = GetFilterLevelPredicate<BatchViewMap>(search, companyGUID);
                var predicate = search.GetSearchPredicate(typeof(BatchViewMap));
                var total = GetListBatchViewQuery()
                                .Where(predicate.Predicates, predicate.Values)
                                .AsNoTracking()
                                .Count();

                var list = GetListBatchViewQuery()
                                .Where(predicate.Predicates, predicate.Values)
                                .OrderBy(predicate.Sorting)
                                .Skip(search.Paginator.Rows.GetPaginatorPages(search.Paginator.Page))
                                .Take(search.Paginator.Rows.GetPaginatorRows(total))
                                .AsNoTracking()
                                .ToMaps<BatchViewMap, BatchView>()
                                .GetBatchViewValidation();

                result = list.SetSearchResult<BatchView>(total, search);
                
                return result;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public BatchView GetBatchViewByJobId(string jobId) {
            try {
                Guid jobID = new Guid(jobId);
                var result = GetBatchViewQuery()
                                .Where(item=>item.JobId == jobID)
                                .AsNoTracking()
                                .FirstOrDefault()
                                .ToMap<BatchViewMap, BatchView>();
                return result;
            }
            catch (Exception ex) {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public BatchObject GetBatchObjectByJobId(string jobId) {
            try {
                Guid guid = new Guid(jobId);
                var result = (from job in Entity.Where(item=> item.JobId == guid).AsNoTracking()
                              join trigger in db.Set<BatchTrigger>().AsNoTracking() on job.JobId equals trigger.JobId
                              select new BatchObject
                              {
                                  BatchJob = new BatchJob()
                                  {
                                      CompanyGUID = job.CompanyGUID,
                                      ControllerUrl = job.ControllerUrl,
                                      Description = job.Description,
                                      GroupName = job.GroupName,
                                      JobData = job.JobData,
                                      JobId = job.JobId,
                                      JobStatus = job.JobStatus,
                                      CreatedBy = job.CreatedBy,
                                      CreatedDateTime = job.CreatedDateTime,
                                      ModifiedBy = job.ModifiedBy,
                                      ModifiedDateTime = job.ModifiedDateTime
                                  },
                                  BatchTrigger = new BatchTrigger()
                                  {
                                      CompanyGUID = trigger.CompanyGUID,
                                      CRONExpression = trigger.CRONExpression,
                                      EndDateTime = trigger.EndDateTime,
                                      IntervalCount = trigger.IntervalCount,
                                      IntervalType = trigger.IntervalType,
                                      IsFinalized = trigger.IsFinalized,
                                      JobId = trigger.JobId,
                                      LastExecuted = trigger.LastExecuted,
                                      NextScheduledTime = trigger.NextScheduledTime,
                                      RecurringCount = trigger.RecurringCount,
                                      StartDateTime = trigger.StartDateTime,
                                      TriggerId = trigger.TriggerId,

                                      CreatedBy = trigger.CreatedBy,
                                      CreatedDateTime = trigger.CreatedDateTime,
                                      ModifiedBy = trigger.ModifiedBy,
                                      ModifiedDateTime = trigger.ModifiedDateTime
                                  }
                              }).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public int? GetJobStatus(Guid? jobId) {
            try {
                var job = Entity.Where(item => item.JobId == jobId).AsNoTracking().FirstOrDefault();
                return job?.JobStatus;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool GetTriggerFinalizedByJobId(string jobId)
        {
            try
            {
                Guid jobGUID = new Guid(jobId);
                var trigger = (from job in Entity.Where(item => item.JobId == jobGUID)
                               join trig in db.Set<BatchTrigger>()
                               on job.JobId equals trig.JobId
                               select trig).FirstOrDefault();
                return trigger == null ? true : trigger.IsFinalized;
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private SearchPredicate GetPredicateBatchViewMap(SearchParameter search, string companyGUID)
        {
            try
            {
                return null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
