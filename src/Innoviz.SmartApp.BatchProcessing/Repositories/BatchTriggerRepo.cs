﻿using Innoviz.SmartApp.BatchProcessing;
using Innoviz.SmartApp.BatchProcessing.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using static Innoviz.SmartApp.Data.BatchProcessing.BatchEnum;

namespace Innoviz.SmartApp.WebApi.BatchProcessing.BatchRepo
{
    public interface IBatchTriggerRepo {
        BatchTrigger Find(params object[] args);
        void Add(BatchTrigger item);
        void Update(BatchTrigger item);
        void Remove(BatchTrigger item);

        void Update(IEnumerable<BatchTrigger> items);

        BatchTrigger GetBatchTriggerByJobId(string jobId);
        BatchObject GetBatchObjectFromBatchView(BatchView model);

        BatchObject GetBatchObjectByTriggerId(string triggerId);

        IEnumerable<BatchObject> GetTriggersByWaitingJobBeforeRestarted(DateTime startedTime);
    }

    public class BatchTriggerRepo: QuartzBaseRepo<BatchTrigger>, IBatchTriggerRepo
    {
        public BatchTriggerRepo(QuartzDbContext context) : base(context) {

        }
        public override void ValidateAdd(BatchTrigger item) {

        }
        public override void ValidateUpdate(BatchTrigger item) {

        }
        public override void ValidateRemove(BatchTrigger item) {

        }

        
        public bool IsTriggerFinalized(Guid triggerId) {
            try {
                BatchTrigger trig = base.Find(triggerId);
                return trig == null ? true: trig.IsFinalized;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchTrigger GetBatchTriggerByJobId(string jobId) {
            try {
                Guid jobid = new Guid(jobId);
                var item = Entity.Where(t => t.JobId == jobid).FirstOrDefault();
                return item;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchObject GetBatchObjectFromBatchView(BatchView model) {
            try {
                Guid? triggerId = model.TriggerId.StringToGuidNull();
                var result = (from trigger in Entity.Where(item => item.TriggerId == triggerId)
                              join job in db.Set<BatchJob>() on trigger.JobId equals job.JobId
                              select new BatchObject
                              {
                                  BatchJob = new BatchJob()
                                  {
                                      CompanyGUID = job.CompanyGUID,
                                      ControllerUrl = job.ControllerUrl,
                                      Description = job.Description,
                                      GroupName = job.GroupName,
                                      JobData = job.JobData,
                                      JobId = job.JobId,
                                      JobStatus = job.JobStatus,
                                      CreatedBy = job.CreatedBy,
                                      CreatedDateTime = job.CreatedDateTime,
                                      ModifiedBy = job.ModifiedBy,
                                      ModifiedDateTime = job.ModifiedDateTime
                                  },
                                  BatchTrigger = new BatchTrigger()
                                  {
                                      CompanyGUID = trigger.CompanyGUID,
                                      CRONExpression = trigger.CRONExpression,
                                      EndDateTime = trigger.EndDateTime,
                                      IntervalCount = trigger.IntervalCount,
                                      IntervalType = trigger.IntervalType,
                                      IsFinalized = trigger.IsFinalized,
                                      JobId = trigger.JobId,
                                      LastExecuted = trigger.LastExecuted,
                                      NextScheduledTime = trigger.NextScheduledTime,
                                      RecurringCount = trigger.RecurringCount,
                                      StartDateTime = trigger.StartDateTime,
                                      TriggerId = trigger.TriggerId,

                                      CreatedBy = trigger.CreatedBy,
                                      CreatedDateTime = trigger.CreatedDateTime,
                                      ModifiedBy = trigger.ModifiedBy,
                                      ModifiedDateTime = trigger.ModifiedDateTime
                                  }
                              }).FirstOrDefault();
                return result;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchObject GetBatchObjectByTriggerId(string triggerId) {
            try {
                Guid guid = new Guid(triggerId);
                var result = (from trigger in Entity.Where(item => item.TriggerId == guid).AsNoTracking()
                              join job in db.Set<BatchJob>().AsNoTracking() on trigger.JobId equals job.JobId
                              select new BatchObject
                              {
                                  BatchJob = new BatchJob()
                                  {
                                      CompanyGUID = job.CompanyGUID,
                                      ControllerUrl = job.ControllerUrl,
                                      Description = job.Description,
                                      GroupName = job.GroupName,
                                      JobData = job.JobData,
                                      JobId = job.JobId,
                                      JobStatus = job.JobStatus,
                                      CreatedBy = job.CreatedBy,
                                      CreatedDateTime = job.CreatedDateTime,
                                      ModifiedBy = job.ModifiedBy,
                                      ModifiedDateTime = job.ModifiedDateTime
                                  },
                                  BatchTrigger = new BatchTrigger()
                                  {
                                      CompanyGUID = trigger.CompanyGUID,
                                      CRONExpression = trigger.CRONExpression,
                                      EndDateTime = trigger.EndDateTime,
                                      IntervalCount = trigger.IntervalCount,
                                      IntervalType = trigger.IntervalType,
                                      IsFinalized = trigger.IsFinalized,
                                      JobId = trigger.JobId,
                                      LastExecuted = trigger.LastExecuted,
                                      NextScheduledTime = trigger.NextScheduledTime,
                                      RecurringCount = trigger.RecurringCount,
                                      StartDateTime = trigger.StartDateTime,
                                      TriggerId = trigger.TriggerId,

                                      CreatedBy = trigger.CreatedBy,
                                      CreatedDateTime = trigger.CreatedDateTime,
                                      ModifiedBy = trigger.ModifiedBy,
                                      ModifiedDateTime = trigger.ModifiedDateTime
                                  }
                              }).AsNoTracking().FirstOrDefault();
                return result;
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<BatchObject> GetTriggersByWaitingJobBeforeRestarted(DateTime startedTime) {
            try {
                int waiting = Convert.ToInt32(BatchJobStatus.Waiting);
                var result = (from trigger in Entity
                              join job in db.Set<BatchJob>().Where(item => item.ModifiedDateTime < startedTime &&
                                                                    item.JobStatus == waiting)
                              on trigger.JobId equals job.JobId

                              select new BatchObject
                              {
                                  BatchJob = new BatchJob()
                                  {
                                      CompanyGUID = job.CompanyGUID,
                                      ControllerUrl = job.ControllerUrl,
                                      Description = job.Description,
                                      GroupName = job.GroupName,
                                      JobData = job.JobData,
                                      JobId = job.JobId,
                                      JobStatus = job.JobStatus,
                                      CreatedBy = job.CreatedBy,
                                      CreatedDateTime = job.CreatedDateTime,
                                      ModifiedBy = job.ModifiedBy,
                                      ModifiedDateTime = job.ModifiedDateTime
                                  },
                                  BatchTrigger = new BatchTrigger
                                  {
                                      CompanyGUID = trigger.CompanyGUID,
                                      CRONExpression = trigger.CRONExpression,
                                      EndDateTime = trigger.EndDateTime,
                                      IntervalCount = trigger.IntervalCount,
                                      IntervalType = trigger.IntervalType,
                                      IsFinalized = trigger.IsFinalized,
                                      JobId = trigger.JobId,
                                      LastExecuted = trigger.LastExecuted,
                                      NextScheduledTime = trigger.NextScheduledTime,
                                      RecurringCount = trigger.RecurringCount,
                                      StartDateTime = trigger.StartDateTime,
                                      TriggerId = trigger.TriggerId,

                                      CreatedBy = trigger.CreatedBy,
                                      CreatedDateTime = trigger.CreatedDateTime,
                                      ModifiedBy = trigger.ModifiedBy,
                                      ModifiedDateTime = trigger.ModifiedDateTime
                                  }
                              });
                return result;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
