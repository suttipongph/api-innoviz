﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.WebApi.BatchProcessing;
using Innoviz.SmartApp.WebApi.BatchProcessing.BatchRepo;
using Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel;
using Innoviz.SmartApp.WebApi.BatchProcessing.ViewModelHandler;
using Quartz;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.BatchProcessing.BatchEnum;

namespace Innoviz.SmartApp.BatchProcessing.Service
{
    public interface IQuartzBatchService {

        SearchResult<BatchView> GetBatchViewList(SearchParameter search, string companyGUID);
        SearchResult<BatchHistoryView> GetBatchHistoryViewList(SearchParameter search);
        SearchResult<BatchInstanceLogView> GetBatchInstanceLogViewList(SearchParameter search);

        BatchView GetQuartzBatchViewItem(string jobId);
        BatchHistoryView GetQuartzBatchHistoryViewItem(string instanceId);
        BatchInstanceLogView GetQuartzBatchInstanceLogViewItem(int id);

        BatchObject CreateJobAndTriggerFromBatchObjectView(BatchObjectView model, 
            string companyGUID, string username);
        BatchObject CreateJobAndTrigger(BatchObject batchObject, string companyGUID, string username);
        BatchInstanceObject CreateInstanceHistoryUpdateJobTrigger(BatchInstanceObject insObj, string username);
        BatchInstanceObject CreateJobInstance(BatchInstanceObject instObject,
                                                string username);

        BatchInstanceObject UpdateJobAndInstanceHistory(BatchInstanceObject instObj, string username);
        BatchInstanceHistory UpdateInstanceHistory(BatchInstanceHistory instance, string username);
        BatchTrigger UpdateTrigger(BatchTrigger trigger, string username);

        BatchTrigger UpdateNextScheduledTime(BatchTrigger batchTrigger, string username);
        BatchObject UpateTriggerFinalized(BatchTrigger batchTrigger);
        BatchObject UpdateJobAndTrigger(BatchObject batchObject, string username);

        bool IsJobExecuting(BatchTrigger model);

        BatchObject CancelJob(BatchObject model, string username);
        BatchObject HoldJob(BatchObject model, string username);
        BatchObject ReleaseJob(BatchObject model, string username);
        BatchInstanceObject RetryJobInstance(BatchInstanceObject model, string username);
        BatchInstanceObject CancelInstance(BatchInstanceObject model, string username);
        bool CleanUpBatchHistory(CleanUpBatchHistoryView model);

        bool DeleteBatchJob(BatchView model);

        BatchObject PrepareJobAndTriggerFromBatchView(BatchView model);
        bool RemoveJobAndTrigger(BatchObject model);
        BatchJob UpdateJob(BatchJob model, string username);

        BatchInstanceObject GetBatchInstanceObjectFromInstanceId(BatchHistoryView model);

        Task<bool> UpdateBatchHistoryBatchJobAtSchedulerStarted(DateTime startedTime, string dateTimeFormant, IScheduler scheduler);
        bool UpdateJobsTriggersUpdateInstancesAddLogs(IEnumerable<BatchJob> jobList, IEnumerable<BatchTrigger> triggerList, IEnumerable<BatchInstanceHistory> historyList, IEnumerable<BatchInstanceLog> logList);

        bool ValidateRetryInstanceHistory(BatchHistoryView model);
        BatchInstanceObject UpdateInstanceState(BatchInstanceObject instObject, string username);
    }
    public class QuartzBatchService: IQuartzBatchService
    {
        private QuartzDbContext db;
        private UnitOfWork UnitOfWork;

        private BuilderHelper Helper;

        public QuartzBatchService(QuartzDbContext context) {
            db = context;
            UnitOfWork = new UnitOfWork(db);
            Helper = new BuilderHelper();
        }
        public QuartzBatchService() { Helper = new BuilderHelper(); }

        public SearchResult<BatchView> GetBatchViewList(SearchParameter search, string companyGUID)
        {
            try
            {
                IBatchJobRepo jobRepo = new BatchJobRepo(db);
                return jobRepo.GetListvw(search, companyGUID);
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SearchResult<BatchHistoryView> GetBatchHistoryViewList(SearchParameter search)
        {
            try
            {
                IBatchInstanceHistoryRepo historyRepo = new BatchInstanceHistoryRepo(db);
                return historyRepo.GetListvw(search);
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SearchResult<BatchInstanceLogView> GetBatchInstanceLogViewList(SearchParameter search)
        {
            try
            {
                IBatchInstanceLogRepo instanceLogRepo = new BatchInstanceLogRepo(db);
                return instanceLogRepo.GetListvw(search);
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public BatchView GetQuartzBatchViewItem(string jobId) {
            try {
                IBatchJobRepo jobRepo = new BatchJobRepo(db);
                return jobRepo.GetBatchViewByJobId(jobId);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchHistoryView GetQuartzBatchHistoryViewItem(string instanceId) {
            try {
                IBatchInstanceHistoryRepo historyRepo = new BatchInstanceHistoryRepo(db);
                return historyRepo.GetByIdvw(instanceId);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchInstanceLogView GetQuartzBatchInstanceLogViewItem(int id) {
            try {
                IBatchInstanceLogRepo instanceLogRepo = new BatchInstanceLogRepo(db);
                return instanceLogRepo.GetByIdvw(id);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        public BatchObject CreateJobAndTrigger(BatchObject batchObject, string companyGUID, string username) {
            try {
                if (db != null) {
                    Guid company = new Guid(companyGUID);
                    BatchJob job = batchObject.BatchJob;
                    job.CompanyGUID = company;
                    job.CreatedBy = username;
                    job.ModifiedBy = username;

                    BatchTrigger trigger = batchObject.BatchTrigger;
                    trigger.CompanyGUID = company;
                    trigger.CreatedBy = username;
                    trigger.ModifiedBy = username;

                    IBatchJobRepo batchJobRepo = new BatchJobRepo(db);
                    IBatchTriggerRepo batchTriggerRepo = new BatchTriggerRepo(db);

                    batchJobRepo.Add(job);
                    batchTriggerRepo.Add(trigger);
                    UnitOfWork.Commit();
                }

                return batchObject;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchInstanceObject CreateInstanceHistoryUpdateJobTrigger(BatchInstanceObject instObj, string username) {
            try {
                BatchJob job = instObj.BatchJob;
                BatchTrigger trigger = instObj.BatchTrigger;
                BatchInstanceHistory instance = instObj.BatchInstanceHistory;

                if (db != null) {
                    
                    job.ModifiedBy = username;
                    trigger.ModifiedBy = username;

                    instance.CreatedBy = username;
                    instance.ModifiedBy = username;

                    IBatchInstanceHistoryRepo historyRepo = new BatchInstanceHistoryRepo(db);
                    IBatchJobRepo batchJobRepo = new BatchJobRepo(db);
                    IBatchTriggerRepo batchTriggerRepo = new BatchTriggerRepo(db);

                    historyRepo.CreateInstanceHistory(instance);
                    
                    batchTriggerRepo.Update(trigger);
                    batchJobRepo.Update(job);
                    UnitOfWork.Commit();
                }

                instObj.BatchInstanceHistory = instance;
                instObj.BatchJob = job;
                instObj.BatchTrigger = trigger;

                return instObj;

            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchInstanceObject UpdateJobAndInstanceHistory(BatchInstanceObject instObj, string username) {
            try {
                if(db != null) {
                   
                    IBatchInstanceHistoryRepo historyRepo = new BatchInstanceHistoryRepo(db);
                    IBatchJobRepo jobRepo = new BatchJobRepo(db);
                    instObj.BatchInstanceHistory.ModifiedBy = username;
                    instObj.BatchJob.ModifiedBy = username;

                    jobRepo.Update(instObj.BatchJob);
                    historyRepo.Update(instObj.BatchInstanceHistory);
                    UnitOfWork.Commit();
                }
                return instObj;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchInstanceHistory UpdateInstanceHistory(BatchInstanceHistory instance, string username) {
            try {
                if(db != null) {
                    IBatchInstanceHistoryRepo historyRepo = new BatchInstanceHistoryRepo(db);
                    instance.ModifiedBy = username;
                    historyRepo.Update(instance);
                    UnitOfWork.Commit();
                }
                return instance;
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchObject PrepareJobAndTriggerForCreate(BatchObjectView model) {
            try {
                BatchObject inputModel = model.ToBatchObject();

                Guid jobId = Guid.NewGuid();
                Guid triggerId = Guid.NewGuid();

                BatchJob job = inputModel.BatchJob;
                BatchTrigger trigger = inputModel.BatchTrigger;

                job.JobId = jobId;
                trigger.TriggerId = triggerId;
                trigger.JobId = jobId;

                job.JobStatus = Convert.ToInt32(BatchJobStatus.Waiting);

                if (trigger.RecurringCount == -1 || trigger.RecurringCount == null) {
                    trigger.CRONExpression = Helper.BuildCronExpression(trigger, model.IsEndOfMonth);
                }

                return inputModel;
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchObject CreateJobAndTriggerFromBatchObjectView(BatchObjectView model, 
                                                    string companyGUID, string username) {
            try {
                model.CompanyGUID = companyGUID;
                BatchObject batchObject = PrepareJobAndTriggerForCreate(model);
                return CreateJobAndTrigger(batchObject, companyGUID, username);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchInstanceObject CreateJobInstance(BatchInstanceObject instObject,
                                                string username) {
            try {
                BatchInstanceHistory instanceHistory = instObject.BatchInstanceHistory;
                BatchJob inputBatchJob = instObject.BatchJob;
                BatchTrigger inputbBatchTrigger = instObject.BatchTrigger;

                IBatchJobRepo jobRepo = new BatchJobRepo(db);
                BatchObject batchObject = jobRepo.GetBatchObjectByJobId(inputBatchJob.JobId.GuidNullToString());
                BatchJob job = batchObject.BatchJob;
                BatchTrigger trigger = batchObject.BatchTrigger;

                job.JobStatus = inputBatchJob.JobStatus;
                trigger.NextScheduledTime = inputbBatchTrigger.NextScheduledTime;
                trigger.LastExecuted = inputbBatchTrigger.LastExecuted;

                instObject.BatchJob = job;
                instObject.BatchTrigger = trigger;

                instObject = 
                    CreateInstanceHistoryUpdateJobTrigger(instObject, username);
                return instObject;

            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchInstanceObject UpdateInstanceState(BatchInstanceObject instObject, string username) {
            try {
                IBatchJobRepo jobRepo = new BatchJobRepo(db);
                BatchObject batchObject = jobRepo.GetBatchObjectByJobId(instObject.BatchJob.JobId.GuidNullToString());
                return UpdateInstanceState(batchObject, instObject, username);
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchInstanceObject UpdateInstanceState(BatchObject batchObject, 
                                    BatchInstanceObject inputInstObject, string username) {
            try {
                BatchJob job = batchObject.BatchJob;
                BatchTrigger trigger = batchObject.BatchTrigger;

                BatchInstanceHistory instance = inputInstObject.BatchInstanceHistory;
                BatchJob inputJob = inputInstObject.BatchJob;

                DateTime finishedTime = DateTime.Now;

                // update instance state
                instance.FinishedDateTime = finishedTime;
                int? instanceState = Convert.ToInt32(instance.InstanceState);

                // find any running instance                
                int? jobStatus = Convert.ToInt32(job.JobStatus);
                int hold = Convert.ToInt32(BatchJobStatus.OnHold);
                int cancelled = Convert.ToInt32(BatchJobStatus.Cancelled);

                
                if (jobStatus != hold && jobStatus != cancelled) {
                    if (instanceState == Convert.ToInt32(BatchInstanceState.Completed) ||
                        instanceState == Convert.ToInt32(BatchInstanceState.RetryCompleted)) {
                        // update job status = ended or waiting
                        if(trigger.IsFinalized) {
                            inputJob.JobStatus = Convert.ToInt32(BatchJobStatus.Ended);
                        }
                        else {

                        }
                        

                    }
                    else if(instanceState == Convert.ToInt32(BatchInstanceState.Failed) ||
                        instanceState == Convert.ToInt32(BatchInstanceState.RetryFailed)) { 
                        // instanceState = Failed, RetryFailed
                    }
                    inputInstObject.BatchJob = inputJob;
                    inputInstObject.BatchInstanceHistory = instance;
                    inputInstObject = UpdateJobAndInstanceHistory(inputInstObject, username);
                }
                else {
                    inputInstObject.BatchInstanceHistory = UpdateInstanceHistory(instance, username);
                }

                return inputInstObject;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchTrigger UpdateNextScheduledTime(BatchTrigger inputTrigger, string username) {
            try {
                IBatchTriggerRepo triggerRepo = new BatchTriggerRepo(db);
                BatchTrigger trigger = triggerRepo.Find(inputTrigger.TriggerId);
                trigger.NextScheduledTime = inputTrigger.NextScheduledTime;
                return UpdateTrigger(trigger, username);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchTrigger UpdateTrigger(BatchTrigger trigger, string username) {
            try {
                if(db != null) {
                    IBatchTriggerRepo triggerRepo = new BatchTriggerRepo(db);
                    trigger.ModifiedBy = username;

                    triggerRepo.Update(trigger);

                    UnitOfWork.Commit();
                }
                return trigger;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchObject UpateTriggerFinalized(BatchTrigger model) {
            try {
                IBatchTriggerRepo triggerRepo = new BatchTriggerRepo(db);
                BatchObject batchObject = triggerRepo.GetBatchObjectByTriggerId(model.TriggerId.GuidNullToString());

                int jobStatus = Convert.ToInt32(batchObject.BatchJob.JobStatus);
                int cancelled = Convert.ToInt32(BatchJobStatus.Cancelled);

                batchObject.BatchTrigger.IsFinalized = model.IsFinalized;

                if(jobStatus == cancelled) {
                    batchObject.BatchTrigger.NextScheduledTime = null;
                    batchObject.BatchTrigger = UpdateTrigger(batchObject.BatchTrigger, model.ModifiedBy);
                }
                else {
                    if (model.IsFinalized) {
                        batchObject.BatchTrigger.NextScheduledTime = null;

                        IBatchInstanceHistoryRepo historyRepo = new BatchInstanceHistoryRepo(db);
                        string jobId = batchObject.BatchJob.JobId.GuidNullToString();

                        if (historyRepo.AnyRunningOrFailedInstances(jobId)) {
                            batchObject.BatchTrigger = UpdateTrigger(batchObject.BatchTrigger, model.ModifiedBy);
                        }
                        else {
                            batchObject.BatchJob.JobStatus = Convert.ToInt32(BatchJobStatus.Ended);
                            batchObject = UpdateJobAndTrigger(batchObject, model.ModifiedBy);
                        }
                        
                    }
                }

                return batchObject;
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchObject UpdateJobAndTrigger(BatchObject batchObject, string username) {
            try {
                if(db != null) {
                    IBatchJobRepo jobRepo = new BatchJobRepo(db);
                    IBatchTriggerRepo triggerRepo = new BatchTriggerRepo(db);
                    batchObject.BatchJob.ModifiedBy = username;
                    batchObject.BatchTrigger.ModifiedBy = username;

                    jobRepo.Update(batchObject.BatchJob);
                    triggerRepo.Update(batchObject.BatchTrigger);

                    UnitOfWork.Commit();
                }
                return batchObject;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsJobExecuting(BatchTrigger model) {
            try {
                IBatchJobRepo jobRepo = new BatchJobRepo(db);
                int? jobStatus = jobRepo.GetJobStatus(model.JobId);

                int executing = Convert.ToInt32(BatchJobStatus.Executing);
                if(jobStatus == executing) {
                    UpdateNextScheduledTime(model, model.ModifiedBy);
                    return true;
                }
                else {
                    UpdateNextScheduledTime(model, model.ModifiedBy);
                    return false;
                }

            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchObject CancelJob(BatchObject batchObject, string username) {
            try {
                batchObject.BatchJob.JobStatus = Convert.ToInt32(BatchJobStatus.Cancelled);
                batchObject.BatchJob = UpdateJob(batchObject.BatchJob, username);

                return batchObject;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchObject PrepareJobAndTriggerFromBatchView(BatchView model) {
            try {
                BatchObject result;
                if (model.TriggerId != null) {
                    IBatchTriggerRepo triggerRepo = new BatchTriggerRepo(db);
                    result = triggerRepo.GetBatchObjectFromBatchView(model);

                }
                else if (model.JobId != null) {
                    IBatchJobRepo jobRepo = new BatchJobRepo(db);
                    result = jobRepo.GetBatchObjectByJobId(model.JobId);
                }
                else {
                    result = null;
                }
                return result;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public BatchObject HoldJob(BatchObject batchObject, string username) {
            try {
                int waiting = Convert.ToInt32(BatchJobStatus.Waiting);
                if (batchObject.BatchJob.JobStatus != waiting) {
                    SmartAppException e = new SmartAppException("ERROR.ERROR");
                    e.AddData("Cannot hold non-Waiting job status.");
                    throw SmartAppUtil.AddStackTrace(e);
                }
                batchObject.BatchJob.JobStatus = Convert.ToInt32(BatchJobStatus.OnHold);
                batchObject.BatchJob = UpdateJob(batchObject.BatchJob, username);
                return batchObject;

            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchObject ReleaseJob(BatchObject batchObject, string username) {
            try {
                int hold = Convert.ToInt32(BatchJobStatus.OnHold);

                if (batchObject.BatchJob.JobStatus != hold) {
                    SmartAppException e = new SmartAppException("ERROR.ERROR");
                    e.AddData("Cannot release non-Hold job status.");
                    throw SmartAppUtil.AddStackTrace(e);
                }
                if(batchObject.BatchTrigger.IsFinalized)
                {
                    batchObject.BatchJob.JobStatus = Convert.ToInt32(BatchJobStatus.Ended);
                }
                else
                {
                    batchObject.BatchJob.JobStatus = Convert.ToInt32(BatchJobStatus.Waiting);
                }
                

                batchObject.BatchJob = UpdateJob(batchObject.BatchJob, username);
                return batchObject;

            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool RemoveJobAndTrigger(BatchObject model) {
            try {
                if (db != null) {
                    IBatchJobRepo jobRepo = new BatchJobRepo(db);
                    IBatchTriggerRepo triggerRepo = new BatchTriggerRepo(db);

                    triggerRepo.Remove(model.BatchTrigger);
                    jobRepo.Remove(model.BatchJob);
                    UnitOfWork.Commit();
                }
                return true;
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchJob UpdateJob(BatchJob model, string username) {
            try {
                if(db != null) {
                    IBatchJobRepo jobRepo = new BatchJobRepo(db);
                    model.ModifiedBy = username;
                    jobRepo.Update(model);

                    UnitOfWork.Commit();
                }
                return model;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        public  BatchInstanceObject GetBatchInstanceObjectFromInstanceId(BatchHistoryView model) {
            try {
                IBatchInstanceHistoryRepo historyRepo = new BatchInstanceHistoryRepo(db);
                return historyRepo.GetBatchInstanceObjectFromInstanceId(model.InstanceHistoryId);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchInstanceObject RetryJobInstance(BatchInstanceObject model, string username) {
            try {
                int failed = Convert.ToInt32(BatchInstanceState.Failed);
                int retryFailed = Convert.ToInt32(BatchInstanceState.RetryFailed);

                if(model.BatchInstanceHistory.InstanceState != failed &&
                    model.BatchInstanceHistory.InstanceState != retryFailed) {
                    SmartAppException e = new SmartAppException("ERROR.ERROR");
                    e.AddData("Cannot retry non-Failed history state.");
                    throw SmartAppUtil.AddStackTrace(e);
                }
                if(model.BatchTrigger.IsFinalized) {
                    model.BatchJob.JobStatus = Convert.ToInt32(BatchJobStatus.Ended);
                    model.BatchJob = UpdateJob(model.BatchJob, username);

                    SmartAppException e = new SmartAppException("ERROR.ERROR");
                    e.AddData("Cannot retry finalized job.");
                    throw SmartAppUtil.AddStackTrace(e);
                }

                model.BatchInstanceHistory.InstanceState = Convert.ToInt32(BatchInstanceState.Retried);
                model.BatchJob.JobStatus = Convert.ToInt32(BatchJobStatus.Waiting);
                model = UpdateJobAndInstanceHistory(model, username);

                return model;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BatchInstanceObject CancelInstance(BatchInstanceObject model, string username) {
            try {
                int failed = Convert.ToInt32(BatchInstanceState.Failed);
                int retryFailed = Convert.ToInt32(BatchInstanceState.RetryFailed);

                if (model.BatchInstanceHistory.InstanceState != failed &&
                    model.BatchInstanceHistory.InstanceState != retryFailed) {
                    SmartAppException e = new SmartAppException("ERROR.ERROR");
                    e.AddData("Cannot cancel non-Failed history state.");
                    throw SmartAppUtil.AddStackTrace(e);
                }
                model.BatchInstanceHistory.InstanceState = Convert.ToInt32(BatchInstanceState.Cancelled);
                if(model.BatchTrigger.IsFinalized)
                {
                    model.BatchJob.JobStatus = Convert.ToInt32(BatchJobStatus.Ended);
                }
                else
                {
                    model.BatchJob.JobStatus = Convert.ToInt32(BatchJobStatus.Waiting);
                }
                
                model = UpdateJobAndInstanceHistory(model, username);

                return model;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool UpdateJobsTriggersUpdateInstancesAddLogs(IEnumerable<BatchJob> jobList, IEnumerable<BatchTrigger> triggerList, IEnumerable<BatchInstanceHistory> historyList, IEnumerable<BatchInstanceLog> logList) {
            try {
                if(db != null) {
                    if(historyList.Count() > 0) {
                        IBatchInstanceHistoryRepo historyRepo = new BatchInstanceHistoryRepo(db);
                        historyRepo.Update(historyList);
                    }
                    if(logList.Count() > 0) {
                        IBatchInstanceLogRepo logRepo = new BatchInstanceLogRepo(db);
                        logRepo.Add(logList);
                    }
                    if(triggerList.Count() > 0) {
                        IBatchTriggerRepo triggerRepo = new BatchTriggerRepo(db);
                        triggerRepo.Update(triggerList);
                    }
                    if(jobList.Count() > 0)
                    {
                        IBatchJobRepo jobRepo = new BatchJobRepo(db);
                        jobRepo.Update(jobList);
                    }
                    UnitOfWork.Commit();
                    
                }
                return true;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool CleanUpBatchHistory(CleanUpBatchHistoryView model) {
            try {
                Guid jobId = model.JobId.StringToGuid();
                var batchHistoryQuery = db.BatchInstanceHistory.Where(item => item.JobId == jobId);

                // get criteria from param model
                DateTime? fromDate = null; 
                DateTime toDate;
                if(model.FromCreatedDate != null) {
                    fromDate = model.FromCreatedDate.StringToDate();
                }
                if(model.ToCreatedDate != null) {
                    toDate = model.ToCreatedDate.StringToDate();
                    toDate.AddDays(1);
                }
                else 
                { 
                    toDate = DateTime.Now;
                }
                
                //build query from date range
                batchHistoryQuery = batchHistoryQuery.Where(item => item.CreatedDateTime <= toDate);

                if (fromDate != null) {
                    batchHistoryQuery = batchHistoryQuery.Where(item => item.CreatedDateTime >= fromDate);
                }

                if (model.InstanceState != null && model.InstanceState.Length > 0) {
                    var instantStates = model.InstanceState.ToList();
                    batchHistoryQuery = batchHistoryQuery.Where(item => item.InstanceState.HasValue && instantStates.Contains(item.InstanceState.Value));
                }

                if(batchHistoryQuery.Count() == 0) {
                    SmartAppException e = new SmartAppException("ERROR.ERROR");
                    e.AddData("ERROR.00477");
                    throw SmartAppUtil.AddStackTrace(e);
                }

                // build batch log query
                var batchLogQuery = db.BatchInstanceLog.Where(item => batchHistoryQuery.Any(item2 => item2.InstanceHistoryId == item.InstanceHistoryId));

                db.BatchInstanceLog.RemoveRange(batchLogQuery);
                db.BatchInstanceHistory.RemoveRange(batchHistoryQuery);

                UnitOfWork.Commit();
                return true;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteBatchJob(BatchView model) {
            try {
                Guid jobId = model.JobId.StringToGuid();
                
                // build job query
                var batchJobQuery = db.BatchJob.Where(item => item.JobId == jobId);

                // validate job status
                int ended = Convert.ToInt32(BatchJobStatus.Ended);
                int cancelled = Convert.ToInt32(BatchJobStatus.Cancelled);
                BatchJob job = batchJobQuery.FirstOrDefault();
                if(job.JobStatus != ended && job.JobStatus != cancelled) {
                    // error deleting batch job
                    SmartAppException e = new SmartAppException("ERROR.00478");
                    e.AddData("ERROR.00479", job.JobStatus);
                    throw SmartAppUtil.AddStackTrace(e);
                }

                // build trigger query
                var batchTriggerQuery = db.BatchTrigger.Where(item => batchJobQuery.Any(item2 => item2.JobId == item.JobId));

                // build history query
                var batchHistoryQuery = db.BatchInstanceHistory.Where(item => batchJobQuery.Any(item2 => item2.JobId == item.JobId));

                // build log query
                var batchLogQuery = db.BatchInstanceLog.Where(item => batchHistoryQuery.Any(item2 => item2.InstanceHistoryId == item.InstanceHistoryId));

                db.BatchInstanceLog.RemoveRange(batchLogQuery);
                db.BatchInstanceHistory.RemoveRange(batchHistoryQuery);
                db.BatchTrigger.RemoveRange(batchTriggerQuery);
                db.BatchJob.RemoveRange(batchJobQuery);
                
                UnitOfWork.Commit();
                return true;
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<bool> UpdateBatchHistoryBatchJobAtSchedulerStarted(DateTime startedTime, string dateTimeFormat, IScheduler scheduler) {
            try {
                // update running instance state = failed/retry failed
                DateTimeOffset started = DateTimeOffset.ParseExact(startedTime.DateTimeToString(), dateTimeFormat, CultureInfo.InvariantCulture);

                IBatchInstanceHistoryRepo historyRepo = new BatchInstanceHistoryRepo(db);
                var runningInstances = historyRepo.GetAllRunningInstancesBeforeRestarted(startedTime).ToList();
                List<BatchInstanceLog> logList = new List<BatchInstanceLog>();
                List<BatchTrigger> triggersToUpdate = new List<BatchTrigger>();


                int running = Convert.ToInt32(BatchInstanceState.Executing);
                int retrying = Convert.ToInt32(BatchInstanceState.Retrying);
                string modifiedBy = SmartAppUtil.GetMachineName();

                foreach (var item in runningInstances) {
                    if(item.InstanceState == running) {
                        item.InstanceState = Convert.ToInt32(BatchInstanceState.Failed);
                        item.ModifiedBy = modifiedBy;

                        BatchInstanceLog log = new BatchInstanceLog();
                        log.TimeStamp = DateTime.Now;
                        log.InstanceHistoryId = item.InstanceHistoryId;
                        log.ResultStatus = Convert.ToInt32(BatchResultStatus.Fail);
                        log.Message = "Scheduler restarted when this instance was running.";
                        
                        logList.Add(log);
                    }
                    if(item.InstanceState == retrying) {
                        item.InstanceState = Convert.ToInt32(BatchInstanceState.RetryFailed);
                        item.ModifiedBy = modifiedBy;

                        BatchInstanceLog log = new BatchInstanceLog();
                        log.TimeStamp = DateTime.Now;
                        log.InstanceHistoryId = item.InstanceHistoryId;
                        log.ResultStatus = Convert.ToInt32(BatchResultStatus.Fail);
                        log.Message = "Scheduler restarted when this instance was running.";

                        logList.Add(log);
                    }

                    if(item.TriggerId != null)
                    {
                        var group = item.Job.GroupName;
                        if (group == null)
                        {
                            group = SchedulerConstants.DefaultGroup;
                        }
                        TriggerKey trigKey = new TriggerKey(item.TriggerId.GuidNullToString(), group);
                        ITrigger trigger = await scheduler.GetTrigger(trigKey);
                        // pause failed instance's trigger (only if there's next fire time)
                        if(trigger != null)
                        {
                            var nextFireTime = trigger.GetFireTimeAfter(started);
                            if(nextFireTime != null)
                            {
                                await scheduler.PauseTrigger(trigKey);
                            }
                            
                        }
                        else
                        {
                            item.Trigger.NextScheduledTime = null;
                            item.Trigger.IsFinalized = true;
                            item.Trigger.ModifiedBy = modifiedBy;
                            triggersToUpdate.Add(item.Trigger);
                        }

                    }
                }

                // update next scheduled time for waiting job
                IBatchTriggerRepo triggerRepo = new BatchTriggerRepo(db);
                List<BatchObject> triggersFromWaitingJobs = triggerRepo.GetTriggersByWaitingJobBeforeRestarted(startedTime).ToList();
                List<BatchJob> jobsToUpdate = new List<BatchJob>();

                if(triggersFromWaitingJobs.Count() > 0) {
                    foreach (var item in triggersFromWaitingJobs) {
                        var group = item.BatchJob.GroupName;
                        if (group == null)
                        {
                            group = SchedulerConstants.DefaultGroup;
                        }
                        TriggerKey k = new TriggerKey(item.BatchTrigger.TriggerId.GuidNullToString(), group);
                        ITrigger trig = await scheduler.GetTrigger(k);
                        if(trig != null)
                        {
                            item.BatchTrigger.NextScheduledTime = trig.GetFireTimeAfter(started)?.ToLocalTime().DateTime;
                        }
                        else
                        {
                            // trigger not in scheduler
                            item.BatchTrigger.NextScheduledTime = null;
                            item.BatchTrigger.IsFinalized = true;

                            item.BatchJob.JobStatus = (int)BatchJobStatus.Ended;
                            item.BatchJob.ModifiedBy = modifiedBy;
                            jobsToUpdate.Add(item.BatchJob);
                        }
                        item.BatchTrigger.ModifiedBy = modifiedBy;
                        triggersToUpdate.Add(item.BatchTrigger);
                    }
                }

                return UpdateJobsTriggersUpdateInstancesAddLogs(jobsToUpdate, triggersToUpdate, runningInstances, logList);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private bool IsStatusEnded(int status) {
            return status == Convert.ToInt32(BatchJobStatus.Ended);
        }
        
        public bool ValidateRetryInstanceHistory(BatchHistoryView model)
        {
            try
            {
                IBatchJobRepo jobRepo = new BatchJobRepo(db);
                return !jobRepo.GetTriggerFinalizedByJobId(model.JobId);
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
