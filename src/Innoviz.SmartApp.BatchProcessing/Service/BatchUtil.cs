﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.InterfaceWithOtherSystems.Services;
using Innoviz.SmartApp.Data.Repositories;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ServicesV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.BatchProcessing.Service
{
    public class BatchUtil {

        private Dictionary<string, FunctionClassMapping> functionClassMapping;

        public BatchUtil() {
            
        }
        public BatchFunctionClassMapping GetMappingFromCallUrl(string callUrl) {
            try {
                string key = callUrl.ToLowerInvariant();
                foreach (var item in BatchFunctionMapping.Mappings.Keys)
                {
                    if(item.ToLowerInvariant() == key)
                    {
                        return BatchFunctionMapping.Mappings[item];
                    }
                }
                throw new Exception("Batch function class mapping does not contain key: '" + callUrl + "'");
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public Type GetCallerClassFromCallUrl(string callUrl) {
            try {
                BatchFunctionClassMapping item = GetMappingFromCallUrl(callUrl);
                string[] classNamespace = item.ClassName.Split(".");
                if(classNamespace.Any(a => a == "InterfaceWithOtherSystems"))
                {
                    return typeof(InterfaceStagingService).Assembly.GetTypes().Where(t => t.FullName == item.ClassName).FirstOrDefault();
                }
                else if (classNamespace.Any(a => a == "ServicesV2"))
                {
                    return typeof(StagingTableService).Assembly.GetTypes().Where(t => t.FullName == item.ClassName).FirstOrDefault();
                }
                else if (classNamespace.Any(a => a == "RepositoriesV2"))
                {
                    return typeof(StagingTableRepo).Assembly.GetTypes().Where(t => t.FullName == item.ClassName).FirstOrDefault();
                }
                else if(classNamespace[classNamespace.Length-2] == "Repositories") {
                    return typeof(SysUserTableRepo).Assembly.GetTypes().Where(t => t.FullName == item.ClassName).FirstOrDefault();
                }
                else {
                    return typeof(SysMessageService).Assembly.GetTypes().Where(t => t.FullName == item.ClassName).FirstOrDefault();
                }
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
    }
    public class FunctionClassMapping {
        public string ClassName { get; set; }
        public string MethodName { get; set; }
    }
}
