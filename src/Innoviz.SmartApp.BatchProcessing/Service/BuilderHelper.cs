﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Quartz;
using System;
using System.Text;
using static Innoviz.SmartApp.Data.BatchProcessing.BatchEnum;

namespace Innoviz.SmartApp.BatchProcessing.Service {
    public class BuilderHelper {

        public string BuildCronExpression(BatchTrigger inputTrigger, bool isEndOfMonth)
        {

            DateTime startDateTime = inputTrigger.StartDateTime;

            BatchIntervalType intervalType = (BatchIntervalType)Convert.ToInt32(inputTrigger.IntervalType);
            string intervalCount = inputTrigger.IntervalCount;

            switch (intervalType)
            {
                case BatchIntervalType.Minutes:
                    return BuildCRONEveryXMinutes(startDateTime, intervalCount);
                case BatchIntervalType.Hours:
                    return BuildCRONEveryXHours(startDateTime, intervalCount);
                case BatchIntervalType.Days:
                    return BuildCRONEveryXDays(startDateTime, intervalCount);
                case BatchIntervalType.Months:
                    return BuildCRONEveryXMonths(startDateTime, intervalCount, isEndOfMonth);
                case BatchIntervalType.Years:
                    return BuildCRONEveryXYears(startDateTime, intervalCount, isEndOfMonth);
                case BatchIntervalType.DayOfWeek:
                    return BuildCRONEveryXDayOfWeek(startDateTime, intervalCount);
                default:
                    throw new SmartAppException("Unsupported batch interval type.");
            }
        }
        public string BuildCRONEveryXMinutes(string inputDateTime, string intervalCount)
        {
            try
            {
                DateTime startDateTime = inputDateTime.StringToDateTime();
                return BuildCRONEveryXMinutes(startDateTime, intervalCount);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string BuildCRONEveryXMinutes(DateTime startDateTime, string intervalCount)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendJoin(" ", 
                    startDateTime.Second, 
                    "*/" + intervalCount, 
                    "*",
                    "*",
                    "*", 
                    "?",
                    "*");
                return sb.ToString();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string BuildCRONEveryXHours(string inputDateTime, string intervalCount)
        {
            try
            {
                DateTime startDateTime = inputDateTime.StringToDateTime();
                return BuildCRONEveryXHours(startDateTime, intervalCount);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string BuildCRONEveryXHours(DateTime startDateTime, string intervalCount)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendJoin(" ",
                    startDateTime.Second,
                    startDateTime.Minute,
                    "*/" + intervalCount,
                    "*",
                    "*",
                    "?",
                    "*");
                return sb.ToString();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string BuildCRONEveryXDays(string inputDateTime, string intervalCount)
        {
            try
            {
                DateTime startDateTime = inputDateTime.StringToDateTime();
                return BuildCRONEveryXDays(startDateTime, intervalCount);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string BuildCRONEveryXDays(DateTime startDateTime, string intervalCount)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendJoin(" ",
                    startDateTime.Second,
                    startDateTime.Minute,
                    startDateTime.Hour,
                    "*/" + intervalCount,
                    "*",
                    "?",
                    "*");
                return sb.ToString();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string BuildCRONEveryXMonths(string inputDateTime, string intervalCount, bool isEndOfMonth)
        {
            try
            {
                DateTime startDateTime = inputDateTime.StringToDateTime();
                return BuildCRONEveryXMonths(startDateTime, intervalCount, isEndOfMonth);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string BuildCRONEveryXMonths(DateTime startDateTime, string intervalCount, bool isEndOfMonth)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                bool isLastDayOfMonth = 
                    startDateTime.Day == DateTime.DaysInMonth(startDateTime.Year, startDateTime.Month);
                if(isLastDayOfMonth || isEndOfMonth)
                {
                    sb.AppendJoin(" ",
                    startDateTime.Second,
                    startDateTime.Minute,
                    startDateTime.Hour,
                    "L",
                    "*/" + intervalCount,
                    "?",
                    "*");
                }
                else
                {
                    sb.AppendJoin(" ",
                    startDateTime.Second,
                    startDateTime.Minute,
                    startDateTime.Hour,
                    startDateTime.Day,
                    "*/" + intervalCount,
                    "?",
                    "*");
                }
                return sb.ToString();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string BuildCRONEveryXYears(string inputDateTime, string intervalCount, bool isEndOfMonth)
        {
            try
            {
                DateTime startDateTime = inputDateTime.StringToDateTime();
                return BuildCRONEveryXYears(startDateTime, intervalCount, isEndOfMonth);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string BuildCRONEveryXYears(DateTime startDateTime, string intervalCount, bool isEndOfMonth)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                // check start date/month (case leap year)
                if(isEndOfMonth ||
                    (startDateTime.Month == 2 &&
                    (startDateTime.Day == 28 ||
                    startDateTime.Day == 29)))
                {
                    sb.AppendJoin(" ",
                    startDateTime.Second,
                    startDateTime.Minute,
                    startDateTime.Hour,
                    "L",
                    startDateTime.Month,
                    "?",
                    startDateTime.Year + "/" + intervalCount);
                }
                else
                {
                    sb.AppendJoin(" ",
                    startDateTime.Second,
                    startDateTime.Minute,
                    startDateTime.Hour,
                    startDateTime.Day,
                    startDateTime.Month,
                    "?",
                    startDateTime.Year + "/" + intervalCount);
                }
                return sb.ToString();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string BuildCRONEveryXDayOfWeek(string inputDateTime, string intervalCount)
        {
            try
            {
                DateTime startDateTime = inputDateTime.StringToDateTime();
                return BuildCRONEveryXDayOfWeek(startDateTime, intervalCount);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string BuildCRONEveryXDayOfWeek(DateTime startDateTime, string intervalCount)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendJoin(" ",
                    startDateTime.Second,
                    startDateTime.Minute,
                    startDateTime.Hour,
                    "?",
                    "*",
                    intervalCount,
                    "*");
                return sb.ToString();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public JobBuilder SetJobIdentity(JobBuilder jobBuilder, string jobId, string groupName) {
            if (groupName != null && groupName != "") {
                jobBuilder = jobBuilder.WithIdentity(jobId, groupName);
            }
            else {
                jobBuilder = jobBuilder.WithIdentity(jobId);
            }
            return jobBuilder;
        }

        public TriggerBuilder SetTriggerIdentity(TriggerBuilder triggerBuilder,
                                                 string triggerId,
                                                 string jobId,
                                                 string groupName) {
            if (groupName != null && groupName != "") {
                triggerBuilder = triggerBuilder.WithIdentity(triggerId, groupName)
                .ForJob(jobId, groupName);
            }
            else {
                triggerBuilder = triggerBuilder.WithIdentity(triggerId)
                .ForJob(jobId);
            }
            return triggerBuilder;
        }

        public TriggerBuilder SetTriggerSchedule(TriggerBuilder triggerBuilder, BatchTrigger inputTrigger) {
            // repeat count
            if (inputTrigger.RecurringCount < 1 || inputTrigger.RecurringCount == null) {
                triggerBuilder = SetCronSchedule(triggerBuilder, inputTrigger);
            }
            else {
                triggerBuilder = SetSimpleSchedule(triggerBuilder, inputTrigger);
            }
            return triggerBuilder;
        }

        private TriggerBuilder SetCronSchedule(TriggerBuilder triggerBuilder, BatchTrigger inputTrigger) {
            triggerBuilder =
            triggerBuilder.WithCronSchedule(inputTrigger.CRONExpression, x => x
                                                .WithMisfireHandlingInstructionDoNothing()
                                                );
            return triggerBuilder;
        }

        private TriggerBuilder SetSimpleSchedule(TriggerBuilder triggerBuilder, BatchTrigger inputTrigger) {

            BatchIntervalType intervalType = (BatchIntervalType)Convert.ToInt32(inputTrigger.IntervalType);
            if(intervalType == BatchIntervalType.DayOfWeek)
            {
                SmartAppException ex = new SmartAppException("Error building batch trigger.");
                ex.AddData("Interval type 'DayOfWeek' cannot be used with repeat count.");
                throw ex;
            }
            int intervalCount = Convert.ToInt32(inputTrigger.IntervalCount);
            int recurringCount = Convert.ToInt32(inputTrigger.RecurringCount);
            recurringCount = recurringCount > 0 ? recurringCount - 1 : 0;

            switch (intervalType) {
                case BatchIntervalType.Seconds:
                triggerBuilder =
                    triggerBuilder.WithSimpleSchedule(x => x
                                        .WithMisfireHandlingInstructionNextWithRemainingCount()
                                        .WithRepeatCount(recurringCount)
                                        .WithIntervalInSeconds(intervalCount));
                break;
                case BatchIntervalType.Minutes:
                triggerBuilder =
                    triggerBuilder.WithSimpleSchedule(x => x
                                        .WithMisfireHandlingInstructionNextWithRemainingCount()
                                        .WithRepeatCount(recurringCount)
                                        .WithIntervalInMinutes(intervalCount));
                break;
                case BatchIntervalType.Hours:
                triggerBuilder =
                    triggerBuilder.WithSimpleSchedule(x => x
                                        .WithMisfireHandlingInstructionNextWithRemainingCount()
                                        .WithRepeatCount(recurringCount)
                                        .WithIntervalInHours(intervalCount));
                break;
                case BatchIntervalType.Days:
                triggerBuilder =
                    triggerBuilder.WithSimpleSchedule(x => x
                                        .WithMisfireHandlingInstructionNextWithRemainingCount()
                                        .WithRepeatCount(recurringCount)
                                        .WithIntervalInHours(intervalCount * 24));
                break;
                case BatchIntervalType.Months:
                // 1 month = 30 days
                triggerBuilder =
                    triggerBuilder.WithSimpleSchedule(x => x
                                        .WithMisfireHandlingInstructionNextWithRemainingCount()
                                        .WithRepeatCount(recurringCount)
                                        .WithIntervalInHours(intervalCount * 30 * 24));
                break;
                case BatchIntervalType.Years:
                // 1 year = 365 days
                triggerBuilder =
                    triggerBuilder.WithSimpleSchedule(x => x
                                        .WithMisfireHandlingInstructionNextWithRemainingCount()
                                        .WithRepeatCount(recurringCount)
                                        .WithIntervalInHours(intervalCount * 365 * 24));
                break;
            }

            return triggerBuilder;
        }
        
    }
}
