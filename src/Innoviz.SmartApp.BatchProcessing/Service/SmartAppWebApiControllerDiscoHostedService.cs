﻿using IdentityModel.Client;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.BatchProcessing.Service
{
    public class SmartAppWebApiControllerDiscoHostedService: IHostedService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IConfiguration Configuration;
        public SmartAppWebApiControllerDiscoHostedService(IServiceProvider serviceProvider, IConfiguration config)
        {
            _serviceProvider = serviceProvider;
            Configuration = config;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                var getlistControllers = await GetListControllerDisco();
                SystemStaticData.SetSysControllerEntityMappingData(getlistControllers.ToStaticSysControllerEntityTempData());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        // noop
        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;

       
        private async Task<List<SysControllerEntityMappingView>> GetListControllerDisco()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    string webApiUrl = Configuration["BusinessApi:BaseUrl"];
                    string authServer = Configuration["JWT:Authority"];
                    string reqClient = Configuration["BatchApi:Client"];
                    string reqSecret = Configuration["BatchApi:ClientSecret"];
                    string reqApi = Configuration["BusinessApi:Scope"];

                    string url = webApiUrl + "SysAccessRight/GetSysControllerDiscoveryList";
                    string token = await GetToken(authServer, reqClient, reqSecret, reqApi);
                    string requestId = Guid.NewGuid().ToString().ToLower();

                    if (token != null && token != "")
                    {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    }

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }

                    var response = await httpClient.GetAsync(url);

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error getting GetListController list.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error getting GetListController list.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        List<SysControllerEntityMappingView> result =
                            JsonConvert.DeserializeObject<List<SysControllerEntityMappingView>>(jsonContent);
                        return result;
                    }

                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
       
        private async Task<string> GetToken(string authServer, string reqClient, string reqSecret, string reqApi)
        {
            //HttpHelper helper = new HttpHelper();
            //return await helper.GetClientCredentialsToken(authServer, reqClient, reqSecret, reqApi);
            DiscoveryDocumentResponse discoRes;
            using (var httpClient = new HttpClient())
            {
                var disco = await httpClient.GetDiscoveryDocumentAsync(authServer);
                if (disco.IsError)
                {
                    SmartAppException ex = new SmartAppException("Error loading Dicovery Document.");
                    ex.AddData(disco.Error);
                    throw SmartAppUtil.AddStackTrace(ex);

                }
                else
                {
                    discoRes = disco;
                }
            }

            TokenResponse tokenResponse;
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
                {
                    Address = discoRes.TokenEndpoint,
                    ClientId = reqClient,
                    ClientSecret = reqSecret,
                    Scope = reqApi,

                });
                if (response.IsError)
                {
                    SmartAppException ex = new SmartAppException("Error getting token.");
                    ex.AddData(response.Error);
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    tokenResponse = response;
                }
            }
            return tokenResponse?.AccessToken;

        }
    }
}
