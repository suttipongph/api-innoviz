﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using Innoviz.SmartApp.Core;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Filters;
using Serilog.Sinks.MSSqlServer;
using Serilog.Sinks.MSSqlServer.Sinks.MSSqlServer.Options;

namespace Innoviz.SmartApp.BatchProcessing {
    public class Program
    {
        public static int Main(string[] args) {
            var configuration = new ConfigurationBuilder()
                                    .SetBasePath(Directory.GetCurrentDirectory())
                                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                                    .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
                                    .AddEnvironmentVariables()
                                    .Build();

            SystemStaticData.SetConfiguration(configuration);

            string connectionString = configuration.GetConnectionString("SmartApp");
            string logTable = "BatchInstanceLog";
            var colOptions = new ColumnOptions();

            colOptions.Store.Remove(StandardColumn.Properties);
            colOptions.Store.Remove(StandardColumn.Level);
            colOptions.Store.Remove(StandardColumn.Exception);
            colOptions.Store.Remove(StandardColumn.LogEvent);
            colOptions.Store.Remove(StandardColumn.Message);
            colOptions.Store.Remove(StandardColumn.MessageTemplate);

            colOptions.AdditionalColumns = new Collection<SqlColumn> {
                new SqlColumn{ DataType = SqlDbType.UniqueIdentifier, ColumnName = "InstanceHistoryId" },
                new SqlColumn{ DataType = SqlDbType.Int, ColumnName = "ResultStatus" },
                new SqlColumn{ DataType = SqlDbType.NVarChar, DataLength = -1, ColumnName = "Message" },
                new SqlColumn{ DataType = SqlDbType.NVarChar, DataLength = -1, ColumnName = "ItemValues" },
                new SqlColumn{ DataType = SqlDbType.NVarChar, DataLength = -1, ColumnName = "StackTrace" },
                new SqlColumn{ DataType = SqlDbType.NVarChar, DataLength = -1, ColumnName = "Reference" },
            };

            string rootLogFolder = configuration.GetValue<string>("LogRootFolder");
            string logFilePath = "logs/batchLog.txt";
            string msgTemplate = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} ReqID:[{HttpRequestId}] InstID:[{BatchInstanceId}] [{BatchEvent}] [{Level:u3}] {Message:lj}{NewLine}{Exception}";

            if (!string.IsNullOrWhiteSpace(rootLogFolder))
            {
                logFilePath = Path.Combine(rootLogFolder, logFilePath);
            }
            Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(configuration)
            .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
            .Enrich.FromLogContext()
            .WriteTo.Logger(lc=>lc
                .WriteTo.File(path: logFilePath, rollingInterval: RollingInterval.Day,
                              outputTemplate: msgTemplate))
            .WriteTo.Logger(lc=>lc
                .WriteTo.Console(outputTemplate: msgTemplate))
            .WriteTo.Logger(lc => lc
                .Filter.ByIncludingOnly(Matching.FromSource("Innoviz.SmartApp.Data.BatchProcessing"))
                .WriteTo.MSSqlServer(
                    connectionString: connectionString,
                    //tableName: logTable,
                    columnOptions: colOptions,
                    restrictedToMinimumLevel: LogEventLevel.Information,
                    //batchPostingLimit: 50
                    sinkOptions: new SinkOptions
                        {
                            TableName = logTable,
                            BatchPostingLimit = 50,
                        }
                    ))
            .CreateLogger();

            try {
                Log.Information("Starting host");
                CreateHostBuilder(args).Build().Run();
                return 0;
            }
            catch (Exception ex) {
                Log.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseContentRoot(Directory.GetCurrentDirectory());
                webBuilder.UseStartup<Startup>();
                webBuilder.UseSerilog();
            });
        //.UseKestrel()
        //.UseContentRoot(Directory.GetCurrentDirectory())
        //.UseIISIntegration()
        //.UseStartup<Startup>()
        //.UseSerilog();
    }
}
