﻿using System;

namespace Innoviz.SmartApp.BatchProcessing.Model
{
    public class TriggerKeyModel
    {
        public string Group { get; set; }
        public string Name { get; set; }
        public bool IsTriggerPaused { get; set; }
        public string UserName { get; set; }
    }
    public class DateTimeParameter
    {
        public DateTime dateTime { get; set; }
    }
}
