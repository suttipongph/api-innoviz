﻿using System.ComponentModel.DataAnnotations;

namespace Innoviz.SmartApp.BatchProcessing.Model.QuartzModels
{
    public partial class QRTZ_SCHEDULER_STATE
    {
        [StringLength(120)]
        public string SCHED_NAME { get; set; }
        [StringLength(200)]
        public string INSTANCE_NAME { get; set; }
        public long LAST_CHECKIN_TIME { get; set; }
        public long CHECKIN_INTERVAL { get; set; }
    }
}
