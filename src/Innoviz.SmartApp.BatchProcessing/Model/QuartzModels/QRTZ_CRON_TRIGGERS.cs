﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.BatchProcessing.Model.QuartzModels
{
    public partial class QRTZ_CRON_TRIGGERS
    {
        [StringLength(120)]
        public string SCHED_NAME { get; set; }
        [StringLength(150)]
        public string TRIGGER_NAME { get; set; }
        [StringLength(150)]
        public string TRIGGER_GROUP { get; set; }
        [Required]
        [StringLength(120)]
        public string CRON_EXPRESSION { get; set; }
        [StringLength(80)]
        public string TIME_ZONE_ID { get; set; }

        [ForeignKey("SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP")]
        [InverseProperty("QRTZ_CRON_TRIGGERS")]
        public QRTZ_TRIGGERS QRTZ_TRIGGERS { get; set; }
    }
}
