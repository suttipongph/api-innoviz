﻿using System.ComponentModel.DataAnnotations;

namespace Innoviz.SmartApp.BatchProcessing.Model.QuartzModels
{
    public partial class QRTZ_CALENDARS
    {
        [StringLength(120)]
        public string SCHED_NAME { get; set; }
        [StringLength(200)]
        public string CALENDAR_NAME { get; set; }
        [Required]
        public byte[] CALENDAR { get; set; }
    }
}
