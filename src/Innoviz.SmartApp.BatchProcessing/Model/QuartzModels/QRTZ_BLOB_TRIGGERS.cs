﻿using System.ComponentModel.DataAnnotations;

namespace Innoviz.SmartApp.BatchProcessing.Model.QuartzModels
{
    public partial class QRTZ_BLOB_TRIGGERS
    {
        [StringLength(120)]
        public string SCHED_NAME { get; set; }
        [StringLength(150)]
        public string TRIGGER_NAME { get; set; }
        [StringLength(150)]
        public string TRIGGER_GROUP { get; set; }
        public byte[] BLOB_DATA { get; set; }
    }
}
