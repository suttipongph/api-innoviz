﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.BatchProcessing.Model.QuartzModels
{
    public partial class QRTZ_JOB_DETAILS
    {
        public QRTZ_JOB_DETAILS()
        {
            QRTZ_TRIGGERS = new HashSet<QRTZ_TRIGGERS>();
        }

        [StringLength(120)]
        public string SCHED_NAME { get; set; }
        [StringLength(150)]
        public string JOB_NAME { get; set; }
        [StringLength(150)]
        public string JOB_GROUP { get; set; }
        [StringLength(250)]
        public string DESCRIPTION { get; set; }
        [Required]
        [StringLength(250)]
        public string JOB_CLASS_NAME { get; set; }
        public bool IS_DURABLE { get; set; }
        public bool IS_NONCONCURRENT { get; set; }
        public bool IS_UPDATE_DATA { get; set; }
        public bool REQUESTS_RECOVERY { get; set; }
        public byte[] JOB_DATA { get; set; }

        [InverseProperty("QRTZ_JOB_DETAILS")]
        public ICollection<QRTZ_TRIGGERS> QRTZ_TRIGGERS { get; set; }
    }
}
