﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.BatchProcessing.Model.QuartzModels
{
    public partial class QRTZ_TRIGGERS
    {
        [StringLength(120)]
        public string SCHED_NAME { get; set; }
        [StringLength(150)]
        public string TRIGGER_NAME { get; set; }
        [StringLength(150)]
        public string TRIGGER_GROUP { get; set; }
        [Required]
        [StringLength(150)]
        public string JOB_NAME { get; set; }
        [Required]
        [StringLength(150)]
        public string JOB_GROUP { get; set; }
        [StringLength(250)]
        public string DESCRIPTION { get; set; }
        public long? NEXT_FIRE_TIME { get; set; }
        public long? PREV_FIRE_TIME { get; set; }
        public int? PRIORITY { get; set; }
        [Required]
        [StringLength(16)]
        public string TRIGGER_STATE { get; set; }
        [Required]
        [StringLength(8)]
        public string TRIGGER_TYPE { get; set; }
        public long START_TIME { get; set; }
        public long? END_TIME { get; set; }
        [StringLength(200)]
        public string CALENDAR_NAME { get; set; }
        public int? MISFIRE_INSTR { get; set; }
        public byte[] JOB_DATA { get; set; }

        [ForeignKey("SCHED_NAME,JOB_NAME,JOB_GROUP")]
        [InverseProperty("QRTZ_TRIGGERS")]
        public QRTZ_JOB_DETAILS QRTZ_JOB_DETAILS { get; set; }
        [InverseProperty("QRTZ_TRIGGERS")]
        public QRTZ_CRON_TRIGGERS QRTZ_CRON_TRIGGERS { get; set; }
        [InverseProperty("QRTZ_TRIGGERS")]
        public QRTZ_SIMPLE_TRIGGERS QRTZ_SIMPLE_TRIGGERS { get; set; }
        [InverseProperty("QRTZ_TRIGGERS")]
        public QRTZ_SIMPROP_TRIGGERS QRTZ_SIMPROP_TRIGGERS { get; set; }
    }
}
