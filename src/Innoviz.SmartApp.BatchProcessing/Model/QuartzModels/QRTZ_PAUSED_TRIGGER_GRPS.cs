﻿using System.ComponentModel.DataAnnotations;

namespace Innoviz.SmartApp.BatchProcessing.Model.QuartzModels
{
    public partial class QRTZ_PAUSED_TRIGGER_GRPS
    {
        [StringLength(120)]
        public string SCHED_NAME { get; set; }
        [StringLength(150)]
        public string TRIGGER_GROUP { get; set; }
    }
}
