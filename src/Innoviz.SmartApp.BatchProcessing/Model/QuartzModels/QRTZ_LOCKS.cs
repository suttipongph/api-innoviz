﻿using System.ComponentModel.DataAnnotations;

namespace Innoviz.SmartApp.BatchProcessing.Model.QuartzModels
{
    public partial class QRTZ_LOCKS
    {
        [StringLength(120)]
        public string SCHED_NAME { get; set; }
        [StringLength(40)]
        public string LOCK_NAME { get; set; }
    }
}
