﻿using Innoviz.SmartApp.BatchProcessing.Job;
using Innoviz.SmartApp.BatchProcessing.Listener;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;
using Quartz.Spi;
using System;
using System.Collections.Specialized;
using System.Linq;

namespace Innoviz.SmartApp.BatchProcessing {
    public static class QuartzExtension
    {
        public static void UseQuartz(this IServiceCollection services, string connectionString, params Type[] jobs) {
            services.AddSingleton<IJobFactory, ApiJobFactory>();
            services.Add(jobs.Select(jobType => new ServiceDescriptor(jobType, jobType, ServiceLifetime.Transient)));

            services.AddSingleton<IJobListener, ApiJobListener>();
            services.AddSingleton<ITriggerListener, ApiTriggerListener>();
            services.AddSingleton<ISchedulerListener, ApiSchedulerListener>();

            services.AddSingleton(provider => {
                var properties = new NameValueCollection
                {
                    // json serialization is the one supported under .NET Core (binary isn't)
                    ["quartz.serializer.type"] = "json",

                    // the following setup of job store is just for example and it didn't change from v2
                    ["quartz.jobStore.type"] = "Quartz.Impl.AdoJobStore.JobStoreTX, Quartz",
                    ["quartz.jobStore.useProperties"] = "true",
                    ["quartz.jobStore.dataSource"] = "default",
                    ["quartz.jobStore.tablePrefix"] = "QRTZ_",
                    ["quartz.jobStore.driverDelegateType"] = "Quartz.Impl.AdoJobStore.SqlServerDelegate, Quartz",
                    ["quartz.dataSource.default.provider"] = "SqlServer",
                    ["quartz.dataSource.default.connectionString"] = connectionString
                };
                var schedulerFactory = new StdSchedulerFactory(properties);
                var scheduler = schedulerFactory.GetScheduler().Result;
                scheduler.JobFactory = provider.GetService<IJobFactory>();
                scheduler.Start().Wait();

                scheduler.ListenerManager.AddJobListener(provider.GetService<IJobListener>(), GroupMatcher<JobKey>.AnyGroup());
                scheduler.ListenerManager.AddTriggerListener(provider.GetService<ITriggerListener>(), GroupMatcher<TriggerKey>.AnyGroup());

                scheduler.ListenerManager.AddSchedulerListener(provider.GetService<ISchedulerListener>());

                return scheduler;
            });
        }
    }
}
