using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.WebApi
{
    public partial class SysFeatureTableData
    {
        public static List<SysFeatureGroupControllerMappingView> GetAddFeatureControllerMapping()
        {
            var list = new List<SysFeatureGroupControllerMappingView>()
            {

            };
            //#region validate
            //SmartAppException ex = new SmartAppException("Error SysFeatureTableData");
            //#region SysFeatureTableData_CUSTOM
            var customList = GetAddFeatureControllerMapping_CUSTOM();
            //if (customList.Any(a => a.ControllerName == "CreditAppTable" ||
            //                         a.ControllerName == "CreditAppRequestTable" ||
            //                         a.ControllerName == "MainAgreementTable"))
            //{
            //    var groups = customList.Where(a => a.ControllerName == "CreditAppTable" ||
            //                        a.ControllerName == "CreditAppRequestTable" ||
            //                        a.ControllerName == "MainAgreementTable")
            //                        .Select(s => s.GroupId);
            //    ex.AddData("Controller name not allowed in SysFeautureTableData_CUSTOM,\n please move to SysFeatureTableData_CreditAppTable\n or SysFeatureTableData_CreditAppRequestTable\n or SysFeatureTableData_MainAgreementTable:\n " + string.Join(", ", groups));
            //    throw ex;
            //}
            //#endregion
            //#region SysFeatureTableData_CreditAppRequestTable
            var caRequestList = GetAddFeatureControllerMapping_CreditAppRequestTable();
            //if(caRequestList.Any(a => a.ControllerName != "CreditAppRequestTable"))
            //{
            //    var groups = caRequestList.Where(a => a.ControllerName != "CreditAppRequestTable").Select(s => s.GroupId);
            //    ex.AddData("Controller name not allowed in SysFeatureTableData_CreditAppRequestTable,\n please move to other SysFeatureTableData files: \n" + string.Join(", ", groups));
            //    throw ex;
            //}
            //#endregion
            //#region SysFeatureTableData_CreditAppTable
            var caList = GetAddFeatureControllerMapping_CreditAppTable();
            //if (caList.Any(a => a.ControllerName != "CreditAppTable"))
            //{
            //    var groups = caList.Where(a => a.ControllerName != "CreditAppTable").Select(s => s.GroupId);
            //    ex.AddData("Controller name not allowed in SysFeatureTableData_CreditAppTable,\n please move to other SysFeatureTableData files: \n" + string.Join(", ", groups));
            //    throw ex;
            //}
            //#endregion
            //#region SysFeatureTableData_MainAgreementTable
            var mainAgmList = GetAddFeatureControllerMapping_MainAgreementTable();
            //if (mainAgmList.Any(a => a.ControllerName != "MainAgreementTable"))
            //{
            //    var groups = mainAgmList.Where(a => a.ControllerName != "MainAgreementTable").Select(s => s.GroupId);
            //    ex.AddData("Controller name not allowed in SysFeatureTableData_MainAgreementTable,\n please move to other SysFeatureTableData files: \n" + string.Join(", ", groups));
            //    throw ex;
            //}
            //#endregion
            //#endregion
            var phase2List = GetAddFeatureControllerMapping_Phase2();
            var assignAgmList = GetAddSysFeatureTableData_AssignmentAgreementTable();
            var businessCollateralAgmTableList = GetAddFeatureControllerMapping_BusinessCollateralAgmTable();

            list.AddRange(customList);
            list.AddRange(caRequestList);
            list.AddRange(caList);
            list.AddRange(mainAgmList);

            list.AddRange(phase2List);
            list.AddRange(assignAgmList);
            list.AddRange(businessCollateralAgmTableList);

            return list;
            
        }

        public static List<string> GetOBGroupId()
        {
            return new List<string>()
            {
                "MIGRATION_TABLE",
                "MIGRATION_LOG_TABLE",
                "GENERATE_DUMMY_RECORD",
                "UPDATE_CHEQUE_REFERENCE",
                "GEN_PURCHASE_LINE_INVOICE",
                "GEN_WITHDRAWAL_LINE_INVOICE"
            };
        }
    }
}
