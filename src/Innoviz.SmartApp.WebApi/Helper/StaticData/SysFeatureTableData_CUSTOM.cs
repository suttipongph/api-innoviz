﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi
{
    public partial class SysFeatureTableData
    {
        public static List<SysFeatureGroupControllerMappingView> GetAddFeatureControllerMapping_CUSTOM()
        {
            return new List<SysFeatureGroupControllerMappingView>()
            {
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentMethod",
                    GroupId = "ASSIGNMENT_METHOD",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentMethodItemPage", Path = "/:site/setup/creditapplication/assignmentmethod/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentMethodListPage", Path = "/:site/setup/creditapplication/assignmentmethod/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BillingResponsibleBy",
                    GroupId = "BILLING_RESPONSIBLE_BY",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BillingResponsibleByItemPage", Path = "/:site/setup/creditapplication/billingresponsibleby/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BillingResponsibleByListPage", Path = "/:site/setup/creditapplication/billingresponsibleby/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditLimitType",
                    GroupId = "CREDIT_LIMIT_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditLimitTypeItemPage", Path = "/:site/setup/creditapplication/creditlimittype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditLimitTypeListPage", Path = "/:site/setup/creditapplication/creditlimittype/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditTerm",
                    GroupId = "CREDIT_TERM",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditTermItemPage", Path = "/:site/setup/creditapplication/creditterm/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditTermListPage", Path = "/:site/setup/creditapplication/creditterm/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InterestType",
                    GroupId = "INTEREST_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InterestTypeItemPage", Path = "/:site/setup/creditapplication/interesttype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InterestTypeListPage", Path = "/:site/setup/creditapplication/interesttype/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InterestTypeValueItemComponent", Path = "/:site/setup/creditapplication/interesttype/:id/interesttypevalue-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "InterestTypeItemPage", FeatureId = "InterestTypeValueListPage", Path = "/:site/setup/creditapplication/interesttype/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ProductSubType",
                    GroupId = "PRODUCT_SUB_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSubTypeItemPage", Path = "/:site/setup/creditapplication/productsubtype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSubTypeListPage", Path = "/:site/setup/creditapplication/productsubtype/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BlacklistStatus",
                    GroupId = "BLACKLIST_STATUS",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BlacklistStatusItemPage", Path = "/:site/setup/masterinformation/blackliststatus/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BlacklistStatusListPage", Path = "/:site/setup/masterinformation/blackliststatus/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessSegment",
                    GroupId = "BUSINESS_SEGMENT",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessSegmentItemPage", Path = "/:site/setup/masterinformation/businesssegment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessSegmentListPage", Path = "/:site/setup/masterinformation/businesssegment/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessSize",
                    GroupId = "BUSINESS_SIZE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessSizeItemPage", Path = "/:site/setup/masterinformation/businesssize/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessSizeListPage", Path = "/:site/setup/masterinformation/businesssize/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessType",
                    GroupId = "BUSINESS_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessTypeItemPage", Path = "/:site/setup/masterinformation/businesstype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessTypeListPage", Path = "/:site/setup/masterinformation/businesstype/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerTableItemPage", Path = "/:site/masterinformation/buyertable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerTableListPage", Path = "/:site/masterinformation/buyertable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditScoring",
                    GroupId = "CREDIT_SCORING",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditScoringItemPage", Path = "/:site/setup/masterinformation/creditscoring/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditScoringListPage", Path = "/:site/setup/masterinformation/creditscoring/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustGroup",
                    GroupId = "CUST_GROUP",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustGroupItemPage", Path = "/:site/setup/masterinformation/custgroup/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustGroupPage", Path = "/:site/setup/masterinformation/custgroup/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTableItemPage", Path = "/:site/masterinformation/customertable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTableListPage", Path = "/:site/masterinformation/customertable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "DocumentType",
                    GroupId = "DOCUMENT_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentTypeItemPage", Path = "/:site/setup/masterinformation/documenttype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentTypeListPage", Path = "/:site/setup/masterinformation/documenttype/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GradeClassification",
                    GroupId = "GRADE_CLASSIFICATION",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GradeClassificationItemPage", Path = "/:site/setup/masterinformation/gradeclassification/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GradeClassificationListPage", Path = "/:site/setup/masterinformation/gradeclassification/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "KYCSetup",
                    GroupId = "KYC_SETUP",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "KYCSetupItemPage", Path = "/:site/setup/masterinformation/kycsetup/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "KYCSetupListPage", Path = "/:site/setup/masterinformation/kycsetup/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Nationality",
                    GroupId = "NATIONALITY",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NationalityItemPage", Path = "/:site/setup/masterinformation/nationality/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NationalityListPage", Path = "/:site/setup/masterinformation/nationality/" }
                    }
                },


                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Bank",
                    GroupId = "MASTER_INFORMATION_BANK",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BankItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/bank/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BankListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/bank" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "RegistrationType",
                    GroupId = "REGISTRATION_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RegistrationTypeItemPage", Path = "/:site/setup/masterinformation/registrationtype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RegistrationTypeListPage", Path = "/:site/setup/masterinformation/registrationtype/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Race",
                    GroupId = "RACE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RaceItemPage", Path = "/:site/setup/masterinformation/race/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RegistrationTypeListPage", Path = "/:site/setup/masterinformation/race/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "RelatedPersonTable",
                    GroupId = "RELATED_PERSON_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RelatedPersonTableItemPage", Path = "/:site/masterinformation/relatedpersontable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RelatedPersonTableListPage", Path = "/:site/masterinformation/relatedpersontable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "VerificationType",
                    GroupId = "VERIFICATION_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationTypeItemPage", Path = "/:site/setup/organization/verificationtype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationTypeListPage", Path = "/:site/setup/organization/verificationtype/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Department",
                    GroupId = "DEPARTMENT",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DepartmentItemPage", Path = "/:site/setup/organization/department/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DepartmentListPage", Path = "/:site/setup/organization/department/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Intercompany",
                    GroupId = "INTERCOMPANY",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntercompanyItemPage", Path = "/:site/setup/organization/intercompany/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntercompanyListPage", Path = "/:site/setup/organization/intercompany/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AuthorizedPersonType",
                    GroupId = "AUTHORIZED_PERSON_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTypeItemPage", Path = "/:site/setup/masterinformation/authorizedpersontype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTypeListPage", Path = "/:site/setup/masterinformation/authorizedpersontype/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "VendorTable",
                    GroupId = "VENDOR_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VendorTableItemPage", Path = "/:site/masterinformation/vendortable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VendorTableListPage", Path = "/:site/masterinformation/vendortable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BankGroup",
                    GroupId = "FINANCIAL_INSTITUTION",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BankGroupItemPage", Path = "/:site/setup/invoiceandcollection/financialinstitution/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BankGroupPage", Path = "/:site/setup/invoiceandcollection/financialinstitution/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BankType",
                    GroupId = "BANK_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BankTypeItemPage", Path = "/:site/setup/masterinformation/banktype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BankTypeListPage", Path = "/:site/setup/masterinformation/banktype/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditType",
                    GroupId = "CREDIT_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditTypeItemPage", Path = "/:site/setup/creditapplication/credittype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditTypeListPage", Path = "/:site/setup/creditapplication/credittype/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralStatus",
                    GroupId = "BUSINESS_COLLATERAL_STATUS",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralStatusItemPage", Path = "/:site/setup/masterinformation/businesscollateralstatus/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralStatusListPage", Path = "/:site/setup/masterinformation/businesscollateralstatus/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "PropertyType",
                    GroupId = "PROPERTY_TYPE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PropertyTypeItemPage", Path = "/:site/setup/masterinformation/propertytype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PropertyTypeListPage", Path = "/:site/setup/masterinformation/propertytype/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "VendGroup",
                    GroupId = "VEND_GROUP",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VendGroupItemPage", Path = "/:site/setup/masterinformation/vendgroup/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VendGroupListPage", Path = "/:site/setup/masterinformation/vendgroup/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_GUARANTOR_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorTransItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/guarantortrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorTransListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/guarantortrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Document",
                    GroupId = "DOCUMENT",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentProcessItemPage", Path = "/:site/setup/organization/documentprocess/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentProcessListPage", Path = "/:site/setup/organization/documentprocess/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentStatusItemPage", Path = "/:site/setup/organization/documentprocess/:id/documentstatus-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "DocumentProcessItemPage", FeatureId = "DocumentStatusListPage", Path = "/:site/setup/organization/documentprocess/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorType",
                    GroupId = "GUARANTOR_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorTypeItemPage", Path = "/:site/setup/masterinformation/guarantortype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorTypeListPage", Path = "/:site/setup/masterinformation/guarantortype/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralType",
                    GroupId = "BUSINESS_COLLATERAL_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralTypeItemPage", Path = "/:site/setup/masterinformation/businesscollateraltype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralTypeListPage", Path = "/:site/setup/masterinformation/businesscollateraltype/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralSubTypeItemPage", Path = "/:site/setup/masterinformation/businesscollateraltype/:id/businesscollateralsubtype-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "BusinessCollateralTypeItemPage", FeatureId = "BusinessCollateralSubTypeListPage", Path = "/:site/setup/masterinformation/businesscollateraltype/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_ADDRESS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressTransItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/addresstrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressTransListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/addresstrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AddressDistrict",
                    GroupId = "ADDRESS_DISTRICT",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressDistrictItemPage", Path = "/:site/setup/organization/addressdistrict/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressDistrictListPage", Path = "/:site/setup/organization/addressdistrict/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Demo",
                    GroupId = "DEMO",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DemoItemPage", Path = "/:site/demo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DemoListPage", Path = "/:site/demo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AddressCountry",
                    GroupId = "ADDRESS_COUNTRY",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressCountryItemPage", Path = "/:site/setup/organization/addresscountry/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressCountryListPage", Path = "/:site/setup/organization/addresscountry/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AddressProvince",
                    GroupId = "ADDRESS_PROVINCE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressProvinceItemPage", Path = "/:site/setup/organization/addressprovince/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressProvinceListPage", Path = "/:site/setup/organization/addressprovince/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Currency",
                    GroupId = "CURRENCY",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CurrencyItemPage",
                            Path = "/:site/setup/organization/currency/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CurrencyListPage",
                            Path = "/:site/setup/organization/currency/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CurrencyItemPage",
                            FeatureId = "ExchangeRateListPage",
                            Path = "/:site/setup/organization/currency/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExchangeRateItemPage",
                            Path = "/:site/setup/organization/currency/:id/exchangerate-child/:id"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_CUST_VISITING_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustVisitingTransItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/custvisitingtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustVisitingTransListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/custvisitingtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "RelatedpersonTable",
                    GroupId = "RELATED_PERSON_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/masterinformation/relatedpersontable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/masterinformation/relatedpersontable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_CONTACT_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactPersonItemPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/contactpersontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactPersonListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/contactpersontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MaritalStatus",
                    GroupId = "MARITAL_STATUS",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MaritalStatusItemPage", Path = "/:site/setup/masterinformation/maritalstatus/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MaritalStatusListPage", Path = "/:site/setup/masterinformation/maritalstatus/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_OWNER_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "OwnerTransItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/ownertrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "OwnerTransListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/ownertrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Gender",
                    GroupId = "GENDER",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenderItemPage", Path = "/:site/setup/masterinformation/gender/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenderListPage", Path = "/:site/setup/masterinformation/gender/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Occupation",
                    GroupId = "OCCUPATION",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "OccupationItemPage", Path = "/:site/setup/masterinformation/occupation/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "OccupationListPage", Path = "/:site/setup/masterinformation/occupation/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AddressPostalCode",
                    GroupId = "ADDRESS_POSTAL_CODE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressPostalCodeItemPage", Path = "/:site/setup/organization/addresspostalcode/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressPostalCodeListPage", Path = "/:site/setup/organization/addresspostalcode/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransItemPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/authorizedpersontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/authorizedpersontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_CONTACT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactItemPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/contacttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/contacttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AddressSubDistrict",
                    GroupId = "ADDRESS_SUB_DISTRICT",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressSubDistrictItemPage", Path = "/:site/setup/organization/addresssubdistrict/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressSubDistrictListPage", Path = "/:site/setup/organization/addresssubdistrict/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "TaxTable",
                    GroupId = "TAX_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxTableItemPage", Path = "/:site/setup/organization/taxtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxTableListPage", Path = "/:site/setup/organization/taxtable/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxTableItemPage", FeatureId = "TaxValueListPage", Path = "/:site/setup/organization/taxtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxValueItemPage", Path = "/:site/setup/organization/taxtable/:id/taxvalue-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_BUYER_CREDIT_LIMIT_BY_PRODUCT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerCreditLimitByProductItemPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/buyercreditlimitbyproduct/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerCreditLimitByProductListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/buyercreditlimitbyproduct/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "LineOfBusiness",
                    GroupId = "LINE_OF_BUSINESS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LineOfBusinessItemPage", Path = "/:site/setup/masterinformation/lineofbusiness/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LineOfBusinessListPage", Path = "/:site/setup/masterinformation/lineofbusiness/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "WithholdingTaxGroup",
                    GroupId = "WITHHOLDING_TAX_GROUP",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "WithholdingTaxGroupItemPage", Path = "/:site/setup/organization/withholdingtaxgroup/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "WithholdingTaxGroupListPage", Path = "/:site/setup/organization/withholdingtaxgroup/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "IntroducedBy",
                    GroupId = "INTRODUCED_BY",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntroducedItemPage", Path = "/:site/setup/masterinformation/introducedby/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntroducedByListPage", Path = "/:site/setup/masterinformation/introducedby/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/authorizedpersontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/authorizedpersontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_CONTACT_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactPersonItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/contactpersontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactPersonListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/contactpersontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_CONTACT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/contacttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/contacttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Territory",
                    GroupId = "TERRITORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TerritoryItemPage", Path = "/:site/setup/organization/territory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TerritoryListPage", Path = "/:site/setup/organization/territory/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_BUYER_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerAgreementItemPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/buyeragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerAgreementListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/buyeragreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = "BuyerAgreementItemPage", FeatureId = "BuyerAgreementLineListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/buyeragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerAgreementLineItemPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/buyeragreementtable/:id/buyeragreementline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_PROJECT_REFERENCE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProjectReferenceItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/projectreferencetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProjectReferenceListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/projectreferencetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "DocumentReason",
                    GroupId = "DOCUMENT_REASON",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentReasonItemPage", Path = "/:site/setup/organization/documentreason/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentReasonListPage", Path = "/:site/setup/organization/documentreason/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "NcbAccountStatus",
                    GroupId = "NCB_ACCOUNT_STATUS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NCBAccountStatusItemPage", Path = "/:site/setup/masterinformation/ncbaccountstatus/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NCBAccountStatusListPage", Path = "/:site/setup/masterinformation/ncbaccountstatus/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_CUSTOMER_CREDIT_LIMIT_BY_PRODUCT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerCreditLimitByProductItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/customercreditlimitbyproduct/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerCreditLimitByProductListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/customercreditlimitbyproduct/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "EmployeeTable",
                    GroupId = "EMPLOYEE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "EmployeeTableItemPage", Path = "/:site/organization/employeetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "EmployeeTableListPage", Path = "/:site/organization/employeetable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "EmplTeam",
                    GroupId = "EMPL_TEAM",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "EmplTeamItemPage", Path = "/:site/setup/organization/emplteam/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "EmplTeamListPage", Path = "/:site/setup/organization/emplteam/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "RetentionConditionSetup",
                    GroupId = "RETENTION_CONDITION_SETUP",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionConditionSetupItemPage", Path = "/:site/setup/creditapplication/retentionconditionsetup/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionConditionSetupListPage", Path = "/:site/setup/creditapplication/retentionconditionsetup/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_ADDRESS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressTransItemPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/addresstrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressTransListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/addresstrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "VendorTable",
                    GroupId = "VENDOR_TABLE_CONTACT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactItemPage", Path = "/:site/masterinformation/vendortable/:id/relatedinfo/contacttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactListPage", Path = "/:site/masterinformation/vendortable/:id/relatedinfo/contacttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MethodOfPayment",
                    GroupId = "METHOD_OF_PAYMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MethodOfPaymentItemPage", Path = "/:site/setup/creditapplication/methodofpayment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MethodOfPaymentListPage", Path = "/:site/setup/creditapplication/methodofpayment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "RelatedPersonTable",
                    GroupId = "RELATED_PERSON_TABLE_ADDRESS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressTransItemPage", Path = "/:site/masterinformation/relatedpersontable/:id/relatedinfo/addresstrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressTransListPage", Path = "/:site/masterinformation/relatedpersontable/:id/relatedinfo/addresstrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Ownership",
                    GroupId = "OWNERSHIP",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "OwnershipItemPage", Path = "/:site/setup/masterinformation/ownership/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "OwnershipListPage", Path = "/:site/setup/masterinformation/ownership/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ParentCompany",
                    GroupId = "PARENT_COMPANY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ParentCompanyItemPage", Path = "/:site/setup/masterinformation/parentcompany/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ParentCompanyListPage", Path = "/:site/setup/masterinformation/parentcompany/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BookmarkDocument",
                    GroupId = "BOOKMARK_DOCUMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/setup/organization/bookmarkdocument/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/setup/organization/bookmarkdocument/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentConditionInfoItemPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/documentconditiontrans/" },
                        new SysFeatureTableView { ParentFeatureId = "DocumentConditionInfoItemPage", FeatureId = "BillingDocumentConditionTransListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/documentconditiontrans/" },
                        new SysFeatureTableView { ParentFeatureId = "DocumentConditionInfoItemPage", FeatureId = "ReceiptDocumentConditionTransListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/documentconditiontrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BillingDocumentConditionTransItemPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptDocumentConditionTransItemPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "NumberSeqTable",
                    GroupId = "NUMBER_SEQ_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NumberSeqTableItemPage", Path = "/:site/setup/organization/numberseqtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NumberSeqTableListPage", Path = "/:site/setup/organization/numberseqtable/" },
                        new SysFeatureTableView { ParentFeatureId = "NumberSeqTableItemPage", FeatureId = "NumberSeqSegmentListPage", Path = "/:site/setup/organization/numberseqtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NumberSeqSegmentItemPage", Path = "/:site/setup/organization/numberseqtable/:id/numberseqsegment-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ConsortiumTable",
                    GroupId = "CONSORTIUM_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTableItemPage", Path = "/:site/masterinformation/consortiumtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTableListPage", Path = "/:site/masterinformation/consortiumtable/" },
                        new SysFeatureTableView { ParentFeatureId = "ConsortiumTableItemPage", FeatureId = "ConsortiumLineListPage", Path = "/:site/masterinformation/consortiumtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumLineItemPage", Path = "/:site/masterinformation/consortiumtable/:id/consortiumline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/financialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/financialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "WithholdingTaxTable",
                    GroupId = "WITHHOLDING_TAX_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "WithholdingTaxTableItemPage", Path = "/:site/setup/organization/withholdingtaxtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "WithholdingTaxTableListPage", Path = "/:site/setup/organization/withholdingtaxtable/" },
                        new SysFeatureTableView { ParentFeatureId = "WithholdingTaxTableItemPage", FeatureId = "WithholdingTaxValueListPage", Path = "/:site/setup/organization/withholdingtaxtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "WithholdingTaxValueItemPage", Path = "/:site/setup/organization/withholdingtaxtable/:id/withholdingtaxvalue-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/taxreporttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/taxreporttrans/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ServiceFeeCondTemplateTable",
                    GroupId = "SERVICE_FEE_COND_TEMPLATE_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeCondTemplateTableItemPage", Path = "/:site/setup/creditapplication/servicefeecondtemplatetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeCondTemplateTableListPage", Path = "/:site/setup/creditapplication/servicefeecondtemplatetable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeCondTemplateLineItemPage", Path = "/:site/setup/creditapplication/servicefeecondtemplatetable/:id/servicefeecondtemplateline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "ServiceFeeCondTemplateTableItemPage", FeatureId = "ServiceFeeCondTemplateLineListPage", Path = "/:site/setup/creditapplication/servicefeecondtemplatetable/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Company",
                    GroupId = "COMPANY",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CompanyItemPage", Path = "/:site/organization/company/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CompanyListPage", Path = "/:site/organization/company/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_CUST_BANK",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustBankItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/custbank/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustBankListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/custbank/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "DocumentConditionTemplateTable",
                    GroupId = "DOCUMENT_CONDITION_TEMPLATE_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentConditionTemplateTableItemPage", Path = "/:site/setup/creditapplication/documentconditiontemplatetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentConditionTemplateTableListPage", Path = "/:site/setup/creditapplication/documentconditiontemplatetable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentConditionTemplateLineItemPage", Path = "/:site/setup/creditapplication/documentconditiontemplatetable/:id/documentconditiontemplateline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "DocumentConditionTemplateTableItemPage", FeatureId = "DocumentConditionTemplateLineListComponent", Path = "/:site/setup/creditapplication/documentconditiontemplatetable/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Company",
                    GroupId = "COMPANY_COMPANY_BANK",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CompanyBankItemPage", Path = "/:site/organization/company/:id/relatedinfo/companybank/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CompanyBankListPage", Path = "/:site/organization/company/:id/relatedinfo/companybank/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Company",
                    GroupId = "COMPANY_COMPANY_BRANCH",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BranchItemPage", Path = "/:site/organization/company/:id/relatedinfo/branch/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BranchListPage", Path = "/:site/organization/company/:id/relatedinfo/branch/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Company",
                    GroupId = "COMPANY_COMPANY_BRANCH_CONTACT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactItemPage", Path = "/:site/organization/company/:id/relatedinfo/branch/:id/relatedinfo/contacttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactListPage", Path = "/:site/organization/company/:id/relatedinfo/branch/:id/relatedinfo/contacttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Company",
                    GroupId = "COMPANY_COMPANY_BRANCH_ADDRESS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressTransItemPage", Path = "/:site/organization/company/:id/relatedinfo/branch/:id/relatedinfo/addresstrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressTransListPage", Path = "/:site/organization/company/:id/relatedinfo/branch/:id/relatedinfo/addresstrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "VendorTable",
                    GroupId = "VENDOR_TABLE_VEND_BANK",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VendBankItemPage", Path = "/:site/masterinformation/vendortable/:id/relatedinfo/vendbank/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VendListPage", Path = "/:site/masterinformation/vendortable/:id/relatedinfo/vendbank/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessUnit",
                    GroupId = "BUSINESS_UNIT",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BusinessUnitItemPage",
                            Path = "/:site/setup/organization/businessunit/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BusinessUnitListPage",
                            Path = "/:site/setup/organization/businessunit/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ExposureGroup",
                    GroupId = "EXPOSURE_GROUP",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExposureGroupItemPage", Path = "/:site/setup/masterinformation/exposuregroup/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExposureGroupListPage", Path = "/:site/setup/masterinformation/exposuregroup/" },
                        new SysFeatureTableView { ParentFeatureId = "ExposureGroupItemPage", FeatureId = "ExposureGroupByProductListPage", Path = "/:site/setup/masterinformation/exposuregroup/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExposureGroupByProductItemPage", Path = "/:site/setup/masterinformation/exposuregroup/:id/exposuregroupbyproduct-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "EmployeeTable",
                    GroupId = "EMPLOYEE_TABLE_CONTACT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactItemPage", Path = "/:site/organization/employeetable/:id/relatedinfo/contacttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ContactListPage", Path = "/:site/organization/employeetable/:id/relatedinfo/contacttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "NumberSeqParameter",
                    GroupId = "NUMBER_SEQ_PARAMETER",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NumberSeqParameterItemPage", Path = "/:site/setup/organization/numberseqparameter/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NumberSeqParameterListPage", Path = "/:site/setup/organization/numberseqparameter/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InvoiceRevenueType",
                    GroupId = "INVOICE_REVENUE_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceRevenueTypeItemPage", Path = "/:site/setup/invoiceandcollection/invoicerevenuetype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceRevenueTypeListPage", Path = "/:site/setup/invoiceandcollection/invoicerevenuetype/" }
                    }
                },


                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "NumberSeqSetupByProductType",
                    GroupId = "NUMBER_SEQ_SETUP_BY_PRODUCT_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NumberSeqSetupByProductTypeItemPage", Path = "/:site/setup/organization/numberseqsetupbyproducttype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NumberSeqSetupByProductTypeListPage", Path = "/:site/setup/organization/numberseqsetupbyproducttype/" }
                    }
                },


                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "LedgerDimension",
                    GroupId = "LEDGER_DIMENSION",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LedgerDimensionItemPage", Path = "/:site/setup/accounting/ledgerdimension/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LedgerDimensionListPage", Path = "/:site/setup/accounting/ledgerdimension/" }
                    }
                },


                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CalendarGroup",
                    GroupId = "CALENDAR_GROUP",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CalendarGroupItemPage", Path = "/:site/setup/organization/calendargroup/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CalendarGroupListPage", Path = "/:site/setup/organization/calendargroup/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CalendarNonWorkingDate",
                    GroupId = "CALENDAR_NON_WORKING_DATE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CalendarNonWorkingDateItemPage", Path = "/:site/setup/organization/calendarnonworkingdate/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CalendarNonWorkingDateListPage", Path = "/:site/setup/organization/calendarnonworkingdate/" }
                    }
                },




                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_UPDATE_CUSTOMER_TABLE_STATUS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateCustomerTableStatusPage", Path = "/:site/masterinformation/customertable/:id/function/updatecustomertablestatus/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_UPDATE_CUSTOMER_TABLE_BLACKLIST_AND_WATCHLIST_STATUS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateCustomerTableBlacklistAndWatchlistStatusPage", Path = "/:site/masterinformation/customertable/:id/function/updatecustomertableblacklistandwatchliststatus/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_UPDATE_BUYER_TABLE_BLACKLIST_AND_WATCHLIST_STATUS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateBuyerTableBlacklistAndWatchlistStatusPage", Path = "/:site/masterinformation/buyertable/:id/function/updatebuyertableblacklistandwatchliststatus/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_DOCUMENT_CONDITION_TRANS_COPY_DOCUMENT_CONDITION_TEMPLATE_BILLING",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyDocumentConditionTemplateBillingPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/documentconditiontrans/function/copydocumentconditiontemplatebilling/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_DOCUMENT_CONDITION_TRANS_COPY_DOCUMENT_CONDITION_TEMPLATE_RECEIPT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyDocumentConditionTemplateReceiptPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/documentconditiontrans/function/copydocumentconditiontemplatereceipt/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "DocumentTemplateTable",
                    GroupId = "DOCUMENT_TEMPLATE_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentTemplateTableItemPage", Path = "/:site/setup/organization/documenttemplatetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentTemplateTableListPage", Path = "/:site/setup/organization/documenttemplatetable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CompanySignature",
                    GroupId = "COMPANY_SIGNATURE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CompanySignatureItemPage", Path = "/:site/setup/organization/companysignature/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CompanySignatureListPage", Path = "/:site/setup/organization/companysignature/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CompanyParameter",
                    GroupId = "COMPANY_PARAMETER",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CompanyParameterItemPage", Path = "/:site/setup/organization/companyparameter/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CompanyParameterListPage", Path = "/:site/setup/organization/companyparameter/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/businesscollateral/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/businesscollateral/" }
                    }
                },


                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InvoiceNumberSeqSetup",
                    GroupId = "INVOICE_NUMBER_SEQ_SETUP",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceNumberSeqSetupItemPage", Path = "/:site/setup/organization/invoicenumberseqsetup/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceNumberSeqSetupListPage", Path = "/:site/setup/organization/invoicenumberseqsetup/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BookmarkDocumentTemplateTable",
                    GroupId = "BOOKMARK_DOCUMENT_TEMPLATE_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentTemplateTableItemPage", Path = "/:site/setup/organization/bookmarkdocumenttemplatetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentTemplateTableListPage", Path = "/:site/setup/organization/bookmarkdocumenttemplatetable/" },
                        new SysFeatureTableView { ParentFeatureId = "BookmarkDocumentTemplateTableItemPage", FeatureId = "BookmarkDocumentTemplateLineListPage", Path = "/:site/setup/organization/bookmarkdocumenttemplatetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentTemplateLineItemPage", Path = "/:site/setup/organization/bookmarkdocumenttemplatetable/:id/bookmarkdocumenttemplateline-child/:id" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "LedgerFiscalYear",
                    GroupId = "LEDGER_FISCAL_YEAR",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LedgerFiscalYearItemPage", Path = "/:site/setup/accounting/ledgerfiscalyear/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LedgerFiscalYearListPage", Path = "/:site/setup/accounting/ledgerfiscalyear/" },
                        new SysFeatureTableView { ParentFeatureId = "LedgerFiscalYearItemPage", FeatureId = "LedgerFiscalPeriodListPage", Path = "/:site/setup/accounting/ledgerfiscalyear/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LedgerFiscalPeriodItemPage", Path = "/:site/setup/accounting/ledgerfiscalyear/:id/ledgerfiscalperiod-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ProdUnit",
                    GroupId = "PRODUCT_UNIT",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProdUnitItemPage", Path = "/:site/setup/masterinformation/productunit/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProdUnitListPage", Path = "/:site/setup/masterinformation/productunit/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MessengerJobTableItemPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MessengerJobTableListPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/" },
                    }
                },
                
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InvoiceType",
                    GroupId = "INVOICE_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTypeItemPage", Path = "/:site/setup/invoiceandcollection/invoicetype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTypeListPage", Path = "/:site/setup/invoiceandcollection/invoicetype/" }

                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "SysUser",
                    GroupId = "SYSUSERTABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "SysUserTableListPage",
                            Path = "/:site/admin/sysusertable/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "SysUserTableItemPage",
                            Path = "/:site/admin/sysusertable/:id"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "SysAccessRight",
                    GroupId = "SYSROLETABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "SysRoleTableListPage",
                            Path = "/:site/admin/sysroletable/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "SysRoleTableItemPage",
                            Path = "/:site/admin/sysroletable/:id"
                        }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ChequeTable",
                    GroupId = "CHEQUE_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ChequeTableItemPage", Path = "/:site/invoiceandcollection/chequetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ChequeTableListPage", Path = "/:site/invoiceandcollection/chequetable/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "JobType",
                    GroupId = "JOB_TYPE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JobTypeItemPage", Path = "/:site/setup/invoiceandcollection/jobtype/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JobTypeListPage", Path = "/:site/setup/invoiceandcollection/jobtype/" }

                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/attachment/" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/attachment/:id" },
                    }
                },
                
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/custtransaction/" }
                    }

                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InvoiceTable",
                    GroupId = "INVOICE_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/invoiceandcollection/inquiry/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/invoiceandcollection/inquiry/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/invoiceandcollection/inquiry/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/invoiceandcollection/inquiry/invoicetable/:id/invoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_BUYER_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerTransactionItemPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/buyertransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerTransactionListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/buyertransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InvoiceTable",
                    GroupId = "INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceItemPage", Path = "/:site/invoiceandcollection/inquiry/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceListmPage", Path = "/:site/invoiceandcollection/inquiry/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/invoiceandcollection/inquiry/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/invoiceandcollection/inquiry/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },



                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InquiryPurchaseLineOutstandingItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InquiryPurchaseLineOutstandingListPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_COLLECTION_FOLLOW_UP",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CollectionFollowUpItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/collectionfollowup/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CollectionFollowUpListPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/collectionfollowup/" }
                    }
                },


                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTable",
                    GroupId = "RECEIPT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTableItemPage", Path = "/:site/invoiceandcollection/inquiry/receipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTableListPage", Path = "/:site/invoiceandcollection/inquiry/receipttable/" },
                        new SysFeatureTableView { ParentFeatureId = "ReceiptTableItemPage", FeatureId = "ReceiptLineListPage", Path = "/:site/invoiceandcollection/inquiry/receipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptLineLineItemPage", Path = "/:site/invoiceandcollection/inquiry/receipttable/:id/receiptline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendLineItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementAffiliateItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendLineItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendLineItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementAffiliateItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendLineItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendLineItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendLineItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendLineItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendLineItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendLineItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendLineItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/leasing/newmainagreementtable/allmainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendLineItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/guarantoragreementline-child/:id" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerTable",
                    GroupId = "MESSENGER_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MessengerTableItemPage", Path = "/:site/masterinformation/messengertable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MessengerTableListPage", Path = "/:site/masterinformation/messengertable/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InvoiceTable",
                    GroupId = "INVOICE_TABLE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/invoiceandcollection/inquiry/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/invoiceandcollection/inquiry/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InquiryWithdrawalLineOutstandItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InquiryWithdrawalLineOutstandListPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_COLLECTION_FOLLOW_UP",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CollectionFollowUpItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/collectionfollowup/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CollectionFollowUpListPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/collectionfollowup/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_COLLECTION_FOLLOW_UP_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/collectionfollowup/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/collectionfollowup/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView 
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage",Path = "/:site/masterinformation/customertable/:id/relatedinfo/retentiontransaction/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage",Path = "/:site/masterinformation/customertable/:id/relatedinfo/retentiontransaction/"}
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "PaymentHistory",
                    GroupId = "PAYMENT_HISTORY",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage",Path = "/:site/invoiceandcollection/inquiry/paymenthistory/"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage",Path = "/:site/invoiceandcollection/inquiry/paymenthistory/:id"},
                        }
                },
                new SysFeatureGroupControllerMappingView
                     {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_PDC",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                            {
                                new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PdcItemPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/pdc/:id"},
                                new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PdcListPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/pdc/"}
                            }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_COLLECTION_FOLLOW_UP_BUYER_RECEIPT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerReceiptTableItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/collectionfollowup/:id/relatedinfo/buyerreceipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerReceiptTableListPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/collectionfollowup/:id/relatedinfo/buyerreceipttable/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InvoiceTable",
                    GroupId = "INVOICE_TABLE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/invoiceandcollection/inquiry/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/invoiceandcollection/inquiry/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE_JOB_CHEQUE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ChequeItemPage",Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/jobchequetable/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ChequeListPage",Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/jobchequetable/"},
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_PDC",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PdcItemPage",Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/pdc/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PdcListPage",Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/pdc/"}
                    }
                },

                  new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MessengerRequestItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MessengerRequestListPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_COLLECTION_FOLLOW_UP_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/collectionfollowup/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/collectionfollowup/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_COLLECTION_FOLLOW_UP_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/collectionfollowup/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/collectionfollowup/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_PURCHASE_LINE_MESSENGER_REQUEST",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PurchaseLineMessengerRequestItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PurchaseLineMessengerRequestListPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/" }
                    }
                },

                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_COLLECTION_FOLLOW_UP_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                            {
                                new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/collectionfollowup/:id/relatedinfo/memotrans/:id"},
                                new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/collectionfollowup/:id/relatedinfo/memotrans/"}
                            }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE_CANCEL_MESSENGER_JOB_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/function/cancelmessengerjobtable/" },

                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE_POST_MESSENGER_JOB_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/function/postmessengerjobtable/" },

                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE_ASSIGN_MESSENGER_JOB_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/function/assignmessengerjobtable/" },

                    }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE",
                    FeatureType = FeatureType.PRIMARY,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReviewActiveCreditAppLineItemPage",Path = "/:site/factoring/reviewactivecreditappline/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReviewActiveCreditAppLineListPage",Path = "/:site/factoring/reviewactivecreditappline/"},
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE_REVIEW_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReviewCreditAppRequestListPage",Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReviewCreditAppRequestItemPage",Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id"},
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE_REVIEW_CREDIT_APP_REQUEST_UPDATE_REVIEW_RESULT",
                    FeatureType = FeatureType.FUNCTION,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateReviewResultPage",Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/function/updatereviewresult/"},
                        }
                    },

                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage",Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage",Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/"},
                    }
                },

                new SysFeatureGroupControllerMappingView {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE_REVIEW_CREDIT_APP_REQUEST_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage",Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/relatedinfo/financialstatementtrans/"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage",Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/relatedinfo/financialstatementtrans/:id"},
                    }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE_REVIEW_CREDIT_APP_REQUEST_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage",Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage",Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/relatedinfo/bookmarkdocumenttrans/"},
                    }
                },
                  new SysFeatureGroupControllerMappingView {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE_REVIEW_CREDIT_APP_REQUEST_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestMemoListPage",Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/relatedinfo/memotrans/"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestMemoItemPage",Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/relatedinfo/memotrans/:id"},
                        }
                    },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/attachment/" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentItemPage",Path = "/:site/masterinformation/buyertable/:id/relatedinfo/attachment/:id"},
                    }
                },
                
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_RETENTION_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingItemPage",Path = "/:site/masterinformation/customertable/:id/relatedinfo/retentionoutstanding/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingListPage",Path = "/:site/masterinformation/customertable/:id/relatedinfo/retentionoutstanding/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_INQUIRY_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InquiryAssignmentOutstandingListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/inquiryassignmentagreementoutstanding/" },
                    }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_INQUIRY_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InquiryAssignmentOutstandingListPage",Path = "/:site/masterinformation/customertable/:id/relatedinfo/inquiryassignmentagreementoutstanding/"}
                        }
                },
                
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE_JOB_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage",Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/attachment/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage",Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/attachment/"},
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE_REVIEW_CREDIT_APP_REQUEST_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage",Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/relatedinfo/attachment/"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage",Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/relatedinfo/attachment/:id"},
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "VendorTable",
                    GroupId = "VENDOR_TABLE_ADDRESS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressTransItemPage", Path = "/:site/masterinformation/vendortable/:id/relatedinfo/addresstrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddressTransListPage", Path = "/:site/masterinformation/vendortable/:id/relatedinfo/addresstrans/" }
                    }
                },
                
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempTableItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempTableListPage", Path = "/:site/invoiceandcollection/receipttemptable/" },
                        new SysFeatureTableView { ParentFeatureId = "ReceiptTempTableItemPage", FeatureId = "ReceiptTempPaymDetailListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempPaymDetailItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/receipttemppaymdetail-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MigrationTable",
                    GroupId = "MIGRATION_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MigrationTableItemPage", Path = "/:site/framework/migrationtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MigrationTableListPage", Path = "/:site/framework/migrationtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MigrationLogTable",
                    GroupId = "MIGRATION_LOG_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MigrationLogTablePage", Path = "/:site/framework/migrationlogtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MigrationTable",
                    GroupId = "GENERATE_DUMMY_RECORD",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenerateDummyRecordPage", Path = "/:site/function/generatedummyrecord/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_INVOICE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/invoicesettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/invoicesettlement/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_INVOICE_SETTLEMENT_PRODUCT_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSettledTransListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/invoicesettlement/:id/relatedinfo/productsettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSettledTransItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/invoicesettlement/:id/relatedinfo/productsettlement/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_PAYM_DETAIL-CHILD_SUSPENSE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SuspenseSettlementListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/receipttemppaymdetail-child/:id/relatedinfo/suspensesettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SuspenseSettlementItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/receipttemppaymdetail-child/:id/relatedinfo/suspensesettlement/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_RECEIPT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTableListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/receipttable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTableItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/receipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = "ReceiptTableItemPage", FeatureId = "ReceiptLineListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/receipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptLineLineItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/receipttable/:id/receiptline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_CANCEL_RECEIPT_TEMP",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelReceiptTempPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/function/cancelreceipttemp/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_POST_RECEIPT_TEMP",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostReceiptTempPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/function/postreceipttemp/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "SUSPENSE_REFUND",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerRefundTableItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerRefundTableListPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/" },
                        new SysFeatureTableView { ParentFeatureId = "CustomerRefundTableItemPage", FeatureId = "SuspenseSettlementListPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SuspenseSettlementItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/suspensesettlement-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "SUSPENSE_REFUND_INVOICE_SETTLEMENT",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementListPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/invoicesettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/invoicesettlement/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "buyerTable",
                    GroupId = "BUYER_TABLE_INQUIRY_BUYER_CREDIT_OF_ALL_CUSTOMER",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "inquiryCreditOutstandingByCustListPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/inquirybuyercreditofallcustomer/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerRefundTableItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerRefundTableListPage", Path = "/:site/invoiceandcollection/refund/reserverefund/" },
                        new SysFeatureTableView { ParentFeatureId = "CustomerRefundTableItemPage", FeatureId = "SuspenseSettlementListPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SuspenseSettlementItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/suspensesettlement-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerRefundTableItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerRefundTableListPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/" },
                        new SysFeatureTableView { ParentFeatureId = "CustomerRefundTableItemPage", FeatureId = "SuspenseSettlementListPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SuspenseSettlementItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/suspensesettlement-child/:id" },
                      }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE_REVIEW_CREDIT_APP_REQUEST_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage",Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/memotrans/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage",Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/memotrans/"}
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/attachment/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/attachment/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTable",
                    GroupId = "RECEIPT_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceItemPage", Path = "/:site/invoiceandcollection/inquiry/receipttable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceListPage", Path = "/:site/invoiceandcollection/inquiry/receipttable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/invoiceandcollection/inquiry/receipttable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/invoiceandcollection/inquiry/receipttable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" }
                    }
                },
                
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_INVOICE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementListPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/invoicesettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/invoicesettlement/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_INVOICE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementListPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/invoicesettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/invoicesettlement/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_INVOICE_SETTLEMENT_PRODUCT_SETTLEMENT_RECALCULATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RecalProductSettledTransPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/invoicesettlement/:id/relatedinfo/productsettlement/:id/function/recalculate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_COLLECTION_FOLLOW_UP_BUYER_RECEIPT_TABLE_CANCEL_BUYER_RECEIPT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelBuyerReceiptTablePage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/collectionfollowup/:id/relatedinfo/buyerreceipttable/:id/function/cancelbuyerreceipttable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/memotrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/memotrans/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/memotrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/memotrans/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "SUSPENSE_REFUND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/memotrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/memotrans/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_VENDOR_PAYMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "vendorpaymenttransListPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/vendorpaymenttrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "vendorpaymenttransItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/vendorpaymenttrans/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_VENDOR_PAYMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "vendorpaymenttransListPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/vendorpaymenttrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "vendorpaymenttransItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/vendorpaymenttrans/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "SUSPENSER_REFUND_VENDOR_PAYMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "vendorpaymenttransListPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/vendorpaymenttrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "vendorpaymenttransItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/vendorpaymenttrans/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "SUSPENSE_REFUND_CANCEL_CUSTOMER_REFUND",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerRefundTableItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/function/cancelcustomerrefund/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_CANCEL_CUSTOMER_REFUND",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerRefundTableItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/function/cancelcustomerrefund/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_CANCEL_CUSTOMER_REFUND",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerRefundTableItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/function/cancelcustomerrefund/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "SUSPENSE_REFUND_POST_CUSTOMER_REFUND",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostCustomerRefundTablePage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/function/postcustomerrefund/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_POST_CUSTOMER_REFUND",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostCustomerRefundTablePage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/function/postcustomerrefund/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_POST_CUSTOMER_REFUND",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostCustomerRefundTablePage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/function/postcustomerrefund/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_PAYMENT_DETAIL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "paymentdetailListPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/paymentdetail/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "paymentdetailItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/paymentdetail/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_PAYMENT_DETAIL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "paymentdetailListPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/paymentdetail/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "paymentdetailItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/paymentdetail/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "SUSPENSER_REFUND_PAYMENT_DETAIL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "paymentdetailListPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/paymentdetail/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "paymentdetailItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/paymentdetail/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage",Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage",Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/"},
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage",Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage",Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/"},
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "SUSPENSE_REFUND_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage",Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage",Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/"},
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "SUSPENSE_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage",Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage",Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/"},
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentListPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/attachment/" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/attachment/:id" },
                    }
                },
                   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentListPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/attachment/" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/attachment/:id" },
                    }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "SUSPENSE_REFUND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentListPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/attachment/" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/attachment/:id" },
                    }
                },
                      new SysFeatureGroupControllerMappingView {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE_REVIEW_CREDIT_APP_REQUEST_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage",Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/"},
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ChequeTable",
                    GroupId = "CHEQUE_TABLE_COMPLETE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CompletePage", Path = "/:site/invoiceandcollection/chequetable/:id/function/complete/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ChequeTable",
                    GroupId = "CHEQUE_TABLE_DEPOSIT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DepositPage", Path = "/:site/invoiceandcollection/chequetable/:id/function/deposit/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ChequeTable",
                    GroupId = "CHEQUE_TABLE_BOUNCE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BouncePage", Path = "/:site/invoiceandcollection/chequetable/:id/function/bounce/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ChequeTable",
                    GroupId = "CHEQUE_TABLE_REPLACE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReplacePage", Path = "/:site/invoiceandcollection/chequetable/:id/function/replace/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ChequeTable",
                    GroupId = "CHEQUE_TABLE_RETURN_CHEQUE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RepturnPage", Path = "/:site/invoiceandcollection/chequetable/:id/function/returncheque/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ChequeTable",
                    GroupId = "CHEQUE_TABLE_CANCEL_CHEQUE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelPage", Path = "/:site/invoiceandcollection/chequetable/:id/function/cancelcheque/" },
                    }
                },
                
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Demo",
                    GroupId = "DEMO_DEMO_REPORT_DEMO",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "DemoReportDemoPage",
                            Path = "/:site/demo/:id/report/demoreportdemo/"
                        }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "StagingTable",
                    GroupId = "STAGING_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "StagingTableItemPage", Path = "/:site/accounting/inquiry/stagingtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "StagingTablelistPage", Path = "/:site/accounting/inquiry/stagingtable/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "DocumentReturnMethod",
                    GroupId = "DOCUMENT_RETURN_METHOD",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentReturnMethodItemPage", Path = "/:site/setup/invoiceandcollection/documentreturnmethod/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentReturnMethodListPage", Path = "/:site/setup/invoiceandcollection/documentreturnmethod/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ProcessTrans",
                    GroupId = "PROCESS_TRANSACTION",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransItemPage", Path = "/:site/accounting/inquiry/processtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransListPage", Path = "/:site/accounting/inquiry/processtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE_PRINT_MESSENGER_JOB",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintMessengerJobPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/report/printmessengerjob/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "StagingTable",
                    GroupId = "STAGING_TABLE_VENDOR_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "StagingTableVendorInfoItemPage", Path = "/:site/accounting/inquiry/stagingtable/:id/relatedinfo/stagingtablevendorinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "StagingTableVendorInfoListPage", Path = "/:site/accounting/inquiry/stagingtable/:id/relatedinfo/stagingtablevendorinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "StagingTransText",
                    GroupId = "STAGING_TRANS_TEXT",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "StagingTransTextItemPage", Path = "/:site/setup/accounting/stagingtranstext/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "StagingTransTextListPage", Path = "/:site/setup/accounting/stagingtranstext/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "DocumentReturnTable",
                    GroupId = "DOCUMENT_RETURN_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentReturnTableItemPage", Path = "/:site/invoiceandcollection/documentreturntable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentReturnTableListPage", Path = "/:site/invoiceandcollection/documentreturntable/" },
                        new SysFeatureTableView { ParentFeatureId = "DocumentReturnTableItemPage", FeatureId = "DocumentReturnLineListPage", Path = "/:site/invoiceandcollection/documentreturntable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentReturnLineItemPage", Path = "/:site/invoiceandcollection/documentreturntable/:id/documentreturntable-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "DocumentReturnTable",
                    GroupId = "DOCUMENT_RETURN_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage",Path = "/:site/invoiceandcollection/documentreturntable/:id/relatedinfo/attachment/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage",Path = "/:site/invoiceandcollection/documentreturntable/:id/relatedinfo/attachment/"},
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "DocumentReturnTable",
                    GroupId = "DOCUMENT_RETURN_TABLE_POST_DOCUMENT_RETURN",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostDocumentReturnPage", Path = "/:site/invoiceandcollection/documentreturntable/:id/function/postdocumentreturn/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_TAX_REPORT_TRANS_IMPORT_TAX_REPORT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ImportTaxReportPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/taxreporttrans/function/importtaxreport/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerTable",
                    GroupId = "CUSTOMER_TABLE_FINANCIAL_STATEMENT_TRANS_IMPORT_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ImportFinancialStatementTransPage", Path = "/:site/masterinformation/customertable/:id/relatedinfo/financialstatementtrans/function/importfinancialstatementtrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BuyerTable",
                    GroupId = "BUYER_TABLE_FINANCIAL_STATEMENT_TRANS_IMPORT_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ImportFinancialStatementTransPage", Path = "/:site/masterinformation/buyertable/:id/relatedinfo/financialstatementtrans/function/importfinancialstatementtrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "DocumentReturnTable",
                    GroupId = "DOCUMENT_RETURN_TABLE_CLOSE_DOCUMENT_RETURN",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseDocumentReturnPage", Path = "/:site/invoiceandcollection/documentreturntable/:id/function/closedocumentreturn/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_PRINT_RECEIPT_TEMP",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintReceiptTempPage",
                            Path = "/:site/invoiceandcollection/receipttemptable/:id/report/printreceipttemp/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTable",
                    GroupId = "RECEIPT_TABLE_PRINT_RECEIPT_COPY",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintReceiptCopy",
                            Path = "/:site/invoiceandcollection/inquiry/receipttable/:id/report/printreceiptcopy/"
                        }
                    }
                },
               new SysFeatureGroupControllerMappingView
               {
                    ControllerName = "ReceiptTable",
                    GroupId = "RECEIPT_TABLE_PRINT_RECEIPT_ORIGINAL",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                             FeatureId = "PrintReceiptOriginal",
                            Path = "/:site/invoiceandcollection/inquiry/receipttable/:id/report/printreceiptoriginal/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_COLLECTION_FOLLOW_UP_BUYER_RECEIPT_TABLE_PRINT_BUYER_RECEIPT",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBuyerReceiptPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/collectionfollowup/:id/relatedinfo/buyerreceipttable/:id/report/printbuyerreceipt/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Accounting",
                    GroupId = "INCOME_REALIZATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenInterestRealizeProcessTransPage", Path = "/:site/accounting/periodic/function/incomerealization/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InvoiceTable",
                    GroupId = "INVOICE_TABLE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintTaxInvoiceCopy",
                            Path = "/:site/invoiceandcollection/inquiry/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InvoiceTable",
                    GroupId = "INVOICE_TABLE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintTaxInvoiceOriginal",
                            Path = "/:site/invoiceandcollection/inquiry/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_PRINT_CUSTOMER_REFUND_RESERVE",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintReserveRefundPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/report/printcustomerrefundreserve/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_PRINT_CUSTOMER_REFUND_RETENTION",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintRetentionRefundPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/report/printcustomerrefundretention/" },
                    }
                },


                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage",Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage",Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/"},
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage",Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage",Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id"},
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MessengerJobTable",
                    GroupId = "MESSENGER_JOB_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/invoiceandcollection/messenger/messengerjobtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },

                  
                  new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "IntercompanyInvoiceTable",
                    GroupId = "INTERCOMPANY_INVOICE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntercompanyItemPage", Path = "/:site/invoiceandcollection/intercompanyinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntercompanyListPage", Path = "/:site/invoiceandcollection/intercompanyinvoice/" }

                    }
                },


                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTable",
                    GroupId = "RECEIPT_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/invoiceandcollection/inquiry/receipttable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTable",
                    GroupId = "RECEIPT_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/invoiceandcollection/inquiry/receipttable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Accounting",
                    GroupId = "INTERFACE_ACCOUNTING_STAGING",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "InterfaceAccountingStagingPage",
                            Path = "/:site/accounting/interface/function/interfaceaccountingstaging/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Accounting",
                    GroupId = "INTERFACE_VENDOR_INVOICE_STAGING",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InterfaceVendorInvoiceStagingPage", Path = "/:site/accounting/interface/function/interfacevendorinvoicestaging/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Accounting",
                    GroupId = "GENERATE_ACCOUNTING_STAGING",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenerateAccountingStagingPage", Path = "/:site/accounting/periodic/function/generateaccountingstaging/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Accounting",
                    GroupId = "GENERATE_VENDOR_INVOICE_STAGING",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenerateVndorInvoiceStagingPage", Path = "/:site/accounting/periodic/function/generatevendorinvoicestaging/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "IntercompanyInvoiceTable",
                    GroupId = "INTERCOMPANY_INVOICE_INTERCOMPANY_INVOICE_ADJUSTMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntercompanyInvoiceAdjustmentItemPage", Path = "/:site/invoiceandcollection/intercompanyinvoice/:id/relatedinfo/intercompanyinvoiceadjustment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntercompanyInvoiceAdjustmentListPage", Path = "/:site/invoiceandcollection/intercompanyinvoice/:id/relatedinfo/intercompanyinvoiceadjustment/" }

                }

                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "IntercompanyInvoiceTable",
                    GroupId = "INTERCOMPANY_INVOICE_INTERCOMPANY_INVOICE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntercompanyInvoiceSettlementItemPage", Path = "/:site/invoiceandcollection/intercompanyinvoice/:id/relatedinfo/intercompanyinvoicesettlement/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntercompanyInvoiceSettlementListPage", Path = "/:site/invoiceandcollection/intercompanyinvoice/:id/relatedinfo/intercompanyinvoicesettlement/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "FreeTextInvoiceTable",
                    GroupId = "FREE_TEXT_INVOICE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FreeTextInvoiceTableItemPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FreeTextInvoiceTableListPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "IntercompanyInvoiceTable",
                    GroupId = "INTERCOMPANY_INVOICE_TRANSACTION_ADJUSTMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TransactionAdjustmentPage", Path = "/:site/invoiceandcollection/intercompanyinvoice/:id/function/transactionadjustment/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/cancelagreement/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/closeagreement/" },
                    }
                },







                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/postagreement/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/sendagreement/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/printsetbookdoctrans/" },
                    }
                },





                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/memotrans/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/Projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/Projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/Projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/Projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/attachment/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "GuarantorAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST_JOB_CHEQUE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ChequeItemPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/jobchequetable/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ChequeListPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/jobchequetable/"},
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/"},
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST_JOB_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/attachment/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/attachment/"},
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/"},
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage",Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id"},
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST_CANCEL_MESSENGER_JOB_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/function/cancelmessengerjobtable/" },

                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST_POST_MESSENGER_JOB_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/function/postmessengerjobtable/" },

                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST_ASSIGN_MESSENGER_JOB_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/function/assignmessengerjobtable/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryPurchaseLineOutstanding",
                    GroupId = "INQUIRY_PURCHASE_LINE_OUTSTANDING_MESSENGER_REQUEST_PRINT_MESSENGER_JOB",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintMessengerJobPage", Path = "/:site/invoiceandcollection/inquiry/inquirypurchaselineoutstanding/:id/relatedinfo/messengerrequest/:id/report/printmessengerjob/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_MESSENGER_REQUEST_JOB_CHEQUE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ChequeItemPage",Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/jobchequetable/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ChequeListPage",Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/jobchequetable/"},
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_MESSENGER_REQUEST_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage",Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage",Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/"},
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_MESSENGER_REQUEST_JOB_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage",Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/attachment/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage",Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/attachment/"},
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_MESSENGER_REQUEST_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage",Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage",Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/"},
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage",Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage",Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id"},
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_MESSENGER_REQUEST_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_MESSENGER_REQUEST_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_MESSENGER_REQUEST_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_MESSENGER_REQUEST_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_MESSENGER_REQUEST_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_MESSENGER_REQUEST_CANCEL_MESSENGER_JOB_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/function/cancelmessengerjobtable/" },

                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_MESSENGER_REQUEST_POST_MESSENGER_JOB_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/function/postmessengerjobtable/" },

                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_MESSENGER_REQUEST_ASSIGN_MESSENGER_JOB_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/function/assignmessengerjobtable/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_MESSENGER_REQUEST_PRINT_MESSENGER_JOB",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintMessengerJobPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/messengerrequest/:id/report/printmessengerjob/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RESERVE_REFUND_VENDOR_PAYMENT_TRANS_SEND_VENDOR_INVOICE_STAGING",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "SendVendorInvoiceStagingPage",
                            Path = "/:site/invoiceandcollection/refund/reserverefund/:id/relatedinfo/vendorpaymenttrans/function/sendvendorinvoicestaging/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "RETENTION_REFUND_VENDOR_PAYMENT_TRANS_SEND_VENDOR_INVOICE_STAGING",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "SendVendorInvoiceStagingPage",
                            Path = "/:site/invoiceandcollection/refund/retentionrefund/:id/relatedinfo/vendorpaymenttrans/function/sendvendorinvoicestaging/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CustomerRefundTable",
                    GroupId = "SUSPENSE_REFUND_VENDOR_PAYMENT_TRANS_SEND_VENDOR_INVOICE_STAGING",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "SendVendorInvoiceStagingPage",
                            Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/vendorpaymenttrans/function/sendvendorinvoicestaging/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "FreeTextInvoiceTable",
                    GroupId = "FREE_TEXT_INVOICE_ATTACHMENT",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "FreeTextInvoiceTable",
                    GroupId = "FREE_TEXT_INVOICE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "FreeTextInvoiceTable",
                    GroupId = "FREE_TEXT_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "FreeTextInvoiceTable",
                    GroupId = "FREE_TEXT_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "StagingTableIntercoInvSettle",
                    GroupId = "INTERCOMPANY_INVOICE_SETTLEMENT_STAGING",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                       new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntercompanyInvoiceSettlementStagingListPage", Path = "/:site/accounting/inquiry/intercompanyinvoicesettlementstaging/" },
                       new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntercompanyInvoiceSettlementStagingItemPage", Path = "/:site/accounting/inquiry/intercompanyinvoicesettlementstaging/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "IntercompanyInvoiceTable",
                    GroupId = "INTERCOMPANY_INVOICE_INTERCOMPANY_INVOICE_SETTLEMENT_INTERCOMPANY_INVOICE_SETTLEMENT_STAGING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntercompanyInvoiceSettlementStagingItemPage", Path = "/:site/invoiceandcollection/intercompanyinvoice/:id/relatedinfo/intercompanyinvoicesettlement/:id/relatedinfo/intercompanyinvoicesettlementstaging/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "IntercompanyInvoiceSettlementStagingListPage", Path = "/:site/invoiceandcollection/intercompanyinvoice/:id/relatedinfo/intercompanyinvoicesettlement/:id/relatedinfo/intercompanyinvoicesettlementstaging/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "FreeTextInvoiceTable",
                    GroupId = "FREE_TEXT_INVOICE_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "FreeTextInvoiceTable",
                    GroupId = "FREE_TEXT_INVOICE_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "FreeTextInvoiceTable",
                    GroupId = "FREE_TEXT_INVOICE_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "FreeTextInvoiceTable",
                    GroupId = "FREE_TEXT_INVOICE_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "FreeTextInvoiceTable",
                    GroupId = "FREE_TEXT_INVOICE_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "FreeTextInvoiceTable",
                    GroupId = "FREE_TEXT_INVOICE_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "FreeTextInvoiceTable",
                    GroupId = "FREE_TEXT_INVOICE_TABLE_POST_FREE_TEXT_INVOICE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PostFreeTextInvoicePage",
                            Path = "/:site/invoiceandcollection/freetextinvoicetable/:id/function/postfreetextinvoice/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "IntercompanyInvoiceTable",
                    GroupId = "INTERCOMPANY_INVOICE_INTERCOMPANY_INVOICE_SETTLEMENT_POST_INTERCOMPANY_INV_SETTLEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PostIntercompanyInvSettlementPage",
                            Path = "/:site/invoiceandcollection/intercompanyinvoice/:id/relatedinfo/intercompanyinvoicesettlement/:id/function/postintercompanyinvsettlement/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Accounting",
                    GroupId = "INTERFACE_INTERCO_INV_SETTLEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InterfaceIntercoInvSettlementPage", Path = "/:site/accounting/interface/function/interfaceintercoinvsettlement/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BatchObject",
                    GroupId = "BATCH",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                       new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BatchViewListPage", Path = "/:site/admin/inquiry/batch/" },
                       new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BatchViewItemPage", Path = "/:site/admin/inquiry/batch/:id" },
                       new SysFeatureTableView { ParentFeatureId = "BatchViewItemPage", FeatureId = "BatchHistoryListPage", Path = "/:site/admin/inquiry/batch/:id" },
                       new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BatchHistoryItemPage", Path = "/:site/admin/inquiry/batch/:id/batchhistory-child/:id" },
                       new SysFeatureTableView { ParentFeatureId = "BatchHistoryItemPage", FeatureId = "BatchLogListPage", Path = "/:site/admin/inquiry/batch/:id/batchhistory-child/:id" },
                       new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BatchLogItemPage", Path = "/:site/admin/inquiry/batch/:id/batchhistory-child/:id/batchinstancelog-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_BUYER_RECEIPT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerReceiptTableItemPage",Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/buyerreceipttable/:id"},
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerReceiptTableListPage",Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/buyerreceipttable/"},
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MigrationTable",
                    GroupId = "UPDATE_CHEQUE_REFERENCE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateChequeReferencePage", Path = "/:site/function/updatechequereference/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE_REVIEW_CREDIT_APP_REQUEST_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE_REVIEW_CREDIT_APP_REQUEST_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE_REVIEW_CREDIT_APP_REQUEST_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE_REVIEW_CREDIT_APP_REQUEST_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReviewActiveCreditAppLine",
                    GroupId = "FACTORING_REVIEW_ACTIVE_CREDIT_APP_LINE_REVIEW_CREDIT_APP_REQUEST_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/reviewactivecreditappline/:id/relatedinfo/reviewcreditapprequest/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MigrationTable",
                    GroupId = "GEN_PURCHASE_LINE_INVOICE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenPurchaseLineInvoicePage", Path = "/:site/function/genpurchaselineinvoice/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MigrationTable",
                    GroupId = "GEN_WITHDRAWAL_LINE_INVOICE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenWithdrawalLineInvoicePage", Path = "/:site/function/genwithdrawallineinvoice/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Report",
                    GroupId = "REPORT_REPORT_WITHDRAWAL_TERM_EXTENSION",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReportWithdrawalTermExtensionPage",
                            Path = "/:site/report/outstanding/report/reportwithdrawaltermextension/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Report",
                    GroupId = "REPORT_REPORT_SERVICE_FEE_FACT_REALIZATION",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReportServiceFeeFactRealizationPage",
                            Path = "/:site/report/outstanding/report/reportservicefeefactrealization/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "DocumentReturnTable",
                    GroupId = "DOCUMENT_RETURN_TABLE_REPORT_PRINT_DOCUMENT_RETURN_LABEL_LETTER",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintDocumentReturnLabelLetterPage",
                            Path = "/:site/invoiceandcollection/documentreturntable/:id/report/printdocumentreturnlabelletter/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "SysAccessRight",
                    GroupId = "SYSROLETABLE_IMPORT_ACCESS_RIGHT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ImportAccessRightPage",
                            Path = "/:site/admin/sysroletable/:id/function/importaccessright/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "SysUser",
                    GroupId = "SYSUSERTABLE_INACTIVE_USER",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "InactiveUserPage",
                            Path = "/:site/admin/sysusertable/:id/function/inactiveuser/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "SysUser",
                    GroupId = "SYSUSERTABLE_ACTIVE_USER",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActiveUserPage",
                            Path = "/:site/admin/sysusertable/:id/function/activeuser/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "SysUserLog",
                    GroupId = "SYSUSERLOG",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "SysUserLogListPage",
                            Path = "/:site/admin/inquiry/sysuserlog/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "SysUserLog",
                    GroupId = "USER_LOG_CLEAN_UP",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "UserLogCleanUpPage",
                            Path = "/:site/admin/periodic/function/userlogcleanup/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BatchObject",
                    GroupId = "BATCH_HISTORY-CHILD_CANCEL_BATCH_INSTANCE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                       new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelBatchInstancePage", Path = "/:site/admin/inquiry/batch/:id/batchhistory-child/:id/function/cancelbatchinstance/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BatchObject",
                    GroupId = "BATCH_HISTORY-CHILD_RETRY_BATCH_INSTANCE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                       new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetryBatchInstancePage", Path = "/:site/admin/inquiry/batch/:id/batchhistory-child/:id/function/retrybatchinstance/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BatchObject",
                    GroupId = "BATCH_CANCEL_BATCH_JOB",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                       new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelBatchJobPage", Path = "/:site/admin/inquiry/batch/:id/function/cancelbatchjob/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BatchObject",
                    GroupId = "BATCH_CLEAN_UP_BATCH_HISTORY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                       new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CleanUpBatchHistoryPage", Path = "/:site/admin/inquiry/batch/:id/function/cleanupbatchhistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BatchObject",
                    GroupId = "BATCH_HOLD_BATCH_JOB",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                       new SysFeatureTableView { ParentFeatureId = null, FeatureId = "HoldBatchJobPage", Path = "/:site/admin/inquiry/batch/:id/function/holdbatchjob/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BatchObject",
                    GroupId = "BATCH_RELEASE_BATCH_JOB",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                       new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReleaseBatchJobPage", Path = "/:site/admin/inquiry/batch/:id/function/releasebatchjob/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "IntercompanyInvoiceTable",
                    GroupId = "INTERCOMPANY_INVOICE_INTERCOMPANY_INVOICE_SETTLEMENT_CANCEL_INTERCOMPANY_INV_SETTLEMENT",FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,FeatureId = "CancelIntercompanyInvSettlementPage",
                            Path = "/:site/invoiceandcollection/intercompanyinvoice/:id/relatedinfo/intercompanyinvoicesettlement/:id/function/cancelintercompanyinvsettlement/"
                        },
                    }
                },
                        new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_COLLECTION_FOLLOW_UP_BUYER_RECEIPT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerReceiptTableItemPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/collectionfollowup/:id/relatedinfo/buyerreceipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerReceiptTableListPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/collectionfollowup/:id/relatedinfo/buyerreceipttable/" }
                    }
                },
                          new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_COLLECTION_FOLLOW_UP_BUYER_RECEIPT_TABLE_CANCEL_BUYER_RECEIPT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelBuyerReceiptTablePage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/collectionfollowup/:id/relatedinfo/buyerreceipttable/:id/function/cancelbuyerreceipttable/" },
                    }
                },
                  new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "InquiryWithdrawalLineOutstand",
                    GroupId = "INQUIRY_WITHDRAWAL_LINE_OUTSTAND_COLLECTION_FOLLOW_UP_BUYER_RECEIPT_TABLE_PRINT_BUYER_RECEIPT",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBuyerReceiptPage", Path = "/:site/invoiceandcollection/inquiry/inquirywithdrawallineoutstand/:id/relatedinfo/collectionfollowup/:id/relatedinfo/buyerreceipttable/:id/report/printbuyerreceipt/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_RECEIPT_TABLE_PRINT_RECEIPT_COPY",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintReceiptCopyPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/receipttable/:id/report/printreceiptcopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_RECEIPT_TABLE_PRINT_RECEIPT_ORIGINAL",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintReceiptOriginalPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/receipttable/:id/report/printreceiptoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_RECEIPT_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/receipttable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/receipttable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/receipttable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/receipttable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Report",
                    GroupId = "REPORT_EXPORT_AGING_FACTORING",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExportAgingFactoringPage",
                            Path = "/:site/report/outstanding/report/exportagingfactoring/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Report",
                    GroupId = "REPORT_EXPORT_AGING_PROJECT_FINANCE",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExportAgingProjectFinancePage",
                            Path = "/:site/report/outstanding/report/exportagingprojectfinance/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "Report",
                    GroupId = "REPORT_EXPORT_ACCRUED_INTEREST",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExportAccruedInterestPage",
                            Path = "/:site/report/outstanding/report/exportaccruedinterest/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CollectionFollowUp",
                    GroupId = "COLLECTION_FOLLOW_UP",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CollectionFollowUpItemPage", Path = "/:site/invoiceandcollection/inquiry/collectionfollowup/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CollectionFollowUpListPage", Path = "/:site/invoiceandcollection/inquiry/collectionfollowup/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CollectionFollowUp",
                    GroupId = "COLLECTION_FOLLOW_UP_BUYER_RECEIPT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerReceiptTableItemPage", Path = "/:site/invoiceandcollection/inquiry/collectionfollowup/:id/relatedinfo/buyerreceipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerReceiptTableListPage", Path = "/:site/invoiceandcollection/inquiry/collectionfollowup/:id/relatedinfo/buyerreceipttable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CollectionFollowUp",
                    GroupId = "COLLECTION_FOLLOW_UP_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/invoiceandcollection/inquiry/collectionfollowup/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/invoiceandcollection/inquiry/collectionfollowup/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CollectionFollowUp",
                    GroupId = "COLLECTION_FOLLOW_UP_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                            {
                                new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage",Path = "/:site/invoiceandcollection/inquiry/collectionfollowup/:id/relatedinfo/memotrans/:id"},
                                new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage",Path = "/:site/invoiceandcollection/inquiry/collectionfollowup/:id/relatedinfo/memotrans/"}
                            }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CollectionFollowUp",
                    GroupId = "COLLECTION_FOLLOW_UP_BUYER_RECEIPT_TABLE_CANCEL_BUYER_RECEIPT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelBuyerReceiptTablePage", Path = "/:site/invoiceandcollection/inquiry/collectionfollowup/:id/relatedinfo/buyerreceipttable/:id/function/cancelbuyerreceipttable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CollectionFollowUp",
                    GroupId = "COLLECTION_FOLLOW_UP_BUYER_RECEIPT_TABLE_PRINT_BUYER_RECEIPT",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBuyerReceiptPage", Path = "/:site/invoiceandcollection/inquiry/collectionfollowup/:id/relatedinfo/buyerreceipttable/:id/report/printbuyerreceipt/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_RECEIPT_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/receipttable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_RECEIPT_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/receipttable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "RECEIPT_TEMP_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/invoiceandcollection/receipttemptable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
            };
        }
    }
}

