using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.WebApi
{
    public partial class SysFeatureTableData
    {
        public static List<SysFeatureGroupControllerMappingView> GetAddFeatureControllerMapping_CreditAppRequestTable()
        {
            return new List<SysFeatureGroupControllerMappingView>()
            {
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableItemPage", Path = "/:site/factoring/creditapprequesttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableListPage", Path = "/:site/factoring/creditapprequesttable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppRequestTableItemPage", FeatureId = "CreditAppRequestLineListPage", Path = "/:site/factoring/creditapprequesttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestLineItemPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransItemPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransListPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransItemPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransListPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransItemPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransListPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransItemPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransListPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransItemPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedPersonTransListPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_AUTHORIZED_PERSON_TRANS_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NCBItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id/relatedinfo/ncbtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NCBListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id/relatedinfo/ncbtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_AUTHORIZED_PERSON_TRANS_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_AUTHORIZED_PERSON_TRANS_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_AUTHORIZED_PERSON_TRANS_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_AUTHORIZED_PERSON_TRANS_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_AUTHORIZED_PERSON_TRANS_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/authorizedpersontrans/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/financialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/financialstatementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/financialstatementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/financialstatementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/financialstatementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/financialstatementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/financialstatementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/financialstatementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/financialstatementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/financialstatementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/financialstatementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/taxreporttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/taxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "TaxReportTransItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/taxreporttrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "TaxReportTransListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/taxreporttrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "TaxReportTransItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/taxreporttrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "TaxReportTransListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/taxreporttrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "TaxReportTransItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/taxreporttrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "TaxReportTransListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/taxreporttrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "TaxReportTransItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/taxreporttrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "TaxReportTransListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/taxreporttrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "TaxReportTransItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/taxreporttrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "TaxReportTransListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/taxreporttrans/"
                        }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/servicefeeconditiontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/servicefeeconditiontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionConditionTransItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionConditionTransListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/"
                        }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableItemPage", Path = "/:site/bond/creditapprequesttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableListPage", Path = "/:site/bond/creditapprequesttable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppRequestTableItemPage", FeatureId = "CreditAppRequestLineListPage", Path = "/:site/bond/creditapprequesttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestLineItemPage", Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_CUST_VISITING_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustVisitingTransItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/custvisitingtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustVisitingTransListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/custvisitingtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_CUST_VISITING_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustVisitingTransItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/custvisitingtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustVisitingTransListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/custvisitingtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_CUST_VISITING_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustVisitingTransItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/custvisitingtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustVisitingTransListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/custvisitingtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_CUST_VISITING_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustVisitingTransItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/custvisitingtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustVisitingTransListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/custvisitingtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_CUST_VISITING_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustVisitingTransItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/custvisitingtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustVisitingTransListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/custvisitingtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_CUST_VISITING_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustVisitingTransItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/custvisitingtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustVisitingTransListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/custvisitingtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_GUARANTOR_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorTransItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/guarantortrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorTransListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/guarantortrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_GUARANTOR_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/guarantortrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/guarantortrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_GUARANTOR_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/guarantortrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/guarantortrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_GUARANTOR_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/guarantortrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/guarantortrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_GUARANTOR_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/guarantortrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/guarantortrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_GUARANTOR_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/guarantortrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/guarantortrans/"
                        }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NCBItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/ncbtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NCBListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/ncbtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableItemPage", Path = "/:site/hirepurchase/creditapprequesttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableListPage", Path = "/:site/hirepurchase/creditapprequesttable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppRequestTableItemPage", FeatureId = "CreditAppRequestLineListPage", Path = "/:site/hirepurchase/creditapprequesttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestLineItemPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableItemPage", Path = "/:site/lcdlc/creditapprequesttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableListPage", Path = "/:site/lcdlc/creditapprequesttable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppRequestTableItemPage", FeatureId = "CreditAppRequestLineListPage", Path = "/:site/lcdlc/creditapprequesttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestLineItemPage", Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableItemPage", Path = "/:site/leasing/creditapprequesttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableListPage", Path = "/:site/leasing/creditapprequesttable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppRequestTableItemPage", FeatureId = "CreditAppRequestLineListPage", Path = "/:site/leasing/creditapprequesttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestLineItemPage", Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableItemPage", Path = "/:site/projectfinance/creditapprequesttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableListPage", Path = "/:site/projectfinance/creditapprequesttable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppRequestTableItemPage", FeatureId = "CreditAppRequestLineListPage", Path = "/:site/projectfinance/creditapprequesttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestLineItemPage", Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_LINE-CHILD_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_LINE-CHILD_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_LINE-CHILD_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_LINE-CHILD_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_LINE-CHILD_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_LINE-CHILD_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_LINE-CHILD_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_LINE-CHILD_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_LINE-CHILD_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_LINE-CHILD_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_LINE-CHILD_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_LINE-CHILD_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialStatementTransListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransItemPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransListPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentConditionInfoItemPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/" },
                        new SysFeatureTableView { ParentFeatureId = "DocumentConditionInfoItemPage", FeatureId = "BillingDocumentConditionTransListPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/" },
                        new SysFeatureTableView { ParentFeatureId = "DocumentConditionInfoItemPage", FeatureId = "ReceiptDocumentConditionTransListPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BillingDocumentConditionTransItemPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptDocumentConditionTransItemPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "DocumentConditionInfoItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "DocumentConditionInfoItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "DocumentConditionInfoItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "DocumentConditionInfoItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "DocumentConditionInfoItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerAgreementTransItemPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerAgreementTransListPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/"
                        }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/financialcredittrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/financialcredittrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialCreditItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/financialcredittrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialCreditListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/financialcredittrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialCreditItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/financialcredittrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialCreditListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/financialcredittrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialCreditItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/financialcredittrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialCreditListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/financialcredittrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialCreditItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/financialcredittrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialCreditListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/financialcredittrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialCreditItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/financialcredittrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "FinancialCreditListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/financialcredittrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/factoring/creditapprequesttable/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_OWNER_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/ownertrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/ownertrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_OWNER_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/ownertrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/ownertrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_OWNER_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "OwnerTransItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/ownertrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "OwnerTransListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/ownertrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_LINE-CHILD_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_OWNER_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/ownertrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/ownertrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_OWNER_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/ownertrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/ownertrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_OWNER_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/ownertrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/ownertrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_COPY_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyTaxReportTransPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/taxreporttrans/function/copytaxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_COPY_CUST_VISITING_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyCustVisitingTransPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/custvisitingtrans/function/copycustvisiting/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_LINE-CHILD_COPY_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBuyerAgreementTransPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/function/copybuyeragreementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIREPURCHASE_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LCDLC_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECTFINANCE_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_LINE-CHILD_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIREPURCHASE_CREDIT_APP_REQUEST_LINE-CHILD_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_LINE-CHILD_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_LINE-CHILD_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECTFINANCE_CREDIT_APP_REQUEST_LINE-CHILD_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_COPY_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyTaxReportTransPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/taxreporttrans/function/copytaxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIREPURCHASE_COPY_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyTaxReportTransPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/taxreporttrans/function/copytaxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_COPY_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyTaxReportTransPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/taxreporttrans/function/copytaxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_COPY_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyTaxReportTransPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/taxreporttrans/function/copytaxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_COPY_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyTaxReportTransPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/taxreporttrans/function/copytaxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BookmarkDocumentItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BookmarkDocumentListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BookmarkDocumentItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BookmarkDocumentListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BookmarkDocumentItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BookmarkDocumentListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BookmarkDocumentItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BookmarkDocumentListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BookmarkDocumentItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BookmarkDocumentListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_COPY_CUST_VISITING_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyCustVisitingTransPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/custvisitingtrans/function/copycustvisiting/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIREPURCHASE_COPY_CUST_VISITING_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyCustVisitingTransPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/custvisitingtrans/function/copycustvisiting/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LCDLC_COPY_CUST_VISITING_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyCustVisitingTransPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/custvisitingtrans/function/copycustvisiting/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_COPY_CUST_VISITING_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyCustVisitingTransPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/custvisitingtrans/function/copycustvisiting/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_COPY_CUST_VISITING_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyCustVisitingTransPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/custvisitingtrans/function/copycustvisiting/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_LINE-CHILD_COPY_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBuyerAgreementTransPage", Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/function/copybuyeragreementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIREPURCHASE_CREDIT_APP_REQUEST_LINE-CHILD_COPY_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBuyerAgreementTransPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/function/copybuyeragreementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LCDLC_CREDIT_APP_REQUEST_LINE-CHILD_COPY_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBuyerAgreementTransPage", Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/function/copybuyeragreementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_LINE-CHILD_COPY_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBuyerAgreementTransPage", Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/function/copybuyeragreementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_LINE-CHILD_COPY_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBuyerAgreementTransPage", Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/function/copybuyeragreementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/bond/creditapprequesttable/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIREPURCHASE_CREDIT_APP_REQUEST_TABLE_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LCDLC_CREDIT_APP_REQUEST_TABLE_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/lcdlc/creditapprequesttable/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/leasing/creditapprequesttable/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/projectfinance/creditapprequesttable/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowCreditAppRequestPage",
                            Path = "/:site/factoring/creditapprequesttable/:id/workflow/startworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowCreditAppRequestPage",
                            Path = "/:site/bond/creditapprequesttable/:id/workflow/startworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowCreditAppRequestPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/workflow/startworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowCreditAppRequestPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/workflow/startworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowCreditAppRequestPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/workflow/startworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowCreditAppRequestPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/workflow/startworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/factoring/creditapprequesttable/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/bond/creditapprequesttable/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/factoring/creditapprequesttable/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/bond/creditapprequesttable/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_RETENTION_CONDITION_TRANS_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_RETENTION_CONDITION_TRANS_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_RETENTION_CONDITION_TRANS_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_RETENTION_CONDITION_TRANS_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_RETENTION_CONDITION_TRANS_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_RETENTION_CONDITION_TRANS_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/retentionconditiontrans/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintBookDocTransPage",
                            Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintBookDocTransPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintBookDocTransPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintBookDocTransPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintBookDocTransPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintBookDocTransPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/"
                        }
                    }
                },
                   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/attachment/" }
                    }
                },
                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/attachment/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/attachment/" }
                    }
                },
                   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/attachment/" }
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/attachment/" }
                    }
                },
                   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/attachment/" }
                    }
                },

                   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/attachment/" }
                    }
                },
                         new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/creditapprequesttable/:id/creditapprequestline-child/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/attachment/" }
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/Leasing/creditapprequesttable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/Leasing/creditapprequesttable/:id/relatedinfo/attachment/" }
                    }
                },
                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/Lcdlc/creditapprequesttable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/Lcdlc/creditapprequesttable/:id/relatedinfo/attachment/" }
                    }
                },
                              new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/attachment/" }
                    }
                },

                

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_RETENTION_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/retentionoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_RETENTION_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingListPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/retentionoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_RETENTION_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingListPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/retentionoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_RETENTION_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingListPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/retentionoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_RETENTION_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingListPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/retentionoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_RETENTION_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingListPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/retentionoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_CREDIT_APP_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/creditapptable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppTableItemPage", FeatureId = "CreditAppLineListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/creditapptable/:id"},
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppLineItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/creditapptable/:id/creditappline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_CREDIT_APP_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableItemPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableListPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/creditapptable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppTableItemPage", FeatureId = "CreditAppLineListPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/creditapptable/:id"},
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppLineItemPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/creditapptable/:id/creditappline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_CREDIT_APP_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableItemPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableListPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/creditapptable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppTableItemPage", FeatureId = "CreditAppLineListPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/creditapptable/:id"},
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppLineItemPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/creditapptable/:id/creditappline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_CREDIT_APP_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableItemPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableListPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/creditapptable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppTableItemPage", FeatureId = "CreditAppLineListPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/creditapptable/:id"},
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppLineItemPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/creditapptable/:id/creditappline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_CREDIT_APP_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableItemPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableListPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/creditapptable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppTableItemPage", FeatureId = "CreditAppLineListPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/creditapptable/:id"},
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppLineItemPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/creditapptable/:id/creditappline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_CREDIT_APP_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableItemPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableListPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/creditapptable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppTableItemPage", FeatureId = "CreditAppLineListPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/creditapptable/:id"},
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppLineItemPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/creditapptable/:id/creditappline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/creditapprequesttable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/creditapprequesttable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/creditapprequesttable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/creditapprequesttable/:id/function/printsetbookdoctrans/" }
                    }
                },     
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/creditapprequesttable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/retentiontransaction/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/retentiontransaction/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemComponent",
                            Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemComponent",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemComponent",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemComponent",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemComponent",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemComponent",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "FACTORING_CREDIT_APP_REQUEST_TABLE_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/factoring/creditapprequesttable/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "BOND_CREDIT_APP_REQUEST_TABLE_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/bond/creditapprequesttable/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_REQUEST_TABLE_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/hirepurchase/creditapprequesttable/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LC_DLC_CREDIT_APP_REQUEST_TABLE_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/lcdlc/creditapprequesttable/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "LEASING_CREDIT_APP_REQUEST_TABLE_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/leasing/creditapprequesttable/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppRequestTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_REQUEST_TABLE_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/projectfinance/creditapprequesttable/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    }
                },
            };
        }

    }
}
