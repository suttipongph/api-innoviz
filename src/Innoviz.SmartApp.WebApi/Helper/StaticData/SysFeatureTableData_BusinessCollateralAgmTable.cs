﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.WebApi
{
    public partial class SysFeatureTableData
    {
        public static List<SysFeatureGroupControllerMappingView> GetAddFeatureControllerMapping_BusinessCollateralAgmTable()
        {
            return new List<SysFeatureGroupControllerMappingView>()
            {
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "BusinessCollateralAgmTableItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmLineItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/businesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "BusinessCollateralAgmTableItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmLineItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/businesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "BusinessCollateralAgmTableItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmLineItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/businesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "BusinessCollateralAgmTableItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmLineItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/businesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "BusinessCollateralAgmTableItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmLineItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/businesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "BusinessCollateralAgmTableItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmLineItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/businesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "BusinessCollateralAgmTableItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmLineItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/businesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "BusinessCollateralAgmTableItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmLineItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/businesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "BusinessCollateralAgmTableItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmLineItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/businesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "BusinessCollateralAgmTableItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmLineItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/businesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "BusinessCollateralAgmTableItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmLineItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/businesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmTableListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "BusinessCollateralAgmTableItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BusinessCollateralAgmLineItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/businesscollateralagmline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "NoticeOfCancellationItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationLineItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/noticeofcancellationbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "NoticeOfCancellationItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationLineItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/noticeofcancellationbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "NoticeOfCancellationItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationLineItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/noticeofcancellationbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "NoticeOfCancellationItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationLineItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/noticeofcancellationbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "NoticeOfCancellationItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationLineItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/noticeofcancellationbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "NoticeOfCancellationItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationLineItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/noticeofcancellationbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "NoticeOfCancellationItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationLineItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/noticeofcancellationbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "NoticeOfCancellationItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationLineItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/noticeofcancellationbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "NoticeOfCancellationItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationLineItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/noticeofcancellationbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "NoticeOfCancellationItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationLineItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/noticeofcancellationbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "NoticeOfCancellationItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationLineItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/noticeofcancellationbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "NoticeOfCancellationItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationLineItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/noticeofcancellationbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "AddendumItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumLineItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/addendumbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "AddendumItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumLineItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/addendumbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "AddendumItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumLineItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/addendumbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "AddendumItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumLineItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/addendumbusinesscollateralagmline-child/:id" },
                    }
                },


                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "AddendumItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumLineItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/addendumbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "AddendumItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumLineItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/addendumbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "AddendumItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumLineItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/addendumbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "AddendumItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumLineItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/addendumbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "AddendumItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumLineItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/addendumbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "AddendumItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumLineItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/addendumbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "AddendumItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumLineItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/addendumbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/" },
                        new SysFeatureTableView { ParentFeatureId = "AddendumItemPage", FeatureId = "BusinessCollateralAgmLineListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumLineItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/addendumbusinesscollateralagmline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },



                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_GENERATE_ADDENDUM",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenBusCollateralAgmAddendumPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/generateaddendum/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_GENERATE_ADDENDUM",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenBusCollateralAgmAddendumPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/generateaddendum/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_GENERATE_ADDENDUM",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenBusCollateralAgmAddendumPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/generateaddendum/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_GENERATE_ADDENDUM",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenBusCollateralAgmAddendumPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/generateaddendum/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_GENERATE_ADDENDUM",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenBusCollateralAgmAddendumPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/generateaddendum/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_GENERATE_ADDENDUM",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenBusCollateralAgmAddendumPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/generateaddendum/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_GENERATE_ADDENDUM",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenBusCollateralAgmAddendumPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/generateaddendum/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_GENERATE_ADDENDUM",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenBusCollateralAgmAddendumPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/generateaddendum/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_GENERATE_ADDENDUM",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenBusCollateralAgmAddendumPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/generateaddendum/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_GENERATE_ADDENDUM",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenBusCollateralAgmAddendumPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/generateaddendum/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_GENERATE_ADDENDUM",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenBusCollateralAgmAddendumPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/generateaddendum/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_GENERATE_ADDENDUM",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenBusCollateralAgmAddendumPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/generateaddendum/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId ="LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FLC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_COPY_ACTIVE_BUSINESS_COLLATERAL_AGREEMENT_LINE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyActiveBusinessCollateralAgreementLinePage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/copyactivebusinesscollateralagreementline/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId ="LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FLC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NotiOfCancelBusCollateralAgmPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/function/noticeofcancellation/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NotiOfCancelBusCollateralAgmPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/function/noticeofcancellation/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NotiOfCancelBusCollateralAgmPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/function/noticeofcancellation/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NotiOfCancelBusCollateralAgmPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/function/noticeofcancellation/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NotiOfCancelBusCollateralAgmPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/function/noticeofcancellation/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NotiOfCancelBusCollateralAgmPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/function/noticeofcancellation/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NotiOfCancelBusCollateralAgmPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/function/noticeofcancellation/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NotiOfCancelBusCollateralAgmPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/function/noticeofcancellation/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NotiOfCancelBusCollateralAgmPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/function/noticeofcancellation/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NotiOfCancelBusCollateralAgmPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/function/noticeofcancellation/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NotiOfCancelBusCollateralAgmPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/function/noticeofcancellation/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NotiOfCancelBusCollateralAgmPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_ADDENDUM_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/addendumbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_FUNCTION_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId ="LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FLC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/factoring/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/bond/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/lcdlc/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "FACTORING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/factoring/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "BOND_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/bond/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "BusinessCollateralAgmTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION_BUSINESS_COLLATERAL_AGM_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/relatedinfo/noticeofcancellationbusinesscollateralagmtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
            };
        }

    }
}