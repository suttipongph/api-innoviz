using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.WebApi
{
    public partial class SysFeatureTableData
    {
        public static List<SysFeatureGroupControllerMappingView> GetAddFeatureControllerMapping_MainAgreementTable()
        {
            return new List<SysFeatureGroupControllerMappingView>()
            {
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MainAgreementTableListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/leasing/newmainagreementtable/allmainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementItemPage", FeatureId = "GuarantorAgreementLineListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorAgreementLineAffiliateItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id/guarantoragreementlineaffiliate-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "GuarantorAgreementLineAffiliateItemPage", FeatureId = "GuarantorAgreementLineAffiliateListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/guarantoragreementline-child/:id" }

                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIREPURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIREPURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LCDLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LCDLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECTFINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECTFINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_COPY_GUARANTOR_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyGuarantorFromCaPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/copyguarantorfromca/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_GUARANTOR",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendGuarantorPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/function/extendguarantor/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_GUARANTOR",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendGuarantorPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/function/extendguarantor/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_GUARANTOR",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendGuarantorPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/function/extendguarantor/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_GUARANTOR",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendGuarantorPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/function/extendguarantor/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIREPURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_GUARANTOR",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendGuarantorPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/function/extendguarantor/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIREPURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_GUARANTOR",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendGuarantorPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/function/extendguarantor/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_GUARANTOR",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendGuarantorPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/function/extendguarantor/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_GUARANTOR",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendGuarantorPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/function/extendguarantor/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LCDLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_GUARANTOR",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendGuarantorPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/function/extendguarantor/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LCDLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_GUARANTOR",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendGuarantorPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/function/extendguarantor/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECTFINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_GUARANTOR",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendGuarantorPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/function/extendguarantor/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECTFINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_EXTEND_GUARANTOR",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendGuarantorPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/extend/function/extendguarantor/" },
                    }
                },
               
                
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/function/addendum/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/function/addendum/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/function/addendum/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/function/addendum/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/function/addendum/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/function/addendum/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/function/addendum/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/function/addendum/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/function/addendum/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/function/addendum/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/function/addendum/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AddendumPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/function/addendum/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/function/noticeofcancellation/" },

                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/hirepurchase/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/hirepurchase/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/noticeofcancellation/" },
                        }
                    },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/leasing/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/leasing/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/lcdlc/allmainagreementtable/businesscollateralagmtable/:id/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/lcdlc/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/noticeofcancellation/" },

                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/projectfinance/newbusinesscollateralagmtable/businesscollateralagmtable/:id/function/noticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_BUSINESS_COLLATERAL_AGM_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationPage", Path = "/:site/projectfinance/allbusinesscollateralagmtable/businesscollateralagmtable/:id/function/noticeofcancellation/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                  new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                      new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/" }
                    }
                },

                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/" }
                    }
                },

                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/" }
                    }
                },

                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                            new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/" }
                    }
                },

                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                          new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/" }
                    }
                },

                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                          new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/" }
                    }
                },

                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/" }
                    }
                },

                          new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/Projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/Projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/" }
                    }
                },

                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                         new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/Projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/Projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/function/printsetbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_LOAN_REQUEST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LoanRequestAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/function/loanrequestagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_LOAN_REQUEST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LoanRequestAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/function/loanrequestagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_LOAN_REQUEST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LoanRequestAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/function/loanrequestagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_LOAN_REQUEST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LoanRequestAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/function/loanrequestagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_LOAN_REQUEST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LoanRequestAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/function/loanrequestagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_LOAN_REQUEST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LoanRequestAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/function/loanrequestagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_LOAN_REQUEST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LoanRequestAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/function/loanrequestagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_LOAN_REQUEST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LoanRequestAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/function/loanrequestagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_LOAN_REQUEST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LoanRequestAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/function/loanrequestagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_LOAN_REQUEST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LoanRequestAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/function/loanrequestagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_LOAN_REQUEST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LoanRequestAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/function/loanrequestagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_LOAN_REQUEST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "LoanRequestAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/function/loanrequestagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },   
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_GUARANTOR_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/guarantoragreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },

                                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },

                                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },

                                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_CONSORTIUM_TRANS_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },

                                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementInformationListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_ADDENDUM_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/addendummainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/factoring/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "FACTORING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/factoring/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/bond/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "BOND_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/bond/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/hirepurchase/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "HIRE_PURCHASE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/hirepurchase/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/leasing/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LEASING_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/leasing/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/lcdlc/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "LC_DLC_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/lcdlc/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_NEW_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/projectfinance/newmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "MainAgreementTable",
                    GroupId = "PROJECT_FINANCE_ALL_MAIN_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MAIN_AGREEMENT_TABLE_SERVICE_FEE_TRANS_COPY_SERVICE_FEE_CONDITION_TRANS_FROM_CA",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyServiceFeeConditionTransFromCAPage", Path = "/:site/projectfinance/allmainagreementtable/mainagreementtable/:id/relatedinfo/noticeofcancellationmainagreementtable/:id/relatedinfo/servicefeetrans/function/copyservicefeeconditiontransfromca/" },

                    }
                }
            };
        }

    }
}
