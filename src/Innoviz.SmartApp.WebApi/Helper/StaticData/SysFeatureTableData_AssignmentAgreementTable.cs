﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.WebApi
{
    public partial class SysFeatureTableData
    {
        public static List<SysFeatureGroupControllerMappingView> GetAddSysFeatureTableData_AssignmentAgreementTable()
        {
            return new List<SysFeatureGroupControllerMappingView>()
            {
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementTableListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementTableItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id" },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AssignmentAgreementTableItemPage",
                            FeatureId = "AssignmentAgreementLineListPage",
                            Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AssignmentAgreementLineItemPage",
                            Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/assignmentagreementline-child/:id"
                        }
                    },
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementTableListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementTableItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id" },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AssignmentAgreementTableItemPage",
                            FeatureId = "AssignmentAgreementLineListPage",
                            Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AssignmentAgreementLineItemPage",
                            Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/assignmentagreementline-child/:id"
                        }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentTransItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentTransListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentTransItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentTransListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/consortiumtrans/" }

                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementTableInfoItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementTableInfoListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementTableInfoItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementTableInfoListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id" },

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/" },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "NoticeOfCancellationItemPage",
                            FeatureId = "AssignmentAgreementLineListPage",
                            Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AssignmentAgreementLineItemPage",
                            Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/noticeofcancellation-child/:id"
                        }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id" },

                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NoticeOfCancellationListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/" },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "NoticeOfCancellationItemPage",
                            FeatureId = "AssignmentAgreementLineListPage",
                            Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NoticeOfCancellationLineItemPage",
                            Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/noticeofcancellation-child/:id"
                        }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenerateNoticeOfCancellationPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/function/generatenoticeofcancellation/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_ASSIGNMENT_AGREEMENT_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementSettleItemmentPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/assignmentagreementsettlement/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementSettleListmentPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/assignmentagreementsettlement/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_ASSIGNMENT_AGREEMENT_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementSettleItemmentPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/assignmentagreementsettlement/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementSettleListmentPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/assignmentagreementsettlement/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_GENERATE_NOTICE_OF_CANCELLATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GenerateNoticeOfCancellationPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/function/generatenoticeofcancellation/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementMemoItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementMemoListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementMemoItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementMemoListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_INQUIRY_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InquiryAssignmentOutstandingListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/inquiryassignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_INQUIRY_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InquiryAssignmentOutstandingListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/inquiryassignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_PRINT_BOOK_DOC_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_PRINT_BOOK_DOC_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_UPDATE_ASSIGNMENT_AGREEMENT_AMOUNT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateAssignmentAgreementAmountPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/function/updateassignmentagreementamount/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_UPDATE_ASSIGNMENT_AGREEMENT_AMOUNT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateAssignmentAgreementAmountPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/function/updateassignmentagreementamount/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "SUSPENSE_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "SUSPENSE_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "SUSPENSE_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "SUSPENSE_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "SUSPENSE_REFUND_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/invoiceandcollection/refund/suspenserefund/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_POST_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostAgreementPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/function/postagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SEND_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SendAgreementPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/function/sendagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SIGN_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SignAgreementPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/function/signagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_CLOSE_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseAgreementPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/function/closeagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_CANCEL_AGREEMENT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelAgreementPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/function/cancelagreement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_PRINT_BOOK_DOC_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_PRINT_BOOK_DOC_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementTableInfoItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementTableInfoListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_AGREEMENT_TABLE_INFO",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementTableInfoItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/agreementtableinfo/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AgreementTableInfoListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/agreementtableinfo/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentTransItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentTransListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentTransItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentTransListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/bookmarkdocumenttrans/" }
                    }
                },
                                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_BOOKMARK_DOCUMENT_TRANS_COPY_BOOKMARK_DOCUMENT_FROM_TEMPLATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyBookmarkDocumentFromTemplatePage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/bookmarkdocumenttrans/function/copybookmarkdocumentfromtemplate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/consortiumtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_CONSORTIUM_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/consortiumtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ConsortiumTransListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/consortiumtrans/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementMemoItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementMemoListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementMemoItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementMemoListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_JOINT_VENTURE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/jointventuretrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "JointVentureTransListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/jointventuretrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/" }
                    }
                },


                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_NOTICE_OF_CANCELLATION_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/noticeofcancellation/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_ALL_ASSIGNMENT_AGREEMENT_TABLE_ASSIGNMENT_AGREEMENT_SETTLEMENT_UPDATE_ASSIGNMENT_AGREEMENT_SETTLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateAssignmentAgreementSettlePage", Path = "/:site/assignmentagreement/all/assignmentagreementtable/:id/relatedinfo/assignmentagreementsettlement/:id/function/updateassignmentagreementsettle/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "AssignmentAgreementTable",
                    GroupId = "ASSIGNMENT_AGREEMENT_NEW_ASSIGNMENT_AGREEMENT_TABLE_ASSIGNMENT_AGREEMENT_SETTLEMENT_UPDATE_ASSIGNMENT_AGREEMENT_SETTLE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateAssignmentAgreementSettlePage", Path = "/:site/assignmentagreement/new/assignmentagreementtable/:id/relatedinfo/assignmentagreementsettlement/:id/function/updateassignmentagreementsettle/" },
                    }
                },
            };
        }

    }
}
