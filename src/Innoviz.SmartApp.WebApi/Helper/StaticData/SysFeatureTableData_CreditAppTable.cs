﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.WebApi
{
    public partial class SysFeatureTableData
    {
        public static List<SysFeatureGroupControllerMappingView> GetAddFeatureControllerMapping_CreditAppTable()
        {
            return new List<SysFeatureGroupControllerMappingView>()
            {
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableItemPage", Path = "/:site/hirepurchase/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableListPage", Path = "/:site/hirepurchase/creditapptable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppTableItemPage", FeatureId = "CreditAppLineListPage", Path = "/:site/hirepurchase/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppLineItemPage", Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableItemPage", Path = "/:site/lcdlc/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableListPage", Path = "/:site/lcdlc/creditapptable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppTableItemPage", FeatureId = "CreditAppLineListPage", Path = "/:site/lcdlc/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppLineItemPage", Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableItemPage", Path = "/:site/leasing/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableListPage", Path = "/:site/leasing/creditapptable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppTableItemPage", FeatureId = "CreditAppLineListPage", Path = "/:site/leasing/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppLineItemPage", Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableItemPage", Path = "/:site/projectfinance/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableListPage", Path = "/:site/projectfinance/creditapptable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppTableItemPage", FeatureId = "CreditAppLineListPage", Path = "/:site/projectfinance/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppLineItemPage", Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableItemPage", Path = "/:site/bond/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableListPage", Path = "/:site/bond/creditapptable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppTableItemPage", FeatureId = "CreditAppLineListPage", Path = "/:site/bond/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppLineItemPage", Path = "/:site/bond/creditapptable/:id/creditappline-child/:id" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableItemPage", Path = "/:site/factoring/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppTableListPage", Path = "/:site/factoring/creditapptable/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppTableItemPage", FeatureId = "CreditAppLineListPage", Path = "/:site/factoring/creditapptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppLineItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedpersontransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/authorizedpersontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedpersontransListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/authorizedpersontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedpersontransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/authorizedpersontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedpersontransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/authorizedpersontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedpersontransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/authorizedpersontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AuthorizedpersontransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/authorizedpersontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedpersontransItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/authorizedpersontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedpersontransListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/authorizedpersontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedpersontransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/authorizedpersontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedpersontransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/authorizedpersontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AUTHORIZED_PERSON_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedpersontransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/authorizedpersontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedpersontransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/authorizedpersontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_OWNER_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/ownertrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/ownertrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_OWNER_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/ownertrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/ownertrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_OWNER_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "OwnerTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/ownertrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "OwnerTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/ownertrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_OWNER_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/ownertrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/ownertrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_OWNER_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/ownertrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/ownertrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_OWNER_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/ownertrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "OwnerTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/ownertrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_GUARANTOR_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/guarantortrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "GuarantorTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/guarantortrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_GUARANTOR_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/guarantortrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/guarantortrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_GUARANTOR_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/guarantortrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/guarantortrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_GUARANTOR_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/guarantortrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/guarantortrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_GUARANTOR_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/guarantortrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/guarantortrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_GUARANTOR_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/guarantortrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/guarantortrans/"
                        }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionConditionTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/retentionconditiontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionConditionTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/retentionconditiontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/retentionconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/retentionconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/retentionconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/retentionconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/retentionconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/retentionconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/retentionconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/retentionconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/retentionconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/retentionconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/servicefeeconditiontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/servicefeeconditiontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/servicefeeconditiontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/servicefeeconditiontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DocumentConditionInfoItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/" },
                        new SysFeatureTableView { ParentFeatureId = "DocumentConditionInfoItemPage", FeatureId = "BillingDocumentConditionTransListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/" },
                        new SysFeatureTableView { ParentFeatureId = "DocumentConditionInfoItemPage", FeatureId = "ReceiptDocumentConditionTransListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BillingDocumentConditionTransItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptDocumentConditionTransItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "DocumentConditionInfoItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "DocumentConditionInfoItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "DocumentConditionInfoItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "DocumentConditionInfoItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "DocumentConditionInfoItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerAgreementTransItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/buyeragreementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerAgreementTransListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/buyeragreementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/buyeragreementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransListPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/buyeragreementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/buyeragreementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/buyeragreementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/buyeragreementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/buyeragreementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/buyeragreementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/buyeragreementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/buyeragreementtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BuyerAgreementTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/buyeragreementtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_BUYER_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerInvoiceTableItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/buyerinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerInvoiceTableListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/buyerinvoice/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_BUYER_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerInvoiceTableItemPage", Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/buyerinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerInvoiceTableListPage", Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/buyerinvoice/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_BUYER_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerInvoiceTableItemPage", Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/buyerinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerInvoiceTableListPage", Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/buyerinvoice/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_BUYER_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerInvoiceTableItemPage", Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/buyerinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerInvoiceTableListPage", Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/buyerinvoice/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_BUYER_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerInvoiceTableItemPage", Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/buyerinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerInvoiceTableListPage", Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/buyerinvoice/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_BUYER_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerInvoiceTableItemPage", Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/buyerinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerInvoiceTableListPage", Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/buyerinvoice/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_PROCESS_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/processtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/processtransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_PROCESS_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransItemPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/processtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/processtransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_PROCESS_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransItemPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/processtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/processtransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_PROCESS_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/processtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/processtransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_PROCESS_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransItemPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/processtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/processtransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_PROCESS_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/processtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProcessTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/processtransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_EXTEND_EXPIRY_DATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendExpiryDatePage", Path = "/:site/factoring/creditapptable/:id/function/extendexpirydate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaItemPage",
                            FeatureId = "AuthorizedPersonTransListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedPersonTransListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedPersonTransItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaItemPage",
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/guarantortrans-child/:id"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaItemPage",
                            FeatureId = "AuthorizedPersonTransListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedPersonTransListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedPersonTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaItemPage",
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/guarantortrans-child/:id"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIREPURCHASE_CREDIT_APP_TABLE_AMEND_CA",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaItemPage",
                            FeatureId = "AuthorizedPersonTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedPersonTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedPersonTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaItemPage",
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/guarantortrans-child/:id"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LCDLC_CREDIT_APP_TABLE_AMEND_CA",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaItemPage",
                            FeatureId = "AuthorizedPersonTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedPersonTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedPersonTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaItemPage",
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/guarantortrans-child/:id"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaItemPage",
                            FeatureId = "AuthorizedPersonTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedPersonTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedPersonTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaItemPage",
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/guarantortrans-child/:id"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaItemPage",
                            FeatureId = "AuthorizedPersonTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedPersonTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AuthorizedPersonTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaItemPage",
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "GuarantorTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/guarantortrans-child/:id"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"

                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"

                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"

                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_CREDIT_APPLICATION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditApplicationTransactionItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/creditapplicationtransaction/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditApplicationTransactionListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/creditapplicationtransaction/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PDC",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PdcItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/pdc/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PdcListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/pdc/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_PDC",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PdcItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/pdc/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PdcListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/pdc/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PAYMENT_DETAIL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentDetailItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/paymentdetail/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentDetailListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/paymentdetail/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_PAYMENT_DETAIL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentDetailItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/paymentdetail/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentDetailListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/paymentdetail/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_VENDOR_PAYMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VendorPaymentTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/vendorpaymenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VendorPaymentTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/vendorpaymenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_RECEIPT_TEMP_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempTableItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempTableListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/" },
                        new SysFeatureTableView { ParentFeatureId = "ReceiptTempTableItemPage", FeatureId = "ReceiptTempPaymDetailListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempPaymDetailItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id/receipttemppaymdetail-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_RECEIPT_TEMP_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempTableItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempTableListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/" },
                        new SysFeatureTableView { ParentFeatureId = "ReceiptTempTableItemPage", FeatureId = "ReceiptTempPaymDetailListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempPaymDetailItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id/receipttemppaymdetail-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_RECEIPT_TEMP_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempTableItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempTableListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/" },
                        new SysFeatureTableView { ParentFeatureId = "ReceiptTempTableItemPage", FeatureId = "ReceiptTempPaymDetailListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempPaymDetailItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id/receipttemppaymdetail-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_RECEIPT_TEMP_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempTableItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempTableListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/" },
                        new SysFeatureTableView { ParentFeatureId = "ReceiptTempTableItemPage", FeatureId = "ReceiptTempPaymDetailListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTempPaymDetailItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id/receipttemppaymdetail-child/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_EXTEND_EXPIRY_DATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendExpiryDatePage", Path = "/:site/bond/creditapptable/:id/function/extendexpirydate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_EXTEND_EXPIRY_DATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendExpiryDatePage", Path = "/:site/hirepurchase/creditapptable/:id/function/extendexpirydate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_EXTEND_EXPIRY_DATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendExpiryDatePage", Path = "/:site/lcdlc/creditapptable/:id/function/extendexpirydate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_EXTEND_EXPIRY_DATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendExpiryDatePage", Path = "/:site/leasing/creditapptable/:id/function/extendexpirydate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_EXTEND_EXPIRY_DATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ExtendExpiryDatePage", Path = "/:site/projectfinance/creditapptable/:id/function/extendexpirydate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_CREDIT_APPLICATION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditApplicationTransactionItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/creditapplicationtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditApplicationTransactionListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/creditapplicationtransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_CREDIT_APPLICATION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditApplicationTransactionItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/creditapplicationtransaction/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditApplicationTransactionListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/creditapplicationtransaction/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_CREDIT_APPLICATION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditApplicationTransactionItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/creditapplicationtransaction/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditApplicationTransactionListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/creditapplicationtransaction/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AmendCaLineItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AmendCaLineListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/" },
                        new SysFeatureTableView { ParentFeatureId = "AmendCaLineItemPage", FeatureId = "BillingDocumentConditionTransListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id" },
                        new SysFeatureTableView { ParentFeatureId = "AmendCaLineItemPage", FeatureId = "ReceiptDocumentConditionTransListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BillingDocumentConditionTransItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/billingdocumentconditiontrans-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptDocumentConditionTransItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/receiptdocumentconditiontrans-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaLineItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaLineListPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaLineItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaLineItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaLineItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaLineListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaLineItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaLineItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaLineItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaLineListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaLineItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaLineItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaLineItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaLineListPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaLineItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaLineItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaLineItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AmendCaLineListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaLineItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "AmendCaLineItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/receiptdocumentconditiontrans-child/:id"
                        },
                    }

                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PurchaseTableItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PurchaseTableListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PurchaseLineItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "PurchaseTableItemPage", FeatureId = "PurchaseLineListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PurchaseTableItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PurchaseTableListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PurchaseLineItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "PurchaseTableItemPage", FeatureId = "PurchaseLineListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_VERIFICATION_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIREPURCHASE_CREDIT_APP_TABLE_VERIFICATION_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LCDLC_CREDIT_APP_TABLE_VERIFICATION_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_VERIFICATION_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECTFINANCE_CREDIT_APP_TABLE_VERIFICATION_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectFinance/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectFinance/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_VERIFICATION_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "VerificationItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/verificationtable/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "VerificationListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/verificationtable/"
                        },
                        new SysFeatureTableView { ParentFeatureId = "VerificationItemPage", FeatureId = "VerificationLineListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/verificationtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationLineItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/verificationtable/:id/verificationline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",

                    GroupId = "BOND_CREDIT_APP_TABLE_VERIFICATION_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "VerificationItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/verificationtable/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "VerificationListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/verificationtable/"
                        },
                        new SysFeatureTableView { ParentFeatureId = "VerificationItemPage", FeatureId = "VerificationLineListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/verificationtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationLineItemPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/verificationtable/:id/verificationline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_VERIFICATION_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/verificationtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/verificationtable/" },
                        new SysFeatureTableView { ParentFeatureId = "VerificationItemPage", FeatureId = "VerificationLineListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/verificationtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationLineItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/verificationtable/:id/verificationline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_VERIFICATION_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "VerificationItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/verificationtable/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "VerificationListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/verificationtable/"
                        },
                        new SysFeatureTableView { ParentFeatureId = "VerificationItemPage", FeatureId = "VerificationLineListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/verificationtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationLineItemPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/verificationtable/:id/verificationline-child/:id" },

                    },
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_VERIFICATION_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "VerificationItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/verificationtable/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "VerificationListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/verificationtable/"
                        },
                        new SysFeatureTableView { ParentFeatureId = "VerificationItemPage", FeatureId = "VerificationLineListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/verificationtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationLineItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/verificationtable/:id/verificationline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_VERIFICATION_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "VerificationItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/verificationtable/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "VerificationListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/verificationtable/"
                        },
                        new SysFeatureTableView { ParentFeatureId = "VerificationItemPage", FeatureId = "VerificationLineListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/verificationtable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationLineItemPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/verificationtable/:id/verificationline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_VERIFICATION_TABLE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_VERIFICATION_TABLE_UPDATE_VERIFICATION_STATUS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "updateverificationstatuspage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/verificationtable/:id/function/updateverificationstatus/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_APP_TABLE_VERIFICATION_TABLE_UPDATE_VERIFICATION_STATUS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "updateverificationstatuspage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/verificationtable/:id/function/updateverificationstatus/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_VERIFICATION_TABLE_UPDATE_VERIFICATION_STATUS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "updateverificationstatuspage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/verificationtable/:id/function/updateverificationstatus/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_VERIFICATION_TABLE_UPDATE_VERIFICATION_STATUS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "updateverificationstatuspage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/verificationtable/:id/function/updateverificationstatus/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_VERIFICATION_TABLE_UPDATE_VERIFICATION_STATUS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "updateverificationstatuspage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/verificationtable/:id/function/updateverificationstatus/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_VERIFICATION_TABLE_UPDATE_VERIFICATION_STATUS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "updateverificationstatuspage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/verificationtable/:id/function/updateverificationstatus"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_APP_TABLE_VERIFICATION_TABLE_COPY_VERIFICATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CopyVerificationPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/verificationtable/:id/function/copyverification/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_VERIFICATION_TABLE_COPY_VERIFICATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CopyVerificationPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/verificationtable/:id/function/copyverification/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_VERIFICATION_TABLE_COPY_VERIFICATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CopyVerificationPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/verificationtable/:id/function/copyverification/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_VERIFICATION_TABLE_COPY_VERIFICATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CopyVerificationPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/verificationtable/:id/function/copyverification/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_VERIFICATION_TABLE_COPY_VERIFICATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CopyVerificationPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/verificationtable/:id/function/copyverification/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_VERIFICATION_TABLE_COPY_VERIFICATION",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CopyVerificationPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/verificationtable/:id/function/copyverification/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_LINE_PURCHASE-CHILD_INTEREST_REALIZED_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InterestRealizedTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/interestrealizedtrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InterestRealizedTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/interestrealizedtrans/:id" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_PURCHASE_LINE_ROLL_BILL-CHILD_INTEREST_REALIZED_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InterestRealizedTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/interestrealizedtrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InterestRealizedTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/interestrealizedtrans/:id" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_LINE_PURCHASE-CHILD_VERIFICATION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/verificationtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/verificationtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_LINE_ROLL_BILL-CHILD_VERIFICATION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/verificationtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/verificationtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_LINE_ROLL_BILL-CHILD_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_LINE_PURCHASE-CHILD_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    },
                },
                
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/attachment/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppRequestTableItemPage", FeatureId = "CreditAppRequestLineListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestLineItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_LINE_PURCHASE-CHILD_ADDITIONAL_PURCHASE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "AdditionalPurchaseePage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/function/additionalpurchase/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_UPDATE_EXPECTED_SIGNING_DATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateExpectedSigningDatePage", Path = "/:site/factoring/creditapptable/:id/function/updateexpectedsigningdate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_UPDATE_EXPECTED_SIGNING_DATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateExpectedSigningDatePage", Path = "/:site/bond/creditapptable/:id/function/updateexpectedsigningdate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_UPDATE_EXPECTED_SIGNING_DATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateExpectedSigningDatePage", Path = "/:site/leasing/creditapptable/:id/function/updateexpectedsigningdate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_UPDATE_EXPECTED_SIGNING_DATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateExpectedSigningDatePage", Path = "/:site/lcdlc/creditapptable/:id/function/updateexpectedsigningdate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_UPDATE_EXPECTED_SIGNING_DATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateExpectedSigningDatePage", Path = "/:site/hirepurchase/creditapptable/:id/function/updateexpectedsigningdate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_UPDATE_EXPECTED_SIGNING_DATE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateExpectedSigningDatePage", Path = "/:site/projectfinance/creditapptable/:id/function/updateexpectedsigningdate/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING",
                    FeatureType = FeatureType.PRIMARY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/" },
                        new SysFeatureTableView { ParentFeatureId = "CreditAppRequestTableItemPage", FeatureId = "CreditAppRequestLineListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestLineItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "WithdrawalTableItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "WithdrawalTableListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "WithdrawalLineItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id" },
                        new SysFeatureTableView { ParentFeatureId = "WithdrawalTableItemPage", FeatureId = "WithdrawalLineListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "WithdrawalTableItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "WithdrawalTableListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "WithdrawalLineItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/withdrawallinetermextension-child/:id"},
                            new SysFeatureTableView { ParentFeatureId = "WithdrawalTableItemPage", FeatureId = "WithdrawalLineListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id"}
                        }
                    },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_WITHDRAWAL_LINE_TERM_EXTENSION-CHILD_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/withdrawallinetermextension-child/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/withdrawallinetermextension-child/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/withdrawallinetermextension-child/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/withdrawallinetermextension-child/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_WITHDRAWAL_LINE_TERM_EXTENSION-CHILD_INTEREST_REALIZED_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InterestRealizedTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/withdrawallinetermextension-child/:id/relatedinfo/interestrealizedtrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InterestRealizedTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/withdrawallinetermextension-child/:id/relatedinfo/interestrealizedtrans/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView {
                   ControllerName = "CreditAppTable",
                   GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_MEMO_TRANS",
                   FeatureType = FeatureType.RELATEDINFO,
                       SysFeatureTables = new List<SysFeatureTableView>()
                       {
                           new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/memotrans/:id"},
                           new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/memotrans/"}
                       }
                },
                new SysFeatureGroupControllerMappingView {
                   ControllerName = "CreditAppTable",
                   GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_BUSINESS_COLLATERAL",
                   FeatureType = FeatureType.RELATEDINFO,
                   SysFeatureTables = new List<SysFeatureTableView> () {
                       new SysFeatureTableView {
                           ParentFeatureId = null,
                           FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                           Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/businesscollateral/"
                       },
                       new SysFeatureTableView {
                           ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                           FeatureId = "BusinessCollateralListPage",
                           Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/businesscollateral/"
                       },
                       new SysFeatureTableView {
                           ParentFeatureId = null,
                           FeatureId = "CustBusinessCollateralItemPage",
                           Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                       },
                       new SysFeatureTableView {
                           ParentFeatureId = null,
                           FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                           Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                       },
                   }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { 
                            ParentFeatureId = null, 
                            FeatureId = "CreditOutstandingListPage", 
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/creditoutstanding/" 
                        }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView {
                            ParentFeatureId = null,
                            FeatureId = "AssignmentAgreementOutstandingListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/assignmentagreementoutstanding/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_CREDIT_APP_REQUEST_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List < SysFeatureTableView > () {
                        new SysFeatureTableView {
                            ParentFeatureId = null,
                            FeatureId = "DocumentConditionInfoItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                 {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_CREDIT_APP_REQUEST_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                 new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/retentiontransaction/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/retentiontransaction/"}
                        }
                    },
                 new SysFeatureGroupControllerMappingView {
                   ControllerName = "CreditAppTable",
                   GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_CREDIT_APP_REQUEST_LINE-CHILD_MEMO_TRANS",
                   FeatureType = FeatureType.RELATEDINFO,
                       SysFeatureTables = new List<SysFeatureTableView>()
                       {
                           new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/memotrans/:id"},
                           new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/memotrans/"}
                       }
                },
                new SysFeatureGroupControllerMappingView
                     {
                         ControllerName = "CreditAppTable",
                         GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_CREDIT_APP_REQUEST_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                         FeatureType = FeatureType.RELATEDINFO,
                         SysFeatureTables = new List<SysFeatureTableView>()
                         {
                             new SysFeatureTableView
                             {
                                 ParentFeatureId = null,
                                 FeatureId = "BuyerAgreementTransItemPage",
                                 Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/:id"
                             },
                             new SysFeatureTableView
                             {
                                 ParentFeatureId = null,
                                 FeatureId = "BuyerAgreementTransListPage",
                                 Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/"
                             }
                         }
                     },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_CREDIT_APP_REQUEST_LINE-CHILD_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowCreditAppRequestPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/workflow/startworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowCreditAppRequestPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/startworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowCreditAppRequestPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/startworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowCreditAppRequestPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/startworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowCreditAppRequestPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/startworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowCreditAppRequestPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/startworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowCreditAppRequestPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/startworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowCreditAppRequestPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/actionworkflow"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowCreditAppRequestActionHistoryPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/workflow/actionhistory/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PAYMENT_DETAIL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentDetailItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/paymentdetail/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentDetailListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/paymentdetail/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_PAYMENT_DETAIL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentDetailItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/paymentdetail/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentDetailListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/paymentdetail/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_VERIFICATION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/verificationtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/verificationtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_VERIFICATION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/verificationtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VerificationTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/verificationtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/bookmarkdocumenttrans/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List < SysFeatureTableView > () {
                        new SysFeatureTableView {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/retentionconditiontrans/:id"
                        },
                        new SysFeatureTableView {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/retentionconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView
                            {
                                ParentFeatureId = null,
                                FeatureId = "AttachmentListPage",
                                Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/attachment/"
                            },
                            new SysFeatureTableView
                            {
                                ParentFeatureId = null,
                                FeatureId = "AttachmentItemPage",
                                Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/attachment/:id"
                            },
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_CREDIT_APP_REQUEST_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView
                            {
                                ParentFeatureId = null,
                                FeatureId = "AttachmentListPage",
                                Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/attachment/"
                            },
                            new SysFeatureTableView
                            {
                                ParentFeatureId = null,
                                FeatureId = "AttachmentItemPage",
                                Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/attachment/:id"
                            },
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                  ControllerName = "CreditAppTable", GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_START_WORKFLOW", FeatureType = FeatureType.WORKFLOW, SysFeatureTables = new List < SysFeatureTableView > () {
                    new SysFeatureTableView {
                      ParentFeatureId = null, FeatureId = "StartWorkflowCreditAppRequestPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/workflow/startworkflow"
                    },
                  }
                },
                new SysFeatureGroupControllerMappingView {
                  ControllerName = "CreditAppTable", GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_START_WORKFLOW", FeatureType = FeatureType.WORKFLOW, SysFeatureTables = new List < SysFeatureTableView > () {
                    new SysFeatureTableView {
                      ParentFeatureId = null, FeatureId = "StartWorkflowCreditAppRequestPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/workflow/startworkflow"
                    },
                  }
                },
                new SysFeatureGroupControllerMappingView {
                  ControllerName = "CreditAppTable", GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_START_WORKFLOW", FeatureType = FeatureType.WORKFLOW, SysFeatureTables = new List < SysFeatureTableView > () {
                    new SysFeatureTableView {
                      ParentFeatureId = null, FeatureId = "StartWorkflowCreditAppRequestPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/workflow/startworkflow"
                    },
                  }
                },
                new SysFeatureGroupControllerMappingView {
                  ControllerName = "CreditAppTable", GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_START_WORKFLOW", FeatureType = FeatureType.WORKFLOW, SysFeatureTables = new List < SysFeatureTableView > () {
                    new SysFeatureTableView {
                      ParentFeatureId = null, FeatureId = "StartWorkflowCreditAppRequestPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/workflow/startworkflow"
                    },
                  }
                },
                new SysFeatureGroupControllerMappingView {
                  ControllerName = "CreditAppTable", GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_START_WORKFLOW", FeatureType = FeatureType.WORKFLOW, SysFeatureTables = new List < SysFeatureTableView > () {
                    new SysFeatureTableView {
                      ParentFeatureId = null, FeatureId = "StartWorkflowCreditAppRequestPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/workflow/startworkflow"
                    },
                  }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_EXTEND_TERM",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExtendTermPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/function/extendterm/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "StartWorkflowCreditAppRequestPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/workflow/startworkflow" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ActionWorkflowCreditAppRequestPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/workflow/actionworkflow" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "WorkFlowCreditAppRequestActionHistoryPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/workflow/actionhistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "StartWorkflowCreditAppRequestPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/workflow/startworkflow" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ActionWorkflowCreditAppRequestPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/workflow/actionworkflow" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "WorkFlowCreditAppRequestActionHistoryPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/workflow/actionhistory/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_RECALCULATE_INTEREST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "RecalWithdrawalInterestPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/function/recalculateinterest/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_SERVICE_FEE_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/servicefeetrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/servicefeetrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/"},
                            new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id"},
                        }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_SERVICE_FEE_TRANS_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/"},
                            new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id"},
                        }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView
                            {
                                ParentFeatureId = null,
                                FeatureId = "AttachmentListPage",
                                Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/attachment/"
                            },
                            new SysFeatureTableView
                            {
                                ParentFeatureId = null,
                                FeatureId = "AttachmentItemPage",
                                Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/attachment/:id"
                            },
                        }
                    },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView {
                            ParentFeatureId = null,
                            FeatureId = "CreditOutstandingListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/creditoutstanding/"
                        }
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView {
                            ParentFeatureId = null,
                            FeatureId = "AssignmentAgreementOutstandingListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/assignmentagreementoutstanding/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView {
                   ControllerName = "CreditAppTable",
                   GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_MEMO_TRANS",
                   FeatureType = FeatureType.RELATEDINFO,
                       SysFeatureTables = new List<SysFeatureTableView>()
                       {
                           new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/memotrans/:id"},
                           new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/memotrans/"}
                       }
                },
                new SysFeatureGroupControllerMappingView {
                   ControllerName = "CreditAppTable",
                   GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_BUSINESS_COLLATERAL",
                   FeatureType = FeatureType.RELATEDINFO,
                   SysFeatureTables = new List<SysFeatureTableView> () {
                       new SysFeatureTableView {
                           ParentFeatureId = null,
                           FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                           Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/businesscollateral/"
                       },
                       new SysFeatureTableView {
                           ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                           FeatureId = "BusinessCollateralListPage",
                           Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/businesscollateral/"
                       },
                       new SysFeatureTableView {
                           ParentFeatureId = null,
                           FeatureId = "CustBusinessCollateralItemPage",
                           Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                       },
                       new SysFeatureTableView {
                           ParentFeatureId = null,
                           FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                           Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                       },
                   }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/bookmarkdocumenttrans/"}
                        }
                    },
                 new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/retentiontransaction/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/retentiontransaction/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_RETENTION_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List < SysFeatureTableView > () {
                        new SysFeatureTableView {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/retentionconditiontrans/:id"
                        },
                        new SysFeatureTableView {
                            ParentFeatureId = null,
                            FeatureId = "RetentionConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/retentionconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                 new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_CREDIT_APP_REQUEST_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView
                            {
                                ParentFeatureId = null,
                                FeatureId = "AttachmentListPage",
                                Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/attachment/"
                            },
                            new SysFeatureTableView
                            {
                                ParentFeatureId = null,
                                FeatureId = "AttachmentItemPage",
                                Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/attachment/:id"
                            },
                        }
                    },
                    new SysFeatureGroupControllerMappingView
                     {
                         ControllerName = "CreditAppTable",
                         GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_CREDIT_APP_REQUEST_LINE-CHILD_BUYER_AGREEMENT_TRANS",
                         FeatureType = FeatureType.RELATEDINFO,
                         SysFeatureTables = new List<SysFeatureTableView>()
                         {
                             new SysFeatureTableView
                             {
                                 ParentFeatureId = null,
                                 FeatureId = "BuyerAgreementTransItemPage",
                                 Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/:id"
                             },
                             new SysFeatureTableView
                             {
                                 ParentFeatureId = null,
                                 FeatureId = "BuyerAgreementTransListPage",
                                 Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/"
                             }
                         }
                     },
                new SysFeatureGroupControllerMappingView {
                   ControllerName = "CreditAppTable",
                   GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_CREDIT_APP_REQUEST_LINE-CHILD_MEMO_TRANS",
                   FeatureType = FeatureType.RELATEDINFO,
                       SysFeatureTables = new List<SysFeatureTableView>()
                       {
                           new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/memotrans/:id"},
                           new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/memotrans/"}
                       }
                },
                 new SysFeatureGroupControllerMappingView
                 {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_CREDIT_APP_REQUEST_LINE-CHILD_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_CREDIT_APP_REQUEST_LINE-CHILD_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_CREDIT_APP_REQUEST_LINE-CHILD_DOCUMENT_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List <SysFeatureTableView> () {
                        new SysFeatureTableView {
                            ParentFeatureId = null,
                            FeatureId = "DocumentConditionInfoItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "BillingDocumentConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView {
                            ParentFeatureId = "DocumentConditionInfoItemPage",
                            FeatureId = "ReceiptDocumentConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/"
                        },
                        new SysFeatureTableView {
                            ParentFeatureId = null,
                            FeatureId = "BillingDocumentConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/billingdocumentconditiontrans-child/:id"
                        },
                        new SysFeatureTableView {
                            ParentFeatureId = null,
                            FeatureId = "ReceiptDocumentConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/documentconditiontrans/receiptdocumentconditiontrans-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/memotrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/memotrans/"}
                        }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_LINE_WITHDRAWAL-CHILD_INVOICE_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceTableListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/" },
                        new SysFeatureTableView { ParentFeatureId = "InvoiceTableItemPage", FeatureId = "InvoiceLineListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceLineItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/:id/invoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_LINE_WITHDRAWAL-CHILD_INTEREST_REALIZED_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InterestRealizedTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/interestrealizedtrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InterestRealizedTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/interestrealizedtrans/:id" }

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                 ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "MemoItemPage",  Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "MemoListPage",  Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/memotrans/"}
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                 ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "MemoItemPage",  Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "MemoListPage",  Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/memotrans/"}
                    }
                 },
                new SysFeatureGroupControllerMappingView
                {
                 ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "MemoItemPage",  Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "MemoListPage",  Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/memotrans/"}
                    }
                 },
                new SysFeatureGroupControllerMappingView
                {
                 ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "MemoItemPage",  Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "MemoListPage",  Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/memotrans/"}
                    }
                 },

                new SysFeatureGroupControllerMappingView
                {
                 ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "MemoItemPage",  Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "MemoListPage",  Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/memotrans/"}
                    }
                 },
                new SysFeatureGroupControllerMappingView
                {
                 ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "MemoItemPage",  Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "MemoListPage",  Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/memotrans/"}
                    }
                 },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PROJECT_PROGRESS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProjectProgressItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/projectprogress/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProjectProgressListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/projectprogress/" }
                    }
                },
                   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_VENDOR_PAYMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VendorPaymentTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/vendorpaymenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "VendorPaymentTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/vendorpaymenttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PROJECT_PROGRESS_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/projectprogress/:id/relatedinfo/attachment/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/projectprogress/:id/relatedinfo/attachment/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PROJECT_PROGRESS_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/projectprogress/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/projectprogress/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditTransItemPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialcredittrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditTransListPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialcredittrans/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditTransItemPage",Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialcredittrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditTransListPage",Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialcredittrans/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditTransItemPage",Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialcredittrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditTransListPage",Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialcredittrans/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditTransItemPage",Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialcredittrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditTransListPage",Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialcredittrans/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditTransItemPage",Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialcredittrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditTransListPage",Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialcredittrans/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditTransItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialcredittrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditTransListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialcredittrans/"}
                        }
                    },

                  new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_FINANCE_CREDIT_APP_TABLE_AMEND_CA_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NCBTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/ncbtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NCBTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/ncbtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_FINANCE_CREDIT_APP_TABLE_AMEND_CA_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBTransListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_FINANCE_CREDIT_APP_TABLE_AMEND_CA_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_FINANCE_CREDIT_APP_TABLE_AMEND_CA_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_FINANCE_CREDIT_APP_TABLE_AMEND_CA_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_FINANCE_CREDIT_APP_TABLE_AMEND_CA_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/ncbtrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "NCBTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/ncbtrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowPurchasePage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/workflow/startworkflow"
                        },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowPurchasePage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/workflow/actionworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/bookmarkdocumenttrans/"}
                  }
                    },
                 new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableBookmarkDocumentItemPage",Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            //new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableBookmarkDocumentListPage",Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/bookmarkdocumenttrans/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage",Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage",Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/bookmarkdocumenttrans/"}
                     }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableBookmarkDocumentItemPage",Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableBookmarkDocumentListPage",Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/bookmarkdocumenttrans/"}
                    }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage",Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage",Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage",Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage",Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/bookmarkdocumenttrans/"}
                     }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableBookmarkDocumentItemPage",Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableBookmarkDocumentListPage",Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/bookmarkdocumenttrans/"}
                    }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage",Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage",Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage",Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage",Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/bookmarkdocumenttrans/"}
                     }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableBookmarkDocumentItemPage",Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableBookmarkDocumentListPage",Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/bookmarkdocumenttrans/"}
                    }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage",Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage",Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage",Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage",Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/bookmarkdocumenttrans/"}
                        }
                    },
				  new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/bookmarkdocumenttrans/"}
                        }
                    },
                    new SysFeatureGroupControllerMappingView {
                        ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableBookmarkDocumentItemPage",Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableBookmarkDocumentListPage",Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/bookmarkdocumenttrans/"}
                    }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage",Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage",Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {                        
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableBookmarkDocumentItemPage",Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/bookmarkdocumenttrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditAppRequestTableBookmarkDocumentListPage",Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/bookmarkdocumenttrans/"}
                        }
                    },              
                new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/:id"},
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/"}
                        }
                    },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowPurchaseActionHistoryPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/workflow/actionhistory/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/" }
                     }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransItemPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_START_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "StartWorkflowPurchasePage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/workflow/startworkflow"
                        },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_ACTION_WORKFLOW",
                    FeatureType = FeatureType.WORKFLOW,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ActionWorkflowPurchasePage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/workflow/actionworkflow"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransItemPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_ACTION_HISTORY",
                    FeatureType = FeatureType.WORKFLOW_ACTIONHISTORY,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "WorkFlowPurchaseActionHistoryPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/workflow/actionhistory/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/" }
                     }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransItemPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/" }
                     }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/" }
                    },
                },        
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_MEMO_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitMemoItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitMemoListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/memotrans/" },
                  }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_MEMO_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitMemoItemPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitMemoListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/memotrans/" },
                   
                     }
                },
                    new SysFeatureGroupControllerMappingView
                {
                         ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitItemPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_MEMO_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitMemoItemPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitMemoListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/memotrans/" },

                     }
                },  
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitItemPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_MEMO_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitMemoItemPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitMemoListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/memotrans/" },
                    
                       }
                },new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable", GroupId = "LC_DLC_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitItemPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_MEMO_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitMemoItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitMemoListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/memotrans/" },
                  
                     }
                },
                  
                   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",  GroupId = "LEASING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_MEMO_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitMemoItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitMemoListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/memotrans/" },
                   
                       }
                } ,
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CloseCreditLimitListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_BOOKMARK_DOCUMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BookmarkDocumentListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentItemPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/attachment/:id"},
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentListPage",Path = "/:site/factoring/creditapptable/:id/relatedinfo/attachment/"},
                    }
                },
                   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentItemPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/attachment/:id"},
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentListPage",Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/attachment/"},
                    }
                },
                   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentItemPage",Path = "/:site/leasing/creditapptable/:id/relatedinfo/attachment/:id"},
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentListPage",Path = "/:site/leasing/creditapptable/:id/relatedinfo/attachment/"},
                    }
                },
                   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentItemPage",Path = "/:site/bond/creditapptable/:id/relatedinfo/attachment/:id"},
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentListPage",Path = "/:site/bond/creditapptable/:id/relatedinfo/attachment/"},
                    }
                }
                   ,
                   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentItemPage",Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/attachment/:id"},
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentListPage",Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/attachment/"},
                    }
                } ,
                   new SysFeatureGroupControllerMappingView
                {

                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentItemPage",Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/attachment/:id"},
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "AttachmentListPage",Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/attachment/"},
                    }
                },
                   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PDC",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PdcItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/pdc/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PdcListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/pdc/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/attachment/" }
                     }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/attachment/" }
                     }
                },
                        new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/attachment/" }
                     }
                },
                         new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/attachment/" }
                     }
                },
                     new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/attachment/" }
                     }
                },
                          new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/attachment/" }
                     }
                },
                new SysFeatureGroupControllerMappingView
                {
                 ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_AUTHORIZEDPERSON_TRANS-CHILD_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "NCBTransItemPage",  Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id/relatedinfo/ncbtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "NCBTransListPage",  Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id/relatedinfo/ncbtrans/"}
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                 ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_AUTHORIZEDPERSON_TRANS-CHILD_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "NCBTransItemPage",  Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id/relatedinfo/ncbtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "NCBTransListPage",  Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id/relatedinfo/ncbtrans/"}
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                 ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_AUTHORIZEDPERSON_TRANS-CHILD_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "NCBTransItemPage",  Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id/relatedinfo/ncbtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "NCBTransListPage",  Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id/relatedinfo/ncbtrans/"}
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                 ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_AUTHORIZEDPERSON_TRANS-CHILD_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "NCBTransItemPage",  Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id/relatedinfo/ncbtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "NCBTransListPage",  Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id/relatedinfo/ncbtrans/"}
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                 ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_AUTHORIZEDPERSON_TRANS-CHILD_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "NCBTransItemPage",  Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id/relatedinfo/ncbtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "NCBTransListPage",  Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id/relatedinfo/ncbtrans/"}
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                 ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_AUTHORIZEDPERSON_TRANS-CHILD_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "NCBTransItemPage",  Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id/relatedinfo/ncbtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null,FeatureId = "NCBTransListPage",  Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/authorizedpersontrans-child/:id/relatedinfo/ncbtrans/"}
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/attachment/" }
                    },

                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/attachment/" }
                    },

                },
                  new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/attachment/" }
                    },

                },
                  new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/attachment/" }
                    },

                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/attachment/" }
                    },

                },
                  new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/attachment/" }
                    },

                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_TABLE_SEND_EMAIL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "SendEmailPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/function/purchasetablesendemail/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_PURCHASE_TABLE_SEND_EMAIL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "SendEmailPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/function/purchasetablesendemail/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_PURCHASE_TABLE_PURCHASE_INVOICE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/invoicesettlement/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/invoicesettlement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_WITHDRAWAL_TABLE_WITHDRAWAL_INVOICE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/invoicesettlement/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/invoicesettlement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_PURCHASE_TABLE_PURCHASE_SUSPENSE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SuspenseSettlementItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/suspensesettlement/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SuspenseSettlementListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/suspensesettlement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_PURCHASE_TABLE_ROLL_BILL_SUSPENSE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SuspenseSettlementItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/suspensesettlement/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SuspenseSettlementListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/suspensesettlement/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_WITHDRAWAL_TABLE_WITHDRAWAL_SUSPENSE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/suspensesettlement/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/suspensesettlement/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_INQUIRY_CUSTOMER_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "InquiryCustomerBuyerCreditOutstandingListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/customerbuyercreditoutstanding/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_INQUIRY_CUSTOMER_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "InquiryCustomerBuyerCreditOutstandingListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/customerbuyercreditoutstanding/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_INQUIRY_CUSTOMER_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InquiryCustomerBuyerCreditOutstandingListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/customerbuyercreditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_INQUIRY_CUSTOMER_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "InquiryCustomerBuyerCreditOutstandingListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/customerbuyercreditoutstanding/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_INQUIRY_CUSTOMER_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "InquiryCustomerBuyerCreditOutstandingListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/customerbuyercreditoutstanding/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_INQUIRY_CUSTOMER_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "InquiryCustomerBuyerCreditOutstandingListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/customerbuyercreditoutstanding/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/attachment/" }
                    }
                },
                  new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/attachment/" }
                    }
                },
                        new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/attachment/" }
                    }
                },
                        new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/attachment/" }
                    }
                },
                         new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/attachment/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/attachment/" }
                    } 
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_VERIFICATION_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_VERIFICATION_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_VERIFICATION_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_VERIFICATION_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_VERIFICATION_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_VERIFICATION_TABLE_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/verificationtable/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/attachment/" }
                    }
                },
                
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_RETENTION_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/retentionoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_RETENTION_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/retentionoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_RETENTION_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/retentionoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_RETENTION_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/retentionoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_RETENTION_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/retentionoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_RETENTION_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionOutstandingListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/retentionoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_POST_WITHDRAWAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PostWithdrawalPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/function/postwithdrawal/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_POST_WITHDRAWAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PostWithdrawalPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/function/postwithdrawal/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_CANCEL_WITHDRAWAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CancelWithdrawalPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/function/cancelwithdrawal/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_CANCEL_WITHDRAWAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CancelWithdrawalPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/function/cancelwithdrawal/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_TABLE_CANCEL_PURCHASE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CancelPurchasePage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/function/cancelpurchase/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_PURCHASE_TABLE_CANCEL_PURCHASE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CancelPurchasePage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/function/cancelpurchase/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_LINE_PURCHASE-CHILD_INQUIRY_ROLL_BILL_PURCHASE_LINE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InquiryRollBillPurchaseLineListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/inquiryrollbillpurchaseline/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InquiryRollBillPurchaseLineItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/inquiryrollbillpurchaseline/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_PRINT_SET_BOOK_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_PRINT_SET_BOOK_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_PRINT_SET_BOOK_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_PRINT_SET_BOOK_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_PRINT_SET_BOOK_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/function/printsetbookdoctrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_PRINT_SET_BOOK_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/function/printsetbookdoctrans/" }
                    }
                },
          new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_PRINT_BOOK_DOC_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/printsetbookdoctrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/printsetbookdoctrans/" },
                  }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_PRINT_BOOK_DOC_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocItemPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/printsetbookdoctrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/printsetbookdoctrans/" },

                     }
                },
               

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_PRINT_BOOK_DOC_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocMemoItemPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/printsetbookdoctrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocMemoListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/printsetbookdoctrans/" },

                     }
                },
              
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_PRINT_BOOK_DOC_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocItemPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/printsetbookdoctrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/printsetbookdoctrans/" },

                       }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_PRINT_BOOK_DOC_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/printsetbookdoctrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/printsetbookdoctrans/" },

                     }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_PRINT_BOOK_DOC_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocMemoItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/printsetbookdoctrans/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocMemoListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/printsetbookdoctrans/" },

                       }
                } ,
                      new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {
                          
                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocItemPage",Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/printsetbookdoctrans/"}
                        }
                    },

                      new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {

                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocItemPage",Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/printsetbookdoctrans/"}
                        }
                    },

                      new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {

                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocItemPage",Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/printsetbookdoctrans/"}
                        }
                    },

                      new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {

                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocItemPage",Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/printsetbookdoctrans/"}
                        }
                    },
                       new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {

                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocItemPage",Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/printsetbookdoctrans/"}
                        }
                    },
                        new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_PRINT_SET_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {

                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocItemPage",Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/printsetbookdoctrans/"}
                        }
                    },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_UPDATE_CLOSING_RESULT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateClosingResultPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/updateclosingresult/" },
                  }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_UPDATE_CLOSING_RESULT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateClosingResultPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/updateclosingresult/" },

                     }
                },


                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_UPDATE_CLOSING_RESULT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateClosingResultPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/updateclosingresult/" },

                     }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_UPDATE_CLOSING_RESULT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateClosingResultPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/updateclosingresult/" },

                       }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_UPDATE_CLOSING_RESULT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateClosingResultPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/updateclosingresult/" },

                     }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_UPDATE_CLOSING_RESULT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateClosingResultPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/updateclosingresult/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PRODUCT_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSettledTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/productsettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSettledTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/productsettlement/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_LINE_PURCHASE-CHILD_PURCHASE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSettledTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/purchasesettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSettledTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/purchasesettlement/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_LINE_PURCHASE-CHILD_PRODUCT_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSettledTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/productsettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSettledTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/productsettlement/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {                                                                                                      
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" },
                      
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_TAX_REPORT_TRANS_COPY_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyTaxReportTransPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/function/copytaxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_TAX_REPORT_TRANS_COPY_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyTaxReportTransPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/function/copytaxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_TAX_REPORT_TRANS_COPY_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyTaxReportTransPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/function/copytaxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_TAX_REPORT_TRANS_COPY_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyTaxReportTransPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/function/copytaxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_TAX_REPORT_TRANS_COPY_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyTaxReportTransPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/function/copytaxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_TAX_REPORT_TRANS_COPY_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyTaxReportTransPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/taxreporttrans/function/copytaxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_CREDIT_APPLICATION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditApplicationTransactionItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/creditapplicationtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditApplicationTransactionListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/creditapplicationtransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_CREDIT_APPLICATION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditApplicationTransactionItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/creditapplicationtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditApplicationTransactionListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/creditapplicationtransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_TABLE_PRINT_PURCHASE",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "printPurchasePage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/report/printpurchase/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_PRINT_PURCHASE_ROLLBILL",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintPurchaseRollbillPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/report/printpurchaserollbill/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_TABLE_POST_PURCHASE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PostPurchasePage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/function/postpurchase/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_PURCHASE_TABLE_POST_PURCHASE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PostPurchasePage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/function/postpurchase/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_APP_TABLE_VERIFICATION_TABLE_PRINT_VERIFICATION",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintVerificationPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/verificationtable/:id/report/printverification/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_VERIFICATION_TABLE_PRINT_VERIFICATION",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintVerificationPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/verificationtable/:id/report/printverification/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_VERIFICATION_TABLE_PRINT_VERIFICATION",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintVerificationPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/verificationtable/:id/report/printverification/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_VERIFICATION_TABLE_PRINT_VERIFICATION",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintVerificationPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/verificationtable/:id/report/printverification/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_VERIFICATION_TABLE_PRINT_VERIFICATION",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintVerificationPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/verificationtable/:id/report/printverification/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_VERIFICATION_TABLE_PRINT_VERIFICATION",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintVerificationPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/verificationtable/:id/report/printverification/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PDC_DEPOSIT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DepositPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/pdc/:id/function/deposit/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PDC_COMPLETE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ComplatePage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/pdc/:id/function/complete/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PDC_CANCEL_CHEQUE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/pdc/:id/function/cancelcheque/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PDC_RETURN_CHEQUE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReturnPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/pdc/:id/function/returncheque/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PDC_REPLACE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReplacePage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/pdc/:id/function/replace/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PDC_BOUNCE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BouncePage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/pdc/:id/function/bounce/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PDC_BOUNCE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BouncePage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/pdc/:id/function/bounce/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PDC_DEPOSIT",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "DepositPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/pdc/:id/function/deposit/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PDC_COMPLETE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ComplatePage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/pdc/:id/function/complete/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PDC_CANCEL_CHEQUE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/pdc/:id/function/cancelcheque/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PDC_RETURN_CHEQUE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReturnPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/pdc/:id/function/returncheque/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PDC_REPLACE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReplacePage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/pdc/:id/function/replace/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PRINT_WITHDRAWAL",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintWithdrawalTablePage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/report/printwithdrawal/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_PRINT_WITHDRAWAL_TERM_EXTENSION",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintTermExtensionPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/report/printwithdrawaltermextension/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                                            }
                },
                  new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/retentiontransaction/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/retentiontransaction/" }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_SERVICE_FEE_TRANS_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/servicefeetrans/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                                            }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_RETENTION_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/retentiontransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "RetentionTransactionListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/retentiontransaction/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerAgreementTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/buyeragreementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "BuyerAgreementTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/buyeragreementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentDetailItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/buyeragreementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentDetailListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/buyeragreementtrans/" }
                    }
                },
                   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_PRINT_WITHDRAWAL_TERM_EXTENSION",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintTermExtensionPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/report/printwithdrawaltermextension/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/servicefeeconditiontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/servicefeeconditiontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransItemPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/servicefeeconditiontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/servicefeeconditiontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_CREDIT_APP_REQUEST_LINE-CHILD_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_CREDIT_APP_REQUEST_LINE-CHILD_FINANCIAL_STATEMENT_TRANS_COPY_FINANCIAL_STATEMENT_TRANS_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CopyFinancialStatementTransPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/financialstatementtrans/function/copyfinancialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_VENDOR_PAYMENT_TRANS_SEND_VENDOR_INVOICE_STAGING",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView 
                        { 
                            ParentFeatureId = null, 
                            FeatureId = "SendVendorInvoiceStagingPage", 
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/vendorpaymenttrans/function/sendvendorinvoicestaging/"
                        }
                    }
                },  
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_PDC",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PdcItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/pdc/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PdcListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/pdc/" }
                    }
                },  
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/memotrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_PROJECT_PROGRESS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProjectProgressListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/projectprogress/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProjectProgressItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/projectprogress/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_PROJECT_PROGRESS_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/projectprogress/:id/relatedinfo/attachment/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/projectprogress/:id/relatedinfo/attachment/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_PROJECT_PROGRESS_MEMO_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/projectprogress/:id/relatedinfo/memotrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "MemoListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/projectprogress/:id/relatedinfo/memotrans/" }
                    }
                },  
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_ATTACHMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/attachment/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AttachmentListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/attachment/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintBookDocTransPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintBookDocTransPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/"
                        }
                    }
                },new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_PRODUCT_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSettledTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/productsettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSettledTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/productsettlement/:id" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_SUSPENSE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SuspenseSettlementItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/suspensesettlement/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "SuspenseSettlementListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/suspensesettlement/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ASSIGNMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentInfoItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "ExistingAssignmentAgreementListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppReqAssignmentInfoItemPage",
                            FeatureId = "CreditAppReqAssignmentListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ExistingAssignmentAgreementItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/existingassignment-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppReqAssignmentItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignment/creditapprequesttableassignment-child/:id"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                     {
                         ControllerName = "CreditAppTable",
                         GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_CREDIT_APP_REQUEST_LINE-CHILD_BUYER_AGREEMENT_TRANS_COPY_BUYER_AGREEMENT_TRANS",
                         FeatureType = FeatureType.FUNCTION,
                         SysFeatureTables = new List<SysFeatureTableView>()
                         {
                             new SysFeatureTableView
                             {
                                 ParentFeatureId = null,
                                 FeatureId = "CopyBuyerAgreementTransPage",
                                 Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/function/copybuyeragreementtrans/"
                             },
                         }
                     },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECTFINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_CREDIT_APP_REQUEST_LINE-CHILD_BUYER_AGREEMENT_TRANS_COPY_BUYER_AGREEMENT_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CopyBuyerAgreementTransPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/creditapprequestline-child/:id/relatedinfo/buyeragreementtrans/function/copybuyeragreementtrans/"
                        },
                    }
                },                
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_VENDOR_PAYMENT_TRANS_SEND_VENDOR_INVOICE_STAGING",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "SendVendorInvoiceStagingPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/vendorpaymenttrans/function/sendvendorinvoicestaging/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/servicefeeconditiontrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ServiceFeeConditionTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/servicefeeconditiontrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_SERVICE_FEE_CONDITION_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/servicefeeconditiontrans/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "ServiceFeeConditionTransListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/servicefeeconditiontrans/"
                        }
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },
                        
                    }
                },   new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },

                    }
                },
                       new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },

                    }
                },
                          new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },

                    }
                },
                             new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },

                    }
                },
                                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_BOOKMARK_DOCUMENT_TRANS_PRINT_BOOK_DOC_TRANS",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintBookDocTransPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/bookmarkdocumenttrans/:id/function/printbookdoctrans/" },

                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_UPDATE_STATUS_ROLLBILL_PURCHASE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "UpdateStatusRollbillPurchasePage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/function/updatestatusrollbillpurchase/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_UPDATE_STATUS_ROLLBILL_PURCHASE",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "UpdateStatusRollbillPurchasePage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/function/updatestatusrollbillpurchase/"
                        }
                    }
                },
                    new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_UPDATE_STATUS_AMEND_BUYER_INFO",
                    FeatureType = FeatureType.FUNCTION,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {

                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateStatusAmendBuyerInfoPage",Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/updatestatusamendbuyerinfo/"}
                        }
                    },
                    new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_UPDATE_STATUS_AMEND_BUYER_INFO",
                    FeatureType = FeatureType.FUNCTION,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {

                            new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateStatusAmendBuyerInfoPage",Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/updatestatusamendbuyerinfo/"}
                        }
                    },

                    new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_UPDATE_STATUS_AMEND_BUYER_INFO",
                    FeatureType = FeatureType.FUNCTION,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {

                          new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateStatusAmendBuyerInfoPage",Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/updatestatusamendbuyerinfo/"}
                        }
                    },

                    new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_UPDATE_STATUS_AMEND_BUYER_INFO",
                    FeatureType = FeatureType.FUNCTION,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {

                          new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateStatusAmendBuyerInfoPage",Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/updatestatusamendbuyerinfo/"}
                        }
                    },
                     new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_UPDATE_STATUS_AMEND_BUYER_INFO",
                    FeatureType = FeatureType.FUNCTION,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {

                          new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateStatusAmendBuyerInfoPage",Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/updatestatusamendbuyerinfo/"}
                        }
                    },
                      new SysFeatureGroupControllerMappingView {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_UPDATE_STATUS_AMEND_BUYER_INFO",
                    FeatureType = FeatureType.FUNCTION,
                        SysFeatureTables = new List<SysFeatureTableView>()
                        {

                          new SysFeatureTableView { ParentFeatureId = null, FeatureId = "UpdateStatusAmendBuyerInfoPage",Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/updatestatusamendbuyerinfo/"}
                        }
                    },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_LINE_PURCHASE-CHILD_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_LINE_PURCHASE-CHILD_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_LINE_PURCHASE-CHILD_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                                            }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_LINE_PURCHASE-CHILD_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_PURCHASE_LINE_PURCHASE-CHILD_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/purchaselinepurchase-child/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_AMEND_CA_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/amendca/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_AMEND_CA_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/amendca/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_AMEND_CA_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/amendca/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_AMEND_CA_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/amendca/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_AMEND_CA_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/amendca/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_AMEND_CA_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/amendca/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/cancelcreditapprequest/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/cancelcreditapprequest/" }
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/cancelcreditapprequest/" }
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LCDLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/factoring/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/cancelcreditapprequest/" }
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/cancelcreditapprequest/" }
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIREPURCHASE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/cancelcreditapprequest/" }
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LCDLC_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/cancelcreditapprequest/" }
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CANCEL_CREDIT_APP_REQUEST",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelCreditAppRequestPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/function/cancelcreditapprequest/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_ASSIGNMENT_AGREEMENT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "AssignmentAgreementOutstandingListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/assignmentagreementoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CreditOutstandingListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/creditoutstanding/" }
                    }
                },
                    new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/bond/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/leasing/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_CLOSE_CREDIT_LIMIT_CA_BUYER_CREDIT_OUTSTANDING",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CaBuyerCreditOutstandingListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/closecreditlimit/:id/relatedinfo/cabuyercreditoutstanding/"
                        },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/taxreporttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/taxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/financialcredittrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/financialcredittrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/financialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_BUYER_MATCHING_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NCBItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/ncbtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NCBListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/buyermatching/:id/relatedinfo/ncbtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_TAX_REPORT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/taxreporttrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxReportTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/taxreporttrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_FINANCIAL_CREDIT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/financialcredittrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialCreditListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/financialcredittrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_FINANCIAL_STATEMENT_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/financialstatementtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "FinancialStatementTransListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/financialstatementtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_LOAN_REQUEST_NCB_TRANS",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NCBItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/ncbtrans/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "NCBListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/loanrequest/:id/relatedinfo/ncbtrans/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "BOND_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/bond/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    },
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "HIRE_PURCHASE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/hirepurchase/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    },
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LC_DLC_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/lcdlc/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    },
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "LEASING_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/leasing/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    },
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_LINE-CHILD_AMEND_CA_LINE_BUSINESS_COLLATERAL",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = "CreditAppRequestTableBusinessCollateralInfoItemPage",
                            FeatureId = "BusinessCollateralListPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CustBusinessCollateralItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/custbusinesscollateral-child/:id"
                        },
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "CreditAppRequestTableBusinessCollateralItemPage",
                            Path = "/:site/projectfinance/creditapptable/:id/creditappline-child/:id/relatedinfo/amendcaline/:id/relatedinfo/businesscollateral/creditapprequesttablebusinesscollateral-child/:id"
                        },
                    },
                },


                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_LINE_WITHDRAWAL-CHILD_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_LINE_WITHDRAWAL-CHILD_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_LINE_WITHDRAWAL-CHILD_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                                            }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_LINE_WITHDRAWAL-CHILD_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_LINE_WITHDRAWAL-CHILD_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/withdrawallinewithdrawal-child/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_RECEIPT_TEMP_TABLE_INVOICE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id/relatedinfo/invoicesettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id/relatedinfo/invoicesettlement/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_RECEIPT_TEMP_TABLE_RECEIPT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTableListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTableItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = "ReceiptTableItemPage", FeatureId = "ReceiptLineListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptLineLineItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id/receiptline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_RECEIPT_TEMP_TABLE_RECEIPT_TABLE_PRINT_RECEIPT_COPY",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintReceiptCopy",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id/report/printreceiptcopy/"
                        }
                    }
                },
               new SysFeatureGroupControllerMappingView
               {
                    ControllerName = "ReceiptTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_RECEIPT_TEMP_TABLE_RECEIPT_TABLE_PRINT_RECEIPT_ORIGINAL",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                             FeatureId = "PrintReceiptOriginal",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id/report/printreceiptoriginal/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_RECEIPT_TEMP_TABLE_CANCEL_RECEIPT_TEMP",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelReceiptTempPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id/function/cancelreceipttemp/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_RECEIPT_TEMP_TABLE_POST_RECEIPT_TEMP",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostReceiptTempPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id/function/postreceipttemp/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_PURCHASE_RECEIPT_TEMP_TABLE_PRINT_RECEIPT_TEMP",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintReceiptTempPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablepurchase/:id/relatedinfo/receipttemptable/:id/report/printreceipttemp/"
                        }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_RECEIPT_TEMP_TABLE_INVOICE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id/relatedinfo/invoicesettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id/relatedinfo/invoicesettlement/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_RECEIPT_TEMP_TABLE_RECEIPT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTableListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTableItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = "ReceiptTableItemPage", FeatureId = "ReceiptLineListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptLineLineItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id/receiptline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_RECEIPT_TEMP_TABLE_RECEIPT_TABLE_PRINT_RECEIPT_COPY",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintReceiptCopy",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id/report/printreceiptcopy/"
                        }
                    }
                },
               new SysFeatureGroupControllerMappingView
               {
                    ControllerName = "ReceiptTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_RECEIPT_TEMP_TABLE_RECEIPT_TABLE_PRINT_RECEIPT_ORIGINAL",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                             FeatureId = "PrintReceiptOriginal",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id/report/printreceiptoriginal/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_RECEIPT_TEMP_TABLE_CANCEL_RECEIPT_TEMP",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelReceiptTempPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id/function/cancelreceipttemp/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_RECEIPT_TEMP_TABLE_POST_RECEIPT_TEMP",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostReceiptTempPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id/function/postreceipttemp/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_RECEIPT_TEMP_TABLE_PRINT_RECEIPT_TEMP",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintReceiptTempPage",
                            Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/relatedinfo/receipttemptable/:id/report/printreceipttemp/"
                        }
                    }
                },


                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_RECEIPT_TEMP_TABLE_INVOICE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id/relatedinfo/invoicesettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id/relatedinfo/invoicesettlement/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_RECEIPT_TEMP_TABLE_RECEIPT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTableListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTableItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = "ReceiptTableItemPage", FeatureId = "ReceiptLineListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptLineLineItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id/receiptline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_RECEIPT_TEMP_TABLE_RECEIPT_TABLE_PRINT_RECEIPT_COPY",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintReceiptCopy",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id/report/printreceiptcopy/"
                        }
                    }
                },
               new SysFeatureGroupControllerMappingView
               {
                    ControllerName = "ReceiptTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_RECEIPT_TEMP_TABLE_RECEIPT_TABLE_PRINT_RECEIPT_ORIGINAL",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                             FeatureId = "PrintReceiptOriginal",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id/report/printreceiptoriginal/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_RECEIPT_TEMP_TABLE_CANCEL_RECEIPT_TEMP",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelReceiptTempPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id/function/cancelreceipttemp/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_RECEIPT_TEMP_TABLE_POST_RECEIPT_TEMP",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostReceiptTempPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id/function/postreceipttemp/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_RECEIPT_TEMP_TABLE_PRINT_RECEIPT_TEMP",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintReceiptTempPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/receipttemptable/:id/report/printreceipttemp/"
                        }
                    }
                },


                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_RECEIPT_TEMP_TABLE_INVOICE_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id/relatedinfo/invoicesettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "InvoiceSettlementItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id/relatedinfo/invoicesettlement/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_RECEIPT_TEMP_TABLE_RECEIPT_TABLE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTableListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptTableItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = "ReceiptTableItemPage", FeatureId = "ReceiptLineListPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ReceiptLineLineItemPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id/receiptline-child/:id" },
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_RECEIPT_TEMP_TABLE_RECEIPT_TABLE_PRINT_RECEIPT_COPY",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintReceiptCopy",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id/report/printreceiptcopy/"
                        }
                    }
                },
               new SysFeatureGroupControllerMappingView
               {
                    ControllerName = "ReceiptTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_RECEIPT_TEMP_TABLE_RECEIPT_TABLE_PRINT_RECEIPT_ORIGINAL",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                             FeatureId = "PrintReceiptOriginal",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id/relatedinfo/receipttable/:id/report/printreceiptoriginal/"
                        }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_RECEIPT_TEMP_TABLE_CANCEL_RECEIPT_TEMP",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CancelReceiptTempPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id/function/cancelreceipttemp/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_RECEIPT_TEMP_TABLE_POST_RECEIPT_TEMP",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PostReceiptTempPage", Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id/function/postreceipttemp/" }
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "ReceiptTempTable",
                    GroupId = "PROJECT_FINANCE_CREDIT_APP_TABLE_WITHDRAWAL_TABLE_WITHDRAWAL_WITHDRAWAL_TABLE_TERM_EXTENSION_RECEIPT_TEMP_TABLE_PRINT_RECEIPT_TEMP",
                    FeatureType = FeatureType.REPORT,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView
                        {
                            ParentFeatureId = null,
                            FeatureId = "PrintReceiptTempPage",
                            Path = "/:site/projectfinance/creditapptable/:id/relatedinfo/withdrawaltablewithdrawal/:id/relatedinfo/withdrawaltabletermextension/:id/relatedinfo/receipttemptable/:id/report/printreceipttemp/"
                        }
                    }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_PURCHASE_LINE_ROLL_BILL-CHILD_INVOICE_TABLE_TAX_INVOICE",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceTableListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = "TaxInvoiceTableItemPage", FeatureId = "TaxInvoiceLineListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "TaxInvoiceLineItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/taxinvoiceline-child/:id" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_PURCHASE_LINE_ROLL_BILL-CHILD_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_COPY",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceCopy", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoicecopy/" },
                    }
                },
                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_PURCHASE_LINE_ROLL_BILL-CHILD_INVOICE_TABLE_TAX_INVOICE_PRINT_TAX_INVOICE_ORIGINAL",
                    FeatureType = FeatureType.FUNCTION,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PrintTaxInvoiceOriginal", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/:id/relatedinfo/taxinvoice/:id/report/printtaxinvoiceoriginal/" },
                                            }
                },

                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_PURCHASE_LINE_ROLL_BILL-CHILD_INVOICE_TABLE_TAX_INVOICE_PAYMENT_HISTORY",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "PaymentHistoryListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/:id/relatedinfo/paymenthistory/" },
                    }
                },

                 new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_PURCHASE_LINE_ROLL_BILL-CHILD_INVOICE_TABLE_TAX_INVOICE_CUST_TRANSACTION",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/:id" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "CustomerTransactionListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/invoicetable/:id/relatedinfo/custtransaction/" },
                    }
                },
                new SysFeatureGroupControllerMappingView
                {
                    ControllerName = "CreditAppTable",
                    GroupId = "FACTORING_CREDIT_APP_TABLE_PURCHASE_TABLE_ROLL_BILL_PURCHASE_LINE_ROLL_BILL-CHILD_PRODUCT_SETTLEMENT",
                    FeatureType = FeatureType.RELATEDINFO,
                    SysFeatureTables = new List<SysFeatureTableView>()
                    {
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSettledTransListPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/productsettlement/" },
                        new SysFeatureTableView { ParentFeatureId = null, FeatureId = "ProductSettledTransItemPage", Path = "/:site/factoring/creditapptable/:id/relatedinfo/purchasetablerollbill/:id/purchaselinerollbill-child/:id/relatedinfo/productsettlement/:id" }
                    }
                },
            };
        }
    }
}