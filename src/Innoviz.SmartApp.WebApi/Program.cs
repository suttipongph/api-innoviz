﻿

using Innoviz.SmartApp.Core;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.MSSqlServer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;

namespace Innoviz.SmartApp.WebApi
{
    public class Program
    {
        public static int Main(string[] args) {

            var configuration = new ConfigurationBuilder()
                                    .SetBasePath(Directory.GetCurrentDirectory())
                                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                                    .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
                                    .AddEnvironmentVariables()
                                    .Build();
            SystemStaticData.SetConfiguration(configuration);

            string connectionString = configuration.GetConnectionString("SmartApp");
            var colOptions = new ColumnOptions();

            colOptions.Store.Remove(StandardColumn.Properties);
            colOptions.Store.Remove(StandardColumn.Level);
            colOptions.Store.Remove(StandardColumn.Exception);
            colOptions.Store.Remove(StandardColumn.LogEvent);
            colOptions.Store.Remove(StandardColumn.Message);
            colOptions.Store.Remove(StandardColumn.MessageTemplate);

            colOptions.AdditionalColumns = new Collection<SqlColumn> {
                new SqlColumn{ DataType = SqlDbType.UniqueIdentifier, ColumnName = "InstanceHistoryId" },
                new SqlColumn{ DataType = SqlDbType.Int, ColumnName = "ResultStatus" },
                new SqlColumn{ DataType = SqlDbType.NVarChar, ColumnName = "Message" },
                new SqlColumn{ DataType = SqlDbType.NVarChar, ColumnName = "ItemValues" },
                new SqlColumn{ DataType = SqlDbType.NVarChar, ColumnName = "StackTrace" },
                new SqlColumn{ DataType = SqlDbType.NVarChar, ColumnName = "Reference" },
            };
            string rootLogFolder = configuration.GetValue<string>("LogRootFolder");
            string logFilePath = "logs/apiLog.txt";
            string msgTemplate = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} ReqID:[{HttpRequestId}] [{Level:u3}] {Message:lj}{NewLine}{Exception}";
            
            if (!string.IsNullOrWhiteSpace(rootLogFolder)) {
                logFilePath = Path.Combine(rootLogFolder, logFilePath);
            }
            Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(configuration)
            .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
            .WriteTo.File(path: logFilePath, rollingInterval: RollingInterval.Day,
                          outputTemplate: msgTemplate)
            .WriteTo.Console(outputTemplate: msgTemplate)
            .Enrich.FromLogContext()
            .CreateLogger();

            try {
                Log.Information("Starting host");
                CreateHostBuilder(args).Build().Run();
                return 0;
            }
            catch (Exception ex) {
                Log.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseContentRoot(Directory.GetCurrentDirectory());
                    webBuilder.UseWebRoot(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot"));
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseSerilog();
                });
                //.UseKestrel()
                //.UseContentRoot(Directory.GetCurrentDirectory())
                //.UseWebRoot(Path.Combine(Directory.GetCurrentDirectory(),"wwwroot"))
                //.UseIISIntegration()
                //.UseStartup<Startup>()
                //.UseSerilog()
                //.Build();
    }
}
