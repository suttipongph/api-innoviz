﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Innoviz.SmartApp.WebApi.Migrations
{
    public partial class _1122021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Staging_ConsortiumLine",
                table: "Staging_ConsortiumLine");

            migrationBuilder.AlterColumn<Guid>(
                name: "ResponsibleByGUID",
                table: "CustomerTable",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Staging_ConsortiumLine",
                table: "Staging_ConsortiumLine",
                columns: new[] { "ConsortiumLineGUID", "Ordering", "CompanyGUID", "ConsortiumTableGUID" });

            migrationBuilder.CreateTable(
                name: "AgingReportSetup",
                columns: table => new
                {
                    AgingReportSetupGUID = table.Column<Guid>(nullable: false),
                    CompanyGUID = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: false),
                    Owner = table.Column<string>(nullable: true),
                    OwnerBusinessUnitGUID = table.Column<Guid>(nullable: true),
                    Description = table.Column<string>(maxLength: 100, nullable: true),
                    LineNum = table.Column<int>(nullable: false),
                    MaximumDays = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgingReportSetup", x => x.AgingReportSetupGUID);
                    table.ForeignKey(
                        name: "FK_AgingReportSetupCompany",
                        column: x => x.CompanyGUID,
                        principalTable: "Company",
                        principalColumn: "CompanyGUID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AgingReportSetupBusinessUnit",
                        column: x => x.OwnerBusinessUnitGUID,
                        principalTable: "BusinessUnit",
                        principalColumn: "BusinessUnitGUID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SysControllerEntityMapping",
                columns: table => new
                {
                    SysControllerEntityMappingGUID = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: false),
                    SysControllerTableGUID = table.Column<Guid>(nullable: false),
                    ModelName = table.Column<string>(nullable: true),
                    FeatureType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysControllerEntityMapping", x => x.SysControllerEntityMappingGUID);
                    table.ForeignKey(
                        name: "FK_SysControllerEntityMappingSysControllerTable",
                        column: x => x.SysControllerTableGUID,
                        principalTable: "SysControllerTable",
                        principalColumn: "SysControllerTableGUID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SysEntityRole",
                columns: table => new
                {
                    SysEntityRoleGUID = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: false),
                    RoleGUID = table.Column<Guid>(nullable: false),
                    ModelName = table.Column<string>(nullable: true),
                    AccessLevel = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysEntityRole", x => x.SysEntityRoleGUID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AgingReportSetup_CompanyGUID",
                table: "AgingReportSetup",
                column: "CompanyGUID");

            migrationBuilder.CreateIndex(
                name: "IX_AgingReportSetup_OwnerBusinessUnitGUID",
                table: "AgingReportSetup",
                column: "OwnerBusinessUnitGUID");

            migrationBuilder.CreateIndex(
                name: "IX_SysControllerEntityMapping_ModelName",
                table: "SysControllerEntityMapping",
                column: "ModelName");

            migrationBuilder.CreateIndex(
                name: "IX_SysControllerEntityMapping_SysControllerTableGUID",
                table: "SysControllerEntityMapping",
                column: "SysControllerTableGUID");

            migrationBuilder.CreateIndex(
                name: "IX_SysEntityRole_ModelName",
                table: "SysEntityRole",
                column: "ModelName");

            migrationBuilder.CreateIndex(
                name: "IX_SysEntityRole_RoleGUID",
                table: "SysEntityRole",
                column: "RoleGUID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AgingReportSetup");

            migrationBuilder.DropTable(
                name: "SysControllerEntityMapping");

            migrationBuilder.DropTable(
                name: "SysEntityRole");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Staging_ConsortiumLine",
                table: "Staging_ConsortiumLine");

            migrationBuilder.AlterColumn<Guid>(
                name: "ResponsibleByGUID",
                table: "CustomerTable",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Staging_ConsortiumLine",
                table: "Staging_ConsortiumLine",
                columns: new[] { "ConsortiumLineGUID", "Ordering", "CompanyGUID" });
        }
    }
}
