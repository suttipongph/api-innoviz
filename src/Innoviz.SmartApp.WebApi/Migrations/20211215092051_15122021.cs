﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Innoviz.SmartApp.WebApi.Migrations
{
    public partial class _15122021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Staging_InterestRealizedTrans",
                table: "Staging_InterestRealizedTrans");

            migrationBuilder.AlterColumn<string>(
                name: "DocumentId",
                table: "Staging_InterestRealizedTrans",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20);

            migrationBuilder.AddColumn<int>(
                name: "MaxAccruedIntDay",
                table: "CompanyParameter",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NormalAccruedIntDay",
                table: "CompanyParameter",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Staging_InterestRealizedTrans",
                table: "Staging_InterestRealizedTrans",
                columns: new[] { "InterestRealizedTransGUID", "RefGUID", "LineNum", "CompanyGUID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Staging_InterestRealizedTrans",
                table: "Staging_InterestRealizedTrans");

            migrationBuilder.DropColumn(
                name: "MaxAccruedIntDay",
                table: "CompanyParameter");

            migrationBuilder.DropColumn(
                name: "NormalAccruedIntDay",
                table: "CompanyParameter");

            migrationBuilder.AlterColumn<string>(
                name: "DocumentId",
                table: "Staging_InterestRealizedTrans",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Staging_InterestRealizedTrans",
                table: "Staging_InterestRealizedTrans",
                columns: new[] { "InterestRealizedTransGUID", "DocumentId", "CompanyGUID" });
        }
    }
}
