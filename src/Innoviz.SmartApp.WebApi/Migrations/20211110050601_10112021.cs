﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Innoviz.SmartApp.WebApi.Migrations
{
    public partial class _10112021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LastReceivedDate",
                table: "ProductSettledTrans",
                type: "date",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "PostedInterestAmount",
                table: "ProductSettledTrans",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "Cancel",
                table: "PaymentHistory",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReceivedDate",
                table: "PaymentHistory",
                type: "date",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "ShowDocConVerifyType",
                table: "JobType",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "GuaranteeLand",
                table: "GuarantorAgreementLine",
                maxLength: 250,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastReceivedDate",
                table: "ProductSettledTrans");

            migrationBuilder.DropColumn(
                name: "PostedInterestAmount",
                table: "ProductSettledTrans");

            migrationBuilder.DropColumn(
                name: "Cancel",
                table: "PaymentHistory");

            migrationBuilder.DropColumn(
                name: "ReceivedDate",
                table: "PaymentHistory");

            migrationBuilder.DropColumn(
                name: "ShowDocConVerifyType",
                table: "JobType");

            migrationBuilder.DropColumn(
                name: "GuaranteeLand",
                table: "GuarantorAgreementLine");
        }
    }
}
