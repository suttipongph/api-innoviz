﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Innoviz.SmartApp.WebApi.Migrations
{
    public partial class _8122021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Remark",
                table: "VerificationLine",
                maxLength: 1000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(250)",
                oldMaxLength: 250,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MigrateSource",
                table: "Staging_WithdrawalLine",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MigrateStatus",
                table: "Staging_WithdrawalLine",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "OperatedBy",
                table: "RelatedPersonTable",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OperatedBy",
                table: "GuarantorAgreementLine",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "AssignmentMethodGUID",
                table: "CreditAppTable",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PurchaseWithdrawalCondition",
                table: "CreditAppTable",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "AssignmentMethodGUID",
                table: "CreditAppRequestTable",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PurchaseWithdrawalCondition",
                table: "CreditAppRequestTable",
                maxLength: 100,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_IntercompanyInvoiceSettlement_CNReasonGUID",
                table: "IntercompanyInvoiceSettlement",
                column: "CNReasonGUID");

            migrationBuilder.CreateIndex(
                name: "IX_CreditAppTable_AssignmentMethodGUID",
                table: "CreditAppTable",
                column: "AssignmentMethodGUID");

            migrationBuilder.CreateIndex(
                name: "IX_CreditAppRequestTable_AssignmentMethodGUID",
                table: "CreditAppRequestTable",
                column: "AssignmentMethodGUID");

            migrationBuilder.AddForeignKey(
                name: "FK_CreditAppRequestTableAssignmentMethod",
                table: "CreditAppRequestTable",
                column: "AssignmentMethodGUID",
                principalTable: "AssignmentMethod",
                principalColumn: "AssignmentMethodGUID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CreditAppTableAssignmentMethod",
                table: "CreditAppTable",
                column: "AssignmentMethodGUID",
                principalTable: "AssignmentMethod",
                principalColumn: "AssignmentMethodGUID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_IntercompanyInvoiceSettlementCNReason",
                table: "IntercompanyInvoiceSettlement",
                column: "CNReasonGUID",
                principalTable: "DocumentReason",
                principalColumn: "DocumentReasonGUID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CreditAppRequestTableAssignmentMethod",
                table: "CreditAppRequestTable");

            migrationBuilder.DropForeignKey(
                name: "FK_CreditAppTableAssignmentMethod",
                table: "CreditAppTable");

            migrationBuilder.DropForeignKey(
                name: "FK_IntercompanyInvoiceSettlementCNReason",
                table: "IntercompanyInvoiceSettlement");

            migrationBuilder.DropIndex(
                name: "IX_IntercompanyInvoiceSettlement_CNReasonGUID",
                table: "IntercompanyInvoiceSettlement");

            migrationBuilder.DropIndex(
                name: "IX_CreditAppTable_AssignmentMethodGUID",
                table: "CreditAppTable");

            migrationBuilder.DropIndex(
                name: "IX_CreditAppRequestTable_AssignmentMethodGUID",
                table: "CreditAppRequestTable");

            migrationBuilder.DropColumn(
                name: "MigrateSource",
                table: "Staging_WithdrawalLine");

            migrationBuilder.DropColumn(
                name: "MigrateStatus",
                table: "Staging_WithdrawalLine");

            migrationBuilder.DropColumn(
                name: "OperatedBy",
                table: "RelatedPersonTable");

            migrationBuilder.DropColumn(
                name: "OperatedBy",
                table: "GuarantorAgreementLine");

            migrationBuilder.DropColumn(
                name: "AssignmentMethodGUID",
                table: "CreditAppTable");

            migrationBuilder.DropColumn(
                name: "PurchaseWithdrawalCondition",
                table: "CreditAppTable");

            migrationBuilder.DropColumn(
                name: "AssignmentMethodGUID",
                table: "CreditAppRequestTable");

            migrationBuilder.DropColumn(
                name: "PurchaseWithdrawalCondition",
                table: "CreditAppRequestTable");

            migrationBuilder.AlterColumn<string>(
                name: "Remark",
                table: "VerificationLine",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 1000,
                oldNullable: true);
        }
    }
}
