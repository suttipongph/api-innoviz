﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Innoviz.SmartApp.WebApi.Migrations
{
    public partial class _811201 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProcessTransCustomerTable",
                table: "ProcessTrans");

            migrationBuilder.DropColumn(
                name: "GroupName",
                table: "SysFeatureGroup");

            migrationBuilder.AlterColumn<string>(
                name: "GuarantorAgreementTableGUID",
                table: "Staging_GuarantorAgreementTable",
                maxLength: 50,
                nullable: false,
                defaultValueSql: "convert(nvarchar(50),newid())",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.AddColumn<Guid>(
                name: "CNReasonGUID",
                table: "IntercompanyInvoiceSettlement",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "OrigInvoiceAmount",
                table: "IntercompanyInvoiceSettlement",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "OrigInvoiceId",
                table: "IntercompanyInvoiceSettlement",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "OrigTaxInvoiceAmount",
                table: "IntercompanyInvoiceSettlement",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "OrigTaxInvoiceId",
                table: "IntercompanyInvoiceSettlement",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "RefIntercompanyInvoiceSettlementGUID",
                table: "IntercompanyInvoiceSettlement",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AttachmentRemark",
                table: "CreditAppReqBusinessCollateral",
                maxLength: 1000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(250)",
                oldMaxLength: 250,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Staging_ConsortiumTrans",
                columns: table => new
                {
                    ConsortiumTransGUID = table.Column<string>(nullable: false, defaultValueSql: "convert(nvarchar(50),newid())"),
                    Address = table.Column<string>(maxLength: 500, nullable: true),
                    AuthorizedPersonTypeGUID = table.Column<string>(maxLength: 50, nullable: true),
                    CustomerName = table.Column<string>(maxLength: 100, nullable: false),
                    OperatedBy = table.Column<string>(maxLength: 100, nullable: true),
                    Ordering = table.Column<int>(nullable: false),
                    RefGUID = table.Column<string>(maxLength: 50, nullable: false),
                    RefType = table.Column<int>(nullable: false),
                    Remark = table.Column<string>(maxLength: 250, nullable: true),
                    ConsortiumLineGUID = table.Column<string>(maxLength: 50, nullable: true),
                    MigrateStatus = table.Column<string>(nullable: true),
                    MigrateSource = table.Column<string>(nullable: true),
                    CompanyGUID = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDateTime = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDateTime = table.Column<string>(nullable: true),
                    Owner = table.Column<string>(nullable: true),
                    OwnerBusinessUnitGUID = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Staging_ConsortiumTrans", x => x.ConsortiumTransGUID);
                });

            migrationBuilder.CreateTable(
                name: "Staging_DocumentConditionTrans",
                columns: table => new
                {
                    DocumentConditionTransGUID = table.Column<string>(maxLength: 50, nullable: false, defaultValueSql: "convert(nvarchar(50),newid())"),
                    DocConVerifyType = table.Column<int>(nullable: false),
                    DocumentTypeGUID = table.Column<string>(maxLength: 50, nullable: false),
                    Inactive = table.Column<bool>(nullable: false),
                    Mandatory = table.Column<bool>(nullable: false),
                    RefDocumentConditionTransGUID = table.Column<string>(maxLength: 50, nullable: true),
                    RefGUID = table.Column<string>(maxLength: 50, nullable: false),
                    RefType = table.Column<int>(nullable: false),
                    MigrateStatus = table.Column<string>(nullable: true),
                    MigrateSource = table.Column<string>(nullable: true),
                    CompanyGUID = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDateTime = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDateTime = table.Column<string>(nullable: true),
                    Owner = table.Column<string>(nullable: true),
                    OwnerBusinessUnitGUID = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Staging_DocumentConditionTrans", x => x.DocumentConditionTransGUID);
                });

            migrationBuilder.CreateTable(
                name: "Staging_GuarantorTrans",
                columns: table => new
                {
                    GuarantorTransGUID = table.Column<string>(maxLength: 50, nullable: false, defaultValueSql: "convert(nvarchar(50),newid())"),
                    Affiliate = table.Column<bool>(nullable: false),
                    GuarantorTypeGUID = table.Column<string>(maxLength: 50, nullable: false),
                    InActive = table.Column<bool>(nullable: false),
                    Ordering = table.Column<int>(nullable: false),
                    RefGuarantorTransGUID = table.Column<string>(maxLength: 50, nullable: true),
                    RefGUID = table.Column<string>(maxLength: 50, nullable: false),
                    RefType = table.Column<int>(nullable: false),
                    RelatedPersonTableGUID = table.Column<string>(maxLength: 50, nullable: false),
                    MaximumGuaranteeAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MigrateStatus = table.Column<string>(nullable: true),
                    MigrateSource = table.Column<string>(nullable: true),
                    CompanyGUID = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDateTime = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDateTime = table.Column<string>(nullable: true),
                    Owner = table.Column<string>(nullable: true),
                    OwnerBusinessUnitGUID = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Staging_GuarantorTrans", x => x.GuarantorTransGUID);
                });

            migrationBuilder.CreateTable(
                name: "Staging_InterestRealizedTrans",
                columns: table => new
                {
                    InterestRealizedTransGUID = table.Column<string>(maxLength: 50, nullable: false),
                    DocumentId = table.Column<string>(maxLength: 20, nullable: false),
                    CompanyGUID = table.Column<string>(maxLength: 50, nullable: false),
                    AccInterestAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    AccountingDate = table.Column<string>(maxLength: 50, nullable: false),
                    Accrued = table.Column<bool>(nullable: false),
                    EndDate = table.Column<string>(maxLength: 50, nullable: false),
                    InterestDay = table.Column<int>(nullable: false),
                    InterestPerDay = table.Column<string>(maxLength: 50, nullable: false),
                    LineNum = table.Column<int>(nullable: false),
                    ProductType = table.Column<int>(nullable: false),
                    RefGUID = table.Column<string>(maxLength: 50, nullable: false),
                    RefType = table.Column<int>(nullable: false),
                    StartDate = table.Column<string>(maxLength: 50, nullable: false),
                    Cancelled = table.Column<bool>(nullable: false),
                    RefProcessTransGUID = table.Column<string>(maxLength: 50, nullable: true),
                    Dimension1GUID = table.Column<string>(maxLength: 50, nullable: true),
                    Dimension2GUID = table.Column<string>(maxLength: 50, nullable: true),
                    Dimension3GUID = table.Column<string>(maxLength: 50, nullable: true),
                    Dimension4GUID = table.Column<string>(maxLength: 50, nullable: true),
                    Dimension5GUID = table.Column<string>(maxLength: 50, nullable: true),
                    MigrateStatus = table.Column<string>(nullable: true),
                    MigrateSource = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDateTime = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDateTime = table.Column<string>(nullable: true),
                    Owner = table.Column<string>(nullable: true),
                    OwnerBusinessUnitGUID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Staging_InterestRealizedTrans", x => new { x.InterestRealizedTransGUID, x.DocumentId, x.CompanyGUID });
                });

            migrationBuilder.CreateTable(
                name: "SysFeatureGroupStructure",
                columns: table => new
                {
                    NodeId = table.Column<int>(nullable: false),
                    SiteLoginType = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDateTime = table.Column<DateTime>(nullable: false),
                    NodeParentId = table.Column<int>(nullable: false),
                    SysFeatureGroupGUID = table.Column<Guid>(nullable: true),
                    NodeLabel = table.Column<string>(nullable: true),
                    FeatureType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysFeatureGroupStructure", x => new { x.NodeId, x.SiteLoginType });
                    table.ForeignKey(
                        name: "FK_SysFeatureGroupStructure_SysFeatureGroup_SysFeatureGroupGUID",
                        column: x => x.SysFeatureGroupGUID,
                        principalTable: "SysFeatureGroup",
                        principalColumn: "SysFeatureGroupGUID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SysFeatureGroupStructure_SysFeatureGroupStructure_NodeParentId_SiteLoginType",
                        columns: x => new { x.NodeParentId, x.SiteLoginType },
                        principalTable: "SysFeatureGroupStructure",
                        principalColumns: new[] { "NodeId", "SiteLoginType" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCollateralAgmLine_BusinessCollateralStatusGUID",
                table: "BusinessCollateralAgmLine",
                column: "BusinessCollateralStatusGUID");

            migrationBuilder.CreateIndex(
                name: "IX_SysFeatureGroupStructure_SysFeatureGroupGUID",
                table: "SysFeatureGroupStructure",
                column: "SysFeatureGroupGUID");

            migrationBuilder.CreateIndex(
                name: "IX_SysFeatureGroupStructure_NodeParentId_SiteLoginType",
                table: "SysFeatureGroupStructure",
                columns: new[] { "NodeParentId", "SiteLoginType" });

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessCollateralAgmLineBusinessCollateralStatus",
                table: "BusinessCollateralAgmLine",
                column: "BusinessCollateralStatusGUID",
                principalTable: "BusinessCollateralStatus",
                principalColumn: "BusinessCollateralStatusGUID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BusinessCollateralAgmLineBusinessCollateralStatus",
                table: "BusinessCollateralAgmLine");

            migrationBuilder.DropTable(
                name: "Staging_ConsortiumTrans");

            migrationBuilder.DropTable(
                name: "Staging_DocumentConditionTrans");

            migrationBuilder.DropTable(
                name: "Staging_GuarantorTrans");

            migrationBuilder.DropTable(
                name: "Staging_InterestRealizedTrans");

            migrationBuilder.DropTable(
                name: "SysFeatureGroupStructure");

            migrationBuilder.DropIndex(
                name: "IX_BusinessCollateralAgmLine_BusinessCollateralStatusGUID",
                table: "BusinessCollateralAgmLine");

            migrationBuilder.DropColumn(
                name: "CNReasonGUID",
                table: "IntercompanyInvoiceSettlement");

            migrationBuilder.DropColumn(
                name: "OrigInvoiceAmount",
                table: "IntercompanyInvoiceSettlement");

            migrationBuilder.DropColumn(
                name: "OrigInvoiceId",
                table: "IntercompanyInvoiceSettlement");

            migrationBuilder.DropColumn(
                name: "OrigTaxInvoiceAmount",
                table: "IntercompanyInvoiceSettlement");

            migrationBuilder.DropColumn(
                name: "OrigTaxInvoiceId",
                table: "IntercompanyInvoiceSettlement");

            migrationBuilder.DropColumn(
                name: "RefIntercompanyInvoiceSettlementGUID",
                table: "IntercompanyInvoiceSettlement");

            migrationBuilder.AddColumn<string>(
                name: "GroupName",
                table: "SysFeatureGroup",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "GuarantorAgreementTableGUID",
                table: "Staging_GuarantorAgreementTable",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldDefaultValueSql: "convert(nvarchar(50),newid())");

            migrationBuilder.AlterColumn<string>(
                name: "AttachmentRemark",
                table: "CreditAppReqBusinessCollateral",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 1000,
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ProcessTransCustomerTable",
                table: "ProcessTrans",
                column: "CustomerTableGUID",
                principalTable: "CustomerTable",
                principalColumn: "CustomerTableGUID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
