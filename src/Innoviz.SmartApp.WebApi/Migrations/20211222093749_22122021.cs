﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Innoviz.SmartApp.WebApi.Migrations
{
    public partial class _22122021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DocumentId",
                table: "AssignmentAgreementSettle",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "InvoiceTableGUID",
                table: "AssignmentAgreementSettle",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SysDataInitializationHistory",
                columns: table => new
                {
                    DataInitializationType = table.Column<int>(nullable: false),
                    Version = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysDataInitializationHistory", x => new { x.DataInitializationType, x.Version });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SysDataInitializationHistory");

            migrationBuilder.DropColumn(
                name: "DocumentId",
                table: "AssignmentAgreementSettle");

            migrationBuilder.DropColumn(
                name: "InvoiceTableGUID",
                table: "AssignmentAgreementSettle");
        }
    }
}
