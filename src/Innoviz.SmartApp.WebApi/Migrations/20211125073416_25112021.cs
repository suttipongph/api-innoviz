﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Innoviz.SmartApp.WebApi.Migrations
{
    public partial class _25112021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StagingBatchStatus",
                table: "IntercompanyInvoiceSettlement");

            migrationBuilder.AlterColumn<decimal>(
                name: "PurchasePct",
                table: "PurchaseLine",
                type: "decimal(10,7)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "LinePurchasePct",
                table: "PurchaseLine",
                type: "decimal(10,7)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)");

            migrationBuilder.AlterColumn<string>(
                name: "FeeLedgerAccount",
                table: "InvoiceRevenueType",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

     
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
          

            migrationBuilder.AlterColumn<decimal>(
                name: "PurchasePct",
                table: "PurchaseLine",
                type: "decimal(5,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(10,7)");

            migrationBuilder.AlterColumn<decimal>(
                name: "LinePurchasePct",
                table: "PurchaseLine",
                type: "decimal(5,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(10,7)");

            migrationBuilder.AlterColumn<string>(
                name: "FeeLedgerAccount",
                table: "InvoiceRevenueType",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StagingBatchStatus",
                table: "IntercompanyInvoiceSettlement",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
