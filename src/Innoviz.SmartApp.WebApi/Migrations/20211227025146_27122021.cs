﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Innoviz.SmartApp.WebApi.Migrations
{
    public partial class _27122021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "WithholdingTaxValue",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "WithholdingTaxTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "WithholdingTaxGroup",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MarketingComment",
                table: "WithdrawalTable",
                maxLength: 250,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "MarketingMarkComment",
                table: "WithdrawalTable",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "WithdrawalTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "WithdrawalLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "VerificationType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "VerificationTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "VerificationTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "VerificationLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "VendorTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "VendorPaymentTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "VendGroup",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "VendBank",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Territory",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "TaxValue",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "TaxTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "TaxReportTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "TaxInvoiceTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "TaxInvoiceLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysUserTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysUserCompanyMapping",
                rowVersion: true,
                nullable: true);

            //migrationBuilder.AddColumn<byte[]>(
            //    name: "RowVersion",
            //    table: "sysssislog",
            //    rowVersion: true,
            //    nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysSettingTimeFormat",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysSettingNumberFormat",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysSettingDateFormat",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysScopeRole",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysRoleTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysNavigation",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysFeatureTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysFeatureGroupStructure",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysFeatureGroupRole",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysFeatureGroupMapping",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysFeatureGroupController",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysFeatureGroup",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysEnumTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysEntityRole",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysDuplicateDetection",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysControllerTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysControllerRole",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "SysControllerEntityMapping",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "StagingTransText",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "StagingTableVendorInfo",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "StagingTableIntercoInvSettle",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "StagingTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ServiceFeeTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ServiceFeeCondTemplateTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ServiceFeeCondTemplateLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ServiceFeeConditionTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "RetentionTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "RetentionConditionTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "RetentionConditionSetup",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "RelatedPersonTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "RegistrationType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ReceiptTempTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ReceiptTempPaymDetail",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ReceiptTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ReceiptLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Race",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "PurchaseLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "PropertyType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ProjectReferenceTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ProjectProgressTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ProdUnit",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ProductSubType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ProductSettledTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ProcessTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "PaymentHistory",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "PaymentFrequency",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "PaymentDetail",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ParentCompany",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "OwnerTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Ownership",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Occupation",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "NumberSeqSetupByProductType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "NumberSeqSegment",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "NumberSeqParameter",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "NCBTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "NCBAccountStatus",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Nationality",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "MigrationTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "MethodOfPayment",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "MessengerTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "MessengerJobTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "MemoTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "MaritalStatus",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "MainAgreementTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "LineOfBusiness",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "LedgerFiscalYear",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "LedgerFiscalPeriod",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "LedgerDimension",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "LeaseType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Language",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "KYCSetup",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "JointVentureTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "JobType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "JobCheque",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "InvoiceType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "InvoiceTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "InvoiceSettlementDetail",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "InvoiceRevenueType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "InvoiceNumberSeqSetup",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "InvoiceLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "IntroducedBy",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "InterestTypeValue",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "InterestType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "InterestRealizedTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "IntercompanyInvoiceTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "IntercompanyInvoiceSettlement",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "IntercompanyInvoiceAdjustment",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Intercompany",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "GuarantorType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "GuarantorTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "GuarantorAgreementTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "GuarantorAgreementLineAffiliate",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "CustomerTableGUID",
                table: "GuarantorAgreementLine",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AlterColumn<Guid>(
                name: "CreditAppTableGUID",
                table: "GuarantorAgreementLine",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "GuarantorAgreementLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "GradeClassification",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Gender",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "FreeTextInvoiceTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "FinancialStatementTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "FinancialCreditTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ExposureGroupByProduct",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ExposureGroup",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ExchangeRate",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "EmplTeam",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "EmployeeTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "DocumentType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "DocumentTemplateTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "DocumentStatus",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "DocumentReturnTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "DocumentReturnMethods",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "DocumentReturnLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "DocumentReason",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "DocumentProcess",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "DocumentConditionTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "DocumentConditionTemplateTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "DocumentConditionTemplateLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Department",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CustVisitingTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CustTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CustomerTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CustomerRefundTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CustomerCreditLimitByProduct",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CustGroup",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CustBusinessCollateral",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CustBank",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Currency",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CreditType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CreditTerm",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CreditScoring",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CreditLimitType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CreditAppTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CreditAppTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CreditAppRequestTableAmend",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CreditAppRequestLineAmend",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CreditAppRequestLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CreditAppReqBusinessCollateral",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CreditAppReqAssignment",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CreditAppLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ContactTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ContactPersonTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ConsortiumTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ConsortiumTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ConsortiumLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CompanySignature",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "FTRollbillInterestRefundInvTypeGUID",
                table: "CompanyParameter",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CompanyParameter",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CompanyBank",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Company",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CollectionGroup",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CollectionFollowUp",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ChequeTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CAReqRetentionOutstanding",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CAReqCreditOutStanding",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CAReqBuyerCreditOutstanding",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CAReqAssignmentOutstanding",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CalendarNonWorkingDate",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "CalendarGroup",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BuyerTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BuyerReceiptTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BuyerInvoiceTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BuyerCreditLimitByProduct",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BuyerAgreementTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BuyerAgreementTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BuyerAgreementLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BusinessUnit",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BusinessType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BusinessSize",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BusinessSegment",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BusinessCollateralType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BusinessCollateralSubType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BusinessCollateralStatus",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BusinessCollateralAgmTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BusinessCollateralAgmLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Branch",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BookmarkDocumentTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BookmarkDocumentTemplateTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BookmarkDocumentTemplateLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BookmarkDocument",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BlacklistStatus",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BillingResponsibleBy",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BankType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "BankGroup",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AuthorizedPersonType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AuthorizedPersonTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Attachment",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AssignmentMethod",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ValidateAssignmentBalance",
                table: "AssignmentMethod",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AssignmentAgreementTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AssignmentAgreementSettle",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AssignmentAgreementLine",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ApplicationTable",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AgreementType",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AgreementTableInfoText",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AgreementTableInfo",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AgingReportSetup",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AddressTrans",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AddressSubDistrict",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AddressProvince",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AddressPostalCode",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AddressDistrict",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "AddressCountry",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "ActionHistory",
                rowVersion: true,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentAgreementSettle_InvoiceTableGUID",
                table: "AssignmentAgreementSettle",
                column: "InvoiceTableGUID");

            migrationBuilder.AddForeignKey(
                name: "FK_AssignmentAgreementSettleInvoiceTable",
                table: "AssignmentAgreementSettle",
                column: "InvoiceTableGUID",
                principalTable: "InvoiceTable",
                principalColumn: "InvoiceTableGUID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssignmentAgreementSettleInvoiceTable",
                table: "AssignmentAgreementSettle");

            migrationBuilder.DropIndex(
                name: "IX_AssignmentAgreementSettle_InvoiceTableGUID",
                table: "AssignmentAgreementSettle");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "WithholdingTaxValue");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "WithholdingTaxTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "WithholdingTaxGroup");

            migrationBuilder.DropColumn(
                name: "MarketingComment",
                table: "WithdrawalTable");

            migrationBuilder.DropColumn(
                name: "MarketingMarkComment",
                table: "WithdrawalTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "WithdrawalTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "WithdrawalLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "VerificationType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "VerificationTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "VerificationTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "VerificationLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "VendorTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "VendorPaymentTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "VendGroup");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "VendBank");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Territory");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "TaxValue");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "TaxTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "TaxReportTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "TaxInvoiceTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "TaxInvoiceLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysUserTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysUserCompanyMapping");

            //migrationBuilder.DropColumn(
            //    name: "RowVersion",
            //    table: "sysssislog");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysSettingTimeFormat");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysSettingNumberFormat");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysSettingDateFormat");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysScopeRole");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysRoleTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysNavigation");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysFeatureTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysFeatureGroupStructure");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysFeatureGroupRole");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysFeatureGroupMapping");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysFeatureGroupController");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysFeatureGroup");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysEnumTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysEntityRole");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysDuplicateDetection");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysControllerTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysControllerRole");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "SysControllerEntityMapping");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "StagingTransText");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "StagingTableVendorInfo");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "StagingTableIntercoInvSettle");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "StagingTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ServiceFeeTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ServiceFeeCondTemplateTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ServiceFeeCondTemplateLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ServiceFeeConditionTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "RetentionTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "RetentionConditionTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "RetentionConditionSetup");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "RelatedPersonTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "RegistrationType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ReceiptTempTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ReceiptTempPaymDetail");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ReceiptTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ReceiptLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Race");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "PurchaseLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "PropertyType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ProjectReferenceTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ProjectProgressTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ProdUnit");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ProductSubType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ProductSettledTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ProcessTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "PaymentHistory");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "PaymentFrequency");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "PaymentDetail");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ParentCompany");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "OwnerTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Ownership");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Occupation");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "NumberSeqSetupByProductType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "NumberSeqSegment");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "NumberSeqParameter");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "NCBTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "NCBAccountStatus");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Nationality");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "MigrationTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "MethodOfPayment");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "MessengerTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "MessengerJobTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "MemoTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "MaritalStatus");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "MainAgreementTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "LineOfBusiness");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "LedgerFiscalYear");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "LedgerFiscalPeriod");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "LedgerDimension");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "LeaseType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Language");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "KYCSetup");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "JointVentureTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "JobType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "JobCheque");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "InvoiceType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "InvoiceTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "InvoiceSettlementDetail");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "InvoiceRevenueType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "InvoiceNumberSeqSetup");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "InvoiceLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "IntroducedBy");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "InterestTypeValue");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "InterestType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "InterestRealizedTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "IntercompanyInvoiceTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "IntercompanyInvoiceSettlement");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "IntercompanyInvoiceAdjustment");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Intercompany");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "GuarantorType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "GuarantorTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "GuarantorAgreementTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "GuarantorAgreementLineAffiliate");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "GuarantorAgreementLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "GradeClassification");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Gender");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "FreeTextInvoiceTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "FinancialStatementTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "FinancialCreditTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ExposureGroupByProduct");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ExposureGroup");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ExchangeRate");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "EmplTeam");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "EmployeeTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "DocumentType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "DocumentTemplateTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "DocumentStatus");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "DocumentReturnTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "DocumentReturnMethods");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "DocumentReturnLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "DocumentReason");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "DocumentProcess");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "DocumentConditionTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "DocumentConditionTemplateTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "DocumentConditionTemplateLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CustVisitingTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CustTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CustomerTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CustomerRefundTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CustomerCreditLimitByProduct");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CustGroup");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CustBusinessCollateral");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CustBank");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CreditType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CreditTerm");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CreditScoring");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CreditLimitType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CreditAppTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CreditAppTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CreditAppRequestTableAmend");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CreditAppRequestLineAmend");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CreditAppRequestLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CreditAppReqBusinessCollateral");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CreditAppReqAssignment");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CreditAppLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ContactTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ContactPersonTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ConsortiumTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ConsortiumTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ConsortiumLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CompanySignature");

            migrationBuilder.DropColumn(
                name: "FTRollbillInterestRefundInvTypeGUID",
                table: "CompanyParameter");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CompanyParameter");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CompanyBank");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CollectionGroup");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CollectionFollowUp");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ChequeTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CAReqRetentionOutstanding");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CAReqCreditOutStanding");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CAReqBuyerCreditOutstanding");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CAReqAssignmentOutstanding");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CalendarNonWorkingDate");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "CalendarGroup");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BuyerTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BuyerReceiptTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BuyerInvoiceTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BuyerCreditLimitByProduct");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BuyerAgreementTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BuyerAgreementTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BuyerAgreementLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BusinessUnit");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BusinessType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BusinessSize");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BusinessSegment");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BusinessCollateralType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BusinessCollateralSubType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BusinessCollateralStatus");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BusinessCollateralAgmTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BusinessCollateralAgmLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BookmarkDocumentTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BookmarkDocumentTemplateTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BookmarkDocumentTemplateLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BookmarkDocument");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BlacklistStatus");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BillingResponsibleBy");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BankType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "BankGroup");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AuthorizedPersonType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AuthorizedPersonTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Attachment");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AssignmentMethod");

            migrationBuilder.DropColumn(
                name: "ValidateAssignmentBalance",
                table: "AssignmentMethod");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AssignmentAgreementTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AssignmentAgreementSettle");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AssignmentAgreementLine");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ApplicationTable");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AgreementType");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AgreementTableInfoText");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AgreementTableInfo");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AgingReportSetup");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AddressTrans");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AddressSubDistrict");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AddressProvince");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AddressPostalCode");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AddressDistrict");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "AddressCountry");

            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "ActionHistory");

            migrationBuilder.AlterColumn<Guid>(
                name: "CustomerTableGUID",
                table: "GuarantorAgreementLine",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "CreditAppTableGUID",
                table: "GuarantorAgreementLine",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);
        }
    }
}
