﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Innoviz.SmartApp.WebApi.Migrations
{
    public partial class _1122021V2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Assignment",
                table: "JobType",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Assignment",
                table: "JobType");
        }
    }
}
