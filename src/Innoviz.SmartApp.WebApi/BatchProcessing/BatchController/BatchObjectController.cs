﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.BatchProcessing;
using Innoviz.SmartApp.WebApi.BatchProcessing.Service;
using Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.BatchProcessing.BatchController
{

    public class BatchObjectController : BaseControllerV2 {
        private string batchUrl;
        private string batchScheduleJobUrl;
        private string batchUnscheduleJobUrl;
        private string batchPauseJobUrl;
        private string batchResumeJobUrl;
        private string batchRetryJobUrl;
        private string batchCancelInstanceUrl;
        private string batchCleanUpHistoryUrl;
        private string batchDeleteBatchJobUrl;
        private string authUrl;
        private string webApiClient;
        private string webApiClientSecret;
        private string batchApiScope;

        private readonly IBatchService batchService;
        private IBatchLogService logger;

        public BatchObjectController(SmartAppContext context, ISysTransactionLogService transactionLogService, IConfiguration config, IBatchLogService logger)
            : base(context, transactionLogService) {
            batchUrl = config["BatchApi:BaseUrl"];
            batchScheduleJobUrl = batchUrl + "Quartz/scheduleJob";
            batchUnscheduleJobUrl = batchUrl + "Quartz/unscheduleJob";
            batchPauseJobUrl = batchUrl + "Quartz/pauseJob";
            batchResumeJobUrl = batchUrl + "Quartz/resumeJob";
            batchRetryJobUrl = batchUrl + "Quartz/retryJob";
            batchCancelInstanceUrl = batchUrl + "Quartz/cancelInstance";
            batchCleanUpHistoryUrl = batchUrl + "Quartz/cleanUpHistory";
            batchDeleteBatchJobUrl = batchUrl + "Quartz/deleteBatchJob";

            authUrl = config["JWT:Authority"];
            webApiClient = config["BusinessApi:Client"];
            webApiClientSecret = config["BusinessApi:ClientSecret"];
            batchApiScope = config["BatchApi:Resource"];

            batchService = new BatchService(context);
            this.logger = logger;
        }
        
        [HttpPost]
        [Route("GetBatchView/List")]
        public async Task<ActionResult> GetBatchViewList([FromBody]SearchParameter search) {
            try {
                var result = await GetQuartzBatchViewList(search);
                return Ok(result);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        } 
        
        [HttpGet]
        [Route("GetBatchView/{id}")]
        public async Task<ActionResult> GetBatchViewByJobId(string id) {
            try {
                BatchView item = await GetQuartzBatchViewByJobId(id);
                return Ok(batchService.GetBatchViewByJobId(item));
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        [HttpPost]
        [Route("GetBatchHistory/List/{jobId}")]
        public async Task<ActionResult> GetBatchHistoryByJobId([FromBody]SearchParameter search)
        {
            try {
                var result = await GetQuartzBatchHistoryViewByJobId(search);
                return Ok(result);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetBatchHistory/{id}")]
        public async Task<ActionResult> GetBatchInstanceHistoryById(string id) {
            try {
                BatchHistoryView item = await GetQuartzBatchHistoryViewById(id);
                return Ok(item);
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        [HttpPost]
        [Route("GetBatchLog/List/{instanceId}")]
        public async Task<ActionResult> GetBatchInstanceLogByInstanceHistoryId([FromBody]SearchParameter search)
        {
            try {
                var result = await GetQuartzBatchInstanceLogViewByInstanceId(search);
                return Ok(result);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetBatchLog/{id}")]
        public async Task<ActionResult> GetBatchInstanceLogById(int id) {
            try {
                BatchInstanceLogView item = await GetQuartzBatchInstanceLogById(id);
                return Ok(item);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        //[HttpPost]
        //[Route("Create")]
        //[Authorize(Policy = "roles_required")]
        //public async Task<ActionResult> CreateJobAndTrigger([FromBody] BatchObjectView model) {
            
        //    try {
        //        await CreateScheduledJob(model);
        //        return Ok(model);
        //    }
        //    catch (Exception e) {

        //        throw SmartAppUtil.AddStackTrace(e);
        //    }
        //}
        
        [HttpPost]
        [Route("Function/CancelBatchJob")]
        public async Task<ActionResult> EndJob([FromBody]BatchView model) {
            try {
                await UnscheduleJob(model);
                return Ok(model);
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Function/HoldBatchJob")]
        public async Task<ActionResult> HoldJob([FromBody]BatchView model) {
            try
            {    
                await PauseJob(model);
                return Ok(model);
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Function/ReleaseBatchJob")]
        public async Task<ActionResult> ReleaseJob([FromBody]BatchView model) {
            try
            {
                await ResumeJob(model);
                return Ok(model);
                
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        [HttpPost]
        [Route("ValidateRetryInstanceFunction")]
        public async Task<ActionResult> ValidateRetryInstanceFunction([FromBody]BatchHistoryView model)
        {
            try
            {
                bool result = await ValidateRetryInstanceHistory(model);
                return Ok(result);
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("BatchHistory-Child/Function/RetryBatchInstance")]
        public async Task<ActionResult> RetryBatchInstance([FromBody]BatchHistoryView model) {
            try {
                await RetryJob(model);
                return Ok(model);
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        [HttpPost]
        [Route("BatchHistory-Child/Function/CancelBatchInstance")]
        public async Task<ActionResult> CancelBatchInstance([FromBody]BatchHistoryView model) {
            try {
                var result = await CancelInstance(model);
                return Ok(result);
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Function/CleanUpBatchHistory")]
        public async Task<ActionResult> CleanUpBatchHistory([FromBody]CleanUpBatchHistoryView model) {
            try {
                var result = await CleanUpInstances(model);
                return Ok(result);
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteBatchJob")]
        public async Task<ActionResult> DeleteSysRoleTable([FromBody] Deletion item) {
            try
            {    
                BatchView itemToDelete = new BatchView();
                itemToDelete.JobId = item.Guid.GuidNullToString();

                bool result = await DeleteBatch(itemToDelete);
                return Ok(result);
            }
            catch (Exception ex) {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        
        
        private async Task<string> GetTokenFromRequest() {
            //var request = HttpContext.Request;
            //string reqAuthHeader = request.Headers["Authorization"];
            //if (reqAuthHeader != null) {
            //    string token = reqAuthHeader.Split(" ").Last();
            //    return (token != null && token != "") ? token : null;
            //}
            //return null;
            HttpHelper helper = new HttpHelper();
            return await helper.GetClientCredentialsToken(authUrl, webApiClient, webApiClientSecret, batchApiScope);
        }
        
        private async Task<bool> ValidateRetryInstanceHistory(BatchHistoryView model)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    string url = batchUrl + "Quartz/ValidateRetryInstanceHistory";
                    string companyGUID = GetCompanyFromRequest();

                    string token = await GetTokenFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    if (token != null && token != "")
                    {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    }
                    
                    if (companyGUID != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyGUID);
                    }

                    if(requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }

                    var jsonObject = JsonConvert.SerializeObject(model, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error validating retry instance.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error validating retry instance.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        bool result =
                            JsonConvert.DeserializeObject<bool>(jsonContent);
                        return result;
                    }
                }
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private async Task<SearchResult<BatchView>> GetQuartzBatchViewList(SearchParameter search)
        {
            try {
                using (var httpClient = new HttpClient()) {
                    
                    string url = batchUrl + "Quartz/GetQuartzBatchViewList";
                    string companyGUID = GetCompanyFromRequest();
                    
                    string token = await GetTokenFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    if (token != null && token != "") {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    }
                    
                    if (companyGUID != null) {
                        httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyGUID);
                    }

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }

                    var jsonObject = JsonConvert.SerializeObject(search, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error getting BatchView list.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error getting BatchView list.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        SearchResult<BatchView> result =
                            JsonConvert.DeserializeObject<SearchResult<BatchView>>(jsonContent);
                        return result;
                    }
                    
                }
            }
            catch(Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<SearchResult<BatchHistoryView>> GetQuartzBatchHistoryViewByJobId(SearchParameter search)
        {
            try {
                using (var httpClient = new HttpClient()) {

                    string url = batchUrl + "Quartz/GetQuartzBatchHistoryViewList";
                    string companyGUID = GetCompanyFromRequest();
                    string token = await GetTokenFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    if (token != null && token != "") {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    }
                    
                    if (companyGUID != null) {
                        httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyGUID);
                    }

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }

                    var jsonObject = JsonConvert.SerializeObject(search, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error getting BatchHistoryView list.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error getting BatchHistoryView list.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        SearchResult<BatchHistoryView> result =
                            JsonConvert.DeserializeObject<SearchResult<BatchHistoryView>>(jsonContent);
                        return result;
                    }

                }
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private async Task<SearchResult<BatchInstanceLogView>> GetQuartzBatchInstanceLogViewByInstanceId(SearchParameter search)
        {
            try {
                using (var httpClient = new HttpClient()) {

                    string url = batchUrl + "Quartz/GetQuartzBatchInstanceLogViewList";
                    string companyGUID = GetCompanyFromRequest();
                    string token = await GetTokenFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    if (token != null && token != "") {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    }


                    if (companyGUID != null) {
                        httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyGUID);
                    }

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }

                    var jsonObject = JsonConvert.SerializeObject(search, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error getting BatchInstanceLogView list.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error getting BatchInstanceLogView list.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        SearchResult<BatchInstanceLogView> result =
                            JsonConvert.DeserializeObject<SearchResult<BatchInstanceLogView>>(jsonContent);
                        return result;
                    }
                    
                }
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<BatchView> GetQuartzBatchViewByJobId(string jobId) {
            try {

                using (var httpClient = new HttpClient()) {

                    string url = batchUrl + "Quartz/GetQuartzBatchViewItem";
                    url += "/" + jobId;
                    string companyGUID = GetCompanyFromRequest();

                    string token = await GetTokenFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    if (token != null && token != "") {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    }


                    if (companyGUID != null) {
                        httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyGUID);
                    }

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }

                    var response = await httpClient.GetAsync(url);

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error getting BatchView.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error getting BatchView.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        BatchView result =
                            JsonConvert.DeserializeObject<BatchView>(jsonContent);
                        return result;
                    }
                    
                }

            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<BatchHistoryView> GetQuartzBatchHistoryViewById(string instanceId) {
            try {

                using (var httpClient = new HttpClient()) {

                    string url = batchUrl + "Quartz/GetQuartzBatchHistoryViewItem";
                    url += "/" + instanceId;
                    string companyGUID = GetCompanyFromRequest();

                    string token = await GetTokenFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    if (token != null && token != "") {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    }


                    if (companyGUID != null) {
                        httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyGUID);
                    }

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }

                    var response = await httpClient.GetAsync(url);

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error getting BatchHistoryView.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error getting BatchHistoryView.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        BatchHistoryView result =
                            JsonConvert.DeserializeObject<BatchHistoryView>(jsonContent);
                        return result;
                    }
                    
                }

            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<BatchInstanceLogView> GetQuartzBatchInstanceLogById(int logId) {
            try {

                using (var httpClient = new HttpClient()) {

                    string url = batchUrl + "Quartz/GetQuartzBatchInstanceLogViewItem";
                    url += "/" + logId.ToString();
                    string companyGUID = GetCompanyFromRequest();

                    string token = await GetTokenFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    if (token != null && token != "") {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    }


                    if (companyGUID != null) {
                        httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyGUID);
                    }

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }


                    var response = await httpClient.GetAsync(url);

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error getting BatchInstanceLogView.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error getting BatchInstanceLogView.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        BatchInstanceLogView result =
                            JsonConvert.DeserializeObject<BatchInstanceLogView>(jsonContent);
                        return result;
                    }
                    
                }

            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<dynamic> CreateScheduledJob(BatchObjectView batchObject) {

            try {
                batchObject.CreatedBy = SmartAppUtil.GetUserNameWithoutDomain(db.GetUserName());
                using (var httpClient = new HttpClient()) {

                    string url = batchScheduleJobUrl;
                    
                    string token = await GetTokenFromRequest();
                    string companyHeader = GetCompanyFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyHeader);

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }


                    var jsonObject = JsonConvert.SerializeObject(batchObject, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error Scheduling job.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error Scheduling job.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        return Ok(content);
                    }

                    
                }
            }
            catch (Exception e) {
                // error occurred

                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        private async Task<dynamic> UnscheduleJob(BatchView batchObject) {
            try {
                batchObject.ModifiedBy = db.GetUserName();
                using (var httpClient = new HttpClient()) {

                    string url = batchUnscheduleJobUrl;
                   
                    string token = await GetTokenFromRequest();
                    string companyHeader = GetCompanyFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyHeader);

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }


                    var jsonObject = JsonConvert.SerializeObject(batchObject, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                   
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error UnScheduling job.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error UnScheduling job.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        return Ok(content);
                    }
                    

                }
            }
            catch (Exception e) {
                // error occurred

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<dynamic> PauseJob(BatchView batchObject) {
            try {
                batchObject.ModifiedBy = db.GetUserName();
                using (var httpClient = new HttpClient()) {

                    string url = batchPauseJobUrl;
                   
                    string token = await GetTokenFromRequest();
                    string companyHeader = GetCompanyFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyHeader);

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }


                    var jsonObject = JsonConvert.SerializeObject(batchObject, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                  
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error pausing job.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error pausing job.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        return Ok(content);
                    }
                    
                }
            }
            catch (Exception e) {
                // error occurred

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<dynamic> ResumeJob(BatchView batchObject) {
            try {
                batchObject.ModifiedBy = db.GetUserName();
                using (var httpClient = new HttpClient()) {

                    string url = batchResumeJobUrl;
                   
                    string token = await GetTokenFromRequest();
                    string companyHeader = GetCompanyFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyHeader);

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }


                    var jsonObject = JsonConvert.SerializeObject(batchObject, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error resuming job.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error resuming job.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        return Ok(content);
                    }
                    
                }
            }
            catch (Exception e) {
                // error occurred

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<dynamic> RetryJob(BatchHistoryView batchJob) {
            try {
                batchJob.ModifiedBy = db.GetUserName();
                using (var httpClient = new HttpClient()) {

                    string url = batchRetryJobUrl;
                    
                    string token = await GetTokenFromRequest();
                    string companyHeader = GetCompanyFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyHeader);
                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }


                    var jsonObject = JsonConvert.SerializeObject(batchJob, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error retrying job.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error retrying job.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        return Ok(content);
                    }
                    

                }
            }
            catch (Exception e) {
                // error occurred

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<dynamic> CancelInstance(BatchHistoryView batchJob) {
            try {
                batchJob.ModifiedBy = db.GetUserName();
                using (var httpClient = new HttpClient()) {

                    string url = batchCancelInstanceUrl;
                    
                    string token = await GetTokenFromRequest();
                    string companyHeader = GetCompanyFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyHeader);
                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }


                    var jsonObject = JsonConvert.SerializeObject(batchJob, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error canceling instance.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error canceling instance.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        return Ok(content);
                    }
                    

                }
            }
            catch (Exception e) {
                // error occurred

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<bool> CleanUpInstances(CleanUpBatchHistoryView model) {
            try {
                using (var httpClient = new HttpClient()) {

                    string url = batchCleanUpHistoryUrl;
                    
                    string token = await GetTokenFromRequest();
                    string companyHeader = GetCompanyFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyHeader);

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }


                    var jsonObject = JsonConvert.SerializeObject(model, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error cleaning up instance.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error cleaning up instance.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        bool result =
                            JsonConvert.DeserializeObject<bool>(jsonContent);
                        return result;
                    }
                    
                }
            }
            catch (Exception e) {
                // error occurred

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private async Task<bool> DeleteBatch(BatchView model) {
            try {
                using (var httpClient = new HttpClient()) {
                    string url = batchDeleteBatchJobUrl;

                    string token = await GetTokenFromRequest();
                    string companyHeader = GetCompanyFromRequest();
                    string requestId = GetRequestIdFromRequest();

                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    httpClient.DefaultRequestHeaders.Add("CompanyHeader", companyHeader);

                    if (requestId != null)
                    {
                        httpClient.DefaultRequestHeaders.Add("RequestHeader", requestId);
                    }


                    var jsonObject = JsonConvert.SerializeObject(model, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));
                    dynamic content = await response.Content.ReadAsAsync<dynamic>();

                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("Error deleting batch job.", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException("Error deleting batch job.: \n" + errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        bool result =
                            JsonConvert.DeserializeObject<bool>(jsonContent);
                        return result;
                    }
                    
                }

            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
