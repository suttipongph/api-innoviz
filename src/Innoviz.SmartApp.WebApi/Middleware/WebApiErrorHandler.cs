﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Innoviz.SmartApp.WebApi.Middleware
{
    public class WebApiErrorHandler {
        private readonly RequestDelegate next;

        public WebApiErrorHandler(RequestDelegate next) {
            this.next = next;
        }
        
        public async Task Invoke(HttpContext context/* other dependencies */) {
            try {
                
                await next(context);
               
            } 
            catch (SmartAppException ex) {
                
                await HandleSmartAppExceptionAsync(context, ex);

            } 
            catch (Exception e) {
                await HandleServerException(context, e);
            }    
        }

        private Task HandleSmartAppExceptionAsync(HttpContext context, SmartAppException ex) {

            string stackTrace = (string)ex.Data[ExceptionDataKey.StackTrace];
            if (stackTrace == null) {
                stackTrace = ex.StackTrace;
            }
            else {
                ex.Data.Remove(ExceptionDataKey.StackTrace);
            }
            
            ex.Data.Remove(ExceptionDataKey.InnerStackTrace);
            
            List<string> msgList = (List<string>)ex.Data[ExceptionDataKey.MessageList];
            if (msgList != null) {
                ex.Data.Remove(ExceptionDataKey.MessageList);
            }

            IDictionary batchLogReference = (IDictionary)ex.Data[ExceptionDataKey.BatchLogReference];
            if(batchLogReference != null)
            {
                ex.Data.Remove(ExceptionDataKey.BatchLogReference);
            }
            var batchLogRefData = ex.BatchLogReference.Count > 0 ? ex.BatchLogReference : batchLogReference;
            if (batchLogRefData != null && batchLogRefData.Count > 0)
            {
                foreach (var k in batchLogRefData.Keys)
                {
                    SmartAppExceptionData data = (SmartAppExceptionData)batchLogRefData[k];
                    if (data != null && data.BatchLogReference != null)
                    {
                        string errorCodeLower = data.ErrorCode.ToLower();
                        ex.Data.AddMsgCode(errorCodeLower, data.ErrorCode);
                        ex.DataParameters.AddMsgCode(errorCodeLower, data.Parameters);
                    }
                }
            }
            var responseMsg = new WebApiErrorResponseMessage
            {
                Source = ex.Source,
                
                Message = ex.Message,
                ErrorMessage = ex.Data,
                ErrorParameter = ex.DataParameters,
                RowParameter = ex.RowParameters,
                StackTrace = stackTrace,
                
                MessageList = (ex.MessageList != null && ex.MessageList.Count() != 0) ?
                            ex.MessageList :
                            (msgList != null ?
                                msgList : GetReadableErrorMessage(ex)),
            };
            
            //log error
            LogError(responseMsg);

            if (context != null) {
                if(context.Response.HasStarted) {
                    throw SmartAppUtil.AddStackTrace(ex);
                }

                var exceptionType = ex.GetType();
                var response = context.Response;

                if(responseMsg != null && !string.IsNullOrWhiteSpace(responseMsg.Message) && responseMsg.Message.Split(".")[0].ToUpper() == "TestErrorCode".ToUpper())
                {
                    var splits = responseMsg.Message.Split(".");
                    if (splits.Length > 1)
                    {
                        response.StatusCode = Convert.ToInt32(splits[1]);
                    }
                    else
                    {
                        response.StatusCode = 400;
                    }
                }
                else
                {
                    response.StatusCode = 400;
                }

                responseMsg.Code = response.StatusCode.ToString();

                if(context.Request.ContentType == "application/xml")
                {
                    var result = Encoding.UTF8.GetBytes(
                            GetErrorAsXml(responseMsg)
                        );
                    response.ContentType = "application/xml";
                    return response.Body.WriteAsync(result, 0, result.Length);
                }
                else 
                {
                    //if(!context.Request.Path.Value.Contains("TestInput444ForErrorCode444"))
                    //{
                        var result = Encoding.UTF8.GetBytes(
                        JsonConvert.SerializeObject(responseMsg,
                            new JsonSerializerSettings
                            {
                                ContractResolver = new CamelCasePropertyNamesContractResolver()
                            })
                        );
                        //if (context.Request.Path.Value.Contains("TestInput500ForErrorCode500"))
                        //{
                        //    response.ContentType = "text/plain";
                        //}
                        //else
                        //{
                            response.ContentType = "application/json";
                        //}
                        return response.Body.WriteAsync(result, 0, result.Length);
                    //}
                    //else
                    //{
                    //    var result = Encoding.UTF8.GetBytes(responseMsg.Message);
                    //    response.ContentType = "text/plain";
                    //    return response.Body.WriteAsync(result, 0, result.Length);
                    //}
                    
                }
                
            }
            else {
                return Task.CompletedTask;
            }

        }
    

        private Task HandleServerException(HttpContext ctx, Exception e) {
            string stackTrace = (string)e.Data[ExceptionDataKey.StackTrace];
            if (stackTrace == null) {
                stackTrace = e.StackTrace;
            }
            else {
                e.Data.Remove(ExceptionDataKey.StackTrace);
            }

            e.Data.Remove(ExceptionDataKey.InnerStackTrace);

            List<string> msgList = (List<string>)e.Data[ExceptionDataKey.MessageList];
            if (msgList != null) {
                e.Data.Remove(ExceptionDataKey.MessageList);
            }

            IDictionary batchLogReference = (IDictionary)e.Data[ExceptionDataKey.BatchLogReference];
            if (batchLogReference != null)
            {
                e.Data.Remove(ExceptionDataKey.BatchLogReference);
            }
            IDictionary dataParameters = new Dictionary<string, dynamic>();
            if (batchLogReference != null && batchLogReference.Count > 0)
            {
                foreach (var k in batchLogReference.Keys)
                {
                    SmartAppExceptionData data = (SmartAppExceptionData)batchLogReference[k];
                    if (data != null && data.BatchLogReference != null)
                    {
                        string errorCodeLower = data.ErrorCode.ToLower();
                        e.Data.AddMsgCode(errorCodeLower, data.ErrorCode);
                        dataParameters.AddMsgCode(errorCodeLower, data.Parameters);
                    }
                }
            }
            var responseMsg = new WebApiErrorResponseMessage
            {
                Source = e.Source,
                Message = e.Message,
                ErrorMessage = e.Data,
                ErrorParameter = dataParameters,
                StackTrace = stackTrace,
                MessageList = GetReadableErrorMessage(e),
            };
            
            // log error
            LogError(responseMsg);

            if(ctx != null) {
                if (ctx.Response.HasStarted) {
                    throw SmartAppUtil.AddStackTrace(e);
                }
                if (ctx.Request.ContentType == "application/xml")
                {
                    ctx.Response.StatusCode = (batchLogReference != null && batchLogReference.Count > 0) ? 400 : 500;
                    ctx.Response.ContentType = "application/xml";
                    responseMsg.Code = ctx.Response.StatusCode.ToString();

                    var result = GetErrorAsXml(responseMsg);
                    return ctx.Response.WriteAsync(result);
                }
                else
                {
                    ctx.Response.StatusCode = (batchLogReference != null && batchLogReference.Count > 0) ? 400 : 500;
                    ctx.Response.ContentType = "application/json";
                    responseMsg.Code = ctx.Response.StatusCode.ToString();

                    var json =
                        JsonConvert.SerializeObject(responseMsg,
                                                    new JsonSerializerSettings()
                                                    {
                                                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                                                        Formatting = Newtonsoft.Json.Formatting.Indented,
                                                        ContractResolver = new CamelCasePropertyNamesContractResolver(),
                                                        NullValueHandling = NullValueHandling.Ignore
                                                    });
                    return ctx.Response.WriteAsync(json);

                }
                
            }
            else {
                return Task.CompletedTask;
            }
            
        }
        
        private List<string> GetReadableErrorMessage(Exception e) {
            List<string> result = new List<string>();
            if (e.GetType() == typeof(SmartAppException)) {
                SmartAppException ex = (SmartAppException)e;
                string exceptionMessage = SystemStaticData.GetTranslatedMessage(ex.Message);
                result.Add(exceptionMessage);
                foreach (var item in ex.Data.Keys) {
                    if(item.ToString() != ExceptionDataKey.StackTrace &&
                        item.ToString() != ExceptionDataKey.InnerStackTrace &&
                        item.ToString() != ExceptionDataKey.MessageList) {
                        string code = (string)ex.Data[item];

                        object[] param = null;
                        try {
                            param = (object[])ex.DataParameters[item];
                        }
                        catch (KeyNotFoundException) {
                            param = null;
                        }

                        var splitCode = code.Split('.');
                        if (splitCode.Length > 2) {
                            code = splitCode[0] + '.' + splitCode[1];
                        }
                        string message = null;
                        if (param != null) {
                            message = SystemStaticData.GetTranslatedMessage(code, param);
                        }
                        else {
                            message = SystemStaticData.GetTranslatedMessage(code);
                        }
                        result.Add(message);
                    }
                }
                return result;

            }
            else {
                if(e.InnerException != null) {
                    result = GetErrorMessageFromInnerException(e, result);
                }
                result.Add(e.Message);

                return result;
            }
        }
        
        private void LogError(WebApiErrorResponseMessage responseMsg)
        {
            try
            {
                // log error
                var _logger = Log.Logger;
                string msgListTemplate = "";
                string newLine = TextConstants.NewLine;
                List<string> logItems = new List<string>();
                if (responseMsg.MessageList.Count() != 0)
                {
                    msgListTemplate = newLine + "#ApiErrorMessages" + newLine;
                    for(int i = 0; i < responseMsg.MessageList.Count(); i++)
                    {
                        var item = responseMsg.MessageList[i];
                        msgListTemplate += "{MsgList" + i + "}";
                        logItems.Add(item);
                        if(i < responseMsg.MessageList.Count() - 1)
                        {
                            msgListTemplate += newLine;
                        }
                    }
                }
                msgListTemplate += newLine + "#StackTrace"+ newLine +"{StackTrace}";
                logItems.Add(responseMsg.StackTrace);
                _logger.Error(msgListTemplate, logItems.ToArray<object>());

            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }

        private string GetErrorAsXml(WebApiErrorResponseMessage responseMsg)
        {
            try
            {
                var serializer = new DataContractSerializer(responseMsg.GetType());
                using (var sw = new StringWriter())
                {
                    using (var writer = new XmlTextWriter(sw))
                    {
                        writer.Formatting = System.Xml.Formatting.Indented;
                        serializer.WriteObject(writer, responseMsg);
                        writer.Flush();
                        return sw.ToString();
                    }
                }
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<string> GetErrorMessageFromInnerException(Exception e, List<string> result)
        {
            try
            {
                if (e.InnerException == null)
                {
                    return result;
                }
                else
                {
                    result.Add(e.InnerException.Message);
                    return GetErrorMessageFromInnerException(e.InnerException, result);
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
    }
}
