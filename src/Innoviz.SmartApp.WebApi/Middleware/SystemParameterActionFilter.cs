﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Services;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.Middleware
{
    public class SystemParameterActionFilter : IActionFilter
    {
        private readonly IConfiguration Configuration;

        public SystemParameterActionFilter(IConfiguration config)
        {
            Configuration = config;
        }
        public void OnActionExecuting(ActionExecutingContext context)
        {
            // works only with BaseControllerV2
            if(context.Controller.GetType().BaseType != typeof(BaseControllerV2))
            {
                return;
            }

            var userName = GetUserName(context);
            var companyGuid = GetCompanyHeader(context);
            var businessUnitGuid = GetBU(context, userName, companyGuid);
            // set system parameters from HttpContext
            SystemParameter param = new SystemParameter
            {
                AccessLevel = GetAccessLevel(context, userName, businessUnitGuid, companyGuid),
                DeleteAccessLevel = GetDeleteAccessLevel(context, userName, businessUnitGuid, companyGuid),
                BranchGUID = GetFromCustomHttpHeader(context, "BranchHeader"),
                CompanyGUID = companyGuid,
                SiteLogin =  GetLoginSiteValue(GetFromCustomHttpHeader(context, "SiteHeader")),
                InstanceHistoryId = GetFromCustomHttpHeader(context, "BatchInstanceId"),
                UserId = GetFromUserClaims(context, "usubj"),
                UserName = userName.Value,
                BearerToken = GetFromCustomHttpHeader(context, "Authorization").Split(" ").Last(),
                RequestId = GetFromCustomHttpHeader(context, "RequestHeader"),
                BusinessUnitGUID = businessUnitGuid
            };
            
            BaseControllerV2 controller = (BaseControllerV2)context.Controller;
            controller.SetSystemParameter(param);
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {

        }

        #region Get values from HttpContext
        private AccessLevelParm GetAccessLevel(ActionExecutingContext context, UserNameClientIdParm parm, string businessUnitGUID, string companyGUID)
        {
            AccessLevel accessLevel = (AccessLevel)Convert.ToInt32(context.HttpContext.Items["AccessLevel"]);
            var result = GetAccessLevelParm(context, accessLevel, parm, businessUnitGUID, companyGUID);
            var passedIsCreateCompany = context.HttpContext.Items["IsCreateCompany"];
            bool isCreateCompany = passedIsCreateCompany == null ? false : Convert.ToBoolean(passedIsCreateCompany);
            result.SkipCheck = isCreateCompany;
            return result;
        }
        private AccessLevelParm GetDeleteAccessLevel(ActionExecutingContext context, UserNameClientIdParm parm, string businessUnitGUID, string companyGUID)
        {
            AccessLevel accessLevel = (AccessLevel)Convert.ToInt32(context.HttpContext.Items["DeleteAccessLevel"]);
            var result = GetAccessLevelParm(context, accessLevel, parm, businessUnitGUID, companyGUID);
            return result;
        }
        private string GetFromUserClaims(ActionExecutingContext context, string claim)
        {
            var result = context.HttpContext.User?.Claims?
                                           .Where(x => x.Type == claim).SingleOrDefault();
            return result?.Value;
        }
        private string GetFromCustomHttpHeader(ActionExecutingContext context, string headerKey)
        {
            var result = context.HttpContext.Request.Headers[headerKey];

            return result;
        }
        private string GetCompanyHeader(ActionExecutingContext context)
        {
            string result = GetFromCustomHttpHeader(context, "CompanyHeader");
            if(result == null)
            {
                var companyParm = context.ActionArguments.Where(x => x.Key.ToUpper() == TextConstants.CompanyGUID.ToUpper()).FirstOrDefault();
                result = companyParm.Value?.ToString();
            }
            return result;
        }
        private UserNameClientIdParm GetUserName(ActionExecutingContext context)
        {
            var userName = GetFromUserClaims(context, "username");
            if (userName == null)
            {
                var clientId = GetFromUserClaims(context, "client_id");
                if (clientId == null)
                {
                    throw new NullReferenceException("System Parameter [client_id] cannot be null.");
                }
                else
                {
                    return new UserNameClientIdParm { Value = clientId, IsClientId = true };
                }
            }
            else
            {
                return new UserNameClientIdParm { Value = userName, IsClientId = false };
            }
        }
        private AccessLevelParm GetAccessLevelParm(ActionExecutingContext context, AccessLevel accessLevel, UserNameClientIdParm parm, 
                                                    string businessUnitGUID, string companyGUID) 
        {
            try
            {
                switch (accessLevel)
                {
                    case AccessLevel.None:
                        return new AccessLevelParm { AccessLevel = AccessLevel.None.GetAttrValue() };
                    case AccessLevel.User:
                        return new AccessLevelParm { AccessLevel = AccessLevel.User.GetAttrValue(), AccessValue = parm.Value };
                    case AccessLevel.BusinessUnit:
                        return new AccessLevelParm { AccessLevel = AccessLevel.BusinessUnit.GetAttrValue(), AccessValue = businessUnitGUID };
                    case AccessLevel.ParentChildBU:
                        return GetAccessLevelParmCaseParentChildBU(context, accessLevel, businessUnitGUID, companyGUID);
                    case AccessLevel.Company:
                        return new AccessLevelParm { AccessLevel = AccessLevel.Company.GetAttrValue() };
                    default:
                        return null;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private AccessLevelParm GetAccessLevelParmCaseParentChildBU(ActionExecutingContext context, AccessLevel accessLevel, string businessUnitGUID, string companyGUID)
        {
            try
            {
                SmartAppDbContext db = ((BaseControllerV2)context.Controller).GetSmartAppDbContext();
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                AccessLevelParm result = null;
                if (accessLevel == AccessLevel.ParentChildBU)
                {
                    result = new AccessLevelParm();
                    result.AccessLevel = accessLevel.GetAttrValue();
                    result.AccessValues = accessLevelService.GetParentChildBusinessUnitGUIDsByBusinessUnit(businessUnitGUID, companyGUID).Select(s => s.GuidNullToString()).ToList();
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetBU(ActionExecutingContext context, UserNameClientIdParm parm, string companyGUID)
        {
            try
            {
                SmartAppDbContext db = ((BaseControllerV2)context.Controller).GetSmartAppDbContext();
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                if(parm.IsClientId)
                {
                    // case prod batch request web api controller list
                    var batchClient = Configuration["BatchApi:Client"];
                    if(!string.IsNullOrWhiteSpace(batchClient) && batchClient == parm.Value)
                    {
                        return null;
                    }
                    else
                    {
                        return accessLevelService.GetRootBusinessUnitGUIDByCompany(companyGUID).GuidNullToString();
                    }
                }
                else
                {
                    return accessLevelService.GetOwnerBusinessUnitGUIDByUserName(parm.Value, companyGUID).GuidNullToString();
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private int GetLoginSiteValue(string site)
        {
            try
            {
                return SysAccessLevelHelper.GetLoginSiteValue(site);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Get values from HttpContext
        private class UserNameClientIdParm
        {
            public string Value { get; set; }
            public bool IsClientId { get; set; }
        }
    }
}
