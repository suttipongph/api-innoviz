﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using Serilog.Context;
using Serilog.Events;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.Middleware
{
    public class RequestResponseLoggingMiddleware
    {
        private readonly string MessageTemplate =
            "HTTP {RequestMethod} {RequestPath} responded code: {StatusCode} in {Elapsed:0.0000} ms";
        private readonly string ErrorMessageTemplate =
            TextConstants.NewLine + "Reqest headers: {RequestHeaders}" +
            TextConstants.NewLine + "Reqest body: {RequestBody}" +
            TextConstants.NewLine + "Reqest host: {RequestHost}" +
            TextConstants.NewLine + "Reqest protocol: {RequestProtocol}"; 

        private readonly RequestDelegate next;

        public RequestResponseLoggingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context, IConfiguration config)
        {
            string requestId = context.Request.Headers["RequestHeader"];
            if (requestId == null)
            {
                requestId = context.TraceIdentifier;
            }
           
            using (LogContext.PushProperty("HttpRequestId", requestId))
            using (LogContext.PushProperty("RequestId", requestId))
            {
                string template = "HTTP {RequestMethod} {RequestPath} started.";
                Log.Logger.Write(LogEventLevel.Information, template,
                                        context.Request.Method,
                                        context.Request.Path);
                var sw = Stopwatch.StartNew();
                string requestContentType = context.Request.ContentType;
                context.Request.EnableBuffering();
                string requestContent = await new StreamReader(context.Request.Body).ReadToEndAsync();
                context.Request.Body.Position = 0;

                Stream responseBody = context.Response.Body;

                try
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        string responseContentType = context.Response.ContentType;
                        context.Response.Body = memoryStream;
                        await next(context);

                        var statusCode = context.Response?.StatusCode;

                        var level = (statusCode == 400 || statusCode > 499) ?
                            LogEventLevel.Error : LogEventLevel.Information;

                        string encKey = config["EncryptionKey"];
                        bool enableEncryption = Convert.ToBoolean(config["DeploymentConfig:EnableEncryption"]);
                        string encSaltIV = null;


                        string responseContent = null;
                        if (level == LogEventLevel.Error &&
                            requestContent != null &&
                            requestContent != "" &&
                            enableEncryption)
                        {
                            // generate salt, iv only in production
                            encSaltIV = SmartAppUtil.GenerateEncryptedSaltIV(encKey);
                            context.Response.Body.Seek(0, SeekOrigin.Begin);
                            
                            responseContent = await new StreamReader(context.Response.Body).ReadToEndAsync();

                            context.Response.Body.Seek(0, SeekOrigin.Begin);

                            if (responseContent != null && responseContent != "")
                            {
                                if(responseContentType == "application/json")
                                {
                                    var responseObj = JObject.Parse(responseContent);
                                    responseObj.Property("messageList").AddAfterSelf(new JProperty("encSaltIV", encSaltIV));

                                    using (MemoryStream respStream = new MemoryStream())
                                    using (var streamWriter = new StreamWriter(respStream))
                                    using (var jsonWriter = new JsonTextWriter(streamWriter))
                                    {
                                        var serializer = new JsonSerializer();
                                        serializer.Serialize(jsonWriter, responseObj);
                                        streamWriter.Flush();

                                        respStream.Position = 0;
                                        await respStream.CopyToAsync(responseBody);
                                    }

                                }
                                else
                                {
                                    memoryStream.Position = 0;
                                    await memoryStream.CopyToAsync(responseBody);
                                }

                            }
                            else
                            {
                                memoryStream.Position = 0;
                                await memoryStream.CopyToAsync(responseBody);
                            }
                        }
                        else
                        {
                            memoryStream.Position = 0;
                            await memoryStream.CopyToAsync(responseBody);

                        }


                        string reqBody = null;
                        if (requestContent != null && requestContent != "")
                        {
                            string jsonString = null;
                            if(requestContentType == "application/json")
                            {
                                if (requestContent.StartsWith('['))
                                {
                                    var jsonObject = JArray.Parse(requestContent);
                                    foreach (var item in jsonObject)
                                    {
                                        if (item.Type == JTokenType.Object)
                                        {
                                            TruncateReqBody(item);
                                        }

                                    }
                                    jsonString = JsonConvert.SerializeObject(jsonObject);
                                }
                                else
                                {
                                    var jsonObject = JObject.Parse(requestContent);

                                    TruncateReqBody(jsonObject);
                                    jsonString = JsonConvert.SerializeObject(jsonObject);
                                }
                                // encryption logic
                                if (enableEncryption)
                                {
                                    jsonString = SmartAppUtil.EncryptString(jsonString, encKey, encSaltIV);
                                }
                                else
                                {
                                    jsonString = JToken.Parse(jsonString).ToString(Formatting.Indented);
                                }

                                reqBody = jsonString;
                            }

                            else if (requestContentType == "application/xml")
                            {
                                reqBody = requestContent;
                            }
                            

                        }
                        sw.Stop();
                        Log.Logger.Write(level, MessageTemplate,
                                        context.Request.Method,
                                        context.Request.Path,
                                        statusCode,
                                        sw.Elapsed.TotalMilliseconds);
                        if (level == LogEventLevel.Error)
                        {
                            string headers = JsonConvert.SerializeObject(context.Request.Headers
                                                        .ToDictionary(h => h.Key, h => h.Value.ToString()),
                                                        Formatting.Indented);
                            Log.Logger.Write(level,
                                            ErrorMessageTemplate,
                                            headers,
                                            reqBody,
                                            context.Request.Host,
                                            context.Request.Protocol);
                        }
                    }

                }
                catch (Exception e)
                {
                    throw SmartAppUtil.AddStackTrace(e);
                }
            }
            
        }

        private void TruncateReqBody(JToken reqBody)
        {
            try
            {
                if(reqBody.Type == JTokenType.Object)
                {
                    foreach (var prop in ((JObject)reqBody).Properties())
                    {
                        if (prop.Value.Type == JTokenType.Array)
                        {
                            if (prop.Value.Count() != 0)
                            {
                                foreach (var item in prop.Value)
                                {
                                    TruncateReqBody(item);
                                }
                            }

                        }
                        else if (prop.Value.Type == JTokenType.Object)
                        {
                            if (prop.Value.Count() != 0)
                            {
                                TruncateReqBody(prop.Value);
                            }
                        }
                        else
                        {
                            if (prop.Value.Type == JTokenType.String)
                            {
                                int strLen = ((string)prop.Value).Length;
                                if (strLen > 300)
                                {
                                    prop.Value = "(omitted)";
                                }
                            }
                        }

                    }
                }
                else if(reqBody.Type == JTokenType.Array)
                {
                    foreach(var item in reqBody)
                    {
                        TruncateReqBody(item);
                    }
                }
                
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

    }
}
