if exists (select 'x' From sys.objects o where object_id = OBJECT_ID(N'LSS_BulkNumberToWords', 'P')) 
begin 
	drop proc LSS_BulkNumberToWords
end
go
create proc LSS_BulkNumberToWords (
	@DecimalString		nvarchar(max) ,
	@Delimiter			nvarchar(1) ,
	@Language			nvarchar(2) ,
	@Suffix				nvarchar(max) = null
)
as
begin 

	begin try

		select	sp.[value] as ValueDecimal , 
				case when @Language = 'EN' then dbo.fn_NumberToWords(sp.[value]) 
					 else dbo.fn_NumberToWordsTH(sp.[value] ,@Suffix) 
				end as ValueInWords
		from	fn_Split(@DecimalString,@Delimiter) sp

	end try
	begin catch

		select ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE()

	end catch
	
end

-- example
--go
--exec LSS_BulkNumberToWords '990022,2938.1' , ',' , 'EN'

--go
-- exec LSS_BulkNumberToWords '990022,2938.1' , ',' , 'TH'