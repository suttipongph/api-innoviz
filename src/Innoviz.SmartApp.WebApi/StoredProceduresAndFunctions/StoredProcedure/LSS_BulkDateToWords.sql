if exists (select 'x' From sys.objects o where object_id = OBJECT_ID(N'LSS_BulkDateToWords', 'P')) 
begin 
	drop proc LSS_BulkDateToWords
end
go
create proc LSS_BulkDateToWords (
	@date	            nvarchar(max) ,
	@format			    nvarchar(100) ,
	@lang			    nvarchar(2) ,
	@delimiter			nvarchar(1) 
)
as
begin 

	begin try

		select	sp.[value] as dateValue , 
		dbo.fn_DateToWord(convert(date, sp.[value]), @lang, @format)  as dateWord
		from	fn_Split(@date,@delimiter) sp
	end try
	begin catch

		select ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE()

	end catch
	
end

-- example
--go
--LSS_BulkDateToWords '8/5/2020, 12/19/2020', 'MONTH', 'TH', ','
