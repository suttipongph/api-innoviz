If Exists (Select 'x' From sys.objects o Where object_id = OBJECT_ID(N'LSS_CustomerAgingBucket', 'P')) 
Begin 
	Drop Proc [LSS_CustomerAgingBucket]
End
Go
/****** Object:  StoredProcedure [dbo].[LSS_CustomerAgingBucket]    Script Date: 8/13/2020 11:41:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[LSS_CustomerAgingBucket] (
	@balanceasof date ,
	@startdate date ,
	@interval int ,
	@dayormonth nvarchar(2),
	@printdirection nvarchar(2),
	@language nvarchar(10) ,
	@numberofbucket int = 7
)
as
begin 

	begin try
		declare	@enumDay int ,
				@enumMonth int ,
				@enumBackward int ,
				@enumForward int ,
				@mindate date = '1900-01-01' ,
				@maxdate date = '2900-01-01',
				@labelBalanceAsOf nvarchar(100),
				@labelDueBefore nvarchar(100),
				@labelDueAfter nvarchar(100)

		select	@enumDay = AttributeValue from SysEnumTable where EnumName = 'DayOrMonth' and AttributeName = 'Day' 
		select	@enumMonth = AttributeValue from SysEnumTable where EnumName = 'DayOrMonth' and AttributeName = 'Month' 
		select	@enumBackward = AttributeValue from SysEnumTable where EnumName = 'PrintingDirection' and AttributeName = 'Backward' 
		select	@enumForward = AttributeValue from SysEnumTable where EnumName = 'PrintingDirection' and AttributeName = 'Forward' 
		select @labelBalanceAsOf = Label from SysLabelReport where LabelId = 'BALANCE_AS_OF_2' and LanguageId = @language
		select @labelDueBefore= Label from SysLabelReport where LabelId = 'DUE_BEFORE' and LanguageId = @language
		select @labelDueAfter= Label from SysLabelReport where LabelId = 'DUE_AFTER' and LanguageId = @language

		;with query as (
			select	1 as [bucket] , @balanceasof as [FromDateValue] , @balanceasof as [ToDateValue] 
			union all
			select	[bucket] + 1 ,
					case when [bucket] + 1 = 2 then null
						 when [bucket] + 1 = 3 then 
							case when convert(int,@dayormonth) = @enumDay and convert(int,@printdirection) = @enumForward then [ToDateValue]
								 when convert(int,@dayormonth) = @enumDay and convert(int,@printdirection) = @enumBackward then DATEADD(DAY,(@interval-1)*(-1),[ToDateValue])
								 when convert(int,@dayormonth) = @enumMonth and convert(int,@printdirection) = @enumForward then [ToDateValue]
								 when convert(int,@dayormonth) = @enumMonth and convert(int,@printdirection) = @enumBackward then CAST(DATEADD(MONTH,(@interval-1)*(-1),DATEADD(MONTH,DATEDIFF(MONTH, 0, [ToDateValue]), 0)) as date)
							end
						 when [bucket] + 1 = @numberofbucket then null
						 else 
							case when convert(int,@dayormonth) = @enumDay and convert(int,@printdirection) = @enumForward then DATEADD(DAY,@interval,[FromDateValue])
								 when convert(int,@dayormonth) = @enumDay and convert(int,@printdirection) = @enumBackward then DATEADD(DAY,@interval*(-1),[FromDateValue])
								 when convert(int,@dayormonth) = @enumMonth and convert(int,@printdirection) = @enumForward then DATEADD(MONTH,@interval,[FromDateValue])
								 when convert(int,@dayormonth) = @enumMonth and convert(int,@printdirection) = @enumBackward then DATEADD(MONTH,@interval*(-1),[FromDateValue])
							end
						 end 
					as [FromDateValue] ,
					case when [bucket] + 1 = 2 then 
							case when convert(int,@dayormonth) = @enumDay and convert(int,@printdirection) = @enumForward then @startdate
								 when convert(int,@dayormonth) = @enumDay and convert(int,@printdirection) = @enumBackward then @startdate
								 when convert(int,@dayormonth) = @enumMonth and convert(int,@printdirection) = @enumForward then CAST(DATEADD(MONTH,DATEDIFF(MONTH, 0, @startdate), 0) as date)
								 when convert(int,@dayormonth) = @enumMonth and convert(int,@printdirection) = @enumBackward then CAST(EOMONTH(DATEADD(MONTH,-1,@startdate)) as date)
							end
						 when [bucket] + 1 = 3 then 
							case when convert(int,@dayormonth) = @enumDay and convert(int,@printdirection) = @enumForward then DATEADD(DAY,@interval-1,[ToDateValue])
								 when convert(int,@dayormonth) = @enumDay and convert(int,@printdirection) = @enumBackward then DATEADD(DAY,@interval-1,DATEADD(DAY,(@interval-1)*(-1),[ToDateValue]))
								 when convert(int,@dayormonth) = @enumMonth and convert(int,@printdirection) = @enumForward then CAST(EOMONTH(DATEADD(MONTH,@interval-1,[ToDateValue])) as date)
								 when convert(int,@dayormonth) = @enumMonth and convert(int,@printdirection) = @enumBackward then CAST(EOMONTH(DATEADD(MONTH,@interval-1,DATEADD(MONTH,(@interval-1)*(-1),DATEADD(MONTH,DATEDIFF(MONTH, 0, [ToDateValue]), 0)))) as date)
							end
						 when [bucket] + 1 = @numberofbucket then 
							case when convert(int,@printdirection) = @enumForward then [ToDateValue]
								 when convert(int,@printdirection) = @enumBackward then [FromDateValue]
							end
						 else 
							case when convert(int,@dayormonth) = @enumDay and convert(int,@printdirection) = @enumForward then DATEADD(DAY,@interval-1,DATEADD(DAY,@interval,[FromDateValue]))
								 when convert(int,@dayormonth) = @enumDay and convert(int,@printdirection) = @enumBackward then DATEADD(DAY,@interval-1,DATEADD(DAY,@interval*(-1),[FromDateValue]))
								 when convert(int,@dayormonth) = @enumMonth and convert(int,@printdirection) = @enumForward then CAST(EOMONTH(DATEADD(MONTH,@interval-1,DATEADD(MONTH,@interval,[FromDateValue]))) as date)
								 when convert(int,@dayormonth) = @enumMonth and convert(int,@printdirection) = @enumBackward then CAST(EOMONTH(DATEADD(MONTH,@interval-1,DATEADD(MONTH,@interval*(-1),[FromDateValue]))) as date)
							end
						 end 
					as [ToDateValue]
			from	query
			where	[bucket] < @numberofbucket
		) , result as (
			-- add description
			select	query.* ,
					case when query.[bucket] = 1 then @labelBalanceAsOf
						 when query.[bucket] = 2 then 
							case when convert(int,@printdirection) = @enumForward then @labelDueBefore
								 when convert(int,@printdirection) = @enumBackward then @labelDueAfter
							end 
						 when query.[bucket] = @numberofbucket then 
							case when convert(int,@printdirection) = @enumForward then @labelDueAfter
								 when convert(int,@printdirection) = @enumBackward then @labelDueBefore
							end
						 else CONVERT(VARCHAR(10),query.[FromDateValue],103)	
						 end 
					as [FromDate] ,
					CONVERT(VARCHAR(10),query.[ToDateValue],103) as [ToDate]
			from query
		)
		select *
		from result

	end try
	begin catch
		select ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE()
	end catch

end

-- example
--go
--exec LSS_CustomerAgingBucket  @balanceasof = '2019-12-31' ,
--							  @startdate = '2019-12-31' ,
--							  @interval = 30 ,
--							  convert(int,@dayormonth) = 0 ,
--							  convert(int,@printdirection) = 1 
GO


