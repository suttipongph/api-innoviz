﻿if exists (select 'x' from sys.objects o where object_id = OBJECT_ID(N'LSS_ExecuteSsisPackage', 'P')) 
begin 
	drop proc LSS_ExecuteSsisPackage
end
go
create proc LSS_ExecuteSsisPackage (
	@PackageName	nvarchar(50),
	@PackageFolder	nvarchar(50)
)
as
begin
		Declare @execution_id bigint
		EXEC [SSISDB].[catalog].[create_execution] @package_name=@PackageName,
			@execution_id=@execution_id OUTPUT,
			@folder_name=@PackageFolder,
			  @project_name=N'Innoviz.MigrationData',
  			@use32bitruntime=True,
			  @reference_id=Null
		Select @execution_id
		DECLARE @var0 smallint = 1
		EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id,
			@object_type=50,
			  @parameter_name=N'LOGGING_LEVEL',
			  @parameter_value=@var0
		EXEC [SSISDB].[catalog].[start_execution] @execution_id

end
