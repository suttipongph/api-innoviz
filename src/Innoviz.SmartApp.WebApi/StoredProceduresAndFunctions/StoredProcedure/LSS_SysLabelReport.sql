if exists (select 'x' from sys.objects o where object_id = OBJECT_ID(N'LSS_SysLabelReport', 'P')) 
begin 
	drop proc LSS_SysLabelReport
end
go
create proc LSS_SysLabelReport (
	@FilePath		VARCHAR(100) 
)
as
begin
	
	set nocount on
	declare @OLE int
	declare @FileId int 
	declare @sqltext nvarchar(max) = ''
	declare @path nvarchar(100)

	create table #syslabel (LabelType nvarchar(20) , LabelId nvarchar(100) , LanguageId nvarchar(20) , Label nvarchar(255) ,
		seq int identity primary key
	)

	exec sp_OACreate 'Scripting.FileSystemObject',@OLE out;
	select @path = @FilePath + '\SysLabelReport.json'
	exec sp_OAMethod @OLE, 'OpenTextFile', @FileId out, @path , 8 , 1 , -1;
	exec sp_OAMethod @FileId, 'WriteLine', Null, '[';

	
	insert into #syslabel (LabelType , LabelId , LanguageId , Label)
	select LabelType , LabelId , LanguageId , Label from SysLabelReport order by LabelId , LanguageId

	--select * From #syslabel order by seq
	--drop table #syslabel

	declare @seq int ,
			@maxrow int

	select @maxrow = max(seq) from #syslabel
	while exists (select 'x' from #syslabel)
	begin
		
		select @seq = seq from #syslabel order by seq desc

		select @sqltext = case when seq = @maxrow 
				then '{"LabelType": "'+ LabelType +'", "LabelId": "'+LabelId+'", "LanguageId": "'+LanguageId+'", "Label": "'+Label+'"}'
				else '{"LabelType": "'+ LabelType +'", "LabelId": "'+LabelId+'", "LanguageId": "'+LanguageId+'", "Label": "'+Label+'"},'
			end
		from #syslabel
		where seq = @seq

		exec sp_OAMethod @FileId, 'WriteLine', null, @sqltext
	
		delete #syslabel where seq = @seq

	end

	exec sp_OAMethod @FileId, 'WriteLine', null, ']';
	exec sp_OAMethod @FileId, 'Close';
	exec sp_OADestroy @FileId out;
	exec sp_OADestroy @OLE out;

	
	exec sp_OACreate 'Scripting.FileSystemObject',@OLE out;
	select @path = @FilePath + '\SysLabelReport_CUSTOM.json'
	exec sp_OAMethod @OLE, 'OpenTextFile', @FileId out, @path , 8 , 1 , -1;
	exec sp_OAMethod @FileId, 'WriteLine', Null, '[';

	
	insert into #syslabel (LabelType , LabelId , LanguageId , Label)
	select LabelType , LabelId , LanguageId , Label from SysLabelReport_CUSTOM order by LabelId , LanguageId

	select @maxrow = max(seq) from #syslabel
	while exists (select 'x' from #syslabel)
	begin
		
		select @seq = seq from #syslabel order by seq desc

		select @sqltext = case when seq = @maxrow 
				then '{"LabelType": "'+ LabelType +'", "LabelId": "'+LabelId+'", "LanguageId": "'+LanguageId+'", "Label": "'+Label+'"}'
				else '{"LabelType": "'+ LabelType +'", "LabelId": "'+LabelId+'", "LanguageId": "'+LanguageId+'", "Label": "'+Label+'"},'
			end
		from #syslabel
		where seq = @seq

		exec sp_OAMethod @FileId, 'WriteLine', null, @sqltext
	
		delete #syslabel where seq = @seq

	end

	exec sp_OAMethod @FileId, 'WriteLine', null, ']';
	exec sp_OAMethod @FileId, 'Close';
	exec sp_OADestroy @FileId out;
	exec sp_OADestroy @OLE out;

	drop table #syslabel
	set nocount off

end
