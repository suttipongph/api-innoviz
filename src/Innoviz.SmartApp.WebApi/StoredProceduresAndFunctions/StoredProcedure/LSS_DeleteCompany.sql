if exists (select 'x' from sys.objects o where object_id = OBJECT_ID(N'LSS_DeleteCompany', 'P')) 
begin 
	drop proc LSS_DeleteCompany
end
go
create proc LSS_DeleteCompany (
	@CompanyGUID varchar(36)
)
as
begin

	set nocount on

	begin tran
		
		declare	@seq int ,
				@sqltext varchar(max) = ''

		create table #temp (seq int identity primary key , sqltext varchar(max))

		begin try

			insert into #temp (sqltext)
			select	'alter table '+ tb.name +' nocheck constraint ' + fk.name 
			from	sys.foreign_keys fk
			inner join sys.tables tb
			on		fk.parent_object_id = OBJECT_ID(tb.name)
			-- where	ic.COLUMN_NAME = 'CompanyGUID' 

			insert into #temp (sqltext)
			select	'if exists (select ''x'' from ' + TABLE_NAME + ' where '+ COLUMN_NAME +' = ''' + @CompanyGUID + ''' ) 
					begin 
						delete ' + TABLE_NAME + ' where '+ COLUMN_NAME +' = ''' + @CompanyGUID + ''' 
					end'
			from	INFORMATION_SCHEMA.COLUMNS 
			where	COLUMN_NAME = 'CompanyGUID' 
			order by TABLE_NAME

			insert into #temp (sqltext)
			select	'alter table '+ tb.name +' check constraint ' + fk.name
			from	sys.foreign_keys fk
			inner join sys.tables tb
			on		fk.parent_object_id = OBJECT_ID(tb.name)
			-- where	ic.COLUMN_NAME = 'CompanyGUID' 

			while exists (select 'x' from #temp)
			begin
				select	@seq = seq ,
						@sqltext = sqltext 
				from	#temp
				order by seq desc

				exec(@sqltext)

				delete #temp where seq = @seq
			end
			
			commit

		end try
		begin catch
			
			select ERROR_LINE() as [error_line] , ERROR_MESSAGE() as [error_message]
			rollback
		
		end catch

		drop table #temp

		set nocount off

End