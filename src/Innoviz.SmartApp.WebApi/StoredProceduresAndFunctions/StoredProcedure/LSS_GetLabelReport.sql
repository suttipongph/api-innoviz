If Exists (Select 'x' From sys.objects o Where object_id = OBJECT_ID(N'LSS_GetLabelReport', 'P')) 
Begin 
	Drop Proc [LSS_GetLabelReport]
End
Go
-- =============================================
-- Author: Benchawan Kiatphibun
-- Create date: 2019-04-09
-- Description: GetLabelReport By Language and LabelString
-- =============================================
create proc [dbo].[LSS_GetLabelReport]
(
    --EX: EXEC [dbo].[LSS_GetLabelReport] @Language = 'EN', @LabelStr = 'CUSTOMER, [NAME], PRODUCT_TABLE, TEL, FAX, TAX_ID, APPLICATION_ID, REVISION, COST_AMOUNT, ADDITIONAL_COST, CAMPAIGN_TABLE, DOWN, BALLOON, PAYMENT_TYPE, FLAT, IRR, TERM, DEPOSIT, RV_AMOUNT, ADV_PAYM_AMOUNT, PAYMENT_FREQUENCY, AGREEMENT_AMOUNT, UNEARNED_INTEREST, [PERIOD], OUTSTANDING_PRINCIPAL, PRINCIPAL_AMOUNT, INTEREST, PAYMENT, TAX, PAYMENT_INC_TAX, [PRINT], [PAGE], REPURCHASE, CALCULATION_SHEET, OF_LABEL'
	@Language CHAR(2),
    @LabelStr VARCHAR(MAX)
)
as
begin

	 declare @sqltext nvarchar(max) = ''
	 select @Language = isnull(@Language,'EN')
	 select @sqltext = 'select * 
	 from (
		 select	LabelId , Label
		 from	SysLabelReport_CUSTOM cus
		 where	LanguageId	=	'''+@Language+'''
		 union all
		 select LabelId , Label
		 from	SysLabelReport std
		 where	LanguageId	=	'''+@Language+'''
		 and	not exists (select ''x'' from SysLabelReport_CUSTOM cus where cus.LabelType = std.LabelType and cus.LabelId = std.LabelId and cus.LanguageId = std.LanguageId) 	
	) as source
	pivot ( max(Label) for LabelId in ('+@LabelStr+')) as pivot_table'

	exec(@sqltext)
end