if exists (select * from dbo.sysobjects where lower(name) = 'fn_EnumDisplay'AND xtype IN (N'FN', N'IF', N'TF'))
	drop function fn_EnumDisplay
go
create function [dbo].[fn_EnumDisplay] (
	@enumType nvarchar(2000), 
	@enumValue int
)
returns nvarchar(2000)
as 
begin

	declare @strReturn nvarchar(2000) = '';

	select @strReturn = AttributeName FROM SysEnumTable
	where EnumName = @enumType AND AttributeValue = @enumValue

	return @strReturn;
end

