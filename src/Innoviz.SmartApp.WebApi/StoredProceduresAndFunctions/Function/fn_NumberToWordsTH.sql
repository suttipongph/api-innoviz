 --=============================================  
 --Author:  Benchawan Kiatphibun  
 --Create date: 2019-10-10  
 --Description: <แปลงค่าตัวเลขเป็นตัวหนังสือ(EN)>  
 --=============================================  
if exists (select * from dbo.sysobjects where lower(name) = 'fn_NumberToWordsTH' and xtype in (N'FN', N'IF', N'TF'))
	drop function fn_NumberToWordsTH
go
create function [dbo].[fn_NumberToWordsTH]   
( 
	@intNumberValue decimal(18,2),
	@suffix nvarchar(max) = null
)    
returns nvarchar(max)
as
begin
	
	declare @number bigint ,
			@numtext varchar(100) ,
			@text nvarchar(max) = ''

	declare @below10 TABLE (id int identity(0,1), word nvarchar(32))  
	declare @digit table (id int identity(1,1), word nvarchar(32))  

	insert @below10 (word) values  
	(N''), (N'หนึ่ง'),(N'สอง'), (N'สาม'),  
	(N'สี่'), (N'ห้า'), (N'หก'), (N'เจ็ด'),  
	(N'แปด'), (N'เก้า')

	insert @digit values 
	(N'ล้าน') , (N'สิบ') , (N'ร้อย'), (N'พัน'), (N'หมื่น'), (N'แสน')

	select @intNumberValue = case when @intNumberValue < 0 then @intNumberValue * -1 else @intNumberValue end
	select @number = @intNumberValue
	select @numtext = CAST(@number as varchar(100))

	if @number = 0
	begin
		select @text = N'ศูนย์'
	end

	;with splitnumber as (
		select	@numtext as outstandingtext , 
				SUBSTRING(@numtext,1,1) as number ,
				case when LEN(@numtext) % 6 = 0 then 6 else LEN(@numtext) % 6 end as digit ,
				'0' as lead ,
				1 as seq 
		union all
		select	SUBSTRING(outstandingtext,2,LEN(outstandingtext)) ,
				SUBSTRING(SUBSTRING(outstandingtext,2,LEN(outstandingtext)),1,1) as number ,
				case when LEN(SUBSTRING(outstandingtext,2,LEN(outstandingtext))) % 6 = 0 then 6 else LEN(SUBSTRING(outstandingtext,2,LEN(outstandingtext))) % 6 end as digit ,
				number as lead ,
				splitnumber.seq + 1 as seq 
		from	splitnumber
		where	splitnumber.seq < LEN(@numtext)
	) 
	select	@text = LTRIM(RTRIM(@text + 
			case when splitnumber.number = 1 and splitnumber.lead > 0 and splitnumber.digit = 1 then N'เอ็ด'
				 when splitnumber.number = 1 and splitnumber.digit = 2 then N''
				 when splitnumber.number = 2 and splitnumber.digit = 2 then N'ยี่'
				 else b10.word end +
			case when splitnumber.number = 0 and len(splitnumber.outstandingtext) = 7 then dg.word
				 when splitnumber.number = 0 then '' 
				 when splitnumber.seq = LEN(@numtext) then ''
				 else dg.word end
			))
	from	splitnumber
	inner join @below10 b10
	on		splitnumber.number = b10.id
	inner join @digit dg
	on		splitnumber.digit = dg.id	

	selectresult:
	if @suffix is null select @text = @text + N'บาท'
	
	if CONVERT(decimal(18,2),@number) = @intNumberValue
	begin
		if @suffix is null select @text = @text + N'ถ้วน'
	end
	else
	begin
		select @numtext = CONVERT(varchar(2),CONVERT(int,(@intNumberValue - CONVERT(decimal(18,2),@number)) * 100))
		if @suffix is not null select @text = @text + N'จุด'
		;with splitnumber as (
			select	@numtext as outstandingtext , 
					SUBSTRING(@numtext,1,1) as number ,
					case when LEN(@numtext) % 6 = 0 then 6 else LEN(@numtext) % 6 end as digit ,
					'0' as lead ,
					1 as seq 
			union all
			select	SUBSTRING(outstandingtext,2,LEN(outstandingtext)) ,
					SUBSTRING(SUBSTRING(outstandingtext,2,LEN(outstandingtext)),1,1) as number ,
					case when LEN(SUBSTRING(outstandingtext,2,LEN(outstandingtext))) % 6 = 0 then 6 else LEN(SUBSTRING(outstandingtext,2,LEN(outstandingtext))) % 6 end as digit ,
					number as lead ,
					splitnumber.seq + 1 as seq 
			from	splitnumber
			where	splitnumber.seq < LEN(@numtext)
		) 
		select	@text = LTRIM(RTRIM(@text + 
			case when splitnumber.number = 1 and splitnumber.lead > 0 and splitnumber.digit = 1 then N'เอ็ด'
			     when splitnumber.number = 1 and splitnumber.digit = 2 and @suffix is null then N''
				 when splitnumber.number = 2 and splitnumber.digit = 2 and @suffix is null then N'ยี่'
				 else b10.word end +
			case when splitnumber.number = 0 then '' 
				 when splitnumber.seq = LEN(@numtext) then ''
				 when splitnumber.digit = 2 and splitnumber.lead = 0 and @suffix is not null then N''
				 else dg.word end
			))
		from	splitnumber
		inner join @below10 b10
		on		splitnumber.number = b10.id
		inner join @digit dg
		on		splitnumber.digit = dg.id	
		-- select @text = @text + ' ' + REPLICATE('0',2-len(@numtext)) + @numtext +'/100 ' + N'สตางค์'
		if @suffix is null select @text = @text + N'สตางค์'
	end
	
	if @suffix is not null select @text = @text + @suffix

	return (@text)  
	-- select @text
end  
  
-- example
--go

--create table #temp(seq int identity primary key, value decimal(18,2))

--insert into #temp(value) select '784192.42'
--insert into #temp(value) select '1053820.65'
--insert into #temp(value) select '16653.3'
--insert into #temp(value) select '261113.78'
--insert into #temp(value) select '4259025.49'
--insert into #temp(value) select '935100'
--insert into #temp(value) select '31088.72'
--insert into #temp(value) select '27272.59'
--insert into #temp(value) select '8624.05'
--insert into #temp(value) select '1400000'
--insert into #temp(value) select '0'
--insert into #temp(value) select '15093.02'
--insert into #temp(value) select '109112.65'
--insert into #temp(value) select '296043.92'
--insert into #temp(value) select '29736.24'
--insert into #temp(value) select '135103.27'
--insert into #temp(value) select '1765085.54'
--insert into #temp(value) select '1282319.86'
--insert into #temp(value) select '475906.24'
--insert into #temp(value) select '306298.79'
--insert into #temp(value) select '15178011.41'
--insert into #temp(value) select '249334.65'
--insert into #temp(value) select '240000'
--insert into #temp(value) select '662786.93'
--insert into #temp(value) select '0'
--insert into #temp(value) select '4259025.49'
--insert into #temp(value) select '998576.78'
--insert into #temp(value) select '10000000'
--insert into #temp(value) select '11000000'
--insert into #temp(value) select '12000000'
--insert into #temp(value) select '101000000'

--select * , dbo.fn_NumberToWordsTH(value)
--from #temp

--drop table #temp