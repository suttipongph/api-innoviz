if exists (select * from dbo.sysobjects where lower(name) = 'fn_DateToWord'AND xtype IN (N'FN', N'IF', N'TF'))
	drop function fn_DateToWord
go
create function [dbo].[fn_DateToWord] (
	@date date ,
    @lang varchar(2) ,
    @format nvarchar(100)
)
returns nvarchar(2000)
as 
begin
   
declare @monthtext table (id int identity(1,1),en varchar(100),th nvarchar(100))
insert into @monthtext (en,th)
values ('January',N'มกราคม') ,
('February',N'กุมภาพันธ์') ,
('March',N'มีนาคม') ,
('April',N'เมษายน') ,
('May',N'พฤษภาคม') ,
('June',N'มิถุนายน')  ,
('July',N'กรกฎาคม')  ,
('August',N'สิงหาคม') ,
('September',N'กันยายน') ,
('October',N'ตุลาคม') ,
('November',N'พฤศจิกายน') ,
('December',N'ธันวาคม') 


select @format = REPLACE(@format,'DAY',CONVERT(nvarchar(2),DAY(@date)))
select @format = REPLACE(@format,'MONTH',(select case when @lang = 'EN' then en else th end from @monthtext where id = MONTH(@date)))
select @format = REPLACE(@format,'YEAR', CONVERT(nvarchar(4),case when @lang = 'EN' then YEAR(@date) else YEAR(@date) + 543 end))

return @format
end

