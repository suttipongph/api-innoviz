if exists (select * from dbo.sysobjects where lower(name) = 'fn_filter' and xtype in (N'FN', N'IF', N'TF'))
	drop function fn_filter
go
create function dbo.fn_filter (
	@key varchar(max) , 
	@text varchar(max)
	
)
returns @Value table (
	isfilter bit
)
as 
begin
	
	declare @selectall uniqueidentifier = '00000000-0000-0000-0000-000000000000',
			@newid uniqueidentifier = '00000000-0000-0000-0000-000001000000'
			
	
		
	if exists (
		select	*
		from	fn_Split(@text,',') sp
		where	isnull(@key,case when @text = @selectall then @newid end) = isnull(sp.value,isnull(@key,@newid))
	)
	begin
		insert into @Value 
		select 1
	end
	return 
		
end