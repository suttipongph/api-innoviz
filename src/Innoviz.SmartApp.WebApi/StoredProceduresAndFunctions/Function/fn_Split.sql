if exists (select * from dbo.sysobjects where lower(name) = 'fn_Split' and xtype in (N'FN', N'IF', N'TF'))
	drop function fn_Split
go
create function dbo.fn_Split (
	@text varchar(max) , 
	@delimiter varchar(20) = ''
)
returns @Value table (
	seq int identity primary key , 
	value varchar(max)
)
as 
begin

	if @text is not null and @text != '00000000-0000-0000-0000-000000000000'
	begin
		declare @index int
		select @index = -1
  
		while (LEN(@text) > 0) 
		begin
 
			select @index = CHARINDEX(@delimiter,@text)

			if (@index = 0) and (LEN(@text) > 0) 
			begin  
				insert into @Value values (@text)
				break 
			end 

			if (@index > 1) 
			begin
				insert into @Value values (LEFT(@text, @index - 1))
				select @text = RIGHT(@text, (LEN(@text) - @index))
			end
			else 
			begin
				select @text = RIGHT(@text, (LEN(@text) - @index))
			end

		End
	end
	else
	begin
		insert into @Value values (NULL)
	end

	return

end