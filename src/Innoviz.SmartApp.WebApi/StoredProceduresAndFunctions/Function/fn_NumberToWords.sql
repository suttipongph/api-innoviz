-- =============================================  
-- Author:  Benchawan Kiatphibun  
-- Create date: 2019-10-10  
-- Description: <แปลงค่าตัวเลขเป็นตัวหนังสือ(EN)>  
-- =============================================  
if exists (select * from dbo.sysobjects where lower(name) = 'fn_NumberToWords' and xtype in (N'FN', N'IF', N'TF'))
	drop function fn_NumberToWords
go
create function [dbo].[fn_NumberToWords]   
( 
	@intNumberValue decimal(18,2) 
)    
returns nvarchar(max)
as
begin

	declare @number bigint ,
			@numtext varchar(100) ,
			@text varchar(max) = ''

	declare @below20 TABLE (id int identity(0,1), word varchar(32))  
	declare @below100 table (id int identity(2,1), word varchar(32))  

	insert @below20 (word) values  
	('Zero'), ('One'),('Two'), ('Three'),  
	('Four'), ('Five'), ('Six'), ('Seven'),  
	('Eight'), ('Nine'), ('Ten'), ('Eleven'),  
	('Twelve'), ('Thirteen'), ('Fourteen'),  
	('Fifteen'), ('Sixteen'), ('Seventeen'),  
	('Eighteen'), ('Nineteen')  
	insert @below100 values 
	('Twenty'), ('Thirty'),('Forty'), ('Fifty'),  
	('Sixty'), ('Seventy'), ('Eighty'), ('Ninety')  

	select @intNumberValue = case when @intNumberValue < 0 then @intNumberValue * -1 else @intNumberValue end
	select @number = @intNumberValue
	select @numtext = CAST(@number as varchar(100))

	if @intNumberValue between 0 and 20
	begin
		select @text = word from @below20 where id = @number
		goto selectresult
	end
	;with splitnumber as (
		select	@numtext as outstandingtext , 
				SUBSTRING(@numtext,1,1) as number ,
				SUBSTRING(@numtext,1,2) as number20 ,
				LEN(@numtext) as lenght ,
				case when LEN(@numtext) % 3 = 2 and CONVERT(int,SUBSTRING(@numtext,1,2)) between 1 and 19 then 1 else 0 end as is20 ,
				case when LEN(@numtext) % 3 = 0 then 1 else 0 end as is100 ,
				1 as isneed ,
				LEN(@numtext) % 3 as [mod] ,
				1 as seq 
		union all
		select	SUBSTRING(outstandingtext,2,LEN(outstandingtext)) ,
				SUBSTRING(SUBSTRING(outstandingtext,2,LEN(outstandingtext)),1,1) as number ,
				SUBSTRING(SUBSTRING(outstandingtext,2,LEN(outstandingtext)),1,2) as number20 ,
				LEN(SUBSTRING(outstandingtext,2,LEN(outstandingtext))) as lenght ,
				case when LEN(SUBSTRING(outstandingtext,2,LEN(outstandingtext))) % 3 = 2 and CONVERT(int,SUBSTRING(SUBSTRING(outstandingtext,2,LEN(outstandingtext)),1,2)) between 1 and 19 then 1 else 0 end as is20 ,
				case when LEN(SUBSTRING(outstandingtext,2,LEN(outstandingtext))) % 3 = 0 then 1 else 0 end as is100 ,
				case when is20 = 1 then 0 else 1 end as isneed ,
				LEN(SUBSTRING(outstandingtext,2,LEN(outstandingtext))) % 3 as [mod] ,
				splitnumber.seq + 1 as seq 
		from	splitnumber
		where	splitnumber.seq < LEN(@numtext)
	) 
	select	@text = ltrim(rtrim(@text + ' ' +
			case when splitnumber.number = 0 and splitnumber.[is20] = 0 then '' 
				 when splitnumber.[is20] = 1 then b202.[word]
				 when splitnumber.[mod] = 0 then b20.word + ' ' + 'Hundred'
				 when splitnumber.[mod] = 2 then case when splitnumber.number = 0 then b202.word else b100.word end 
				 when splitnumber.[isneed] = 0 then ''
				 else b20.word end) + rtrim(
			case when splitnumber.[mod] = 1 then ' ' +
					case when splitnumber.lenght = 10 then 'Billion'
						 when splitnumber.lenght = 7 then 'Million'
						 when splitnumber.lenght = 4 then 'Thousand'
						 else '' end
				 else '' end ))
	from	splitnumber
	inner join @below20 b20
	on		splitnumber.number = b20.id
	left join @below100 b100
	on		splitnumber.number = b100.id
	left join @below20 b202
	on		splitnumber.number20 = b202.id

	
	selectresult:
	select @numtext = CONVERT(varchar(2),CONVERT(int,(@intNumberValue - CONVERT(decimal(18,2),@number)) * 100))
	select @text = @text + ' and ' + REPLICATE('0',2-len(@numtext)) + @numtext +'/100'

	return (@text)  
end  
  
-- example
go

--create table #temp(seq int identity primary key, value decimal(18,2))

--insert into #temp(value) select '784192.42'
--insert into #temp(value) select '1053820.65'
--insert into #temp(value) select '16653.3'
--insert into #temp(value) select '261113.78'
--insert into #temp(value) select '4259025.49'
--insert into #temp(value) select '935100'
--insert into #temp(value) select '31088.72'
--insert into #temp(value) select '27272.59'
--insert into #temp(value) select '8624.05'
--insert into #temp(value) select '1400000'
--insert into #temp(value) select '0'
--insert into #temp(value) select '15093.02'
--insert into #temp(value) select '109112.65'
--insert into #temp(value) select '296043.92'
--insert into #temp(value) select '29736.24'
--insert into #temp(value) select '135103.27'
--insert into #temp(value) select '1765085.54'
--insert into #temp(value) select '1282319.86'
--insert into #temp(value) select '475906.24'
--insert into #temp(value) select '306298.79'
--insert into #temp(value) select '15178011.41'
--insert into #temp(value) select '249334.65'
--insert into #temp(value) select '240000'
--insert into #temp(value) select '662786.93'
--insert into #temp(value) select '0'
--insert into #temp(value) select '4259025.49'
--insert into #temp(value) select '998576.78'

--select * , dbo.fn_NumberToWords(value)
--from #temp

--drop table #temp