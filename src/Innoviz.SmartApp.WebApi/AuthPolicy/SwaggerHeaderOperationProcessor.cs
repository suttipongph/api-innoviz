﻿using NSwag.Generation.Processors;
using NSwag.Generation.Processors.Contexts;

namespace Innoviz.SmartApp.WebApi.AuthPolicy
{
    public class SwaggerHeaderOperationProcessor : IOperationProcessor
    {
        public bool Process(OperationProcessorContext context)
        {
            // company header
            context.OperationDescription.Operation.Parameters.Add(
            new NSwag.OpenApiParameter
            {
                Name = "CompanyHeader",
                Kind = NSwag.OpenApiParameterKind.Header,
                Type = NJsonSchema.JsonObjectType.String,
                IsRequired = false,
                Description = "Company key",
                //Default = "0a099e6f-9ba8-4a06-9e7e-5cc28123a90e"
            });

            // branch header
            context.OperationDescription.Operation.Parameters.Add(
            new NSwag.OpenApiParameter
            {
                Name = "BranchHeader",
                Kind = NSwag.OpenApiParameterKind.Header,
                Type = NJsonSchema.JsonObjectType.String,
                IsRequired = false,
                Description = "Branch key",
                //Default = "71aa8740-9dc4-45c7-9a69-63f6794bd9df"
            });

            return true;
        }
    }
}
