﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.AuthPolicy
{
    public class CustomRolesRequirementHandler : AuthorizationHandler<CustomRolesRequirement> {
        private readonly SmartAppContext _dbcontext;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CustomRolesRequirementHandler(SmartAppContext dbcontext,
                            IHttpContextAccessor httpContextAccessor,
                            IConfiguration configuration) {
            _dbcontext = dbcontext;
            _httpContextAccessor = httpContextAccessor;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, CustomRolesRequirement requirement) {


            // check authenticated identity 
            if (!IsAuthenticated(context))
            {
                // 401
                context.Fail();
                return Task.CompletedTask;
            }
            
            string userId = GetUserId(context);
            string username = GetUserName(context);
            string companyHeader = GetCompanyFromHeader(context);
            string controllerName = GetControllerName(context);
            string routeAttr = GetRoutePath(context);
            string httpMethod = GetHttpMethod(context);

            ISysAccessRightService accessRightService = new SysAccessRightService(_dbcontext);
            // case: clientId with user
            if (userId != null)
            {
                ISysUserService userService = new SysUserService(_dbcontext);
                string site = GetSiteLoginFromHeader(context);
                #region LIT
                var checkAccessLevel = accessRightService.CanUserAccessController(userId, site, companyHeader,
                                                               controllerName, routeAttr, httpMethod);
                //var checkAccessLevel = new CheckAccessLevelResult { AccessLevel = 4, CanAccessController = true };
                bool canUserAccessController = checkAccessLevel.CanAccessController;
                #endregion LIT

                // check for case first user (no existing user in SysUserTable)
                if (!userService.HasAnyUser() && canUserAccessController)
                {
                    context.Succeed(requirement);
                    return Task.CompletedTask;
                }
                // check Inactive user
                SysUserTableParm parm = new SysUserTableParm { Id = userId, UserName = username };
                if (userService.CheckExistingUser(parm))
                {
                    if(userService.IsInactiveUser(userId))
                    {
                        // 401
                        context.Fail();
                        return Task.CompletedTask;
                    }
                }

                if (!canUserAccessController)
                {
                    // 401
                    context.Fail();
                    return Task.CompletedTask;
                }

                SetAccessLevel(context, checkAccessLevel.AccessLevel, checkAccessLevel.IsCreateCompany);
                // case get list
                if(routeAttr.ToUpper().Contains("GET") && routeAttr.ToUpper().Contains("LIST"))
                {
                    var deleteRouteAttr = routeAttr.Replace("/Get", "/Delete");
                    SetDeleteAccessLevel(context, accessRightService,
                                        userId, site, companyHeader,
                                        controllerName, deleteRouteAttr, "POST");
                }
            }
            // case: non-user clientId
            else
            {
                string clientId = GetClientId(context);
                IEnumerable<Claim> scopeClaims = GetScope(context);
                IEnumerable<string> scopes = (scopeClaims != null) ? scopeClaims.Select(sc => sc.Value) : null;
                var checkAccessLevel =
                    accessRightService.CanClientScopeAccessController(clientId, scopes, companyHeader,
                                                                      controllerName, routeAttr, httpMethod);
                bool canClientScopeAccessController = checkAccessLevel.CanAccessController;
                if (!canClientScopeAccessController)
                {
                    // 401
                    context.Fail();
                    return Task.CompletedTask;
                }
                SetAccessLevel(context, checkAccessLevel.AccessLevel);
            }

            // authorized
            context.Succeed(requirement);

            return Task.CompletedTask;
        }

        /* Helper Methods */
        private string GetUserId(AuthorizationHandlerContext context) {
            var user = context.User;
            
            var userId = user.Claims.Where(item => item.Type == "usubj").FirstOrDefault();
            if(userId != null) {
                return userId.Value;
            }
            else {
                return null;
            }
        }
        private string GetClientId(AuthorizationHandlerContext context) {
            var user = context.User;
            var clientId = user.Claims.Where(item => item.Type == "client_id").FirstOrDefault();
            return clientId == null ? null : clientId.Value;
        }
        private string GetUserName(AuthorizationHandlerContext context) {
            var user = context.User;
            var userName = user.Claims.Where(item => item.Type == "username").FirstOrDefault();
            if (userName != null) {
                return userName.Value;
            } else {
                return null;
            }
        }
        private bool IsAuthenticated(AuthorizationHandlerContext context) {
            return context.User.Identity.IsAuthenticated;
        }

        private string GetControllerName(AuthorizationHandlerContext context)
        {
            var actiondescriptor = GetActionDescriptor(context);

            var controllerName = actiondescriptor.ControllerName;
            return controllerName;
        }
        private string GetHttpMethod(AuthorizationHandlerContext context)
        {
            var actionDescriptor = GetActionDescriptor(context);

            var httpMethod = (HttpMethodMetadata)actionDescriptor.EndpointMetadata.Where(item => item.GetType() == typeof(HttpMethodMetadata)).FirstOrDefault();

            return httpMethod != null ? httpMethod.HttpMethods.FirstOrDefault() : null;
        }
        private string GetRouteDataValue(AuthorizationHandlerContext context, string key)
        {
            var resource = (AuthorizationFilterContext)context.Resource;
            var result = resource.RouteData.Values[key];
            return result != null ? result.ToString() : null;
        }
        private string GetRoutePath(AuthorizationHandlerContext context)
        {
            var actiondescriptor = GetActionDescriptor(context);
            var controllerName = GetControllerName(context);
            var routeTemplate = actiondescriptor.AttributeRouteInfo.Template.Split("/" + controllerName + "/", 2);

            return routeTemplate.Last();
        }

        private ControllerActionDescriptor GetActionDescriptor(AuthorizationHandlerContext context)
        {
            if(context.Resource is Endpoint endpoint)
            {
                return endpoint.Metadata.GetMetadata<ControllerActionDescriptor>();
            }
            else
            {
                var resource = (AuthorizationFilterContext)context.Resource;
                var actionDescriptor = (ControllerActionDescriptor)resource.ActionDescriptor;
                return actionDescriptor;
            }
            
        }
        private IEnumerable<Claim> GetScope(AuthorizationHandlerContext context)
        {
            var user = context.User;
            var scope = user.Claims.Where(item => item.Type == "scope");
            return scope == null ? null : scope;
        }
        
        private string GetSiteLoginFromHeader(AuthorizationHandlerContext context)
        {
            return _httpContextAccessor.HttpContext.Request.Headers["SiteHeader"];
        }
        private string GetCompanyFromHeader(AuthorizationHandlerContext context)
        {
            string fromHeader = _httpContextAccessor.HttpContext.Request.Headers["CompanyHeader"];
            if(fromHeader == null)
            {
                string fromQuery = _httpContextAccessor.HttpContext.Request.Query.Where(w => w.Key.ToUpper() == TextConstants.CompanyGUID.ToUpper()).FirstOrDefault().Value;
                if(fromQuery != null)
                {
                    return fromQuery;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return fromHeader;
            }
            
        }
        private void SetAccessLevel(AuthorizationHandlerContext context, int accessLevel, bool isCreateCompany = false)
        {
            _httpContextAccessor.HttpContext.Items["AccessLevel"] = accessLevel;
            _httpContextAccessor.HttpContext.Items["IsCreateCompany"] = isCreateCompany;
        }
        private void SetDeleteAccessLevel(AuthorizationHandlerContext context, ISysAccessRightService accessRightService,
                                        string userId, string site, string companyHeader, 
                                        string controllerName, string routeAttr, string httpMethod)
        {
            var deleteAccessLevel =
                accessRightService.CanUserAccessController(userId, site, companyHeader,
                                                            controllerName, routeAttr, httpMethod).AccessLevel;
            //var deleteAccessLevel = 4;
            _httpContextAccessor.HttpContext.Items["DeleteAccessLevel"] = deleteAccessLevel;
        }
    }
}
