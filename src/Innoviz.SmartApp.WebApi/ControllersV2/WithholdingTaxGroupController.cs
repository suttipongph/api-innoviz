﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class WithholdingTaxGroupController : BaseControllerV2
    {
        public WithholdingTaxGroupController(SmartAppContext context, ISysTransactionLogService transactionLogService)
           : base(context, transactionLogService)
        {
        }
        [HttpPost]
        [Route("GetWithholdingTaxGroupList/ByCompany")]
        public ActionResult GetCustomerTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetWithholdingTaxGroupListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetWithholdingTaxGroupById/id={id}")]
        public ActionResult GetWithholdingTaxGroupById(string id)
        {
            try
            {
                IWithholdingTaxGroupService withholdingTaxGroupService = new WithholdingTaxGroupService(db);
                return Ok(withholdingTaxGroupService.GetWithholdingTaxGroupById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateWithholdingTaxGroup")]
        public ActionResult CreateWithholdingTaxGroup([FromBody] WithholdingTaxGroupItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IWithholdingTaxGroupService withholdingTaxGroupService = new WithholdingTaxGroupService(db, SysTransactionLogService);
                    return Ok(withholdingTaxGroupService.CreateWithholdingTaxGroup(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateWithholdingTaxGroup")]
        public ActionResult UpdateWithholdingTaxGroup([FromBody] WithholdingTaxGroupItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IWithholdingTaxGroupService withholdingTaxGroupService = new WithholdingTaxGroupService(db, SysTransactionLogService);
                    return Ok(withholdingTaxGroupService.UpdateWithholdingTaxGroup(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteWithholdingTaxGroup")]
        public ActionResult DeleteWithholdingTaxGroup([FromBody] RowIdentity parm)
        {
            try
            {
                IWithholdingTaxGroupService withholdingTaxGroupService = new WithholdingTaxGroupService(db, SysTransactionLogService);
                return Ok(withholdingTaxGroupService.DeleteWithholdingTaxGroup(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        #endregion
    }
}
