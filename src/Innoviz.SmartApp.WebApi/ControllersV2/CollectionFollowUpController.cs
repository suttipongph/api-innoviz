﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class CollectionFollowUpController : BaseControllerV2
	{

		public CollectionFollowUpController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

        }
        [HttpPost]
        [Route("GetCollectionFollowUpList/ByCompany")]
        public ActionResult GetCollectionFollowUpListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCollectionFollowUpListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetCollectionFollowUpById/id={id}")]
        public ActionResult GetCollectionFollowUpById(string id)
        {
            try
            {
                ICollectionFollowUpService collectionFollowUpService = new CollectionFollowUpService(db);
                return Ok(collectionFollowUpService.GetCollectionFollowUpById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateCollectionFollowUp")]
        public ActionResult CreateCollectionFollowUp([FromBody] CollectionFollowUpItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICollectionFollowUpService collectionFollowUpService = new CollectionFollowUpService(db, SysTransactionLogService);
                    return Ok(collectionFollowUpService.CreateCollectionFollowUp(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateCollectionFollowUp")]
        public ActionResult UpdateCollectionFollowUp([FromBody] CollectionFollowUpItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICollectionFollowUpService collectionFollowUpService = new CollectionFollowUpService(db, SysTransactionLogService);
                    return Ok(collectionFollowUpService.UpdateCollectionFollowUp(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteCollectionFollowUp")]
        public ActionResult DeleteCollectionFollowUp([FromBody] RowIdentity parm)
        {
            try
            {
                ICollectionFollowUpService collectionFollowUpService = new CollectionFollowUpService(db, SysTransactionLogService);
                return Ok(collectionFollowUpService.DeleteCollectionFollowUp(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetCollectionFollowUpInitialData/id={id}")]
        public ActionResult GetCollectionFollowUpInitialData(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                return Ok(purchaseTableService.GetCollectionFollowUpInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDownByCollectionFollowUp([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDownByCollectionFollowUp([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDownByCollectionFollowUp([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("GetBankGroupDropDown")]
        public ActionResult GetBankGroupDropDownByCollectionFollowUp([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        #region memo
        [HttpPost]
        [Route("RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult GetMemoTransList([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                var result = sysListViewService.GetMemoTransListvw(search);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpGet]
        [Route("RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetMemoTransById(string id)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                return Ok(memoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [Route("RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult DeleteMemo([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialData(string id)
        {
            try
            {
                ICollectionFollowUpService collectionFollowUpService = new CollectionFollowUpService(db);
                return Ok(collectionFollowUpService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region BuyerReceiptTable
        [HttpPost]
        [Route("RelatedInfo/buyerreceipttable/GetBuyerReceiptTableList/ByCompany")]
        public ActionResult GetBuyerReceiptTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBuyerRecepiptTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/Pdc/GetChequeTableById/id={id}")]
        public ActionResult GetChequeTableById(string id)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetChequeTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/buyerreceipttable/GetBuyerReceiptTableById/id={id}")]
        public ActionResult GetBuyerReceiptTableById(string id)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db);
                return Ok(buyerReceiptTableService.GetBuyerReceiptTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Pdc/CreateChequeTable")]
        public ActionResult CreateChequeTable([FromBody] ChequeTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IChequeTableService chequeTableService = new ChequeTableService(db, SysTransactionLogService);
                    return Ok(chequeTableService.CreateChequeTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/buyerreceipttable/CreateBuyerReceiptTable")]
        public ActionResult CreateBuyerReceiptTable([FromBody] BuyerReceiptTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db, SysTransactionLogService);
                    return Ok(buyerReceiptTableService.CreateBuyerReceiptTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/Pdc/UpdateChequeTable")]
        public ActionResult UpdateChequeTable([FromBody] ChequeTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IChequeTableService chequeTableService = new ChequeTableService(db, SysTransactionLogService);
                    return Ok(chequeTableService.UpdateChequeTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/buyerreceipttable/UpdateBuyerReceiptTable")]
        public ActionResult UpdateBuyerReceiptTable([FromBody] BuyerReceiptTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db, SysTransactionLogService);
                    return Ok(buyerReceiptTableService.UpdateBuyerReceiptTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/Pdc/DeleteChequeTable")]
        public ActionResult DeleteChequeTable([FromBody] RowIdentity parm)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db, SysTransactionLogService);
                return Ok(chequeTableService.DeleteChequeTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/buyerreceipttable/ Deletebuyerreceipttable")]
        public ActionResult DeleteBuyerReceiptTable([FromBody] RowIdentity parm)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db, SysTransactionLogService);
                return Ok(buyerReceiptTableService.DeleteBuyerReceiptTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/Pdc/GetRefPDCDropDown")]

        public ActionResult GetRefPDCDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetRefPDCDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("RelatedInfo/Pdc/GetChequeTableInitialData/id={id}")]
        public ActionResult GetChequeTableInitialData(string id)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                var chequeTableServiceInitaildata = chequeTableService.GetChequeTableInitialDataByPurchase(id);
                return Ok(chequeTableServiceInitaildata);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/buyerreceipttable/GetBuyerReceiptInitialPurchaseLineData/id={id}")]
        public ActionResult GetBuyerReceiptInitialPurchaseLineData(string id)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTable = new BuyerReceiptTableService(db, SysTransactionLogService);
                return Ok(buyerReceiptTable.GetBuyerReceiptInitialPurchaseLineData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/Pdc/GetBankGroupDropDown")]
        public ActionResult GetBankGroupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region dropdown
        [HttpPost]
        [Route("RelatedInfo/buyerreceipttable/GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDownByBuyerReceiptTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Pdc/GetDocumentStatusDropDown")]

        public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDocumentStatusDocumentProcessDropdown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/GetDocumentStatusDropDown")]

        public ActionResult GetDocumentStatusDropDownMessengerRequest([FromBody] SearchParameter search)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.GetDocumentStatusDocumentProcessDropdown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region function
        #region cancel buyer receipt table
        [HttpGet]
        [Route("RelatedInfo/buyerreceipttable/Function/CancelBuyerReceiptTable/GetCancelBuyerReceiptTableById/id={id}")]
        public ActionResult GetCancelBuyerReceiptById(string id)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db);
                return Ok(buyerReceiptTableService.GetCancelBuyerReceiptTableById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/buyerreceipttable/Function/CancelBuyerReceiptTable/CancelBuyerReceiptTable")]
        public ActionResult CancelWithdrawal([FromBody] CancelBuyerReceiptTableView view)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db);
                return Ok(buyerReceiptTableService.CancelBuyerReceiptTable(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        #endregion

        #endregion
        #region NumberSeq
        [HttpGet]
        [Route("RelatedInfo/buyerreceipttable/ValidateIsManualNumberSeq/companyId={companyId}")]
        public ActionResult ValidateIsManualNumberSeq(string companyId)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTable = new BuyerReceiptTableService(db);
                return Ok(buyerReceiptTable.IsManualBuyerReceiptTable(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Attachment
        [HttpPost]
        [Route("RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentById(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Attachment/CreateAttachment")]
        public ActionResult CreateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Attachment/DeleteAttachment")]
        public ActionResult DeleteAttachment([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefId([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Attachment
        #region report
        [HttpGet]
        [Route("RelatedInfo/buyerreceipttable/Report/PrintBuyerReceipt/GetPrintBuyerReceiptById/id={id}")]
        public ActionResult GetPrintBuyerReceiptById(string id)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db);
                return Ok(buyerReceiptTableService.GetPrintBuyerReceiptById(id));

            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/buyerreceipttable/Report/PrintBuyerReceipt/RenderReport")]
        public ActionResult PrintBuyerReceipt([FromBody] PrintBuyerReceiptReportView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
