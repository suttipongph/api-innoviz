﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class ConsortiumTableController : BaseControllerV2
	{

		public ConsortiumTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetConsortiumTableList/ByCompany")]
		public ActionResult GetConsortiumTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetConsortiumTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetConsortiumTableById/id={id}")]
		public ActionResult GetConsortiumTableById(string id)
		{
			try
			{
				IConsortiumTableService interestTypeService = new ConsortiumTableService(db);
				return Ok(interestTypeService.GetConsortiumTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateConsortiumTable")]
		public ActionResult CreateConsortiumTable([FromBody] ConsortiumTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IConsortiumTableService interestTypeService = new ConsortiumTableService(db, SysTransactionLogService);
					return Ok(interestTypeService.CreateConsortiumTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateConsortiumTable")]
		public ActionResult UpdateConsortiumTable([FromBody] ConsortiumTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IConsortiumTableService interestTypeService = new ConsortiumTableService(db, SysTransactionLogService);
					return Ok(interestTypeService.UpdateConsortiumTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteConsortiumTable")]
		public ActionResult DeleteConsortiumTable([FromBody] RowIdentity parm)
		{
			try
			{
				IConsortiumTableService interestTypeService = new ConsortiumTableService(db, SysTransactionLogService);
				return Ok(interestTypeService.DeleteConsortiumTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetConsortiumLineList/ByCompany")]
		public ActionResult GetConsortiumLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetConsortiumLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetConsortiumLineById/id={id}")]
		public ActionResult GetConsortiumLineById(string id)
		{
			try
			{
				IConsortiumTableService interestTypeService = new ConsortiumTableService(db);
				return Ok(interestTypeService.GetConsortiumLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateConsortiumLine")]
		public ActionResult CreateConsortiumLine([FromBody] ConsortiumLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IConsortiumTableService interestTypeService = new ConsortiumTableService(db, SysTransactionLogService);
					return Ok(interestTypeService.CreateConsortiumLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateConsortiumLine")]
		public ActionResult UpdateConsortiumLine([FromBody] ConsortiumLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IConsortiumTableService interestTypeService = new ConsortiumTableService(db, SysTransactionLogService);
					return Ok(interestTypeService.UpdateConsortiumLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteConsortiumLine")]
		public ActionResult DeleteConsortiumLine([FromBody] RowIdentity parm)
		{
			try
			{
				IConsortiumTableService interestTypeService = new ConsortiumTableService(db, SysTransactionLogService);
				return Ok(interestTypeService.DeleteConsortiumLine(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("ValidateIsManualNumberSeq/companyId={companyId}")]
		public ActionResult ValidateIsManualNumberSeq(string companyId)
		{
			try
			{
				IConsortiumTableService consortiumTableService = new ConsortiumTableService(db);
				return Ok(consortiumTableService.IsManualByConsortium(companyId));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("GetConsortiumLineInitialData/id={id}")]
		public ActionResult GetConsortiumLineInitialData(string id)
		{
			try
			{
				IConsortiumTableService consortiumTableService = new ConsortiumTableService(db, SysTransactionLogService);
				return Ok(consortiumTableService.GetConsortiumLineInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown
		[HttpPost]
		[Route("GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IConsortiumTableService consortiumTableService = new ConsortiumTableService(db);
				return Ok(consortiumTableService.GetDropDownItemConsortiumTableStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetAuthorizedPersonTypeDropDown")]
		public ActionResult GetAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
