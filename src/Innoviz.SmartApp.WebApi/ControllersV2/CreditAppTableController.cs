using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.InterfaceWithOtherSystems;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;
namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public partial class CreditAppTableController : BaseControllerV2
    {
        private readonly InterfaceAXDbContext axContext;
        public CreditAppTableController(SmartAppContext context, ISysTransactionLogService transactionLogService, InterfaceAXDbContext axContext)
            : base(context, transactionLogService)
        {
            this.axContext = axContext;
        }
        [HttpPost]
        [Route("Factoring/GetCreditAppLineList/ByCompany")]
        [Route("Bond/GetCreditAppLineList/ByCompany")]
        [Route("HirePurchase/GetCreditAppLineList/ByCompany")]
        [Route("LCDLC/GetCreditAppLineList/ByCompany")]
        [Route("Leasing/GetCreditAppLineList/ByCompany")]
        [Route("ProjectFinance/GetCreditAppLineList/ByCompany")]
        public ActionResult GetCreditAppLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/GetCreditAppLineById/id={id}")]
        [Route("Bond/GetCreditAppLineById/id={id}")]
        [Route("HirePurchase/GetCreditAppLineById/id={id}")]
        [Route("LCDLC/GetCreditAppLineById/id={id}")]
        [Route("Leasing/GetCreditAppLineById/id={id}")]
        [Route("ProjectFinance/GetCreditAppLineById/id={id}")]
        public ActionResult GetCreditAppLineById(string id)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetCreditAppLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreateCreditAppLine")]
        [Route("Bond/CreateCreditAppLine")]
        [Route("HirePurchase/CreateCreditAppLine")]
        [Route("LCDLC/CreateCreditAppLine")]
        [Route("Leasing/CreateCreditAppLine")]
        [Route("ProjectFinance/CreateCreditAppLine")]
        public ActionResult CreateCreditAppLine([FromBody] CreditAppLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                    return Ok(creditAppTableService.CreateCreditAppLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/UpdateCreditAppLine")]
        [Route("Bond/UpdateCreditAppLine")]
        [Route("HirePurchase/UpdateCreditAppLine")]
        [Route("LCDLC/UpdateCreditAppLine")]
        [Route("Leasing/UpdateCreditAppLine")]
        [Route("ProjectFinance/UpdateCreditAppLine")]
        public ActionResult UpdateCreditAppLine([FromBody] CreditAppLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                    return Ok(creditAppTableService.UpdateCreditAppLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/DeleteCreditAppLine")]
        [Route("Bond/DeleteCreditAppLine")]
        [Route("HirePurchase/DeleteCreditAppLine")]
        [Route("LCDLC/DeleteCreditAppLine")]
        [Route("Leasing/DeleteCreditAppLine")]
        [Route("ProjectFinance/DeleteCreditAppLine")]
        public ActionResult DeleteCreditAppLine([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                return Ok(creditAppTableService.DeleteCreditAppLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCreditAppTableList/ByCompany")]
        [Route("Bond/GetCreditAppTableList/ByCompany")]
        [Route("HirePurchase/GetCreditAppTableList/ByCompany")]
        [Route("LCDLC/GetCreditAppTableList/ByCompany")]
        [Route("Leasing/GetCreditAppTableList/ByCompany")]
        [Route("ProjectFinance/GetCreditAppTableList/ByCompany")]
        public ActionResult GetCreditAppTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/GetCreditAppTableById/id={id}")]
        [Route("Bond/GetCreditAppTableById/id={id}")]
        [Route("HirePurchase/GetCreditAppTableById/id={id}")]
        [Route("LCDLC/GetCreditAppTableById/id={id}")]
        [Route("Leasing/GetCreditAppTableById/id={id}")]
        [Route("ProjectFinance/GetCreditAppTableById/id={id}")]
        public ActionResult GetCreditAppTableById(string id)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetCreditAppTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreateCreditAppTable")]
        [Route("Bond/CreateCreditAppTable")]
        [Route("HirePurchase/CreateCreditAppTable")]
        [Route("LCDLC/CreateCreditAppTable")]
        [Route("Leasing/CreateCreditAppTable")]
        [Route("ProjectFinance/CreateCreditAppTable")]
        public ActionResult CreateCreditAppTable([FromBody] CreditAppTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                    return Ok(creditAppTableService.CreateCreditAppTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/UpdateCreditAppTable")]
        [Route("Bond/UpdateCreditAppTable")]
        [Route("HirePurchase/UpdateCreditAppTable")]
        [Route("LCDLC/UpdateCreditAppTable")]
        [Route("Leasing/UpdateCreditAppTable")]
        [Route("ProjectFinance/UpdateCreditAppTable")]
        public ActionResult UpdateCreditAppTable([FromBody] CreditAppTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                    return Ok(creditAppTableService.UpdateCreditAppTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/DeleteCreditAppTable")]
        [Route("Bond/DeleteCreditAppTable")]
        [Route("HirePurchase/DeleteCreditAppTable")]
        [Route("LCDLC/DeleteCreditAppTable")]
        [Route("Leasing/DeleteCreditAppTable")]
        [Route("ProjectFinance/DeleteCreditAppTable")]
        public ActionResult DeleteCreditAppTable([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                return Ok(creditAppTableService.DeleteCreditAppTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/GetAddressTransDropDown")]
        [Route("Bond/GetAddressTransDropDown")]
        [Route("HirePurchase/GetAddressTransDropDown")]
        [Route("LCDLC/GetAddressTransDropDown")]
        [Route("Leasing/GetAddressTransDropDown")]
        [Route("ProjectFinance/GetAddressTransDropDown")]
        public ActionResult GetAddressTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAddressTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetApplicationTableDropDown")]
        [Route("Bond/GetApplicationTableDropDown")]
        [Route("HirePurchase/GetApplicationTableDropDown")]
        [Route("LCDLC/GetApplicationTableDropDown")]
        [Route("Leasing/GetApplicationTableDropDown")]
        [Route("ProjectFinance/GetApplicationTableDropDown")]
        public ActionResult GetApplicationTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemApplicationTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetAssignmentAgreementTableDropDown")]
        [Route("Bond/GetAssignmentAgreementTableDropDown")]
        [Route("HirePurchase/GetAssignmentAgreementTableDropDown")]
        [Route("LCDLC/GetAssignmentAgreementTableDropDown")]
        [Route("Leasing/GetAssignmentAgreementTableDropDown")]
        [Route("ProjectFinance/GetAssignmentAgreementTableDropDown")]
        public ActionResult GetAssignmentAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetAssignmentMethodDropDown")]
        [Route("Bond/GetAssignmentMethodDropDown")]
        [Route("HirePurchase/GetAssignmentMethodDropDown")]
        [Route("LCDLC/GetAssignmentMethodDropDown")]
        [Route("Leasing/GetAssignmentMethodDropDown")]
        [Route("ProjectFinance/GetAssignmentMethodDropDown")]
        public ActionResult GetAssignmentMethodDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentMethod(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetBillingResponsibleByDropDown")]
        [Route("Bond/GetBillingResponsibleByDropDown")]
        [Route("HirePurchase/GetBillingResponsibleByDropDown")]
        [Route("LCDLC/GetBillingResponsibleByDropDown")]
        [Route("Leasing/GetBillingResponsibleByDropDown")]
        [Route("ProjectFinance/GetBillingResponsibleByDropDown")]
        public ActionResult GetBillingResponsibleByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBillingResponsibleBy(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetBusinessSegmentDropDown")]
        [Route("Bond/GetBusinessSegmentDropDown")]
        [Route("HirePurchase/GetBusinessSegmentDropDown")]
        [Route("LCDLC/GetBusinessSegmentDropDown")]
        [Route("Leasing/GetBusinessSegmentDropDown")]
        [Route("ProjectFinance/GetBusinessSegmentDropDown")]
        public ActionResult GetBusinessSegmentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessSegment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetBusinessTypeDropDown")]
        [Route("Bond/GetBusinessTypeDropDown")]
        [Route("HirePurchase/GetBusinessTypeDropDown")]
        [Route("LCDLC/GetBusinessTypeDropDown")]
        [Route("Leasing/GetBusinessTypeDropDown")]
        [Route("ProjectFinance/GetBusinessTypeDropDown")]
        public ActionResult GetBusinessTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetBuyerTableDropDown")]
        [Route("Bond/GetBuyerTableDropDown")]
        [Route("HirePurchase/GetBuyerTableDropDown")]
        [Route("LCDLC/GetBuyerTableDropDown")]
        [Route("Leasing/GetBuyerTableDropDown")]
        [Route("ProjectFinance/GetBuyerTableDropDown")]
        [Route("Factoring/RelatedInfo/verificationtable/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/verificationtable/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/verificationtable/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/verificationtable/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/verificationtable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/verificationtable/GetBuyerTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/GetBuyerTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-child/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetBuyerTableDropDown")]

        [Route("Factoring/RelatedInfo/retentiontransaction/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/retentiontransaction/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/retentiontransaction/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/retentiontransaction/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/retentiontransaction/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/retentiontransaction/GetBuyerTableDropDown")]

        [Route("Factoring/RelatedInfo/CreditApplicationTransaction/GetBuyerTableDropDown")]
        /// pdc
        [Route("Factoring/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetBuyerTableDropDown")]
        //
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetBuyerTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetBuyerTableDropDown")]

        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/pdc/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetConsortiumTableDropDown")]
        [Route("Bond/GetConsortiumTableDropDown")]
        [Route("HirePurchase/GetConsortiumTableDropDown")]
        [Route("LCDLC/GetConsortiumTableDropDown")]
        [Route("Leasing/GetConsortiumTableDropDown")]
        [Route("ProjectFinance/GetConsortiumTableDropDown")]
        public ActionResult GetConsortiumTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemConsortiumTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetContactPersonTransDropDown")]
        [Route("Bond/GetContactPersonTransDropDown")]
        [Route("HirePurchase/GetContactPersonTransDropDown")]
        [Route("LCDLC/GetContactPersonTransDropDown")]
        [Route("Leasing/GetContactPersonTransDropDown")]
        [Route("ProjectFinance/GetContactPersonTransDropDown")]
        public ActionResult GetContactPersonTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemContactPersonTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCreditAppRequestTableDropDown")]
        [Route("Bond/GetCreditAppRequestTableDropDown")]
        [Route("HirePurchase/GetCreditAppRequestTableDropDown")]
        [Route("LCDLC/GetCreditAppRequestTableDropDown")]
        [Route("Leasing/GetCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/GetCreditAppRequestTableDropDown")]
        public ActionResult GetCreditAppRequestTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCreditAppTableDropDown")]
        [Route("Bond/GetCreditAppTableDropDown")]
        [Route("HirePurchase/GetCreditAppTableDropDown")]
        [Route("LCDLC/GetCreditAppTableDropDown")]
        [Route("Leasing/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/GetCreditAppTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]

        [Route("Factoring/RelatedInfo/retentiontransaction/GetCreditAppTableDropDown")]
        [Route("Bond/RelatedInfo/retentiontransaction/GetCreditAppTableDropDown")]
        [Route("HirePurchase/RelatedInfo/retentiontransaction/GetCreditAppTableDropDown")]
        [Route("LCDLC/RelatedInfo/retentiontransaction/GetCreditAppTableDropDown")]
        [Route("Leasing/RelatedInfo/retentiontransaction/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/retentiontransaction/GetCreditAppTableDropDown")]
        #region customerbuyercreditoutstanding
        [Route("Factoring/RelatedInfo/customerbuyercreditoutstanding/GetCreditAppTableDropDown")]
        [Route("Bond/RelatedInfo/customerbuyercreditoutstanding/GetCreditAppTableDropDown")]
        [Route("HirePurchase/RelatedInfo/customerbuyercreditoutstanding/GetCreditAppTableDropDown")]
        [Route("LCDLC/RelatedInfo/customerbuyercreditoutstanding/GetCreditAppTableDropDown")]
        [Route("Leasing/RelatedInfo/customerbuyercreditoutstanding/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/customerbuyercreditoutstanding/GetCreditAppTableDropDown")]
        #endregion customerbuyercreditoutstanding
        #region credit app trans
        [Route("Factoring/RelatedInfo/CreditApplicationTransaction/GetCreditAppTableDropDown")]
        #endregion
        #region processtrans
        [Route("Factoring/RelatedInfo/ProcessTransaction/GetCreditAppTableDropDown")]
        #endregion processtrans
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-child/RelatedInfo/invoicetable/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCreditLimitTypeDropDown")]
        [Route("Bond/GetCreditLimitTypeDropDown")]
        [Route("HirePurchase/GetCreditLimitTypeDropDown")]
        [Route("LCDLC/GetCreditLimitTypeDropDown")]
        [Route("Leasing/GetCreditLimitTypeDropDown")]
        [Route("ProjectFinance/GetCreditLimitTypeDropDown")]
        #region customerbuyercreditoutstanding
        [Route("Factoring/RelatedInfo/customerbuyercreditoutstanding/GetCreditLimitTypeDropDown")]
        [Route("Bond/RelatedInfo/customerbuyercreditoutstanding/GetCreditLimitTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/customerbuyercreditoutstanding/GetCreditLimitTypeDropDown")]
        [Route("LCDLC/RelatedInfo/customerbuyercreditoutstanding/GetCreditLimitTypeDropDown")]
        [Route("Leasing/RelatedInfo/customerbuyercreditoutstanding/GetCreditLimitTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/customerbuyercreditoutstanding/GetCreditLimitTypeDropDown")]
        #endregion customerbuyercreditoutstanding
        public ActionResult GetCreditLimitTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditLimitType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/RelatedInfo/WithdrawalTableTermExtension/Function/ExtendTerm/GetCreditTermDropDown")]
        [Route("Factoring/GetCreditTermDropDown")]
        [Route("Bond/GetCreditTermDropDown")]
        [Route("HirePurchase/GetCreditTermDropDown")]
        [Route("LCDLC/GetCreditTermDropDown")]
        [Route("Leasing/GetCreditTermDropDown")]
        [Route("ProjectFinance/GetCreditTermDropDown")]
        public ActionResult GetCreditTermDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditTerm(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCustBankDropDown")]
        [Route("Bond/GetCustBankDropDown")]
        [Route("HirePurchase/GetCustBankDropDown")]
        [Route("LCDLC/GetCustBankDropDown")]
        [Route("Leasing/GetCustBankDropDown")]
        [Route("ProjectFinance/GetCustBankDropDown")]
        public ActionResult GetCustBankDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustBank(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCustomerTableDropDown")]
        [Route("Bond/GetCustomerTableDropDown")]
        [Route("HirePurchase/GetCustomerTableDropDown")]
        [Route("LCDLC/GetCustomerTableDropDown")]
        [Route("Leasing/GetCustomerTableDropDown")]
        [Route("ProjectFinance/GetCustomerTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/GetCustomerTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetCustomerTableDropDown")]
        #region customerbuyercreditoutstanding
        [Route("Factoring/RelatedInfo/customerbuyercreditoutstanding/GetCustomerTableDropDown")]
        [Route("Bond/RelatedInfo/customerbuyercreditoutstanding/GetCustomerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/customerbuyercreditoutstanding/GetCustomerTableDropDown")]
        [Route("LCDLC/RelatedInfo/customerbuyercreditoutstanding/GetCustomerTableDropDown")]
        [Route("Leasing/RelatedInfo/customerbuyercreditoutstanding/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/customerbuyercreditoutstanding/GetCustomerTableDropDown")]
        #endregion customerbuyercreditoutstanding
        [Route("Factoring/RelatedInfo/CreditApplicationTransaction/GetCustomerTableDropDown")]
        #region processtrans
        [Route("Factoring/RelatedInfo/ProcessTransaction/GetCustomerTableDropDown")]
        #endregion processtrans

        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/pdc/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetDocumentReasonDropDown")]
        [Route("Bond/GetDocumentReasonDropDown")]
        [Route("HirePurchase/GetDocumentReasonDropDown")]
        [Route("LCDLC/GetDocumentReasonDropDown")]
        [Route("Leasing/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/GetDocumentReasonDropDown")]
        [Route("Factoring/RelatedInfo/VerificationTable/Function/updateverificationstatus/GetDocumentReasonDropDown")]
        [Route("Bond/RelatedInfo/VerificationTable/Function/updateverificationstatus/GetDocumentReasonDropDown")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/Function/updateverificationstatus/GetDocumentReasonDropDown")]
        [Route("LCDLC/RelatedInfo/VerificationTable/Function/updateverificationstatus/GetDocumentReasonDropDown")]
        [Route("Leasing/RelatedInfo/VerificationTable/Function/updateverificationstatus/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/Function/updateverificationstatus/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetInterestTypeDropDown")]
        [Route("Bond/GetInterestTypeDropDown")]
        [Route("HirePurchase/GetInterestTypeDropDown")]
        [Route("LCDLC/GetInterestTypeDropDown")]
        [Route("Leasing/GetInterestTypeDropDown")]
        [Route("ProjectFinance/GetInterestTypeDropDown")]
        public ActionResult GetInterestTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInterestType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetIntroducedByDropDown")]
        [Route("Bond/GetIntroducedByDropDown")]
        [Route("HirePurchase/GetIntroducedByDropDown")]
        [Route("LCDLC/GetIntroducedByDropDown")]
        [Route("Leasing/GetIntroducedByDropDown")]
        [Route("ProjectFinance/GetIntroducedByDropDown")]
        public ActionResult GetIntroducedByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemIntroducedBy(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetLedgerDimensionDropDown")]
        [Route("Bond/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/GetLedgerDimensionDropDown")]
        [Route("LCDLC/GetLedgerDimensionDropDown")]
        [Route("Leasing/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/GetLedgerDimensionDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        public ActionResult GetLedgerDimensionDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetLineOfBusinessDropDown")]
        [Route("Bond/GetLineOfBusinessDropDown")]
        [Route("HirePurchase/GetLineOfBusinessDropDown")]
        [Route("LCDLC/GetLineOfBusinessDropDown")]
        [Route("Leasing/GetLineOfBusinessDropDown")]
        [Route("ProjectFinance/GetLineOfBusinessDropDown")]
        public ActionResult GetLineOfBusinessDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLineOfBusiness(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetMethodOfPaymentDropDown")]
        [Route("Bond/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/GetMethodOfPaymentDropDown")]
        [Route("Leasing/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/GetMethodOfPaymentDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-child/RelatedInfo/invoicetable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetMethodOfPaymentDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetProductSubTypeDropDown")]
        [Route("Bond/GetProductSubTypeDropDown")]
        [Route("HirePurchase/GetProductSubTypeDropDown")]
        [Route("LCDLC/GetProductSubTypeDropDown")]
        [Route("Leasing/GetProductSubTypeDropDown")]
        [Route("ProjectFinance/GetProductSubTypeDropDown")]
        public ActionResult GetProductSubTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemProductSubType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetServiceFeeCondTemplateTableDropDown")]
        [Route("Bond/GetServiceFeeCondTemplateTableDropDown")]
        [Route("HirePurchase/GetServiceFeeCondTemplateTableDropDown")]
        [Route("LCDLC/GetServiceFeeCondTemplateTableDropDown")]
        [Route("Leasing/GetServiceFeeCondTemplateTableDropDown")]
        [Route("ProjectFinance/GetServiceFeeCondTemplateTableDropDown")]
        public ActionResult GetServiceFeeCondTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemServiceFeeCondTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region AuthorizedPersonTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]
        public ActionResult GetAuthorizedPersonListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAuthorizedPersonTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]
        public ActionResult GetAuthorizedPersonById(string id)
        {
            try
            {
                IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(AuthorizedPersonTransService.GetAuthorizedPersonTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetRelatedInfoAuthorizedPersonTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]
        public ActionResult GetRelatedInfoAuthorizedPersonTransRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AuthorizedPersonTrans
        #region OwnerTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/OwnerTrans/GetOwnerTransList/ByCompany")]
        [Route("Bond/RelatedInfo/OwnerTrans/GetOwnerTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/OwnerTrans/GetOwnerTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/OwnerTrans/GetOwnerTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/OwnerTrans/GetOwnerTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/OwnerTrans/GetOwnerTransList/ByCompany")]
        public ActionResult GetOwnerTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetOwnerTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/OwnerTrans/GetOwnerTransById/id={id}")]
        [Route("Bond/RelatedInfo/OwnerTrans/GetOwnerTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/OwnerTrans/GetOwnerTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/OwnerTrans/GetOwnerTransById/id={id}")]
        [Route("Leasing/RelatedInfo/OwnerTrans/GetOwnerTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/OwnerTrans/GetOwnerTransById/id={id}")]
        public ActionResult GetOwnerTransById(string id)
        {
            try
            {
                IOwnerTransService ownerTransService = new OwnerTransService(db);
                return Ok(ownerTransService.GetOwnerTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/OwnerTrans/GetRelatedPersonTableDropDown")]
        [Route("Bond/RelatedInfo/OwnerTrans/GetRelatedPersonTableDropDown")]
        [Route("HirePurchase/RelatedInfo/OwnerTrans/GetRelatedPersonTableDropDown")]
        [Route("LCDLC/RelatedInfo/OwnerTrans/GetRelatedPersonTableDropDown")]
        [Route("Leasing/RelatedInfo/OwnerTrans/GetRelatedPersonTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/OwnerTrans/GetRelatedPersonTableDropDown")]
        public ActionResult GetOwnerTransRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region GuarantorTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/GuarantorTrans/GetGuarantorTransList/ByCompany")]
        [Route("Bond/RelatedInfo/GuarantorTrans/GetGuarantorTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/GetGuarantorTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/GetGuarantorTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/GetGuarantorTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/GetGuarantorTransList/ByCompany")]
        public ActionResult GetGuarantorTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetGuarantorTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/GuarantorTrans/GetGuarantorTransById/id={id}")]
        [Route("Bond/RelatedInfo/GuarantorTrans/GetGuarantorTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/GetGuarantorTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/GetGuarantorTransById/id={id}")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/GetGuarantorTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/GetGuarantorTransById/id={id}")]
        public ActionResult GetGuarantorTransById(string id)
        {
            try
            {
                IGuarantorTransService guarantorTransService = new GuarantorTransService(db);
                return Ok(guarantorTransService.GetGuarantorTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/GuarantorTrans/GetGuarantorTypeDropDown")]
        [Route("Bond/RelatedInfo/GuarantorTrans/GetGuarantorTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/GetGuarantorTypeDropDown")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/GetGuarantorTypeDropDown")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/GetGuarantorTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/GetGuarantorTypeDropDown")]
        public ActionResult GetGuarantorTransGuarantorTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemGuarantorType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/GuarantorTrans/GetRelatedPersonTableDropDown")]
        [Route("Bond/RelatedInfo/GuarantorTrans/GetRelatedPersonTableDropDown")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/GetRelatedPersonTableDropDown")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/GetRelatedPersonTableDropDown")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/GetRelatedPersonTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/GetRelatedPersonTableDropDown")]
        public ActionResult GetGuarantorTransRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region RetentionConditionTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransList/ByCompany")]
        [Route("Bond/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransList/ByCompany")]
        public ActionResult GetRetentionConditionTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetRetentionConditionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransById/id={id}")]
        [Route("Bond/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransById/id={id}")]
        [Route("Leasing/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransById/id={id}")]
        public ActionResult GetRetentionConditionTransById(string id)
        {
            try
            {
                IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db);
                return Ok(retentionConditionTransService.GetRetentionConditionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateBase")]
        [Route("Bond/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateBase")]
        [Route("HirePurchase/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateBase")]
        [Route("LCDLC/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateBase")]
        [Route("Leasing/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateBase")]
        [Route("ProjectFinance/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateBase")]
        public ActionResult GetRetentionCalculateBase([FromBody] RetentionConditionTransItemView vwModel)
        {
            try
            {
                IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db);
                return Ok(retentionConditionTransService.GetRetentionCalculateBase(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateMethod")]
        [Route("Bond/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateMethod")]
        [Route("HirePurchase/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateMethod")]
        [Route("LCDLC/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateMethod")]
        [Route("Leasing/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateMethod")]
        [Route("ProjectFinance/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateMethod")]
        public ActionResult GetRetentionCalculateMethod([FromBody] RetentionConditionTransItemView vwModel)
        {
            try
            {
                IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db);
                return Ok(retentionConditionTransService.GetRetentionCalculateMethod(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        #endregion
        #endregion
        #region ServiceFeeConditionTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        #region AmendCA
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        #endregion AmendCA
        #region AmendCALine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        #endregion AmendCALine
        public ActionResult GetServiceFeeConditionTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeConditionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        public ActionResult GetAmendCaServiceFeeConditionTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetServiceFeeConditionTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        public ActionResult GetServiceFeeConditionTransAccessModeByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusLessThenWaitingForMarketingStaff(id, string.Empty));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        public ActionResult GetAmendCaLineServiceFeeConditionTransAccessModeByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusLessThenWaitingForMarketingStaff(string.Empty, id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        public ActionResult GetAmendCaLineServiceFeeConditionTransChildInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetServiceFeeConditionTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        #region AmendCALine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        #endregion AmendCALine
        public ActionResult CreateAmendCaServiceFeeConditionTrans([FromBody] ServiceFeeConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db, SysTransactionLogService);
                    return Ok(serviceFeeConditionTransService.CreateServiceFeeConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        #region AmendCA
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        #endregion AmendCA
        #region AmendCALine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        #endregion AmendCALine
        public ActionResult GetServiceFeeConditionTransById(string id)
        {
            try
            {
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db);
                return Ok(serviceFeeConditionTransService.GetServiceFeeConditionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        #region AmendCA
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        #endregion AmendCA
        #region AmendCALine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        #endregion AmendCALine
        public ActionResult GetCreditAppRequestTableProductTypeByIdServiceFeeCondition(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                var a = creditAppRequestTableService.GetCreditAppRequestTableById(id).ProductType;
                return Ok(a);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ServiceFeeConditionTrans
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        #region AmendCA
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        #endregion AmendCA
        #region AmendCALine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        #endregion AmendCALine
        public ActionResult GetDropDownInvoiceRevenueTypeByProductTypeServiceFeeCondition([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueType = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueType.GetInvoiceRevenueTypeByProductType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        #region AmendCA
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        #endregion AmendCA

        #region AmendCALine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        #endregion AmendCALine
        public ActionResult UpdateServiceFeeConditionTrans([FromBody] ServiceFeeConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db, SysTransactionLogService);
                    return Ok(serviceFeeConditionTransService.UpdateServiceFeeConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        #region AmendCA
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        #endregion AmendCA
        #region AmendCALine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        #endregion AmendCALine
        public ActionResult DeleteServiceFeeConditionTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db, SysTransactionLogService);
                return Ok(serviceFeeConditionTransService.DeleteServiceFeeConditionTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region BuyerAgreementTrans
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        public ActionResult GetBuyerAgreementTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBuyerAgreementTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        public ActionResult GetBuyerAgreementTransById(string id)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                return Ok(buyerAgreementTransService.GetBuyerAgreementTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        public ActionResult GetCreditAppRequestTableProductTypeByIdBuyerAgreementTrans(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                var a = creditAppRequestTableService.GetCreditAppRequestTableById(id).ProductType;
                return Ok(a);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        public ActionResult GetDropDownBuyerAgreementTableBuyerAgreementTrans([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementTableByBuyerAgreementTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]

        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]

        public ActionResult GetDropDownBuyerAgreementLineBuyerAgreementTrans([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementLineTableByBuyerAgreementTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region DocumentConditionTrans
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        public ActionResult GetDocumentConditionTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetDocumentConditionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        public ActionResult GetDocumentConditionTransById(string id)
        {
            try
            {
                IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db);
                return Ok(DocumentConditionTransService.GetDocumentConditionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        public ActionResult CreateDocumentConditionTrans([FromBody] DocumentConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
                    return Ok(DocumentConditionTransService.CreateDocumentConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        public ActionResult UpdateDocumentConditionTrans([FromBody] DocumentConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
                    return Ok(DocumentConditionTransService.UpdateDocumentConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        public ActionResult DeleteDocumentConditionTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
                return Ok(DocumentConditionTransService.DeleteDocumentConditionTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        public ActionResult GetDocumentConditionTransChildInitialData(string id)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetDocumentConditionTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        public ActionResult GetRelatedInfoDocumentConditionTransDocumentTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DocumentConditionTrans
        #region BuyerInvoiceTable
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceTableList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceTableList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceTableList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceTableList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceTableList/ByCompany")]
        public ActionResult GetBuyerInvoiceTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBuyerInvoiceTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceTableById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceTableById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceTableById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceTableById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceTableById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceTableById/id={id}")]
        public ActionResult GetBuyerInvoiceTableById(string id)
        {
            try
            {
                IBuyerInvoiceTableService buyerInvoiceTableService = new BuyerInvoiceTableService(db);
                return Ok(buyerInvoiceTableService.GetBuyerInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerInvoice/CreateBuyerInvoiceTable")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerInvoice/CreateBuyerInvoiceTable")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerInvoice/CreateBuyerInvoiceTable")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerInvoice/CreateBuyerInvoiceTable")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerInvoice/CreateBuyerInvoiceTable")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerInvoice/CreateBuyerInvoiceTable")]
        public ActionResult CreateBuyerInvoiceTable([FromBody] BuyerInvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerInvoiceTableService buyerInvoiceTableService = new BuyerInvoiceTableService(db, SysTransactionLogService);
                    return Ok(buyerInvoiceTableService.CreateBuyerInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerInvoice/UpdateBuyerInvoiceTable")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerInvoice/UpdateBuyerInvoiceTable")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerInvoice/UpdateBuyerInvoiceTable")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerInvoice/UpdateBuyerInvoiceTable")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerInvoice/UpdateBuyerInvoiceTable")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerInvoice/UpdateBuyerInvoiceTable")]
        public ActionResult UpdateBuyerInvoiceTable([FromBody] BuyerInvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerInvoiceTableService buyerInvoiceTableService = new BuyerInvoiceTableService(db, SysTransactionLogService);
                    return Ok(buyerInvoiceTableService.UpdateBuyerInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerInvoice/DeleteBuyerInvoiceTable")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerInvoice/DeleteBuyerInvoiceTable")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerInvoice/DeleteBuyerInvoiceTable")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerInvoice/DeleteBuyerInvoiceTable")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerInvoice/DeleteBuyerInvoiceTable")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerInvoice/DeleteBuyerInvoiceTable")]
        public ActionResult DeleteBuyerInvoiceTable([FromBody] RowIdentity parm)
        {
            try
            {
                IBuyerInvoiceTableService buyerInvoiceTableService = new BuyerInvoiceTableService(db, SysTransactionLogService);
                return Ok(buyerInvoiceTableService.DeleteBuyerInvoiceTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceInitialData/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceInitialData/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceInitialData/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceInitialData/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceInitialData/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerInvoiceInitialData/id={id}")]
        public ActionResult GetBuyerInvoiceInitialData(string id)
        {
            try
            {
                IBuyerInvoiceTableService buyerInvoiceTableService = new BuyerInvoiceTableService(db, SysTransactionLogService);
                return Ok(buyerInvoiceTableService.GetBuyerInvoiceInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Dropdown
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerAgreementLineDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerAgreementLineDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerAgreementLineDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerAgreementLineDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerAgreementLineDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerAgreementLineDropDown")]
        public ActionResult GetBuyerAgreementLineDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementLineTableByBuyerAgreementTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerAgeementTableByCustomerAndBuyerDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerAgeementTableByCustomerAndBuyerDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerAgeementTableByCustomerAndBuyerDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerAgeementTableByCustomerAndBuyerDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerAgeementTableByCustomerAndBuyerDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetBuyerAgeementTableByCustomerAndBuyerDropDown")]

        public ActionResult GetBuyerAgreementTableDropDownByCustomerAndBuyer([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementTableByCustomerAndBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]

        [Route("Factoring/RelatedInfo/retentiontransaction/GetBuyerAgreementTableDropDown")]
        [Route("Bond/RelatedInfo/retentiontransaction/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/RelatedInfo/retentiontransaction/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/RelatedInfo/retentiontransaction/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/RelatedInfo/retentiontransaction/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/retentiontransaction/GetBuyerAgreementTableDropDown")]

        public ActionResult GetBuyerAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        public ActionResult GetReceiptTempTableTaxInvoiceDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemReceiptTempTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CA Request
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/RelatedInfo/AmendCa/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/RelatedInfo/AmendCa/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/RelatedInfo/AmendCa/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/ValidateIsManualNumberSeq/productType={productType}")]
        public ActionResult ValidateIsManualNumberSeq(int productType)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.IsManualByCreditAppRequest(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region AmendCA
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendList/ByCompany")]
        public ActionResult GetCreditAppRequestTableAmendListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppRequestTableAmendListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendById/id={id}")]
        public ActionResult GetCreditAppRequestTableAmendById(string id)
        {
            try
            {
                ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db);
                return Ok(creditAppRequestTableAmendService.GetCreditAppRequestTableAmendById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/CreateCreditAppRequestTableAmend")]
        [Route("Bond/RelatedInfo/AmendCa/CreateCreditAppRequestTableAmend")]
        [Route("HirePurchase/RelatedInfo/AmendCa/CreateCreditAppRequestTableAmend")]
        [Route("LCDLC/RelatedInfo/AmendCa/CreateCreditAppRequestTableAmend")]
        [Route("Leasing/RelatedInfo/AmendCa/CreateCreditAppRequestTableAmend")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/CreateCreditAppRequestTableAmend")]
        public ActionResult CreateCreditAppRequestTableAmend([FromBody] CreditAppRequestTableAmendItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableAmendService.CreateCreditAppRequestTableAmend(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/UpdateCreditAppRequestTableAmend")]
        [Route("Bond/RelatedInfo/AmendCa/UpdateCreditAppRequestTableAmend")]
        [Route("HirePurchase/RelatedInfo/AmendCa/UpdateCreditAppRequestTableAmend")]
        [Route("LCDLC/RelatedInfo/AmendCa/UpdateCreditAppRequestTableAmend")]
        [Route("Leasing/RelatedInfo/AmendCa/UpdateCreditAppRequestTableAmend")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/UpdateCreditAppRequestTableAmend")]
        public ActionResult UpdateCreditAppRequestTableAmend([FromBody] CreditAppRequestTableAmendItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableAmendService.UpdateCreditAppRequestTableAmend(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/DeleteCreditAppRequestTableAmend")]
        [Route("Bond/RelatedInfo/AmendCa/DeleteCreditAppRequestTableAmend")]
        [Route("HirePurchase/RelatedInfo/AmendCa/DeleteCreditAppRequestTableAmend")]
        [Route("LCDLC/RelatedInfo/AmendCa/DeleteCreditAppRequestTableAmend")]
        [Route("Leasing/RelatedInfo/AmendCa/DeleteCreditAppRequestTableAmend")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/DeleteCreditAppRequestTableAmend")]
        public ActionResult DeleteCreditAppRequestTableAmend([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableAmendService.DeleteCreditAppRequestTableAmend(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/GetCreditAppRequestTableAmnendInitialData/CreditAppId={creditAppId}")]
        [Route("Bond/RelatedInfo/AmendCa/GetCreditAppRequestTableAmnendInitialData/CreditAppId={creditAppId}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetCreditAppRequestTableAmnendInitialData/CreditAppId={creditAppId}")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetCreditAppRequestTableAmnendInitialData/CreditAppId={creditAppId}")]
        [Route("Leasing/RelatedInfo/AmendCa/GetCreditAppRequestTableAmnendInitialData/CreditAppId={creditAppId}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetCreditAppRequestTableAmnendInitialData/CreditAppId={creditAppId}")]
        public ActionResult GetCreditAppRequestTableAmnendInitialData(string creditAppId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableAmendService.GetCreditAppRequestTableAmnendInitialData(creditAppId));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/GetAccessModeAmendCaByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/GetAccessModeAmendCaByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetAccessModeAmendCaByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetAccessModeAmendCaByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/GetAccessModeAmendCaByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetAccessModeAmendCaByCreditAppRequestTable/id={id}")]
        public ActionResult GetAccessModeAmendCaByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusBetweenDraftAndWaitingForAssistMDAndNotWaitingForMarketingHead(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetInterestTypeValueByCreditAppRequestAmend")]
        [Route("Bond/RelatedInfo/AmendCa/GetInterestTypeValueByCreditAppRequestAmend")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetInterestTypeValueByCreditAppRequestAmend")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetInterestTypeValueByCreditAppRequestAmend")]
        [Route("Leasing/RelatedInfo/AmendCa/GetInterestTypeValueByCreditAppRequestAmend")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetInterestTypeValueByCreditAppRequestAmend")]
        public ActionResult GetInterestTypeValueByCreditAppRequestAmend([FromBody] CreditAppRequestTableAmendItemView model)
        {
            try
            {
                ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db);
                return Ok(creditAppRequestTableAmendService.GetInterestTypeValueByCreditAppRequestAmend(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetCustomerCreditLimit")]
        [Route("Bond/RelatedInfo/AmendCa/GetCustomerCreditLimit")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetCustomerCreditLimit")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetCustomerCreditLimit")]
        [Route("Leasing/RelatedInfo/AmendCa/GetCustomerCreditLimit")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetCustomerCreditLimit")]
        public ActionResult GetCustomerCreditLimit([FromBody] CreditAppRequestTableAmendItemView model)
        {
            try
            {
                ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db);
                return Ok(creditAppRequestTableAmendService.GetCustomerCreditLimit(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/UpdateCreditAppRequestTableByAmendCA")]
        [Route("Bond/RelatedInfo/AmendCa/UpdateCreditAppRequestTableByAmendCA")]
        [Route("HirePurchase/RelatedInfo/AmendCa/UpdateCreditAppRequestTableByAmendCA")]
        [Route("LCDLC/RelatedInfo/AmendCa/UpdateCreditAppRequestTableByAmendCA")]
        [Route("Leasing/RelatedInfo/AmendCa/UpdateCreditAppRequestTableByAmendCA")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/UpdateCreditAppRequestTableByAmendCA")]
        public ActionResult UpdateCreditAppRequestTableByAmendCA([FromBody] CreditAppRequestTableAmendItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableAmendService.UpdateCreditAppRequestTableByAmendCA(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendUnboundForAmendRate")]
        [Route("Bond/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendUnboundForAmendRate")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendUnboundForAmendRate")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendUnboundForAmendRate")]
        [Route("Leasing/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendUnboundForAmendRate")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetCreditAppRequestTableAmendUnboundForAmendRate")]
        public ActionResult GetCreditAppRequestTableAmendUnboundForAmendRate([FromBody] CreditAppRequestTableAmendItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableAmendService.GetCreditAppRequestTableAmendUnboundForAmendRate(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/CreateAuthorizedPersonTransByAmendCa")]
        [Route("Bond/RelatedInfo/AmendCa/CreateAuthorizedPersonTransByAmendCa")]
        [Route("HirePurchase/RelatedInfo/AmendCa/CreateAuthorizedPersonTransByAmendCa")]
        [Route("LCDLC/RelatedInfo/AmendCa/CreateAuthorizedPersonTransByAmendCa")]
        [Route("Leasing/RelatedInfo/AmendCa/CreateAuthorizedPersonTransByAmendCa")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/CreateAuthorizedPersonTransByAmendCa")]
        public ActionResult CreateAuthorizedPersonTransByAmendCa([FromBody] CreditAppRequestTableAmendItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db, SysTransactionLogService);
                    creditAppRequestTableAmendService.CreateAuthorizedPersonTrans(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/DeleteAuthorizedPersonTransByAmendCa")]
        [Route("Bond/RelatedInfo/AmendCa/DeleteAuthorizedPersonTransByAmendCa")]
        [Route("HirePurchase/RelatedInfo/AmendCa/DeleteAuthorizedPersonTransByAmendCa")]
        [Route("LCDLC/RelatedInfo/AmendCa/DeleteAuthorizedPersonTransByAmendCa")]
        [Route("Leasing/RelatedInfo/AmendCa/DeleteAuthorizedPersonTransByAmendCa")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/DeleteAuthorizedPersonTransByAmendCa")]
        public ActionResult DeleteAuthorizedPersonTransByAmendCa([FromBody] CreditAppRequestTableAmendItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db, SysTransactionLogService);
                    creditAppRequestTableAmendService.DeleteAuthorizedPersonTrans(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/CreateGuarantorTransByAmendCa")]
        [Route("Bond/RelatedInfo/AmendCa/CreateGuarantorTransByAmendCa")]
        [Route("HirePurchase/RelatedInfo/AmendCa/CreateGuarantorTransByAmendCa")]
        [Route("LCDLC/RelatedInfo/AmendCa/CreateGuarantorTransByAmendCa")]
        [Route("Leasing/RelatedInfo/AmendCa/CreateGuarantorTransByAmendCa")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/CreateGuarantorTransByAmendCa")]
        public ActionResult CreateGuarantorTransByAmendCa([FromBody] CreditAppRequestTableAmendItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db, SysTransactionLogService);
                    creditAppRequestTableAmendService.CreateGuarantorTrans(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/DeleteGuarantorTransByAmendCa")]
        [Route("Bond/RelatedInfo/AmendCa/DeleteGuarantorTransByAmendCa")]
        [Route("HirePurchase/RelatedInfo/AmendCa/DeleteGuarantorTransByAmendCa")]
        [Route("LCDLC/RelatedInfo/AmendCa/DeleteGuarantorTransByAmendCa")]
        [Route("Leasing/RelatedInfo/AmendCa/DeleteGuarantorTransByAmendCa")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/DeleteGuarantorTransByAmendCa")]
        public ActionResult DeleteGuarantorTransByAmendCa([FromBody] CreditAppRequestTableAmendItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db, SysTransactionLogService);
                    creditAppRequestTableAmendService.DeleteGuarantorTrans(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region TaxReport
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetTaxReportTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetTaxReportTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region FinancialCreditTrans
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetFinancialCreditTransById(string id)
        {
            try
            {
                IFinancialCreditTransService FinancialCreditTransService = new FinancialCreditTransService(db);
                return Ok(FinancialCreditTransService.GetFinancialCreditTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        [Route("ProjectFinance/RelatedInfo/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult DeleteTaxReportTrans([FromBody] RowIdentity parm)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db, SysTransactionLogService);
                return Ok(taxReportTransService.DeleteTaxReportTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetFinancialCreditListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetFinancialCreditTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetTaxReportTransById(string id)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db);
                return Ok(taxReportTransService.GetTaxReportTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetFinancialCreditTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetFinancialCreditTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region FinancialCreditTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        #endregion
        #region NCBTrans
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        #endregion
        #region NCBTransAuth
        [Route("Factoring/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetCreditTypeDropDown")]
        [Route("Bond/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetCreditTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetCreditTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetCreditTypeDropDown")]
        [Route("Leasing/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetCreditTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetCreditTypeDropDown")]
        #endregion
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetCreditTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        [Route("ProjectFinance/RelatedInfo/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult CreateTaxReportTrans([FromBody] TaxReportTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ITaxReportTransService taxReportTransService = new TaxReportTransService(db, SysTransactionLogService);
                    return Ok(taxReportTransService.CreateTaxReportTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult CreateFinancialCreditTrans([FromBody] FinancialCreditTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialCreditTransService financialCreditTransService = new FinancialCreditTransService(db, SysTransactionLogService);
                    return Ok(financialCreditTransService.CreateFinancialCreditTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult UpdateFinancialCreditTrans([FromBody] FinancialCreditTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialCreditTransService financialCreditTransService = new FinancialCreditTransService(db, SysTransactionLogService);
                    return Ok(financialCreditTransService.UpdateFinancialCreditTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        [Route("ProjectFinance/RelatedInfo/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult UpdateTaxReportTrans([FromBody] TaxReportTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ITaxReportTransService taxReportTransService = new TaxReportTransService(db, SysTransactionLogService);
                    return Ok(taxReportTransService.UpdateTaxReportTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult DeleteFinancialCreditTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IFinancialCreditTransService financialCreditTransService = new FinancialCreditTransService(db, SysTransactionLogService);
                return Ok(financialCreditTransService.DeleteFinancialCreditTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region NCBTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        #region NCBTransAuth
        [Route("Factoring/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransList/ByCompany")]
        [Route("Bond/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransList/ByCompany")]
        #endregion
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetNCBTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetNCBTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/CreateNCBTrans")]
        #region NCBTransAuth
        [Route("Factoring/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/CreateNCBTrans")]
        [Route("Bond/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/CreateNCBTrans")]
        [Route("HirePurchase/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/CreateNCBTrans")]
        [Route("LCDLC/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/CreateNCBTrans")]
        [Route("Leasing/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/CreateNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/CreateNCBTrans")]
        #endregion
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/NCBTrans/CreateNCBTrans")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult CreateNCBTrans([FromBody] NCBTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    INCBTransService ncbTransService = new NCBTransService(db, SysTransactionLogService);
                    return Ok(ncbTransService.CreateNCBTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        #region NCBTransAuth
        [Route("Factoring/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/UpdateNCBTrans")]
        [Route("Bond/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/UpdateNCBTrans")]
        [Route("HirePurchase/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/UpdateNCBTrans")]
        [Route("LCDLC/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/UpdateNCBTrans")]
        [Route("Leasing/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/UpdateNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/UpdateNCBTrans")]
        #endregion
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult UpdateNCBTrans([FromBody] NCBTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    INCBTransService ncbTransService = new NCBTransService(db, SysTransactionLogService);
                    return Ok(ncbTransService.UpdateNCBTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetTaxReportTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetTaxReportTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        #region NCBTransAuth
        [Route("Factoring/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/DeleteNCBTrans")]
        [Route("Bond/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/DeleteNCBTrans")]
        [Route("HirePurchase/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/DeleteNCBTrans")]
        [Route("LCDLC/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/DeleteNCBTrans")]
        [Route("Leasing/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/DeleteNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/DeleteNCBTrans")]
        #endregion
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult DeleteNCBTrans([FromBody] RowIdentity parm)
        {
            try
            {
                INCBTransService ncbTransService = new NCBTransService(db, SysTransactionLogService);
                return Ok(ncbTransService.DeleteNCBTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        #region NCBTransAuth
        [Route("Factoring/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransById/id={id}")]
        [Route("Bond/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransById/id={id}")]
        [Route("Leasing/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransById/id={id}")]
        #endregion
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetNCBTransById(string id)
        {
            try
            {
                INCBTransService ncbTransService = new NCBTransService(db);
                return Ok(ncbTransService.GetNCBTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        #region NCBTransAuth
        [Route("Factoring/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBAccountStatusDropDown")]
        [Route("Bond/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBAccountStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBAccountStatusDropDown")]
        [Route("LCDLC/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBAccountStatusDropDown")]
        [Route("Leasing/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBAccountStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBAccountStatusDropDown")]
        #endregion
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetNCBAccountStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemNCBAccountStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetNCBTransInitialDataByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetNCBTransInitialDataByCreditAppRequestTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetNCBTransInitialData/id={id}")]
        public ActionResult GetNCBTransInitialDataByAuthorizedpersonTrans(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetNCBTransInitialDataByAuthorizedPersonTrans(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion    
        #region BookmarkDocumentTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        #region BookmarkDocumentTransLine 
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        #endregion
        public ActionResult GetBookmarkDocumentTransListByCompanyAmendCa([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBookmarkDocumentTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/RelatedInfo/AmendCa/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        #region BookmarkDocumentTransLine 
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        #endregion
        public ActionResult GetBookmarkDocumentTransByIdAmendCa(string id)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(BookmarkDocumentTransService.GetBookmarkDocumentTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        #region BookmarkDocumentTransLine 
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        #endregion
        public ActionResult CreateBookmarkDocumentTransAmendCa([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.CreateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        #region BookmarkDocumentTransLine 
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        #endregion
        public ActionResult UpdateBookmarkDocumentTransAmendCa([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.UpdateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        #region BookmarkDocumentTransLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        #endregion
        public ActionResult DeleteBookmarkDocumentTransAmendCa([FromBody] RowIdentity parm)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                return Ok(BookmarkDocumentTransService.DeleteBookmarkDocumentTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        #region BookmarkDocumentTransLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        #endregion
        public ActionResult GetBookmarkDocumentTransAmendCaInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetBookmarkDocumentTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        #region BookmarkDocumentTransLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        #endregion 
        public ActionResult GetBookMarkDocumentTransAmendCaAccessModeByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetBookMarkDocumentTransAccessModeByCreditAppRequestTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        #region BookmarkDocumentTransLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        #endregion
        public ActionResult GetBookmarkDocumentAmendCaDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
                return Ok(bookmarkDocumentService.GetDropDownItemBookmarkDocumentByCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        #region BookmarkDocumentTransLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        #endregion
        public ActionResult GetDocumentTemplateTableDropDownByDocumentTypeAmendCa([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
                return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        #region BookmarkDocumentTransLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        #endregion
        public ActionResult GetDocumentTemplateTableDropDownAmendCa([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        #region BookmarkDocumentTransLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        #endregion
        public ActionResult GetBookmarkDocumentTransAmendCaDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetDropDownItemBookMarkDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #endregion BookmarkDocumentTrans
        #region Amend CA Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetInterestTypeDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/GetInterestTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetInterestTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetInterestTypeDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/GetInterestTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetInterestTypeDropDown")]
        public ActionResult GetAmendCaInterestTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInterestType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetCreditScoringDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/GetCreditScoringDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetCreditScoringDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetCreditScoringDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/GetCreditScoringDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetCreditScoringDropDown")]
        public ActionResult GetAmendCaCreditScoringDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditScoring(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetBlacklistStatusDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/GetBlacklistStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetBlacklistStatusDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetBlacklistStatusDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/GetBlacklistStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetBlacklistStatusDropDown")]
        public ActionResult GetAmendCaBlacklistStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBlacklistStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetKYCSetupDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/GetKYCSetupDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetKYCSetupDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetKYCSetupDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/GetKYCSetupDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetKYCSetupDropDown")]
        public ActionResult GetAmendCaKYCSetupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemKYCSetup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetOriginalInvoiceDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/GetOriginalInvoiceDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetOriginalInvoiceDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetOriginalInvoiceDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetOriginalInvoiceDropDown")]
        public ActionResult GetAmendCaOriginalInvoiceDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetOriginalInvoiceDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetDocumentReasonDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/GetDocumentReasonDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetDocumentReasonDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetDocumentReasonDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetDocumentReasonDropDown")]

        public ActionResult GetAmendCaDocumentReasonDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Amend CA Dropdown
        #region AuthorizedPersonTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetAuthorizedPersonTransList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/GetAuthorizedPersonTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetAuthorizedPersonTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetAuthorizedPersonTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/GetAuthorizedPersonTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetAuthorizedPersonTransList/ByCompany")]
        public ActionResult GetAmendCaAuthorizedPersonListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAuthorizedPersonTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/GetAuthorizedPersonTransById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/GetAuthorizedPersonTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetAuthorizedPersonTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetAuthorizedPersonTransById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/GetAuthorizedPersonTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetAuthorizedPersonTransById/id={id}")]
        public ActionResult GetAmendCaAuthorizedPersonById(string id)
        {
            try
            {
                IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(AuthorizedPersonTransService.GetAuthorizedPersonTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/CreateAuthorizedPersonTrans")]
        [Route("Bond/RelatedInfo/AmendCa/CreateAuthorizedPersonTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/CreateAuthorizedPersonTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/CreateAuthorizedPersonTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/CreateAuthorizedPersonTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/CreateAuthorizedPersonTrans")]
        public ActionResult CreateAuthorizedPerson([FromBody] AuthorizedPersonTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db, SysTransactionLogService);
                    return Ok(AuthorizedPersonTransService.CreateAuthorizedPersonTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/UpdateAuthorizedPersonTrans")]
        [Route("Bond/RelatedInfo/AmendCa/UpdateAuthorizedPersonTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/UpdateAuthorizedPersonTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/UpdateAuthorizedPersonTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/UpdateAuthorizedPersonTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/UpdateAuthorizedPersonTrans")]
        public ActionResult UpdateAuthorizedPerson([FromBody] AuthorizedPersonTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db, SysTransactionLogService);
                    return Ok(AuthorizedPersonTransService.UpdateAuthorizedPersonTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/DeleteAuthorizedPersonTrans")]
        [Route("Bond/RelatedInfo/AmendCa/DeleteAuthorizedPersonTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/DeleteAuthorizedPersonTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/DeleteAuthorizedPersonTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/DeleteAuthorizedPersonTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/DeleteAuthorizedPersonTrans")]
        public ActionResult DeleteAuthorizedPerson([FromBody] RowIdentity parm)
        {
            try
            {
                IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db, SysTransactionLogService);
                return Ok(AuthorizedPersonTransService.DeleteAuthorizedPersonTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/GetAuthorizedPersonTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/GetAuthorizedPersonTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetAuthorizedPersonTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetAuthorizedPersonTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/GetAuthorizedPersonTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetAuthorizedPersonTransInitialData/id={id}")]
        public ActionResult GetAuthorizedPersonTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db);
                return Ok(creditAppRequestTableAmendService.GetAuthorizedPersonTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetAccessModeByCreditAppRequestTable/id={id}")]
        #region TaxReport
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        #endregion TaxReport
        [Route("Factoring/RelatedInfo/AmendCa/GetAccessModeAuthorizedPersonTransByCreditAppRequestTable/creditAppRequestId={id}")]
        [Route("Bond/RelatedInfo/AmendCa/GetAccessModeAuthorizedPersonTransByCreditAppRequestTable/creditAppRequestId={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetAccessModeAuthorizedPersonTransByCreditAppRequestTable/creditAppRequestId={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetAccessModeAuthorizedPersonTransByCreditAppRequestTable/creditAppRequestId={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/GetAccessModeAuthorizedPersonTransByCreditAppRequestTable/creditAppRequestId={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetAccessModeAuthorizedPersonTransByCreditAppRequestTable/creditAppRequestId={id}")]

        public ActionResult GetAccessModeByCreditAppRequestTableStatusIsLessThanWaitingForMarketingStaffByAmendCa(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(id, string.Empty));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        #region NCBTrans
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]


        #endregion
        #region FinancialCreditTrans
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        #endregion
        #region FinancialStatement
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        #endregion FinancialStatement
        #region FinancialStatement [Amend CA line]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        #endregion
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetAccessModeByCreditAppRequestTableStatusIsLessThanWaitingForMarketingStaffByCreditAppRequest(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetAccessModeCARequestTableByAuthorizedperson/id={id}")]
        [Route("Bond/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetAccessModeCARequestTableByAuthorizedperson/id={id}")]
        [Route("HirePurchase/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetAccessModeCARequestTableByAuthorizedperson/id={id}")]
        [Route("LCDLC/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetAccessModeCARequestTableByAuthorizedperson/id={id}")]
        [Route("Leasing/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetAccessModeCARequestTableByAuthorizedperson/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetAccessModeCARequestTableByAuthorizedperson/id={id}")]
        public ActionResult GetAccessModeCARequestTableByAuthorizedperson(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeCARequestTableByAuthorizedperson(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/RelatedInfo/AmendCa/GetAuthorizedPersonTransInitialListData/amendCAId={amendCAId}")]
        [Route("Bond/RelatedInfo/AmendCa/GetAuthorizedPersonTransInitialListData/amendCAId={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetAuthorizedPersonTransInitialListData/amendCAId={amendCAId}")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetAuthorizedPersonTransInitialListData/amendCAId={amendCAId}")]
        [Route("Leasing/RelatedInfo/AmendCa/GetAuthorizedPersonTransInitialListData/amendCAId={amendCAId}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetAuthorizedPersonTransInitialListData/amendCAId={amendCAId}")]
        public ActionResult GetAuthorizedPersonTransInitialListData(string amendCAId)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAuthorizedPersonTransInitialListData(amendCAId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region AuthorizedPersonTrans Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetRelatedPersonTableByCreditAppRequestTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/GetRelatedPersonTableByCreditAppRequestTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetRelatedPersonTableByCreditAppRequestTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetRelatedPersonTableByCreditAppRequestTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/GetRelatedPersonTableByCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetRelatedPersonTableByCreditAppRequestTableDropDown")]
        public ActionResult GetRelatedPersonTableByCreditAppRequestTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
                return Ok(relatedPersonTableService.GetDropDownItemRelatedPersonTableItemByCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown")]
        public ActionResult GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
                return Ok(relatedPersonTableService.GetDropDownItemRelatedPersonTableItemByCreditAppRequestTableGuarantor(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetRelatedPersonTableByAmendCADropDown")]
        [Route("Bond/RelatedInfo/AmendCa/GetRelatedPersonTableByAmendCADropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetRelatedPersonTableByAmendCADropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetRelatedPersonTableByAmendCADropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/GetRelatedPersonTableByAmendCADropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetRelatedPersonTableByAmendCADropDown")]
        public ActionResult GetRelatedPersonTableByAmendCADropDown([FromBody] SearchParameter search)
        {
            try
            {
                IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
                return Ok(relatedPersonTableService.GetDropDownItemRelatedPersonTableItemByAmendCA(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetAmendCaAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AuthorizedPersonTrans Dropdown
        #endregion
        #region GuarantorTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetGuarantorTransList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/GetGuarantorTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetGuarantorTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetGuarantorTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/GetGuarantorTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetGuarantorTransList/ByCompany")]
        public ActionResult GetAmendCaGuarantorTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetGuarantorTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/GetGuarantorTransById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/GetGuarantorTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetGuarantorTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetGuarantorTransById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/GetGuarantorTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetGuarantorTransById/id={id}")]
        public ActionResult GetAmendCaGuarantorTransById(string id)
        {
            try
            {
                IGuarantorTransService GuarantorTransService = new GuarantorTransService(db);
                return Ok(GuarantorTransService.GetGuarantorTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/CreateGuarantorTrans")]
        [Route("Bond/RelatedInfo/AmendCa/CreateGuarantorTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/CreateGuarantorTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/CreateGuarantorTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/CreateGuarantorTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/CreateGuarantorTrans")]
        public ActionResult CreateGuarantorTrans([FromBody] GuarantorTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorTransService GuarantorTransService = new GuarantorTransService(db, SysTransactionLogService);
                    return Ok(GuarantorTransService.CreateGuarantorTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/UpdateGuarantorTrans")]
        [Route("Bond/RelatedInfo/AmendCa/UpdateGuarantorTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/UpdateGuarantorTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/UpdateGuarantorTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/UpdateGuarantorTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/UpdateGuarantorTrans")]
        public ActionResult UpdateGuarantorTrans([FromBody] GuarantorTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorTransService GuarantorTransService = new GuarantorTransService(db, SysTransactionLogService);
                    return Ok(GuarantorTransService.UpdateGuarantorTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/DeleteGuarantorTrans")]
        [Route("Bond/RelatedInfo/AmendCa/DeleteGuarantorTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/DeleteGuarantorTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/DeleteGuarantorTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/DeleteGuarantorTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/DeleteGuarantorTrans")]
        public ActionResult DeleteGuarantorTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IGuarantorTransService GuarantorTransService = new GuarantorTransService(db, SysTransactionLogService);
                return Ok(GuarantorTransService.DeleteGuarantorTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/GetGuarantorTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/GetGuarantorTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetGuarantorTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetGuarantorTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/GetGuarantorTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetGuarantorTransInitialData/id={id}")]
        public ActionResult GetGuarantorTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db);
                return Ok(creditAppRequestTableAmendService.GetGuarantorTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/RelatedInfo/AmendCa/GetAccessModeGuarantorTransByCreditAppRequestTable/creditAppRequestId={creditAppRequestId}")]
        [Route("Bond/RelatedInfo/AmendCa/GetAccessModeGuarantorTransByCreditAppRequestTable/creditAppRequestId={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetAccessModeGuarantorTransByCreditAppRequestTable/creditAppRequestId={creditAppRequestId}")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetAccessModeGuarantorTransByCreditAppRequestTable/creditAppRequestId={creditAppRequestId}")]
        [Route("Leasing/RelatedInfo/AmendCa/GetAccessModeGuarantorTransByCreditAppRequestTable/creditAppRequestId={creditAppRequestId}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetAccessModeGuarantorTransByCreditAppRequestTable/creditAppRequestId={creditAppRequestId}")]
        public ActionResult GetAccessModeGuarantorTransByCreditAppRequestTable(string creditAppRequestId)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(creditAppRequestId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/RelatedInfo/AmendCa/GetGuarantorTransInitialListData/amendCAId={amendCAId}")]
        [Route("Bond/RelatedInfo/AmendCa/GetGuarantorTransInitialListData/amendCAId={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetGuarantorTransInitialListData/amendCAId={amendCAId}")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetGuarantorTransInitialListData/amendCAId={amendCAId}")]
        [Route("Leasing/RelatedInfo/AmendCa/GetGuarantorTransInitialListData/amendCAId={amendCAId}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetGuarantorTransInitialListData/amendCAId={amendCAId}")]
        public ActionResult GetGuarantorTransInitialListData(string amendCAId)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetGuarantorTransInitialListData(amendCAId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region GuarantorTrans Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetGuarantorTypeDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/GetGuarantorTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetGuarantorTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetGuarantorTypeDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/GetGuarantorTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetGuarantorTypeDropDown")]
        public ActionResult GetAmendCaGuarantorTransGuarantorTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemGuarantorType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetRelatedPersonTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/GetRelatedPersonTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetRelatedPersonTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetRelatedPersonTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/GetRelatedPersonTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetRelatedPersonTableDropDown")]
        public ActionResult GetAmendCaGuarantorTransRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GuarantorTrans Dropdown
        #endregion            
        #region MemoTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        //verificationtable
        [Route("Factoring/RelatedInfo/verificationtable/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        [Route("Bond/RelatedInfo/verificationtable/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/verificationtable/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/verificationtable/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/verificationtable/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/verificationtable/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        // purchase
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        //Amendca
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        //closecreditlimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        //projectporgress
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult GetMemoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        //verificationtable
        [Route("Factoring/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        // purchase
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/memotrans/GetMemoTransById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/memotrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/memotrans/GetMemoTransById/id={id}")]
        //Amendcaline
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        //CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        //projectporgress
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetMemoById(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/CreateMemoTrans")]
        //verificationtable
        [Route("Factoring/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        // purchase
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/memotrans/CreateMemoTrans")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/memotrans/CreateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/memotrans/CreateMemoTrans")]
        //amendcaline
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/CreateMemoTrans")]
        //CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/CreateMemoTrans")]
        //projectporgress
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        // purchase
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/memotrans/UpdateMemoTrans")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/memotrans/UpdateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/memotrans/UpdateMemoTrans")]
        //amendcaline
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        //CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        //projectporgress
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        //verificationtable
        [Route("Factoring/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        // purchase
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/memotrans/DeleteMemoTrans")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/memotrans/DeleteMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/memotrans/DeleteMemoTrans")]
        //amendcaline
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        //verificationtable
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        //projectporgress
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult DeleteMemo([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialDataByAmendCa(string id)
        {
            try
            {
                ICreditAppRequestTableAmendService creditAppRequestTableAmendService = new CreditAppRequestTableAmendService(db);
                return Ok(creditAppRequestTableAmendService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        // purchase
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/memotrans/GetMemoTransInitialData/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/memotrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialDataByPurchase(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //projectporgress
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialDataByProjectProgress(string id)
        {
            try
            {
                IProjectProgressTableService projectProgressTableService = new ProjectProgressTableService(db);
                return Ok(projectProgressTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialDataByWithdrawalTermExtention(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpGet]
        [Route("Factoring/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByVerification/id={id}")]
        [Route("Bond/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByVerification/id={id}")]
        [Route("HirePurchase/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByVerification/id={id}")]
        [Route("LCDLC/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByVerification/id={id}")]
        [Route("Leasing/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByVerification/id={id}")]
        [Route("ProjectFinance/RelatedInfo/verificationtable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByVerification/id={id}")]
        public ActionResult GetMemoTransInitialDataByVerification(string id)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetMemoTransInitialDataByVerification(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/memotrans/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/memotrans/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        public ActionResult GetPurchaseTableAccessModeByMemoTrans(string purchaseId, string isWorkflowMode)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPurchaseTableAccessMode(purchaseId, isWorkflowMode));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CreditAppReqBusinessCollateral
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        public ActionResult GetCreditAppReqBusinessCollateralListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppReqBusinessCollateralListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region ProcessTransaction
        [HttpPost]
        [Route("Factoring/RelatedInfo/ProcessTransaction/GetProcessTransList/ByCompany")]
        [Route("Bond/RelatedInfo/ProcessTransaction/GetProcessTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/ProcessTransaction/GetProcessTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/ProcessTransaction/GetProcessTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/ProcessTransaction/GetProcessTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/ProcessTransaction/GetProcessTransList/ByCompany")]
        public ActionResult GetProcessTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetProcessTransactionListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        public ActionResult GetCreditAppReqBusinessCollateralById(string id)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(CreditAppReqBusCollateralInfoService.GetCreditAppReqBusinessCollateralById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/RelatedInfo/ProcessTransaction/GetProcessTransById/id={id}")]
        [Route("Bond/RelatedInfo/ProcessTransaction/GetProcessTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/ProcessTransaction/GetProcessTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/ProcessTransaction/GetProcessTransById/id={id}")]
        [Route("Leasing/RelatedInfo/ProcessTransaction/GetProcessTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/ProcessTransaction/GetProcessTransById/id={id}")]
        public ActionResult GetProcessTransById(string id)
        {
            try
            {
                IProcessTransService processTransactionService = new ProcessTransService(db);
                var cus = processTransactionService.GetProcessTransById(id);
                return Ok(cus);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        public ActionResult CreateCreditAppReqBusinessCollateral([FromBody] CreditAppReqBusinessCollateralItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                    return Ok(CreditAppReqBusCollateralInfoService.CreateCreditAppReqBusinessCollateral(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        public ActionResult UpdateCreditAppReqBusinessCollateral([FromBody] CreditAppReqBusinessCollateralItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                    return Ok(CreditAppReqBusCollateralInfoService.UpdateCreditAppReqBusinessCollateral(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        public ActionResult DeleteCreditAppReqBusinessCollateral([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(CreditAppReqBusCollateralInfoService.DeleteCreditAppReqBusinessCollateral(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        public ActionResult GetCreditAppReqBusColateralInitialData(string creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetCreditAppReqBusinessCollateralInitialData(creditAppRequestTableGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        public ActionResult GetCreditAppReqBusinessCollateralAccessModeByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdoen CreditAppReqBusColateral
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        public ActionResult GetBankTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        public ActionResult GetBusinessCollateralBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        public ActionResult GetNotCancelCustBusinessCollateralByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService creditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(creditAppReqBusCollateralInfoService.GetDropDownItemNotCancelCustBusinessCollateralByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        #region NCBTransAuth
        [Route("Factoring/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetBankGroupDropDown")]
        [Route("Bond/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetBankGroupDropDown")]
        [Route("HirePurchase/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetBankGroupDropDown")]
        [Route("LCDLC/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetBankGroupDropDown")]
        [Route("Leasing/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedpersonTrans-Child/RelatedInfo/NcbTrans/GetBankGroupDropDown")]
        #endregion
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetBusinessCollateralBankGroupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        public ActionResult GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db);
                return Ok(businessCollateralTypeService.GetDropDownItemBusinessCollateralSubTypeByBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        public ActionResult GetCustBusinessCollateralDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustBusinessCollateral(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        public ActionResult GetBusinessCollateralSubTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralSubType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region CustBusinessCollateral
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        public ActionResult GetCustBusinessCollateralListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustBusinessCollateralListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        public ActionResult GetCustBusinessCollateralById(string id)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(CreditAppReqBusCollateralInfoService.GetCustBusinessCollateralById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown BusinessCollateral
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        public ActionResult GetBusinessCollateralStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        public ActionResult GetBusinessCollateralTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #endregion
        #region PurchaseTable & PurchaseLine
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetPurchaseLineList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetPurchaseLineList/ByCompany")]

        public ActionResult GetPurchaseLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetPurchaseLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetListPurchaseLineOutstandingList/ByCompany")]
        public ActionResult GetListPurchaseLineOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetListPurchaseLineOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetPurchaseLineById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetPurchaseLineById/id={id}")]
        [Route("Factoring/RelatedInfo/purchaselinepurchase-child/RelatedInfo/inquiryrollbillpurchaseline/GetPurchaseLineById/id={id}")]
        public ActionResult GetPurchaseLineById(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPurchaseLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/CreatePurchaseLine")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/CreatePurchaseLine")]
        public ActionResult CreatePurchaseLine([FromBody] PurchaseLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                    return Ok(purchaseTableService.CreatePurchaseLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/UpdatePurchaseLine")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/UpdatePurchaseLine")]
        public ActionResult UpdatePurchaseLine([FromBody] PurchaseLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                    return Ok(purchaseTableService.UpdatePurchaseLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/DeletePurchaseLine")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/DeletePurchaseLine")]
        public ActionResult DeletePurchaseLine([FromBody] RowIdentity parm)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                return Ok(purchaseTableService.DeletePurchaseLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetPurchaseTableList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetPurchaseTableList/ByCompany")]
        public ActionResult GetPurchaseTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetPurchaseTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetPurchaseTableById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetPurchaseTableById/id={id}")]
        public ActionResult GetPurchaseTableById(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPurchaseTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/CreatePurchaseTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/CreatePurchaseTable")]
        public ActionResult CreatePurchaseTable([FromBody] PurchaseTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                    return Ok(purchaseTableService.CreatePurchaseTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/UpdatePurchaseTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/UpdatePurchaseTable")]
        public ActionResult UpdatePurchaseTable([FromBody] PurchaseTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                    return Ok(purchaseTableService.UpdatePurchaseTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/DeletePurchaseTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/DeletePurchaseTable")]
        public ActionResult DeletePurchaseTable([FromBody] RowIdentity parm)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                return Ok(purchaseTableService.DeletePurchaseTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetPurchaseTableInitialData/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetPurchaseTableInitialData/id={id}")]
        public ActionResult GetPurchaseTableInitialData(string id)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                return Ok(creditAppTableService.GetPurchaseTableInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetPurchaseLineInitialData/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetPurchaseLineInitialData/id={id}")]
        public ActionResult GetPurchaseLineInitialData(string id)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                return Ok(creditAppTableService.GetPurchaseLineInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetPurchaseLineRollBillInitialData")]
        public ActionResult GetPurchaseLineRollBillInitialData([FromBody] PurchaseLineRollBillInitialDataParm parm)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                return Ok(creditAppTableService.GetPurchaseLineRollBillInitialData(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/ValidateIsManualNumberSeqPurchaseTable/companyId={companyId}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/ValidateIsManualNumberSeqPurchaseTable/companyId={companyId}")]
        public ActionResult ValidateIsManualNumberSeqPurchaseTable(string companyId)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.IsManualByPurchaseTable(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ValidateIsManualNumberSeq/companyId={companyId}")]
        public ActionResult ValidateIsManualNumberSeq(string companyId)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.IsManualByBuyer(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/ValidateIsPurchaseLineByPurchaseTableEmpty/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/ValidateIsPurchaseLineByPurchaseTableEmpty/id={id}")]
        public ActionResult ValidateIsPurchaseLineByPurchaseTableEmpty(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.IsPurchaseLineByPurchaseTableEmpty(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetRetentionConditionTransByPurchase/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetRetentionConditionTransByPurchase/id={id}")]
        public ActionResult GetRetentionConditionTransByPurchase(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetRetentionConditionTransByPurchase(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetPurchaseLineByDueDate")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetPurchaseLineByDueDate")]
        public ActionResult GetPurchaseLineByDueDate([FromBody] PurchaseLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                    return Ok(purchaseTableService.GetPurchaseLineByDueDate(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetPurchaseLineByLinePurchasePct")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetPurchaseLineByLinePurchasePct")]
        public ActionResult GetPurchaseLineByLinePurchasePct([FromBody] PurchaseLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                    return Ok(purchaseTableService.GetPurchaseLineByLinePurchasePct(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetPurchaseLineByBuyerInvoice")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetPurchaseLineByBuyerInvoice")]
        public ActionResult GetPurchaseLineByBuyerInvoice([FromBody] PurchaseLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                    return Ok(purchaseTableService.GetPurchaseLineByBuyerInvoice(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        public ActionResult GetPurchaseTableAccessModeByPurchase(string purchaseId, string isWorkflowMode)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPurchaseTableAccessMode(purchaseId, isWorkflowMode));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetPurchaseTableAccessModeForPurchaseLine/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetPurchaseTableAccessModeForPurchaseLine/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        public ActionResult GetPurchaseTableAccessModeForPurchaseLine(string purchaseId, string isWorkflowMode)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPurchaseTableAccessModeForPurchaseLine(purchaseId, isWorkflowMode));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Purchase functions
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Function/PurchaseTableSendEmail/GetSendPurchaseEmailById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Function/PurchaseTableSendEmail/GetSendPurchaseEmailById/id={id}")]
        public ActionResult GetSendPurchaseEmailById(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetSendEmailPurchaseById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Function/PurchaseTableSendEmail/StartWorkflow")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Function/PurchaseTableSendEmail/StartWorkflow")]
        public async Task<ActionResult> StartWorkflowBySendEmailPurchase([FromBody] SendPurchaseEmailView workflowInstance)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(await purchaseTableService.ValidateAndStartWorkflowBySendEmailPurchase(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Function/CancelPurchase/GetCancelPurchaseById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Function/CancelPurchase/GetCancelPurchaseById/id={id}")]
        public ActionResult GetCancelPurchaseById(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetCancelPurchaseById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Function/CancelPurchase/CancelPurchase")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Function/CancelPurchase/CancelPurchase")]
        public ActionResult CancelPurchase([FromBody] CancelPurchaseView view)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.CancelPurchase(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Function/CancelPurchase/GetDocumentReasonDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Function/CancelPurchase/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByServiceFeeTrans([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Additional purchase
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/ValidateAdditionalPurchaseMenuBtn")]
        public ActionResult ValidateAdditionalPurchaseMenuBtn([FromBody] PurchaseLineItemView model)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.ValidateAdditionalPurchaseMenu(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/Function/AdditionalPurchase/ValidateMaxPurchasePct")]
        public ActionResult ValidateMaxPurchasePct([FromBody] AdditionalPurchaseParamView model)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.ValidateMaxPurchasePct(model.PurchaseLineGUID, model.PurchasePct));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/Function/AdditionalPurchase/GetAdditionalPurchaseById/id={id}")]
        public ActionResult GetAdditionalPurchaseById(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetAdditionalPurchaseById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/Function/AdditionalPurchase")]
        public ActionResult AdditionalPurchase([FromBody] AdditionalPurchaseParamView parm)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                return Ok(purchaseTableService.AdditionalPurchase(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region PostPurchase
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Function/PostPurchase/GetPostPurchaseById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Function/PostPurchase/GetPostPurchaseById/id={id}")]
        public ActionResult GetPostPurchaseById(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPostPurchaseById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Function/PostPurchase")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Function/PostPurchase")]
        public ActionResult PostPurchase([FromBody] PostPurchaseParamView view)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.PostPurchase(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region UpdateStatusRollBillPurchase
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Function/UpdateStatusRollbillPurchase/GetUpdateStatusRollbillPurchaseById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Function/UpdateStatusRollbillPurchase/GetUpdateStatusRollbillPurchaseById/id={id}")]
        public ActionResult GetUpdateStatusRollbillPurchaseById(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetUpdateStatusRollbillPurchaseById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Function/UpdateStatusRollbillPurchase/UpdateStatusRollbillPurchase")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Function/UpdateStatusRollbillPurchase/UpdateStatusRollbillPurchase")]
        public ActionResult UpdateStatusRollbillPurchase([FromBody] UpdateStatusRollbillPurchaseView view)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.UpdateStatusRollbillPurchase(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetAssignmentAgreementTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetAssignmentAgreementTableDropDown")]
        public ActionResult GetAssignmentAgreementTableDropDownByPurchaseTable([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDropDownItemAssignmentAgreementItemByPurchaseLine(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetBuyerAgreementTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableDropDownByPurchaseTable([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementTableByPurchaseLine(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetBuyerAgreementTablePurchaseLineValidateBuyerDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetBuyerAgreementTablePurchaseLineValidateBuyerDropDown")]
        public ActionResult GetDropDownItemBuyerAgreementTableByPurchaseLineValidateBuyer([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementTableByPurchaseLineValidateBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetBuyerTableDropDownByPurchaseTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetBuyerTableDropDownByPurchaseTable")]
        public ActionResult GetBuyerTableDropDownByPurchaseTable([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetDropDownItemByPurchaseLine(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetBuyerInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetBuyerInvoiceTableDropDown")]
        public ActionResult GetBuyerInvoiceTableDropDownByPurchaseTable([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerInvoiceTableService buyerInvoiceTableService = new BuyerInvoiceTableService(db);
                return Ok(buyerInvoiceTableService.GetDropDownItemByCreditAppLine(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetChequeTableCustomerDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetChequeTableCustomerDropDown")]
        public ActionResult GetChequeTableDropDownByPurchaseTableCustomer([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDropDownItemChequeTableByPurchaseLineCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetChequeTableBuyerDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetChequeTableBuyerDropDown")]
        public ActionResult GetChequeTableDropDownByPurchaseTableBuyer([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDropDownItemChequeTableByPurchaseLineBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetDocumentStatusDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDownByPurchaseTable([FromBody] SearchParameter search)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetDropDownItemPurchaseTableStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetLedgerDimensionDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetLedgerDimensionDropDown")]
        public ActionResult GetLedgerDimensionDropDownByPurchaseTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetEmployeeFilterActiveStatusDropdown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetEmployeeFilterActiveStatusDropdown")]
        public ActionResult GetActiveEmployeeTableDropDownByPurchaseTable([FromBody] SearchParameter search)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetActiveEmployeeTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetPurchaseLineDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetPurchaseLineDropDown")]
        public ActionResult GetPurchaseLineDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemPurchaseLine(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetCustomerTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDownByPurchaseTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region ServiceFeeTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        public ActionResult GetServiceFeeTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        public ActionResult GetServiceFeeTransById(string id)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetServiceFeeTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        public ActionResult CreateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(serviceFeeTransService.CreateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        public ActionResult UpdateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(serviceFeeTransService.UpdateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        public ActionResult DeleteServiceFeeTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                return Ok(serviceFeeTransService.DeleteServiceFeeTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        public ActionResult GetServiceFeeTransInitialData(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                return Ok(purchaseTableService.GetServiceFeeTransInitialDataByPurchaseTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        public ActionResult GetPurchaseTableAccessModeByServiceFeeTrans(string purchaseId, string isWorkflowMode)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPurchaseTableAccessMode(purchaseId, isWorkflowMode));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        public ActionResult GetCalculateField([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetCalculateField(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        public ActionResult ValidateServiceFeeTrans([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.ValidateServiceFeeTrans(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        public ActionResult GetTaxValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetTaxValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        public ActionResult GetWHTValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetWHTValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeByProductTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeByProductTypeAndDefaultNone(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        public ActionResult GetLedgerDimensionDropDownByServiceFeeTrans([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        public ActionResult GetWithholdingTaxTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByCancelPurchase([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        public ActionResult GetOriginalInvoiceByServiceFeeTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetOriginalInvoiceByServiceFeeTransDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion ServiceFeeTrans
        #region VendorPaymentTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/VendorPaymentTrans/GetVendorPaymentTransList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/VendorPaymentTrans/GetVendorPaymentTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/vendorpaymenttrans/GetVendorPaymentTransList/ByCompany")]
        public ActionResult GetVendorPaymentTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetVendorPaymentTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/VendorPaymentTrans/GetVendorPaymentTransById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/VendorPaymentTrans/GetVendorPaymentTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/vendorpaymenttrans/GetVendorPaymentTransById/id={id}")]
        public ActionResult GetVendorPaymentTransById(string id)
        {
            try
            {
                IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db);
                return Ok(vendorPaymentTransService.GetVendorPaymentTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/VendorPaymentTrans/CreateVendorPaymentTrans")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/VendorPaymentTrans/CreateVendorPaymentTrans")]
        public ActionResult CreateVendorPaymentTrans([FromBody] VendorPaymentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db, SysTransactionLogService);
                    return Ok(vendorPaymentTransService.CreateVendorPaymentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/VendorPaymentTrans/UpdateVendorPaymentTrans")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/VendorPaymentTrans/UpdateVendorPaymentTrans")]
        public ActionResult UpdateVendorPaymentTrans([FromBody] VendorPaymentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db, SysTransactionLogService);
                    return Ok(vendorPaymentTransService.UpdateVendorPaymentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/VendorPaymentTrans/DeleteVendorPaymentTrans")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/VendorPaymentTrans/DeleteVendorPaymentTrans")]
        public ActionResult DeleteVendorPaymentTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db, SysTransactionLogService);
                return Ok(vendorPaymentTransService.DeleteVendorPaymentTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/VendorPaymentTrans/GetLedgerDimensionDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/VendorPaymentTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/vendorpaymenttrans/GetLedgerDimensionDropDown")]
        public ActionResult GetLedgerDimensionDropDownByVendorPaymentTrans([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/VendorPaymentTrans/GetTaxTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/VendorPaymentTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/vendorpaymenttrans/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDownByVendorPaymentTrans([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/VendorPaymentTrans/GetCreditAppTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/VendorPaymentTrans/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/vendorpaymenttrans/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDownByVendorPaymentTrans([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/VendorPaymentTrans/GetDocumentStatusDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/VendorPaymentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/vendorpaymenttrans/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDownByVendorPaymentTrans([FromBody] SearchParameter search)
        {
            try
            {
                IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db);
                return Ok(vendorPaymentTransService.GetDropDownItemVendorPaymentTransStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/VendorPaymentTrans/GetVendorTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/VendorPaymentTrans/GetVendorTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/vendorpaymenttrans/GetVendorTableDropDown")]
        public ActionResult GetVendorTableDropDownByVendorPaymentTrans([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemVendorTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region VendorPaymentTrans function
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/VendorPaymentTrans/ValidateSendVendorInvoiceStagingMenuBtn")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/vendorpaymenttrans/ValidateSendVendorInvoiceStagingMenuBtn")]
        public ActionResult ValidateSendVendorInvoiceStagingMenuBtn([FromBody] SendVendorInvoiceStagingParamView model)
        {
            try
            {
                IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db);
                return Ok(vendorPaymentTransService.ValidateSendVendorInvoiceStagingMenuBtn(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/VendorPaymentTrans/Function/SendVendorInvoiceStaging/GetSendVendorInvoiceStagingInitialData")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/vendorpaymenttrans/Function/SendVendorInvoiceStaging/GetSendVendorInvoiceStagingInitialData")]
        public ActionResult GetSendVendorInvoiceStagingInitialData([FromBody] SendVendorInvoiceStagingParamView model)
        {
            try
            {
                IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db);
                return Ok(vendorPaymentTransService.GetSendVendorInvoiceStagingInitialData(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/VendorPaymentTrans/Function/SendVendorInvoiceStaging")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/vendorpaymenttrans/Function/SendVendorInvoiceStaging")]
        public ActionResult SendVendorInvoiceStaging([FromBody] SendVendorInvoiceStagingParamView model)
        {
            try
            {
                List<DbContext> contexts = new List<DbContext>() { db, axContext };
                IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(contexts, SysTransactionLogService);
                return Ok(vendorPaymentTransService.SendVendorInvoiceStaging(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion VendorPaymentTrans
        #region PaymentDetail
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/PaymentDetail/GetPaymentDetailList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/PaymentDetail/GetPaymentDetailList/ByCompany")]
        public ActionResult GetPaymentDetailListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetPaymentDetailListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/PaymentDetail/GetPaymentDetailById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/PaymentDetail/GetPaymentDetailById/id={id}")]
        public ActionResult GetPaymentDetailById(string id)
        {
            try
            {
                IPaymentDetailService paymentDetailService = new PaymentDetailService(db);
                return Ok(paymentDetailService.GetPaymentDetailById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/PaymentDetail/CreatePaymentDetail")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/PaymentDetail/CreatePaymentDetail")]
        public ActionResult CreatePaymentDetail([FromBody] PaymentDetailItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPaymentDetailService paymentDetailService = new PaymentDetailService(db, SysTransactionLogService);
                    return Ok(paymentDetailService.CreatePaymentDetail(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/PaymentDetail/UpdatePaymentDetail")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/PaymentDetail/UpdatePaymentDetail")]
        public ActionResult UpdatePaymentDetail([FromBody] PaymentDetailItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPaymentDetailService paymentDetailService = new PaymentDetailService(db, SysTransactionLogService);
                    return Ok(paymentDetailService.UpdatePaymentDetail(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/PaymentDetail/DeletePaymentDetail")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/PaymentDetail/DeletePaymentDetail")]
        public ActionResult DeletePaymentDetail([FromBody] RowIdentity parm)
        {
            try
            {
                IPaymentDetailService paymentDetailService = new PaymentDetailService(db, SysTransactionLogService);
                return Ok(paymentDetailService.DeletePaymentDetail(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/PaymentDetail/GetPaymentDetailInitialData/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/PaymentDetail/GetPaymentDetailInitialData/id={id}")]
        public ActionResult GetPaymentDetailInitialData(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                return Ok(purchaseTableService.GetPaymentDetailInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/PaymentDetail/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/PaymentDetail/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        public ActionResult GetPurchaseTableAccessModeByPaymentDetail(string purchaseId, string isWorkflowMode)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPurchaseTableAccessMode(purchaseId, isWorkflowMode));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/PaymentDetail/GetCustomerTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/PaymentDetail/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDownByPaymentDetail([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/PaymentDetail/GetVendorTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/PaymentDetail/GetVendorTableDropDown")]
        public ActionResult GetVendorTableDropDownByPaymentDetail([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemVendorTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/PaymentDetail/GetInvoiceTypeDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/PaymentDetail/GetInvoiceTypeDropDown")]
        public ActionResult GetInvoiceTypeDropDownByPaymentDetail([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTypeService invoiceTypeService = new InvoiceTypeService(db);
                return Ok(invoiceTypeService.GetInvoiceTypeByPaymentDetail(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion PaymentDetail
        #region Pdc
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/GetChequeTableList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/Pdc/GetChequeTableList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetChequeTableList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/pdc/GetChequeTableList/ByCompany")]
        public ActionResult GetChequeTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetChequeTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/GetChequeTableById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/Pdc/GetChequeTableById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetChequeTableById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/pdc/GetChequeTableById/id={id}")]
        public ActionResult GetChequeTableById(string id)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetChequeTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/CreateChequeTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/Pdc/CreateChequeTable")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/CreateChequeTable")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/pdc/CreateChequeTable")]
        public ActionResult CreateChequeTable([FromBody] ChequeTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IChequeTableService chequeTableService = new ChequeTableService(db, SysTransactionLogService);
                    return Ok(chequeTableService.CreateChequeTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/UpdateChequeTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/Pdc/UpdateChequeTable")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/UpdateChequeTable")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/pdc/UpdateChequeTable")]
        public ActionResult UpdateChequeTable([FromBody] ChequeTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IChequeTableService chequeTableService = new ChequeTableService(db, SysTransactionLogService);
                    return Ok(chequeTableService.UpdateChequeTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/DeleteChequeTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/Pdc/DeleteChequeTable")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/DeleteChequeTable")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/pdc/DeleteChequeTable")]
        public ActionResult DeleteChequeTable([FromBody] RowIdentity parm)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db, SysTransactionLogService);
                return Ok(chequeTableService.DeleteChequeTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/Pdc/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        public ActionResult GetPurchaseTableAccessModeByPdc(string purchaseId, string isWorkflowMode)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPurchaseTableAccessMode(purchaseId, isWorkflowMode));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/GetBankGroupDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/Pdc/GetBankGroupDropDown")]
        #region FinancialCreditTrans
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]
        #endregion
        #region NCBTrans
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        #endregion
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/pdc/GetBankGroupDropDown")]

        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetBankGroupDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetBankGroupDropDown")]
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetBankGroupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/GetChequeTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/Pdc/GetChequeTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetChequeTableDropDown")]
        public ActionResult GetChequeTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithDocumentStatusChequeTable(search, "130140"));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/GetDocumentStatusDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/Pdc/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetDocumentStatusDropDown")]

        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/pdc/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDocumentStatusDocumentProcessDropdown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/Pdc/GetChequeTableInitialData/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/GetChequeTableInitialData/id={id}")]
        public ActionResult GetChequeTableInitialDataPDC(string id)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);

                return Ok(chequeTableService.GetChequeTableInitialDataByPDC(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Pdc
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/GetRefPDCDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/Pdc/GetRefPDCDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetRefPDCDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/pdc/GetRefPDCDropDown")]
        public ActionResult GetRefPDCDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetRefPDCDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region VerificationTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/VerificationTrans/GetVerificationTransList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/VerificationTrans/GetVerificationTransList/ByCompany")]
        public ActionResult GetVerificationTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetVerificationTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/VerificationTrans/GetVerificationTransById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/VerificationTrans/GetVerificationTransById/id={id}")]
        public ActionResult GetVerificationTransById(string id)
        {
            try
            {
                IVerificationTransService verificationTransService = new VerificationTransService(db);
                return Ok(verificationTransService.GetVerificationTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/VerificationTrans/CreateVerificationTrans")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/VerificationTrans/CreateVerificationTrans")]
        public ActionResult CreateVerificationTrans([FromBody] VerificationTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVerificationTransService verificationTransService = new VerificationTransService(db, SysTransactionLogService);
                    return Ok(verificationTransService.CreateVerificationTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/VerificationTrans/UpdateVerificationTrans")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/VerificationTrans/UpdateVerificationTrans")]
        public ActionResult UpdateVerificationTrans([FromBody] VerificationTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVerificationTransService verificationTransService = new VerificationTransService(db, SysTransactionLogService);
                    return Ok(verificationTransService.UpdateVerificationTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/VerificationTrans/DeleteVerificationTrans")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/VerificationTrans/DeleteVerificationTrans")]
        public ActionResult DeleteVerificationTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IVerificationTransService verificationTransService = new VerificationTransService(db, SysTransactionLogService);
                return Ok(verificationTransService.DeleteVerificationTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/VerificationTrans/GetVerificationTransInitialData/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/VerificationTrans/GetVerificationTransInitialData/id={id}")]
        public ActionResult GetVerificationTransInitialData(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                return Ok(purchaseTableService.GetVerificationTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/VerificationTrans/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/VerificationTrans/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        public ActionResult GetPurchaseTableAccessModeByVerificationTrans(string purchaseId, string isWorkflowMode)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPurchaseTableAccessMode(purchaseId, isWorkflowMode));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/VerificationTrans/GetVerificationTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/VerificationTrans/GetVerificationTableDropDown")]
        public ActionResult GetVerificationTableDropDownByVerificationTransListView([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemVerificationTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/VerificationTrans/GetVerificationTableByVerificationTransDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/VerificationTrans/GetVerificationTableByVerificationTransDropDown")]
        public ActionResult GetVerificationTableDropDownByVerificationTransItemView([FromBody] SearchParameter search)
        {
            try
            {
                IVerificationTableService verificationTableService = new VerificationTableService(db);
                return Ok(verificationTableService.GetVerificationTableByVerificationTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion VerificationTrans
        #region ReceiptTempTable
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetReceiptTempPaymDetailList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetReceiptTempPaymDetailList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetReceiptTempPaymDetailList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetReceiptTempPaymDetailList/ByCompany")]
        public ActionResult GetReceiptTempPaymDetailListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetReceiptTempPaymDetailListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetReceiptTempPaymDetailById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetReceiptTempPaymDetailById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetReceiptTempPaymDetailById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetReceiptTempPaymDetailById/id={id}")]
        public ActionResult GetReceiptTempPaymDetailById(string id)
        {
            try
            {
                IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
                return Ok(receiptTempTableService.GetReceiptTempPaymDetailById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/CreateReceiptTempPaymDetail")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/CreateReceiptTempPaymDetail")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/CreateReceiptTempPaymDetail")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/CreateReceiptTempPaymDetail")]
        public ActionResult CreateReceiptTempPaymDetail([FromBody] ReceiptTempPaymDetailItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
                    return Ok(receiptTempTableService.CreateReceiptTempPaymDetail(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/UpdateReceiptTempPaymDetail")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/UpdateReceiptTempPaymDetail")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/UpdateReceiptTempPaymDetail")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/UpdateReceiptTempPaymDetail")]
        public ActionResult UpdateReceiptTempPaymDetail([FromBody] ReceiptTempPaymDetailItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
                    return Ok(receiptTempTableService.UpdateReceiptTempPaymDetail(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/DeleteReceiptTempPaymDetail")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/DeleteReceiptTempPaymDetail")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/DeleteReceiptTempPaymDetail")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/DeleteReceiptTempPaymDetail")]
        public ActionResult DeleteReceiptTempPaymDetail([FromBody] RowIdentity parm)
        {
            try
            {
                IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
                return Ok(receiptTempTableService.DeleteReceiptTempPaymDetail(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetInvoiceTypeDropDownByReceiptTempTableProductType")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetInvoiceTypeDropDownByReceiptTempTableProductType")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetInvoiceTypeDropDownByReceiptTempTableProductType")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetInvoiceTypeDropDownByReceiptTempTableProductType")]
        public ActionResult GetInvoiceTypeDropDownByReceiptTempTableProductType([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTypeService invoiceTypeService = new InvoiceTypeService(db);
                return Ok(invoiceTypeService.GetInvoiceTypeByReceiptTempTableProductType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetBuyerReceiptTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetBuyerReceiptTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetBuyerReceiptTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetBuyerReceiptTableDropDown")]
        public ActionResult GetBuyerReceiptTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db);
                return Ok(buyerReceiptTableService.GetDropDownItemByReceiptTempTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetReceiptTempPaymDetailInitialData/companyId={companyId}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetReceiptTempPaymDetailInitialData/companyId={companyId}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetReceiptTempPaymDetailInitialData/companyId={companyId}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetReceiptTempPaymDetailInitialData/companyId={companyId}")]
        public ActionResult GetReceiptTempPaymDetailInitialData(string companyId)
        {
            try
            {
                IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
                return Ok(receiptTempTableService.GetReceiptTempPaymDetailInitialData(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/ReceiptTempTable/GetReceiptTempTableList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetReceiptTempTableList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetReceiptTempTableList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetReceiptTempTableList/ByCompany")]
        public ActionResult GetReceiptTempTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetReceiptTempTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetReceiptTempTableById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetReceiptTempTableById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetReceiptTempTableById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetReceiptTempTableById/id={id}")]
        public ActionResult GetReceiptTempTableById(string id)
        {
            try
            {
                IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
                return Ok(receiptTempTableService.GetReceiptTempTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/CreateReceiptTempTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/CreateReceiptTempTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/CreateReceiptTempTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/CreateReceiptTempTable")]
        public ActionResult CreateReceiptTempTable([FromBody] ReceiptTempTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
                    return Ok(receiptTempTableService.CreateReceiptTempTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/UpdateReceiptTempTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/UpdateReceiptTempTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/UpdateReceiptTempTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/UpdateReceiptTempTable")]
        public ActionResult UpdateReceiptTempTable([FromBody] ReceiptTempTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
                    return Ok(receiptTempTableService.UpdateReceiptTempTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/DeleteReceiptTempTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/DeleteReceiptTempTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/DeleteReceiptTempTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/DeleteReceiptTempTable")]
        public ActionResult DeleteReceiptTempTable([FromBody] RowIdentity parm)
        {
            try
            {
                IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
                return Ok(receiptTempTableService.DeleteReceiptTempTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetReceiptTempTableInitialData/companyId={companyId}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetReceiptTempTableInitialData/companyId={companyId}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetReceiptTempTableInitialData/companyId={companyId}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetReceiptTempTableInitialData/companyId={companyId}")]
        public ActionResult GetReceiptTempTableInitialData(string companyId)
        {
            try
            {
                IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
                return Ok(receiptTempTableService.GetReceiptTempTableInitialData(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/ValidateIsManualNumberSeqReceiptTempTable/companyId={companyId}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/ValidateIsManualNumberSeqReceiptTempTable/companyId={companyId}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/ValidateIsManualNumberSeqReceiptTempTable/companyId={companyId}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/ValidateIsManualNumberSeqReceiptTempTable/companyId={companyId}")]
        public ActionResult ValidateIsManualNumberSeqReceiptTempTable(string companyId)
        {
            try
            {
                IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
                return Ok(receiptTempTableService.IsManualByReceiptTempTable(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/ValidateIsInvoiceSettlementOrPaymentDetailCreated/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/ValidateIsInvoiceSettlementOrPaymentDetailCreated/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/ValidateIsInvoiceSettlementOrPaymentDetailCreated/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/ValidateIsInvoiceSettlementOrPaymentDetailCreated/id={id}")]
        public ActionResult ValidateIsInvoiceSettlementOrPaymentDetailCreated(string id)
        {
            try
            {
                IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
                return Ok(receiptTempTableService.IsInvoiceSettlementOrPaymentDetailCreated(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetExChangeRate")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetExChangeRate")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetExChangeRate")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetExChangeRate")]
        public ActionResult GetExChangeRate([FromBody] ReceiptTempTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
                    return Ok(receiptTempTableService.GetExChangeRate(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetReceiptTempTableAccessMode/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetReceiptTempTableAccessMode/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetReceiptTempTableAccessMode/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetReceiptTempTableAccessMode/id={id}")]
        public ActionResult GetReceiptTempTablePurchaseAccessMode(string id)
        {
            try
            {
                IReceiptTempTableService receiptTempTable = new ReceiptTempTableService(db);
                return Ok(receiptTempTable.GetReceiptTempTableAccessMode(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetCreditAppTableDropDownByReceiptTempTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetCreditAppTableDropDownByReceiptTempTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetCreditAppTableDropDownByReceiptTempTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetCreditAppTableDropDownByReceiptTempTable")]
        public ActionResult GetCreditAppTableDropDownByReceiptTempTable([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetDropDownItemCreditAppTableByReceiptTempTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetInvoiceTypeDropDownByReceiptTempTableNoneProductType")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetInvoiceTypeDropDownByReceiptTempTableNoneProductType")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetInvoiceTypeDropDownByReceiptTempTableNoneProductType")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetInvoiceTypeDropDownByReceiptTempTableNoneProductType")]
        public ActionResult GetInvoiceTypeDropDownByReceiptTempTableNoneProductType([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTypeService invoiceTypeService = new InvoiceTypeService(db);
                return Ok(invoiceTypeService.GetInvoiceTypeByReceiptTempTableNoneProductType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetChequeTableByReceiptTempPaymDetailDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetChequeTableByReceiptTempPaymDetailDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetChequeTableByReceiptTempPaymDetailDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetChequeTableByReceiptTempPaymDetailDropDown")]

        public ActionResult GetChequeTableDropDownReceiptTempPaymDetail([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDropDownItemChequeTableByReceiptTempPaymDetail(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region function
        [HttpGet]
        [Route("Bond/Function/ExtendExpiryDate/GetExtendExpiryDateById/id={id}")]
        [Route("HirePurchase/Function/ExtendExpiryDate/GetExtendExpiryDateById/id={id}")]
        [Route("LCDLC/Function/ExtendExpiryDate/GetExtendExpiryDateById/id={id}")]
        [Route("Leasing/Function/ExtendExpiryDate/GetExtendExpiryDateById/id={id}")]
        [Route("ProjectFinance/Function/ExtendExpiryDate/GetExtendExpiryDateById/id={id}")]
        [Route("Factoring/Function/ExtendExpiryDate/GetExtendExpiryDateById/id={id}")]
        public ActionResult GetExtendExpiryDateById(string id)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetExtendExpiryDateById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Bond/Function/ExtendExpiryDate")]
        [Route("HirePurchase/Function/ExtendExpiryDate")]
        [Route("LCDLC/Function/ExtendExpiryDate")]
        [Route("Leasing/Function/ExtendExpiryDate")]
        [Route("ProjectFinance/Function/ExtendExpiryDate")]
        [Route("Factoring/Function/ExtendExpiryDate")]
        public ActionResult ExtendExpiryDate([FromBody] ExtendExpiryDateResultView param)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.ExtendExpiryDate(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Bond/Function/ExtendExpiryDate/GetDocumentReasonDropdown")]
        [Route("HirePurchase/Function/ExtendExpiryDate/GetDocumentReasonDropdown")]
        [Route("LCDLC/Function/ExtendExpiryDate/GetDocumentReasonDropdown")]
        [Route("Leasing/Function/ExtendExpiryDate/GetDocumentReasonDropdown")]
        [Route("ProjectFinance/Function/ExtendExpiryDate/GetDocumentReasonDropdown")]
        [Route("Factoring/Function/ExtendExpiryDate/GetDocumentReasonDropdown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Bond/Function/UpdateExpectedSigningDate/GetDocumentReasonDropdown")]
        [Route("HirePurchase/Function/UpdateExpectedSigningDate/GetDocumentReasonDropdown")]
        [Route("LCDLC/Function/UpdateExpectedSigningDate/GetDocumentReasonDropdown")]
        [Route("Leasing/Function/UpdateExpectedSigningDate/GetDocumentReasonDropdown")]
        [Route("ProjectFinance/Function/UpdateExpectedSigningDate/GetDocumentReasonDropdown")]
        [Route("Factoring/Function/UpdateExpectedSigningDate/GetDocumentReasonDropdown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropdown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Bond/Function/ExtendExpiryDate/GetUpdateExpectedSigningDateById/id={id}")]
        [Route("HirePurchase/Function/UpdateExpectedSigningDate/GetUpdateExpectedSigningDateById/id={id}")]
        [Route("LCDLC/Function/UpdateExpectedSigningDate/GetUpdateExpectedSigningDateById/id={id}")]
        [Route("Leasing/Function/UpdateExpectedSigningDate/GetUpdateExpectedSigningDateById/id={id}")]
        [Route("ProjectFinance/Function/UpdateExpectedSigningDate/GetUpdateExpectedSigningDateById/id={id}")]
        [Route("Factoring/Function/UpdateExpectedSigningDate/GetUpdateExpectedSigningDateById/id={id}")]
        public ActionResult GetUpdateExpectedSigningDateById(string id)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetUpdateExpectedSigningDateById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Bond/Function/UpdateExpectedSigningDate")]
        [Route("HirePurchase/Function/UpdateExpectedSigningDate")]
        [Route("LCDLC/Function/UpdateExpectedSigningDate")]
        [Route("Leasing/Function/UpdateExpectedSigningDate")]
        [Route("ProjectFinance/Function/UpdateExpectedSigningDate")]
        [Route("Factoring/Function/UpdateExpectedSigningDate")]
        public ActionResult UpdateExpectedSigningDate([FromBody] UpdateExpectedSigningDateView param)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.UpdateExpectedSigningDate(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #region PrintSetBookDocTrans
        [HttpPost]
        [Route("Bond/RelatedInfo/CloseCreditLimit/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Factoring/relatedinfo/amendca/Function/printsetbookdoctrans/PrintSetBookDocTrans")]
        [Route("Bond/relatedinfo/amendca/Function/printsetbookdoctrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/relatedinfo/amendca/Function/printsetbookdoctrans/PrintSetBookDocTrans")]
        [Route("LCDLC/relatedinfo/amendca/Function/printsetbookdoctrans/PrintSetBookDocTrans")]
        [Route("Leasing/relatedinfo/amendca/Function/printsetbookdoctrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/relatedinfo/amendca/Function/printsetbookdoctrans/PrintSetBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransaction([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByRefGUID(printSetBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region VerificationTabletable
        [HttpGet]
        [Route("Factoring/RelatedInfo/VerificationTable/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Bond/RelatedInfo/VerificationTable/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("LCDLC/RelatedInfo/VerificationTable/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Leasing/RelatedInfo/VerificationTable/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/ValidateIsManualNumberSeq/companyId={companyId}")]
        public ActionResult ValidateIsManual_NumberSeq(string companyId)
        {
            try
            {
                IVerificationTableService verificationService = new VerificationTableService(db);
                return Ok(verificationService.IsManualByVerificationId(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/GetVerificationTableList/ByCompany")]
        [Route("Bond/RelatedInfo/VerificationTable/GetVerificationTableList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/GetVerificationTableList/ByCompany")]
        [Route("LCDLC/RelatedInfo/VerificationTable/GetVerificationTableList/ByCompany")]
        [Route("Leasing/RelatedInfo/VerificationTable/GetVerificationTableList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/GetVerificationTableList/ByCompany")]
        public ActionResult GetVerificationTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetVerificationTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/VerificationTable/GetVerificationTableById/id={id}")]
        [Route("Bond/RelatedInfo/VerificationTable/GetVerificationTableById/id={id}")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/GetVerificationTableById/id={id}")]
        [Route("LCDLC/RelatedInfo/VerificationTable/GetVerificationTableById/id={id}")]
        [Route("Leasing/RelatedInfo/VerificationTable/GetVerificationTableById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/GetVerificationTableById/id={id}")]
        public ActionResult GetVerificationTableById(string id)
        {
            try
            {
                IVerificationTableService VerificationTableService = new VerificationTableService(db);
                return Ok(VerificationTableService.GetVerificationTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/VerificationTable/Function/copyverification/CheckVerificationHesLineById/id={id}")]
        [Route("Bond/RelatedInfo/VerificationTable/Function/copyverification/CheckVerificationHesLineById/id={id}")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/Function/copyverification/CheckVerificationHesLineById/id={id}")]
        [Route("LCDLC/RelatedInfo/VerificationTable/Function/copyverification/CheckVerificationHesLineById/id={id}")]
        [Route("Leasing/RelatedInfo/VerificationTable/Function/copyverification/CheckVerificationHesLineById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/Function/copyverification/CheckVerificationHesLineById/id={id}")]
        public ActionResult CheckVerificationHesLineById(string id)
        {
            try
            {
                IVerificationTableService VerificationTableService = new VerificationTableService(db);
                return Ok(VerificationTableService.CheckVerificationHesLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/VerificationTable/Function/copyverification/CheckIsHassLineById/id={id}")]
        [Route("Bond/RelatedInfo/VerificationTable/Function/copyverification/CheckIsHassLineById/id={id}")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/Function/copyverification/CheckIsHassLineById/id={id}")]
        [Route("LCDLC/RelatedInfo/VerificationTable/Function/copyverification/CheckIsHassLineById/id={id}")]
        [Route("Leasing/RelatedInfo/VerificationTable/Function/copyverification/CheckIsHassLineById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/Function/copyverification/CheckIsHassLineById/id={id}")]
        public ActionResult CheckIsHassLineById(string id)
        {
            try
            {
                IVerificationTableService VerificationTableService = new VerificationTableService(db);
                return Ok(VerificationTableService.CheckIsHassLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/UpdateVerificationTable")]
        [Route("Bond/RelatedInfo/VerificationTable/UpdateVerificationTable")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/UpdateVerificationTable")]
        [Route("LCDLC/RelatedInfo/VerificationTable/UpdateVerificationTable")]
        [Route("Leasing/RelatedInfo/VerificationTable/UpdateVerificationTable")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/UpdateVerificationTable")]
        public ActionResult UpdateVerificationTable([FromBody] VerificationTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVerificationTableService VerificationTableService = new VerificationTableService(db, SysTransactionLogService);
                    return Ok(VerificationTableService.UpdateVerificationTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/DeleteVerificationTable")]
        [Route("Bond/RelatedInfo/VerificationTable/DeleteVerificationTable")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/DeleteVerificationTable")]
        [Route("LCDLC/RelatedInfo/VerificationTable/DeleteVerificationTable")]
        [Route("Leasing/RelatedInfo/VerificationTable/DeleteVerificationTable")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/DeleteVerificationTable")]
        public ActionResult DeleteVerificationTable([FromBody] RowIdentity parm)
        {
            try
            {
                IVerificationTableService VerificationTableService = new VerificationTableService(db, SysTransactionLogService);
                return Ok(VerificationTableService.DeleteVerificationTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/CreateVerificationTable")]
        [Route("Bond/RelatedInfo/VerificationTable/CreateVerificationTable")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/CreateVerificationTable")]
        [Route("LCDLC/RelatedInfo/VerificationTable/CreateVerificationTable")]
        [Route("Leasing/RelatedInfo/VerificationTable/CreateVerificationTable")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/CreateVerificationTable")]
        public ActionResult CreateVerificationTable([FromBody] VerificationTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVerificationTableService VerificationTableService = new VerificationTableService(db, SysTransactionLogService);
                    return Ok(VerificationTableService.CreateVerificationTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/VerificationTable/GetVerificationTableInitialData/id={id}")]
        [Route("Bond/RelatedInfo/VerificationTable/GetVerificationTableInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/GetVerificationTableInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/VerificationTable/GetVerificationTableInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/VerificationTable/GetVerificationTableInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/GetVerificationTableInitialData/id={id}")]
        public ActionResult GetVerificationInitialData(string id)
        {
            try
            {
                IVerificationTableService verificationTableService = new VerificationTableService(db);
                return Ok(verificationTableService.GetVerificationInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/GetDocumentStatusDropDown")]
        [Route("Bond/RelatedInfo/VerificationTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/RelatedInfo/VerificationTable/GetDocumentStatusDropDown")]
        [Route("Leasing/RelatedInfo/VerificationTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDownByVerificationTable([FromBody] SearchParameter search)
        {
            try
            {
                IVerificationTableService verificationTableService = new VerificationTableService(db);
                return Ok(verificationTableService.GetDropDownItemVerificationTableStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region report
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/Report/PrintVerification/RenderReport")]
        [Route("Bond/RelatedInfo/VerificationTable/Report/PrintVerification/RenderReport")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/Report/PrintVerification/RenderReport")]
        [Route("LCDLC/RelatedInfo/VerificationTable/Report/PrintVerification/RenderReport")]
        [Route("Leasing/RelatedInfo/VerificationTable/Report/PrintVerification/RenderReport")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/Report/PrintVerification/RenderReport")]
        public ActionResult PrintVerification([FromBody] RptPrintVerificationReportParm model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion report
        #endregion VerificationTabletable
        #region UpdateVerificationStaus
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/Function/updateverificationstatus/UpdateUpdateVerificationStatus")]
        [Route("Bond/RelatedInfo/VerificationTable/Function/updateverificationstatus/UpdateUpdateVerificationStatus")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/Function/updateverificationstatus/UpdateUpdateVerificationStatus")]
        [Route("LCDLC/RelatedInfo/VerificationTable/Function/updateverificationstatus/UpdateUpdateVerificationStatus")]
        [Route("Leasing/RelatedInfo/VerificationTable/Function/updateverificationstatus/UpdateUpdateVerificationStatus")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/Function/updateverificationstatus/UpdateUpdateVerificationStatus")]
        public ActionResult UpdateUpdateVerificationStatus([FromBody] UpdateVerificationStatusParamView param)
        {
            try
            {
                IVerificationTableService VerificationTableService = new VerificationTableService(db, SysTransactionLogService);
                return Ok(VerificationTableService.UpdateUpdateVerificationStatus(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion UpdateVerificationStaus
        #region CopyVerificationStaus
        [HttpGet]
        [Route("Factoring/RelatedInfo/VerificationTable/Function/copyverification/GetCopyVerificationById/id={id}")]
        [Route("Bond/RelatedInfo/VerificationTable/Function/copyverification/GetCopyVerificationById/id={id}")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/Function/copyverification/GetCopyVerificationById/id={id}")]
        [Route("LCDLC/RelatedInfo/VerificationTable/Function/copyverification/GetCopyVerificationById/id={id}")]
        [Route("Leasing/RelatedInfo/VerificationTable/Function/copyverification/GetCopyVerificationById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/Function/copyverification/GetCopyVerificationById/id={id}")]
        public ActionResult GetCopyVerificationById(string id)
        {
            try
            {
                IVerificationTableService VerificationTableService = new VerificationTableService(db, SysTransactionLogService);
                return Ok(VerificationTableService.GetCopyVerificationTableById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/Function/copyverification/CreateCopyVerification")]
        [Route("Bond/RelatedInfo/VerificationTable/Function/copyverification/CreateCopyVerification")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/Function/copyverification/CreateCopyVerification")]
        [Route("LCDLC/RelatedInfo/VerificationTable/Function/copyverification/CreateCopyVerification")]
        [Route("Leasing/RelatedInfo/VerificationTable/Function/copyverification/CreateCopyVerification")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/Function/copyverification/CreateCopyVerification")]
        public ActionResult CreateCopyVerification([FromBody] VerificationTableItemView vwModel)
        {
            try
            {
                IVerificationTableService VerificationTableService = new VerificationTableService(db, SysTransactionLogService);
                return Ok(VerificationTableService.CopyVerificationLine(vwModel));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/Function/copyverification/UpdateCopyVerification")]
        [Route("Bond/RelatedInfo/VerificationTable/Function/copyverification/UpdateCopyVerification")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/Function/copyverification/UpdateCopyVerification")]
        [Route("LCDLC/RelatedInfo/VerificationTable/Function/copyverification/UpdateCopyVerification")]
        [Route("Leasing/RelatedInfo/VerificationTable/Function/copyverification/UpdateCopyVerification")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/Function/copyverification/UpdateCopyVerification")]
        public ActionResult UpdateCopyVerification([FromBody] VerificationTableItemView vwModel)
        {
            try
            {
                IVerificationTableService VerificationTableService = new VerificationTableService(db, SysTransactionLogService);
                return Ok(VerificationTableService.UpdateVerificationTable(vwModel));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/Function/copyverification/DeleteCopyVerification")]
        [Route("Bond/RelatedInfo/VerificationTable/Function/copyverification/DeleteCopyVerification")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/Function/copyverification/DeleteCopyVerification")]
        [Route("LCDLC/RelatedInfo/VerificationTable/Function/copyverification/DeleteCopyVerification")]
        [Route("Leasing/RelatedInfo/VerificationTable/Function/copyverification/DeleteCopyVerification")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/Function/copyverification/DeleteCopyVerification")]
        public ActionResult DeleteCopyVerification([FromBody] RowIdentity parm)
        {
            try
            {
                IVerificationTableService VerificationTableService = new VerificationTableService(db, SysTransactionLogService);
                return Ok(VerificationTableService.DeleteVerificationTable(parm.Guid));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/Function/copyverification/GetVerificationTableDropDown")]
        [Route("Bond/RelatedInfo/VerificationTable/Function/copyverification/GetVerificationTableDropDown")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/Function/copyverification/GetVerificationTableDropDown")]
        [Route("LCDLC/RelatedInfo/VerificationTable/Function/copyverification/GetVerificationTableDropDown")]
        [Route("Leasing/RelatedInfo/VerificationTable/Function/copyverification/GetVerificationTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/Function/copyverification/GetVerificationTableDropDown")]
        public ActionResult GetVerificationTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IVerificationTableService verificationTableService = new VerificationTableService(db);
                return Ok(verificationTableService.GetVerificationTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CopyVerificationStaus
        #region function
        [HttpGet]
        [Route("Factoring/RelatedInfo/VerificationTable/Function/updateverificationstatus/GetUpdateVerificationStatusById/id={id}")]
        [Route("Bond/RelatedInfo/VerificationTable/Function/updateverificationstatus/GetUpdateVerificationStatusById/id={id}")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/Function/updateverificationstatus/GetUpdateVerificationStatusById/id={id}")]
        [Route("LCDLC/RelatedInfo/VerificationTable/Function/updateverificationstatus/GetUpdateVerificationStatusById/id={id}")]
        [Route("Leasing/RelatedInfo/VerificationTable/Function/updateverificationstatus/GetUpdateVerificationStatusById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/Function/updateverificationstatus/GetUpdateVerificationStatusById/id={id}")]
        public ActionResult<bool> GetUpdateVerificationStatusById(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                IVerificationTableService verificationTableService = new VerificationTableService(db);
                return Ok(verificationTableService.GetUpdateVerificationStatusById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion function 
        #region Dropdown
        [HttpPost]
        [Route("Bond/RelatedInfo/VerificationTable/Function/updateverificationstatus/getDocumentStatusByVerificationTableDropDown")]
        [Route("Factoring/RelatedInfo/VerificationTable/Function/updateverificationstatus/getDocumentStatusByVerificationTableDropDown")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/Function/updateverificationstatus/getDocumentStatusByVerificationTableDropDown")]
        [Route("LCDLC/RelatedInfo/VerificationTable/Function/updateverificationstatus/getDocumentStatusByVerificationTableDropDown")]
        [Route("Leasing/RelatedInfo/VerificationTable/Function/updateverificationstatus/getDocumentStatusByVerificationTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/Function/updateverificationstatus/getDocumentStatusByVerificationTableDropDown")]
        public ActionResult getDocumentStatusByVerificationTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                return Ok(documentService.GetDocumentStatusByVerificationTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/RelatedInfo/VerificationTable/Function/updateverificationstatus/getDocumentStatusDropDown")]
        [Route("Factoring/RelatedInfo/VerificationTable/Function/updateverificationstatus/getDocumentStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/Function/updateverificationstatus/getDocumentStatusDropDown")]
        [Route("LCDLC/RelatedInfo/VerificationTable/Function/updateverificationstatus/getDocumentStatusDropDown")]
        [Route("Leasing/RelatedInfo/VerificationTable/Function/updateverificationstatus/getDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/Function/updateverificationstatus/getDocumentStatusDropDown")]
        public ActionResult getDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/MemoTrans/GetMemoTransInitialDataByWithdrawal/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/MemoTrans/GetMemoTransInitialDataByWithdrawal/id={id}")]
        public ActionResult GetMemoTransInitialDataByWithdrawal(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchaselinepurchase-child/RelatedInfo/interestrealizedtrans/GetProcessTransDropDown")]
        [Route("Factoring/RelatedInfo/purchaselinerollbill-child/RelatedInfo/interestrealizedtrans/GetProcessTransDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawallinewithdrawal-child/RelatedInfo/interestrealizedtrans/GetProcessTransDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalLineTermExtension-Child/RelatedInfo/InterestRealizedTrans/GetProcessTransDropDown")]
        public ActionResult GetProcessTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemProcesTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #region VerificationLine
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/GetVerificationLineList/ByCompany")]
        [Route("Bond/RelatedInfo/VerificationTable/GetVerificationLineList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/GetVerificationLineList/ByCompany")]
        [Route("LCDLC/RelatedInfo/VerificationTable/GetVerificationLineList/ByCompany")]
        [Route("Leasing/RelatedInfo/VerificationTable/GetVerificationLineList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/GetVerificationLineList/ByCompany")]
        public ActionResult GetVerificationLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetVerificationLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/VerificationTable/GetAccessModeByParentVerificationLine/id={id}")]
        [Route("Bond/RelatedInfo/VerificationTable/GetAccessModeByParentVerificationLine/id={id}")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/GetAccessModeByParentVerificationLine/id={id}")]
        [Route("LCDLC/RelatedInfo/VerificationTable/GetAccessModeByParentVerificationLine/id={id}")]
        [Route("Leasing/RelatedInfo/VerificationTable/GetAccessModeByParentVerificationLine/id={id}")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/GetAccessModeByParentVerificationLine/id={id}")]
        public ActionResult GetAccessModeByParentVerificationLine(string id)
        {
            try
            {
                IVerificationTableService VerificationLineService = new VerificationTableService(db, SysTransactionLogService);
                return Ok(VerificationLineService.GetAccessModeByParentVerificationLine(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/VerificationTable/GetVerificationLineById/id={id}")]
        [Route("Bond/RelatedInfo/VerificationTable/GetVerificationLineById/id={id}")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/GetVerificationLineById/id={id}")]
        [Route("LCDLC/RelatedInfo/VerificationTable/GetVerificationLineById/id={id}")]
        [Route("Leasing/RelatedInfo/VerificationTable/GetVerificationLineById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/GetVerificationLineById/id={id}")]
        public ActionResult GetVerificationLineById(string id)
        {
            try
            {
                IVerificationTableService VerificationTableService = new VerificationTableService(db);
                return Ok(VerificationTableService.GetVerificationLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/UpdateVerificationLine")]
        [Route("Bond/RelatedInfo/VerificationTable/UpdateVerificationLine")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/UpdateVerificationLine")]
        [Route("LCDLC/RelatedInfo/VerificationTable/UpdateVerificationLine")]
        [Route("Leasing/RelatedInfo/VerificationTable/UpdateVerificationLine")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/UpdateVerificationLine")]
        public ActionResult UpdateVerificationLine([FromBody] VerificationLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVerificationTableService VerificationTableService = new VerificationTableService(db, SysTransactionLogService);
                    return Ok(VerificationTableService.UpdateVerificationLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/DeleteVerificationLine")]
        [Route("Bond/RelatedInfo/VerificationTable/DeleteVerificationLine")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/DeleteVerificationLine")]
        [Route("LCDLC/RelatedInfo/VerificationTable/DeleteVerificationLine")]
        [Route("Leasing/RelatedInfo/VerificationTable/DeleteVerificationLine")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/DeleteVerificationLine")]
        public ActionResult DeleteVerificationLine([FromBody] RowIdentity parm)
        {
            try
            {
                IVerificationTableService VerificationTableService = new VerificationTableService(db, SysTransactionLogService);
                return Ok(VerificationTableService.DeleteVerificationLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/CreateVerificationLine")]
        [Route("Bond/RelatedInfo/VerificationTable/CreateVerificationLine")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/CreateVerificationLine")]
        [Route("LCDLC/RelatedInfo/VerificationTable/CreateVerificationLine")]
        [Route("Leasing/RelatedInfo/VerificationTable/CreateVerificationLine")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/CreateVerificationLine")]
        public ActionResult CreateVerificationLine([FromBody] VerificationLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVerificationTableService VerificationTableService = new VerificationTableService(db, SysTransactionLogService);
                    return Ok(VerificationTableService.CreateVerificationLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/VerificationTable/GetVerificationLineInitialData/id={id}")]
        [Route("Bond/RelatedInfo/VerificationTable/GetVerificationLineInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/GetVerificationLineInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/VerificationTable/GetVerificationLineInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/VerificationTable/GetVerificationLineInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/GetVerificationLineInitialData/id={id}")]
        public ActionResult GetVerificationLineInitialData(string id)
        {
            try
            {
                IVerificationTableService verificationTableService = new VerificationTableService(db);
                return Ok(verificationTableService.GetVerificationLineInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion       
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/ProcessTransaction/GetGuarantorTypeDropDown")]
        [Route("Bond/RelatedInfo/ProcessTransaction/GetGuarantorTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/ProcessTransaction/GetGuarantorTypeDropDown")]
        [Route("LCDLC/RelatedInfo/ProcessTransaction/GetGuarantorTypeDropDown")]
        [Route("Leasing/RelatedInfo/ProcessTransaction/GetGuarantorTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/ProcessTransaction/GetGuarantorTypeDropDown")]
        public ActionResult GetProcessTransactionGuarantorTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemGuarantorType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/GetVerificationTypeDropDown")]
        [Route("Bond/RelatedInfo/VerificationTable/GetVerificationTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/GetVerificationTypeDropDown")]
        [Route("LCDLC/RelatedInfo/VerificationTable/GetVerificationTypeDropDown")]
        [Route("Leasing/RelatedInfo/VerificationTable/GetVerificationTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/GetVerificationTypeDropDown")]
        public ActionResult GetVerificationTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemVerificationType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/RelatedInfo/ProcessTransaction/GetRelatedPersonTableDropDown")]
        [Route("Bond/RelatedInfo/ProcessTransaction/GetRelatedPersonTableDropDown")]
        [Route("HirePurchase/RelatedInfo/ProcessTransaction/GetRelatedPersonTableDropDown")]
        [Route("LCDLC/RelatedInfo/ProcessTransaction/GetRelatedPersonTableDropDown")]
        [Route("Leasing/RelatedInfo/ProcessTransaction/GetRelatedPersonTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/ProcessTransaction/GetRelatedPersonTableDropDown")]
        public ActionResult GetProcessTransactionRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region VerificationTabletable
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/GetVendorTableDropDown")]
        [Route("Bond/RelatedInfo/VerificationTable/GetVendorTableDropDown")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/GetVendorTableDropDown")]
        [Route("LCDLC/RelatedInfo/VerificationTable/GetVendorTableDropDown")]
        [Route("Leasing/RelatedInfo/VerificationTable/GetVendorTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/GetVendorTableDropDown")]
        public ActionResult GetVendorTableLineDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemVendorTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]
        [Route("Bond/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]
        [Route("LCDLC/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]
        [Route("Leasing/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]
        public ActionResult GetCustomerTableByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetAccessModeByCreditAppLine/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetAccessModeByCreditAppLine/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetAccessModeByCreditAppLine/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetAccessModeByCreditAppLine/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetAccessModeByCreditAppLine/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/BuyerInvoice/GetAccessModeByCreditAppLine/id={id}")]
        public ActionResult GetAccessModeByCreditAppLine(string id)
        {
            try
            {
                IBuyerInvoiceTableService buyerInvoiceTableService = new BuyerInvoiceTableService(db, SysTransactionLogService);
                return Ok(buyerInvoiceTableService.GetAccessModeByCreditAppLine(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetBuyerTableByCreditAppTableDropDown")]
        [Route("Bond/GetBuyerTableByCreditAppTableDropDown")]
        [Route("HirePurchase/GetBuyerTableByCreditAppTableDropDown")]
        [Route("LCDLC/GetBuyerTableByCreditAppTableDropDown")]
        [Route("Leasing/GetBuyerTableByCreditAppTableDropDown")]
        [Route("ProjectFinance/GetBuyerTableByCreditAppTableDropDown")]
        [Route("Factoring/RelatedInfo/VerificationTable/GetBuyerTableByCreditAppTableDropDown")]
        [Route("Bond/RelatedInfo/VerificationTable/GetBuyerTableByCreditAppTableDropDown")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/GetBuyerTableByCreditAppTableDropDown")]
        [Route("LCDLC/RelatedInfo/VerificationTable/GetBuyerTableByCreditAppTableDropDown")]
        [Route("Leasing/RelatedInfo/VerificationTable/GetBuyerTableByCreditAppTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/GetBuyerTableByCreditAppTableDropDown")]
        public ActionResult GetBuyerTableByCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTableBy(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CreditApplicationTransaction
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditApplicationTransaction/GetCreditAppTransList/ByCompany")]
        [Route("Bond/RelatedInfo/CreditApplicationTransaction/GetCreditAppTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/CreditApplicationTransaction/GetCreditAppTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/CreditApplicationTransaction/GetCreditAppTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/CreditApplicationTransaction/GetCreditAppTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CreditApplicationTransaction/GetCreditAppTransList/ByCompany")]
        public ActionResult GetCreditApplicationTransactionListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditApplicationTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditApplicationTransaction/GetCreditAppTransById/id={id}")]
        [Route("Bond/RelatedInfo/CreditApplicationTransaction/GetCreditAppTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/CreditApplicationTransaction/GetCreditAppTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/CreditApplicationTransaction/GetCreditAppTransById/id={id}")]
        [Route("Leasing/RelatedInfo/CreditApplicationTransaction/GetCreditAppTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditApplicationTransaction/GetCreditAppTransById/id={id}")]
        public ActionResult GetCreditAppTransById(string id)
        {
            try
            {
                ICreditAppTransService creditAppTrans = new CreditAppTransService(db);
                return Ok(creditAppTrans.GetCreditAppTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditApplicationTransaction
        #region AmendCaLine
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAmendCaLineInitialData/creditAppLineId={creditAppLineId}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAmendCaLineInitialData/creditAppLineId={creditAppLineId}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAmendCaLineInitialData/creditAppLineId={creditAppLineId}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAmendCaLineInitialData/creditAppLineId={creditAppLineId}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAmendCaLineInitialData/creditAppLineId={creditAppLineId}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAmendCaLineInitialData/creditAppLineId={creditAppLineId}")]
        public ActionResult GetAmendCaLineInitialData(string creditAppLineId)
        {
            try
            {
                ICreditAppRequestLineAmendService creditAppRequestLineAmendService = new CreditAppRequestLineAmendService(db, SysTransactionLogService);
                return Ok(creditAppRequestLineAmendService.GetCreditAppRequestLineAmendInitialData(creditAppLineId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/CreateCreditAppRequestLineAmend")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/CreateCreditAppRequestLineAmend")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/CreateCreditAppRequestLineAmend")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/CreateCreditAppRequestLineAmend")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/CreateCreditAppRequestLineAmend")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/CreateCreditAppRequestLineAmend")]
        public ActionResult CreateCreditAppRequestLineAmend([FromBody] CreditAppRequestLineAmendItemView vwModel)
        {
            try
            {
                ICreditAppRequestLineAmendService creditAppRequestLineAmendService = new CreditAppRequestLineAmendService(db, SysTransactionLogService);
                return Ok(creditAppRequestLineAmendService.CreateCreditAppRequestLineAmend(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditAppRequestLineAmendList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditAppRequestLineAmendList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditAppRequestLineAmendList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditAppRequestLineAmendList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditAppRequestLineAmendList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditAppRequestLineAmendList/ByCompany")]
        public ActionResult GetCreditAppRequestLineAmendList([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppRequestLineAmendListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditAppRequestLineAmendById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditAppRequestLineAmendById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditAppRequestLineAmendById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditAppRequestLineAmendById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditAppRequestLineAmendById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditAppRequestLineAmendById/id={id}")]
        public ActionResult GetCreditAppRequestLineAmendById(string id)
        {
            try
            {
                ICreditAppRequestLineAmendService creditAppRequestLineAmendService = new CreditAppRequestLineAmendService(db, SysTransactionLogService);
                return Ok(creditAppRequestLineAmendService.GetCreditAppRequestLineAmendById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAccessModeAmendCaLineByCreditAppRequestTable/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAccessModeAmendCaLineByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAccessModeAmendCaLineByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAccessModeAmendCaLineByCreditAppRequestTable/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAccessModeAmendCaLineByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAccessModeAmendCaLineByCreditAppRequestTable/id={id}")]
        public ActionResult GetAccessModeAmendCaLineByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusBetweenDraftAndWaitingForAssistMDAndNotWaitingForMarketingHead(string.Empty, id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialDataByAmendCaLine(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppTableService.GetMemoTransInitialDataByAmendCaline(id));
            }

            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AmendCaLine
        #region Interest Realization Transaction
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchaselinepurchase-child/RelatedInfo/interestrealizedtrans/GetInterestRealizedTransList/ByCompany")]
        [Route("Factoring/RelatedInfo/purchaselinerollbill-child/RelatedInfo/interestrealizedtrans/GetInterestRealizedTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/withdrawallinewithdrawal-child/RelatedInfo/interestrealizedtrans/GetInterestRealizedTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalLineTermExtension-Child/RelatedInfo/InterestRealizedTrans/GetInterestRealizedTransList/ByCompany")]
        public ActionResult GetinterestrealizedtransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInterestRealizedTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/purchaselinepurchase-child/relatedinfo/interestrealizedtrans/GetInterestRealizedTransById/id={id}")]
        [Route("Factoring/RelatedInfo/purchaselinerollbill-child/relatedinfo/interestrealizedtrans/GetInterestRealizedTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawallinewithdrawal-child/RelatedInfo/interestrealizedtrans/GetInterestRealizedTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalLineTermExtension-Child/RelatedInfo/InterestRealizedTrans/GetInterestRealizedTransById/id={id}")]
        public ActionResult GetInterestRealizedTransById(string id)
        {
            try
            {
                IInterestRealizedTransService interestRealizedTransService = new InterestRealizedTransService(db);
                return Ok(interestRealizedTransService.GetInterestRealizedTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Interest Realization Transaction
        #region InvoiceTable
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        public ActionResult GetInvoiceLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        public ActionResult GetInvoiceLineById(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        public ActionResult CreateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        public ActionResult UpdateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        public ActionResult DeleteInvoiceLine([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        public ActionResult UpdateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        public ActionResult DeleteInvoiceTable([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        public ActionResult GetInvoiceTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        public ActionResult GetInvoiceTableById(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        public ActionResult CreateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceTable
        #region InvoiceTableDropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        public ActionResult GetInvoiceTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        public ActionResult GetBuyerInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetCurrencyDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetCurrencyDropDown")]
        public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCurrency(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        #region processtrans
        [Route("Factoring/RelatedInfo/ProcessTransaction/GetInvoiceTableDropDown")]
        #endregion processtrans
        public ActionResult GetInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        public ActionResult GetProdUnitTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        public ActionResult GetTaxInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        public ActionResult GetRefInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetRefInvoiceTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        public ActionResult GetDocumentStatusByInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceTableDropDown
        #region Amend CA Line
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/ValidateIsManualNumberSeq/productType={productType}")]
        public ActionResult AmendCaLineValidateIsManualNumberSeq(int productType)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.IsManualByCreditAppRequest(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineAmend")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineAmend")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineAmend")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineAmend")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineAmend")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineAmend")]
        public ActionResult UpdateCreditAppRequestLineAmend([FromBody] CreditAppRequestLineAmendItemView vwModel)
        {
            try
            {
                ICreditAppRequestLineAmendService creditAppRequestLineAmendService = new CreditAppRequestLineAmendService(db, SysTransactionLogService);
                return Ok(creditAppRequestLineAmendService.UpdateCreditAppRequestLineAmend(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendRate")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendRate")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendRate")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendRate")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendRate")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendRate")]
        public ActionResult UpdateCreditAppRequestLineByAmendRate([FromBody] CreditAppRequestLineAmendItemView vwModel)
        {
            try
            {
                ICreditAppRequestLineAmendService creditAppRequestLineAmendService = new CreditAppRequestLineAmendService(db, SysTransactionLogService);
                return Ok(creditAppRequestLineAmendService.UpdateCreditAppRequestLineByAmendRate(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendAssignmentMethod")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendAssignmentMethod")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendAssignmentMethod")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendAssignmentMethod")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendAssignmentMethod")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendAssignmentMethod")]
        public ActionResult UpdateCreditAppRequestLineByAmendAssignmentMethod([FromBody] CreditAppRequestLineAmendItemView vwModel)
        {
            try
            {
                ICreditAppRequestLineAmendService creditAppRequestLineAmendService = new CreditAppRequestLineAmendService(db, SysTransactionLogService);
                return Ok(creditAppRequestLineAmendService.UpdateCreditAppRequestLineByAmendAssignmentMethod(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendBillingInformation")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendBillingInformation")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendBillingInformation")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendBillingInformation")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendBillingInformation")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendBillingInformation")]
        public ActionResult UpdateCreditAppRequestLineByAmendBillingInformation([FromBody] CreditAppRequestLineAmendItemView vwModel)
        {
            try
            {
                ICreditAppRequestLineAmendService creditAppRequestLineAmendService = new CreditAppRequestLineAmendService(db, SysTransactionLogService);
                return Ok(creditAppRequestLineAmendService.UpdateCreditAppRequestLineByAmendBillingInformation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendReceiptInformation")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendReceiptInformation")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendReceiptInformation")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendReceiptInformation")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendReceiptInformation")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendReceiptInformation")]
        public ActionResult UpdateCreditAppRequestLineByAmendReceiptInformation([FromBody] CreditAppRequestLineAmendItemView vwModel)
        {
            try
            {
                ICreditAppRequestLineAmendService creditAppRequestLineAmendService = new CreditAppRequestLineAmendService(db, SysTransactionLogService);
                return Ok(creditAppRequestLineAmendService.UpdateCreditAppRequestLineByAmendReceiptInformation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendBillingDocumentCondition")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendBillingDocumentCondition")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendBillingDocumentCondition")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendBillingDocumentCondition")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendBillingDocumentCondition")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendBillingDocumentCondition")]
        public ActionResult UpdateCreditAppRequestLineByAmendBillingDocumentCondition([FromBody] CreditAppRequestLineAmendItemView vwModel)
        {
            try
            {
                ICreditAppRequestLineAmendService creditAppRequestLineAmendService = new CreditAppRequestLineAmendService(db, SysTransactionLogService);
                creditAppRequestLineAmendService.UpdateCreditAppRequestLineByAmendBillingDocumentCondition(vwModel);
                return Ok();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendReceiptDocumentCondition")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendReceiptDocumentCondition")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendReceiptDocumentCondition")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendReceiptDocumentCondition")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendReceiptDocumentCondition")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateCreditAppRequestLineByAmendReceiptDocumentCondition")]
        public ActionResult UpdateCreditAppRequestLineByAmendReceiptDocumentCondition([FromBody] CreditAppRequestLineAmendItemView vwModel)
        {
            try
            {
                ICreditAppRequestLineAmendService creditAppRequestLineAmendService = new CreditAppRequestLineAmendService(db, SysTransactionLogService);
                creditAppRequestLineAmendService.UpdateCreditAppRequestLineByAmendReceiptDocumentCondition(vwModel);
                return Ok();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region AmendCaLine Dropdown
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAssignmentMethodDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAssignmentMethodDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAssignmentMethodDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAssignmentMethodDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAssignmentMethodDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAssignmentMethodDropDown")]
        public ActionResult GetDropDownItemAssignmentMethod([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentMethod(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAddressTransByBuyerDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAddressTransByBuyerDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAddressTransByBuyerDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAddressTransByBuyerDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAddressTransByBuyerDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAddressTransByBuyerDropDown")]
        public ActionResult GetAddressTransByBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressTransByBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetMethodOfPaymentDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetMethodOfPaymentDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetMethodOfPaymentDropDown")]

        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetMethodOfPaymentDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetMethodOfPaymentDropDown")]
        public ActionResult GetAmendCALineMethodOfPaymentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetBuyerTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetBuyerTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetBuyerTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetBuyerTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetBuyerTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetBuyerTableDropDown")]
        #region customerbuyercreditoutstanding
        [Route("Factoring/RelatedInfo/customerbuyercreditoutstanding/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/customerbuyercreditoutstanding/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/customerbuyercreditoutstanding/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/customerbuyercreditoutstanding/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/customerbuyercreditoutstanding/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/customerbuyercreditoutstanding/GetBuyerTableDropDown")]
        #endregion customerbuyercreditoutstanding
        #region reciepttemptable
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/ReceiptTempTable/GetBuyerTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetBuyerTableDropDown")]
        #endregion
        public ActionResult GetDropDownItemBuyerTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCustomerTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCustomerTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCustomerTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCustomerTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCustomerTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCustomerTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]

        //pdc
        [Route("Factoring/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetCustomerTableDropDown")]
        [Route("Bond/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetCustomerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetCustomerTableDropDown")]
        [Route("LCDLC/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetCustomerTableDropDown")]
        [Route("Leasing/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetCustomerTableDropDown")]

        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetCustomerTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetCustomerTableDropDown")]
        public ActionResult GetDropDownItemCustomerTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetKYCSetupDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetKYCSetupDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetKYCSetupDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetKYCSetupDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetKYCSetupDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetKYCSetupDropDown")]
        public ActionResult GetAmendCaLineKYCSetupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemKYCSetup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetBlacklistStatusDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetBlacklistStatusDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetBlacklistStatusDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetBlacklistStatusDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetBlacklistStatusDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetBlacklistStatusDropDown")]
        public ActionResult GetAmendCaLineBlacklistStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBlacklistStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditScoringDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditScoringDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditScoringDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditScoringDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditScoringDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetCreditScoringDropDown")]
        public ActionResult GetAmendCaLineCreditScoringDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditScoring(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AmendCaLine Dropdown
        #region DocumentConditionTrans
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransList/ByCompany")]
        public ActionResult GetAmednCALineDocumentConditionTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetDocumentConditionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransById/id={id}")]
        public ActionResult GetAmendCaLineDocumentConditionTransById(string id)
        {
            try
            {
                IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db);
                return Ok(DocumentConditionTransService.GetDocumentConditionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/CreateDocumentConditionTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/CreateDocumentConditionTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/CreateDocumentConditionTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/CreateDocumentConditionTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/CreateDocumentConditionTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/CreateDocumentConditionTrans")]
        public ActionResult CreateAmendCaLineDocumentConditionTrans([FromBody] DocumentConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
                    return Ok(DocumentConditionTransService.CreateDocumentConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateDocumentConditionTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateDocumentConditionTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateDocumentConditionTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateDocumentConditionTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateDocumentConditionTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/UpdateDocumentConditionTrans")]
        public ActionResult UpdateAmendCaLineDocumentConditionTrans([FromBody] DocumentConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
                    return Ok(DocumentConditionTransService.UpdateDocumentConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/DeleteDocumentConditionTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/DeleteDocumentConditionTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/DeleteDocumentConditionTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/DeleteDocumentConditionTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/DeleteDocumentConditionTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/DeleteDocumentConditionTrans")]
        public ActionResult DeleteAmendCaLineDocumentConditionTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
                return Ok(DocumentConditionTransService.DeleteDocumentConditionTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransInitialData/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransInitialData/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransInitialData/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransInitialData/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransInitialData/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransInitialData/id={id}")]
        public ActionResult GetAmendCaLineDocumentConditionTransChildInitialData(string id)
        {
            try
            {
                ICreditAppRequestLineAmendService creditAppRequestLineAmendService = new CreditAppRequestLineAmendService(db);
                return Ok(creditAppRequestLineAmendService.GetDocumentConditionTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransInitialListData/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransInitialListData/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransInitialListData/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransInitialListData/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransInitialListData/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentConditionTransInitialListData/id={id}")]
        public ActionResult GetDocumentConditionTransInitialListData(string id)
        {
            try
            {
                ICreditAppRequestLineAmendService creditAppRequestLineAmendService = new CreditAppRequestLineAmendService(db);
                return Ok(creditAppRequestLineAmendService.GetDocumentConditionTransInitialListData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAccessModeByCreditAppRequestLine/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAccessModeByCreditAppRequestLine/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAccessModeByCreditAppRequestLine/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAccessModeByCreditAppRequestLine/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAccessModeByCreditAppRequestLine/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetAccessModeByCreditAppRequestLine/id={id}")]
        public ActionResult GetAccessModeByCreditAppRequestLine(string id)
        {
            try
            {
                // GetAccessModeByCreditAppRequestTableStatusIsLessThanWaitingForMarketingStaff
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestLine(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentTypeDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentTypeDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentTypeDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentTypeDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentTypeDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetDocumentTypeDropDown")]
        public ActionResult GetAmendCaLineRelatedInfoDocumentConditionTransDocumentTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion DocumentConditionTrans
        #region CreditAppReqBusinessCollateral
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        public ActionResult GetAmendCaLineCreditAppReqBusinessCollateralListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppReqBusinessCollateralListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        public ActionResult GetAmendCaLineCreditAppReqBusinessCollateralById(string id)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(CreditAppReqBusCollateralInfoService.GetCreditAppReqBusinessCollateralById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        public ActionResult CreateAmendCaLineCreditAppReqBusinessCollateral([FromBody] CreditAppReqBusinessCollateralItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                    return Ok(CreditAppReqBusCollateralInfoService.CreateCreditAppReqBusinessCollateral(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        public ActionResult UpdateAmendCaLineCreditAppReqBusinessCollateral([FromBody] CreditAppReqBusinessCollateralItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                    return Ok(CreditAppReqBusCollateralInfoService.UpdateCreditAppReqBusinessCollateral(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        public ActionResult DeleteAmendCaLineCreditAppReqBusinessCollateral([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(CreditAppReqBusCollateralInfoService.DeleteCreditAppReqBusinessCollateral(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        public ActionResult GetAmendCaLineCreditAppReqBusColateralInitialData(string creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetCreditAppReqBusinessCollateralInitialData(creditAppRequestTableGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        public ActionResult GetAmendCaLineCreditAppReqBusinessCollateralAccessModeByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdoen CreditAppReqBusColateral
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        public ActionResult GetAmendCaLineBankTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]


        public ActionResult GetAmendCaLineBusinessCollateralBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        public ActionResult GetAmendCaLineNotCancelCustBusinessCollateralByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService creditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(creditAppReqBusCollateralInfoService.GetDropDownItemNotCancelCustBusinessCollateralByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        public ActionResult GetAmendCaLineBusinessCollateralBankGroupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        public ActionResult GetAmendCaLineBusinessCollateralSubTypeByBusinessCollateralTypDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db);
                return Ok(businessCollateralTypeService.GetDropDownItemBusinessCollateralSubTypeByBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        public ActionResult GetAmendCaLineCustBusinessCollateralDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustBusinessCollateral(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        public ActionResult GetAmendCaLineBusinessCollateralSubTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralSubType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region CustBusinessCollateral
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        public ActionResult GetAmendCaLineCustBusinessCollateralListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustBusinessCollateralListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        public ActionResult GetAmendCaLineCustBusinessCollateralById(string id)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(CreditAppReqBusCollateralInfoService.GetCustBusinessCollateralById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown BusinessCollateral
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        public ActionResult GetAmendCaLineBusinessCollateralStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        public ActionResult GetAmendCaLineBusinessCollateralTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #endregion  Amend CA Line
        #region WithdrawalTable & WithdrawalLine
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetWithdrawalLineList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetWithdrawalLineList/ByCompany")]
        public ActionResult GetWithdrawalLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetWithdrawalLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetWithdrawalLineById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetWithdrawalLineById/id={id}")]
        public ActionResult GetWithdrawalLineById(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetWithdrawalLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/CreateWithdrawalLine")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/CreateWithdrawalLine")]
        public ActionResult CreateWithdrawalLine([FromBody] WithdrawalLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db, SysTransactionLogService);
                    return Ok(withdrawalTableService.CreateWithdrawalLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/UpdateWithdrawalLine")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/UpdateWithdrawalLine")]
        public ActionResult UpdateWithdrawalLine([FromBody] WithdrawalLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db, SysTransactionLogService);
                    return Ok(withdrawalTableService.UpdateWithdrawalLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/DeleteWithdrawalLine")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/DeleteWithdrawalLine")]
        public ActionResult DeleteWithdrawalLine([FromBody] RowIdentity parm)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db, SysTransactionLogService);
                return Ok(withdrawalTableService.DeleteWithdrawalLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetWithdrawalTableList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetWithdrawalTableList/ByCompany")]
        public ActionResult GetWithdrawalTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetWithdrawalTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetWithdrawalTableById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetWithdrawalTableById/id={id}")]
        public ActionResult GetWithdrawalTableById(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetWithdrawalTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/CreateWithdrawalTable")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/CreateWithdrawalTable")]
        public ActionResult CreateWithdrawalTable([FromBody] WithdrawalTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db, SysTransactionLogService);
                    return Ok(withdrawalTableService.CreateWithdrawalTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/UpdateWithdrawalTable")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/UpdateWithdrawalTable")]
        public ActionResult UpdateWithdrawalTable([FromBody] WithdrawalTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db, SysTransactionLogService);
                    return Ok(withdrawalTableService.UpdateWithdrawalTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/DeleteWithdrawalTable")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/DeleteWithdrawalTable")]
        public ActionResult DeleteWithdrawalTable([FromBody] RowIdentity parm)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db, SysTransactionLogService);
                return Ok(withdrawalTableService.DeleteWithdrawalTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetWithdrawalTableAccessMode/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetWithdrawalTableAccessMode/id={id}")]
        public ActionResult GetWithdrawalTableAccessMode(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetWithdrawalTableAccessMode(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/ValidateIsManualNumberSeqWithdrawalTable/companyId={companyId}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/ValidateIsManualNumberSeqWithdrawalTable/companyId={companyId}")]
        public ActionResult ValidateIsManualNumberSeqWithdrawalTable(string companyId)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.IsManualByWithdrawalTable(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/ValidateIsWithdrawalLineByWithdrawalEmpty/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/ValidateIsWithdrawalLineByWithdrawalEmpty/id={id}")]
        public ActionResult ValidateIsWithdrawalLineByWithdrawalTableEmpty(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.IsWithdrawalLineByWithdrawalTableEmpty(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/ValidateIsBuyerAgreementTransByWithdrawalTableExist/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/ValidateIsBuyerAgreementTransByWithdrawalTableExist/id={id}")]
        public ActionResult ValidateIsBuyerAgreementTransByWithdrawalTableExist(string id)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                return Ok(buyerAgreementTransService.IsExistByWithdrawal(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetWithdrawalTableInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetWithdrawalTableInitialData/id={id}")]
        public ActionResult GetWithdrawalTableInitialData(string id)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                return Ok(creditAppTableService.GetWithdrawalTableInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/RelatedInfo/WithdrawalTableTermExtension/GetWithdrawalTermExtensionDisable/id={id}")]
        public ActionResult GetWithdrawalTermExtensionDisable(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetWithdrawalTermExtensionDisable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetAssignmentAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetAssignmentAgreementTableDropDown")]
        public ActionResult GetAssignmentAgreementTableWithdrawalDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDropDownItemAssignmentAgreementItemByWithdrawalTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableWithdrawalDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementTableByWithdrawal(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetChequeTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetChequeTableDropDown")]
        public ActionResult GetChequeTableWithdrawalDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemChequeTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetChequeTableCustomerDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetChequeTableCustomerDropDown")]
        public ActionResult GetChequeTableWithdrawalCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDropDownItemChequeTableByWithdrawalLineCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetChequeTableBuyerDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetChequeTableBuyerDropDown")]
        public ActionResult GetChequeTableWithdrawalBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDropDownItemChequeTableByWithdrawalLineBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetCreditAppLineDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetCreditAppLineDropDown")]
        public ActionResult GetCreditAppLineDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetDropDownItemCreditAppLineByCreditAppTableWithdrawal(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditApplicationTransaction/GetCreditAppLineDropDown")]
        public ActionResult GetCreditAppLineDropDown2([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetDropDownItemCreditAppLineByCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetCreditTermDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetCreditTermDropDown")]
        public ActionResult GetCreditTermWithdrawalDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditTerm(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeWithdrawalDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetLedgerDimensionDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetLedgerDimensionDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetLedgerDimensionDropDown")]
        public ActionResult GetLedgerDimensionWithdrawalDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetReceiptTempTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetReceiptTempTableDropDown")]

        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ReceiptTempTable/GetReceiptTempTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/ReceiptTempTable/GetReceiptTempTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ReceiptTempTable/GetReceiptTempTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ReceiptTempTable/GetReceiptTempTableDropDown")]
        public ActionResult GetReceiptTempTableWithdrawalDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemReceiptTempTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDownByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetDropDownItemWithdrawalTableStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetCustomerTableDropDown")]
        public ActionResult RecalculateInterestGetCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/Function/CancelWithdrawal/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/RelatedInfo/WithdrawalTableTermExtension/Function/CancelWithdrawal/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonWithdrawalDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetEmployeeFilterActiveStatusDropdown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetEmployeeFilterActiveStatusDropdown")]
        public ActionResult GetActiveEmployeeTableDropDownByWithdrawal([FromBody] SearchParameter search)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetActiveEmployeeTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetWithdrawalTableByWithdrawalDate")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetWithdrawalTableByWithdrawalDate")]
        public ActionResult GetWithdrawalTableByWithdrawalDate([FromBody] WithdrawalTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                    return Ok(withdrawalTableService.GetWithdrawalTableByWithdrawalDate(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/GetWithdrawalTableByTermExtensionInvoiceRevenueType")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/GetWithdrawalTableByTermExtensionInvoiceRevenueType")]
        public ActionResult GetWithdrawalTableByTermExtensionInvoiceRevenueType([FromBody] WithdrawalTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                    return Ok(withdrawalTableService.GetWithdrawalTableByTermExtensionInvoiceRevenueType(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region WithdrawalTable Function
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/Function/RecalculateInterest/GetRecalWithdrawalInterestById/id={id}")]
        public ActionResult GetRecalWithdrawalInterestById(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetRecalWithdrawalInterestById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/Function/RecalculateInterest")]
        public ActionResult RecalculateInterest([FromBody] RecalWithdrawalInterestParamView parm)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db, SysTransactionLogService);
                return Ok(withdrawalTableService.RecalWithdrawalInterest(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/Function/RecalculateInterest/GetCreditTermDropDown")]
        public ActionResult RecalculateInterestGetCreditTermDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditTerm(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/RelatedInfo/WithdrawalTableTermExtension/Function/ExtendTerm/GetExtendTermById/id={id}")]
        public ActionResult GetExtendTermById(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetExtendTermById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/RelatedInfo/WithdrawalTableTermExtension/Function/ExtendTerm/ExtendTermValidation")]
        public ActionResult ExtendTermValidation([FromBody] ExtendTermParamView view)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetExtendTermValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/RelatedInfo/WithdrawalTableTermExtension/Function/ExtendTerm")]
        public ActionResult ExtendTerm([FromBody] ExtendTermParamView view)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.ExtendTerm(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/Function/PostWithdrawal/GetPostWithdrawalById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/RelatedInfo/WithdrawalTableTermExtension/Function/PostWithdrawal/GetPostWithdrawalById/id={id}")]
        public ActionResult GetPostWithdrawalById(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetPostWithdrawalById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/Function/PostWithdrawal/PostWithdrawal")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/RelatedInfo/WithdrawalTableTermExtension/Function/PostWithdrawal/PostWithdrawal")]
        public ActionResult PostWithdrawal([FromBody] PostWithdrawalView view)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.PostWithdrawal(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/Function/CancelWithdrawal/GetCancelWithdrawalById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/RelatedInfo/WithdrawalTableTermExtension/Function/CancelWithdrawal/GetCancelWithdrawalById/id={id}")]
        public ActionResult GetCancelWithdrawalById(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetCancelWithdrawalById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/Function/CancelWithdrawal/CancelWithdrawal")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTablewithdrawal/RelatedInfo/WithdrawalTableTermExtension/Function/CancelWithdrawal/CancelWithdrawal")]
        public ActionResult CancelWithdrawal([FromBody] CancelWithdrawalView view)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.CancelWithdrawal(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region PaymentDetail
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/PaymentDetail/GetPaymentDetailList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/PaymentDetail/GetPaymentDetailList/ByCompany")]
        public ActionResult GetPaymentDetailListByCompanyByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetPaymentDetailListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/PaymentDetail/GetPaymentDetailById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/PaymentDetail/GetPaymentDetailById/id={id}")]
        public ActionResult GetPaymentDetailByIdByWithdrawalTable(string id)
        {
            try
            {
                IPaymentDetailService paymentDetailService = new PaymentDetailService(db);
                return Ok(paymentDetailService.GetPaymentDetailById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/PaymentDetail/CreatePaymentDetail")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/PaymentDetail/CreatePaymentDetail")]
        public ActionResult CreatePaymentDetailByWithdrawalTable([FromBody] PaymentDetailItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPaymentDetailService paymentDetailService = new PaymentDetailService(db, SysTransactionLogService);
                    return Ok(paymentDetailService.CreatePaymentDetail(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/PaymentDetail/UpdatePaymentDetail")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/PaymentDetail/UpdatePaymentDetail")]
        public ActionResult UpdatePaymentDetailByWithdrawalTable([FromBody] PaymentDetailItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPaymentDetailService paymentDetailService = new PaymentDetailService(db, SysTransactionLogService);
                    return Ok(paymentDetailService.UpdatePaymentDetail(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/PaymentDetail/DeletePaymentDetail")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/PaymentDetail/DeletePaymentDetail")]
        public ActionResult DeletePaymentDetailByWithdrawalTable([FromBody] RowIdentity parm)
        {
            try
            {
                IPaymentDetailService paymentDetailService = new PaymentDetailService(db, SysTransactionLogService);
                return Ok(paymentDetailService.DeletePaymentDetail(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/PaymentDetail/GetPaymentDetailInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/PaymentDetail/GetPaymentDetailInitialData/id={id}")]
        public ActionResult GetPaymentDetailInitialDataByWithdrawalTable(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db, SysTransactionLogService);
                return Ok(withdrawalTableService.GetPaymentDetailInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/PaymentDetail/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/PaymentDetail/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDownByPaymentDetailByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/VerificationTrans/GetVerificationTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/VerificationTrans/GetVerificationTransInitialData/id={id}")]
        public ActionResult GetVerificationTransInitialDataByWithdrawalTable(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db, SysTransactionLogService);
                return Ok(withdrawalTableService.GetVerificationTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/VerificationTrans/GetWithdrawalTableAcessMode/withdrawalId={withdrawalId}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/VerificationTrans/GetWithdrawalTableAcessMode/withdrawalId={withdrawalId}")]
        public ActionResult GetWithdrawalTableAcessModeByVerificationTransByWithdrawalTable(string withdrawalId, string isWorkflowMode)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetWithdrawalTableAccessMode(withdrawalId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/PaymentDetail/GetVendorTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/PaymentDetail/GetVendorTableDropDown")]
        public ActionResult GetVendorTableDropDownByPaymentDetailByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemVendorTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion PaymentDetail
        #region VerificationTrans
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/VerificationTrans/GetVerificationTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/VerificationTrans/GetVerificationTransList/ByCompany")]
        public ActionResult GetVerificationTransListByCompanyByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetVerificationTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/PaymentDetail/GetWithdrawalTableAccessMode/withdrawalId={withdrawalId}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/PaymentDetail/GetWithdrawalTableAccessMode/withdrawalId={withdrawalId}")]
        public ActionResult GetWithdrawalTableAccessModeByPaymentDetail(string withdrawalId)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetWithdrawalTableAccessMode(withdrawalId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/VerificationTrans/GetVerificationTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/VerificationTrans/GetVerificationTransById/id={id}")]
        public ActionResult GetVerificationTransByIdByWithdrawalTable(string id)
        {
            try
            {
                IVerificationTransService verificationTransService = new VerificationTransService(db);
                return Ok(verificationTransService.GetVerificationTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/VerificationTrans/CreateVerificationTrans")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/VerificationTrans/CreateVerificationTrans")]
        public ActionResult CreateVerificationTransByWithdrawalTable([FromBody] VerificationTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVerificationTransService verificationTransService = new VerificationTransService(db, SysTransactionLogService);
                    return Ok(verificationTransService.CreateVerificationTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/VerificationTrans/UpdateVerificationTrans")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/VerificationTrans/UpdateVerificationTrans")]
        public ActionResult UpdateVerificationTransByWithdrawalTable([FromBody] VerificationTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVerificationTransService verificationTransService = new VerificationTransService(db, SysTransactionLogService);
                    return Ok(verificationTransService.UpdateVerificationTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/VerificationTrans/DeleteVerificationTrans")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/VerificationTrans/DeleteVerificationTrans")]
        public ActionResult DeleteVerificationTransByWithdrawalTable([FromBody] RowIdentity parm)
        {
            try
            {
                IVerificationTransService verificationTransService = new VerificationTransService(db, SysTransactionLogService);
                return Ok(verificationTransService.DeleteVerificationTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/VerificationTrans/GetWithdrawalTableAccessMode/withdrawalId={withdrawalId}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/VerificationTrans/GetWithdrawalTableAccessMode/withdrawalId={withdrawalId}")]
        public ActionResult GetWithdrawalTableAccessModeByVerificationTransByWithdrawalTable(string withdrawalId)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetWithdrawalTableAccessMode(withdrawalId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/VerificationTrans/GetVerificationTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/VerificationTrans/GetVerificationTableDropDown")]
        public ActionResult GetVerificationTableDropDownByVerificationTransListViewByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemVerificationTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/PaymentDetail/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/PaymentDetail/GetInvoiceTypeDropDown")]
        public ActionResult GetInvoiceTypeDropDownByPaymentDetailByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTypeService invoiceTypeService = new InvoiceTypeService(db);
                return Ok(invoiceTypeService.GetInvoiceTypeByPaymentDetail(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/VerificationTrans/GetVerificationTableByVerificationTransDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/VerificationTrans/GetVerificationTableByVerificationTransDropDown")]
        public ActionResult GetVerificationTableDropDownByVerificationTransItemViewByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                IVerificationTableService verificationTableService = new VerificationTableService(db);
                return Ok(verificationTableService.GetVerificationTableByVerificationTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion VerificationTrans
        #region ServiceFeeTrans
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        public ActionResult GetServiceFeeTransListByCompanyByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        public ActionResult GetServiceFeeTransByIdByWithdrawalTable(string id)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetServiceFeeTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        public ActionResult CreateServiceFeeTransByWithdrawalTable([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(serviceFeeTransService.CreateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        public ActionResult UpdateServiceFeeTransByWithdrawalTable([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(serviceFeeTransService.UpdateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        public ActionResult DeleteServiceFeeTransByWithdrawalTable([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                return Ok(serviceFeeTransService.DeleteServiceFeeTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        public ActionResult GetServiceFeeTransInitialDataByWithdrawalTable(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db, SysTransactionLogService);
                return Ok(withdrawalTableService.GetServiceFeeTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/GetWithdrawalTableAcessMode/withdrawalId={withdrawalId}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/GetWithdrawalTableAcessMode/withdrawalId={withdrawalId}")]
        public ActionResult GetWithdrawalTableAcessModeByServiceFeeTransByWithdrawalTablewithdrawal(string withdrawalId)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetWithdrawalTableAccessMode(withdrawalId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        public ActionResult GetCalculateFieldByWithdrawalTable([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetCalculateField(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        public ActionResult ValidateServiceFeeTransByWithdrawalTable([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.ValidateServiceFeeTrans(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        public ActionResult GetTaxValueByWithdrawalTable([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetTaxValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        public ActionResult GetWHTValueByWithdrawalTable([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetWHTValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeByProductTypeDropDownByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeByProductTypeAndDefaultNone(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        public ActionResult GetLedgerDimensionDropDownByServiceFeeTransByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDownByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        public ActionResult GetWithholdingTaxTableDropDownByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByServiceFeeTransByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        public ActionResult GetOriginalInvoiceByServiceFeeTransDropDownByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetOriginalInvoiceByServiceFeeTransDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion ServiceFeeTrans
        #region BuyerAgreementTrans

        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/BuyerAgreementTrans/CreateBuyerAgreementTrans")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/BuyerAgreementTrans/CreateBuyerAgreementTrans")]
        public ActionResult CreateBuyerAgreementTransByWithdrawalTable([FromBody] BuyerAgreementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db, SysTransactionLogService);
                    return Ok(buyerAgreementTransService.CreateBuyerAgreementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/BuyerAgreementTrans/UpdateBuyerAgreementTrans")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/BuyerAgreementTrans/UpdateBuyerAgreementTrans")]
        public ActionResult UpdateBuyerAgreementTransByWithdrawalTable([FromBody] BuyerAgreementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db, SysTransactionLogService);
                    return Ok(buyerAgreementTransService.UpdateBuyerAgreementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/BuyerAgreementTrans/DeleteBuyerAgreementTrans")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/BuyerAgreementTrans/DeleteBuyerAgreementTrans")]
        public ActionResult DeleteBuyerAgreementTransByWithdrawalTable([FromBody] RowIdentity parm)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db, SysTransactionLogService);
                return Ok(buyerAgreementTransService.DeleteBuyerAgreementTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/BuyerAgreementTrans/GetWithdrawalTableAcessMode/withdrawalId={withdrawalId}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/BuyerAgreementTrans/GetWithdrawalTableAcessMode/withdrawalId={withdrawalId}")]
        public ActionResult GetWithdrawalTableAcessModeByBuyerAgreementTransByWithdrawalTable(string withdrawalId, string isWorkflowMode)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetWithdrawalTableAccessMode(withdrawalId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransInitialData/id={id}")]
        public ActionResult GetBuyerAgreementTransInitialData(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetBuyerAgreementTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        public ActionResult GetBuyerAgreementTransListByCompanyByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBuyerAgreementTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        public ActionResult GetBuyerAgreementTransByIdByWithdrawalTable(string id)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                return Ok(buyerAgreementTransService.GetBuyerAgreementTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        public ActionResult GetCreditAppRequestTableProductTypeByIdBuyerAgreementTransByWithdrawalTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                var a = creditAppRequestTableService.GetCreditAppRequestTableById(id).ProductType;
                return Ok(a);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        public ActionResult GetDropDownBuyerAgreementTableBuyerAgreementTransByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementTableByBuyerAgreementTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        public ActionResult GetDropDownBuyerAgreementLineBuyerAgreementTransByWithdrawalTable([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementLineTableByBuyerAgreementTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region WorkFlow
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetTaskBySerialNumber")]
        [Route("Bond/RelatedInfo/AmendCa/GetTaskBySerialNumber")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetTaskBySerialNumber")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetTaskBySerialNumber")]
        [Route("Leasing/RelatedInfo/AmendCa/GetTaskBySerialNumber")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetTaskBySerialNumber")]
        [Route("Factoring/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("Bond/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("HirePurchase/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("LCDLC/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("Leasing/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("Factoring/RelatedInfo/AmendCa/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("Bond/RelatedInfo/AmendCa/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("HirePurchase/RelatedInfo/AmendCa/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("LCDLC/RelatedInfo/AmendCa/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("Leasing/RelatedInfo/AmendCa/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        // child
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetTaskBySerialNumber")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetTaskBySerialNumber")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetTaskBySerialNumber")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetTaskBySerialNumber")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetTaskBySerialNumber")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetTaskBySerialNumber")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        // Purchase
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetTaskBySerialNumber")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetTaskBySerialNumber")]
        public async Task<ActionResult> GetTaskBySerialNumber([FromBody] WorkflowInstance workflowInstance)
        {
            try
            {
                IK2Service k2Service = new K2Service(db);
                return Ok(await k2Service.GetTaskBySerialNumber(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/Workflow/StartWorkflow")]
        [Route("Bond/RelatedInfo/AmendCa/Workflow/StartWorkflow")]
        [Route("HirePurchase/RelatedInfo/AmendCa/Workflow/StartWorkflow")]
        [Route("LCDLC/RelatedInfo/AmendCa/Workflow/StartWorkflow")]
        [Route("Leasing/RelatedInfo/AmendCa/Workflow/StartWorkflow")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/Workflow/StartWorkflow")]
        // child
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow")]
        public async Task<ActionResult> StartWorkflow([FromBody] WorkFlowCreditAppRequestView workflowInstance)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(await creditAppRequestTableService.ValidateAndStartWorkflow(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/Workflow/ActionWorkflow")]
        [Route("Bond/RelatedInfo/AmendCa/Workflow/ActionWorkflow")]
        [Route("HirePurchase/RelatedInfo/AmendCa/Workflow/ActionWorkflow")]
        [Route("LCDLC/RelatedInfo/AmendCa/Workflow/ActionWorkflow")]
        [Route("Leasing/RelatedInfo/AmendCa/Workflow/ActionWorkflow")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/Workflow/ActionWorkflow")]
        // child
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow")]
        public async Task<ActionResult> ActionWorkflow([FromBody] WorkFlowCreditAppRequestView workflowInstance)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(await creditAppRequestTableService.ValidateAndActionWorkflow(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Factoring/RelatedInfo/AmendCa/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        // child
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        public ActionResult GetWorkFlowCreditAppRequestById(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetWorkFlowCreditAppRequestById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        // child
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        public ActionResult GetEmployeeTableWithUserDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetEmployeeTableWithUserDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region action history
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("Bond/RelatedInfo/AmendCa/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("LCDLC/RelatedInfo/AmendCa/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("Leasing/RelatedInfo/AmendCa/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        // child
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        // purchase 
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        public ActionResult GetActionHistoryByRefGUID(string refGUID)
        {
            try
            {
                IActionHistoryService actionHistoryService = new ActionHistoryService(db);
                return Ok(actionHistoryService.GetActionHistoryByRefGUID(refGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region release task k2
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/GetReleaseTask")]
        [Route("Bond/RelatedInfo/AmendCa/GetReleaseTask")]
        [Route("HirePurchase/RelatedInfo/AmendCa/GetReleaseTask")]
        [Route("LCDLC/RelatedInfo/AmendCa/GetReleaseTask")]
        [Route("Leasing/RelatedInfo/AmendCa/GetReleaseTask")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/GetReleaseTask")]
        // child
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetReleaseTask")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetReleaseTask")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetReleaseTask")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetReleaseTask")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetReleaseTask")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/GetReleaseTask")]
        // Purchase
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/GetReleaseTask")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/GetReleaseTask")]
        public async Task<ActionResult> GetReleaseTask([FromBody] WorkflowInstance workflowInstance)
        {
            try
            {
                IK2Service k2Service = new K2Service(db);
                return Ok(await k2Service.ReleaseTask(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region Purchase WorkFlow
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Workflow/StartWorkflow")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Workflow/StartWorkflow")]
        public async Task<ActionResult> StartWorkflow([FromBody] WorkflowPurchaseView workflowInstance)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(await purchaseTableService.ValidateAndStartWorkflow(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Workflow/ActionWorkflow")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Workflow/ActionWorkflow")]
        public async Task<ActionResult> ActionWorkflow([FromBody] WorkflowPurchaseView workflowInstance)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(await purchaseTableService.ValidateAndActionWorkflow(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Workflow/ActionWorkflow/GetWorkFlowPurchaseById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Workflow/StartWorkflow/GetWorkFlowPurchaseById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Workflow/ActionWorkflow/GetWorkFlowPurchaseById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Workflow/StartWorkflow/GetWorkFlowPurchaseById/id={id}")]
        public ActionResult GetWorkFlowPurchaseById(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetWorkFlowPurchaseById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Workflow/ActionWorkflow/GetDocumentReasonDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Workflow/StartWorkflow/GetDocumentReasonDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Workflow/ActionWorkflow/GetDocumentReasonDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Workflow/StartWorkflow/GetDocumentReasonDropDown")]
        public ActionResult GetPurchaseWorkflowDropDownItemDocumentReason([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region Project Progress
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress/GetProjectProgressTableList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/GetProjectProgressTableList/ByCompany")]
        public ActionResult GetProjectProgressListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetProjectProgressListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress/GetProjectProgressTableById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/GetProjectProgressTableById/id={id}")]
        public ActionResult GetProjectProgressById(string id)
        {
            try
            {
                IProjectProgressTableService projectProgressTableService = new ProjectProgressTableService(db);
                return Ok(projectProgressTableService.GetProjectProgressTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress/CreateProjectProgressTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/CreateProjectProgressTable")]
        public ActionResult CreateProjectProgress([FromBody] ProjectProgressTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IProjectProgressTableService projectProgressTableService = new ProjectProgressTableService(db, SysTransactionLogService);
                    return Ok(projectProgressTableService.CreateProjectProgressTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress/UpdateProjectProgressTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/UpdateProjectProgressTable")]
        public ActionResult UpdateProjectProgress([FromBody] ProjectProgressTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IProjectProgressTableService projectProgressTableService = new ProjectProgressTableService(db, SysTransactionLogService);
                    return Ok(projectProgressTableService.UpdateProjectProgressTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress/DeleteProjectProgressTable")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/DeleteProjectProgressTable")]
        public ActionResult DeleteProjectProgress([FromBody] RowIdentity parm)
        {
            try
            {
                IProjectProgressTableService projectProgressTableService = new ProjectProgressTableService(db, SysTransactionLogService);
                return Ok(projectProgressTableService.DeleteProjectProgressTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress/GetProjectProgressInitialDataTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/GetProjectProgressInitialDataTable/id={id}")]
        public ActionResult GetProjectProgressInitialData(string id)
        {
            try
            {
                IProjectProgressTableService projectProgressTableService = new ProjectProgressTableService(db);
                IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
                string refId = withdrawalTableRepo.GetWithdrawalTableByIdNoTrackingByAccessLevel(id.StringToGuid()).WithdrawalId;
                string transDate = DateTime.Now.DateToString();
                return Ok(projectProgressTableService.ProjectProgressInitialData(refId, id.ToString(), transDate));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/GetMemoTransInitialDataByProjectPorgress/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/RelatedInfo/MemoTrans/GetMemoTransInitialDataByProjectPorgress/id={id}")]
        public ActionResult GetMemoTransInitialDataByProjectPorgressid(string id)
        {
            try
            {
                IProjectProgressTableService projectProgressTableService = new ProjectProgressTableService(db);
                return Ok(projectProgressTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region InvoiceSettlement
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/InvoiceSettlement/GetInvoiceSettlementDetailList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/GetInvoiceSettlementDetailList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailList/ByCompany")]
        public ActionResult GetInvoiceSettlementDetailListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceSettlementDetailListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/InvoiceSettlement/GetListInvoiceOutstandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/GetListInvoiceOutstandingList/ByCompany")]
        public ActionResult GetInvoiceOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/suspensesettlement/GetListInvoiceOutstandingList/ByCompany")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/suspensesettlement/GetListInvoiceOutstandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/suspensesettlement/GetListInvoiceOutstandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/suspensesettlement/GetListInvoiceOutstandingList/ByCompany")]
        public ActionResult GetSuspenseOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/InvoiceSettlement/GetInvoiceSettlementDetailById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailById/id={id}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/GetInvoiceSettlementDetailById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailById/id={id}")]
        public ActionResult GetInvoiceSettlementDetailById(string id)
        {
            try
            {
                IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
                return Ok(invoiceSettlementDetailService.GetInvoiceSettlementDetailById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/InvoiceSettlement/CreateInvoiceSettlementDetail")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/suspensesettlement/CreateInvoiceSettlementDetail")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/suspensesettlement/CreateInvoiceSettlementDetail")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/CreateInvoiceSettlementDetail")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/suspensesettlement/CreateInvoiceSettlementDetail")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/suspensesettlement/CreateInvoiceSettlementDetail")]
        public ActionResult CreateInvoiceSettlementDetail([FromBody] InvoiceSettlementDetailItemView vwModel)
        {
            try
            {
                IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
                return Ok(invoiceSettlementDetailService.CreateInvoiceSettlementDetail(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/InvoiceSettlement/UpdateInvoiceSettlementDetail")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/suspensesettlement/UpdateInvoiceSettlementDetail")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/suspensesettlement/UpdateInvoiceSettlementDetail")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/UpdateInvoiceSettlementDetail")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/suspensesettlement/UpdateInvoiceSettlementDetail")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/suspensesettlement/UpdateInvoiceSettlementDetail")]
        public ActionResult UpdateInvoiceSettlementDetail([FromBody] InvoiceSettlementDetailItemView vwModel)
        {
            try
            {
                IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
                return Ok(invoiceSettlementDetailService.UpdateInvoiceSettlementDetail(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/InvoiceSettlement/DeleteInvoiceSettlementDetail")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/suspensesettlement/DeleteInvoiceSettlementDetail")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/suspensesettlement/DeleteInvoiceSettlementDetail")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/DeleteInvoiceSettlementDetail")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/suspensesettlement/DeleteInvoiceSettlementDetail")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/suspensesettlement/DeleteInvoiceSettlementDetail")]
        public ActionResult DeletePInvoiceSettlementDetail([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
                return Ok(invoiceSettlementDetailService.DeleteInvoiceSettlementDetail(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/InvoiceSettlement/GetInvoiceSettlementDetailInitialData")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/GetInvoiceSettlementDetailInitialData")]
        public ActionResult GetInvoiceSettlementDetailInitialData([FromBody] InvoiceSettlementDetailItemView vwModel)
        {
            try
            {
                IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
                return Ok(invoiceSettlementDetailService.GetInvoiceSettlementDetailInitialData(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailInitialData")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailInitialData")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailInitialData")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailInitialData")]
        public ActionResult GetSuspenseSettlementDetailInitialData([FromBody] InvoiceSettlementDetailItemView vwModel)
        {
            try
            {
                IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
                return Ok(invoiceSettlementDetailService.GetInvoiceSettlementDetailInitialData(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/InvoiceSettlement/GetCalcSettlePurchaseAmount")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/suspensesettlement/GetCalcSettlePurchaseAmount")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/suspensesettlement/GetCalcSettlePurchaseAmount")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/GetCalcSettlePurchaseAmount")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/suspensesettlement/GetCalcSettlePurchaseAmount")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/suspensesettlement/GetCalcSettlePurchaseAmount")]
        public ActionResult GetCalcSettlePurchaseAmount([FromBody] InvoiceSettlementDetailItemView vwModel)
        {
            try
            {
                IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
                return Ok(invoiceSettlementDetailService.CalcSettlePurchaseAmount(vwModel.InvoiceTableGUID, vwModel.SettleAmount));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/InvoiceSettlement/GetCalcSettleInvoiceAmount/EditWHT={editWHT}")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/suspensesettlement/GetCalcSettleInvoiceAmount/EditWHT={editWHT}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/suspensesettlement/GetCalcSettleInvoiceAmount/EditWHT={editWHT}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/GetCalcSettleInvoiceAmount/EditWHT={editWHT}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/suspensesettlement/GetCalcSettleInvoiceAmount/EditWHT={editWHT}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/suspensesettlement/GetCalcSettleInvoiceAmount/EditWHT={editWHT}")]
        public ActionResult GetCalcSettleInvoiceAmountEditWHT([FromBody] InvoiceSettlementDetailItemView vwModel, bool editWHT)
        {
            try
            {
                IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
                return Ok(invoiceSettlementDetailService.GetCalcSettleInvoiceAmount(vwModel, editWHT));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/InvoiceSettlement/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/suspensesettlement/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/suspensesettlement/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
        public ActionResult GetPurchaseTableAccessModeByInvoiceSettlement(string purchaseId, string isWorkflowMode)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPurchaseTableAccessMode(purchaseId, isWorkflowMode));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/InvoiceSettlement/GetWithdrawalTableAcessMode/withdrawalId={withdrawalId}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/suspensesettlement/GetWithdrawalTableAcessMode/withdrawalId={withdrawalId}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/suspensesettlement/GetWithdrawalTableAcessMode/withdrawalId={withdrawalId}")]
        public ActionResult GetWithdrawalTableAcessModeByInvoiceSettlement(string withdrawalId)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetWithdrawalTableAccessMode(withdrawalId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/InvoiceSettlement/GetInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/suspensesettlement/GetInvoiceTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/suspensesettlement/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/suspensesettlement/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/suspensesettlement/GetInvoiceTableDropDown")]
        public ActionResult GetInvoiceSettlementInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/InvoiceSettlement/GetInvoiceTypeDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/suspensesettlement/GetInvoiceTypeDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/RelatedInfo/suspensesettlement/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/suspensesettlement/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/WithdrawalTableTermExtension/RelatedInfo/suspensesettlement/GetInvoiceTypeDropDown")]
        public ActionResult GetInvoiceSettlementInvoiceTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/InvoiceSettlement/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/GetBuyerInvoiceTableDropDown")]
        public ActionResult GetInvoiceSettlementBuyerInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/InvoiceSettlement/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/GetBuyerAgreementTableDropDown")]
        public ActionResult GetInvoiceSettlementBuyerAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/InvoiceSettlement/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/InvoiceSettlement/GetBuyerTableDropDown")]
        public ActionResult GetInvoiceSettlementBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion InvoiceSettlement
        #region pdc
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetWithdrawalTableAccessMode/id={id}")]
        public ActionResult GetWithdrawalTableAccessModeByPdc(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetWithdrawalTableAccessMode(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetChequeTableInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/pdc/GetChequeTableInitialData/id={id}")]
        public ActionResult GetChequeTableInitialDataByWithdrawal(string id)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                var temp_chequeTableService = chequeTableService.GetChequeTableInitialDataByWithdrawal(id);
                return Ok(temp_chequeTableService);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/pdc/GetChequeTableByCustomerBuyerReplacedDropDown")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetChequeTableByCustomerBuyerReplacedDropDown")]
        //pdc
        [Route("Factoring/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetChequeTableByCustomerBuyerReplacedDropDown")]
        [Route("Bond/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetChequeTableByCustomerBuyerReplacedDropDown")]
        [Route("HirePurchase/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetChequeTableByCustomerBuyerReplacedDropDown")]
        [Route("Leasing/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetChequeTableByCustomerBuyerReplacedDropDown")]
        [Route("Lcdlc/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetChequeTableByCustomerBuyerReplacedDropDown")]
        [Route("ProjectFinance/RelatedInfo/purchasetablerollbill/RelatedInfo/pdc/GetChequeTableByCustomerBuyerReplacedDropDown")]

        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/pdc/GetChequeTableByCustomerBuyerReplacedDropDown")]

        public ActionResult GetChequeRefPDCDropdown([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDropDownItemChequeTableByCustomerBuyerReplaced(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Manage Function
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/pdc/GetDeposittStatusValidation")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetDeposittStatusValidation")]
        public ActionResult GetDeposittStatusValidation([FromBody] ChequeTableItemView view)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDeposittStatusValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        // complate
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/pdc/GetDocumentStatusValidation")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetDocumentStatusValidation")]
        public ActionResult GetDocumentStatusComplateValidation([FromBody] ChequeTableItemView view)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDocumentStatusValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        //Bounce
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/pdc/GetBounceStatusValidation")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetBounceStatusValidation")]
        public ActionResult GetBounceStatusValidation([FromBody] ChequeTableItemView view)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetBouncetStatusValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        //Replace
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/pdc/GetReplaceStatusValidation")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetReplaceStatusValidation")]
        public ActionResult GetReplaceStatusValidation([FromBody] ChequeTableItemView view)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetReplaceStatusValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/pdc/GetreturnStatusValidation")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetreturnStatusValidation")]
        public ActionResult GetReturnStatusValidation([FromBody] ChequeTableItemView view)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetReturnStatusValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/pdc/GetcancelStatusValidation")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/pdc/GetcancelStatusValidation")]
        public ActionResult GetCancelStatusValidation([FromBody] ChequeTableItemView view)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetCancelStatusValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion pdc
        #region Attachment
        [HttpPost]
        //creditappLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        //amendca
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        //Verification
        [Route("Factoring/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Bond/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        //Withdrawal
        [Route("Projectfinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/ProjectProgress/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        //closecreditlimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        //termextension
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        //RollBill
        [Route("Factoring/RelatedInfo/purchasetablerollbill/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        public ActionResult CreditAppTableGetAttachmentListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        //amendca
        [Route("Projectfinance/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        //Verification
        [Route("Projectfinance/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Factoring/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Bond/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        //Withdrawal
        [Route("Projectfinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        //closecreditlimit
        [Route("Projectfinance/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        //projectprogress
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        //RollBill
        [Route("Factoring/RelatedInfo/purchasetablerollbill/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> CreditAppTableGetAttachmentById(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/CreateAttachment")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/CreateAttachment")]
        //amendca
        [Route("Lcdlc/RelatedInfo/AmendCa/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Attachment/CreateAttachment")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Projectfinance/RelatedInfo/AmendCa/RelatedInfo/Attachment/CreateAttachment")]
        //verifacation
        [Route("Projectfinance/RelatedInfo/VerificationTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Factoring/RelatedInfo/VerificationTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Bond/RelatedInfo/VerificationTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Leasing/RelatedInfo/VerificationTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Lcdlc/RelatedInfo/VerificationTable/RelatedInfo/Attachment/CreateAttachment")]
        //Withdrawal
        [Route("Projectfinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/Attachment/CreateAttachment")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/Attachment/CreateAttachment")]
        //CloseCreditLimit
        [Route("Projectfinance/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/CreateAttachment")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Lcdlc/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/CreateAttachment")]
        //projectprogress
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/RelatedInfo/attachment/CreateAttachment")]
        //RollBill
        [Route("Factoring/RelatedInfo/purchasetablerollbill/RelatedInfo/Attachment/CreateAttachment")]
        public ActionResult CreditAppTableCreateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/UpdateAttachment")]
        //amendca
        [Route("Projectfinance/RelatedInfo/AmendCa/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/RelatedInfo/AmendCa/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Attachment/UpdateAttachment")]
        //verifacation
        [Route("Projectfinance/RelatedInfo/VerificationTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/RelatedInfo/VerificationTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Bond/RelatedInfo/VerificationTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/RelatedInfo/VerificationTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/RelatedInfo/VerificationTable/RelatedInfo/Attachment/UpdateAttachment")]
        //Withdrawal
        [Route("Projectfinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/Attachment/UpdateAttachment")]
        // CloseCreditLimit
        [Route("Projectfinance/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/UpdateAttachment")]
        //projectprogress
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/RelatedInfo/attachment/UpdateAttachment")]
        //RollBill
        [Route("Factoring/RelatedInfo/purchasetablerollbill/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult CreditAppTableUpdateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/DeleteAttachment")]
        //amendca
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/RelatedInfo/AmendCa/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Projectfinance/RelatedInfo/AmendCa/RelatedInfo/Attachment/DeleteAttachment")]
        //verifacation
        [Route("Projectfinance/RelatedInfo/VerificationTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/RelatedInfo/VerificationTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Bond/RelatedInfo/VerificationTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/RelatedInfo/VerificationTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/RelatedInfo/VerificationTable/RelatedInfo/Attachment/DeleteAttachment")]
        //Withdrawal
        [Route("Projectfinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/Attachment/DeleteAttachment")]
        //CloseCreditLimit
        [Route("Projectfinance/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/DeleteAttachment")]
        //projectprogress
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/RelatedInfo/attachment/DeleteAttachment")]
        //RollBill
        [Route("Factoring/RelatedInfo/purchasetablerollbill/RelatedInfo/Attachment/DeleteAttachment")]
        public ActionResult CreditAppTableDeleteAttachment([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Attachment/GetAttachmentRefId")]
        //amendca
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Projectfinance/RelatedInfo/AmendCa/RelatedInfo/Attachment/GetAttachmentRefId")]

        //verifacation
        [Route("Projectfinance/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Bond/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/RelatedInfo/VerificationTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        //Withdrawal
        [Route("Projectfinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/Attachment/GetAttachmentRefId")]
        //CloseCreditLimit
        [Route("Projectfinance/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/RelatedInfo/CloseCreditLimit/RelatedInfo/Attachment/GetAttachmentRefId")]
        //projectprogress
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProjectProgress/RelatedInfo/attachment/GetAttachmentRefId")]
        //RollBill
        [Route("Factoring/RelatedInfo/purchasetablerollbill/RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult CreditAppTableGetAttachmentRefId([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Attachment
        #region InquiryCustomerBuyerCreditOutstanding
        [HttpPost]
        [Route("Factoring/RelatedInfo/customerbuyercreditoutstanding/GetInquiryCALineOutstandingByCAList/ByCompany")]
        [Route("Bond/RelatedInfo/customerbuyercreditoutstanding/GetInquiryCALineOutstandingByCAList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/customerbuyercreditoutstanding/GetInquiryCALineOutstandingByCAList/ByCompany")]
        [Route("LCDLC/RelatedInfo/customerbuyercreditoutstanding/GetInquiryCALineOutstandingByCAList/ByCompany")]
        [Route("Leasing/RelatedInfo/customerbuyercreditoutstanding/GetInquiryCALineOutstandingByCAList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/customerbuyercreditoutstanding/GetInquiryCALineOutstandingByCAList/ByCompany")]
        public ActionResult GetInquiryCALineOutstandingByCAToList([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetCALineOutstandingByCA(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Retentions OutStanding
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCAReqRetentionOutstandingList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCAReqRetentionOutstandingList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCAReqRetentionOutstandingList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCAReqRetentionOutstandingList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCAReqRetentionOutstandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCAReqRetentionOutstandingList/ByCompany")]
        public ActionResult GetRetentionOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetRetentionOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCustomerTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCustomerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCustomerTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCustomerTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCustomerTableDropDown")]
        public ActionResult GetRetentionOutstandingCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCreditAppTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCreditAppTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCreditAppTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCreditAppTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/RetentionOutstanding/GetCreditAppTableDropDown")]
        public ActionResult GetRetentionOutstandingCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion  Retentions OutStanding
        #region AssignmentAgreementOutstanding
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        #endregion CloseCreditLimit
        public ActionResult GetAssignmentOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAssignmentOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        #endregion CloseCreditLimit
        public ActionResult GetAssignmentOutstandingCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        #endregion CloseCreditLimit
        public ActionResult GetAssignmentOutstandingBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        #endregion CloseCreditLimit
        public ActionResult GetAssignmentOutstandingAssignmentAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AssignmentAgreementOutstanding        
        #region CreditOutStanding
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        #endregion CloseCreditLimit
        public ActionResult GetCreditOutStandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditOutStandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        #endregion CloseCreditLimit
        public ActionResult GetCreditOutStandingCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        #endregion CloseCreditLimit
        public ActionResult GetCreditOutStandingCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        #endregion CloseCreditLimit
        public ActionResult GetCreditOutStandingCreditLimitTypeDropDownn([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditLimitType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion  CreditOutStanding
        #region Inquiry Roll Bill PurchaseLine
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchaselinepurchase-child/RelatedInfo/inquiryrollbillpurchaseline/GetInquiryRollBillPurchaseLineList/ByCompany")]
        public ActionResult GetInquiryRollBillPurchaseLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInquiryRollBillPurchaseLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/purchaselinepurchase-child/relatedinfo/inquiryrollbillpurchaseline/GetInquiryRollBillPurchaseLineById/id={id}")]
        public ActionResult GetInquiryRollBillPurchaseLineById(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetInquiryRollBillPurchaseLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Inquiry Roll Bill PurchaseLine
        #region update closing result 
        [HttpGet]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/GetUpdateClosingResultById/id={id}")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/GetUpdateClosingResultById/id={id}")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/GetUpdateClosingResultById/id={id}")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/GetUpdateClosingResultById/id={id}")]
        [Route("Lcdlc/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/GetUpdateClosingResultById/id={id}")]
        [Route("Projectfinance/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/GetUpdateClosingResultById/id={id}")]
        public ActionResult GetUpdateClosingResult(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetUpdateClosingResultById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/GetDocumentReasonDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/GetDocumentReasonDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/GetDocumentReasonDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/GetDocumentReasonDropDown")]
        [Route("Lcdlc/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/GetDocumentReasonDropDown")]
        [Route("Projectfinance/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/GetDocumentReasonDropDown")]
        public ActionResult GetDropDownItemDocumentReason([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/UpdateClosingResult")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/UpdateClosingResult")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/UpdateClosingResult")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/UpdateClosingResult")]
        [Route("Lcdlc/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/UpdateClosingResult")]
        [Route("Projectfinance/RelatedInfo/CloseCreditLimit/Function/UpdateClosingResult/UpdateClosingResult")]
        public ActionResult UpdateClosingResult([FromBody] UpdateClosingResultView view)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.UpdateClosingResult(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region productsettlement
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchaselinepurchase-child/RelatedInfo/productsettlement/GetProductSettledTransList/ByCompany")]
        [Route("Factoring/RelatedInfo/purchaselinerollbill-child/RelatedInfo/productsettlement/GetProductSettledTransList/ByCompany")]
        public ActionResult GetProductSettlementListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetProductSettledTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProductSettlement/GetProductSettledTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProductSettlement/GetProductSettledTransList/ByCompany")]
        public ActionResult GetProductSettlementListByCompanyWithdrawal([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetProductSettledTransListvwByWithdrawal(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/purchaselinepurchase-child/RelatedInfo/productsettlement/GetProductSettledTransById/id={id}")]
        [Route("Factoring/RelatedInfo/purchaselinerollbill-child/RelatedInfo/productsettlement/GetProductSettledTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProductSettlement/GetProductSettledTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProductSettlement/GetProductSettledTransById/id={id}")]
        public ActionResult GetProductSettledTransById(string id)
        {
            try
            {
                IProductSettledTransService productSettledTransService = new ProductSettledTransService(db);
                return Ok(productSettledTransService.GetProductSettledTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/purchaselinepurchase-child/RelatedInfo/productsettlement/GetPurchaseTablePurchaseAccessMode/id={id}")]
        [Route("Factoring/RelatedInfo/purchaselinerollbill-child/RelatedInfo/productsettlement/GetPurchaseTablePurchaseAccessMode/id={id}")]
        public ActionResult GetPurchaseTablePurchaseAccessMode(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPurchaseTablePurchaseAccessMode(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProductSettlement/GetWithdawalTableAccessMode/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProductSettlement/GetWithdawalTableAccessMode/id={id}")]
        public ActionResult GetReceiptTempTableAccessMode(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetWithdrawalTableAccessMode(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchaselinepurchase-child/RelatedInfo/productsettlement/GetInvoiceSettlementDetailDropDown")]
        [Route("Factoring/RelatedInfo/purchaselinerollbill-child/RelatedInfo/productsettlement/GetInvoiceSettlementDetailDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProductSettlement/GetInvoiceSettlementDetailDropDown")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/withdrawaltabletermextension/RelatedInfo/ProductSettlement/GetInvoiceSettlementDetailDropDown")]
        public ActionResult GetProductSettlementInvoiceSettlementDetailDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceSettlementDetail(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Credit application request assignment
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        #endregion AmendCaLine
        public ActionResult GetCreditAppReqAssignmentListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppReqAssignmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        #endregion AmendCaLine
        public ActionResult GetCreditAppReqAssignmentById(string id)
        {
            try
            {
                ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db);
                return Ok(creditAppReqAssignmentService.GetCreditAppReqAssignmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        #endregion AmendCaLine
        public ActionResult CreateCreditAppReqAssignment([FromBody] CreditAppReqAssignmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db, SysTransactionLogService);
                    return Ok(creditAppReqAssignmentService.CreateCreditAppReqAssignment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        #endregion AmendCaLine
        public ActionResult UpdateCreditAppReqAssignment([FromBody] CreditAppReqAssignmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db, SysTransactionLogService);
                    return Ok(creditAppReqAssignmentService.UpdateCreditAppReqAssignment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        #endregion AmendCaLine

        public ActionResult DeleteCreditAppReqAssignment([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db, SysTransactionLogService);
                return Ok(creditAppReqAssignmentService.DeleteCreditAppReqAssignment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        #endregion AmendCaLine
        public ActionResult GetAssignmentAgreementOutstanding(string refGUID, int reftype)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementOutstanding(new Guid(refGUID), reftype));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/AssignmentGetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/AssignmentGetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        #endregion AmendCaLine
        public ActionResult GetAssignmentAccessModeByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region ExistingAssignmentAgreement
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        #endregion AmendCaLine
        public ActionResult GetExistingAssignmentAgreementListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementOutstanding(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ExistingAssignmentAgreement
        #region DropDown
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        #endregion AmendCaLine
        public ActionResult GetAssignmentDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDropDownItemAssignmentAgreementStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        #endregion AmendCaLine
        public ActionResult GetAssignmentBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        #endregion AmendCaLine
        public ActionResult GetAssignmentAssignmentMethodDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentMethod(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        #endregion AmendCaLine
        public ActionResult GetAssignmentBuyerAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementLineTableByBuyerAndCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        #endregion AmendCaLine
        public ActionResult GetAssignmentAgreementeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDropDownItemAssignmentAgreementByCustomerAndStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        #endregion AmendCaLine

        public ActionResult GetAssignmentAssignmentAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #endregion Credit application request assignment

        #region RetentionTransaction
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        [Route("Bond/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        [Route("Lcdlc/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        [Route("Projectfinance/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        public ActionResult GetCARetentionTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetRetentionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        [Route("Bond/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        [Route("Leasing/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        [Route("Lcdlc/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        [Route("Projectfinance/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        public ActionResult GetCARetentionTransById(string id)
        {
            try
            {
                IRetentionTransService retentionTransService = new RetentionTransService(db);
                return Ok(retentionTransService.GetRetentionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region dropdow

        #endregion
        #endregion

        #region Multiple Selection
        [HttpPost]
        [Route("Factoring/GetMultipleAction")]
        public ActionResult MultipleAction([FromBody] RowIdentity[] vmModel)
        {
            try
            {
                return Ok();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Multiple Selection

        #region copyfinancialstatementtrans
        [HttpPost]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/Function/copyfinancialstatementtrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/Function/copyfinancialstatementtrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/Function/copyfinancialstatementtrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/Function/copyfinancialstatementtrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/Function/copyfinancialstatementtrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/Function/copyfinancialstatementtrans/GetCopyFinancialStatementTransByRefTypeValidation")]

        public ActionResult GetCopyFinancialStatementTransByRefTypeValidationByCustomerCARL([FromBody] CopyFinancialStatementTransView param)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                return Ok(financialStatementTransService.GetCopyFinancialStatementTransByRefTypeValidationCARL(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("Lcdlc/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("Projectfinance/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        public ActionResult GetCopyFinancialStatementTransByRefTypeValidationByCustomerCART([FromBody] CopyFinancialStatementTransView param)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                return Ok(financialStatementTransService.GetCopyFinancialStatementTransByRefTypeValidationCART(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/Function/copyfinancialstatementtrans")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/Function/copyfinancialstatementtrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/Function/copyfinancialstatementtrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/Function/copyfinancialstatementtrans")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/Function/copyfinancialstatementtrans")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/Function/copyfinancialstatementtrans")]
        public ActionResult CopyFinancialStatementTrans([FromBody] CopyFinancialStatementTransView param)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.CopyFinancialStatementTransByCARL(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("Lcdlc/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("Projectfinance/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]

        public ActionResult CopyFinancialStatementTransByAmendCa([FromBody] CopyFinancialStatementTransView param)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.CopyFinancialStatementTransByCART(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CopyTaxReportTrans
        [HttpPost]

        [Route("Bond/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefType")]
        [Route("Factoring/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefType")]
        [Route("HirePurchase/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefType")]
        [Route("Leasing/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefType")]
        [Route("Lcdlc/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefType")]
        [Route("Projectfinance/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefType")]

        public ActionResult GetCopyTaxReportTransByRefType([FromBody] RefTypeModel param)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db);
                return Ok(taxReportTransService.GetCopyTaxReportTransByRefType(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Bond/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefTypeValidation")]
        [Route("Projectfinance/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefTypeValidation")]
        [Route("Lcdlc/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefTypeValidation")]
        [Route("Leasing/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefTypeValidation")]
        [Route("HirePurchase/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefTypeValidation")]
        [Route("Factoring/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefTypeValidation")]

        public ActionResult GetCopyTaxReportTransByRefTypeValidation([FromBody] CopyTaxReportTransView param)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db);
                return Ok(taxReportTransService.GetCopyTaxReportTransByRefTypeValidation(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Bond/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans")]
        [Route("Projectfinance/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans")]
        [Route("Lcdlc/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans")]
        [Route("Leasing/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans")]
        [Route("Factoring/RelatedInfo/AmendCA/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans")]

        public ActionResult CopyTaxReportTrans([FromBody] CopyTaxReportTransView param)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db);
                return Ok(taxReportTransService.CopyTaxReportTrans(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region report PurchaseTable
        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/Report/PrintPurchase/GetPrintPurchaseById/id={id}")]
        public ActionResult GetPrintPurchaseById(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPrintPurchaseById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/Report/PrintPurchase/RenderReport")]
        public ActionResult PrintPurchase([FromBody] PrintPurchaseReporttView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Report/printpurchaserollbill/GetPrintPurchaseRollbillById/id={id}")]
        public ActionResult GetPrintPurchaseRollbillById(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetPrintPurchaseRollbillById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTableRollBill/Report/printpurchaserollbill/RenderReport")]
        public ActionResult PrintPurchaseRollbill([FromBody] PrintPurchaseRollbillReportView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region report WithdrawalTable
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/Report/PrintWithdrawalTable/GetPrintWithdrawalById/id={id}")]
        public ActionResult GetPrintWithdrawalById(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetPrintWithdrawalTableById(id));

            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/Report/PrintWithdrawalTable/RenderReport")]
        public ActionResult PrintWithdrawal([FromBody] PrintWithdrawalTableReporttView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        // PrintWithdrawalTermExtension
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/Report/PrintWithdrawalTermExtension/GetPrintWithdrawalTermExtensionById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/WithdrawalTableTermExtension/Report/PrintWithdrawalTermExtension/GetPrintWithdrawalTermExtensionById/id={id}")]
        public ActionResult GetPrintWithdrawalTermExtensionById(string id)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetPrintPrintWithdrawalTermExtensionById(id));

            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/Report/PrintWithdrawalTermExtension/RenderReport")]
        [Route("ProjectFinance/RelatedInfo/withdrawaltablewithdrawal/RelatedInfo/WithdrawalTableTermExtension/Report/PrintWithdrawalTermExtension/RenderReport")]
        public ActionResult PrintWithdrawalTermExtension([FromBody] PrintWithdrawalTermExtensionReporttView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CaBuyerCreditOutstanding
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        #endregion CloseCreditLimit
        public ActionResult GetCAReqBuyerCreditOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCAReqBuyerCreditOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown 
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        #endregion CloseCreditLimit
        public ActionResult CaBuyerCreditOutstandingGetBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        #endregion CloseCreditLimit
        public ActionResult CaBuyerCreditOutstandingGetAssignmentAssignmentMethodDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentMethod(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        #endregion CloseCreditLimit
        public ActionResult CaBuyerCreditOutstandingGetBillingResponsibleByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBillingResponsibleBy(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        #endregion CloseCreditLimit
        public ActionResult CaBuyerCreditOutstandingGetMethodOfPaymentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion CaBuyerCreditOutstanding
        #region InvoiceTable
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/Pdc/GetReceiptTempTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Withdrawallinewithdrawal-child/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        public ActionResult GetReceiptTempTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemReceiptTempTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        // AddInvoiceTable RelatedInfo
        [HttpPost]
        [Route("Factoring/RelatedInfo/PurchaseTablePurchase/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLinePurchase-Child/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Factoring/RelatedInfo/PurchaseLineRollBill-Child/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDownInvoiceTable([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region PrintBookDocTrans
        [HttpPost]
        #region AmendCA
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        #endregion AmendCA
        #region AmendCALine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        #endregion AmendCALine
        public ActionResult GetPrintBookDocTransValidationByAmendCA([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintBookDocTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        #region AmendCA
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        #endregion AmendCA
        #region AmendCALine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        #endregion AmendCALine
        public async Task<ActionResult> PrintBookmarkDocumentTransactionByAmendCA([FromBody] PrintBookDocTransParm printBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CopyBuyerAgreementTrans
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransById/id={id}")]
        public ActionResult<CopyBuyerAgreementTransView> GetCopyBuyerAgreementTransById(string id)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                return Ok(buyerAgreementTransService.GetCopyBuyerAgreementTransById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableByBuyerDropdown([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                return Ok(buyerAgreementTransService.GetBuyerAgreementByBuyerDropdown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransByRefTypeValidation")]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransByRefTypeValidation")]
        public ActionResult GetCopyBuyerAgreementTransByRefTypeValidation([FromBody] CopyBuyerAgreementTransView param)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                return Ok(buyerAgreementTransService.GetCopyBuyerAgreementTransByRefTypeValidation(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans")]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans")]
        public ActionResult CopyBuyerAgreementTrans([FromBody] CopyBuyerAgreementTransView param)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                return Ok(buyerAgreementTransService.CopyBuyerAgreementTrans(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion CopyBuyerAgreementTrans
        #region UpdateStatusAmendBuyerInfo
        [HttpPost]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/UpdateStatusAmendBuyerInfo")]
        public ActionResult UpdateStatusAmendBuyerInfo([FromBody] UpdateStatusAmendBuyerInfoView vmModel)
        {
            try
            {
                ICreditAppRequestLineAmendService creditAppRequestLineAmendService = new CreditAppRequestLineAmendService(db, SysTransactionLogService);
                return Ok(creditAppRequestLineAmendService.UpdateStatusAmendBuyerInfo(vmModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion UpdateStatusAmendBuyerInfo
    }
}