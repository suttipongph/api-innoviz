﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class DocumentController : BaseControllerV2
	{

		public DocumentController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetDocumentProcessList/ByCompany")]
		public ActionResult GetDocumentProcessListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetDocumentProcessListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetDocumentProcessById/id={id}")]
		public ActionResult GetDocumentProcessById(string id)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db);
				return Ok(documentService.GetDocumentProcessById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateDocumentProcess")]
		public ActionResult CreateDocumentProcess([FromBody] DocumentProcessItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentService documentService = new DocumentService(db, SysTransactionLogService);
					return Ok(documentService.CreateDocumentProcess(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateDocumentProcess")]
		public ActionResult UpdateDocumentProcess([FromBody] DocumentProcessItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentService documentService = new DocumentService(db, SysTransactionLogService);
					return Ok(documentService.UpdateDocumentProcess(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteDocumentProcess")]
		public ActionResult DeleteDocumentProcess([FromBody] RowIdentity parm)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db, SysTransactionLogService);
				return Ok(documentService.DeleteDocumentProcess(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetDocumentStatusList/ByCompany")]
		public ActionResult GetDocumentStatusListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetDocumentStatusListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetDocumentStatusById/id={id}")]
		public ActionResult GetDocumentStatusById(string id)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db);
				return Ok(documentService.GetDocumentStatusById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateDocumentStatus")]
		public ActionResult CreateDocumentStatus([FromBody] DocumentStatusItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentService documentService = new DocumentService(db, SysTransactionLogService);
					return Ok(documentService.CreateDocumentStatus(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateDocumentStatus")]
		public ActionResult UpdateDocumentStatus([FromBody] DocumentStatusItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentService documentService = new DocumentService(db, SysTransactionLogService);
					return Ok(documentService.UpdateDocumentStatus(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteDocumentStatus")]
		public ActionResult DeleteDocumentStatus([FromBody] RowIdentity parm)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db, SysTransactionLogService);
				return Ok(documentService.DeleteDocumentStatus(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetInitialDataByProcess/id={id}")]
		public ActionResult GetInitialDataByProcess(string id)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db);
				return Ok(documentService.GetInitialDataByProcess(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
		#region Dropdown
		#endregion
	}
}
