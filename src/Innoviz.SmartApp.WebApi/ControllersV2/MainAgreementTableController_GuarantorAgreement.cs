﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public partial class MainAgreementTableController : BaseControllerV2
    {
        [HttpPost]
        //Gurantor Copy Function
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditAppTableDropDown")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditAppTableDropDown")]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditAppTableDropDown")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditAppTableDropDown")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditAppTableDropDown")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditAppTableDropDown")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditAppTableDropDown")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditAppTableDropDown")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditAppTableDropDown")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDownByGuarantorAgreementTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("Bond/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByGuarantorAgreementTable/id={id}")]
        public ActionResult GetAccessModeByGuarantorAgreementTable(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);

                return Ok(guarantorAgreementTableService.GetAccessModeByGuarantorAgreementTableBookmarkDocumentTrans(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Memo
        [HttpPost]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult GetMemoListByCompanyByGuarantorAgreementTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetMemoByIdByGuarantorAgreementTable(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateMemoByGuarantorAgreementTable([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateMemoByGuarantorAgreementTable([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult DeleteMemoByGuarantorAgreementTable([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByGuarantorAgreement/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByGuarantorAgreement/id={id}")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByGuarantorAgreement/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByGuarantorAgreement/id={id}")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByGuarantorAgreement/id={id}")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByGuarantorAgreement/id={id}")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByGuarantorAgreement/id={id}")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByGuarantorAgreement/id={id}")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByGuarantorAgreement/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialDataByGuarantorAgreement/id={id}")]
        public ActionResult GetMemoTransInitialDataByGuarantorAgreement(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetMemoTransInitialDataByGuarantorAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Memo
        #region BookmarkDocumentTrans
        [HttpPost]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]

        public ActionResult GetBookmarkDocumentTransListByCompanyByGuarantorAgreementTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBookmarkDocumentTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        public ActionResult GetBookmarkDocumentTransByIdByGuarantorAgreementTable(string id)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(BookmarkDocumentTransService.GetBookmarkDocumentTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        public ActionResult CreateBookmarkDocumentTransByGuarantorAgreementTable([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.CreateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        public ActionResult UpdateBookmarkDocumentTransByGuarantorAgreementTable([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.UpdateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        public ActionResult DeleteBookmarkDocumentTransByGuarantorAgreementTable([FromBody] RowIdentity parm)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                return Ok(BookmarkDocumentTransService.DeleteBookmarkDocumentTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        public ActionResult GetBookmarkDocumentTransInitialDataByGuarantorAgreementTable(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetBookmarkDocumentTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        public ActionResult GetBookmarkDocumentTransInitialDataGuarantorAgreementByGuarantorAgreementTable(string id)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                return Ok(guarantorAgreementTableService.GetBookmarkDocumentTransInitialDataByGuarantorAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        public ActionResult GetGuarantorAgreementTableBookmarkDocumentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
                return Ok(bookmarkDocumentService.GetDropDownItemBookmarkDocumentByGuarantorAgreement(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        public ActionResult GetDocumentTemplateTableDropDownByDocumentTypeByGuarantorAgreementTable([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
                return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        public ActionResult GetDocumentTemplateTableDropDownByGuarantorAgreementTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]


        public ActionResult GetBookmarkDocumentTransDocumentStatusDropDownByGuarantorAgreementTable([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetDropDownItemBookMarkDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region PrintBookDocTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        public ActionResult GetPrintBookDocTransValidationByGuarantorAgreementTable([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintBookDocTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransactionByGuarantorAgreementTable([FromBody] PrintBookDocTransParm printBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion BookmarkDocumentTrans
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]

        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetEmployeeTableDropDown")]
        public ActionResult GetAgreementTableInfoEmployeeTableDropDownByGuarantorAgreementTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AgreementTableInfo
        #region GuarantorAgreementTable
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableById/id={id}")]
        public ActionResult GetGuarantorAgreementTableById(string id)
        {
            try
            {
                IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(GuarantorAgreementTableService.GetGuarantorAgreementTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementTable")]
        public ActionResult CreateGuarantorAgreementTable([FromBody] GuarantorAgreementTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                    return Ok(GuarantorAgreementTableService.CreateGuarantorAgreementTable(vwModel));

                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementTable")]
        public ActionResult UpdateGuarantorAgreementTable([FromBody] GuarantorAgreementTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                    return Ok(GuarantorAgreementTableService.UpdateGuarantorAgreementTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementTable")]
        public ActionResult DeleteGuarantorAgreementTable([FromBody] RowIdentity parm)
        {
            try
            {
                IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                return Ok(GuarantorAgreementTableService.DeleteGuarantorAgreementTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableInitialData/id={id}")]

        public ActionResult GetGuarantorAgreementTableInitialData(string id)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetGuarantorAgreementTableInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementTableList/ByCompany")]
        public ActionResult GetGuarantorAgreementTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetGuarantorAgreementTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualNumberSeq/productType={productType}")]
        public ActionResult ValidateIsManualNumberSeq(int productType)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.IsManualByGuarantorRequest(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualInternalNumberSeq/productType={productType}")]
        public ActionResult ValidateIsManualInternalNumberSeq(int productType)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.IsManualByInternalGuarantorRequest(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/ValidateIsManualGuarantorNumberSeq/productType={productType}")]

        public ActionResult ValidateIsManualGuarantorNumberSeq(int productType)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.ValidateIsManualGuarantorNumberSeq(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetVisibleManageMenu/id={id}")]
        public ActionResult GetVisibleManageMenuByGuarantorAgreementTable(string id)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetVisibleManageMenu(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Copy guarantor from CA
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCopyGuarantorFromCAById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCopyGuarantorFromCAById/id={id}")]

        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCopyGuarantorFromCAById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCopyGuarantorFromCAById/id={id}")]

        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCopyGuarantorFromCAById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCopyGuarantorFromCAById/id={id}")]

        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCopyGuarantorFromCAById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCopyGuarantorFromCAById/id={id}")]

        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCopyGuarantorFromCAById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCopyGuarantorFromCAById/id={id}")]

        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCopyGuarantorFromCAById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCopyGuarantorFromCAById/id={id}")]

        public ActionResult GetCopyGuarantorFromCAById(string id)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetCopyGuarantorFromCAById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditApplicationRequestTableDropDown")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditApplicationRequestTableDropDown")]

        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditApplicationRequestTableDropDown")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditApplicationRequestTableDropDown")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditApplicationRequestTableDropDown")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditApplicationRequestTableDropDown")]

        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditApplicationRequestTableDropDown")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditApplicationRequestTableDropDown")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditApplicationRequestTableDropDown")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditApplicationRequestTableDropDown")]

        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditApplicationRequestTableDropDown")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/GetCreditApplicationRequestTableDropDown")]
        public ActionResult GetCreditApplicationRequestTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.CreditAppRequestDropdownByDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/CreateCopyGuarantorFromCA")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/CreateCopyGuarantorFromCA")]

        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/CreateCopyGuarantorFromCA")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/CreateCopyGuarantorFromCA")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/CreateCopyGuarantorFromCA")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/CreateCopyGuarantorFromCA")]

        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/CreateCopyGuarantorFromCA")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/CreateCopyGuarantorFromCA")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/CreateCopyGuarantorFromCA")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/CreateCopyGuarantorFromCA")]

        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/CreateCopyGuarantorFromCA")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/Function/copyguarantorfromca/CreateCopyGuarantorFromCA")]
        public ActionResult CreateCopyGuarantorFromCA([FromBody] GuarantorAgreementLineItemView vwModel)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.CreateCopyGuarantorFromCA(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/ManageAgreement")]
        public ActionResult ManageAgreementByGuarantorAgreementTable([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.ManageAgreement(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        public ActionResult GetManageAgreementValidationByGuarantorAgreementTable([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        public ActionResult GetManageAgreementByRefTypeByGuarantorAgreementTable([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementByRefType(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetDocumentReasonDropDown")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetDocumentReasonDropDown")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetDocumentReasonDropDown")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetDocumentReasonDropDown")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetDocumentReasonDropDown")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetDocumentReasonDropDown")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetDocumentReasonDropDown")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetDocumentReasonDropDown")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetDocumentReasonDropDown")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetDocumentReasonDropDown")]

        public ActionResult GetDocumentReasonDropDownByManageAgreementByGuarantorAgreementTable([FromBody] SearchParameter search)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetDropDownItemGuarantorAgreementTable(search));


            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GuarantorAgreementTable
        #region GuarantorAgreementLine
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineList/ByCompany")]
        public ActionResult GetGuarantorAgreementLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetGuarantorAgreementLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineById/id={id}")]
        public ActionResult GetGuarantorAgreementLineById(string id)
        {
            try
            {
                IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db);
                var result = GuarantorAgreementTableService.GetGuarantorAgreementLineById(id);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLine")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLine")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLine")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLine")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLine")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLine")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLine")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLine")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLine")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLine")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLine")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLine")]
        public ActionResult CreateGuarantorAgreementLine([FromBody] GuarantorAgreementLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                    return Ok(GuarantorAgreementTableService.CreateGuarantorAgreementLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLine")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLine")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLine")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLine")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLine")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLine")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLine")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLine")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLine")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLine")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLine")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLine")]
        public ActionResult UpdateGuarantorAgreementLine([FromBody] GuarantorAgreementLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                    return Ok(GuarantorAgreementTableService.UpdateGuarantorAgreementLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLine")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLine")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLine")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLine")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLine")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLine")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLine")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLine")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLine")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLine")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLine")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLine")]
        public ActionResult DeleteGuarantorAgreementLine([FromBody] RowIdentity parm)
        {
            try
            {
                IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                return Ok(GuarantorAgreementTableService.DeleteGuarantorAgreementLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getGuarantorTransDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getGuarantorTransDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getGuarantorTransDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getGuarantorTransDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getGuarantorTransDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getGuarantorTransDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getGuarantorTransDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getGuarantorTransDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getGuarantorTransDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getGuarantorTransDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getGuarantorTransDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getGuarantorTransDropDown")]
        public ActionResult getGuarantorTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GuarantorTransDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetNationalityDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetNationalityDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetNationalityDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetNationalityDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetNationalityDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetNationalityDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetNationalityDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetNationalityDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetNationalityDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetNationalityDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetNationalityDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetNationalityDropDown")]
        public ActionResult GetNationalityDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetNationalityDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetRaceDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetRaceDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetRaceDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetRaceDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetRaceDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetRaceDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetRaceDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetRaceDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetRaceDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetRaceDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetRaceDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetRaceDropDown")]
        public ActionResult GetRaceDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetRaceDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCreditAppDropdownByProductMainAgreementDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCreditAppDropdownByProductMainAgreementDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCreditAppDropdownByProductMainAgreementDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCreditAppDropdownByProductMainAgreementDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCreditAppDropdownByProductMainAgreementDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCreditAppDropdownByProductMainAgreementDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCreditAppDropdownByProductMainAgreementDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCreditAppDropdownByProductMainAgreementDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCreditAppDropdownByProductMainAgreementDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCreditAppDropdownByProductMainAgreementDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCreditAppDropdownByProductMainAgreementDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCreditAppDropdownByProductMainAgreementDropDown")]
        public ActionResult GetCreditAppDropdownByProductMainAgreementDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetCreditAppTableByGuarantorLineIdDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByDropDown")]
        public ActionResult GetCustomerTableByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICustomerTableRepo ICustomerTableRepo = new CustomerTableRepo(db);
                return Ok(ICustomerTableRepo.GetDropDownItem(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineInitialData/id={id}")]

        public ActionResult GetGuarantorAgreementLineInitialData(string id)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetGuarantorAgreementLineInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAccessMode/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAccessMode/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAccessMode/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAccessMode/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAccessMode/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAccessMode/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAccessMode/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAccessMode/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAccessMode/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAccessMode/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAccessMode/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAccessMode/id={id}")]

        public ActionResult GetGuarantorAgreementLineAccessMode(string id)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetGuarantorAgreementLineAccessMode(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GuarantorAgreementGuarantorAgreementLine
        #region GuarantorAgreementLineAffiliate
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
        public ActionResult GetGuarantorAgreementLineByIdFromAffiliate(string id)
        {
            try
            {
                IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db);
                var result = GuarantorAgreementTableService.GetGuarantorAgreementLineByIdFromAffiliate(id);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetAccessModeByGuarantorAgreement/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetAccessModeByGuarantorAgreement/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetAccessModeByGuarantorAgreement/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetAccessModeByGuarantorAgreement/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetAccessModeByGuarantorAgreement/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetAccessModeByGuarantorAgreement/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetAccessModeByGuarantorAgreement/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetAccessModeByGuarantorAgreement/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetAccessModeByGuarantorAgreement/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetAccessModeByGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetAccessModeByGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetAccessModeByGuarantorAgreement/id={id}")]
        public ActionResult GetAccessModeByGuarantorAgreement(string id)
        {
            try
            {
                IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db);
                var result = GuarantorAgreementTableService.GetAccessModeByGuarantorAgreement(id);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateList/ByCompany")]
        public ActionResult GetGuarantorAgreementLineAffiliateListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetGuarantorAgreementLineAffiliateListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateById/id={id}")]
        public ActionResult GetGuarantorAgreementLineAffiliateById(string id)
        {
            try
            {
                IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(GuarantorAgreementTableService.GetGuarantorAgreementLineAffiliateById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLineAffiliate")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLineAffiliate")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLineAffiliate")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLineAffiliate")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLineAffiliate")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLineAffiliate")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLineAffiliate")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLineAffiliate")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLineAffiliate")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLineAffiliate")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLineAffiliate")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/CreateGuarantorAgreementLineAffiliate")]
        public ActionResult CreateGuarantorAgreementLineAffiliate([FromBody] GuarantorAgreementLineAffiliateItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                    return Ok(GuarantorAgreementTableService.CreateGuarantorAgreementLineAffiliate(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLineAffiliate")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLineAffiliate")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLineAffiliate")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLineAffiliate")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLineAffiliate")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLineAffiliate")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLineAffiliate")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLineAffiliate")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLineAffiliate")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLineAffiliate")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLineAffiliate")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/UpdateGuarantorAgreementLineAffiliate")]
        public ActionResult UpdateGuarantorAgreementLineAffiliate([FromBody] GuarantorAgreementLineAffiliateItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                    return Ok(GuarantorAgreementTableService.UpdateGuarantorAgreementLineAffiliate(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLineAffiliate")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLineAffiliate")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLineAffiliate")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLineAffiliate")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLineAffiliate")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLineAffiliate")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLineAffiliate")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLineAffiliate")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLineAffiliate")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLineAffiliate")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLineAffiliate")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/DeleteGuarantorAgreementLineAffiliate")]
        public ActionResult DeleteGuarantorAgreementLineAffiliate([FromBody] RowIdentity parm)
        {
            try
            {
                IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                return Ok(GuarantorAgreementTableService.DeleteGuarantorAgreementLineAffiliate(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByMainAgreementDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByMainAgreementDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByMainAgreementDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByMainAgreementDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByMainAgreementDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByMainAgreementDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByMainAgreementDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByMainAgreementDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByMainAgreementDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByMainAgreementDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByMainAgreementDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetCustomerTableByMainAgreementDropDown")]
        public ActionResult GetCustomerTableByMainAgreementDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetCustomerTableByMainAgreementDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetMainAgreementTableByGuarantorDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetMainAgreementTableByGuarantorDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetMainAgreementTableByGuarantorDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetMainAgreementTableByGuarantorDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetMainAgreementTableByGuarantorDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetMainAgreementTableByGuarantorDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetMainAgreementTableByGuarantorDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetMainAgreementTableByGuarantorDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetMainAgreementTableByGuarantorDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetMainAgreementTableByGuarantorDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetMainAgreementTableByGuarantorDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetMainAgreementTableByGuarantorDropDown")]
        public ActionResult GetMainAgreementTableByGuarantorDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetMainAgreementTableByGuarantorDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]

        public ActionResult GetGuarantorAgreementLineAffiliateInitialData(string id)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetGuarantorAgreementLineAffiliateInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getCustomerTableByGuarantorIdDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getCustomerTableByGuarantorIdDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getCustomerTableByGuarantorIdDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getCustomerTableByGuarantorIdDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getCustomerTableByGuarantorIdDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getCustomerTableByGuarantorIdDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getCustomerTableByGuarantorIdDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getCustomerTableByGuarantorIdDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getCustomerTableByGuarantorIdDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getCustomerTableByGuarantorIdDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getCustomerTableByGuarantorIdDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/getCustomerTableByGuarantorIdDropDown")]
        public ActionResult GetCustomerTableByGuarantorIdDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetCustomerTableByGuarantorIdDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GuarantorAgreementGuarantorAgreementLineAffiliate
        #region BookmarkDocumentTrans
        [HttpPost]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]

        public ActionResult GetAddendumBookmarkDocumentTransListByCompanyByGuarantorAgreementTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBookmarkDocumentTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        public ActionResult GetAddendumBookmarkDocumentTransByIdByGuarantorAgreementTable(string id)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(BookmarkDocumentTransService.GetBookmarkDocumentTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        public ActionResult CreateAddendumBookmarkDocumentTransByGuarantorAgreementTable([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.CreateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        public ActionResult UpdateAddendumBookmarkDocumentTransByGuarantorAgreementTable([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.UpdateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        public ActionResult DeleteAddendumBookmarkDocumentTransByGuarantorAgreementTable([FromBody] RowIdentity parm)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                return Ok(BookmarkDocumentTransService.DeleteBookmarkDocumentTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        public ActionResult GetAddendumBookmarkDocumentTransInitialDataByGuarantorAgreementTable(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetBookmarkDocumentTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        public ActionResult GetAddendumBookmarkDocumentTransInitialDataGuarantorAgreementByGuarantorAgreementTable(string id)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                return Ok(guarantorAgreementTableService.GetBookmarkDocumentTransInitialDataByGuarantorAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        public ActionResult GetAddendumGuarantorAgreementTableBookmarkDocumentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
                return Ok(bookmarkDocumentService.GetDropDownItemBookmarkDocumentByGuarantorAgreement(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        public ActionResult GetAddendumDocumentTemplateTableDropDownByDocumentTypeByGuarantorAgreementTable([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
                return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        public ActionResult GetAddendumDocumentTemplateTableDropDownByGuarantorAgreementTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]


        public ActionResult GetAddendumBookmarkDocumentTransDocumentStatusDropDownByGuarantorAgreementTable([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetDropDownItemBookMarkDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetDocumentStatusDropDown")]
        public ActionResult GetGuarantorAgreementDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetDropDownItemGuarantorAgreementTableStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region PrintBookDocTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        public ActionResult GetAddendumPrintBookDocTransValidationByGuarantorAgreementTable([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintBookDocTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        public async Task<ActionResult> PrintAddendumBookmarkDocumentTransactionByGuarantorAgreementTable([FromBody] PrintBookDocTransParm printBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region copy bookmark document from template
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Bond/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Bond/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        public ActionResult GetAddendumCopyBookmarkDocumentValidationByGuarantorAgreementTable([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.GetCopyBookmarkDocumentValidation(param));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Bond/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Bond/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        public ActionResult CopyAddendumBookmarkDocumentFromTemplateByGuarantorAgreementTable([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.CopyBookmarkDocumentFromTemplate(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Bond/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Bond/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        public ActionResult GetAddendumBookmarkDocumentTemplateTableDropDownByGuarantorAgreement([FromBody] SearchParameter search)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetDropDownItemBookmarkDocumentTemplateTableByGuarantorAgreement(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #endregion BookmarkDocumentTrans
        #region Extendguarantor
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        public ActionResult GetGenerateNoticeOfCancellationValidation(string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                    return Ok(GuarantorAgreementTableService.GetGenerateNoticeOfCancellationValidation(id));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
        public ActionResult GetExtendGuarantorAgreementTableById(string id)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetExtendGuarantorAgreementTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GenerateExtendGuarantorAgreement")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GenerateExtendGuarantorAgreement")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GenerateExtendGuarantorAgreement")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GenerateExtendGuarantorAgreement")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GenerateExtendGuarantorAgreement")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GenerateExtendGuarantorAgreement")]
        [Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GenerateExtendGuarantorAgreement")]
        [Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GenerateExtendGuarantorAgreement")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GenerateExtendGuarantorAgreement")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GenerateExtendGuarantorAgreement")]
        [Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GenerateExtendGuarantorAgreement")]
        [Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GenerateExtendGuarantorAgreement")]
        public ActionResult GetGuarantorAgreementExtendTableById([FromBody] ExtendGuarantorAgreementView vwModel)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GuarantorAgreementExtend(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/ValidateIsManualInternalNumberSeq/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/ValidateIsManualInternalNumberSeq/productType={productType}")]
        public ActionResult ValidateIsManualInternalNumberSeqExtendGuarantor(int productType)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.IsManualByInternalGuarantorRequest(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region PrintSetBookDocTrans
        [HttpPost]
        //guarantorAgreement
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransactionByGuarantorAgreementTable([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByRefGUID(printSetBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Attachment
        [HttpPost]
        //GuarantorAgreementTable
        [HttpPost]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompanyByGuarantorAgreementTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //GuarantorAgreementTable
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentByIdByGuarantorAgreementTable(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        //GuarantorAgreementTable
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/CreateAttachment")]
        public ActionResult CreateAttachmentByGuarantorAgreementTable([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        //GuarantorAgreementTable
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachmentByGuarantorAgreementTable([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        //GuarantorAgreementTable
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        public ActionResult DeleteAttachmentByGuarantorAgreementTable([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        //GuarantorAgreementTable

        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefIdByGuarantorAgreementTable([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region copy bookmark document from template
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        public ActionResult GetCopyBookmarkDocumentValidationByGuarantorAgreementTable([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.GetCopyBookmarkDocumentValidation(param));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        public ActionResult CopyBookmarkDocumentFromTemplateByGuarantorAgreementTable([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.CopyBookmarkDocumentFromTemplate(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        public ActionResult GetBookmarkDocumentTemplateTableDropDownByGuarantorAgreement([FromBody] SearchParameter search)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetDropDownItemBookmarkDocumentTemplateTableByGuarantorAgreement(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        [HttpGet]
        [Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/getValidateOnCreate/id={id}")]
        public ActionResult GetAccessModeByAssignmentTable(string id)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                return Ok(guarantorAgreementTableService.GetAccessModeByDocumentStatus(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }

}