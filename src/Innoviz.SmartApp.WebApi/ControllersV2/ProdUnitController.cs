﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class ProdUnitController: BaseControllerV2
    {
        public ProdUnitController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            :base(context, transactionLogService)
        {

        }
		[HttpPost]
		[Route("GetProdUnitList/ByCompany")]
		public ActionResult GetProdUnitListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetProdUnitListListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetProdUnitById/id={id}")]
		public ActionResult GetProdUnitById(string id)
		{
			try
			{
				IProdUnitService prodUnitService = new ProdUnitService(db);
				return Ok(prodUnitService.GetProdUnitById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateProdUnit")]
		public ActionResult CreateProductSubType([FromBody] ProdUnitItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IProdUnitService prodUnitService = new ProdUnitService(db, SysTransactionLogService);
					return Ok(prodUnitService.CreateProdUnit(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateProdUnit")]
		public ActionResult UpdateProductSubType([FromBody] ProdUnitItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IProdUnitService prodUnitService = new ProdUnitService(db, SysTransactionLogService);
					return Ok(prodUnitService.UpdateProdUnit(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteProdUnit")]
		public ActionResult DeleteProdUnit([FromBody] RowIdentity parm)
		{
			try
			{
				IProdUnitService prodUnitService = new ProdUnitService(db, SysTransactionLogService);
				return Ok(prodUnitService.DeleteProdUnit(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
