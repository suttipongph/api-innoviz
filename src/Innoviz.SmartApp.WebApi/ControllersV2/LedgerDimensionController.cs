﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class LedgerDimensionController : BaseControllerV2
    {
        public LedgerDimensionController(SmartAppContext context, ISysTransactionLogService transactionLogService)
          : base(context, transactionLogService)
        {

        }
        [HttpPost]
        [Route("GetLedgerDimensionList/ByCompany")]
        public ActionResult GetLedgerDimensionListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetLedgerDimensionListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetLedgerDimensionById/id={id}")]
        public ActionResult GetLedgerDimensionById(string id)
        {
            try
            {
                ILedgerDimensionService ledgerDimensionService = new LedgerDimensionService(db);
                return Ok(ledgerDimensionService.GetLedgerDimensionById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateLedgerDimension")]
        public ActionResult CreateLedgerDimension([FromBody] LedgerDimensionItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ILedgerDimensionService ledgerDimensionService = new LedgerDimensionService(db, SysTransactionLogService);
                    return Ok(ledgerDimensionService.CreateLedgerDimension(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateLedgerDimension")]
        public ActionResult UpdateLedgerDimension([FromBody] LedgerDimensionItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ILedgerDimensionService ledgerDimensionService = new LedgerDimensionService(db, SysTransactionLogService);
                    return Ok(ledgerDimensionService.UpdateLedgerDimension(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteLedgerDimension")]
        public ActionResult DeleteLedgerDimension([FromBody] RowIdentity parm)
        {
            try
            {
                ILedgerDimensionService ledgerDimensionService = new LedgerDimensionService(db, SysTransactionLogService);
                return Ok(ledgerDimensionService.DeleteLedgerDimension(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        #endregion
    }
}
