﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public partial class MainAgreementTableController : BaseControllerV2
    {

        public MainAgreementTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/GetMainAgreementTableList/ByCompany")]

        public ActionResult GetMainAgreementTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMainAgreementTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Bond/AllMainAgreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Bond/NewMainAgreementTable/GetMainAgreementTableById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/GetMainAgreementTableById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/GetMainAgreementTableById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/GetMainAgreementTableById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/GetMainAgreementTableById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/GetMainAgreementTableById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/GetMainAgreementTableById/id={id}")]
        public ActionResult GetMainAgreementTableById(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetMainAgreementTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/CreateMainAgreementTable")]
        [Route("Factoring/NewMainAgreementTable/CreateMainAgreementTable")]
        [Route("Bond/AllMainAgreementTable/CreateMainAgreementTable")]
        [Route("Bond/NewMainAgreementTable/CreateMainAgreementTable")]
        [Route("HirePurchase/AllMainAgreementTable/CreateMainAgreementTable")]
        [Route("HirePurchase/NewMainAgreementTable/CreateMainAgreementTable")]
        [Route("LCDLC/AllMainAgreementTable/CreateMainAgreementTable")]
        [Route("LCDLC/NewMainAgreementTable/CreateMainAgreementTable")]
        [Route("Leasing/AllMainAgreementTable/CreateMainAgreementTable")]
        [Route("Leasing/NewMainAgreementTable/CreateMainAgreementTable")]
        [Route("ProjectFinance/AllMainAgreementTable/CreateMainAgreementTable")]
        [Route("ProjectFinance/NewMainAgreementTable/CreateMainAgreementTable")]
        public ActionResult CreateMainAgreementTable([FromBody] MainAgreementTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                    return Ok(mainAgreementTableService.CreateMainAgreementTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/UpdateMainAgreementTable")]
        [Route("Factoring/NewMainAgreementTable/UpdateMainAgreementTable")]
        [Route("Bond/AllMainAgreementTable/UpdateMainAgreementTable")]
        [Route("Bond/NewMainAgreementTable/UpdateMainAgreementTable")]
        [Route("HirePurchase/AllMainAgreementTable/UpdateMainAgreementTable")]
        [Route("HirePurchase/NewMainAgreementTable/UpdateMainAgreementTable")]
        [Route("LCDLC/AllMainAgreementTable/UpdateMainAgreementTable")]
        [Route("LCDLC/NewMainAgreementTable/UpdateMainAgreementTable")]
        [Route("Leasing/AllMainAgreementTable/UpdateMainAgreementTable")]
        [Route("Leasing/NewMainAgreementTable/UpdateMainAgreementTable")]
        [Route("ProjectFinance/AllMainAgreementTable/UpdateMainAgreementTable")]
        [Route("ProjectFinance/NewMainAgreementTable/UpdateMainAgreementTable")]


        public ActionResult UpdateMainAgreementTable([FromBody] MainAgreementTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                    return Ok(mainAgreementTableService.UpdateMainAgreementTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/DeleteMainAgreementTable")]
        [Route("Factoring/NewMainAgreementTable/DeleteMainAgreementTable")]
        [Route("Bond/AllMainAgreementTable/DeleteMainAgreementTable")]
        [Route("Bond/NewMainAgreementTable/DeleteMainAgreementTable")]
        [Route("HirePurchase/AllMainAgreementTable/DeleteMainAgreementTable")]
        [Route("HirePurchase/NewMainAgreementTable/DeleteMainAgreementTable")]
        [Route("LCDLC/AllMainAgreementTable/DeleteMainAgreementTable")]
        [Route("LCDLC/NewMainAgreementTable/DeleteMainAgreementTable")]
        [Route("Leasing/AllMainAgreementTable/DeleteMainAgreementTable")]
        [Route("Leasing/NewMainAgreementTable/DeleteMainAgreementTable")]
        [Route("ProjectFinance/AllMainAgreementTable/DeleteMainAgreementTable")]
        [Route("ProjectFinance/NewMainAgreementTable/DeleteMainAgreementTable")]


        public ActionResult DeleteMainAgreementTable([FromBody] RowIdentity parm)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.DeleteMainAgreementTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/GetBuyerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/GetBuyerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/GetBuyerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/GetBuyerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/GetBuyerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/GetBuyerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/GetBuyerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/GetBuyerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/GetBuyerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/GetBuyerTableDropDown")]

        // InvoiceTable RelatedInfo
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetDropDownItemByMainAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Factoring/NewMainAgreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Bond/AllMainAgreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Bond/NewMainAgreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("LCDLC/AllMainAgreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("LCDLC/NewMainAgreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Leasing/AllMainAgreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Leasing/NewMainAgreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/GetCreditAppTableDropDownByMainAgreement")]

        public ActionResult GetCreditAppTableDropDownByMainAgreement([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetDropDownItemCreditAppTableByMainAgreement(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/GetCreditAppTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/GetCreditAppTableDropDown")]
        [Route("Bond/AllMainAgreementTable/GetCreditAppTableDropDown")]
        [Route("Bond/NewMainAgreementTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/GetCreditAppTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/GetCreditAppTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/GetCreditAppTableDropDown")]

        // InvoiceTable RelatedInfo
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/GetCustomerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/GetCustomerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/GetCustomerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/GetCustomerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/GetCustomerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/GetCustomerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/GetCustomerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/GetCustomerTableDropDown")]
        // InvoiceTable RelatedInfo
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Factoring/NewMainAgreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Bond/AllMainAgreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Bond/NewMainAgreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("LCDLC/AllMainAgreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("LCDLC/NewMainAgreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Leasing/AllMainAgreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Leasing/NewMainAgreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/GetWithdrawalTableDropDownByMainAgreement")]

        public ActionResult GetWithdrawalTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetDropDownItemByMainAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/GetDocumentStatusDropDown")]

        public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetDropDownItemMainTainAgreementStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        // LoanRequestAgreemetn Function
        [Route("Factoring/allmainagreementtable/Function/loanrequestagreement/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/Function/loanrequestagreement/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/Function/loanrequestagreement/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/Function/loanrequestagreement/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/loanrequestagreement/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/loanrequestagreement/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/Function/loanrequestagreement/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/Function/loanrequestagreement/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/Function/loanrequestagreement/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/Function/loanrequestagreement/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/loanrequestagreement/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/loanrequestagreement/ValidateIsManualNumberSeq/productType={productType}")]
        public ActionResult ValidateIsManualNumberSeqInternalMainAgreement(int productType)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.IsManualInternalMainAgreement(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        public ActionResult ValidateIsManualNumberSeqMainAgreement(int productType)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.IsManualMainAgreement(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/GetMainAgreementTableInitialData/productType={productType}")]

        public ActionResult GetMainAgreementTableInitialData(int productType)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetMainAgreementTableInitialData(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Factoring/NewMainAgreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Bond/AllMainAgreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Bond/NewMainAgreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("HirePurchase/AllMainAgreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("HirePurchase/NewMainAgreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("LCDLC/AllMainAgreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("LCDLC/NewMainAgreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Leasing/AllMainAgreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Leasing/NewMainAgreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("ProjectFinance/AllMainAgreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("ProjectFinance/NewMainAgreementTable/GetBuyerAgreementTableByBuyerTable")]
        public ActionResult GetBuyerAgreementTableByBuyerTable([FromBody] MainAgreementTableItemView model)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetBuyerAgreementTableByBuyerTable(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        public ActionResult GetAccessModeByMainAgreementTable(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Memo
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult GetMemoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetMemoById(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult DeleteMemo([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Memo
        #region JointVentureTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        public ActionResult GetJointVentureTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetJointVentureTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        public ActionResult GetJointVentureTransById(string id)
        {
            try
            {
                IJointVentureTransService JointVentureTransService = new JointVentureTransService(db);
                return Ok(JointVentureTransService.GetJointVentureTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        public ActionResult CreateJointVentureTrans([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.CreateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        public ActionResult UpdateJointVentureTrans([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.UpdateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        public ActionResult DeleteJointVentureTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                return Ok(JointVentureTransService.DeleteJointVentureTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        public ActionResult GetJointVentureTransInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetJointVentureTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetJointVentureTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion JointVentureTrans
        #region BookmarkDocumentTrans
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        public ActionResult GetAccessModeByMainAgreementTableBookmarkDocumentTrans(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetAccessModeByStatusLessThanEqSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]

        public ActionResult GetBookmarkDocumentTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBookmarkDocumentTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        public ActionResult GetBookmarkDocumentTransById(string id)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(BookmarkDocumentTransService.GetBookmarkDocumentTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        public ActionResult CreateBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.CreateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        public ActionResult UpdateBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.UpdateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        public ActionResult DeleteBookmarkDocumentTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                return Ok(BookmarkDocumentTransService.DeleteBookmarkDocumentTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        public ActionResult GetBookmarkDocumentTransInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetBookmarkDocumentTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        public ActionResult GetBookmarkDocumentTransInitialDataGuarantorAgreement(string id)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                return Ok(guarantorAgreementTableService.GetBookmarkDocumentTransInitialDataByGuarantorAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        public ActionResult GetBookmarkDocumentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
                return Ok(bookmarkDocumentService.GetDropDownItemBookmarkDocumentByMainAgreement(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        public ActionResult GetDocumentTemplateTableDropDownByDocumentType([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
                return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        public ActionResult GetDocumentTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]


        public ActionResult GetBookmarkDocumentTransDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetDropDownItemBookMarkDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region PrintBookDocTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        public ActionResult GetPrintBookDocTransValidation([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintBookDocTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransaction([FromBody] PrintBookDocTransParm printBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion BookmarkDocumentTrans
        #region AgreementTableInfo
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        public ActionResult GetAgreementTableInfoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAgreementTableInfoListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        public ActionResult GetAgreementTableInfoById(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        public ActionResult CreateAgreementTableInfo([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.CreateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        public ActionResult UpdateAgreementTableInfo([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.UpdateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        public ActionResult DeleteAgreementTableInfo([FromBody] RowIdentity parm)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                return Ok(AgreementTableInfoService.DeleteAgreementTableInfo(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Factoring/NewMainAgreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Bond/AllMainAgreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Bond/NewMainAgreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Hire/AllMainAgreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Hire/NewMainAgreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Leasing/AllMainAgreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Leasing/NewMainAgreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        public ActionResult GetAgreementTableInfoByMainAgreement(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoByMainAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        public ActionResult GetAgreementTableInfoInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetAgreementTableInfoInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AgreementTableInfo

        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        public ActionResult GetAgreementTableInfoAddressTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressTransByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(agreementTableInfoService.GetDropDownItemAuthorizedPersonTransByAgreementInfo(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTransByBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(authorizedPersonTransService.GetDropDownItemAuthorizedPersonTransByBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        public ActionResult GetAgreementTableInfoCompanyBankDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCompanyBank(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        public ActionResult GetAgreementTableInfoCompanySignatureDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetEmployeeTableWithCompanySignatureDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        public ActionResult GetAgreementTableInfoEmployeeTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AgreementTableInfo
        #region ConsortiumTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        public ActionResult GetConsortiumTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetConsortiumTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        public ActionResult GetConsortiumTransById(string id)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db);
                return Ok(ConsortiumTransService.GetConsortiumTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        public ActionResult CreateConsortiumTrans([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.CreateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        public ActionResult UpdateConsortiumTrans([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.UpdateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        public ActionResult DeleteConsortiumTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                return Ok(ConsortiumTransService.DeleteConsortiumTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        public ActionResult GetConsortiumTransInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetConsortiumTransInitialDataByMainAgreementTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ConsortiumTrans

        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetConsortiumTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        public ActionResult GetConsortiumTransConsortiumLineDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IConsortiumTableService consortiumTableService = new ConsortiumTableService(db);
                return Ok(consortiumTableService.GetDropDownItemConsortiumLineByConsortiumTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ConsortiumTrans

        #region ServiceFeeTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        public ActionResult GetServiceFeeTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        public ActionResult GetServiceFeeTransById(string id)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetServiceFeeTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        public ActionResult CreateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.CreateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        public ActionResult UpdateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.UpdateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        public ActionResult DeleteServiceFeeTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                return Ok(ServiceFeeTransService.DeleteServiceFeeTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        public ActionResult GetAccessModeServiceFeeTransByMainAgreementTable(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        public ActionResult GetServiceFeeTransInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetServiceFeeTransInitialDataByMainAgreementTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]

        public ActionResult GetCalculateField([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetCalculateField(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        public ActionResult ValidateServiceFeeTrans([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.ValidateServiceFeeTrans(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        public ActionResult GetTaxValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetTaxValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        public ActionResult GetWHTValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetWHTValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]

        public ActionResult GetInvoiceRevenueTypeByProductTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeByProductTypeAndDefaultNone(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]

        // AddRelatedInfoInvoiceTable
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]

        public ActionResult GetLedgerDimensionDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]

        public ActionResult GetTaxTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]

        public ActionResult GetWithholdingTaxTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByServiceFeeTrans([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        public ActionResult GetOriginalInvoiceByServiceFeeTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetOriginalInvoiceByServiceFeeTransDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion ServiceFeeTrans

        #region InvoiceTable
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        public ActionResult GetInvoiceTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        public ActionResult GetInvoiceTableById(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        public ActionResult CreateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceTable(vwModel));

                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        public ActionResult UpdateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        public ActionResult DeleteInvoiceTable([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        public ActionResult GetInvoiceLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        public ActionResult DeleteInvoiceLine([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        public ActionResult CreateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        public ActionResult UpdateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        public ActionResult GetInvoiceLineById(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDownInvoiceLine([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceTable
        #region DropdownInvoiceTable
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        public ActionResult GetInvoiceTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        public ActionResult GetBuyerInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByInvoiceTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCurrency(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        public ActionResult GetInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        public ActionResult GetProdUnitTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        public ActionResult GetTaxInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        public ActionResult GetRefInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetRefInvoiceTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        public ActionResult GetDocumentStatusByInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]

        public ActionResult GetWithholdingTaxTableDropDownByInvoiceTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropdownInvoiceTable

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("Factoring/NewMainAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/AllMainAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/NewMainAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/AllMainAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/NewMainAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/GetVisibleManageMenu/id={id}")]
        public ActionResult GetVisibleManageMenu(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);

                return Ok(mainAgreementTableService.GetVisibleManageMenu(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Manage agreement
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/SignAgreement/ManageAgreement")]
        public ActionResult ManageAgreement([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.ManageAgreement(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/SignAgreement/GetDocumentReasonDropDown")]

        public ActionResult GetDocumentReasonDropDownByManageAgreement([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Function
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/CloseAgreement/GetManageAgreementValidation")]
        public ActionResult GetManageAgreementValidation([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/SignAgreement/GetManageAgreementByRefType")]
        public ActionResult GetManageAgreementByRefType([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementByRefType(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Copy Service fee from CA
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        public ActionResult GetCopyServiceFeeCondCARequestInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetCopyServiceFeeCondCARequestInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        public ActionResult GetCreditAppRequestTableByCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetDropDownItemCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        public ActionResult CreateCopyServiceFeeCondCARequest([FromBody] MainAgreementTableItemView view)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.CreateCopyServiceFeeCondCARequest(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Copy Service fee from CA
        #region PrintSetBookDocTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/GetPrintSetBookDocTransValidation")]
        [Route("Factoring/NewMainAgreementTable/GetPrintSetBookDocTransValidation")]
        [Route("Bond/AllMainAgreementTable/GetPrintSetBookDocTransValidation")]
        [Route("Bond/NewMainAgreementTable/GetPrintSetBookDocTransValidation")]
        [Route("HirePurchase/AllMainAgreementTable/GetPrintSetBookDocTransValidation")]
        [Route("HirePurchase/NewMainAgreementTable/GetPrintSetBookDocTransValidation")]
        [Route("LCDLC/AllMainAgreementTable/GetPrintSetBookDocTransValidation")]
        [Route("LCDLC/NewMainAgreementTable/GetPrintSetBookDocTransValidation")]
        [Route("Leasing/AllMainAgreementTable/GetPrintSetBookDocTransValidation")]
        [Route("Leasing/NewMainAgreementTable/GetPrintSetBookDocTransValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/GetPrintSetBookDocTransValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/GetPrintSetBookDocTransValidation")]
        public ActionResult GetPrintSetBookDocTransValidation([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintSetBookDocTransValidation(printSetBookDocTransParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Factoring/NewMainAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/AllMainAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/NewMainAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/AllMainAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/NewMainAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/AllMainAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/NewMainAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/AllMainAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/NewMainAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransaction([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByRefGUID(printSetBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Attachment
        [HttpPost]

        [Route("Factoring/allmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Bond/allmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Bond/newmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Hirepurchase/allmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Hirepurchase/newmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/allmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Bond/allmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Hirepurchase/allmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Hirepurchase/newmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentById(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/attachment/CreateAttachment")]

        [Route("Bond/allmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Bond/newmainagreementtable/RelatedInfo/attachment/CreateAttachment")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/attachment/CreateAttachment")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/attachment/CreateAttachment")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/attachment/CreateAttachment")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        public ActionResult CreateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Bond/allmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Bond/newmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Bond/allmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Bond/newmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        public ActionResult DeleteAttachment([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Bond/allmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Bond/newmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefId([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Loan Request Agreement
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/getGenerateLoanRequestAGMValidation")]
        [Route("Factoring/NewMainAgreementTable/getGenerateLoanRequestAGMValidation")]
        [Route("Bond/AllMainAgreementTable/getGenerateLoanRequestAGMValidation")]
        [Route("Bond/NewMainAgreementTable/getGenerateLoanRequestAGMValidation")]
        [Route("HirePurchase/AllMainAgreementTable/getGenerateLoanRequestAGMValidation")]
        [Route("HirePurchase/NewMainAgreementTable/getGenerateLoanRequestAGMValidation")]
        [Route("LCDLC/AllMainAgreementTable/getGenerateLoanRequestAGMValidation")]
        [Route("LCDLC/NewMainAgreementTable/getGenerateLoanRequestAGMValidation")]
        [Route("Leasing/AllMainAgreementTable/getGenerateLoanRequestAGMValidation")]
        [Route("Leasing/NewMainAgreementTable/getGenerateLoanRequestAGMValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/getGenerateLoanRequestAGMValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/getGenerateLoanRequestAGMValidation")]
        public ActionResult getGenerateLoanRequestAGMValidation([FromBody] MainAgreementTableItemView view)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.getGenerateLoanRequestAGMValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/allmainagreementtable/Function/loanrequestagreement/GetGenMainAgmLoanRequestById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/Function/loanrequestagreement/GetGenMainAgmLoanRequestById/id={id}")]
        [Route("Bond/AllMainAgreementTable/Function/loanrequestagreement/GetGenMainAgmLoanRequestById/id={id}")]
        [Route("Bond/NewMainAgreementTable/Function/loanrequestagreement/GetGenMainAgmLoanRequestById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/loanrequestagreement/GetGenMainAgmLoanRequestById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/loanrequestagreement/GetGenMainAgmLoanRequestById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/Function/loanrequestagreement/GetGenMainAgmLoanRequestById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/Function/loanrequestagreement/GetGenMainAgmLoanRequestById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/Function/loanrequestagreement/GetGenMainAgmLoanRequestById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/Function/loanrequestagreement/GetGenMainAgmLoanRequestById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/loanrequestagreement/GetGenMainAgmLoanRequestById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/loanrequestagreement/GetGenMainAgmLoanRequestById/id={id}")]
        public ActionResult GetGenMainAgmLoanRequestById(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetGenMainAgmLoanRequestById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/allmainagreementtable/Function/loanrequestagreement/GetCreditAppRequestTableDropDownLoanRequest/id={id}")]
        [Route("Factoring/NewMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestTableDropDownLoanRequest/id={id}")]
        [Route("Bond/AllMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestTableDropDownLoanRequest/id={id}")]
        [Route("Bond/NewMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestTableDropDownLoanRequest/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/loanrequestagreement/GetCreditAppRequestTableDropDownLoanRequest/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/loanrequestagreement/GetCreditAppRequestTableDropDownLoanRequest/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestTableDropDownLoanRequest/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestTableDropDownLoanRequest/id={id}")]
        [Route("Leasing/AllMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestTableDropDownLoanRequest/id={id}")]
        [Route("Leasing/NewMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestTableDropDownLoanRequest/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestTableDropDownLoanRequest/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestTableDropDownLoanRequest/id={id}")]
        public ActionResult GetCreditAppRequestTableDropDownLoanRequest(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetDropDownItemCreditAppRequestTablebyLoanRequest(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/Function/loanrequestagreement/CreateGenMainAgmLoanRequest")]
        [Route("Factoring/NewMainAgreementTable/Function/loanrequestagreement/CreateGenMainAgmLoanRequest")]
        [Route("Bond/AllMainAgreementTable/Function/loanrequestagreement/CreateGenMainAgmLoanRequest")]
        [Route("Bond/NewMainAgreementTable/Function/loanrequestagreement/CreateGenMainAgmLoanRequest")]
        [Route("HirePurchase/AllMainAgreementTable/loanrequestagreement/CreateGenMainAgmLoanRequest")]
        [Route("HirePurchase/NewMainAgreementTable/loanrequestagreement/CreateGenMainAgmLoanRequest")]
        [Route("LCDLC/AllMainAgreementTable/Function/loanrequestagreement/CreateGenMainAgmLoanRequest")]
        [Route("LCDLC/NewMainAgreementTable/Function/loanrequestagreement/CreateGenMainAgmLoanRequest")]
        [Route("Leasing/AllMainAgreementTable/Function/loanrequestagreement/CreateGenMainAgmLoanRequest")]
        [Route("Leasing/NewMainAgreementTable/Function/loanrequestagreement/CreateGenMainAgmLoanRequest")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/loanrequestagreement/CreateGenMainAgmLoanRequest")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/loanrequestagreement/CreateGenMainAgmLoanRequest")]
        public ActionResult CreateGenMainAgmLoanRequest([FromBody] GenMainAgmLoanRequestView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                    return Ok(mainAgreementTableService.CreateGenMainAgmLoanRequest(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region copy bookmark document from template

        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Bond/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Bond/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        public ActionResult GetBookmarkDocumentTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetDropDownItemBookmarkDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Bond/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Bond/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        public ActionResult GetCopyBookmarkDocumentValidation([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.GetCopyBookmarkDocumentValidation(param));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Bond/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Bond/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        public ActionResult CopyBookmarkDocumentFromTemplate([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.CopyBookmarkDocumentFromTemplate(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region TaxInvoice
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        public ActionResult GetTaxInvoiceListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetTaxInvoiceTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        public ActionResult GetTaxInvoiceTableById(string id)
        {
            try
            {
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetTaxInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/InvoiceTaTaxInvoiceble/GetTaxInvoiceLineList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        public ActionResult GetTaxInvoiceLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetTaxInvoiceLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        public ActionResult GetTaxInvoiceLineById(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetTaxInvoiceLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusTaxInvoiceDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetDropDownItemTaxInvoiceDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDownTaxInvoiceLine([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeDropDownTaxInvoiceLine([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Report
        [HttpGet]
        //[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        public ActionResult GetPrintTaxInvoiceCopyById(string id)
        {
            try
            {
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetPrintTaxInvoiceCopyById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        //[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        public ActionResult PrintTaxInvoiceCopy([FromBody] PrintTaxInvoiceReportView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        public ActionResult GetPrintTaxInvoiceOriginalById(string id)
        {
            try
            {
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetPrintTaxInvoiceOriginalById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        //[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        public ActionResult PrintTaxInvoiceOriginal([FromBody] PrintTaxInvoiceReportView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Report

        #endregion Tax Invoice
        #region payment history
        [HttpPost]
        //[Route("GetPaymentHistoryList/ByCompany")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        public ActionResult GetPaymentHistoryListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                var result = sysListViewService.GetPaymentHistoryListvw(search);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("GetPaymentHistoryById/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        public ActionResult GetPaymentHistoryById(string id)
        {
            try
            {
                IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db);
                return Ok(paymentHistoryService.GetPaymentHistoryById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        public ActionResult CreatePaymentHistory([FromBody] PaymentHistoryItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db, SysTransactionLogService);
                    return Ok(paymentHistoryService.CreatePaymentHistory(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        //[Route("UpdatePaymentHistory")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        public ActionResult UpdatePaymentHistory([FromBody] PaymentHistoryItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db, SysTransactionLogService);
                    return Ok(paymentHistoryService.UpdatePaymentHistory(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        //[Route("/PaymentHistory/DeletePaymentHistory")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        public ActionResult DeletePaymentHistory([FromBody] RowIdentity parm)
        {
            try
            {
                IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db, SysTransactionLogService);
                return Ok(paymentHistoryService.DeletePaymentHistory(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDownByPaymentHistory([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        public ActionResult GetInvoiceTableDropDownByPaymentHistory([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDownByPaymentHistory([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CustTrans
        [HttpPost]
        //[Route("RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        public ActionResult GetCustTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        //[Route("RelatedInfo/CustTransaction/GetCustTransListByCustomer/ByCompany")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        public ActionResult GetCustTransListByCustomer([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        public ActionResult GetCustTransById(string id)
        {
            try
            {
                ICustTransService custTransService = new CustTransService(db);
                return Ok(custTransService.GetCustTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        public ActionResult GetCustomerTablebyCusttrans(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetCustomerTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        public ActionResult GetInvoiceTablebyCusttrans(string id)
        {
            try
            {

                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        public ActionResult GetReceiptTempTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemReceiptTempTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        // AddInvoiceTable RelatedInfo
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDownInvoiceTable([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }

}