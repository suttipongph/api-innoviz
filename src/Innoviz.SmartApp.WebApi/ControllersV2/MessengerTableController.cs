using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class MessengerTableController : BaseControllerV2
	{

		public MessengerTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetMessengerTableList/ByCompany")]
		public ActionResult GetMessengerTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetMessengerTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetMessengerTableById/id={id}")]
		public ActionResult GetMessengerTableById(string id)
		{
			try
			{
				IMessengerTableService messengerTableService = new MessengerTableService(db);
				return Ok(messengerTableService.GetMessengerTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateMessengerTable")]
		public ActionResult CreateMessengerTable([FromBody] MessengerTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IMessengerTableService messengerTableService = new MessengerTableService(db, SysTransactionLogService);
					return Ok(messengerTableService.CreateMessengerTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateMessengerTable")]
		public ActionResult UpdateMessengerTable([FromBody] MessengerTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IMessengerTableService messengerTableService = new MessengerTableService(db, SysTransactionLogService);
					return Ok(messengerTableService.UpdateMessengerTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteMessengerTable")]
		public ActionResult DeleteMessengerTable([FromBody] RowIdentity parm)
		{
			try
			{
				IMessengerTableService messengerTableService = new MessengerTableService(db, SysTransactionLogService);
				return Ok(messengerTableService.DeleteMessengerTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetVendorTableDropDown")]
		public ActionResult GetVendorTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemVendorTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
	}
}
