using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class GuarantorAgreementTableController : BaseControllerV2
	{

		public GuarantorAgreementTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetGuarantorAgreementLineList/ByCompany")]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineList/ByCompany")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineList/ByCompany")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineList/ByCompany")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineList/ByCompany")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineList/ByCompany")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineList/ByCompany")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineList/ByCompany")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineList/ByCompany")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineList/ByCompany")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineList/ByCompany")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineList/ByCompany")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineList/ByCompany")]
		public ActionResult GetGuarantorAgreementLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetGuarantorAgreementLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
	    [Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineById/id={id}")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineById/id={id}")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineById/id={id}")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineById/id={id}")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineById/id={id}")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineById/id={id}")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineById/id={id}")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineById/id={id}")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineById/id={id}")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineById/id={id}")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineById/id={id}")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineById/id={id}")]
		[Route("GetGuarantorAgreementLineById/id={id}")]
		public ActionResult GetGuarantorAgreementLineById(string id)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.GetGuarantorAgreementLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateGuarantorAgreementLine")]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementLine")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementLine")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementLine")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementLine")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementLine")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementLine")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementLine")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementLine")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementLine")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementLine")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementLine")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementLine")]
		public ActionResult CreateGuarantorAgreementLine([FromBody] GuarantorAgreementLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
					return Ok(guarantorAgreementTableService.CreateGuarantorAgreementLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateGuarantorAgreementLine")]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementLine")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementLine")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementLine")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementLine")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementLine")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementLine")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementLine")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementLine")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementLine")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementLine")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementLine")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementLine")]
		public ActionResult UpdateGuarantorAgreementLine([FromBody] GuarantorAgreementLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
					return Ok(guarantorAgreementTableService.UpdateGuarantorAgreementLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]

		[Route("DeleteGuarantorAgreementLine")]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementLine")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementLine")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementLine")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementLine")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementLine")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementLine")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementLine")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementLine")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementLine")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementLine")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementLine")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementLine")]
		public ActionResult DeleteGuarantorAgreementLine([FromBody] RowIdentity parm)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
				return Ok(guarantorAgreementTableService.DeleteGuarantorAgreementLine(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetGuarantorAgreementTableList/ByCompany")]
		public ActionResult GetGuarantorAgreementTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetGuarantorAgreementTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		[Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		public ActionResult GetGuarantorAgreementTableById(string id)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.GetGuarantorAgreementTableExtendById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateGuarantorAgreementTable")]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementTable")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementTable")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementTable")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementTable")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementTable")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementTable")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementTable")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementTable")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementTable")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementTable")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementTable")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/CreateGuarantorAgreementTable")]
		public ActionResult CreateGuarantorAgreementTable([FromBody] GuarantorAgreementTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
					return Ok(guarantorAgreementTableService.CreateGuarantorAgreementTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateGuarantorAgreementTable")]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementTable")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementTable")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementTable")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementTable")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementTable")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementTable")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementTable")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementTable")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementTable")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementTable")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementTable")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/UpdateGuarantorAgreementTable")]
		public ActionResult UpdateGuarantorAgreementTable([FromBody] GuarantorAgreementTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
					return Ok(guarantorAgreementTableService.UpdateGuarantorAgreementTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteGuarantorAgreementTable")]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementTable")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementTable")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementTable")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementTable")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementTable")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementTable")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementTable")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementTable")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementTable")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementTable")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementTable")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/DeleteGuarantorAgreementTable")]
		public ActionResult DeleteGuarantorAgreementTable([FromBody] RowIdentity parm)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
				return Ok(guarantorAgreementTableService.DeleteGuarantorAgreementTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineInitialData/id={id}")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineInitialData/id={id}")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineInitialData/id={id}")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineInitialData/id={id}")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineInitialData/id={id}")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineInitialData/id={id}")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineInitialData/id={id}")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineInitialData/id={id}")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineInitialData/id={id}")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineInitialData/id={id}")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineInitialData/id={id}")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineInitialData/id={id}")]

		public ActionResult GetGuarantorAgreementLineInitialData(string id)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.GetGuarantorAgreementLineInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown

		[HttpPost]
		[Route("GetEmployeeTableDropDown")]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetEmployeeTableDropDown")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetEmployeeTableDropDown")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetEmployeeTableDropDown")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetEmployeeTableDropDown")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetEmployeeTableDropDown")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetEmployeeTableDropDown")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetEmployeeTableDropDown")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetEmployeeTableDropDown")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetEmployeeTableDropDown")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetEmployeeTableDropDown")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetEmployeeTableDropDown")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetEmployeeTableDropDown")]
		public ActionResult GetEmployeeTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorTransDropDown")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorTransDropDown")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorTransDropDown")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorTransDropDown")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorTransDropDown")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorTransDropDown")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorTransDropDown")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorTransDropDown")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorTransDropDown")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorTransDropDown")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorTransDropDown")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorTransDropDown")]
		public ActionResult GetGuarantorTransDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.GuarantorTransDropDown(search));

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetNationalityDropDown")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetNationalityDropDown")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetNationalityDropDown")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetNationalityDropDown")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetNationalityDropDown")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetNationalityDropDown")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetNationalityDropDown")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetNationalityDropDown")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetNationalityDropDown")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetNationalityDropDown")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetNationalityDropDown")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetNationalityDropDown")]
		public ActionResult GetNationalityDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemNationality(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetRaceDropDown")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetRaceDropDown")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetRaceDropDown")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetRaceDropDown")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetRaceDropDown")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetRaceDropDown")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetRaceDropDown")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetRaceDropDown")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetRaceDropDown")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetRaceDropDown")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetRaceDropDown")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetRaceDropDown")]
		public ActionResult GetRaceDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemRace(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
		#region extend
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetDocumentStatusDropDown")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetDocumentStatusDropDown")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetDocumentStatusDropDown")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetDocumentStatusDropDown")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetDocumentStatusDropDown")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetDocumentStatusDropDown")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetDocumentStatusDropDown")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetDocumentStatusDropDown")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetDocumentStatusDropDown")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetDocumentStatusDropDown")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetDocumentStatusDropDown")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetDocumentStatusDropDown")]
		[Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.GetDropDownItemGuarantorAgreementTableStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableList/ByCompany")]
		[Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableList/ByCompany")]
		[Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableList/ByCompany")]
		[Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableList/ByCompany")]
		[Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableList/ByCompany")]
		[Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableList/ByCompany")]
		[Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableList/ByCompany")]
		[Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableList/ByCompany")]
		[Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableList/ByCompany")]
		[Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableList/ByCompany")]
		[Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableList/ByCompany")]
		[Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementTableList/ByCompany")]

		public ActionResult GetGuarantorAgreementExtendTableList([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetGuarantorAgreementTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/Extend/GetGuarantorAgreementTableById/id={id}")]
		[Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]

		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/Function/extendguarantor/GetExtendGuarantorAgreementById/id={id}")]
		public ActionResult GetGuarantorAgreementExtendTableById(string id)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.GetGuarantorAgreementTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAccessMode/id={id}")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAccessMode/id={id}")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAccessMode/id={id}")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAccessMode/id={id}")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAccessMode/id={id}")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAccessMode/id={id}")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAccessMode/id={id}")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAccessMode/id={id}")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAccessMode/id={id}")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAccessMode/id={id}")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAccessMode/id={id}")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAccessMode/id={id}")]

		public ActionResult GetGuarantorAgreementLineAccessMode(string id)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.GetGuarantorAgreementLineAccessMode(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		
		[HttpGet]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleGenerateFunction/id={id}")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleGenerateFunction/id={id}")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleGenerateFunction/id={id}")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleGenerateFunction/id={id}")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleGenerateFunction/id={id}")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleGenerateFunction/id={id}")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleGenerateFunction/id={id}")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleGenerateFunction/id={id}")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleGenerateFunction/id={id}")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleGenerateFunction/id={id}")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleGenerateFunction/id={id}")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleGenerateFunction/id={id}")]

		public ActionResult GetVisibleGenerateFunction(string id)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);

				return Ok(guarantorAgreementTableService.GetVisibleGenerateFunction(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleManageMenu/id={id}")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleManageMenu/id={id}")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleManageMenu/id={id}")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleManageMenu/id={id}")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleManageMenu/id={id}")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleManageMenu/id={id}")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleManageMenu/id={id}")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleManageMenu/id={id}")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleManageMenu/id={id}")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleManageMenu/id={id}")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleManageMenu/id={id}")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetVisibleManageMenu/id={id}")]

		public ActionResult GetVisibleManageMenu(string id)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);

				return Ok(guarantorAgreementTableService.GetVisibleManageMenuByGuarantor(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/ValidateIsManualGuarantorNumberSeq/productType={productType}")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/ValidateIsManualGuarantorNumberSeq/productType={productType}")]

		public ActionResult ValidateIsManualGuarantorNumberSeq(int productType)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.ValidateIsManualGuarantorNumberSeq(productType));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetAccessModeByGuarantorAgreement/id={id}")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetAccessModeByGuarantorAgreement/id={id}")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetAccessModeByGuarantorAgreement/id={id}")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetAccessModeByGuarantorAgreement/id={id}")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetAccessModeByGuarantorAgreement/id={id}")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetAccessModeByGuarantorAgreement/id={id}")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetAccessModeByGuarantorAgreement/id={id}")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetAccessModeByGuarantorAgreement/id={id}")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetAccessModeByGuarantorAgreement/id={id}")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetAccessModeByGuarantorAgreement/id={id}")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetAccessModeByGuarantorAgreement/id={id}")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetAccessModeByGuarantorAgreement/id={id}")]
		public ActionResult GetAccessModeByGuarantorAgreement(string id)
		{
			try
			{
				IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db);
				var result = GuarantorAgreementTableService.GetAccessModeByGuarantorAgreement(id);
				return Ok(result);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCreditAppDropdownByProductMainAgreementDropDown")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCreditAppDropdownByProductMainAgreementDropDown")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCreditAppDropdownByProductMainAgreementDropDown")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCreditAppDropdownByProductMainAgreementDropDown")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCreditAppDropdownByProductMainAgreementDropDown")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCreditAppDropdownByProductMainAgreementDropDown")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCreditAppDropdownByProductMainAgreementDropDown")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCreditAppDropdownByProductMainAgreementDropDown")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCreditAppDropdownByProductMainAgreementDropDown")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCreditAppDropdownByProductMainAgreementDropDown")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCreditAppDropdownByProductMainAgreementDropDown")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCreditAppDropdownByProductMainAgreementDropDown")]
		public ActionResult GetCreditAppDropdownByProductMainAgreementDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.GetCreditAppTableByGuarantorLineIdDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByDropDown")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByDropDown")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByDropDown")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByDropDown")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByDropDown")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByDropDown")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByDropDown")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByDropDown")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByDropDown")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByDropDown")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByDropDown")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByDropDown")]
		public ActionResult GetCustomerTableByDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion extent
		[HttpGet]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineByIdFromAffiliate/id={id}")]
		public ActionResult GetGuarantorAgreementLineByIdFromAffiliate(string id)
		{
			try
			{
				IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db);
				var result = GuarantorAgreementTableService.GetGuarantorAgreementLineByIdFromAffiliate(id);
				return Ok(result);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateList/ByCompany")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateList/ByCompany")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateList/ByCompany")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateList/ByCompany")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateList/ByCompany")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateList/ByCompany")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateList/ByCompany")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateList/ByCompany")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateList/ByCompany")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateList/ByCompany")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateList/ByCompany")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateList/ByCompany")]
		public ActionResult GetGuarantorAgreementLineAffiliateListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetGuarantorAgreementLineAffiliateListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateById/id={id}")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateById/id={id}")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateById/id={id}")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateById/id={id}")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateById/id={id}")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateById/id={id}")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateById/id={id}")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateById/id={id}")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateById/id={id}")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateById/id={id}")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateById/id={id}")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateById/id={id}")]
		public ActionResult GetGuarantorAgreementLineAffiliateById(string id)
		{
			try
			{
				IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(GuarantorAgreementTableService.GetGuarantorAgreementLineAffiliateById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetMainAgreementTableByGuarantorDropDown")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetMainAgreementTableByGuarantorDropDown")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetMainAgreementTableByGuarantorDropDown")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetMainAgreementTableByGuarantorDropDown")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetMainAgreementTableByGuarantorDropDown")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetMainAgreementTableByGuarantorDropDown")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetMainAgreementTableByGuarantorDropDown")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetMainAgreementTableByGuarantorDropDown")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetMainAgreementTableByGuarantorDropDown")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreeGuarantorAgreementTable/RelatedInfo/ExtendmentTable/GetMainAgreementTableByGuarantorDropDown")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetMainAgreementTableByGuarantorDropDown")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetMainAgreementTableByGuarantorDropDown")]
		public ActionResult GetMainAgreementTableByGuarantorDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.GetMainAgreementTableByGuarantorDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByMainAgreementDropDown")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByMainAgreementDropDown")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByMainAgreementDropDown")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByMainAgreementDropDown")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByMainAgreementDropDown")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByMainAgreementDropDown")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByMainAgreementDropDown")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByMainAgreementDropDown")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByMainAgreementDropDown")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByMainAgreementDropDown")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByMainAgreementDropDown")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetCustomerTableByMainAgreementDropDown")]
		public ActionResult GetCustomerTableByMainAgreementDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.GetCustomerTableByMainAgreementDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateNoticeOfCancellationValidation/id={id}")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateNoticeOfCancellationValidation/id={id}")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateNoticeOfCancellationValidation/id={id}")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateNoticeOfCancellationValidation/id={id}")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateNoticeOfCancellationValidation/id={id}")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateNoticeOfCancellationValidation/id={id}")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateNoticeOfCancellationValidation/id={id}")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateNoticeOfCancellationValidation/id={id}")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateNoticeOfCancellationValidation/id={id}")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateNoticeOfCancellationValidation/id={id}")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateNoticeOfCancellationValidation/id={id}")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateNoticeOfCancellationValidation/id={id}")]
		public ActionResult GetGenerateNoticeOfCancellationValidation(string id)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IGuarantorAgreementTableService GuarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
					return Ok(GuarantorAgreementTableService.GetGenerateNoticeOfCancellationValidation(id));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGuarantorAgreementLineAffiliateInitialData/id={id}")]

		public ActionResult GetGuarantorAgreementLineAffiliateInitialData(string id)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.GetGuarantorAgreementLineAffiliateInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/getCustomerTableByGuarantorIdDropDown")]
		[Route("Factoring/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/getCustomerTableByGuarantorIdDropDown")]
		[Route("Bond/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/getCustomerTableByGuarantorIdDropDown")]
		[Route("Bond/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/getCustomerTableByGuarantorIdDropDown")]
		[Route("HirePurchase/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/getCustomerTableByGuarantorIdDropDown")]
		[Route("HirePurchase/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/getCustomerTableByGuarantorIdDropDown")]
		[Route("LCDLC/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/getCustomerTableByGuarantorIdDropDown")]
		[Route("LCDLC/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/getCustomerTableByGuarantorIdDropDown")]
		[Route("Leasing/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/getCustomerTableByGuarantorIdDropDown")]
		[Route("Leasing/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/getCustomerTableByGuarantorIdDropDown")]
		[Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/getCustomerTableByGuarantorIdDropDown")]
		[Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/getCustomerTableByGuarantorIdDropDown")]
		public ActionResult GetCustomerTableByGuarantorIdDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.GetCustomerTableByGuarantorIdDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("Bond/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateExtendGuarantorAgreementValidation/id={id}")]
		[Route("Bond/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateExtendGuarantorAgreementValidation/id={id}")]
		[Route("Factoring/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateExtendGuarantorAgreementValidation/id={id}")]
		[Route("Factoring/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateExtendGuarantorAgreementValidation/id={id}")]
		[Route("HirePurchase/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateExtendGuarantorAgreementValidation/id={id}")]
		[Route("HirePurchase/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateExtendGuarantorAgreementValidation/id={id}")]
		[Route("LCDLC/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateExtendGuarantorAgreementValidation/id={id}")]
		[Route("LCDLC/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateExtendGuarantorAgreementValidation/id={id}")]
		[Route("Leasing/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateExtendGuarantorAgreementValidation/id={id}")]
		[Route("Leasing/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateExtendGuarantorAgreementValidation/id={id}")]
		[Route("ProjectFinance/allmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateExtendGuarantorAgreementValidation/id={id}")]
		[Route("ProjectFinance/newmainagreementtable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/Extend/GetGenerateExtendGuarantorAgreementValidation/id={id}")]
		public ActionResult GetGenerateExtendGuarantorAgreementValidation(string id)
		{
			try
			{
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				return Ok(guarantorAgreementTableService.GetGenerateExtendGuarantorAgreementValidation(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
