﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class WithholdingTaxTableController : BaseControllerV2
	{

		public WithholdingTaxTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetWithholdingTaxTableList/ByCompany")]
		public ActionResult GetWithholdingTaxTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetWithholdingTaxTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetWithholdingTaxTableById/id={id}")]
		public ActionResult GetWithholdingTaxTableById(string id)
		{
			try
			{
				IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db);
				return Ok(withholdingTaxTableService.GetWithholdingTaxTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateWithholdingTaxTable")]
		public ActionResult CreateWithholdingTaxTable([FromBody] WithholdingTaxTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db, SysTransactionLogService);
					return Ok(withholdingTaxTableService.CreateWithholdingTaxTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateWithholdingTaxTable")]
		public ActionResult UpdateWithholdingTaxTable([FromBody] WithholdingTaxTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db, SysTransactionLogService);
					return Ok(withholdingTaxTableService.UpdateWithholdingTaxTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteWithholdingTaxTable")]
		public ActionResult DeleteWithholdingTaxTable([FromBody] RowIdentity parm)
		{
			try
			{
				IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db, SysTransactionLogService);
				return Ok(withholdingTaxTableService.DeleteWithholdingTaxTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetWithholdingTaxValueList/ByCompany")]
		public ActionResult GetWithholdingTaxValueListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetWithholdingTaxValueListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetWithholdingTaxValueById/id={id}")]
		public ActionResult GetWithholdingTaxValueById(string id)
		{
			try
			{
				IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db);
				return Ok(withholdingTaxTableService.GetWithholdingTaxValueById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateWithholdingTaxValue")]
		public ActionResult CreateWithholdingTaxValue([FromBody] WithholdingTaxValueItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db, SysTransactionLogService);
					return Ok(withholdingTaxTableService.CreateWithholdingTaxValue(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateWithholdingTaxValue")]
		public ActionResult UpdateWithholdingTaxValue([FromBody] WithholdingTaxValueItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db, SysTransactionLogService);
					return Ok(withholdingTaxTableService.UpdateWithholdingTaxValue(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteWithholdingTaxValue")]
		public ActionResult DeleteWithholdingTaxValue([FromBody] RowIdentity parm)
		{
			try
			{
				IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db, SysTransactionLogService);
				return Ok(withholdingTaxTableService.DeleteWithholdingTaxValue(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetWithholdingTaxValueInitialData/id={id}")]
		public ActionResult GetWithholdingTaxValueInitialData(string id)
		{
			try
			{
				IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db, SysTransactionLogService);
				return Ok(withholdingTaxTableService.GetWithholdingTaxValueInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetWithholdingTaxTableDropDown")]
		public ActionResult GetWithholdingTaxTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
	}
}
