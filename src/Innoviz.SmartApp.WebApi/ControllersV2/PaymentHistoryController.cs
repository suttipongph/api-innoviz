using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class PaymentHistoryController : BaseControllerV2
	{

		public PaymentHistoryController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetPaymentHistoryList/ByCompany")]
		public ActionResult GetPaymentHistoryListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				var result = sysListViewService.GetPaymentHistoryListvw(search);
				return Ok(result);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetCreditAppTableDropDown")]
		public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceTableDropDown")]
		public ActionResult GetInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetCustomerTableDropDown")]
		public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetPaymentHistoryById/id={id}")]
		public ActionResult GetPaymentHistoryById(string id)
		{
			try
			{
				IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db);
				return Ok(paymentHistoryService.GetPaymentHistoryById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreatePaymentHistory")]
		public ActionResult CreatePaymentHistory([FromBody] PaymentHistoryItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db, SysTransactionLogService);
					return Ok(paymentHistoryService.CreatePaymentHistory(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdatePaymentHistory")]
		public ActionResult UpdatePaymentHistory([FromBody] PaymentHistoryItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db, SysTransactionLogService);
					return Ok(paymentHistoryService.UpdatePaymentHistory(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeletePaymentHistory")]
		public ActionResult DeletePaymentHistory([FromBody] RowIdentity parm)
		{
			try
			{
				IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db, SysTransactionLogService);
				return Ok(paymentHistoryService.DeletePaymentHistory(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
