using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class IntercompanyInvoiceTableController : BaseControllerV2
	{

		public IntercompanyInvoiceTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetIntercompanyInvoiceTableList/ByCompany")]
		public ActionResult GetIntercompanyInvoiceTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetIntercompanyInvoiceTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetIntercompanyInvoiceTableById/id={id}")]
		public ActionResult GetIntercompanyInvoiceTableById(string id)
		{
			try
			{
				IIntercompanyInvoiceTableService intercompanyInvoiceTableService = new IntercompanyInvoiceTableService(db);
				return Ok(intercompanyInvoiceTableService.GetIntercompanyInvoiceTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		

		[HttpGet]
		[Route("GetValidateAdjustFunction/id={id}")]
		public ActionResult GetValidateAdjustFunction(string id)
		{
			try
			{
				IIntercompanyInvoiceTableService intercompanyInvoiceTableService = new IntercompanyInvoiceTableService(db);
				return Ok(intercompanyInvoiceTableService.GetValidateAdjustFunction(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Function/Transactionadjustment/GetAdjustIntercompanyInvoiceById/id={id}")]
		public ActionResult GetAdjustIntercompanyInvoiceById(string id)
		{
			try
			{
				IIntercompanyInvoiceAdjustmentService adjustintercompanyInvoiceTableService = new IntercompanyInvoiceAdjustmentService(db);
				return Ok(adjustintercompanyInvoiceTableService.GetAdjustIntercompanyInvoiceById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Function/Transactionadjustment/GetIntercompanyById/id={id}")]
		public ActionResult GetIntercompanyById(string id)
		{
			try
			{
				IIntercompanyInvoiceTableService intercompanyInvoiceTableService = new IntercompanyInvoiceTableService(db);
				return Ok(intercompanyInvoiceTableService.GetIntercompanyById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Function/Transactionadjustment/UpdateAdjustIntercompanyInvoice")]
		public ActionResult UpdateAdjustIntercompanyInvoice([FromBody] IntercompanyInvoiceAdjustmentItemView vwModel)
		{
			try
			{
				IIntercompanyInvoiceAdjustmentService adjustintercompanyInvoiceTableService = new IntercompanyInvoiceAdjustmentService(db);
				return Ok(adjustintercompanyInvoiceTableService.UpdateAdjustIntercompanyInvoice(vwModel));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Function/Transactionadjustment/GetDocumentReasonDropDown")]
		public ActionResult GetDocumentReasonDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateIntercompanyInvoiceTable")]
		public ActionResult CreateIntercompanyInvoiceTable([FromBody] IntercompanyInvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IIntercompanyInvoiceTableService intercompanyInvoiceTableService = new IntercompanyInvoiceTableService(db, SysTransactionLogService);
					return Ok(intercompanyInvoiceTableService.CreateIntercompanyInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateIntercompanyInvoiceTable")]
		public ActionResult UpdateIntercompanyInvoiceTable([FromBody] IntercompanyInvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IIntercompanyInvoiceTableService intercompanyInvoiceTableService = new IntercompanyInvoiceTableService(db, SysTransactionLogService);
					return Ok(intercompanyInvoiceTableService.UpdateIntercompanyInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteIntercompanyInvoiceTable")]
		public ActionResult DeleteIntercompanyInvoiceTable([FromBody] RowIdentity parm)
		{
			try
			{
				IIntercompanyInvoiceTableService intercompanyInvoiceTableService = new IntercompanyInvoiceTableService(db, SysTransactionLogService);
				return Ok(intercompanyInvoiceTableService.DeleteIntercompanyInvoiceTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown
		[HttpPost]
		[Route("GetCustomerTableDropDown")]
		public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetCreditAppTableDropDown")]
		public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/intercompanyinvoicesettlement/GetMethodOfPaymentDropDown")]
		public ActionResult GetMethodOfPaymentDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#region intercompany invoice adjustment
		[HttpPost]
		[Route("RelatedInfo/intercompanyinvoiceadjustment/GetIntercompanyInvoiceAdjustmentList/ByCompany")]
		public ActionResult GetIntercompanyInvoiceAdjustmentListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetIntercompanyInvoiceAdjustmentListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/intercompanyinvoiceadjustment/GetIntercompanyInvoiceAdjustmentById/id={id}")]
		public ActionResult GetIntercompanyInvoiceAdjustmentById(string id)
		{
			try
			{
				IIntercompanyInvoiceAdjustmentService intercompanyInvoiceAdjustmentService = new IntercompanyInvoiceAdjustmentService(db);
				return Ok(intercompanyInvoiceAdjustmentService.GetIntercompanyInvoiceAdjustmentById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#region intercompany invoice settlement
		[HttpPost]
		[Route("RelatedInfo/intercompanyinvoicesettlement/GetIntercompanyInvoiceSettlementList/ByCompany")]
		public ActionResult GetIntercompanyInvoiceSettlementListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetIntercompanyInvoiceSettlementListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/intercompanyinvoicesettlement/GetIntercompanyInvoiceSettlementById/id={id}")]
		public ActionResult GetIntercompanyInvoiceSettlementById(string id)
		{
			try
			{
				IIntercompanyInvoiceSettlementService intercompanyInvoiceSettlementService = new IntercompanyInvoiceSettlementService(db);
				return Ok(intercompanyInvoiceSettlementService.GetIntercompanyInvoiceSettlementById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/intercompanyinvoicesettlement/GetIntercompanyInvoiceSettlementInitialData/id={id}")]
		public ActionResult GetIntercompanyInvoiceSettlementInitialData(string id)
		{
			try
			{
				IIntercompanyInvoiceSettlementService intercompanyInvoiceSettlementService = new IntercompanyInvoiceSettlementService(db);
				return Ok(intercompanyInvoiceSettlementService.GetIntercompanyInvoiceSettlementInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("RelatedInfo/intercompanyinvoicesettlement/CreateIntercompanyInvoiceSettlement")]
		public ActionResult CreateIntercompanyInvoiceSettlement([FromBody] IntercompanyInvoiceSettlementItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IIntercompanyInvoiceSettlementService intercompanyInvoiceSettlementService = new IntercompanyInvoiceSettlementService(db, SysTransactionLogService);
					return Ok(intercompanyInvoiceSettlementService.CreateIntercompanyInvoiceSettlement(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/intercompanyinvoicesettlement/UpdateIntercompanyInvoiceSettlement")]
		public ActionResult UpdateIntercompanyInvoiceSettlement([FromBody] IntercompanyInvoiceSettlementItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IIntercompanyInvoiceSettlementService intercompanyInvoiceSettlementService = new IntercompanyInvoiceSettlementService(db, SysTransactionLogService);
					return Ok(intercompanyInvoiceSettlementService.UpdateIntercompanyInvoiceSettlement(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/intercompanyinvoicesettlement/DeleteIntercompanyInvoiceSettlement")]
		public ActionResult DeleteIntercompanyInvoiceSettlement([FromBody] RowIdentity parm)
		{
			try
			{
				IIntercompanyInvoiceSettlementService intercompanyInvoiceSettlementService = new IntercompanyInvoiceSettlementService(db, SysTransactionLogService);
				return Ok(intercompanyInvoiceSettlementService.DeleteIntercompanyInvoiceSettlement(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/intercompanyinvoicesettlement/GetAccessModeByIntercompanyInvoiceTable/id={id}")]
		public ActionResult GetAccessModeByIntercompanyInvoiceTable(string id)
		{
			try
			{
				IIntercompanyInvoiceSettlementService intercompanyInvoiceSettlementService = new IntercompanyInvoiceSettlementService(db);
				return Ok(intercompanyInvoiceSettlementService.GetAccessModeByIntercompanyInvoiceTable(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/intercompanyinvoicesettlement/RelatedInfo/IntercompanyInvoiceSettlementStaging/GetStagingTableIntercoInvSettleList/ByCompany")]
		public ActionResult GetStagingTableIntercoInvSettleListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetStagingTableIntercoInvSettleListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/intercompanyinvoicesettlement/RelatedInfo/IntercompanyInvoiceSettlementStaging/GetStagingTableIntercoInvSettleById/id={id}")]
		public ActionResult GetStagingTableIntercoInvSettleById(string id)
		{
			try
			{
				IStagingTableIntercoInvSettleService stagingTableIntercoInvSettleService = new StagingTableIntercoInvSettleService(db);
				return Ok(stagingTableIntercoInvSettleService.GetStagingTableIntercoInvSettleById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Function
		#region Post intercompany inv settlement
		[HttpGet]
		[Route("RelatedInfo/intercompanyinvoicesettlement/Function/PostIntercompanyInvSettlement/GetPostIntercompanyInvSettlementById/id={id}")]
		public ActionResult GetPostIntercompanyInvSettlementById(string id)
		{
			try
			{
				IIntercompanyInvoiceSettlementService intercompanyInvoiceSettlementService = new IntercompanyInvoiceSettlementService(db);
				return Ok(intercompanyInvoiceSettlementService.GetPostIntercompanyInvSettlementById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/intercompanyinvoicesettlement/Function/PostIntercompanyInvSettlement/PostIntercompanyInvSettlement")]
		public ActionResult PostIntercompanyInvSettlement([FromBody] PostIntercompanyInvSettlementView view)
		{
			try
			{
				IIntercompanyInvoiceSettlementService intercompanyInvoiceSettlementService = new IntercompanyInvoiceSettlementService(db);
				return Ok(intercompanyInvoiceSettlementService.PostIntercompanyInvSettlement(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Post intercompany inv settlement
		#region Cancel intercompany inv settlement
		[HttpPost]
		[Route("RelatedInfo/intercompanyinvoicesettlement/GetCancelIntercompanyInvSettlementMenuValidation")]
		public ActionResult GetCancelIntercompanyInvSettlementMenuValidation([FromBody] IntercompanyInvoiceSettlementItemView view)
		{
			try
			{
				IIntercompanyInvoiceSettlementService intercompanyInvoiceSettlementService = new IntercompanyInvoiceSettlementService(db);
				return Ok(intercompanyInvoiceSettlementService.GetCancelIntercompanyInvSettlementMenuValidation(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/intercompanyinvoicesettlement/Function/CancelIntercompanyInvSettlement/GetCancelIntercompanyInvSettlementById/id={id}")]
		public ActionResult GetCancelIntercompanyInvSettlementById(string id)
		{
			try
			{
				IIntercompanyInvoiceSettlementService intercompanyInvoiceSettlementService = new IntercompanyInvoiceSettlementService(db);
				return Ok(intercompanyInvoiceSettlementService.GetCancelIntercompanyInvSettlementById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/intercompanyinvoicesettlement/Function/CancelIntercompanyInvSettlement/CancelIntercompanyInvSettlement")]
		public ActionResult CancelIntercompanyInvSettlement([FromBody] CancelIntercompanyInvSettlementView view)
		{
			try
			{
				IIntercompanyInvoiceSettlementService intercompanyInvoiceSettlementService = new IntercompanyInvoiceSettlementService(db);
				return Ok(intercompanyInvoiceSettlementService.CancelIntercompanyInvSettlement(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/intercompanyinvoicesettlement/Function/CancelIntercompanyInvSettlement/GetDocumentReasonDropDown")]
		public ActionResult GetDocumentReasonDropDownByCancelIntercompanyInvSettlement([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Cancel intercompany inv settlement
		#endregion Function
		#endregion
	}
}
