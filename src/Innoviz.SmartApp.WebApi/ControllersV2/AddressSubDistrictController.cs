﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic; 
namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class AddressSubDistrictController : BaseControllerV2
	{
		public AddressSubDistrictController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetAddressSubDistrictList/ByCompany")]
		public ActionResult GetAddressSubDistrictByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAddressSubDistrictListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetAddressSubDistrictById/id={id}")]
		public ActionResult GetAddressSubDistrictById(string id)
		{
			try
			{
				IAddressService addressService = new AddressService(db);
				return Ok(addressService.GetAddressSubDistrictById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateAddressSubDistrict")]
		public ActionResult CreateAddressSubDistrict([FromBody] AddressSubDistrictItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressService addressService = new AddressService(db, SysTransactionLogService);
					return Ok(addressService.CreateAddressSubDistrict(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateAddressSubDistrict")]
		public ActionResult UpdateAddressSubDistrict([FromBody] AddressSubDistrictItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressService addressService = new AddressService(db, SysTransactionLogService);
					return Ok(addressService.UpdateAddressSubDistrict(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteAddressSubDistrict")]
		public ActionResult DeleteAddressSubDistrict([FromBody] RowIdentity parm)
		{
			try
			{
				IAddressService addressService = new AddressService(db, SysTransactionLogService);
				return Ok(addressService.DeleteAddressSubDistrict(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetAddressProvinceDropDown")]
		public ActionResult GetAddressProvinceDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemAddressProvince(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[Route("GetAddressDistrictDropDown")]
		public ActionResult GetAddressDistrictDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemAddressDistrict(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
