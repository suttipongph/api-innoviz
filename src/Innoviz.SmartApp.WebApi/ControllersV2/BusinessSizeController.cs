using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class BusinessSizeController : BaseControllerV2
	{

		public BusinessSizeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetBusinessSizeList/ByCompany")]
		public ActionResult GetBusinessSizeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBusinessSizeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBusinessSizeById/id={id}")]
		public ActionResult GetBusinessSizeById(string id)
		{
			try
			{
				IBusinessSizeService businessSizeService = new BusinessSizeService(db);
				return Ok(businessSizeService.GetBusinessSizeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBusinessSize")]
		public ActionResult CreateBusinessSize([FromBody] BusinessSizeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessSizeService businessSizeService = new BusinessSizeService(db, SysTransactionLogService);
					return Ok(businessSizeService.CreateBusinessSize(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBusinessSize")]
		public ActionResult UpdateBusinessSize([FromBody] BusinessSizeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessSizeService businessSizeService = new BusinessSizeService(db, SysTransactionLogService);
					return Ok(businessSizeService.UpdateBusinessSize(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBusinessSize")]
		public ActionResult DeleteBusinessSize([FromBody] RowIdentity parm)
		{
			try
			{
				IBusinessSizeService businessSizeService = new BusinessSizeService(db, SysTransactionLogService);
				return Ok(businessSizeService.DeleteBusinessSize(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
