﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class LedgerFiscalYearController : BaseControllerV2
    {
        public LedgerFiscalYearController(SmartAppContext context, ISysTransactionLogService transactionLogService)
         : base(context, transactionLogService)
        {

        }
        #region GetLedgerFiscalYear
        [HttpPost]
        [Route("GetLedgerFiscalYearList/ByCompany")]
        public ActionResult GetLedgerFiscalYearListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetLedgerFiscalYearListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetLedgerFiscalYearById/id={id}")]
        public ActionResult GetLedgerFiscalYearById(string id)
        {
            try
            {
                ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db);
                return Ok(ledgerFiscalService.GetLedgerFiscalYearById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateLedgerFiscalYear")]
        public ActionResult CreateLedgerFiscalYear([FromBody] LedgerFiscalYearItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db, SysTransactionLogService);
                    return Ok(ledgerFiscalService.CreateLedgerFiscalYear(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateLedgerFiscalYear")]
        public ActionResult UpdateLedgerFiscalYear([FromBody] LedgerFiscalYearItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db, SysTransactionLogService);
                    return Ok(ledgerFiscalService.UpdateLedgerFiscalYear(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteLedgerFiscalYear")]
        public ActionResult DeleteLedgerFiscalYear([FromBody] RowIdentity parm)
        {
            try
            {
                ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db, SysTransactionLogService);
                return Ok(ledgerFiscalService.DeleteLedgerFiscalYear(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region LedgerFiscalPeriod
        [HttpPost]
        [Route("GetLedgerFiscalPeriodList/ByCompany")]
        public ActionResult GetLedgerFiscalPeriodListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetLedgerFiscalPeriodListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetLedgerFiscalPeriodById/id={id}")]
        public ActionResult GetLedgerFiscalPeriodById(string id)
        {
            try
            {
                ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db);
                return Ok(ledgerFiscalService.GetLedgerFiscalPeriodById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateLedgerFiscalPeriod")]
        public ActionResult CreateLedgerFiscalPeriod([FromBody] LedgerFiscalPeriodItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db, SysTransactionLogService);
                    return Ok(ledgerFiscalService.CreateLedgerFiscalPeriod(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateLedgerFiscalPeriod")]
        public ActionResult UpdateLedgerFiscalPeriod([FromBody] LedgerFiscalPeriodItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db, SysTransactionLogService);
                    return Ok(ledgerFiscalService.UpdateLedgerFiscalPeriod(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteLedgerFiscalPeriod")]
        public ActionResult DeleteLedgerFiscalPeriod([FromBody] RowIdentity parm)
        {
            try
            {
                ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db, SysTransactionLogService);
                return Ok(ledgerFiscalService.DeleteLedgerFiscalPeriod(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetInitialLedgerFiscalYearData/id={id}")]
        public ActionResult GetInitialLedgerFiscalYearData(string id)
        {
            try
            {
                ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db);
                return Ok(ledgerFiscalService.GetInitialLedgerFiscalYearData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Dropdown
        [HttpPost]
        [Route("GetLedgerFiscalYearDropDown")]
        public ActionResult GetLedgerFiscalYearDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerFiscalYear(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
