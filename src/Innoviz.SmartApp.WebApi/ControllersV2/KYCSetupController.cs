using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class KYCSetupController : BaseControllerV2
	{

		public KYCSetupController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetKYCSetupList/ByCompany")]
		public ActionResult GetKYCSetupListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetKYCSetupListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetKYCSetupById/id={id}")]
		public ActionResult GetKYCSetupById(string id)
		{
			try
			{
				IKYCSetupService kycSetupService = new KYCSetupService(db);
				return Ok(kycSetupService.GetKYCSetupById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateKYCSetup")]
		public ActionResult CreateKYCSetup([FromBody] KYCSetupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IKYCSetupService kycSetupService = new KYCSetupService(db, SysTransactionLogService);
					return Ok(kycSetupService.CreateKYCSetup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateKYCSetup")]
		public ActionResult UpdateKYCSetup([FromBody] KYCSetupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IKYCSetupService kycSetupService = new KYCSetupService(db, SysTransactionLogService);
					return Ok(kycSetupService.UpdateKYCSetup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteKYCSetup")]
		public ActionResult DeleteKYCSetup([FromBody] RowIdentity parm)
		{
			try
			{
				IKYCSetupService kycSetupService = new KYCSetupService(db, SysTransactionLogService);
				return Ok(kycSetupService.DeleteKYCSetup(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
