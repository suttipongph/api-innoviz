﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModels;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.ControllersV2.Helper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class SysAccessRightController : BaseControllerV2
    {
        private readonly IControllerDiscovery controllerDisco;

        public SysAccessRightController(SmartAppContext context, ISysTransactionLogService transactionLogService, IControllerDiscovery controllerDisco)
            : base(context, transactionLogService)
        {
            this.controllerDisco = controllerDisco;
        }

        //[HttpPost]
        //[Route("SysRoleTableView")]
        //public ActionResult GetSysRoleTableList([FromBody]SearchParameter search) 
        //{ 
        //    try 
        //    {
        //        ISysAccessRightService accessRightService = new SysAccessRightService(db);
        //        int site = GetSiteHeader();
        //        return Ok(accessRightService.GetSysRoleTableList(search, site));
        //    }
        //    catch (Exception e) 
        //    {
        //        throw SmartAppUtil.AddStackTrace(e);
        //    }
        //}
        //[HttpGet]
        //[Route("SysRoleTableView/{id}")]
        //public ActionResult GetSysRoleTableById(string id) 
        //{ 
        //    try 
        //    {
        //        ISysAccessRightService accessRightService = new SysAccessRightService(db);
        //        return Ok(accessRightService.GetSysRoleTableById(id));
        //    }
        //    catch (Exception e) 
        //    {
        //        throw SmartAppUtil.AddStackTrace(e);
        //    }
        //}

        [HttpGet]
        [Route("SysFeatureRoleView/GetByUser/{userId}")]
        public ActionResult GetSysFeatureRoleViewByUserId(string userId)
        {
            try
            {
                ISysAccessRightService accessRightService = new SysAccessRightService(db);
                string companyHeader = GetCompanyFromRequest();
                if (companyHeader == null || companyHeader == "")
                {
                    return Ok(new AccessRightBU { AccessRight = new List<AccessRightView>() });
                }
                else
                {
                    int site = GetSiteHeader();
                    return Ok(accessRightService.GetAccessRightBUByUserSiteCompany(userId, site, companyHeader));
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("UserAccessRightCashier/{userId}")]
        public ActionResult GetUserAccessRightCashier(string userId)
        {
            try
            {
                ISysAccessRightService accessRightService = new SysAccessRightService(db);
                string username = GetUserNameFromToken();
                string companyHeader = GetCompanyFromRequest();
                int site = GetSiteHeader();
                UserLogonView result = accessRightService.GetUserDataAccessRightCashierAndInitDataCheck(userId, username, site, companyHeader);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("UserDefaultBranchCompanyAccessRight/{userId}")]
        public ActionResult GetUserDefaultBranchCompanyAccessRight(string userId)
        {
            try
            {
                ISysAccessRightService accessRightService = new SysAccessRightService(db);
                int site = GetSiteHeader();
                string username = GetUserNameFromToken();
                // get user settings and access rights by default company
                //SysUserSettingsView result = accessRightService.GetUserDefaultDataAccessRight(userId, site);
                UserLogonView result = accessRightService.GetUserDefaultDataAndInitDataCheck(userId, username, site);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("UserAccessRightByCompany/{userId}/{companyGUID}")]
        public ActionResult GetUserAccessRightByCompany(string userId, string companyGUID)
        {
            try
            {
                ISysAccessRightService accessRightService = new SysAccessRightService(db);
                int site = GetSiteHeader();
                string username = GetUserNameFromToken();
                return Ok(accessRightService.GetUserAccessRightByCompany(userId, username, companyGUID, site));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("UserLogonViewByCompany/{userId}/{companyGUID}")]
        public ActionResult GetUserLogonViewByCompany(string userId, string companyGUID)
        {
            try
            {
                ISysAccessRightService accessRightService = new SysAccessRightService(db);
                int site = GetSiteHeader();
                string username = GetUserNameFromToken();
                return Ok(accessRightService.GetUserLogonViewByCompany(userId, username, site, companyGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("UserAccessRightForOB/{userId}")]
        public ActionResult GetUserAccessRightForOB(string userId)
        {
            try
            {
                ISysAccessRightService accessRightService = new SysAccessRightService(db);
                int site = GetSiteHeader();
                string username = GetUserNameFromToken();
                if (site != SiteLoginValue.OB)
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00805");
                    throw ex;
                }
                else
                {
                    return Ok(accessRightService.GetUserAccessRightForOB(userId, username));
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("UserDefaultBranchCompanyMappings/{userId}")]
        public ActionResult GetUserDefaultBranchCompanyMappings(string userId)
        {
            try
            {
                ISysAccessRightService accessRightService = new SysAccessRightService(db);
                // get user settings and branch-company mapping
                SysUserSettingsItemView result = accessRightService.GetUserDefaultDataCompanyMapping(userId);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("SysFeatureRoleView/GetBySite/{site}")]
        public ActionResult GetSysFeatureRoleViewBySite(string site)
        {
            try
            {
                string companyHeader = GetCompanyFromRequest();
                ISysAccessRightService accessRightService = new SysAccessRightService(db);
                int siteLogin = GetSiteHeader();
                return Ok(accessRightService.GetSysFeatureGroupRoleViewByCompanySiteAdminRole(siteLogin, companyHeader));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        //[HttpPost]
        //[Route("CreateSysRoleTable")]
        //public ActionResult CreateSysRoleTable([FromBody]SysRoleTableView model) 
        //{ 
        //    try 
        //    {
        //        ISysAccessRightService accessRightService = new SysAccessRightService(db);
        //        SysRoleTable role = accessRightService.CreateSysRoleTable(model);
        //        return Ok(role);
        //    }
        //    catch (Exception e) 
        //    {
        //        throw SmartAppUtil.AddStackTrace(e);
        //    }
        //}
        //[HttpPost]
        //[Route("UpdateSysRoleTable")]
        //public ActionResult UpdateSysRoleTable([FromBody]SysRoleTableView model) 
        //{ 
        //    try 
        //    {
        //        ISysAccessRightService accessRightService = new SysAccessRightService(db);
        //        SysRoleTable role = accessRightService.UpdateSysRoleTable(model);
        //        return Ok(role);
        //    }
        //    catch (Exception e) 
        //    {
        //        throw SmartAppUtil.AddStackTrace(e);
        //    }
        //}
        //[HttpPost]
        //[Route("DeleteSysRoleTable")]
        //public ActionResult DeleteSysRoleTable([FromBody]Deletion item) 
        //{ 
        //    try 
        //    {
        //        ISysAccessRightService accessRightService = new SysAccessRightService(db);
        //        accessRightService.ValidateDeleteSysRoleTable(item.Guid);
        //        bool isRoleDeleted = accessRightService.DeleteSysRoleTable(item.Guid);
        //        return Ok(isRoleDeleted);
        //    }
        //    catch (Exception ex) 
        //    {
        //        throw SmartAppUtil.AddStackTrace(ex);
        //    }
        //}
        [HttpGet]
        [Route("GetExportRoleData/id={id}")]
        public ActionResult GetExportRoleData(string id)
        {
            try
            {
                ISysAccessRightService sysAccessRightService = new SysAccessRightService(db);
                return Ok(sysAccessRightService.GetExportRoleData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetSysFeatureGroupStructure")]
        public ActionResult GetSysFeatureGroupStructure()
        {
            try
            {
                ISysAccessRightService sysAccessRightService = new SysAccessRightService(db);
                return Ok(sysAccessRightService.GetRoleStructure());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region V2
        [HttpPost]
        [Route("GetSysRoleTableList/ByCompany")]
        public ActionResult GetSysRoleTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetSysRoleTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetSysRoleTableById/id={id}")]
        public ActionResult GetSysRoleTableById(string id)
        {
            try
            {
                ISysAccessRightService sysAccessRightService = new SysAccessRightService(db);
                return Ok(sysAccessRightService.GetSysRoleTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateSysRoleTable")]
        public ActionResult CreateSysRoleTable([FromBody] SysRoleTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ISysAccessRightService sysAccessRightService = new SysAccessRightService(db, SysTransactionLogService);
                    return Ok(sysAccessRightService.CreateSysRoleTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateSysRoleTable")]
        public ActionResult UpdateSysRoleTable([FromBody] SysRoleTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ISysAccessRightService sysAccessRightService = new SysAccessRightService(db, SysTransactionLogService);
                    return Ok(sysAccessRightService.UpdateSysRoleTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteSysRoleTable")]
        public ActionResult DeleteSysRoleTable([FromBody] RowIdentity parm)
        {
            try
            {
                ISysAccessRightService sysAccessRightService = new SysAccessRightService(db, SysTransactionLogService);
                return Ok(sysAccessRightService.DeleteSysRoleTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetSysFeatureGroupRoleViewList")]
        public ActionResult GetSysFeatureGroupRoleViewList()
        {
            try
            {
                ISysAccessRightService sysAccessRightService = new SysAccessRightService(db);
                return Ok(sysAccessRightService.GetSysFeatureGroupRoleViewByCompanySiteAdminRole(GetSiteHeader(), GetCompanyFromRequest()));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown

        #endregion
        [HttpPost]
        [Route("Function/ImportAccessRight")]
        public ActionResult ImportAccessRight([FromBody]ImportAccessRightView importAccessRightView)
        {
            try
            {
                ISysAccessRightService sysAccessRightService = new SysAccessRightService(db, SysTransactionLogService);
                return Ok(sysAccessRightService.ImportAccessRight(importAccessRightView));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        #endregion
        [HttpGet]
        [Route("GetSysControllerDiscoveryList")]
        public ActionResult GetSysControllerDiscoveryList()
        {
            try
            {
                ISysAccessRightService sysAccessRightService = new SysAccessRightService(db);
                var tableNames = sysAccessRightService.GetTableNamesForControllerEntityMapping();
                return Ok(controllerDisco.GetListControllers(tableNames));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
