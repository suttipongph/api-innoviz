﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public partial class MainAgreementTableController : BaseControllerV2
    {
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableList/ByCompany")]

        public ActionResult GetMainAgreementTableListByCompanyByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMainAgreementTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableById/id={id}")]
        public ActionResult GetMainAgreementTableByIdByAddendum(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetMainAgreementTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/CreateMainAgreementTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/CreateMainAgreementTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/CreateMainAgreementTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/CreateMainAgreementTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/CreateMainAgreementTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/CreateMainAgreementTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/CreateMainAgreementTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/CreateMainAgreementTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/CreateMainAgreementTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/CreateMainAgreementTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/CreateMainAgreementTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/CreateMainAgreementTable")]
        public ActionResult CreateMainAgreementTableByAddendum([FromBody] MainAgreementTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                    return Ok(mainAgreementTableService.CreateMainAgreementTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/UpdateMainAgreementTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/UpdateMainAgreementTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/UpdateMainAgreementTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/UpdateMainAgreementTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/UpdateMainAgreementTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/UpdateMainAgreementTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/UpdateMainAgreementTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/UpdateMainAgreementTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/UpdateMainAgreementTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/UpdateMainAgreementTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/UpdateMainAgreementTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/UpdateMainAgreementTable")]


        public ActionResult UpdateMainAgreementTableByAddendum([FromBody] MainAgreementTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                    return Ok(mainAgreementTableService.UpdateMainAgreementTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/DeleteMainAgreementTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/DeleteMainAgreementTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/DeleteMainAgreementTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/DeleteMainAgreementTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/DeleteMainAgreementTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/DeleteMainAgreementTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/DeleteMainAgreementTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/DeleteMainAgreementTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/DeleteMainAgreementTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/DeleteMainAgreementTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/DeleteMainAgreementTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/DeleteMainAgreementTable")]


        public ActionResult DeleteMainAgreementTableByAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.DeleteMainAgreementTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerTableDropDown")]
        // AddendumMainagreementTable RelatedInfo > InvoiceTable RelatedInfo
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetDropDownItemByMainAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]

        public ActionResult GetCreditAppTableDropDownByMainAgreementByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetDropDownItemCreditAppTableByMainAgreement(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCreditAppTableDropDown")]

        // AddendumMainagreementTable RelatedInfo > InvoiceTable RelatedInfo
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCustomerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCustomerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCustomerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCustomerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCustomerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCustomerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCustomerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetCustomerTableDropDown")]
        // AddendumMainagreementTable RelatedInfo > InvoiceTable RelatedInfo
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]

        public ActionResult GetWithdrawalTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetDropDownItemByMainAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetDocumentStatusDropDown")]

        public ActionResult GetDocumentStatusDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetDropDownItemMainTainAgreementStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        // Function Generate Addendum main agreement
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/ValidateIsManualNumberSeq/productType={productType}")]
        public ActionResult ValidateIsManualNumberSeqInternalMainAgreementByAddendum(int productType)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.IsManualInternalMainAgreement(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        public ActionResult ValidateIsManualNumberSeqMainAgreementByAddendum(int productType)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.IsManualMainAgreement(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]

        public ActionResult GetMainAgreementTableInitialDataByAddendum(int productType)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetMainAgreementTableInitialData(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        public ActionResult GetBuyerAgreementTableByBuyerTableByAddendum([FromBody] MainAgreementTableItemView model)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetBuyerAgreementTableByBuyerTable(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]

        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]

        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]

        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        public ActionResult GetAccessModeByMainAgreementTableByAddendum(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        public ActionResult GetAccessModeByMainAgreementTableByAddendumBookmarkDocumentTrans(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetAccessModeByStatusLessThanEqSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/GetVisibleManageMenu/id={id}")]
        public ActionResult GetVisibleManageMenuByAddendum(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);

                return Ok(mainAgreementTableService.GetVisibleManageMenu(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Addendum
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/GetGenerateAddendumValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/GetGenerateAddendumValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/GetGenerateAddendumValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/GetGenerateAddendumValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/GetGenerateAddendumValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/GetGenerateAddendumValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/GetGenerateAddendumValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/GetGenerateAddendumValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/GetGenerateAddendumValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/GetGenerateAddendumValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/GetGenerateAddendumValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/GetGenerateAddendumValidation")]
        public ActionResult GetGenerateAddendumValidation([FromBody] MainAgreementTableItemView view)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetGenerateAddendumValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetGenMainAgmAddendumById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetGenMainAgmAddendumById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetGenMainAgmAddendumById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetGenMainAgmAddendumById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetGenMainAgmAddendumById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetGenMainAgmAddendumById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetGenMainAgmAddendumById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetGenMainAgmAddendumById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetGenMainAgmAddendumById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetGenMainAgmAddendumById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetGenMainAgmAddendumById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetGenMainAgmAddendumById/id={id}")]
        public ActionResult GetGenMainAgmAddendumById(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetGenMainAgmAddendumById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/allmainagreementtable/RelatedInfo/addendummainagreementtable/Function/addendum/GetCreditAppRequestTableDropDown/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestTableDropDown/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestTableDropDown/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestTableDropDown/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestTableDropDown/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestTableDropDown/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestTableDropDown/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestTableDropDown/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestTableDropDown/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestTableDropDown/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestTableDropDown/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestTableDropDown/id={id}")]
        public ActionResult GetDropDownItemCreditAppRequestTable(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetDropDownItemCreditAppRequestTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/allmainagreementtable/Function/loanrequestagreement/GetCreditAppRequestIDCById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestIDCById/id={id}")]
        [Route("Bond/AllMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestIDCById/id={id}")]
        [Route("Bond/NewMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestIDCById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/loanrequestagreement/GetCreditAppRequestIDCById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/loanrequestagreement/GetCreditAppRequestIDCById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestIDCById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestIDCById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestIDCById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestIDCById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestIDCById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/loanrequestagreement/GetCreditAppRequestIDCById/id={id}")]

        [Route("Factoring/allmainagreementtable/RelatedInfo/addendummainagreementtable/Function/addendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/GetCreditAppRequestIDCById/id={id}")]
        public ActionResult GetCreditAppRequestIDCById(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetDatabyCreditAppRequestID(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/CreateGenMainAgmAddendum")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/CreateGenMainAgmAddendum")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/CreateGenMainAgmAddendum")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/CreateGenMainAgmAddendum")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/CreateGenMainAgmAddendum")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/CreateGenMainAgmAddendum")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/CreateGenMainAgmAddendum")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/CreateGenMainAgmAddendum")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/CreateGenMainAgmAddendum")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/CreateGenMainAgmAddendum")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/CreateGenMainAgmAddendum")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/Addendum/CreateGenMainAgmAddendum")]
        public ActionResult CreateGenMainAgmAddendum([FromBody] GenMainAgmAddendumView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                    return Ok(mainAgreementTableService.CreateGenMainAgmView(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Related info - addendum
        #region ServiceFeeTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        public ActionResult GetServiceFeeTransListByCompanyByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        public ActionResult GetServiceFeeTransByIdByRelatedInfoAddendum(string id)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetServiceFeeTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        public ActionResult CreateServiceFeeTransByRelatedInfoAddendum([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.CreateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        public ActionResult UpdateServiceFeeTransByRelatedInfoAddendum([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.UpdateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        public ActionResult DeleteServiceFeeTransByRelatedInfoAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                return Ok(ServiceFeeTransService.DeleteServiceFeeTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        public ActionResult GetAccessModeServiceFeeTransByMainAgreementTableByRelatedInfoAddendum(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        public ActionResult GetServiceFeeTransInitialDataByRelatedInfoAddendum(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetServiceFeeTransInitialDataByMainAgreementTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]

        public ActionResult GetCalculateFieldByRelatedInfoAddendum([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetCalculateField(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        public ActionResult ValidateServiceFeeTransByRelatedInfoAddendum([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.ValidateServiceFeeTrans(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        public ActionResult GetTaxValueByRelatedInfoAddendum([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetTaxValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        public ActionResult GetWHTValueByRelatedInfoAddendum([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetWHTValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]

        public ActionResult GetInvoiceRevenueTypeByProductTypeDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeByProductTypeAndDefaultNone(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        // AddRelatedInfoInvoiceTable
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]

        public ActionResult GetLedgerDimensionDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        public ActionResult GetWithholdingTaxTableDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByServiceFeeTransByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        public ActionResult GetOriginalInvoiceByServiceFeeTransDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetOriginalInvoiceByServiceFeeTransDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #region Related info - service fee trans
        #region InvoiceTable
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        public ActionResult GetInvoiceTableListByCompanyByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        public ActionResult GetInvoiceTableByIdByRelatedInfoAddendum(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        public ActionResult CreateInvoiceTableByRelatedInfoAddendum([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceTable(vwModel));

                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        public ActionResult UpdateInvoiceTableByRelatedInfoAddendum([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        public ActionResult DeleteInvoiceTableByRelatedInfoAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        public ActionResult GetInvoiceLineListByCompanyByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        public ActionResult DeleteInvoiceLineByRelatedInfoAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        public ActionResult CreateInvoiceLineByRelatedInfoAddendum([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        public ActionResult UpdateInvoiceLineByRelatedInfoAddendum([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        public ActionResult GetInvoiceLineByIdByRelatedInfoAddendum(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDownInvoiceLineByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropdownInvoiceTable
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        public ActionResult GetInvoiceTypeDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        public ActionResult GetBuyerInvoiceTableDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByInvoiceTableByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        public ActionResult GetCurrencyDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCurrency(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        public ActionResult GetInvoiceTableDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        public ActionResult GetProdUnitTableDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        public ActionResult GetTaxInvoiceTableDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        public ActionResult GetRefInvoiceTableDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetRefInvoiceTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        public ActionResult GetDocumentStatusByInvoiceTableDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]

        public ActionResult GetWithholdingTaxTableDropDownByInvoiceTableByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropdownInvoiceTable
        #region Related info - invoice table
        #region TaxInvoice
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        public ActionResult GetTaxInvoiceListByCompanyByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetTaxInvoiceTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        public ActionResult GetTaxInvoiceTableByIdByRelatedInfoAddendum(string id)
        {
            try
            {
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetTaxInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/InvoiceTaTaxInvoiceble/GetTaxInvoiceLineList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        public ActionResult GetTaxInvoiceLineListByCompanyByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetTaxInvoiceLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        public ActionResult GetTaxInvoiceLineByIdByRelatedInfoAddendum(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetTaxInvoiceLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusTaxInvoiceDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetDropDownItemTaxInvoiceDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDownTaxInvoiceLineByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeDropDownTaxInvoiceLineByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Report
        [HttpGet]
        //[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        public ActionResult GetPrintTaxInvoiceCopyByIdByRelatedInfoAddendum(string id)
        {
            try
            {
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetPrintTaxInvoiceCopyById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        //[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        public ActionResult PrintTaxInvoiceCopyByRelatedInfoAddendum([FromBody] PrintTaxInvoiceReportView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        public ActionResult GetPrintTaxInvoiceOriginalByIdByRelatedInfoAddendum(string id)
        {
            try
            {
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetPrintTaxInvoiceOriginalById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        //[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        public ActionResult PrintTaxInvoiceOriginalByRelatedInfoAddendum([FromBody] PrintTaxInvoiceReportView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Report

        #endregion Tax Invoice
        #region payment history
        [HttpPost]
        //[Route("GetPaymentHistoryList/ByCompany")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        public ActionResult GetPaymentHistoryListByCompanyByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                var result = sysListViewService.GetPaymentHistoryListvw(search);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("GetPaymentHistoryById/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        public ActionResult GetPaymentHistoryByIdByRelatedInfoAddendum(string id)
        {
            try
            {
                IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db);
                return Ok(paymentHistoryService.GetPaymentHistoryById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        public ActionResult CreatePaymentHistoryByRelatedInfoAddendum([FromBody] PaymentHistoryItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db, SysTransactionLogService);
                    return Ok(paymentHistoryService.CreatePaymentHistory(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        //[Route("UpdatePaymentHistory")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        public ActionResult UpdatePaymentHistoryByRelatedInfoAddendum([FromBody] PaymentHistoryItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db, SysTransactionLogService);
                    return Ok(paymentHistoryService.UpdatePaymentHistory(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        //[Route("/PaymentHistory/DeletePaymentHistory")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        public ActionResult DeletePaymentHistoryByRelatedInfoAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db, SysTransactionLogService);
                return Ok(paymentHistoryService.DeletePaymentHistory(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDownByPaymentHistoryByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        public ActionResult GetInvoiceTableDropDownByPaymentHistoryByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDownByPaymentHistoryByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CustTrans
        [HttpPost]
        //[Route("RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        public ActionResult GetCustTransListByCompanyByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        //[Route("RelatedInfo/CustTransaction/GetCustTransListByCustomer/ByCompany")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        public ActionResult GetCustTransListByCustomerByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        public ActionResult GetCustTransByIdByRelatedInfoAddendum(string id)
        {
            try
            {
                ICustTransService custTransService = new CustTransService(db);
                return Ok(custTransService.GetCustTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        public ActionResult GetCustomerTablebyCusttransByRelatedInfoAddendum(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetCustomerTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        public ActionResult GetInvoiceTablebyCusttransByRelatedInfoAddendum(string id)
        {
            try
            {

                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        public ActionResult GetReceiptTempTableDropDownByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemReceiptTempTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        // InvoiceTable RelatedInfo
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]

        public ActionResult GetDocumentStatusDropDownInvoiceTableByRelatedInfoAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Related info - invoice table
        #endregion InvoiceTable
        #endregion Related info - service fee trans

        #region Function
        #region Copy Service fee from CA
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        public ActionResult GetCopyServiceFeeCondCARequestInitialDataByAddendum(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetCopyServiceFeeCondCARequestInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        public ActionResult GetCreditAppRequestTableByCreditAppTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetDropDownItemCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        public ActionResult CreateCopyServiceFeeCondCARequestByAddendum([FromBody] MainAgreementTableItemView view)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.CreateCopyServiceFeeCondCARequest(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Copy Service fee from CA
        #endregion Function
        #endregion ServiceFeeTrans
        #region Memo
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult GetAddendumMemoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetAddendumMemoById(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateAddendumMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateAddendumMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult DeleteAddendumMemo([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetAddendumMemoTransInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Memo
        #region JointVentureTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        public ActionResult GetAddendumJointVentureTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetJointVentureTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        public ActionResult GetAddendumJointVentureTransById(string id)
        {
            try
            {
                IJointVentureTransService JointVentureTransService = new JointVentureTransService(db);
                return Ok(JointVentureTransService.GetJointVentureTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        public ActionResult CreateAddendumJointVentureTrans([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.CreateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        public ActionResult UpdateAddendumJointVentureTrans([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.UpdateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        public ActionResult DeleteAddendumJointVentureTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                return Ok(JointVentureTransService.DeleteJointVentureTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        public ActionResult GetAddendumJointVentureTransInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetJointVentureTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetAddendumJointVentureTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion JointVentureTrans        
        #region ConsortiumTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        public ActionResult GetAddendumConsortiumTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetConsortiumTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        public ActionResult GetAddendumConsortiumTransById(string id)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db);
                return Ok(ConsortiumTransService.GetConsortiumTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        public ActionResult CreateAddendumConsortiumTrans([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.CreateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        public ActionResult UpdateAddendumConsortiumTrans([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.UpdateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        public ActionResult DeleteAddendumConsortiumTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                return Ok(ConsortiumTransService.DeleteConsortiumTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        public ActionResult GetAddendumConsortiumTransInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetConsortiumTransInitialDataByMainAgreementTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetAddendumConsortiumTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        public ActionResult GetAddendumConsortiumTransConsortiumLineDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IConsortiumTableService consortiumTableService = new ConsortiumTableService(db);
                return Ok(consortiumTableService.GetDropDownItemConsortiumLineByConsortiumTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion ConsortiumTrans
        #region BookmarkDocumentTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]

        public ActionResult GetAddendumBookmarkDocumentTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBookmarkDocumentTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        public ActionResult GetAddendumBookmarkDocumentTransById(string id)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(BookmarkDocumentTransService.GetBookmarkDocumentTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        public ActionResult CreateAddendumBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.CreateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        public ActionResult UpdateAddendumBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.UpdateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        public ActionResult DeleteAddendumBookmarkDocumentTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                return Ok(BookmarkDocumentTransService.DeleteBookmarkDocumentTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        public ActionResult GetAddendumBookmarkDocumentTransInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetBookmarkDocumentTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        public ActionResult GetAddendumBookmarkDocumentTransInitialDataGuarantorAgreement(string id)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                return Ok(guarantorAgreementTableService.GetBookmarkDocumentTransInitialDataByGuarantorAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        public ActionResult GetAddendumBookmarkDocumentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
                return Ok(bookmarkDocumentService.GetDropDownItemBookmarkDocumentByMainAgreement(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        public ActionResult GetAddendumDocumentTemplateTableDropDownByDocumentType([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
                return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        public ActionResult GetAddendumDocumentTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]


        public ActionResult GetAddendumBookmarkDocumentTransDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetDropDownItemBookMarkDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region PrintBookDocTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        public ActionResult GetAddendumPrintBookDocTransValidation([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintBookDocTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        public async Task<ActionResult> PrintAddendumBookmarkDocumentTransaction([FromBody] PrintBookDocTransParm printBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region copy bookmark document from template

        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Bond/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Bond/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        public ActionResult GetAddendumBookmarkDocumentTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetDropDownItemBookmarkDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Bond/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Bond/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        public ActionResult GetAddendumCopyBookmarkDocumentValidation([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.GetCopyBookmarkDocumentValidation(param));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Bond/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Bond/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        public ActionResult CopyAddendumBookmarkDocumentFromTemplate([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.CopyBookmarkDocumentFromTemplate(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #endregion

        #endregion BookmarkDocumentTrans

        #region AgreementTableInfo
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        public ActionResult GetAddendumAgreementTableInfoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAgreementTableInfoListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        public ActionResult GetAddendumAgreementTableInfoById(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        public ActionResult CreateAddendumAgreementTableInfo([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.CreateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        public ActionResult UpdateAddendumAgreementTableInfo([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.UpdateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        public ActionResult DeleteAddendumAgreementTableInfo([FromBody] RowIdentity parm)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                return Ok(AgreementTableInfoService.DeleteAgreementTableInfo(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetAgreementTableInfoByMainAgreement/id={id}")]
        public ActionResult GetAddendumAgreementTableInfoByMainAgreement(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoByMainAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        public ActionResult GetAddendumAgreementTableInfoInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetAgreementTableInfoInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        public ActionResult GetAddendumAgreementTableInfoAddressTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressTransByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        public ActionResult GetAddendumAgreementTableInfoAuthorizedPersonTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(agreementTableInfoService.GetDropDownItemAuthorizedPersonTransByAgreementInfo(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        public ActionResult GetAddendumAgreementTableInfoAuthorizedPersonTransByBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(authorizedPersonTransService.GetDropDownItemAuthorizedPersonTransByBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetAddendumAgreementTableInfoAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        public ActionResult GetAddendumAgreementTableInfoCompanyBankDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCompanyBank(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        public ActionResult GetAddendumAgreementTableInfoCompanySignatureDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetEmployeeTableWithCompanySignatureDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        public ActionResult GetAddendumAgreementTableInfoEmployeeTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion AgreementTableInfo

        #endregion Related info - addendum
        #region Function - ManageAgreement
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/ManageAgreement")]
        public ActionResult ManageAgreementByaddendummainagreementtable([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.ManageAgreement(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        public ActionResult GetManageAgreementValidationByaddendummainagreementtable([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        public ActionResult GetManageAgreementByRefTypeByAddendummainagreementtable([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementByRefType(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/addendummainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]

        public ActionResult GetDocumentReasonDropDownByManageAgreementByAddendummainagreementtable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion Function - ManageAgreement
        #endregion
        #region PrintSetBookDocTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetPrintSetBookDocTransValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetPrintSetBookDocTransValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetPrintSetBookDocTransValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetPrintSetBookDocTransValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetPrintSetBookDocTransValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetPrintSetBookDocTransValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetPrintSetBookDocTransValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetPrintSetBookDocTransValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetPrintSetBookDocTransValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetPrintSetBookDocTransValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetPrintSetBookDocTransValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/GetPrintSetBookDocTransValidation")]
        public ActionResult GetPrintSetBookDocTransValidationByAddendum([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintSetBookDocTransValidation(printSetBookDocTransParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/AddendumMainagreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/AddendumMainagreementTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransactionByAddendum([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByRefGUID(printSetBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Attachment
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Bond/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Bond/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Hirepurchase/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Hirepurchase/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompanyByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Bond/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Hirepurchase/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Hirepurchase/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentByIdByAddendum(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/CreateAttachment")]

        [Route("Bond/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Bond/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/CreateAttachment")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/CreateAttachment")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/CreateAttachment")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/CreateAttachment")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/CreateAttachment")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/attachment/CreateAttachment")]
        public ActionResult CreateAttachmentByAddendum([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Bond/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Bond/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachmentByAddendum([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Bond/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Bond/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/DeleteAttachment")]
        public ActionResult DeleteAttachmentByAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Bond/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Bond/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/AddendumMainagreementTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefIdByAddendum([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }

}