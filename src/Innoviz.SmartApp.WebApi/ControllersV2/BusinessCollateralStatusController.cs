using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class BusinessCollateralStatusController : BaseControllerV2
	{

		public BusinessCollateralStatusController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetBusinessCollateralStatusList/ByCompany")]
		public ActionResult GetBusinessCollateralStatusListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBusinessCollateralStatusListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBusinessCollateralStatusById/id={id}")]
		public ActionResult GetBusinessCollateralStatusById(string id)
		{
			try
			{
				IBusinessCollateralStatusService businessCollateralStatusService = new BusinessCollateralStatusService(db);
				return Ok(businessCollateralStatusService.GetBusinessCollateralStatusById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBusinessCollateralStatus")]
		public ActionResult CreateBusinessCollateralStatus([FromBody] BusinessCollateralStatusItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessCollateralStatusService businessCollateralStatusService = new BusinessCollateralStatusService(db, SysTransactionLogService);
					return Ok(businessCollateralStatusService.CreateBusinessCollateralStatus(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBusinessCollateralStatus")]
		public ActionResult UpdateBusinessCollateralStatus([FromBody] BusinessCollateralStatusItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessCollateralStatusService businessCollateralStatusService = new BusinessCollateralStatusService(db, SysTransactionLogService);
					return Ok(businessCollateralStatusService.UpdateBusinessCollateralStatus(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBusinessCollateralStatus")]
		public ActionResult DeleteBusinessCollateralStatus([FromBody] RowIdentity parm)
		{
			try
			{
				IBusinessCollateralStatusService businessCollateralStatusService = new BusinessCollateralStatusService(db, SysTransactionLogService);
				return Ok(businessCollateralStatusService.DeleteBusinessCollateralStatus(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
