using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class DocumentReturnTableController : BaseControllerV2
	{

		public DocumentReturnTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetDocumentReturnLineList/ByCompany")]
		public ActionResult GetDocumentReturnLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetDocumentReturnLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetDocumentReturnLineById/id={id}")]
		public ActionResult GetDocumentReturnLineById(string id)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				return Ok(documentReturnTableService.GetDocumentReturnLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("ValidateIsManualNumberSeq/companyId={companyId}")]
		public ActionResult ValidateIsManualNumberSeq(string companyId)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				return Ok(documentReturnTableService.ValidateIsManualNumberSeq(companyId));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetInitialDataDocumentReturnTable/id={id}")]
		public ActionResult InitialDataDocumentReturnTable(string id)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				return Ok(documentReturnTableService.InitialDataDocumentReturnTable(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
		[HttpGet]
		[Route("GetAccessModeByDocumentReturnTable/id={id}")]
		public ActionResult GetAccessModeByDocumentReturnTable(string id)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				return Ok(documentReturnTableService.GetAccessModeByDocumentReturnTable(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetAddressTransByBuyerDropDown")]

		public ActionResult GetAddressTransByBuyerDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				return Ok(documentReturnTableService.GetDropDownItemAddressTransByBuyer(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetAddressTransByCustomerDropDown")]

		public ActionResult GetAddressTransByCustomerDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				return Ok(documentReturnTableService.GetAddressTransByCustomerDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateDocumentReturnLine")]
		public ActionResult CreateDocumentReturnLine([FromBody] DocumentReturnLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db, SysTransactionLogService);
					return Ok(documentReturnTableService.CreateDocumentReturnLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateDocumentReturnLine")]
		public ActionResult UpdateDocumentReturnLine([FromBody] DocumentReturnLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db, SysTransactionLogService);
					return Ok(documentReturnTableService.UpdateDocumentReturnLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteDocumentReturnLine")]
		public ActionResult DeleteDocumentReturnLine([FromBody] RowIdentity parm)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db, SysTransactionLogService);
				return Ok(documentReturnTableService.DeleteDocumentReturnLine(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetDocumentReturnTableList/ByCompany")]
		public ActionResult GetDocumentReturnTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetDocumentReturnTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetDocumentReturnTableById/id={id}")]
		public ActionResult GetDocumentReturnTableById(string id)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				return Ok(documentReturnTableService.GetDocumentReturnTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
		[HttpGet]
		[Route("GetInitialDatatDocumentReturnLine/id={id}")]
		public ActionResult InitialDatatDocumentReturnLine(string id)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				return Ok(documentReturnTableService.InitialDatatDocumentReturnLine(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateDocumentReturnTable")]
		public ActionResult CreateDocumentReturnTable([FromBody] DocumentReturnTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db, SysTransactionLogService);
					return Ok(documentReturnTableService.CreateDocumentReturnTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateDocumentReturnTable")]
		public ActionResult UpdateDocumentReturnTable([FromBody] DocumentReturnTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db, SysTransactionLogService);
					return Ok(documentReturnTableService.UpdateDocumentReturnTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteDocumentReturnTable")]
		public ActionResult DeleteDocumentReturnTable([FromBody] RowIdentity parm)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db, SysTransactionLogService);
				return Ok(documentReturnTableService.DeleteDocumentReturnTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetAddressTransDropDown")]
		public ActionResult GetAddressTransDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemAddressTrans(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetBuyerTableDropDown")]
		public ActionResult GetBuyerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetCustomerTableDropDown")]
		public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetDocumentReturnMethodDropDown")]
		public ActionResult GetDocumentReturnMethodDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReturnMethod(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetDocumentTypeDropDown")]
		public ActionResult GetDocumentTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetEmployeeTableDropDown")]
		public ActionResult GetEmployeeTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				return Ok(documentReturnTableService.GetDocumentStatusDocumentProcessDropdown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region function
		[HttpGet]
		[Route("function/postdocumentreturn/GetManageDocumentReturnInitialData/id={id}/statusId={statusId}")]
		[Route("function/Closedocumentreturn/GetManageDocumentReturnInitialData/id={id}/statusId={statusId}")]
		public ActionResult GetManageDocumentReturnInitialData(string id, string statusId)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				return Ok(documentReturnTableService.GetManageDocumentReturnInitialData(id, statusId));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("function/postdocumentreturn/UpdateManageDocumentReturnStatus")]
		[Route("function/Closedocumentreturn/UpdateManageDocumentReturnStatus")]
		public ActionResult UpdateManageDocumentReturnStatus([FromBody] DocumentReturnTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db, SysTransactionLogService);
					return Ok(documentReturnTableService.UpdateManageDocumentReturnStatus(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("getDocumentStatusPostValidation")]
		[Route("Function/Postdocumentreturn/getDocumentStatusPostValidation")]

		public ActionResult getDocumentStatusPostValidation([FromBody] DocumentReturnTableItemView view)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				return Ok(documentReturnTableService.GetDocumentStatusPostValidation(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("getDocumentStatusCloseValidation")]
		[Route("Function/Closedocumentreturn/getDocumentStatusCloseValidation")]
		public ActionResult getDocumentStatusColseValidation([FromBody] DocumentReturnTableItemView view)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				return Ok(documentReturnTableService.GetDocumentStatusCloseValidation(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion function
		#region Attachment
		[HttpPost]
		[Route("RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
		public ActionResult GetAttachmentListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAttachmentListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/Attachment/GetAttachmentById/id={id}")]
		public async Task<ActionResult> GetAttachmentById(string id)
		{
			try
			{
				IAttachmentService attachmentService = new AttachmentService(db);
				return Ok(await attachmentService.GetAttachmentById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/CreateAttachment")]
		public ActionResult CreateAttachment([FromBody] AttachmentItemView vwModel)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				if (ModelState.IsValid)
				{
					IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
					return Ok(attachmentService.CreateAttachment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/UpdateAttachment")]
		public ActionResult UpdateAttachment([FromBody] AttachmentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db, SysTransactionLogService);
					IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
					return Ok(attachmentService.UpdateAttachment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/DeleteAttachment")]
		public ActionResult DeleteAttachment([FromBody] RowIdentity parm)
		{
			try
			{
				IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
				return Ok(attachmentService.DeleteAttachment(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/GetAttachmentRefId")]
		public ActionResult GetAttachmentRefId([FromBody] RefIdParm parm)
		{
			try
			{
				IAttachmentService attachmentService = new AttachmentService(db);
				return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Report
		[HttpGet]
		[Route("Report/PrintDocumentReturnLabelLetter/GetPrintDocumentReturnLabelLetterById/id={id}")]
		public ActionResult GetPrintPurchaseById(string id)
		{
			try
			{
				IDocumentReturnTableService documentReturnTableService = new DocumentReturnTableService(db);
				return Ok(documentReturnTableService.GetPrintDocumentReturnLabelLetter(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
        }
        [HttpPost]
        [Route("Report/PrintDocumentReturnLabelLetter/RenderReport")]
        public ActionResult PrintPurchase([FromBody] PrintDocumentReturnLabelLetterViewReport model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        #endregion

    }
}
