using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class RegistrationTypeController : BaseControllerV2
	{

		public RegistrationTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetRegistrationTypeList/ByCompany")]
		public ActionResult GetRegistrationTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetRegistrationTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetRegistrationTypeById/id={id}")]
		public ActionResult GetRegistrationTypeById(string id)
		{
			try
			{
				IRegistrationTypeService registrationTypeService = new RegistrationTypeService(db);
				return Ok(registrationTypeService.GetRegistrationTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateRegistrationType")]
		public ActionResult CreateRegistrationType([FromBody] RegistrationTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IRegistrationTypeService registrationTypeService = new RegistrationTypeService(db, SysTransactionLogService);
					return Ok(registrationTypeService.CreateRegistrationType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateRegistrationType")]
		public ActionResult UpdateRegistrationType([FromBody] RegistrationTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IRegistrationTypeService registrationTypeService = new RegistrationTypeService(db, SysTransactionLogService);
					return Ok(registrationTypeService.UpdateRegistrationType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteRegistrationType")]
		public ActionResult DeleteRegistrationType([FromBody] RowIdentity parm)
		{
			try
			{
				IRegistrationTypeService registrationTypeService = new RegistrationTypeService(db, SysTransactionLogService);
				return Ok(registrationTypeService.DeleteRegistrationType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
