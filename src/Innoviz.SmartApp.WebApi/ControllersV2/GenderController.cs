﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class GenderController : BaseControllerV2
    {
		public GenderController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetGenderList/ByCompany")]
		public ActionResult GetGenderListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetGenderListvw(search));
				//return Ok();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetGenderById/id={id}")]
		public ActionResult GetGenderById(string id)
		{
			try
			{
				IGenderService genderService = new GenderService(db);
				return Ok(genderService.GetGenderById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateGender")]
		public ActionResult CreateGender([FromBody] GenderItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IGenderService genderService = new GenderService(db, SysTransactionLogService);
					return Ok(genderService.CreateGender(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateGender")]
		public ActionResult UpdateGender([FromBody] GenderItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IGenderService genderService = new GenderService(db, SysTransactionLogService);
					return Ok(genderService.UpdateGender(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteGender")]
		public ActionResult DeleteGender([FromBody] RowIdentity parm)
		{
			try
			{
				IGenderService genderService = new GenderService(db, SysTransactionLogService);
				return Ok(genderService.DeleteGender(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
