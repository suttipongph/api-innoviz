﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class InvoiceTypeController : BaseControllerV2
    {
        public InvoiceTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
		[HttpPost]
		[Route("GetInvoiceTypeList/ByCompany")]
		public ActionResult GetInvoiceTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetInvoiceTypeById/id={id}")]
		public ActionResult GetInvoiceTypeById(string id)
		{
			try
			{
				IInvoiceTypeService invoiceTypeService = new InvoiceTypeService(db);
				return Ok(invoiceTypeService.GetInvoiceTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateInvoiceType")]
		public ActionResult CreateInvoiceType([FromBody] InvoiceTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTypeService invoiceTypeService = new InvoiceTypeService(db, SysTransactionLogService);
					return Ok(invoiceTypeService.CreateInvoiceType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateInvoiceType")]
		public ActionResult UpdateCompany([FromBody] InvoiceTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTypeService invoiceTypeService = new InvoiceTypeService(db, SysTransactionLogService);
					return Ok(invoiceTypeService.UpdateInvoiceType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteInvoiceType")]
		public ActionResult DeleteCompanyParameter([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceTypeService invoiceTypeService = new InvoiceTypeService(db, SysTransactionLogService);
				return Ok(invoiceTypeService.DeleteInvoiceType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		[HttpPost]
		[Route("GetInvoiceRevenueTypeByProductTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeByProductType([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
				return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeByProductType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
