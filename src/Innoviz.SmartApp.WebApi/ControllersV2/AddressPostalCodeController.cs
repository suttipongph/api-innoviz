﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class AddressPostalCodeController : BaseControllerV2
    {
		public AddressPostalCodeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetAddressPostalCodeList/ByCompany")]
		public ActionResult GetAddressPostalCodeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAddressPostalCodeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetAddressPostalCodeById/id={id}")]
		public ActionResult GetAddressPostalCodeById(string id)
		{
			try
			{
				IAddressService addressService = new AddressService(db);
				return Ok(addressService.GetAddressPostalCodeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateAddressPostalCode")]
		public ActionResult CreateAddressPostalCode([FromBody] AddressPostalCodeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressService addressService = new AddressService(db, SysTransactionLogService);
					return Ok(addressService.CreateAddressPostalCode(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateAddressPostalCode")]
		public ActionResult UpdateAddressPostalCode([FromBody] AddressPostalCodeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressService addressService = new AddressService(db, SysTransactionLogService);
					return Ok(addressService.UpdateAddressPostalCode(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteAddressPostalCode")]
		public ActionResult DeleteAddressPostalCode([FromBody] RowIdentity parm)
		{
			try
			{
				IAddressService addressService = new AddressService(db, SysTransactionLogService);
				return Ok(addressService.DeleteAddressPostalCode(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown
		[HttpPost]
		[Route("GetAddressSubDistrictDropDown")]
		public ActionResult GetRelatedInfoAddressTransAddressSubDistrictDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IAddressTransService addressTransService = new AddressTransService(db);
				return Ok(addressTransService.GetDropDownItemAddressSubDistrict(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
	}
}
