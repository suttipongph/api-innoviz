using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class BookmarkDocumentController : BaseControllerV2
	{

		public BookmarkDocumentController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetBookmarkDocumentList/ByCompany")]
		public ActionResult GetBookmarkDocumentListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBookmarkDocumentListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBookmarkDocumentById/id={id}")]
		public ActionResult GetBookmarkDocumentById(string id)
		{
			try
			{
				IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
				return Ok(bookmarkDocumentService.GetBookmarkDocumentById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBookmarkDocument")]
		public ActionResult CreateBookmarkDocument([FromBody] BookmarkDocumentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db, SysTransactionLogService);
					return Ok(bookmarkDocumentService.CreateBookmarkDocument(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBookmarkDocument")]
		public ActionResult UpdateBookmarkDocument([FromBody] BookmarkDocumentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db, SysTransactionLogService);
					return Ok(bookmarkDocumentService.UpdateBookmarkDocument(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBookmarkDocument")]
		public ActionResult DeleteBookmarkDocument([FromBody] RowIdentity parm)
		{
			try
			{
				IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db, SysTransactionLogService);
				return Ok(bookmarkDocumentService.DeleteBookmarkDocument(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
