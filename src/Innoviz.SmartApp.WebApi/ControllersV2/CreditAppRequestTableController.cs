using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class CreditAppRequestTableController : BaseControllerV2
    {
        public CreditAppRequestTableController(SmartAppContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        [HttpPost]
        [Route("Factoring/GetCreditAppRequestLineList/ByCompany")]
        [Route("Bond/GetCreditAppRequestLineList/ByCompany")]
        [Route("HirePurchase/GetCreditAppRequestLineList/ByCompany")]
        [Route("LCDLC/GetCreditAppRequestLineList/ByCompany")]
        [Route("Leasing/GetCreditAppRequestLineList/ByCompany")]
        [Route("ProjectFinance/GetCreditAppRequestLineList/ByCompany")]

        public ActionResult GetCreditAppRequestLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppRequestLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/GetCreditAppRequestLineById/id={id}")]
        [Route("Bond/GetCreditAppRequestLineById/id={id}")]
        [Route("HirePurchase/GetCreditAppRequestLineById/id={id}")]
        [Route("LCDLC/GetCreditAppRequestLineById/id={id}")]
        [Route("Leasing/GetCreditAppRequestLineById/id={id}")]
        [Route("ProjectFinance/GetCreditAppRequestLineById/id={id}")]

        public ActionResult GetCreditAppRequestLineById(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetCreditAppRequestLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreateCreditAppRequestLine")]
        [Route("Bond/CreateCreditAppRequestLine")]
        [Route("HirePurchase/CreateCreditAppRequestLine")]
        [Route("LCDLC/CreateCreditAppRequestLine")]
        [Route("Leasing/CreateCreditAppRequestLine")]
        [Route("ProjectFinance/CreateCreditAppRequestLine")]

        public ActionResult CreateCreditAppRequestLine([FromBody] CreditAppRequestLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableService.CreateCreditAppRequestLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/UpdateCreditAppRequestLine")]
        [Route("Bond/UpdateCreditAppRequestLine")]
        [Route("HirePurchase/UpdateCreditAppRequestLine")]
        [Route("LCDLC/UpdateCreditAppRequestLine")]
        [Route("Leasing/UpdateCreditAppRequestLine")]
        [Route("ProjectFinance/UpdateCreditAppRequestLine")]

        public ActionResult UpdateCreditAppRequestLine([FromBody] CreditAppRequestLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableService.UpdateCreditAppRequestLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/DeleteCreditAppRequestLine")]
        [Route("Bond/DeleteCreditAppRequestLine")]
        [Route("HirePurchase/DeleteCreditAppRequestLine")]
        [Route("LCDLC/DeleteCreditAppRequestLine")]
        [Route("Leasing/DeleteCreditAppRequestLine")]
        [Route("ProjectFinance/DeleteCreditAppRequestLine")]

        public ActionResult DeleteCreditAppRequestLine([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.DeleteCreditAppRequestLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/GetCreditAppRequestLineInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("Bond/GetCreditAppRequestLineInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("HirePurchase/GetCreditAppRequestLineInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("LCDLC/GetCreditAppRequestLineInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("Leasing/GetCreditAppRequestLineInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("ProjectFinance/GetCreditAppRequestLineInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]

        public ActionResult GetCreditAppRequestLineInitialData(string creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.GetCreditAppRequestLineInitialData(creditAppRequestTableGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/GetAccessModeByCreditAppRequestTableCreditLimitType/CreditAppRequestLine/id={id}")]
        [Route("Bond/GetAccessModeByCreditAppRequestTableCreditLimitType/CreditAppRequestLine/id={id}")]
        [Route("HirePurchase/GetAccessModeByCreditAppRequestTableCreditLimitType/CreditAppRequestLine/id={id}")]
        [Route("LCDLC/GetAccessModeByCreditAppRequestTableCreditLimitType/CreditAppRequestLine/id={id}")]
        [Route("Leasing/GetAccessModeByCreditAppRequestTableCreditLimitType/CreditAppRequestLine/id={id}")]
        [Route("ProjectFinance/GetAccessModeByCreditAppRequestTableCreditLimitType/CreditAppRequestLine/id={id}")]
        public ActionResult GetAccessModeByCreditAppLineList(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableCreditLimitType(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/RetentionConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/RetentionConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/RetentionConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/RetentionConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/TaxReportTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/RetentionConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/GuarantorTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/GuarantorTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/CustvisitingTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/CustvisitingTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/CustvisitingTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/CustvisitingTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/CustvisitingTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CustvisitingTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/OwnerTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/OwnerTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/OwnerTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/OwnerTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/OwnerTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/OwnerTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/FinancialCreditTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        public ActionResult GetAccessModeByCreditAppRequestTableStatusIsLessThanWaitingForMarketingStaff(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCreditAppRequestTableList/ByCompany")]
        [Route("Bond/GetCreditAppRequestTableList/ByCompany")]
        [Route("HirePurchase/GetCreditAppRequestTableList/ByCompany")]
        [Route("LCDLC/GetCreditAppRequestTableList/ByCompany")]
        [Route("Leasing/GetCreditAppRequestTableList/ByCompany")]
        [Route("ProjectFinance/GetCreditAppRequestTableList/ByCompany")]

        public ActionResult GetCreditAppRequestTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppRequestTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/GetCreditAppRequestTableById/id={id}")]
        [Route("Bond/GetCreditAppRequestTableById/id={id}")]
        [Route("HirePurchase/GetCreditAppRequestTableById/id={id}")]
        [Route("LCDLC/GetCreditAppRequestTableById/id={id}")]
        [Route("Leasing/GetCreditAppRequestTableById/id={id}")]
        [Route("ProjectFinance/GetCreditAppRequestTableById/id={id}")]

        public ActionResult GetCreditAppRequestTableById(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetCreditAppRequestTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreateCreditAppRequestTable")]
        [Route("Bond/CreateCreditAppRequestTable")]
        [Route("HirePurchase/CreateCreditAppRequestTable")]
        [Route("LCDLC/CreateCreditAppRequestTable")]
        [Route("Leasing/CreateCreditAppRequestTable")]
        [Route("ProjectFinance/CreateCreditAppRequestTable")]

        public ActionResult CreateCreditAppRequestTable([FromBody] CreditAppRequestTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableService.CreateCreditAppRequestTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/UpdateCreditAppRequestTable")]
        [Route("Bond/UpdateCreditAppRequestTable")]
        [Route("HirePurchase/UpdateCreditAppRequestTable")]
        [Route("LCDLC/UpdateCreditAppRequestTable")]
        [Route("Leasing/UpdateCreditAppRequestTable")]
        [Route("ProjectFinance/UpdateCreditAppRequestTable")]

        public ActionResult UpdateCreditAppRequestTable([FromBody] CreditAppRequestTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableService.UpdateCreditAppRequestTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/DeleteCreditAppRequestTable")]
        [Route("Bond/DeleteCreditAppRequestTable")]
        [Route("HirePurchase/DeleteCreditAppRequestTable")]
        [Route("LCDLC/DeleteCreditAppRequestTable")]
        [Route("Leasing/DeleteCreditAppRequestTable")]
        [Route("ProjectFinance/DeleteCreditAppRequestTable")]

        public ActionResult DeleteCreditAppRequestTable([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.DeleteCreditAppRequestTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/GetCreditAppRequestTableInitialData/productType={productType}")]
        [Route("Bond/GetCreditAppRequestTableInitialData/productType={productType}")]
        [Route("HirePurchase/GetCreditAppRequestTableInitialData/productType={productType}")]
        [Route("LCDLC/GetCreditAppRequestTableInitialData/productType={productType}")]
        [Route("Leasing/GetCreditAppRequestTableInitialData/productType={productType}")]
        [Route("ProjectFinance/GetCreditAppRequestTableInitialData/productType={productType}")]

        public ActionResult GetCreditAppRequestTableInitialData(int productType)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.GetCreditAppRequestTableInitialData(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/ValidateIsManualNumberSeq/productType={productType}")]

        public ActionResult ValidateIsManualNumberSeq(int productType)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.IsManualByCreditAppRequest(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]

        [Route("Factoring/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        [Route("Bond/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        [Route("Lcdlc/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        [Route("Projectfinance/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        public ActionResult GetRetentionTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetRetentionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]

        [Route("Factoring/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        [Route("Bond/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        [Route("Leasing/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        [Route("Lcdlc/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        [Route("Projectfinance/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        public ActionResult GetRetentionTransById(string id)
        {
            try
            {
                IRetentionTransService retentionTransService = new RetentionTransService(db);
                return Ok(retentionTransService.GetRetentionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/GetDocumentStatusDropDown")]
        [Route("Bond/GetDocumentStatusDropDown")]
        [Route("HirePurchase/GetDocumentStatusDropDown")]
        [Route("LCDLC/GetDocumentStatusDropDown")]
        [Route("Leasing/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetDropDownItemCreditAppRequestStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetAddressTransByCustomerDropDown")]
        [Route("Bond/GetAddressTransByCustomerDropDown")]
        [Route("HirePurchase/GetAddressTransByCustomerDropDown")]
        [Route("LCDLC/GetAddressTransByCustomerDropDown")]
        [Route("Leasing/GetAddressTransByCustomerDropDown")]
        [Route("ProjectFinance/GetAddressTransByCustomerDropDown")]

        public ActionResult GetAddressTransByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressTransByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetAddressTransByBuyerDropDown")]
        [Route("Bond/GetAddressTransByBuyerDropDown")]
        [Route("HirePurchase/GetAddressTransByBuyerDropDown")]
        [Route("LCDLC/GetAddressTransByBuyerDropDown")]
        [Route("Leasing/GetAddressTransByBuyerDropDown")]
        [Route("ProjectFinance/GetAddressTransByBuyerDropDown")]

        public ActionResult GetAddressTransByBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressTransByBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetApplicationTableDropDown")]
        [Route("Bond/GetApplicationTableDropDown")]
        [Route("HirePurchase/GetApplicationTableDropDown")]
        [Route("LCDLC/GetApplicationTableDropDown")]
        [Route("Leasing/GetApplicationTableDropDown")]
        [Route("ProjectFinance/GetApplicationTableDropDown")]

        public ActionResult GetApplicationTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                //return Ok(sysDropDownService.GetDropDownItemApplicationTable(search));
                return Ok();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetNotCancelApplicationTableByCustomerDropDown")]
        [Route("Bond/GetNotCancelApplicationTableByCustomerDropDown")]
        [Route("HirePurchase/GetNotCancelApplicationTableByCustomerDropDown")]
        [Route("LCDLC/GetNotCancelApplicationTableByCustomerDropDown")]
        [Route("Leasing/GetNotCancelApplicationTableByCustomerDropDown")]
        [Route("ProjectFinance/GetNotCancelApplicationTableByCustomerDropDown")]

        public ActionResult GetNotCancelApplicationTableByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IApplicationService applicationService = new ApplicationService(db);
                return Ok(applicationService.GetDropDownItemNotCancelApplicationTableByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetAssignmentAgreementTableDropDown")]
        [Route("Bond/GetAssignmentAgreementTableDropDown")]
        [Route("HirePurchase/GetAssignmentAgreementTableDropDown")]
        [Route("LCDLC/GetAssignmentAgreementTableDropDown")]
        [Route("Leasing/GetAssignmentAgreementTableDropDown")]
        [Route("ProjectFinance/GetAssignmentAgreementTableDropDown")]

        public ActionResult GetAssignmentAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetAssignmentAgreementTableByBuyerDropDown")]
        [Route("Bond/GetAssignmentAgreementTableByBuyerDropDown")]
        [Route("HirePurchase/GetAssignmentAgreementTableByBuyerDropDown")]
        [Route("LCDLC/GetAssignmentAgreementTableByBuyerDropDown")]
        [Route("Leasing/GetAssignmentAgreementTableByBuyerDropDown")]
        [Route("ProjectFinance/GetAssignmentAgreementTableByBuyerDropDown")]

        public ActionResult GetAssignmentAgreementTableByBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDropDownItemAssignmentAgreementItemtNotStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetAssignmentMethodDropDown")]
        [Route("Bond/GetAssignmentMethodDropDown")]
        [Route("HirePurchase/GetAssignmentMethodDropDown")]
        [Route("LCDLC/GetAssignmentMethodDropDown")]
        [Route("Leasing/GetAssignmentMethodDropDown")]
        [Route("ProjectFinance/GetAssignmentMethodDropDown")]

        public ActionResult GetAssignmentMethodDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentMethod(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetBillingResponsibleByDropDown")]
        [Route("Bond/GetBillingResponsibleByDropDown")]
        [Route("HirePurchase/GetBillingResponsibleByDropDown")]
        [Route("LCDLC/GetBillingResponsibleByDropDown")]
        [Route("Leasing/GetBillingResponsibleByDropDown")]
        [Route("ProjectFinance/GetBillingResponsibleByDropDown")]

        public ActionResult GetBillingResponsibleByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBillingResponsibleBy(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetBlacklistStatusDropDown")]
        [Route("Bond/GetBlacklistStatusDropDown")]
        [Route("HirePurchase/GetBlacklistStatusDropDown")]
        [Route("LCDLC/GetBlacklistStatusDropDown")]
        [Route("Leasing/GetBlacklistStatusDropDown")]
        [Route("ProjectFinance/GetBlacklistStatusDropDown")]

        public ActionResult GetBlacklistStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBlacklistStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetBuyerTableDropDown")]
        [Route("Bond/GetBuyerTableDropDown")]
        [Route("HirePurchase/GetBuyerTableDropDown")]
        [Route("LCDLC/GetBuyerTableDropDown")]
        [Route("Leasing/GetBuyerTableDropDown")]
        [Route("ProjectFinance/GetBuyerTableDropDown")]

        public ActionResult GetBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetBuyerTableByCreditAppRequestDropDown")]
        [Route("Bond/GetBuyerTableByCreditAppRequestDropDown")]
        [Route("HirePurchase/GetBuyerTableByCreditAppRequestDropDown")]
        [Route("LCDLC/GetBuyerTableByCreditAppRequestDropDown")]
        [Route("Leasing/GetBuyerTableByCreditAppRequestDropDown")]
        [Route("ProjectFinance/GetBuyerTableByCreditAppRequestDropDown")]

        public ActionResult GetBuyerTableByCreditAppRequestDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetDropDownItemByCreditAppRequest(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetConsortiumTableDropDown")]
        [Route("Bond/GetConsortiumTableDropDown")]
        [Route("HirePurchase/GetConsortiumTableDropDown")]
        [Route("LCDLC/GetConsortiumTableDropDown")]
        [Route("Leasing/GetConsortiumTableDropDown")]
        [Route("ProjectFinance/GetConsortiumTableDropDown")]

        public ActionResult GetConsortiumTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemConsortiumTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetContactPersonTransByCustomerDropDown")]
        [Route("Bond/GetContactPersonTransByCustomerDropDown")]
        [Route("HirePurchase/GetContactPersonTransByCustomerDropDown")]
        [Route("LCDLC/GetContactPersonTransByCustomerDropDown")]
        [Route("Leasing/GetContactPersonTransByCustomerDropDown")]
        [Route("ProjectFinance/GetContactPersonTransByCustomerDropDown")]

        public ActionResult GetContactPersonTransByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db);
                return Ok(contactPersonTransService.GetDropDownItemContactPersonTransItemByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetContactPersonTransByBuyerDropDown")]
        [Route("Bond/GetContactPersonTransByBuyerDropDown")]
        [Route("HirePurchase/GetContactPersonTransByBuyerDropDown")]
        [Route("LCDLC/GetContactPersonTransByBuyerDropDown")]
        [Route("Leasing/GetContactPersonTransByBuyerDropDown")]
        [Route("ProjectFinance/GetContactPersonTransByBuyerDropDown")]

        public ActionResult GetContactPersonTransByBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db);
                return Ok(contactPersonTransService.GetDropDownItemContactPersonTransItemByBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCreditAppRequestTableDropDown")]
        [Route("Bond/GetCreditAppRequestTableDropDown")]
        [Route("HirePurchase/GetCreditAppRequestTableDropDown")]
        [Route("LCDLC/GetCreditAppRequestTableDropDown")]
        [Route("Leasing/GetCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/GetCreditAppRequestTableDropDown")]

        public ActionResult GetCreditAppRequestTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCreditAppTableByCreditAppRequestTableDropDown")]
        [Route("Bond/GetCreditAppTableByCreditAppRequestTableDropDown")]
        [Route("HirePurchase/GetCreditAppTableByCreditAppRequestTableDropDown")]
        [Route("LCDLC/GetCreditAppTableByCreditAppRequestTableDropDown")]
        [Route("Leasing/GetCreditAppTableByCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/GetCreditAppTableByCreditAppRequestTableDropDown")]

        public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetDropDownItemCreditAppTableByCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCreditLimitTypeByProductTypeDropDown")]
        [Route("Bond/GetCreditLimitTypeByProductTypeDropDown")]
        [Route("HirePurchase/GetCreditLimitTypeByProductTypeDropDown")]
        [Route("LCDLC/GetCreditLimitTypeByProductTypeDropDown")]
        [Route("Leasing/GetCreditLimitTypeByProductTypeDropDown")]
        [Route("ProjectFinance/GetCreditLimitTypeByProductTypeDropDown")]

        public ActionResult GetCreditLimitTypeByProductTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditLimitTypeService creditLimitTypeService = new CreditLimitTypeService(db);
                return Ok(creditLimitTypeService.GetDropDownItemCreditLimitType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCreditLimitTypeDropDown")]
        [Route("Bond/GetCreditLimitTypeDropDown")]
        [Route("HirePurchase/GetCreditLimitTypeDropDown")]
        [Route("LCDLC/GetCreditLimitTypeDropDown")]
        [Route("Leasing/GetCreditLimitTypeDropDown")]
        [Route("ProjectFinance/GetCreditLimitTypeDropDown")]

        public ActionResult GetCreditLimitTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditLimitType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCreditScoringDropDown")]
        [Route("Bond/GetCreditScoringDropDown")]
        [Route("HirePurchase/GetCreditScoringDropDown")]
        [Route("LCDLC/GetCreditScoringDropDown")]
        [Route("Leasing/GetCreditScoringDropDown")]
        [Route("ProjectFinance/GetCreditScoringDropDown")]

        public ActionResult GetCreditScoringDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditScoring(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCreditTermDropDown")]
        [Route("Bond/GetCreditTermDropDown")]
        [Route("HirePurchase/GetCreditTermDropDown")]
        [Route("LCDLC/GetCreditTermDropDown")]
        [Route("Leasing/GetCreditTermDropDown")]
        [Route("ProjectFinance/GetCreditTermDropDown")]

        public ActionResult GetCreditTermDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditTerm(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCustBankDropDown")]
        [Route("Bond/GetCustBankDropDown")]
        [Route("HirePurchase/GetCustBankDropDown")]
        [Route("LCDLC/GetCustBankDropDown")]
        [Route("Leasing/GetCustBankDropDown")]
        [Route("ProjectFinance/GetCustBankDropDown")]

        public ActionResult GetCustBankDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustBank(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCustomerTableDropDown")]
        [Route("Bond/GetCustomerTableDropDown")]
        [Route("HirePurchase/GetCustomerTableDropDown")]
        [Route("LCDLC/GetCustomerTableDropDown")]
        [Route("Leasing/GetCustomerTableDropDown")]
        [Route("ProjectFinance/GetCustomerTableDropDown")]

        public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCustomerTableByDropDown")]
        [Route("Bond/GetCustomerTableByDropDown")]
        [Route("HirePurchase/GetCustomerTableByDropDown")]
        [Route("LCDLC/GetCustomerTableByDropDown")]
        [Route("Leasing/GetCustomerTableByDropDown")]
        [Route("ProjectFinance/GetCustomerTableByDropDown")]
        [Route("Factoring/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]
        [Route("Bond/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]
        [Route("HirePurchase/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]
        [Route("LCDLC/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]
        [Route("Leasing/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]
        [Route("ProjectFinance/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]

        public ActionResult GetCustomerTableByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTableBy(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/Function/CancelCreditAppRequest/GetDocumentReasonDropDown")]
        [Route("Factoring/GetDocumentReasonDropDown")]
        [Route("Bond/GetDocumentReasonDropDown")]
        [Route("HirePurchase/GetDocumentReasonDropDown")]
        [Route("LCDLC/GetDocumentReasonDropDown")]
        [Route("Leasing/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/GetDocumentReasonDropDown")]

        public ActionResult GetDocumentReasonDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetInterestTypeDropDown")]
        [Route("Bond/GetInterestTypeDropDown")]
        [Route("HirePurchase/GetInterestTypeDropDown")]
        [Route("LCDLC/GetInterestTypeDropDown")]
        [Route("Leasing/GetInterestTypeDropDown")]
        [Route("ProjectFinance/GetInterestTypeDropDown")]

        public ActionResult GetInterestTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInterestType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetKYCSetupDropDown")]
        [Route("Bond/GetKYCSetupDropDown")]
        [Route("HirePurchase/GetKYCSetupDropDown")]
        [Route("LCDLC/GetKYCSetupDropDown")]
        [Route("Leasing/GetKYCSetupDropDown")]
        [Route("ProjectFinance/GetKYCSetupDropDown")]

        public ActionResult GetKYCSetupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemKYCSetup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetLedgerDimensionDropDown")]
        [Route("Bond/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/GetLedgerDimensionDropDown")]
        [Route("LCDLC/GetLedgerDimensionDropDown")]
        [Route("Leasing/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/GetLedgerDimensionDropDown")]

        public ActionResult GetLedgerDimensionDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetMethodOfPaymentDropDown")]
        [Route("Bond/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/GetMethodOfPaymentDropDown")]
        [Route("Leasing/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/GetMethodOfPaymentDropDown")]

        public ActionResult GetMethodOfPaymentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetProductSubTypeByProductTypeDropDown")]
        [Route("Bond/GetProductSubTypeByProductTypeDropDown")]
        [Route("HirePurchase/GetProductSubTypeByProductTypeDropDown")]
        [Route("LCDLC/GetProductSubTypeByProductTypeDropDown")]
        [Route("Leasing/GetProductSubTypeByProductTypeDropDown")]
        [Route("ProjectFinance/GetProductSubTypeByProductTypeDropDown")]

        public ActionResult GetProductSubTypeByProductTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IProductSubTypeService productSubTypeService = new ProductSubTypeService(db);
                return Ok(productSubTypeService.GetDropDownItemProductSubType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetProductSubTypeDropDown")]
        [Route("Bond/GetProductSubTypeDropDown")]
        [Route("HirePurchase/GetProductSubTypeDropDown")]
        [Route("LCDLC/GetProductSubTypeDropDown")]
        [Route("Leasing/GetProductSubTypeDropDown")]
        [Route("ProjectFinance/GetProductSubTypeDropDown")]

        public ActionResult GetProductSubTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemProductSubType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetBusinessSegmentDropDown")]
        [Route("Bond/GetBusinessSegmentDropDown")]
        [Route("HirePurchase/GetBusinessSegmentDropDown")]
        [Route("LCDLC/GetBusinessSegmentDropDown")]
        [Route("Leasing/GetBusinessSegmentDropDown")]
        [Route("ProjectFinance/GetBusinessSegmentDropDown")]

        public ActionResult GetBusinessSegmentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessSegment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetServiceFeeCondTemplateTableByProductTypeDropDown")]
        [Route("Bond/GetServiceFeeCondTemplateTableByProductTypeDropDown")]
        [Route("HirePurchase/GetServiceFeeCondTemplateTableByProductTypeDropDown")]
        [Route("LCDLC/GetServiceFeeCondTemplateTableByProductTypeDropDown")]
        [Route("Leasing/GetServiceFeeCondTemplateTableByProductTypeDropDown")]
        [Route("ProjectFinance/GetServiceFeeCondTemplateTableByProductTypeDropDown")]

        public ActionResult GetServiceFeeCondTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IServiceFeeCondTemplateTableService serviceFeeCondTemplateTableService = new ServiceFeeCondTemplateTableService(db);
                return Ok(serviceFeeCondTemplateTableService.GetDropDownItemServiceFeeCondTemplateTableByProductType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCustBankByBankAccountControlDropDown")]
        [Route("Bond/GetCustBankByBankAccountControlDropDown")]
        [Route("HirePurchase/GetCustBankByBankAccountControlDropDown")]
        [Route("LCDLC/GetCustBankByBankAccountControlDropDown")]
        [Route("Leasing/GetCustBankByBankAccountControlDropDown")]
        [Route("ProjectFinance/GetCustBankByBankAccountControlDropDown")]

        public ActionResult GetCustBankByBankAccountControlDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICustBankService custBankService = new CustBankService(db);
                return Ok(custBankService.GetDropDownItemCustBankItemByBankAccountControl(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCustBankByPDCDropDown")]
        [Route("Bond/GetCustBankByPDCDropDown")]
        [Route("HirePurchase/GetCustBankByPDCDropDown")]
        [Route("LCDLC/GetCustBankByPDCDropDown")]
        [Route("Leasing/GetCustBankByPDCDropDown")]
        [Route("ProjectFinance/GetCustBankByPDCDropDown")]

        public ActionResult GetCustBankByPDCDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICustBankService custBankService = new CustBankService(db);
                return Ok(custBankService.GetDropDownItemCustBankItemByPDC(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetInterestTypeValueByCreditAppRequest")]
        [Route("Bond/GetInterestTypeValueByCreditAppRequest")]
        [Route("HirePurchase/GetInterestTypeValueByCreditAppRequest")]
        [Route("LCDLC/GetInterestTypeValueByCreditAppRequest")]
        [Route("Leasing/GetInterestTypeValueByCreditAppRequest")]
        [Route("ProjectFinance/GetInterestTypeValueByCreditAppRequest")]

        public ActionResult GetInterestTypeValueByCreditAppRequest([FromBody] CreditAppRequestTableItemView model)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetInterestTypeValueByCreditAppRequest(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/OwnerTrans/GetRelatedPersonTableByCreditAppRequestTableOwnerTransDropDown")]
        [Route("Bond/RelatedInfo/OwnerTrans/GetRelatedPersonTableByCreditAppRequestTableOwnerTransDropDown")]
        [Route("HirePurchase/RelatedInfo/OwnerTrans/GetRelatedPersonTableByCreditAppRequestTableOwnerTransDropDown")]
        [Route("LCDLC/RelatedInfo/OwnerTrans/GetRelatedPersonTableByCreditAppRequestTableOwnerTransDropDown")]
        [Route("Leasing/RelatedInfo/OwnerTrans/GetRelatedPersonTableByCreditAppRequestTableOwnerTransDropDown")]
        [Route("ProjectFinance/RelatedInfo/OwnerTrans/GetRelatedPersonTableByCreditAppRequestTableOwnerTransDropDown")]
        public ActionResult GetRelatedPersonTableByCreditAppRequestTableOwnerTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
                return Ok(relatedPersonTableService.GetDropDownItemRelatedPersonTableItemByCreditAppRequestTableOwnerTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableByCreditAppRequestTableAuthorizedPersonTransDropDown")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableByCreditAppRequestTableAuthorizedPersonTransDropDown")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableByCreditAppRequestTableAuthorizedPersonTransDropDown")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableByCreditAppRequestTableAuthorizedPersonTransDropDown")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableByCreditAppRequestTableAuthorizedPersonTransDropDown")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableByCreditAppRequestTableAuthorizedPersonTransDropDown")]
        public ActionResult GetDropDownItemByCreditAppRequestTableAuthorizedPersonTrans([FromBody] SearchParameter search)
        {
            try
            {
                IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
                return Ok(relatedPersonTableService.GetDropDownItemByCreditAppRequestTableAuthorizedPersonTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/GuarantorTrans/GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown")]
        [Route("Bond/RelatedInfo/GuarantorTrans/GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/GetRelatedPersonTableByCreditAppRequestTableGuarantorDropDown")]
        public ActionResult GetRelatedPersonTableByCreditAppRequestTableGuarantorTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
                return Ok(relatedPersonTableService.GetDropDownItemRelatedPersonTableItemByCreditAppRequestTableGuarantor(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region NCBTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("Bond/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransList/ByCompany")]

        public ActionResult GetNCBTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetNCBTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("Bond/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("Leasing/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransById/id={id}")]
        public ActionResult GetNCBTransById(string id)
        {
            try
            {
                INCBTransService ncbTransService = new NCBTransService(db);
                return Ok(ncbTransService.GetNCBTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("Bond/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("HirePurchase/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("LCDLC/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("Leasing/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/CreateNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/CreateNCBTrans")]

        public ActionResult CreateNCBTrans([FromBody] NCBTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    INCBTransService ncbTransService = new NCBTransService(db, SysTransactionLogService);
                    return Ok(ncbTransService.CreateNCBTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("Bond/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("HirePurchase/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("LCDLC/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("Leasing/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/UpdateNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/UpdateNCBTrans")]

        public ActionResult UpdateNCBTrans([FromBody] NCBTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    INCBTransService ncbTransService = new NCBTransService(db, SysTransactionLogService);
                    return Ok(ncbTransService.UpdateNCBTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("Bond/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("HirePurchase/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("LCDLC/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("Leasing/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/DeleteNCBTrans")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/DeleteNCBTrans")]

        public ActionResult DeleteNCBTrans([FromBody] RowIdentity parm)
        {
            try
            {
                INCBTransService ncbTransService = new NCBTransService(db, SysTransactionLogService);
                return Ok(ncbTransService.DeleteNCBTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("Bond/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("HirePurchase/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("LCDLC/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("Leasing/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetBankGroupDropDown")]
        [Route("Factoring/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]
        [Route("Bond/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]
        [Route("HirePurchase/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]
        [Route("LCDLC/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]
        [Route("Leasing/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/FinancialCreditTrans/GetBankGroupDropDown")]

        public ActionResult GetBankGroupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("Bond/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("LCDLC/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("Leasing/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetCreditTypeDropDown")]
        [Route("Factoring/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        [Route("Bond/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        [Route("LCDLC/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        [Route("Leasing/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/FinancialCreditTrans/GetCreditTypeDropDown")]
        public ActionResult GetCreditTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("Bond/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("LCDLC/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("Leasing/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBAccountStatusDropDown")]

        public ActionResult GetNCBAccountStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemNCBAccountStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        public ActionResult GetNCBTransInitialDataByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetNCBTransInitialDataByCreditAppRequestTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        [HttpPost]
        [Route("Factoring/GetCreditLimitByProductByCreditAppRequest")]
        [Route("Bond/GetCreditLimitByProductByCreditAppRequest")]
        [Route("HirePurchase/GetCreditLimitByProductByCreditAppRequest")]
        [Route("LCDLC/GetCreditLimitByProductByCreditAppRequest")]
        [Route("Leasing/GetCreditLimitByProductByCreditAppRequest")]
        [Route("ProjectFinance/GetCreditLimitByProductByCreditAppRequest")]

        public ActionResult GetCreditLimitByProductByCreditAppRequest([FromBody] CreditAppRequestLineItemView model)
        {
            try
            {
                IBuyerCreditLimitByProductService buyerCreditLimitByProductService = new BuyerCreditLimitByProductService(db);
                return Ok(buyerCreditLimitByProductService.GetCreditLimitByProductByCreditAppRequest(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region AuthorizedPersonTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]

        public ActionResult GetAuthorizedPersonListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAuthorizedPersonTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]

        public ActionResult GetAuthorizedPersonById(string id)
        {
            try
            {
                IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(AuthorizedPersonTransService.GetAuthorizedPersonTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/CreateAuthorizedPersonTrans")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/CreateAuthorizedPersonTrans")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/CreateAuthorizedPersonTrans")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/CreateAuthorizedPersonTrans")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/CreateAuthorizedPersonTrans")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/CreateAuthorizedPersonTrans")]

        public ActionResult CreateAuthorizedPerson([FromBody] AuthorizedPersonTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db, SysTransactionLogService);
                    return Ok(AuthorizedPersonTransService.CreateAuthorizedPersonTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/UpdateAuthorizedPersonTrans")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/UpdateAuthorizedPersonTrans")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/UpdateAuthorizedPersonTrans")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/UpdateAuthorizedPersonTrans")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/UpdateAuthorizedPersonTrans")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/UpdateAuthorizedPersonTrans")]

        public ActionResult UpdateAuthorizedPerson([FromBody] AuthorizedPersonTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db, SysTransactionLogService);
                    return Ok(AuthorizedPersonTransService.UpdateAuthorizedPersonTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/DeleteAuthorizedPersonTrans")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/DeleteAuthorizedPersonTrans")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/DeleteAuthorizedPersonTrans")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/DeleteAuthorizedPersonTrans")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/DeleteAuthorizedPersonTrans")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/DeleteAuthorizedPersonTrans")]

        public ActionResult DeleteAuthorizedPerson([FromBody] RowIdentity parm)
        {
            try
            {
                IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db, SysTransactionLogService);
                return Ok(AuthorizedPersonTransService.DeleteAuthorizedPersonTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransInitialData/id={id}")]

        public ActionResult GetAuthorizedPersonTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAuthorizedPersonTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/RelatedInfo/NCBTrans/GetNCBTransInitialData/id={id}")]

        public ActionResult GetNCBTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetNCBTransInitialDataByAuthorizedPersonTrans(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]

        public ActionResult GetRelatedInfoAuthorizedPersonTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]
        [Route("Bond/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]
        [Route("LCDLC/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]
        [Route("Leasing/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]

        public ActionResult GetRelatedInfoAuthorizedPersonTransRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AuthorizedPersonTrans
        #region FinancialStatementTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("Bond/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        public ActionResult GetFinancialStatementTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetFinancialStatementTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("Bond/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("Leasing/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        public ActionResult GetFinancialStatementTransById(string id)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                return Ok(financialStatementTransService.GetFinancialStatementTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("Bond/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("HirePurchase/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("LCDLC/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("Leasing/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("ProjectFinance/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        public ActionResult CreateFinancialStatementTrans([FromBody] FinancialStatementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                    return Ok(financialStatementTransService.CreateFinancialStatementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("Bond/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("HirePurchase/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("LCDLC/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("Leasing/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("ProjectFinance/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        public ActionResult UpdateFinancialStatementTrans([FromBody] FinancialStatementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                    return Ok(financialStatementTransService.UpdateFinancialStatementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("Bond/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("HirePurchase/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("LCDLC/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("Leasing/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("ProjectFinance/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]

        public ActionResult DeleteFinancialStatementTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                return Ok(financialStatementTransService.DeleteFinancialStatementTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]

        public ActionResult GetFinancialStatementTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetFinancialStatementTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        public ActionResult GetFinancialStatementTransChildInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetFinancialStatementTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        #endregion
        #endregion
        #region TaxReportTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        [Route("Bond/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]

        public ActionResult GetTaxReportTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetTaxReportTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        [Route("Bond/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        [Route("Leasing/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]

        public ActionResult GetTaxReportTransById(string id)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db);
                return Ok(taxReportTransService.GetTaxReportTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        [Route("Bond/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        [Route("HirePurchase/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        [Route("LCDLC/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        [Route("Leasing/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        [Route("ProjectFinance/RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]

        public ActionResult CreateTaxReportTrans([FromBody] TaxReportTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ITaxReportTransService taxReportTransService = new TaxReportTransService(db, SysTransactionLogService);
                    return Ok(taxReportTransService.CreateTaxReportTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        [Route("Bond/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        [Route("HirePurchase/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        [Route("LCDLC/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        [Route("Leasing/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        [Route("ProjectFinance/RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]

        public ActionResult UpdateTaxReportTrans([FromBody] TaxReportTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ITaxReportTransService taxReportTransService = new TaxReportTransService(db, SysTransactionLogService);
                    return Ok(taxReportTransService.UpdateTaxReportTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        [Route("Bond/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        [Route("HirePurchase/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        [Route("LCDLC/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        [Route("Leasing/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        [Route("ProjectFinance/RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]

        public ActionResult DeleteTaxReportTrans([FromBody] RowIdentity parm)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db, SysTransactionLogService);
                return Ok(taxReportTransService.DeleteTaxReportTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]

        public ActionResult GetTaxReportTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetTaxReportTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        #endregion
        #endregion
        #region ServiceFeeConditionTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]

        public ActionResult GetServiceFeeConditionTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeConditionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]

        public ActionResult GetServiceFeeConditionTransById(string id)
        {
            try
            {
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db);
                return Ok(serviceFeeConditionTransService.GetServiceFeeConditionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]

        public ActionResult CreateServiceFeeConditionTrans([FromBody] ServiceFeeConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db, SysTransactionLogService);
                    return Ok(serviceFeeConditionTransService.CreateServiceFeeConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]

        public ActionResult UpdateServiceFeeConditionTrans([FromBody] ServiceFeeConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db, SysTransactionLogService);
                    return Ok(serviceFeeConditionTransService.UpdateServiceFeeConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]

        public ActionResult DeleteServiceFeeConditionTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db, SysTransactionLogService);
                return Ok(serviceFeeConditionTransService.DeleteServiceFeeConditionTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]

        public ActionResult GetServiceFeeConditionTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetServiceFeeConditionTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]

        public ActionResult GetServiceFeeConditionTransChildInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetServiceFeeConditionTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]

        public ActionResult GetCreditAppRequestTableProductTypeByIdServiceFeeCondition(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                var a = creditAppRequestTableService.GetCreditAppRequestTableById(id).ProductType;
                return Ok(a);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]

        public ActionResult GetDropDownInvoiceRevenueTypeByProductTypeServiceFeeCondition([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueType = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueType.GetInvoiceRevenueTypeByProductType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetDropDownInvoiceRevenueTypeServiceFeeCondition([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region RetentionConditionTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransList/ByCompany")]
        [Route("Bond/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransList/ByCompany")]

        public ActionResult GetRetentionConditionTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetRetentionConditionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransById/id={id}")]
        [Route("Bond/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransById/id={id}")]
        [Route("Leasing/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransById/id={id}")]

        public ActionResult GetRetentionConditionTransById(string id)
        {
            try
            {
                IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db);
                return Ok(retentionConditionTransService.GetRetentionConditionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/CreateRetentionConditionTrans")]
        [Route("Bond/RelatedInfo/RetentionConditionTrans/CreateRetentionConditionTrans")]
        [Route("HirePurchase/RelatedInfo/RetentionConditionTrans/CreateRetentionConditionTrans")]
        [Route("LCDLC/RelatedInfo/RetentionConditionTrans/CreateRetentionConditionTrans")]
        [Route("Leasing/RelatedInfo/RetentionConditionTrans/CreateRetentionConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/RetentionConditionTrans/CreateRetentionConditionTrans")]

        public ActionResult CreateRetentionConditionTrans([FromBody] RetentionConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db, SysTransactionLogService);
                    return Ok(retentionConditionTransService.CreateRetentionConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/UpdateRetentionConditionTrans")]
        [Route("Bond/RelatedInfo/RetentionConditionTrans/UpdateRetentionConditionTrans")]
        [Route("HirePurchase/RelatedInfo/RetentionConditionTrans/UpdateRetentionConditionTrans")]
        [Route("LCDLC/RelatedInfo/RetentionConditionTrans/UpdateRetentionConditionTrans")]
        [Route("Leasing/RelatedInfo/RetentionConditionTrans/UpdateRetentionConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/RetentionConditionTrans/UpdateRetentionConditionTrans")]

        public ActionResult UpdateRetentionConditionTrans([FromBody] RetentionConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db, SysTransactionLogService);
                    return Ok(retentionConditionTransService.UpdateRetentionConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/DeleteRetentionConditionTrans")]
        [Route("Bond/RelatedInfo/RetentionConditionTrans/DeleteRetentionConditionTrans")]
        [Route("HirePurchase/RelatedInfo/RetentionConditionTrans/DeleteRetentionConditionTrans")]
        [Route("LCDLC/RelatedInfo/RetentionConditionTrans/DeleteRetentionConditionTrans")]
        [Route("Leasing/RelatedInfo/RetentionConditionTrans/DeleteRetentionConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/RetentionConditionTrans/DeleteRetentionConditionTrans")]

        public ActionResult DeleteRetentionConditionTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db, SysTransactionLogService);
                return Ok(retentionConditionTransService.DeleteRetentionConditionTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/RetentionConditionTrans/GetRetentionConditionTransInitialData/id={id}")]

        public ActionResult GetRetentionConditionTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetRetentionConditionTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateBase")]
        [Route("Bond/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateBase")]
        [Route("HirePurchase/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateBase")]
        [Route("LCDLC/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateBase")]
        [Route("Leasing/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateBase")]
        [Route("ProjectFinance/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateBase")]

        public ActionResult GetRetentionCalculateBase([FromBody] RetentionConditionTransItemView vwModel)
        {
            try
            {
                IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db);
                return Ok(retentionConditionTransService.GetRetentionCalculateBase(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateMethod")]
        [Route("Bond/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateMethod")]
        [Route("HirePurchase/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateMethod")]
        [Route("LCDLC/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateMethod")]
        [Route("Leasing/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateMethod")]
        [Route("ProjectFinance/RelatedInfo/RetentionConditionTrans/GetRetentionCalculateMethod")]

        public ActionResult GetRetentionCalculateMethod([FromBody] RetentionConditionTransItemView vwModel)
        {
            try
            {
                IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db);
                return Ok(retentionConditionTransService.GetRetentionCalculateMethod(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        #endregion
        #endregion
        #region CustVisitingTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/CustVisitingTrans/GetCustVisitingTransList/ByCompany")]
        [Route("Bond/RelatedInfo/CustVisitingTrans/GetCustVisitingTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/CustVisitingTrans/GetCustVisitingTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/CustVisitingTrans/GetCustVisitingTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/CustVisitingTrans/GetCustVisitingTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CustVisitingTrans/GetCustVisitingTransList/ByCompany")]
        public ActionResult GetCustVisitingTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustVisitingTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CustVisitingTrans/GetCustVisitingTransById/id={id}")]
        [Route("Bond/RelatedInfo/CustVisitingTrans/GetCustVisitingTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/CustVisitingTrans/GetCustVisitingTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/CustVisitingTrans/GetCustVisitingTransById/id={id}")]
        [Route("Leasing/RelatedInfo/CustVisitingTrans/GetCustVisitingTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CustVisitingTrans/GetCustVisitingTransById/id={id}")]
        public ActionResult GetCustVisitingTransById(string id)
        {
            try
            {
                ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db);
                return Ok(custVisitingTransService.GetCustVisitingTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CustVisitingTrans/CreateCustVisitingTrans")]
        [Route("Bond/RelatedInfo/CustVisitingTrans/CreateCustVisitingTrans")]
        [Route("HirePurchase/RelatedInfo/CustVisitingTrans/CreateCustVisitingTrans")]
        [Route("LCDLC/RelatedInfo/CustVisitingTrans/CreateCustVisitingTrans")]
        [Route("Leasing/RelatedInfo/CustVisitingTrans/CreateCustVisitingTrans")]
        [Route("ProjectFinance/RelatedInfo/CustVisitingTrans/CreateCustVisitingTrans")]
        public ActionResult CreateCustVisitingTrans([FromBody] CustVisitingTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db, SysTransactionLogService);
                    return Ok(custVisitingTransService.CreateCustVisitingTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CustVisitingTrans/UpdateCustVisitingTrans")]
        [Route("Bond/RelatedInfo/CustVisitingTrans/UpdateCustVisitingTrans")]
        [Route("HirePurchase/RelatedInfo/CustVisitingTrans/UpdateCustVisitingTrans")]
        [Route("LCDLC/RelatedInfo/CustVisitingTrans/UpdateCustVisitingTrans")]
        [Route("Leasing/RelatedInfo/CustVisitingTrans/UpdateCustVisitingTrans")]
        [Route("ProjectFinance/RelatedInfo/CustVisitingTrans/UpdateCustVisitingTrans")]
        public ActionResult UpdateCustVisitingTrans([FromBody] CustVisitingTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db, SysTransactionLogService);
                    return Ok(custVisitingTransService.UpdateCustVisitingTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CustVisitingTrans/DeleteCustVisitingTrans")]
        [Route("Bond/RelatedInfo/CustVisitingTrans/DeleteCustVisitingTrans")]
        [Route("HirePurchase/RelatedInfo/CustVisitingTrans/DeleteCustVisitingTrans")]
        [Route("LCDLC/RelatedInfo/CustVisitingTrans/DeleteCustVisitingTrans")]
        [Route("Leasing/RelatedInfo/CustVisitingTrans/DeleteCustVisitingTrans")]
        [Route("ProjectFinance/RelatedInfo/CustVisitingTrans/DeleteCustVisitingTrans")]
        public ActionResult DeleteCustVisitingTrans([FromBody] RowIdentity parm)
        {
            try
            {
                ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db, SysTransactionLogService);
                return Ok(custVisitingTransService.DeleteCustVisitingTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CustVisitingTrans/GetCustVisitingTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/CustVisitingTrans/GetCustVisitingTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/CustVisitingTrans/GetCustVisitingTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/CustVisitingTrans/GetCustVisitingTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/CustVisitingTrans/GetCustVisitingTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CustVisitingTrans/GetCustVisitingTransInitialData/id={id}")]
        public ActionResult GetCustVisitingTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetCustVisitingTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/CustVisitingTrans/GetEmployeeTableDropDown")]
        [Route("Bond/RelatedInfo/CustVisitingTrans/GetEmployeeTableDropDown")]
        [Route("HirePurchase/RelatedInfo/CustVisitingTrans/GetEmployeeTableDropDown")]
        [Route("LCDLC/RelatedInfo/CustVisitingTrans/GetEmployeeTableDropDown")]
        [Route("Leasing/RelatedInfo/CustVisitingTrans/GetEmployeeTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CustVisitingTrans/GetEmployeeTableDropDown")]
        public ActionResult GetCustVisitingTransEmployeeTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region GuarantorTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/GuarantorTrans/GetGuarantorTransList/ByCompany")]
        [Route("Bond/RelatedInfo/GuarantorTrans/GetGuarantorTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/GetGuarantorTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/GetGuarantorTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/GetGuarantorTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/GetGuarantorTransList/ByCompany")]
        public ActionResult GetGuarantorTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetGuarantorTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/GuarantorTrans/GetGuarantorTransById/id={id}")]
        [Route("Bond/RelatedInfo/GuarantorTrans/GetGuarantorTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/GetGuarantorTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/GetGuarantorTransById/id={id}")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/GetGuarantorTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/GetGuarantorTransById/id={id}")]
        public ActionResult GetGuarantorTransById(string id)
        {
            try
            {
                IGuarantorTransService guarantorTransService = new GuarantorTransService(db);
                return Ok(guarantorTransService.GetGuarantorTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/GuarantorTrans/CreateGuarantorTrans")]
        [Route("Bond/RelatedInfo/GuarantorTrans/CreateGuarantorTrans")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/CreateGuarantorTrans")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/CreateGuarantorTrans")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/CreateGuarantorTrans")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/CreateGuarantorTrans")]
        public ActionResult CreateGuarantorTrans([FromBody] GuarantorTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorTransService guarantorTransService = new GuarantorTransService(db, SysTransactionLogService);
                    return Ok(guarantorTransService.CreateGuarantorTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/GuarantorTrans/UpdateGuarantorTrans")]
        [Route("Bond/RelatedInfo/GuarantorTrans/UpdateGuarantorTrans")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/UpdateGuarantorTrans")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/UpdateGuarantorTrans")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/UpdateGuarantorTrans")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/UpdateGuarantorTrans")]
        public ActionResult UpdateGuarantorTrans([FromBody] GuarantorTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorTransService guarantorTransService = new GuarantorTransService(db, SysTransactionLogService);
                    return Ok(guarantorTransService.UpdateGuarantorTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/GuarantorTrans/DeleteGuarantorTrans")]
        [Route("Bond/RelatedInfo/GuarantorTrans/DeleteGuarantorTrans")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/DeleteGuarantorTrans")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/DeleteGuarantorTrans")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/DeleteGuarantorTrans")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/DeleteGuarantorTrans")]
        public ActionResult DeleteGuarantorTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IGuarantorTransService guarantorTransService = new GuarantorTransService(db, SysTransactionLogService);
                return Ok(guarantorTransService.DeleteGuarantorTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/GuarantorTrans/GetGuarantorTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/GuarantorTrans/GetGuarantorTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/GetGuarantorTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/GetGuarantorTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/GetGuarantorTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/GetGuarantorTransInitialData/id={id}")]
        public ActionResult GetGuarantorInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetGuarantorTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/GuarantorTrans/GetGuarantorTypeDropDown")]
        [Route("Bond/RelatedInfo/GuarantorTrans/GetGuarantorTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/GetGuarantorTypeDropDown")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/GetGuarantorTypeDropDown")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/GetGuarantorTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/GetGuarantorTypeDropDown")]
        public ActionResult GetGuarantorTransGuarantorTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemGuarantorType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/GuarantorTrans/GetRelatedPersonTableDropDown")]
        [Route("Bond/RelatedInfo/GuarantorTrans/GetRelatedPersonTableDropDown")]
        [Route("HirePurchase/RelatedInfo/GuarantorTrans/GetRelatedPersonTableDropDown")]
        [Route("LCDLC/RelatedInfo/GuarantorTrans/GetRelatedPersonTableDropDown")]
        [Route("Leasing/RelatedInfo/GuarantorTrans/GetRelatedPersonTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/GuarantorTrans/GetRelatedPersonTableDropDown")]
        public ActionResult GetGuarantorTransRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region OwnerTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/OwnerTrans/GetOwnerTransList/ByCompany")]
        [Route("Bond/RelatedInfo/OwnerTrans/GetOwnerTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/OwnerTrans/GetOwnerTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/OwnerTrans/GetOwnerTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/OwnerTrans/GetOwnerTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/OwnerTrans/GetOwnerTransList/ByCompany")]
        public ActionResult GetOwnerTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetOwnerTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/OwnerTrans/GetOwnerTransById/id={id}")]
        [Route("Bond/RelatedInfo/OwnerTrans/GetOwnerTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/OwnerTrans/GetOwnerTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/OwnerTrans/GetOwnerTransById/id={id}")]
        [Route("Leasing/RelatedInfo/OwnerTrans/GetOwnerTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/OwnerTrans/GetOwnerTransById/id={id}")]
        public ActionResult GetOwnerTransById(string id)
        {
            try
            {
                IOwnerTransService ownerTransService = new OwnerTransService(db);
                return Ok(ownerTransService.GetOwnerTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/OwnerTrans/CreateOwnerTrans")]
        [Route("Bond/RelatedInfo/OwnerTrans/CreateOwnerTrans")]
        [Route("HirePurchase/RelatedInfo/OwnerTrans/CreateOwnerTrans")]
        [Route("LCDLC/RelatedInfo/OwnerTrans/CreateOwnerTrans")]
        [Route("Leasing/RelatedInfo/OwnerTrans/CreateOwnerTrans")]
        [Route("ProjectFinance/RelatedInfo/OwnerTrans/CreateOwnerTrans")]
        public ActionResult CreateOwnerTrans([FromBody] OwnerTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IOwnerTransService ownerTransService = new OwnerTransService(db, SysTransactionLogService);
                    return Ok(ownerTransService.CreateOwnerTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/OwnerTrans/UpdateOwnerTrans")]
        [Route("Bond/RelatedInfo/OwnerTrans/UpdateOwnerTrans")]
        [Route("HirePurchase/RelatedInfo/OwnerTrans/UpdateOwnerTrans")]
        [Route("LCDLC/RelatedInfo/OwnerTrans/UpdateOwnerTrans")]
        [Route("Leasing/RelatedInfo/OwnerTrans/UpdateOwnerTrans")]
        [Route("ProjectFinance/RelatedInfo/OwnerTrans/UpdateOwnerTrans")]
        public ActionResult UpdateOwnerTrans([FromBody] OwnerTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IOwnerTransService ownerTransService = new OwnerTransService(db, SysTransactionLogService);
                    return Ok(ownerTransService.UpdateOwnerTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/OwnerTrans/DeleteOwnerTrans")]
        [Route("Bond/RelatedInfo/OwnerTrans/DeleteOwnerTrans")]
        [Route("HirePurchase/RelatedInfo/OwnerTrans/DeleteOwnerTrans")]
        [Route("LCDLC/RelatedInfo/OwnerTrans/DeleteOwnerTrans")]
        [Route("Leasing/RelatedInfo/OwnerTrans/DeleteOwnerTrans")]
        [Route("ProjectFinance/RelatedInfo/OwnerTrans/DeleteOwnerTrans")]
        public ActionResult DeleteOwnerTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IOwnerTransService ownerTransService = new OwnerTransService(db, SysTransactionLogService);
                return Ok(ownerTransService.DeleteOwnerTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/OwnerTrans/GetOwnerTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/OwnerTrans/GetOwnerTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/OwnerTrans/GetOwnerTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/OwnerTrans/GetOwnerTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/OwnerTrans/GetOwnerTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/OwnerTrans/GetOwnerTransInitialData/id={id}")]
        public ActionResult GetOwnerTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetOwnerTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/OwnerTrans/GetRelatedPersonTableDropDown")]
        [Route("Bond/RelatedInfo/OwnerTrans/GetRelatedPersonTableDropDown")]
        [Route("HirePurchase/RelatedInfo/OwnerTrans/GetRelatedPersonTableDropDown")]
        [Route("LCDLC/RelatedInfo/OwnerTrans/GetRelatedPersonTableDropDown")]
        [Route("Leasing/RelatedInfo/OwnerTrans/GetRelatedPersonTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/OwnerTrans/GetRelatedPersonTableDropDown")]
        public ActionResult GetOwnerTransRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region Memo
        [HttpPost]
        [Route("Factoring/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        // child
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]

        public ActionResult GetMemoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        // child
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetMemoById(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/MemoTrans/CreateMemoTrans")]
        //child
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        // child
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        // child
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/DeleteMemoTrans")]

        public ActionResult DeleteMemo([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransChildInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.GetMemoTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Memo
        #region BuyerAgreementTrans
        [HttpPost]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]

        public ActionResult GetBuyerAgreementTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBuyerAgreementTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]

        public ActionResult GetBuyerAgreementTransById(string id)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                return Ok(buyerAgreementTransService.GetBuyerAgreementTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/CreateBuyerAgreementTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/CreateBuyerAgreementTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/CreateBuyerAgreementTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/CreateBuyerAgreementTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/CreateBuyerAgreementTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/CreateBuyerAgreementTrans")]

        public ActionResult CreateBuyerAgreementTrans([FromBody] BuyerAgreementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db, SysTransactionLogService);
                    return Ok(buyerAgreementTransService.CreateBuyerAgreementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/UpdateBuyerAgreementTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/UpdateBuyerAgreementTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/UpdateBuyerAgreementTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/UpdateBuyerAgreementTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/UpdateBuyerAgreementTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/UpdateBuyerAgreementTrans")]

        public ActionResult UpdateBuyerAgreementTrans([FromBody] BuyerAgreementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db, SysTransactionLogService);
                    return Ok(buyerAgreementTransService.UpdateBuyerAgreementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/DeleteBuyerAgreementTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/DeleteBuyerAgreementTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/DeleteBuyerAgreementTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/DeleteBuyerAgreementTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/DeleteBuyerAgreementTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/DeleteBuyerAgreementTrans")]

        public ActionResult DeleteBuyerAgreementTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db, SysTransactionLogService);
                return Ok(buyerAgreementTransService.DeleteBuyerAgreementTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransInitialData/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransInitialData/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransInitialData/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransInitialData/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransInitialData/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransInitialData/id={id}")]

        public ActionResult GetBuyerAgreementTransChildInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetBuyerAgreementTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]

        public ActionResult GetCreditAppRequestTableProductTypeByIdBuyerAgreementTrans(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                var a = creditAppRequestTableService.GetCreditAppRequestTableById(id).ProductType;
                return Ok(a);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]

        public ActionResult GetDropDownBuyerAgreementTableBuyerAgreementTrans([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementTableByBuyerAgreementTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementLineDropDown")]
        public ActionResult GetDropDownBuyerAgreementLineBuyerAgreementTrans([FromBody] SearchParameter search)
        {
            try
            {

                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementLineTableByBuyerAgreementTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region DocumentConditionTrans
        [HttpPost]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        public ActionResult GetDocumentConditionTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);

                return Ok(sysListViewService.GetDocumentConditionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        public ActionResult GetDocumentConditionTransById(string id)
        {
            try
            {
                IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db);
                return Ok(DocumentConditionTransService.GetDocumentConditionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        public ActionResult CreateDocumentConditionTrans([FromBody] DocumentConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
                    return Ok(DocumentConditionTransService.CreateDocumentConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        public ActionResult UpdateDocumentConditionTrans([FromBody] DocumentConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
                    return Ok(DocumentConditionTransService.UpdateDocumentConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        public ActionResult DeleteDocumentConditionTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
                return Ok(DocumentConditionTransService.DeleteDocumentConditionTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        public ActionResult GetDocumentConditionTransChildInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetDocumentConditionTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        public ActionResult GetRelatedInfoDocumentConditionTransDocumentTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion DocumentConditionTrans
        #region FinancialCreditTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]
        [Route("Bond/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransList/ByCompany")]

        public ActionResult GetFinancialCreditTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetFinancialCreditTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]
        [Route("Bond/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]
        [Route("Leasing/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransById/id={id}")]

        public ActionResult GetFinancialCreditTransById(string id)
        {
            try
            {
                IFinancialCreditTransService financialCreditTransService = new FinancialCreditTransService(db);
                return Ok(financialCreditTransService.GetFinancialCreditTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]
        [Route("Bond/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]
        [Route("HirePurchase/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]
        [Route("LCDLC/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]
        [Route("Leasing/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]
        [Route("ProjectFinance/RelatedInfo/FinancialCreditTrans/CreateFinancialCreditTrans")]

        public ActionResult CreateFinancialCreditTrans([FromBody] FinancialCreditTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialCreditTransService financialCreditTransService = new FinancialCreditTransService(db, SysTransactionLogService);
                    return Ok(financialCreditTransService.CreateFinancialCreditTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]
        [Route("Bond/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]
        [Route("HirePurchase/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]
        [Route("LCDLC/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]
        [Route("Leasing/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]
        [Route("ProjectFinance/RelatedInfo/FinancialCreditTrans/UpdateFinancialCreditTrans")]

        public ActionResult UpdateFinancialCreditTrans([FromBody] FinancialCreditTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialCreditTransService financialCreditTransService = new FinancialCreditTransService(db, SysTransactionLogService);
                    return Ok(financialCreditTransService.UpdateFinancialCreditTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]
        [Route("Bond/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]
        [Route("HirePurchase/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]
        [Route("LCDLC/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]
        [Route("Leasing/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]
        [Route("ProjectFinance/RelatedInfo/FinancialCreditTrans/DeleteFinancialCreditTrans")]

        public ActionResult DeleteFinancialCreditTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IFinancialCreditTransService financialCreditTransService = new FinancialCreditTransService(db, SysTransactionLogService);
                return Ok(financialCreditTransService.DeleteFinancialCreditTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/FinancialCreditTrans/GetFinancialCreditTransInitialData/id={id}")]

        public ActionResult GetFinancialCreditTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetFinancialCreditTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        #endregion
        #endregion
        #region CustBusinessCollateral
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        public ActionResult GetCustBusinessCollateralListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustBusinessCollateralListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        public ActionResult GetCustBusinessCollateralById(string id)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(CreditAppReqBusCollateralInfoService.GetCustBusinessCollateralById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown BusinessCollateral
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        public ActionResult GetBusinessCollateralStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        public ActionResult GetBusinessCollateralTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetCreditAppRequestTableDropDown")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetCreditAppRequestTableDropDown")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetCreditAppRequestTableDropDown")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetCreditAppRequestTableDropDown")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetCreditAppRequestTableDropDown")]
        public ActionResult GetBusinessCollateralCreditAppRequestTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetCustomerTableByDropDown")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetCustomerTableByDropDown")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetCustomerTableByDropDown")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetCustomerTableByDropDown")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetCustomerTableByDropDown")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetCustomerTableByDropDown")]
        public ActionResult GetBusinessCollateralCustomerTableByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region function
        [HttpGet]
        [Route("Factoring/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("Bond/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("HirePurchase/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("LCDLC/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("Leasing/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        public ActionResult<bool> GetCancelCreditApplicationRequestByAppReqGUID(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetCancelCreditApplicationRequestByAppReqGUID(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Bond/Function/CancelCreditAppRequest")]
        [Route("HirePurchase/Function/CancelCreditAppRequest")]
        [Route("LCDLC/Function/CancelCreditAppRequest")]
        [Route("Leasing/Function/CancelCreditAppRequest")]
        [Route("ProjectFinance/Function/CancelCreditAppRequest")]
        [Route("Factoring/Function/CancelCreditAppRequest")]
        public ActionResult CancelCreditAppRequest([FromBody] CancelCreditAppRequestParamView param)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.CancelCreditApplicationRequest(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Bond/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("HirePurchase/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("LCDLC/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("Leasing/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("ProjectFinance/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("Factoring/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        public ActionResult GetCopyFinancialStatementTransByRefTypeValidationCART([FromBody] CopyFinancialStatementTransView param)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                return Ok(financialStatementTransService.GetCopyFinancialStatementTransByRefTypeValidationCART(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
        public ActionResult GetCopyFinancialStatementTransByRefTypeValidationCARL([FromBody] CopyFinancialStatementTransView param)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                return Ok(financialStatementTransService.GetCopyFinancialStatementTransByRefTypeValidationCARL(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        [HttpPost]
        [Route("Bond/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("HirePurchase/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("LCDLC/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("Leasing/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("ProjectFinance/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("Factoring/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        public ActionResult CopyFinancialStatementTrans([FromBody] CopyFinancialStatementTransView param)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.CopyFinancialStatementTransByCART(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        [HttpPost]
        [Route("Bond/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefType")]
        [Route("HirePurchase/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefType")]
        [Route("LCDLC/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefType")]
        [Route("Leasing/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefType")]
        [Route("ProjectFinance/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefType")]
        [Route("Factoring/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefType")]
        public ActionResult GetCopyTaxReportTransByRefType([FromBody] RefTypeModel param)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db);
                return Ok(taxReportTransService.GetCopyTaxReportTransByRefType(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        [HttpPost]
        [Route("Bond/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefTypeValidation")]
        [Route("HirePurchase/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefTypeValidation")]
        [Route("LCDLC/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefTypeValidation")]
        [Route("Leasing/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefTypeValidation")]
        [Route("ProjectFinance/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefTypeValidation")]
        [Route("Factoring/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans/GetCopyTaxReportTransByRefTypeValidation")]
        public ActionResult GetCopyTaxReportTransByRefTypeValidation([FromBody] CopyTaxReportTransView param)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db);
                return Ok(taxReportTransService.GetCopyTaxReportTransByRefTypeValidation(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        [HttpPost]
        [Route("Bond/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans")]
        [Route("HirePurchase/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans")]
        [Route("LCDLC/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans")]
        [Route("Leasing/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans")]
        [Route("ProjectFinance/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans")]
        [Route("Factoring/RelatedInfo/TaxReportTrans/Function/CopyTaxReportTrans")]
        public ActionResult CopyTaxReportTrans([FromBody] CopyTaxReportTransView param)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db);
                return Ok(taxReportTransService.CopyTaxReportTrans(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        [HttpPost]
        [Route("Bond/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting")]
        [Route("HirePurchase/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting")]
        [Route("LCDLC/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting")]
        [Route("Leasing/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting")]
        [Route("ProjectFinance/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting")]
        [Route("Factoring/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting")]
        public ActionResult CopyCustvisiting([FromBody] CopyCustVisitingView param)
        {
            try
            {
                ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db);
                return Ok(custVisitingTransService.CopyCustVisiting(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpGet]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransById/id={id}")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransById/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransById/id={id}")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransById/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransById/id={id}")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransById/id={id}")]
        public ActionResult<CopyBuyerAgreementTransView> GetCopyBuyerAgreementTransById(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                return Ok(buyerAgreementTransService.GetCopyBuyerAgreementTransById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        public ActionResult CopyFinancialStatementTransByCARL([FromBody] CopyFinancialStatementTransView param)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.CopyFinancialStatementTransByCARL(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        [HttpPost]
        [Route("Bond/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting/GetCustomerVisitingTransDropDown")]
        [Route("HirePurchase/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting/GetCustomerVisitingTransDropDown")]
        [Route("LCDLC/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting/GetCustomerVisitingTransDropDown")]
        [Route("Leasing/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting/GetCustomerVisitingTransDropDown")]
        [Route("ProjectFinance/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting/GetCustomerVisitingTransDropDown")]
        [Route("Factoring/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting/GetCustomerVisitingTransDropDown")]
        public ActionResult GetCustomerVisitingTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db);
                return Ok(custVisitingTransService.GetCustVisitingTransByCustomer(search));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableByBuyerDropdown([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                return Ok(buyerAgreementTransService.GetBuyerAgreementByBuyerDropdown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Bond/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting/GetCopyCustvisitingById/id={id}")]
        [Route("HirePurchase/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting/GetCopyCustvisitingById/id={id}")]
        [Route("LCDLC/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting/GetCopyCustvisitingById/id={id}")]
        [Route("Leasing/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting/GetCopyCustvisitingById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting/GetCopyCustvisitingById/id={id}")]
        [Route("Factoring/RelatedInfo/CustVisitingTrans/Function/CopyCustvisiting/GetCopyCustvisitingById/id={id}")]
        public ActionResult GetCopyCustvisitingById(string id)
        {
            try
            {
                ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db);
                return Ok(custVisitingTransService.GetCopyCustVisitingById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransByRefTypeValidation")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransByRefTypeValidation")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransByRefTypeValidation")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransByRefTypeValidation")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransByRefTypeValidation")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans/GetCopyBuyerAgreementTransByRefTypeValidation")]
        public ActionResult GetCopyBuyerAgreementTransByRefTypeValidation([FromBody] CopyBuyerAgreementTransView param)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                return Ok(buyerAgreementTransService.GetCopyBuyerAgreementTransByRefTypeValidation(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        [HttpPost]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans")]
        [Route("LCDLC/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans")]
        [Route("ProjectFinance/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/Function/CopyBuyerAgreementTrans")]
        public ActionResult CopyBuyerAgreementTrans([FromBody] CopyBuyerAgreementTransView param)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                return Ok(buyerAgreementTransService.CopyBuyerAgreementTrans(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Bond/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Factoring/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Lcdlc/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Projectfinance/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]

        public async Task<ActionResult> PrintBookmarkDocumentTransaction([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByRefGUID(printSetBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CreditAppReqBusinessCollateral
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        public ActionResult GetCreditAppReqBusinessCollateralListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppReqBusinessCollateralListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        public ActionResult GetCreditAppReqBusinessCollateralById(string id)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(CreditAppReqBusCollateralInfoService.GetCreditAppReqBusinessCollateralById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("Bond/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        public ActionResult CreateCreditAppReqBusinessCollateral([FromBody] CreditAppReqBusinessCollateralItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                    return Ok(CreditAppReqBusCollateralInfoService.CreateCreditAppReqBusinessCollateral(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("Bond/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]

        public ActionResult UpdateCreditAppReqBusinessCollateral([FromBody] CreditAppReqBusinessCollateralItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                    return Ok(CreditAppReqBusCollateralInfoService.UpdateCreditAppReqBusinessCollateral(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("Bond/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("LCDLCCreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        public ActionResult DeleteCreditAppReqBusinessCollateral([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(CreditAppReqBusCollateralInfoService.DeleteCreditAppReqBusinessCollateral(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]

        public ActionResult GetCreditAppReqBusColateralInitialData(string creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetCreditAppReqBusinessCollateralInitialData(creditAppRequestTableGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        public ActionResult GetCreditAppReqBusinessCollateralAccessModeByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdoen CreditAppReqBusColateral
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        public ActionResult GetBankTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        public ActionResult GetBusinessCollateralBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        public ActionResult GetNotCancelCustBusinessCollateralByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService creditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(creditAppReqBusCollateralInfoService.GetDropDownItemNotCancelCustBusinessCollateralByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        public ActionResult GetBusinessCollateralBankGroupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        public ActionResult GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db);
                return Ok(businessCollateralTypeService.GetDropDownItemBusinessCollateralSubTypeByBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralDropDown")]
        public ActionResult GetCustBusinessCollateralDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustBusinessCollateral(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("Bond/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("LCDLC/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("Leasing/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        public ActionResult GetBusinessCollateralSubTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralSubType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region BookmarkDocumentTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        public ActionResult GetBookmarkDocumentTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBookmarkDocumentTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        public ActionResult GetBookmarkDocumentTransById(string id)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(BookmarkDocumentTransService.GetBookmarkDocumentTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        public ActionResult CreateBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.CreateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        public ActionResult UpdateBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.UpdateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        public ActionResult DeleteBookmarkDocumentTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                return Ok(BookmarkDocumentTransService.DeleteBookmarkDocumentTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        public ActionResult GetBookmarkDocumentTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetBookmarkDocumentTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        public ActionResult GetBookMarkDocumentTransAccessModeByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetBookMarkDocumentTransAccessModeByCreditAppRequestTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        public ActionResult GetBookmarkDocumentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
                return Ok(bookmarkDocumentService.GetDropDownItemBookmarkDocumentByCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        public ActionResult GetDocumentTemplateTableDropDownByDocumentType([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
                return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        public ActionResult GetDocumentTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        public ActionResult GetBookmarkDocumentTransDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetDropDownItemBookMarkDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region PrintBookDocTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        public ActionResult GetPrintBookDocTransValidation([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintBookDocTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransaction([FromBody] PrintBookDocTransParm printBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion BookmarkDocumentTrans
        #region WorkFlow
        [HttpPost]
        [Route("Factoring/GetTaskBySerialNumber")]
        [Route("Bond/GetTaskBySerialNumber")]
        [Route("HirePurchase/GetTaskBySerialNumber")]
        [Route("LCDLC/GetTaskBySerialNumber")]
        [Route("Leasing/GetTaskBySerialNumber")]
        [Route("ProjectFinance/GetTaskBySerialNumber")]
        
        [Route("Factoring/Workflow/ActionWorkflow/GetTaskBySerialNumber")]        
        [Route("Bond/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("HirePurchase/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("LCDLC/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("Leasing/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("ProjectFinance/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        
        [Route("Factoring/Workflow/StartWorkflow/GetTaskBySerialNumber")]        
        [Route("Bond/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("HirePurchase/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("LCDLC/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("Leasing/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("ProjectFinance/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        public async Task<ActionResult> GetTaskBySerialNumber([FromBody] WorkflowInstance workflowInstance)
        {
            try
            {
                IK2Service k2Service = new K2Service(db);
                return Ok(await k2Service.GetTaskBySerialNumber(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/Workflow/StartWorkflow")]
        [Route("Bond/Workflow/StartWorkflow")]
        [Route("HirePurchase/Workflow/StartWorkflow")]
        [Route("LCDLC/Workflow/StartWorkflow")]
        [Route("Leasing/Workflow/StartWorkflow")]
        [Route("ProjectFinance/Workflow/StartWorkflow")]
        public async Task<ActionResult> StartWorkflow([FromBody] WorkFlowCreditAppRequestView workflowInstance)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(await creditAppRequestTableService.ValidateAndStartWorkflow(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/Workflow/ActionWorkflow")]
        [Route("Bond/Workflow/ActionWorkflow")]
        [Route("HirePurchase/Workflow/ActionWorkflow")]
        [Route("LCDLC/Workflow/ActionWorkflow")]
        [Route("Leasing/Workflow/ActionWorkflow")]
        [Route("ProjectFinance/Workflow/ActionWorkflow")]
        public async Task<ActionResult> ActionWorkflow([FromBody] WorkFlowCreditAppRequestView workflowInstance)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(await creditAppRequestTableService.ValidateAndActionWorkflow(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Bond/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("HirePurchase/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("LCDLC/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Leasing/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Factoring/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Bond/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("HirePurchase/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("LCDLC/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Leasing/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        public ActionResult GetWorkFlowCreditAppRequestById(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetWorkFlowCreditAppRequestById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("Bond/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("HirePurchase/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("LCDLC/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("Leasing/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("ProjectFinance/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        public ActionResult GetEmployeeTableWithUserDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetEmployeeTableWithUserDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region action history
        [HttpGet]
        [Route("Factoring/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("Bond/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("HirePurchase/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("LCDLC/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("Leasing/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("ProjectFinance/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        public ActionResult GetActionHistoryByRefGUID(string refGUID)
        {
            try
            {
                IActionHistoryService actionHistoryService = new ActionHistoryService(db);
                return Ok(actionHistoryService.GetActionHistoryByRefGUID(refGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region release task k2
        [HttpPost]
        [Route("Factoring/GetReleaseTask")]
        [Route("Bond/GetReleaseTask")]
        [Route("HirePurchase/GetReleaseTask")]
        [Route("LCDLC/GetReleaseTask")]
        [Route("Leasing/GetReleaseTask")]
        [Route("ProjectFinance/GetReleaseTask")]
        public async Task<ActionResult> GetReleaseTask([FromBody] WorkflowInstance workflowInstance)
        {
            try
            {
                IK2Service k2Service = new K2Service(db);
                return Ok(await k2Service.ReleaseTask(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region Attachment

        [HttpPost]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        
        [Route("Bond/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Factoring/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        public ActionResult GetAttachmentListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Projectfinance/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        
        [Route("Bond/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Factoring/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("HirePurchase/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Projectfinance/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentById(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/Attachment/CreateAttachment")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Lcdlc/CreditAppRequestLine-Child/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Projectfinance/CreditAppRequestLine-Child/RelatedInfo/Attachment/CreateAttachment")]
        
        [Route("Bond/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Factoring/RelatedInfo/Attachment/CreateAttachment")]
        [Route("HirePurchase/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Leasing/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Lcdlc/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Projectfinance/RelatedInfo/Attachment/CreateAttachment")]
        public ActionResult CreateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/CreditAppRequestLine-Child/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Projectfinance/CreditAppRequestLine-Child/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Bond/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Projectfinance/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/CreditAppRequestLine-Child/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Projectfinance/CreditAppRequestLine-Child/RelatedInfo/Attachment/DeleteAttachment")]
        
        [Route("Bond/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Projectfinance/RelatedInfo/Attachment/DeleteAttachment")]

        public ActionResult DeleteAttachment([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Projectfinance/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        
        [Route("Bond/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Projectfinance/RelatedInfo/Attachment/GetAttachmentRefId")]

        public ActionResult GetAttachmentRefId([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Retentions OutStanding
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionOutstanding/GetCAReqRetentionOutstandingList/ByCompany")]
        [Route("Bond/RelatedInfo/RetentionOutstanding/GetCAReqRetentionOutstandingList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/RetentionOutstanding/GetCAReqRetentionOutstandingList/ByCompany")]
        [Route("LCDLC/RelatedInfo/RetentionOutstanding/GetCAReqRetentionOutstandingList/ByCompany")]
        [Route("Leasing/RelatedInfo/RetentionOutstanding/GetCAReqRetentionOutstandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/RetentionOutstanding/GetCAReqRetentionOutstandingList/ByCompany")]
        public ActionResult GetRetentionOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetRetentionOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionOutstanding/GetCustomerTableDropDown")]
        [Route("Bond/RelatedInfo/RetentionOutstanding/GetCustomerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/RetentionOutstanding/GetCustomerTableDropDown")]
        [Route("LCDLC/RelatedInfo/RetentionOutstanding/GetCustomerTableDropDown")]
        [Route("Leasing/RelatedInfo/RetentionOutstanding/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/RetentionOutstanding/GetCustomerTableDropDown")]
        public ActionResult GetRetentionOutstandingCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/RetentionOutstanding/GetCreditAppTableDropDown")]
        [Route("Bond/RelatedInfo/RetentionOutstanding/GetCreditAppTableDropDown")]
        [Route("HirePurchase/RelatedInfo/RetentionOutstanding/GetCreditAppTableDropDown")]
        [Route("LCDLC/RelatedInfo/RetentionOutstanding/GetCreditAppTableDropDown")]
        [Route("Leasing/RelatedInfo/RetentionOutstanding/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/RetentionOutstanding/GetCreditAppTableDropDown")]
        public ActionResult GetRetentionOutstandingCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion  Retentions OutStanding
        #region AssignmentAgreementOutstanding
        [HttpPost]
        [Route("Factoring/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("Bond/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("Leasing/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        public ActionResult GetAssignmentOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAssignmentOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("Bond/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("LCDLC/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("Leasing/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        public ActionResult GetAssignmentOutstandingCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        public ActionResult GetAssignmentOutstandingBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("Bond/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("HirePurchase/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("LCDLC/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("Leasing/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        public ActionResult GetAssignmentOutstandingAssignmentAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AssignmentAgreementOutstanding        
        #region CreditOutStanding
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("Bond/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("LCDLC/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("Leasing/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        public ActionResult GetCreditOutStandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditOutStandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("Bond/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("LCDLC/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("Leasing/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        public ActionResult GetCreditOutStandingCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("Bond/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("HirePurchase/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("LCDLC/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("Leasing/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        public ActionResult GetCreditOutStandingCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("Bond/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("LCDLC/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("Leasing/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        public ActionResult GetCreditOutStandingCreditLimitTypeDropDownn([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditLimitType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion  CreditOutStanding
        #region Creditapplication

        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppTable/GetCreditAppTableList/ByCompany")]
        [Route("Bond/RelatedInfo/CreditAppTable/GetCreditAppTableList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/CreditAppTable/GetCreditAppTableList/ByCompany")]
        [Route("LCDLC/RelatedInfo/CreditAppTable/GetCreditAppTableList/ByCompany")]
        [Route("Leasing/RelatedInfo/CreditAppTable/GetCreditAppTableList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CreditAppTable/GetCreditAppTableList/ByCompany")]
        public ActionResult GetCreditAppTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppTable/GetCreditAppTableById/id={id}")]
        [Route("Bond/RelatedInfo/CreditAppTable/GetCreditAppTableById/id={id}")]
        [Route("HirePurchase/RelatedInfo/CreditAppTable/GetCreditAppTableById/id={id}")]
        [Route("LCDLC/RelatedInfo/CreditAppTable/GetCreditAppTableById/id={id}")]
        [Route("Leasing/RelatedInfo/CreditAppTable/GetCreditAppTableById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppTable/GetCreditAppTableById/id={id}")]
        public ActionResult GetCreditAppTableById(string id)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetCreditAppTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
       
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppTable/UpdateCreditAppTable")]
        [Route("Bond/RelatedInfo/CreditAppTable/UpdateCreditAppTable")]
        [Route("HirePurchase/RelatedInfo/CreditAppTable/UpdateCreditAppTable")]
        [Route("LCDLC/RelatedInfo/CreditAppTable/UpdateCreditAppTable")]
        [Route("Leasing/RelatedInfo/CreditAppTable/UpdateCreditAppTable")]
        [Route("ProjectFinance/RelatedInfo/CreditAppTable/UpdateCreditAppTable")]
        public ActionResult UpdateCreditAppTable([FromBody] CreditAppTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                    return Ok(creditAppTableService.UpdateCreditAppTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCreditAppLineList/ByCompany")]
        [Route("Bond/GetCreditAppLineList/ByCompany")]
        [Route("HirePurchase/GetCreditAppLineList/ByCompany")]
        [Route("LCDLC/GetCreditAppLineList/ByCompany")]
        [Route("Leasing/GetCreditAppLineList/ByCompany")]
        [Route("ProjectFinance/GetCreditAppLineList/ByCompany")]
        public ActionResult GetCreditAppLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppTable/GetCreditAppLineById/id={id}")]
        [Route("Bond/RelatedInfo/CreditAppTable/GetCreditAppLineById/id={id}")]
        [Route("HirePurchase/RelatedInfo/CreditAppTable/GetCreditAppLineById/id={id}")]
        [Route("LCDLC/RelatedInfo/CreditAppTable/GetCreditAppLineById/id={id}")]
        [Route("Leasing/RelatedInfo/CreditAppTable/GetCreditAppLineById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppTable/GetCreditAppLineById/id={id}")]
        public ActionResult GetCreditAppLineById(string id)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetCreditAppLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Credit application request assignment
        [HttpPost]
        [Route("Factoring/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("Bond/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("LCDLC/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("Leasing/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]

        public ActionResult GetCreditAppReqAssignmentListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppReqAssignmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("Bond/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("HirePurchase/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("LCDLC/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("Leasing/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]

        public ActionResult GetCreditAppReqAssignmentById(string id)
        {
            try
            {
                ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db);
                return Ok(creditAppReqAssignmentService.GetCreditAppReqAssignmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("Bond/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("HirePurchase/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("LCDLC/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("Leasing/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("ProjectFinance/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]

        public ActionResult CreateCreditAppReqAssignment([FromBody] CreditAppReqAssignmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db, SysTransactionLogService);
                    return Ok(creditAppReqAssignmentService.CreateCreditAppReqAssignment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("Bond/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("HirePurchase/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("LCDLC/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("Leasing/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("ProjectFinance/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]

        public ActionResult UpdateCreditAppReqAssignment([FromBody] CreditAppReqAssignmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db, SysTransactionLogService);
                    return Ok(creditAppReqAssignmentService.UpdateCreditAppReqAssignment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("Bond/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("HirePurchase/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("LCDLC/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("Leasing/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("ProjectFinance/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]

        public ActionResult DeleteCreditAppReqAssignment([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db, SysTransactionLogService);
                return Ok(creditAppReqAssignmentService.DeleteCreditAppReqAssignment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("Bond/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("HirePurchase/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("LCDLC/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("Leasing/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("ProjectFinance/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        public ActionResult GetAssignmentAgreementOutstanding(string refGUID, int reftype)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementOutstanding(new Guid(refGUID), reftype));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/AssignmentGetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        public ActionResult GetAssignmentAccessModeByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region ExistingAssignmentAgreement
        [HttpPost]
        [Route("Factoring/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("Bond/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("LCDLC/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("Leasing/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        public ActionResult GetExistingAssignmentAgreementListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementOutstanding(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ExistingAssignmentAgreement
        #region DropDown
        [HttpPost]
        [Route("Factoring/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("Bond/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("LCDLC/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("Leasing/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        public ActionResult GetAssignmentDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDropDownItemAssignmentAgreementStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Assignment/GetBuyerTableDropDown")]

        public ActionResult GetAssignmentBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("Bond/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("HirePurchase/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("LCDLC/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("Leasing/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("ProjectFinance/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]

        public ActionResult GetAssignmentAssignmentMethodDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentMethod(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("Bond/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementLineTableByBuyerAndCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("Bond/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("LCDLC/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("Leasing/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        public ActionResult GetAssignmentAgreementeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDropDownItemAssignmentAgreementByCustomerAndStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("Bond/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("HirePurchase/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("LCDLC/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("Leasing/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        public ActionResult GetAssignmentAssignmentAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #endregion Credit application request assignment
        #region CaBuyerCreditOutstanding
        [HttpPost]
        [Route("Factoring/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("Bond/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("LCDLC/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("Leasing/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        public ActionResult GetCAReqBuyerCreditOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCAReqBuyerCreditOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown 
        [HttpPost]
        [Route("Factoring/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("Bond/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("LCDLC/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("Leasing/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]

        public ActionResult CaBuyerCreditOutstandingGetBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("Bond/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("HirePurchase/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("LCDLC/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("Leasing/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("ProjectFinance/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        public ActionResult CaBuyerCreditOutstandingGetAssignmentAssignmentMethodDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentMethod(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("Bond/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("HirePurchase/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("LCDLC/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("Leasing/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("ProjectFinance/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        public ActionResult CaBuyerCreditOutstandingGetBillingResponsibleByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBillingResponsibleBy(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("Bond/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("Leasing/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        public ActionResult CaBuyerCreditOutstandingGetMethodOfPaymentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion CaBuyerCreditOutstanding
    }
}