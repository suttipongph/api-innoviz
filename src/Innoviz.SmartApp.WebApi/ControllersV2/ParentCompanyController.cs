using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class ParentCompanyController : BaseControllerV2
	{

		public ParentCompanyController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetParentCompanyList/ByCompany")]
		public ActionResult GetParentCompanyListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetParentCompanyListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetParentCompanyById/id={id}")]
		public ActionResult GetParentCompanyById(string id)
		{
			try
			{
				IParentCompanyService parentCompanyService = new ParentCompanyService(db);
				return Ok(parentCompanyService.GetParentCompanyById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateParentCompany")]
		public ActionResult CreateParentCompany([FromBody] ParentCompanyItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IParentCompanyService parentCompanyService = new ParentCompanyService(db, SysTransactionLogService);
					return Ok(parentCompanyService.CreateParentCompany(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateParentCompany")]
		public ActionResult UpdateParentCompany([FromBody] ParentCompanyItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IParentCompanyService parentCompanyService = new ParentCompanyService(db, SysTransactionLogService);
					return Ok(parentCompanyService.UpdateParentCompany(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteParentCompany")]
		public ActionResult DeleteParentCompany([FromBody] RowIdentity parm)
		{
			try
			{
				IParentCompanyService parentCompanyService = new ParentCompanyService(db, SysTransactionLogService);
				return Ok(parentCompanyService.DeleteParentCompany(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
