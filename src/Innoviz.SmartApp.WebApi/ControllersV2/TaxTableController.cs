﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class TaxTableController : BaseControllerV2
	{

		public TaxTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetTaxTableList/ByCompany")]
		public ActionResult GetTaxTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetTaxTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetTaxTableById/id={id}")]
		public ActionResult GetTaxTableById(string id)
		{
			try
			{
				ITaxTableService taxTableService = new TaxTableService(db);
				return Ok(taxTableService.GetTaxTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateTaxTable")]
		public ActionResult CreateTaxTable([FromBody] TaxTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ITaxTableService taxTableService = new TaxTableService(db, SysTransactionLogService);
					return Ok(taxTableService.CreateTaxTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateTaxTable")]
		public ActionResult UpdateTaxTable([FromBody] TaxTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ITaxTableService taxTableService = new TaxTableService(db, SysTransactionLogService);
					return Ok(taxTableService.UpdateTaxTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteTaxTable")]
		public ActionResult DeleteTaxTable([FromBody] RowIdentity parm)
		{
			try
			{
				ITaxTableService taxTableService = new TaxTableService(db, SysTransactionLogService);
				return Ok(taxTableService.DeleteTaxTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetTaxValueList/ByCompany")]
		public ActionResult GetTaxValueListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetTaxValueListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetTaxValueById/id={id}")]
		public ActionResult GetTaxValueById(string id)
		{
			try
			{
				ITaxTableService taxTableService = new TaxTableService(db);
				return Ok(taxTableService.GetTaxValueById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateTaxValue")]
		public ActionResult CreateTaxValue([FromBody] TaxValueItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ITaxTableService taxTableService = new TaxTableService(db, SysTransactionLogService);
					return Ok(taxTableService.CreateTaxValue(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateTaxValue")]
		public ActionResult UpdateTaxValue([FromBody] TaxValueItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ITaxTableService taxTableService = new TaxTableService(db, SysTransactionLogService);
					return Ok(taxTableService.UpdateTaxValue(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteTaxValue")]
		public ActionResult DeleteTaxValue([FromBody] RowIdentity parm)
		{
			try
			{
				ITaxTableService taxTableService = new TaxTableService(db, SysTransactionLogService);
				return Ok(taxTableService.DeleteTaxValue(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetTaxValueInitialData/id={id}")]
		public ActionResult GetTaxValueInitialData(string id)
		{
			try
			{
				ITaxTableService taxTableService = new TaxTableService(db, SysTransactionLogService);
				return Ok(taxTableService.GetTaxValueInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetTaxTableDropDown")]
		public ActionResult GetTaxTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
	}
}
