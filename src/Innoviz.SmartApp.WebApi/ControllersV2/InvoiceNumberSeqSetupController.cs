﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class InvoiceNumberSeqSetupController : BaseControllerV2
    {
		public InvoiceNumberSeqSetupController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetInvoiceNumberSeqSetupList/ByCompany")]
		public ActionResult GetInvoiceNumberSeqSetupListByCompany([FromBody] SearchParameter search)

		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceNumberSeqSetupListvw(search));
				//return null;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetInvoiceNumberSeqSetupById/id={id}")]
		public ActionResult GetInvoiceNumberSeqSetupById(string id)
		{
			try
			{
				IInvoiceNumberSeqSetupService invoiceNumberSeqSetupService = new InvoiceNumberSeqSetupService(db);
				return Ok(invoiceNumberSeqSetupService.GetInvoiceNumberSeqSetupById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateInvoiceNumberSeqSetup")]
		public ActionResult CreateInvoiceNumberSeqSetup([FromBody] InvoiceNumberSeqSetupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceNumberSeqSetupService invoiceNumberSeqSetupService = new InvoiceNumberSeqSetupService(db);
					return Ok(invoiceNumberSeqSetupService.CreateInvoiceNumberSeqSetup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("UpdateInvoiceNumberSeqSetup")]
		public ActionResult UpdateInvoiceNumberSeqSetup([FromBody] InvoiceNumberSeqSetupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceNumberSeqSetupService invoiceNumberSeqSetupService = new InvoiceNumberSeqSetupService(db, SysTransactionLogService);
					return Ok(invoiceNumberSeqSetupService.UpdateInvoiceNumberSeqSetup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteInvoiceNumberSeqSetup")]
		public ActionResult DeleteInvoiceNumberSeqSetup([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceNumberSeqSetupService invoiceNumberSeqSetupService = new InvoiceNumberSeqSetupService(db, SysTransactionLogService);
				return Ok(invoiceNumberSeqSetupService.DeleteInvoiceNumberSeqSetup(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropDown
		[HttpPost]
		[Route("GetBranchDropDown")]
		public ActionResult GetBranchDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBranch(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceTypeDropDown")]
		public ActionResult GetInvoiceTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetNumberSeqTableDropDown")]
		public ActionResult GetNumberSeqTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemNumberSeqTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
