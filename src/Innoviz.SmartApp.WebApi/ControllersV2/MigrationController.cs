﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class MigrationController : BaseControllerV2
    {

        public MigrationController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }

        #region TestPackage
        [HttpPost]
        [Route("ValidateTestPackage")]
        public ActionResult ValidateTestPackage()
        {
            try
            {
                return Ok("Test Package Success");
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region MainAgreementTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateMainAgreementTable")]
        public ActionResult CreateMainAgreementTableCollection([FromBody] IEnumerable<MainAgreementTableItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                    mainAgreementTableService.CreateMainAgreementTableCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CustomerTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateCustomerTableleCollection")]
        public ActionResult CreateCustomerTableleCollection([FromBody] List<CustomerTableItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustomerTableService customerTableService = new CustomerTableService(db, SysTransactionLogService);
                    customerTableService.CreateCustomerTableCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CustomerTable
        #region AgreementTableInfo
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateAgreementTableInfo")]
        public ActionResult CreateAgreementTableInfoCollection([FromBody] IEnumerable<AgreementTableInfoItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    agreementTableInfoService.CreateAgreementTableInfoCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AgreementTableInfo
        #region CreditAppRequestTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateCreditAppRequestTableCollection")]
        public ActionResult CreateCreditAppRequestTableCollection([FromBody] List<CreditAppRequestTableItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    creditAppRequestTableService.CreateCreditAppRequestTableCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditAppRequestTable
        #region CreditAppRequestLine
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateCreditAppRequestLineCollection")]
        public ActionResult CreateCreditAppRequestLineCollection([FromBody] List<CreditAppRequestLineItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    creditAppRequestTableService.CreateCreditAppRequestLineCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditAppRequestLine
        #region CreditAppReqBusinessCollateral
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateCreditAppReqBusinessCollateralCollection")]
        public ActionResult CreateCreditAppReqBusinessCollateralCollection([FromBody] List<CreditAppReqBusinessCollateralItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqBusCollateralInfoService creditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db, SysTransactionLogService);
                    creditAppReqBusCollateralInfoService.CreateCreditAppReqBusinessCollateralCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditAppReqBusinessCollateral
        #region ServiceFeeConditionTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateServiceFeeConditionTransCollection")]
        public ActionResult CreateServiceFeeConditionTransCollection([FromBody] List<ServiceFeeConditionTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db, SysTransactionLogService);
                    serviceFeeConditionTransService.CreateServiceFeeConditionTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ServiceFeeConditionTrans
        #region NCBTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateNCBTransCollection")]
        public ActionResult CreateNCBTransCollection([FromBody] List<NCBTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    INCBTransService ncbTransService = new NCBTransService(db, SysTransactionLogService);
                    ncbTransService.CreateNCBTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion NCBTrans
        #region CustBusinessCollateral
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateCustBusinessCollateralCollection")]
        public ActionResult CreateCustBusinessCollateralCollection([FromBody] List<CustBusinessCollateralItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqBusCollateralInfoService creditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db, SysTransactionLogService);
                    creditAppReqBusCollateralInfoService.CreateCustBusinessCollateralCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CustBusinessCollateral
        #region AssignmentAgreementTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateAssignmentAgreementTable")]
        public ActionResult CreateAssignmentAgreementTableCollection([FromBody] IEnumerable<AssignmentAgreementTableItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db, SysTransactionLogService);
                    assignmentAgreementTableService.CreateAssignmentAgreementTableCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region AssignmentAgreementLine
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateAssignmentAgreementLine")]
        public ActionResult CreateAssignmentAgreementLineCollection([FromBody] IEnumerable<AssignmentAgreementLineItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db, SysTransactionLogService);
                    assignmentAgreementTableService.CreateAssignmentAgreementLineCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CreditAppTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateCreditAppTableCollection")]
        public ActionResult CreateCreditAppTableCollection([FromBody] List<CreditAppTableItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                    creditAppTableService.CreateCreditAppTableCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region AssignmentAgreementSettle
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateAssignmentAgreementSettle")]
        public ActionResult CreateAssignmentAgreementSettleCollection([FromBody] IEnumerable<AssignmentAgreementSettleItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAssignmentAgreementSettleService assignmentAgreementSettleService = new AssignmentAgreementSettleService(db, SysTransactionLogService);
                    assignmentAgreementSettleService.CreateAssignmentAgreementSettleCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AssignmentAgreementSettle
        #region BusinessCollateralAgmTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateBusinessCollateralAgmTable")]
        public ActionResult CreateBusinessCollateralAgmTableCollection([FromBody] IEnumerable<BusinessCollateralAgmTableItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    businessCollateralAgmTableService.CreateBusinessCollateralAgmTableCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BusinessCollateralAgmTable
        #region RetentionConditionTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateRetentionConditionTransCollection")]
        public ActionResult CreateRetentionConditionTransCollection([FromBody] List<RetentionConditionTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db, SysTransactionLogService);
                    retentionConditionTransService.CreateRetentionConditionTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region FinancialCreditTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateFinancialCreditTransCollection")]
        public ActionResult CreateFinancialCreditTransCollection([FromBody] List<FinancialCreditTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialCreditTransService financialCreditTransService = new FinancialCreditTransService(db, SysTransactionLogService);
                    financialCreditTransService.CreateFinancialCreditTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion FinancialCreditTrans
        #region CustVisitingTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateCustVisitingTrans")]
        public ActionResult CreateCustVisitingTransCollection([FromBody] IEnumerable<CustVisitingTransItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db, SysTransactionLogService);
                    custVisitingTransService.CreateCustVisitingTransCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region BusinessCollateralAgmLine
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateBusinessCollateralAgmLine")]
        public ActionResult CreateBusinessCollateralAgmLineCollection([FromBody] IEnumerable<BusinessCollateralAgmLineItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    businessCollateralAgmTableService.CreateBusinessCollateralAgmLineCollection(vmModel, true);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BusinessCollateralAgmLine
        #region CreditAppLine
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateCreditAppLineCollection")]
        public ActionResult CreateCreditAppLineCollection([FromBody] List<CreditAppLineItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                    creditAppTableService.CreateCreditAppLineCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CreditAppTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateCreditAppTransCollection")]
        public ActionResult CreateCreditAppTransCollection([FromBody] List<CreditAppTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppTransService creditAppTransService = new CreditAppTransService(db, SysTransactionLogService);
                    creditAppTransService.CreateCreditAppTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditAppTrans
        #region VerificationTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateVerificationTableCollection")]
        public ActionResult CreateVerificationTableCollection([FromBody] List<VerificationTableItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVerificationTableService verificationTableService = new VerificationTableService(db, SysTransactionLogService);
                    verificationTableService.CreateVerificationTableCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion VerificationTable
        #region VerificationLine
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateVerificationLineCollection")]
        public ActionResult CreateVerificationLineCollection([FromBody] List<VerificationLineItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVerificationTableService verificationTableService = new VerificationTableService(db, SysTransactionLogService);
                    verificationTableService.CreateVerificationLineCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion VerificationLine
        #region GuarantorAgreementTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateGuarantorAgreementTable")]
        public ActionResult CreateGuarantorAgreementTableCollection([FromBody] IEnumerable<GuarantorAgreementTableItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorAgreementTableService guarantorAgreementTable = new GuarantorAgreementTableService(db, SysTransactionLogService);
                    guarantorAgreementTable.CreateGuarantorAgreementTableCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GuarantorAgreementTable
        #region BuyerInvoiceTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateBuyerInvoiceTableCollection")]
        public ActionResult CreateBuyerInvoiceTableCollection([FromBody] List<BuyerInvoiceTableItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerInvoiceTableService buyerInvoiceTableService = new BuyerInvoiceTableService(db, SysTransactionLogService);
                    buyerInvoiceTableService.CreateBuyerInvoiceTableCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerInvoiceTable
        #region GuarantorAgreementLine
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateGuarantorAgreementLine")]
        public ActionResult CreateGuarantorAgreementLineCollection([FromBody] IEnumerable<GuarantorAgreementLineItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorAgreementTableService guarantorAgreementTable = new GuarantorAgreementTableService(db, SysTransactionLogService);
                    guarantorAgreementTable.CreateGuarantorAgreementLineCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GuarantorAgreementLine
        #region GuarantorAgreementLineAffiliate
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateGuarantorAgreementLineAffiliate")]
        public ActionResult CreateGuarantorAgreementLineAffiliateCollection([FromBody] IEnumerable<GuarantorAgreementLineAffiliateItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorAgreementTableService guarantorAgreementTable = new GuarantorAgreementTableService(db, SysTransactionLogService);
                    guarantorAgreementTable.CreateGuarantorAgreementLineAffiliateCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GuarantorAgreementLineAffiliate
        #region PurchaseTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreatePurchaseTable")]
        public ActionResult CreatePurchaseTableCollection([FromBody] IEnumerable<PurchaseTableItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPurchaseTableService purchaseTable = new PurchaseTableService(db, SysTransactionLogService);
                    purchaseTable.CreatePurchaseTableCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion PurchaseTable
        #region InvoiceTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateInvoiceTableCollection")]
        public ActionResult CreateInvoiceTableCollection([FromBody] List<InvoiceTableItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    invoiceTableService.CreateInvoiceTableCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceTable
        #region PurchaseLine
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreatePurchaseLine")]
        public ActionResult CreatePurchaseLineCollection([FromBody] IEnumerable<PurchaseLineItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPurchaseTableService purchaseTable = new PurchaseTableService(db, SysTransactionLogService);
                    purchaseTable.CreatePurchaseLineCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion PurchaseLine
        #region InvoiceSettlementDetail
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateInvoiceSettlementDetail")]
        public ActionResult CreateInvoiceSettlementDetailCollection([FromBody] IEnumerable<InvoiceSettlementDetailItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceSettlementDetailService invoiceSettlementDetail = new InvoiceSettlementDetailService(db, SysTransactionLogService);
                    invoiceSettlementDetail.CreateInvoiceSettlementDetailCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceSettlementDetail
        #region PaymentDetail
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreatePaymentDetail")]
        public ActionResult CreatePaymentDetailCollection([FromBody] IEnumerable<PaymentDetailItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPaymentDetailService paymentDetail = new PaymentDetailService(db, SysTransactionLogService);
                    paymentDetail.CreatePaymentDetailCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion PaymentDetail
        #region VerificationTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateVerificationTrans")]
        public ActionResult CreateVerificationTransCollection([FromBody] IEnumerable<VerificationTransItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVerificationTransService verificationTrans = new VerificationTransService(db, SysTransactionLogService);
                    verificationTrans.CreateVerificationTransCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion VerificationTrans
        #region InvoiceLine
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateInvoiceLineCollection")]
        public ActionResult CreateInvoiceLineCollection([FromBody] List<InvoiceLineItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    invoiceTableService.CreateInvoiceLineCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceLine
        #region ProductSettledTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateProductSettledTrans")]
        public ActionResult CreateProductSettledTransCollection([FromBody] IEnumerable<ProductSettledTransItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IProductSettledTransService productSettledTrans = new ProductSettledTransService(db, SysTransactionLogService);
                    productSettledTrans.CreateProductSettledTransCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ProductSettledTrans
        #region ServiceFeeTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateServiceFeeTransCollection")]
        public ActionResult CreateServiceFeeTransCollection([FromBody] List<ServiceFeeTransItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    serviceFeeTransService.CreateServiceFeeTransCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ServiceFeeTrans
        #region WithdrawalTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateWithdrawalTable")]
        public ActionResult CreateWithdrawalTableCollection([FromBody] IEnumerable<WithdrawalTableItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IWithdrawalTableService withdrawalTable = new WithdrawalTableService(db, SysTransactionLogService);
                    withdrawalTable.CreateWithdrawalTableCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion WithdrawalTable
        #region BuyerTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateBuyerTableCollection")]
        public ActionResult CreateBuyerTableCollection([FromBody] List<BuyerTableItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerTableService buyerTableService = new BuyerTableService(db, SysTransactionLogService);
                    buyerTableService.CreateBuyerTableCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerTable
        #region CustomerCreditLimitByProduct
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateCustomerCreditLimitByProductCollection")]
        public ActionResult CreateCustomerCreditLimitByProductCollection([FromBody] List<CustomerCreditLimitByProductItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustomerCreditLimitByProductService customerCreditLimitByProductService = new CustomerCreditLimitByProductService(db, SysTransactionLogService);
                    customerCreditLimitByProductService.CreateCustomerCreditLimitByProductCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CustomerCreditLimitByProduct
        #region FinancialStatementTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateFinancialStatementTransCollection")]
        public ActionResult CreateFinancialStatementTransCollection([FromBody] List<FinancialStatementTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                    financialStatementTransService.CreateFinancialStatementTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion FinancialStatementTrans
        #region JointVentureTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateJointVentureTransCollection")]
        public ActionResult CreateJointVentureTransCollection([FromBody] List<JointVentureTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService jointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    jointVentureTransService.CreateJointVentureTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion JointVentureTrans
        #region ConsortiumTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateConsortiumTableCollection")]
        public ActionResult CreateConsortiumTableCollection([FromBody] List<ConsortiumTableItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTableService consortiumTableService = new ConsortiumTableService(db, SysTransactionLogService);
                    consortiumTableService.CreateConsortiumTableCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ConsortiumTable
        #region OwnerTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateOwnerTransCollection")]
        public ActionResult CreateOwnerTransCollection([FromBody] List<OwnerTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IOwnerTransService ownerTransService = new OwnerTransService(db, SysTransactionLogService);
                    ownerTransService.CreateOwnerTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion OwnerTrans 
        #region TaxReportTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateTaxReportTransCollection")]
        public ActionResult CreateTaxReportTransCollection([FromBody] List<TaxReportTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ITaxReportTransService taxReportTransService = new TaxReportTransService(db, SysTransactionLogService);
                    taxReportTransService.CreateTaxReportTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion TaxReportTrans
        #region BuyerCreditLimitByProduct
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateBuyerCreditLimitByProductCollection")]
        public ActionResult CreateBuyerCreditLimitByProductCollection([FromBody] List<BuyerCreditLimitByProductItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerCreditLimitByProductService buyerCreditLimitByProductService = new BuyerCreditLimitByProductService(db, SysTransactionLogService);
                    buyerCreditLimitByProductService.CreateBuyerCreditLimitByProductCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerCreditLimitByProduct
        #region BuyerAgreementLine
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateBuyerAgreementLineCollection")]
        public ActionResult CreateBuyerAgreementLineCollection([FromBody] List<BuyerAgreementLineItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db, SysTransactionLogService);
                    buyerAgreementTableService.CreateBuyerAgreementLineCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerAgreementLine
        #region BuyerAgreementTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateBuyerAgreementTableCollection")]
        public ActionResult CreateBuyerAgreementTableCollection([FromBody] List<BuyerAgreementTableItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db, SysTransactionLogService);
                    buyerAgreementTableService.CreateBuyerAgreementTableCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerAgreementTable
        #region ConsortiumLine
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateConsortiumLineCollection")]
        public ActionResult CreateConsortiumLineCollection([FromBody] List<ConsortiumLineItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTableService consortiumTableService = new ConsortiumTableService(db, SysTransactionLogService);
                    consortiumTableService.CreateConsortiumLineCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ConsortiumLine
        #region EmployeeTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateEmployeeTableCollection")]
        public ActionResult CreateEmployeeTableCollection([FromBody] List<EmployeeTableItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IEmployeeTableService employeeTableService = new EmployeeTableService(db, SysTransactionLogService);
                    employeeTableService.CreateEmployeeTableCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion EmployeeTable
        #region AddressTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateAddressTransCollection")]
        public ActionResult CreateAddressTransCollection([FromBody] List<AddressTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAddressTransService addressTransService = new AddressTransService(db, SysTransactionLogService);
                    addressTransService.CreateAddressTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AddressTrans
        #region RelatedPersonTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateRelatedPersonTableCollection")]
        public ActionResult CreateRelatedPersonTableCollection([FromBody] List<RelatedPersonTableItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db, SysTransactionLogService);
                    relatedPersonTableService.CreateRelatedPersonTableCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion RelatedPersonTable
        #region VendorTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateVendorTableCollection")]
        public ActionResult CreateVendorTableCollection([FromBody] List<VendorTableItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVendorTableService vendorTableService = new VendorTableService(db, SysTransactionLogService);
                    vendorTableService.CreateVendorTableCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion VendorTable
        #region ContactPersonTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateContactPersonTransCollection")]
        public ActionResult CreateContactPersonTransCollection([FromBody] List<ContactPersonTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db, SysTransactionLogService);
                    contactPersonTransService.CreateContactPersonTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ContactPersonTrans
        #region ContactTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateContactTransCollection")]
        public ActionResult CreateContactTransCollection([FromBody] List<ContactTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IContactTransService contactTransService = new ContactTransService(db, SysTransactionLogService);
                    contactTransService.CreateContactTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ContactTrans
        #region AuthorizedPersonTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateAuthorizedPersonTransCollection")]
        public ActionResult CreateAuthorizedPersonTransCollection([FromBody] List<AuthorizedPersonTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db, SysTransactionLogService);
                    authorizedPersonTransService.CreateAuthorizedPersonTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AuthorizedPersonTrans
        #region WithdrawalLine
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateWithdrawalLineCollection")]
        public ActionResult CreateWithdrawalLineCollection([FromBody] List<WithdrawalLineItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db, SysTransactionLogService);
                    withdrawalTableService.CreateWithdrawalLineCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion WithdrawalLine
        #region ProjectReferenceTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateProjectReferenceTransCollection")]
        public ActionResult CreateProjectReferenceTransCollection([FromBody] List<ProjectReferenceTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IProjectReferenceTransService projectReferenceTransService = new ProjectReferenceTransService(db, SysTransactionLogService);
                    projectReferenceTransService.CreateProjectReferenceTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ProjectReferenceTrans
        #region CustBank
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateCustBankCollection")]
        public ActionResult CreateCustBankCollection([FromBody] List<CustBankItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustBankService custBankService = new CustBankService(db, SysTransactionLogService);
                    custBankService.CreateCustBankCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CustBank
        #region VendBank
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateVendBankCollection")]
        public ActionResult CreateVendBankCollection([FromBody] List<VendBankItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IVendBankService vendBankService = new VendBankService(db, SysTransactionLogService);
                    vendBankService.CreateVendBankCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion VendBank
        #region ChequeTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateChequeTableCollection")]
        public ActionResult CreateChequeTableCollection([FromBody] List<ChequeTableItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IChequeTableService chequeTableService = new ChequeTableService(db, SysTransactionLogService);
                    chequeTableService.CreateChequeTableCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ChequeTable
        #region ProjectProgressTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateProjectProgressTableCollection")]
        public ActionResult CreateProjectProgressTableCollection([FromBody] List<ProjectProgressTableItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IProjectProgressTableService projectProgressTableService = new ProjectProgressTableService(db, SysTransactionLogService);
                    projectProgressTableService.CreateProjectProgressTableCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ProjectProgressTable
        #region BuyerAgreementTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateBuyerAgreementTransCollection")]
        public ActionResult CreateBuyerAgreementTransCollection([FromBody] List<BuyerAgreementTransItemView> vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db, SysTransactionLogService);
                    buyerAgreementTransService.CreateBuyerAgreementTransCollection(vwModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerAgreementTrans
        #region IntercompanyInvoiceTable
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateIntercompanyInvoiceTable")]
        public ActionResult CreateIntercompanyInvoiceTableCollection([FromBody] IEnumerable<IntercompanyInvoiceTableItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IIntercompanyInvoiceTableService intercompanyInvoiceTableService = new IntercompanyInvoiceTableService(db, SysTransactionLogService);
                    intercompanyInvoiceTableService.CreateIntercompanyInvoiceTableCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AgreementTableInfo
        #region InterestRealizedTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateInterestRealizedTrans")]
        public ActionResult CreateInterestRealizedTransCollection([FromBody] IEnumerable<InterestRealizedTransItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInterestRealizedTransService interestRealizedTransService = new InterestRealizedTransService(db, SysTransactionLogService);
                    interestRealizedTransService.CreateInterestRealizedTransCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AgreementTableInfo
        #region GuarantorTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateGuarantorTrans")]
        public ActionResult CreateGuarantorTransCollection([FromBody] IEnumerable<GuarantorTransItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorTransService guarantorTransService = new GuarantorTransService(db, SysTransactionLogService);
                    guarantorTransService.CreateGuarantorTransCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AgreementTableInfo
        #region DocumentConditionTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateDocumentConditionTrans")]
        public ActionResult CreateDocumentConditionTransCollection([FromBody] IEnumerable<DocumentConditionTransItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IDocumentConditionTransService documentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
                    documentConditionTransService.CreateDocumentConditionTransCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AgreementTableInfo

        #region ConsortiumTrans
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("CreateConsortiumTrans")]
        public ActionResult CreateConsortiumTransCollection([FromBody] IEnumerable<ConsortiumTransItemView> vmModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService consortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    consortiumTransService.CreateConsortiumTransCollection(vmModel);
                    return Ok();
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AgreementTableInfo
    }
}
