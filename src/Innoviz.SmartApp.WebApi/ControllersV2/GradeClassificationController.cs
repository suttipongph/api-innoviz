using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class GradeClassificationController : BaseControllerV2
	{

		public GradeClassificationController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetGradeClassificationList/ByCompany")]
		public ActionResult GetGradeClassificationListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetGradeClassificationListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetGradeClassificationById/id={id}")]
		public ActionResult GetGradeClassificationById(string id)
		{
			try
			{
				IGradeClassificationService gradeClassificationService = new GradeClassificationService(db);
				return Ok(gradeClassificationService.GetGradeClassificationById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateGradeClassification")]
		public ActionResult CreateGradeClassification([FromBody] GradeClassificationItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IGradeClassificationService gradeClassificationService = new GradeClassificationService(db, SysTransactionLogService);
					return Ok(gradeClassificationService.CreateGradeClassification(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateGradeClassification")]
		public ActionResult UpdateGradeClassification([FromBody] GradeClassificationItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IGradeClassificationService gradeClassificationService = new GradeClassificationService(db, SysTransactionLogService);
					return Ok(gradeClassificationService.UpdateGradeClassification(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteGradeClassification")]
		public ActionResult DeleteGradeClassification([FromBody] RowIdentity parm)
		{
			try
			{
				IGradeClassificationService gradeClassificationService = new GradeClassificationService(db, SysTransactionLogService);
				return Ok(gradeClassificationService.DeleteGradeClassification(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
