using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class NumberSeqSetupByProductTypeController : BaseControllerV2
	{

		public NumberSeqSetupByProductTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetNumberSeqSetupByProductTypeList/ByCompany")]
		public ActionResult GetNumberSeqSetupByProductTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetNumberSeqSetupByProductTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetNumberSeqSetupByProductTypeById/id={id}")]
		public ActionResult GetNumberSeqSetupByProductTypeById(string id)
		{
			try
			{
				INumberSeqSetupByProductTypeService numberSeqSetupByProductTypeService = new NumberSeqSetupByProductTypeService(db);
				return Ok(numberSeqSetupByProductTypeService.GetNumberSeqSetupByProductTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateNumberSeqSetupByProductType")]
		public ActionResult CreateNumberSeqSetupByProductType([FromBody] NumberSeqSetupByProductTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					INumberSeqSetupByProductTypeService numberSeqSetupByProductTypeService = new NumberSeqSetupByProductTypeService(db, SysTransactionLogService);
					return Ok(numberSeqSetupByProductTypeService.CreateNumberSeqSetupByProductType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateNumberSeqSetupByProductType")]
		public ActionResult UpdateNumberSeqSetupByProductType([FromBody] NumberSeqSetupByProductTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					INumberSeqSetupByProductTypeService numberSeqSetupByProductTypeService = new NumberSeqSetupByProductTypeService(db, SysTransactionLogService);
					return Ok(numberSeqSetupByProductTypeService.UpdateNumberSeqSetupByProductType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteNumberSeqSetupByProductType")]
		public ActionResult DeleteNumberSeqSetupByProductType([FromBody] RowIdentity parm)
		{
			try
			{
				INumberSeqSetupByProductTypeService numberSeqSetupByProductTypeService = new NumberSeqSetupByProductTypeService(db, SysTransactionLogService);
				return Ok(numberSeqSetupByProductTypeService.DeleteNumberSeqSetupByProductType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetNumberSeqTableDropDown")]
		public ActionResult GetNumberSeqTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemNumberSeqTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
	}
}
