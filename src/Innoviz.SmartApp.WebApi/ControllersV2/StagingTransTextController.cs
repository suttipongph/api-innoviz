using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class StagingTransTextController : BaseControllerV2
	{

		public StagingTransTextController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetStagingTransTextList/ByCompany")]
		public ActionResult GetStagingTransTextListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetStagingTransTextListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetStagingTransTextById/id={id}")]
		public ActionResult GetStagingTransTextById(string id)
		{
			try
			{
				IStagingTransTextService stagingTransTextService = new StagingTransTextService(db);
				return Ok(stagingTransTextService.GetStagingTransTextById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateStagingTransText")]
		public ActionResult CreateStagingTransText([FromBody] StagingTransTextItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IStagingTransTextService stagingTransTextService = new StagingTransTextService(db, SysTransactionLogService);
					return Ok(stagingTransTextService.CreateStagingTransText(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateStagingTransText")]
		public ActionResult UpdateStagingTransText([FromBody] StagingTransTextItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IStagingTransTextService stagingTransTextService = new StagingTransTextService(db, SysTransactionLogService);
					return Ok(stagingTransTextService.UpdateStagingTransText(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteStagingTransText")]
		public ActionResult DeleteStagingTransText([FromBody] RowIdentity parm)
		{
			try
			{
				IStagingTransTextService stagingTransTextService = new StagingTransTextService(db, SysTransactionLogService);
				return Ok(stagingTransTextService.DeleteStagingTransText(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
