using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class LineOfBusinessController : BaseControllerV2
	{

		public LineOfBusinessController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetLineOfBusinessList/ByCompany")]
		public ActionResult GetLineOfBusinessListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetLineOfBusinessListvw(search));
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetLineOfBusinessById/id={id}")]
		public ActionResult GetLineOfBusinessById(string id)
		{
			try
			{
				ILineOfBusinessService lineOfBusinessService = new LineOfBusinessService(db);
				return Ok(lineOfBusinessService.GetLineOfBusinessById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateLineOfBusiness")]
		public ActionResult CreateLineOfBusiness([FromBody] LineOfBusinessItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ILineOfBusinessService lineOfBusinessService = new LineOfBusinessService(db, SysTransactionLogService);
					return Ok(lineOfBusinessService.CreateLineOfBusiness(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateLineOfBusiness")]
		public ActionResult UpdateLineOfBusiness([FromBody] LineOfBusinessItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ILineOfBusinessService lineOfBusinessService = new LineOfBusinessService(db, SysTransactionLogService);
					return Ok(lineOfBusinessService.UpdateLineOfBusiness(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteLineOfBusiness")]
		public ActionResult DeleteLineOfBusiness([FromBody] RowIdentity parm)
		{
			try
			{
				ILineOfBusinessService lineOfBusinessService = new LineOfBusinessService(db, SysTransactionLogService);
				return Ok(lineOfBusinessService.DeleteLineOfBusiness(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		
		

      

	}
}
