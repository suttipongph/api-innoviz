using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class FreeTextInvoiceTableController : BaseControllerV2
	{

		public FreeTextInvoiceTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetFreeTextInvoiceTableList/ByCompany")]
		public ActionResult GetFreeTextInvoiceTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetFreeTextInvoiceTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetFreeTextInvoiceNumberSequence/companyId={companyId}")]
		public ActionResult GetFreeTextInvoiceNumberSequence(string companyId)
		{
			try
			{
				IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db);
				return Ok(freeTextInvoiceTableService.GetFreeTextInvoiceNumberSequence(companyId));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetInitialCreateData")]
		public ActionResult GetInitialCreateData()
		{
			try
			{
				IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db);
				return Ok(freeTextInvoiceTableService.GetInitialCreateData());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetFreeTextInvoiceTableById/id={id}")]
		public ActionResult GetFreeTextInvoiceTableById(string id)
		{
			try
			{
				IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db);
				return Ok(freeTextInvoiceTableService.GetFreeTextInvoiceTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateFreeTextInvoiceTable")]
		public ActionResult CreateFreeTextInvoiceTable([FromBody] FreeTextInvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db, SysTransactionLogService);
					return Ok(freeTextInvoiceTableService.CreateFreeTextInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateFreeTextInvoiceTable")]
		public ActionResult UpdateFreeTextInvoiceTable([FromBody] FreeTextInvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db, SysTransactionLogService);
					return Ok(freeTextInvoiceTableService.UpdateFreeTextInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteFreeTextInvoiceTable")]
		public ActionResult DeleteFreeTextInvoiceTable([FromBody] RowIdentity parm)
		{
			try
			{
				IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db, SysTransactionLogService);
				return Ok(freeTextInvoiceTableService.DeleteFreeTextInvoiceTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region attachment
		[HttpPost]
		[Route("RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
		public ActionResult GetAttachmentListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAttachmentListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/Attachment/GetAttachmentById/id={id}")]
		public async Task<ActionResult> GetAttachmentById(string id)
		{
			try
			{
				IAttachmentService attachmentService = new AttachmentService(db);
				return Ok(await attachmentService.GetAttachmentById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/CreateAttachment")]
		public ActionResult CreateAttachment([FromBody] AttachmentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
					return Ok(attachmentService.CreateAttachment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/UpdateAttachment")]
		public ActionResult UpdateAttachment([FromBody] AttachmentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
					return Ok(attachmentService.UpdateAttachment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/DeleteAttachment")]
		public ActionResult DeleteAttachment([FromBody] RowIdentity parm)
		{
			try
			{
				IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
				return Ok(attachmentService.DeleteAttachment(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/GetAttachmentRefId")]
		public ActionResult GetAttachmentRefId([FromBody] RefIdParm parm)
		{
			try
			{
				IAttachmentService attachmentService = new AttachmentService(db);
				return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion attachment

		#region TaxInvoice
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
		public ActionResult GetTaxInvoiceListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetTaxInvoiceTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
		public ActionResult GetTaxInvoiceTableById(string id)
		{
			try
			{
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetTaxInvoiceTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
		public ActionResult GetTaxInvoiceLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetTaxInvoiceLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
		public ActionResult GetTaxInvoiceLineById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetTaxInvoiceLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/TaxInvoice/GetTaxInvoiceTableByByFreeTextInvoice/id={id}")]
		public ActionResult GetTaxInvoiceTableByByFreeTextInvoice(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db);
				return Ok(freeTextInvoiceTableService.GetFreeTextInvoiceTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
		public ActionResult GetTaxTableDropDownTaxInvoice([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
		[Route("RelatedInfo/invoicetable/GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDownTaxInvoiceLine([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Report
		[HttpGet]
		[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
		public ActionResult GetPrintTaxInvoiceCopyById(string id)
		{
			try
			{
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetPrintTaxInvoiceCopyById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
		public ActionResult PrintTaxInvoiceCopy([FromBody] PrintTaxInvoiceReportView model)
		{
			try
			{
				IReportViewerService reportViewerService = new ReportViewerService();
				return Ok(reportViewerService.RenderReport(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
		public ActionResult GetPrintTaxInvoiceOriginalById(string id)
		{
			try
			{
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetPrintTaxInvoiceOriginalById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
		public ActionResult PrintTaxInvoiceOriginal([FromBody] PrintTaxInvoiceReportView model)
		{
			try
			{
				IReportViewerService reportViewerService = new ReportViewerService();
				return Ok(reportViewerService.RenderReport(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Report
		#endregion TaxInvoice
		#region InvoiceTable
		[HttpPost]
		[Route("RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		public ActionResult GetInvoiceLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		public ActionResult GetInvoiceLineById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetInvoiceLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		public ActionResult CreateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.CreateInvoiceLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		public ActionResult UpdateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.UpdateInvoiceLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		public ActionResult DeleteInvoiceLine([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
				return Ok(invoiceTableService.DeleteInvoiceLine(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		public ActionResult UpdateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.UpdateInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		public ActionResult DeleteInvoiceTable([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
				return Ok(invoiceTableService.DeleteInvoiceTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		public ActionResult GetInvoiceTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		public ActionResult GetInvoiceTableById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetInvoiceTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		public ActionResult CreateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.CreateInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion InvoiceTable

		#region Dropdown

		[HttpPost]
		[Route("GetAddressTransByDropDown")]
		public ActionResult GetAddressTransDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db);
				return Ok(freeTextInvoiceTableService.GetDropDownItemAddressTrans(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetCreditAppTableDropDown")]
		public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db);
				return Ok(freeTextInvoiceTableService.GetDropDownItemCreditAppTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetCurrencyDropDown")]
		public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemPlusExchangeRate(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetCustomerTableDropDown")]
		public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetDocumentReasonDropDown")]
		public ActionResult GetDocumentReasonDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db);
				return Ok(freeTextInvoiceTableService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetInvoiceTableDropDown")]
		public ActionResult GetInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db);

				return Ok(freeTextInvoiceTableService.GetDropDownItemInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/invoicetable/GetInvoiceTableDropDown")]
		public ActionResult GetInvoiceTableLinItemDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceTypeDropDown")]
		public ActionResult GetInvoiceTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceTypeByProductTypeDropDown")]
		public ActionResult GetInvoiceTypeByProductTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db);
				return Ok(freeTextInvoiceTableService.GetDropDownItemInvoiceTypeTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetLedgerDimensionDropDown")]
		public ActionResult GetLedgerDimensionDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetTaxTableDropDown")]
		public ActionResult GetTaxTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetWithholdingTaxTableDropDown")]
		public ActionResult GetWithholdingTaxTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
		#region InvoiceTableDropdown
		
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		public ActionResult GetBuyerInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	
		
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		[Route("RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		public ActionResult GetProdUnitTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		public ActionResult GetTaxInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		public ActionResult GetRefInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetRefInvoiceTableDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		public ActionResult GetDocumentStatusByInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		public ActionResult GetCustomerTableServiceFeeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
		public ActionResult GetBuyerTableServiceFeeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}


		[HttpPost]
		[Route("RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
		public ActionResult GetLedgerDimensionServiceFeeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/invoicetable/GetMethodOfPaymentDropDown")]
		public ActionResult GetMethodOfPaymentDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}


		[HttpPost]
		[Route("RelatedInfo/invoicetable/GetBuyerAgreementTableDropDown")]
		public ActionResult GetBuyerAgreementTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("RelatedInfo/invoicetable/GetDocumentReasonDropDown")]
		public ActionResult GetDocumentReasonDropdown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/invoicetable/GetTaxTableDropDown")]
		public ActionResult GetTaxTableLinInvoiceDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/invoicetable/GetWithholdingTaxTableDropDown")]
		public ActionResult GetWithholdingTaxTableLineInvoiceDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/invoicetable/GetReceiptTempTableDropDown")]
		public ActionResult GetReceiptTempTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemReceiptTempTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/invoicetable/GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion InvoiceTableDropDown
		[HttpPost]
		[Route("GetAmount")]
		public ActionResult GetAmount([FromBody] FreeTextInvoiceTableItemView vwModel)
		{
			try
			{
				IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db);
				return Ok(freeTextInvoiceTableService.GetAmount(vwModel));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Function
		#region Post free text invoice
		[HttpGet]
		[Route("Function/PostFreeTextInvoice/GetPostFreeTextInvoiceById/id={id}")]
		public ActionResult GetPostFreeTextInvoiceById(string id)
		{
			try
			{
				IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db);
				return Ok(freeTextInvoiceTableService.GetPostFreeTextInvoiceById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Function/PostFreeTextInvoice/PostFreeTextInvoice")]
		public ActionResult PostFreeTextInvoice([FromBody] PostFreeTextInvoiceView view)
		{
			try
			{
				IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db);
				return Ok(freeTextInvoiceTableService.PostFreeTextInvoice(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Post free text invoice
		#endregion Function

	}
}
