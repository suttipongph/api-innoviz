﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class AddressCountryController : BaseControllerV2
    {
		public AddressCountryController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetAddressCountryList/ByCompany")]
		public ActionResult GetAddressCountryListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAddressCountryListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetAddressCountryById/id={id}")]
		public ActionResult GetAddressCountryById(string id)
		{
			try
			{
				IAddressService addressService = new AddressService(db);
				return Ok(addressService.GetAddressCountryById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateAddressCountry")]
		public ActionResult CreateAddressCountry([FromBody] AddressCountryItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressService addressService = new AddressService(db, SysTransactionLogService);
					return Ok(addressService.CreateAddressCountry(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateAddressCountry")]
		public ActionResult UpdateAddressCountry([FromBody] AddressCountryItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressService addressService = new AddressService(db, SysTransactionLogService);
					return Ok(addressService.UpdateAddressCountry(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteAddressCountry")]
		public ActionResult DeleteAddressCountry([FromBody] RowIdentity parm)
		{
			try
			{
				IAddressService addressService = new AddressService(db, SysTransactionLogService);
				return Ok(addressService.DeleteAddressCountry(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
