using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class StagingTableIntercoInvSettleController : BaseControllerV2
	{

		public StagingTableIntercoInvSettleController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetStagingTableIntercoInvSettleList/ByCompany")]
		public ActionResult GetStagingTableIntercoInvSettleListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetStagingTableIntercoInvSettleListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetStagingTableIntercoInvSettleById/id={id}")]
		public ActionResult GetStagingTableIntercoInvSettleById(string id)
		{
			try
			{
				IStagingTableIntercoInvSettleService stagingTableIntercoInvSettleService = new StagingTableIntercoInvSettleService(db);
				return Ok(stagingTableIntercoInvSettleService.GetStagingTableIntercoInvSettleById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateStagingTableIntercoInvSettle")]
		public ActionResult CreateStagingTableIntercoInvSettle([FromBody] StagingTableIntercoInvSettleItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IStagingTableIntercoInvSettleService stagingTableIntercoInvSettleService = new StagingTableIntercoInvSettleService(db, SysTransactionLogService);
					return Ok(stagingTableIntercoInvSettleService.CreateStagingTableIntercoInvSettle(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateStagingTableIntercoInvSettle")]
		public ActionResult UpdateStagingTableIntercoInvSettle([FromBody] StagingTableIntercoInvSettleItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IStagingTableIntercoInvSettleService stagingTableIntercoInvSettleService = new StagingTableIntercoInvSettleService(db, SysTransactionLogService);
					return Ok(stagingTableIntercoInvSettleService.UpdateStagingTableIntercoInvSettle(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteStagingTableIntercoInvSettle")]
		public ActionResult DeleteStagingTableIntercoInvSettle([FromBody] RowIdentity parm)
		{
			try
			{
				IStagingTableIntercoInvSettleService stagingTableIntercoInvSettleService = new StagingTableIntercoInvSettleService(db, SysTransactionLogService);
				return Ok(stagingTableIntercoInvSettleService.DeleteStagingTableIntercoInvSettle(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
