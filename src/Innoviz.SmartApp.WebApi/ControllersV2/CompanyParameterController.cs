﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class CompanyParameterController : BaseControllerV2
	{

		public CompanyParameterController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetCompanyParameterList/ByCompany")]
		public ActionResult GetCompanyParameterListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetCompanyParameterListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetCompanyParameterById/id={id}")]
		public ActionResult GetCompanyParameterById(string id)
		{
			try
			{
                ICompanyParameterService companyParameterService = new CompanyParameterService(db);
				return Ok(companyParameterService.GetCompanyParameterById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetCompanyParameterByCompanyId/id={id}")]
		public ActionResult GetCompanyParameterByCompany(string id)
		{
			try
			{
				ICompanyParameterService companyParameterService = new CompanyParameterService(db);
				return Ok(companyParameterService.GetCompanyParameterByCompany(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateCompanyParameter")]
		public ActionResult CreateCompanyParameter([FromBody] CompanyParameterItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
                    ICompanyParameterService companyParameterService = new CompanyParameterService(db, SysTransactionLogService);
					return Ok(companyParameterService.CreateCompanyParameter(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateCompanyParameter")]
		public ActionResult UpdateCompany([FromBody] CompanyParameterItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
                    ICompanyParameterService companyParameterService = new CompanyParameterService(db, SysTransactionLogService);
					return Ok(companyParameterService.UpdateCompanyParameter(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteCompanyParameter")]
		public ActionResult DeleteCompanyParameter([FromBody] RowIdentity parm)
		{
			try
			{
                ICompanyParameterService companyParameterService = new CompanyParameterService(db, SysTransactionLogService);
				return Ok(companyParameterService.DeleteCompanyParameter(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetDataCompaneParameterValidation")]
		public ActionResult GetDataCompaneParameterValidation([FromBody] CompanyParameterItemView view)
		{
			try
			{
				ICompanyParameterService companyParameterService = new CompanyParameterService(db);
				return Ok(companyParameterService.GetDataCompaneParameterValidation(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown
		[HttpPost]
        [Route("GetCurrencyDropDown")]
        public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCurrency(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		[HttpPost]
		[Route("GetMethodOfPaymentDropDown")]
		public ActionResult GetMethodOfPaymentDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetTaxTableDropDown")]
		public ActionResult GetTaxTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceTypeDropDown")]
		public ActionResult GetInvoiceTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetCalendarGroupDropDown")]
		public ActionResult GetCalendarGroupDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCalendarGroup(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetCreditTermDropDown")]
		public ActionResult GetCreditTermDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCreditTerm(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetEmployeeFilterActiveStatusDropdown")]
		public ActionResult GetActiveEmployeeTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IEmployeeTableService employeeTableService = new EmployeeTableService(db);
				return Ok(employeeTableService.GetActiveEmployeeTableDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
