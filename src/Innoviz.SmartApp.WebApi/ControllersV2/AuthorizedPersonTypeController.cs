using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class AuthorizedPersonTypeController : BaseControllerV2
	{

		public AuthorizedPersonTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetAuthorizedPersonTypeList/ByCompany")]
		public ActionResult GetAuthorizedPersonTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAuthorizedPersonTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetAuthorizedPersonTypeById/id={id}")]
		public ActionResult GetAuthorizedPersonTypeById(string id)
		{
			try
			{
				IAuthorizedPersonTypeService authorizedPersonTypeService = new AuthorizedPersonTypeService(db);
				return Ok(authorizedPersonTypeService.GetAuthorizedPersonTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateAuthorizedPersonType")]
		public ActionResult CreateAuthorizedPersonType([FromBody] AuthorizedPersonTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAuthorizedPersonTypeService authorizedPersonTypeService = new AuthorizedPersonTypeService(db, SysTransactionLogService);
					return Ok(authorizedPersonTypeService.CreateAuthorizedPersonType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateAuthorizedPersonType")]
		public ActionResult UpdateAuthorizedPersonType([FromBody] AuthorizedPersonTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAuthorizedPersonTypeService authorizedPersonTypeService = new AuthorizedPersonTypeService(db, SysTransactionLogService);
					return Ok(authorizedPersonTypeService.UpdateAuthorizedPersonType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteAuthorizedPersonType")]
		public ActionResult DeleteAuthorizedPersonType([FromBody] RowIdentity parm)
		{
			try
			{
				IAuthorizedPersonTypeService authorizedPersonTypeService = new AuthorizedPersonTypeService(db, SysTransactionLogService);
				return Ok(authorizedPersonTypeService.DeleteAuthorizedPersonType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
