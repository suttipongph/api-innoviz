﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class NCBAccountStatusController : BaseControllerV2
	{



		public NCBAccountStatusController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetNCBAccountStatusList/ByCompany")]
		public ActionResult GetNCBAccountStatusListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetNCBAccountStatusListvw(search));
				//return Ok();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetNCBAccountStatusById/id={id}")]
		public ActionResult GetNCBAccountStatusById(string id)
		{
			try
			{
				INCBAccountStatusService nCBAccountStatusService = new NCBAccountStatusService(db);
				return Ok(nCBAccountStatusService.GetNCBAccountStatusById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateNCBAccountStatus")]
		public ActionResult CreateNCBAccountStatus([FromBody] NCBAccountStatusItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					INCBAccountStatusService nCBAccountStatusService = new NCBAccountStatusService(db, SysTransactionLogService);
					return Ok(nCBAccountStatusService.CreateNCBAccountStatus(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateNCBAccountStatus")]
		public ActionResult UpdateNCBAccountStatus([FromBody] NCBAccountStatusItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					INCBAccountStatusService nCBAccountStatusService = new NCBAccountStatusService(db, SysTransactionLogService);
					return Ok(nCBAccountStatusService.UpdateNCBAccountStatus(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteNCBAccountStatus")]
		public ActionResult DeleteNCBAccountStatus([FromBody] RowIdentity parm)
		{
			try
			{
				INCBAccountStatusService nCBAccountStatusService = new NCBAccountStatusService(db, SysTransactionLogService);
				return Ok(nCBAccountStatusService.DeleteNCBAccountStatus(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
