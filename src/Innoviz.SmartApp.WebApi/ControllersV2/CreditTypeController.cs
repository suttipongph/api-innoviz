using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class CreditTypeController : BaseControllerV2
	{

		public CreditTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetCreditTypeList/ByCompany")]
		public ActionResult GetCreditTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetCreditTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetCreditTypeById/id={id}")]
		public ActionResult GetCreditTypeById(string id)
		{
			try
			{
				ICreditTypeService creditTypeService = new CreditTypeService(db);
				return Ok(creditTypeService.GetCreditTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateCreditType")]
		public ActionResult CreateCreditType([FromBody] CreditTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICreditTypeService creditTypeService = new CreditTypeService(db, SysTransactionLogService);
					return Ok(creditTypeService.CreateCreditType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateCreditType")]
		public ActionResult UpdateCreditType([FromBody] CreditTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICreditTypeService creditTypeService = new CreditTypeService(db, SysTransactionLogService);
					return Ok(creditTypeService.UpdateCreditType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteCreditType")]
		public ActionResult DeleteCreditType([FromBody] RowIdentity parm)
		{
			try
			{
				ICreditTypeService creditTypeService = new CreditTypeService(db, SysTransactionLogService);
				return Ok(creditTypeService.DeleteCreditType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
