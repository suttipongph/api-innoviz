﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class MaritalStatusController : BaseControllerV2
    {
        public MaritalStatusController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }

		[HttpPost]
		[Route("GetMaritalStatusList/ByCompany")]
		public ActionResult GetMaritalStatusListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetMaritalStatusListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetMaritalStatusById/id={id}")]
		public ActionResult GetMaritalStatusById(string id)
		{
			try
			{
				IMaritalStatusService maritalStatusService = new MaritalStatusService(db);
				return Ok(maritalStatusService.GetMaritalStatusById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateMaritalStatus")]
		public ActionResult CreateMaritalStatus([FromBody] MaritalStatusItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IMaritalStatusService maritalStatusService = new MaritalStatusService(db, SysTransactionLogService);
					return Ok(maritalStatusService.CreateMaritalStatus(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateMaritalStatus")]
		public ActionResult UpdateMaritalStatus([FromBody] MaritalStatusItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IMaritalStatusService maritalStatusService = new MaritalStatusService(db, SysTransactionLogService);
					return Ok(maritalStatusService.UpdateMaritalStatus(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteMaritalStatus")]
		public ActionResult DeleteMaritalStatus([FromBody] RowIdentity parm)
		{
			try
			{
				IMaritalStatusService maritalStatusService = new MaritalStatusService(db, SysTransactionLogService);
				return Ok(maritalStatusService.DeleteMaritalStatus(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
