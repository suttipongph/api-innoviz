﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class CurrencyController : BaseControllerV2
    {
        public CurrencyController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
        [HttpPost]
        [Route("GetCurrencyList/ByCompany")]
        public ActionResult GetGetCurrencyListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCurrencyListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetCurrencyById/id={id}")]
        public ActionResult GetCurrencyById(string id)
        {
            try
            {
                ICurrencyService currencyService = new CurrencyService(db);
                return Ok(currencyService.GetCurrencyById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateCurrency")]
        public ActionResult CreateCurrency([FromBody] CurrencyItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICurrencyService currencyService = new CurrencyService(db, SysTransactionLogService);
                    return Ok(currencyService.CreateCurrency(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateCurrency")]
        public ActionResult UpdateCurrency([FromBody] CurrencyItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICurrencyService currencyService = new CurrencyService(db, SysTransactionLogService);
                    return Ok(currencyService.UpdateCurrency(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteCurrency")]
        public ActionResult DeleteCurrency([FromBody] RowIdentity parm)
        {
            try
            {
                ICurrencyService currencyService = new CurrencyService(db, SysTransactionLogService);
                return Ok(currencyService.DeleteCurrency(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        #endregion
        #region ExchangeRate
        [HttpPost]
        [Route("GetExchangeRateList/ByCompany")]
        public ActionResult GetExchangeRateListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetExchangeRateListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetExchangeRateById/id={id}")]
        public ActionResult GetExchangeRateById(string id)
        {
            try
            {
                ICurrencyService currencyService = new CurrencyService(db, SysTransactionLogService);
                return Ok(currencyService.GetExchangeRateById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteExchangeRate")]
        public ActionResult DeleteExchangeRate([FromBody] RowIdentity parm)
        {
            try
            {
                ICurrencyService currencyService = new CurrencyService(db, SysTransactionLogService);
                return Ok(currencyService.DeleteExchangeRate(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateExchangeRate")]
        public ActionResult CreateExchangeRate([FromBody] ExchangeRateItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICurrencyService currencyService = new CurrencyService(db, SysTransactionLogService);
                    return Ok(currencyService.CreateExchangeRate(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateExchangeRate")]
        public ActionResult UpdateExchangeRate([FromBody] ExchangeRateItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICurrencyService currencyService = new CurrencyService(db, SysTransactionLogService);
                    return Ok(currencyService.UpdateExchangeRate(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("GetExchangeRateInitialData/id={currencyGUID}")]
        public ActionResult GetExchangeRateInitialData(string currencyGUID)
        {
            try
            {
                ICurrencyService currencyService = new CurrencyService(db, SysTransactionLogService);
                return Ok(currencyService.GetExchangeRateInitialData(currencyGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("GetCurrencyDropDown")]
        public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCurrency(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
    }
}
