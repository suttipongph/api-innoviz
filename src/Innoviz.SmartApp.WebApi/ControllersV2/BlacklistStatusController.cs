using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class BlacklistStatusController : BaseControllerV2
	{

		public BlacklistStatusController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetBlacklistStatusList/ByCompany")]
		public ActionResult GetBlacklistStatusListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBlacklistStatusListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBlacklistStatusById/id={id}")]
		public ActionResult GetBlacklistStatusById(string id)
		{
			try
			{
				IBlacklistStatusService blacklistStatusService = new BlacklistStatusService(db);
				return Ok(blacklistStatusService.GetBlacklistStatusById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBlacklistStatus")]
		public ActionResult CreateBlacklistStatus([FromBody] BlacklistStatusItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBlacklistStatusService blacklistStatusService = new BlacklistStatusService(db, SysTransactionLogService);
					return Ok(blacklistStatusService.CreateBlacklistStatus(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBlacklistStatus")]
		public ActionResult UpdateBlacklistStatus([FromBody] BlacklistStatusItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBlacklistStatusService blacklistStatusService = new BlacklistStatusService(db, SysTransactionLogService);
					return Ok(blacklistStatusService.UpdateBlacklistStatus(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBlacklistStatus")]
		public ActionResult DeleteBlacklistStatus([FromBody] RowIdentity parm)
		{
			try
			{
				IBlacklistStatusService blacklistStatusService = new BlacklistStatusService(db, SysTransactionLogService);
				return Ok(blacklistStatusService.DeleteBlacklistStatus(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
