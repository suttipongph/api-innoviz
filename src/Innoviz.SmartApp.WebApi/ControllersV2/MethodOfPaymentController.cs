using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class MethodOfPaymentController : BaseControllerV2
	{

		public MethodOfPaymentController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetMethodOfPaymentList/ByCompany")]
		public ActionResult GetMethodOfPaymentListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetMethodOfPaymentListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetMethodOfPaymentById/id={id}")]
		public ActionResult GetMethodOfPaymentById(string id)
		{
			try
			{
				IMethodOfPaymentService methodOfPaymentService = new MethodOfPaymentService(db);
				return Ok(methodOfPaymentService.GetMethodOfPaymentById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateMethodOfPayment")]
		public ActionResult CreateMethodOfPayment([FromBody] MethodOfPaymentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IMethodOfPaymentService methodOfPaymentService = new MethodOfPaymentService(db, SysTransactionLogService);
					return Ok(methodOfPaymentService.CreateMethodOfPayment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateMethodOfPayment")]
		public ActionResult UpdateMethodOfPayment([FromBody] MethodOfPaymentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IMethodOfPaymentService methodOfPaymentService = new MethodOfPaymentService(db, SysTransactionLogService);
					return Ok(methodOfPaymentService.UpdateMethodOfPayment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteMethodOfPayment")]
		public ActionResult DeleteMethodOfPayment([FromBody] RowIdentity parm)
		{
			try
			{
				IMethodOfPaymentService methodOfPaymentService = new MethodOfPaymentService(db, SysTransactionLogService);
				return Ok(methodOfPaymentService.DeleteMethodOfPayment(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetCompanyBankDropDown")]
		public ActionResult GetCompanyBankDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCompanyBank(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
	}
}
