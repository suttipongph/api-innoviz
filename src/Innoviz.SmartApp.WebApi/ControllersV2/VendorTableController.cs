﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class VendorTableController : BaseControllerV2
	{

		public VendorTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetVendorTableList/ByCompany")]
		public ActionResult GetVendorTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetVendorTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetVendorTableById/id={id}")]
		public ActionResult GetVendorTableById(string id)
		{
			try
			{
				IVendorTableService vendorTableService = new VendorTableService(db);
				return Ok(vendorTableService.GetVendorTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateVendorTable")]
		public ActionResult CreateVendorTable([FromBody] VendorTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IVendorTableService vendorTableService = new VendorTableService(db, SysTransactionLogService);
					return Ok(vendorTableService.CreateVendorTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateVendorTable")]
		public ActionResult UpdateVendorTable([FromBody] VendorTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IVendorTableService vendorTableService = new VendorTableService(db, SysTransactionLogService);
					return Ok(vendorTableService.UpdateVendorTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteVendorTable")]
		public ActionResult DeleteVendorTable([FromBody] RowIdentity parm)
		{
			try
			{
				IVendorTableService vendorTableService = new VendorTableService(db, SysTransactionLogService);
				return Ok(vendorTableService.DeleteVendorTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		[HttpPost]
		[Route("GetCurrencyDropDown")]
		public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCurrency(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetVendGroupDropDown")]
		public ActionResult GetVendGroupDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemVendGroup(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetLanguageDropDown")]
		public ActionResult GetLanguageDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemLanguage(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		[HttpGet]
		[Route("ValidateIsManualNumberSeq/companyId={companyId}")]
		public ActionResult ValidateIsManualNumberSeq(string companyId)
		{
			try
			{
				IVendorTableService vendorTableService = new VendorTableService(db);
				return Ok(vendorTableService.IsManual(companyId));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region ContactTrans
		[HttpPost]
		[Route("RelatedInfo/ContactTrans/GetContactTransList/ByCompany")]
		public ActionResult GetContactListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetContactTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ContactTrans/GetContactTransById/id={id}")]
		public ActionResult GetContactById(string id)
		{
			try
			{
				IContactTransService ContactTransService = new ContactTransService(db);
				return Ok(ContactTransService.GetContactTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ContactTrans/CreateContactTrans")]
		public ActionResult CreateContact([FromBody] ContactTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
					return Ok(ContactTransService.CreateContactTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ContactTrans/UpdateContactTrans")]
		public ActionResult UpdateContact([FromBody] ContactTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
					return Ok(ContactTransService.UpdateContactTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ContactTrans/DeleteContactTrans")]
		public ActionResult DeleteContact([FromBody] RowIdentity parm)
		{
			try
			{
				IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
				return Ok(ContactTransService.DeleteContactTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ContactTrans/GetContactTransInitialData/id={id}")]
		public ActionResult GetContactTransInitialData(string id)
		{
			try
			{
				IVendorTableService vendorTableService = new VendorTableService(db);
				return Ok(vendorTableService.GetContactTransInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Contact
		#region VendBank
		[HttpPost]
		[Route("RelatedInfo/VendBank/GetVendBankList/ByCompany")]
		public ActionResult GetVendBankListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetVendBankListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/VendBank/GetVendBankById/id={id}")]
		public ActionResult GetVendBankById(string id)
		{
			try
			{
				IVendBankService VendBankService = new VendBankService(db);
				return Ok(VendBankService.GetVendBankById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/VendBank/CreateVendBank")]
		public ActionResult CreateVendBank([FromBody] VendBankItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IVendBankService VendBankService = new VendBankService(db, SysTransactionLogService);
					return Ok(VendBankService.CreateVendBank(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/VendBank/UpdateVendBank")]
		public ActionResult UpdateVendBank([FromBody] VendBankItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IVendBankService VendBankService = new VendBankService(db, SysTransactionLogService);
					return Ok(VendBankService.UpdateVendBank(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/VendBank/DeleteVendBank")]
		public ActionResult DeleteVendBank([FromBody] RowIdentity parm)
		{
			try
			{
				IVendBankService VendBankService = new VendBankService(db, SysTransactionLogService);
				return Ok(VendBankService.DeleteVendBank(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("RelatedInfo/VendBank/GetBankGroupDropDown")]
		public ActionResult GetBankGroupDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("RelatedInfo/VendBank/GetVendorTableDropDown")]
		public ActionResult GetVendorTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemVendorTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion VendBank
		#region AdressTrans
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetAddressTransList/ByCompany")]
		public ActionResult GetAddressTransListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAdressTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/AddressTrans/GetAddressTransById/id={id}")]
		public ActionResult GetAddressTransById(string id)
		{
			try
			{
				IAddressTransService addressTransService = new AddressTransService(db);
				return Ok(addressTransService.GetAddressTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
	   [HttpPost]
		[Route("RelatedInfo/AddressTrans/CreateAddressTrans")]
		public ActionResult CreateAdressTrans([FromBody] AddressTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressTransService addressTransService = new AddressTransService(db, SysTransactionLogService);
					return Ok(addressTransService.CreateAddressTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/UpdateAddressTrans")]
		public ActionResult UpdateAdressTrans([FromBody] AddressTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressTransService addressTransService = new AddressTransService(db, SysTransactionLogService);
					return Ok(addressTransService.UpdateAddressTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/DeleteAddressTrans")]
		public ActionResult DeleteAdressTrans([FromBody] RowIdentity parm)
		{
			try
			{
				IAddressTransService addressTeansService = new AddressTransService(db, SysTransactionLogService);
				return Ok(addressTeansService.DeleteAddressTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/AddressTrans/GetAddressTransInitialData/id={id}")]
		public ActionResult GetAddressTransInitialData(string id)
		{
			try
			{
				IVendorTableService vendorTableService = new VendorTableService(db);
				return Ok(vendorTableService.GetContactTransInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Dropdown
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetAddressCountryDropDown")]
		public ActionResult GetRelatedInfoAddressTransAddressCountryDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemAddressCountry(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetAddressPostalCodeDropDown")]
		public ActionResult GetRelatedInfoAddressTransAddressPostalCodeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IAddressTransService addressTransService = new AddressTransService(db);
				return Ok(addressTransService.GetDropDownItemAddressPostalCode(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetOwnershipDropDown")]
		public ActionResult GetRelatedInfoAddressTransOwnershipDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemOwnership(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetPropertyTypeDropDown")]
		public ActionResult GetRelatedInfoAddressTransPropertyTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemPropertyType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetAddressProvinceDropDown")]
		public ActionResult GetAddressProvinceDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemAddressProvince(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetAddressDistrictDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressDistrictDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressDistrict(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		[HttpPost]

		[Route("RelatedInfo/AddressTrans/GetAddressSubDistrictDropDown")]
		public ActionResult GetRelatedInfoAddressTransAddressSubDistrictDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IAddressTransService addressTransService = new AddressTransService(db);
				return Ok(addressTransService.GetDropDownItemAddressSubDistrict(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
