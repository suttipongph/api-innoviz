﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class IntroducedByController : BaseControllerV2
    {
        public IntroducedByController(SmartAppContext context, ISysTransactionLogService transactionLogService)
           : base(context, transactionLogService)
        {

        }
        [HttpPost]
        [Route("GetIntroducedByList/ByCompany")]
        public ActionResult GetIntroducedByListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetIntroducedByListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetIntroducedByById/id={id}")]
        public ActionResult GetIntroducedByById(string id)
        {
            try
            {
                IIntroducedByService introducedByService = new IntroducedByService(db);
                return Ok(introducedByService.GetIntroducedByById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateIntroducedBy")]
        public ActionResult CreateIntroducedBy([FromBody] IntroducedByItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IIntroducedByService introducedByService = new IntroducedByService(db, SysTransactionLogService);
                    return Ok(introducedByService.CreateIntroducedBy(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateIntroducedBy")]
        public ActionResult UpdateIntroducedBy([FromBody] IntroducedByItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IIntroducedByService introducedByService = new IntroducedByService(db, SysTransactionLogService);
                    return Ok(introducedByService.UpdateIntroducedBy(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteIntroducedBy")]
        public ActionResult DeleteIntroducedBy([FromBody] RowIdentity parm)
        {
            try
            {
                IIntroducedByService introducedByService = new IntroducedByService(db, SysTransactionLogService);
                return Ok(introducedByService.DeleteIntroducedBy(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("GetVendorTableDropDown")]
        public ActionResult GetVendorTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemVendorTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
