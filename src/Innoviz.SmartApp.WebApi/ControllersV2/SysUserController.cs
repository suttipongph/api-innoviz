﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelHandler;
using Innoviz.SmartApp.Data.ViewModels;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.Controllers
{
    public class SysUserController: BaseControllerV2
    {
        
        public SysUserController(SmartAppContext context, ISysTransactionLogService transactionLogService, IConfiguration config) 
            : base(context, transactionLogService) {
        }

        
        //[HttpGet]
        //[Route("SysUserTableView/Get/{username}")]
        //public ActionResult<SysUserTableView> GetSysUserTableByUserName(string username) { 
        //    try {
        //        int site = GetSiteHeader();
        //        SysUserTableView user = userService.GetByUserNameWithEmpRolesBranchCompany(username, site);
                
        //        return Ok(user);
        //    }
        //    catch(Exception e) {
        //        throw SmartAppUtil.AddStackTrace(e);
        //    }
        //}
        

        [HttpPost]
        [Route("Function/InactiveUser")]
        [Route("Function/ActiveUser")]
        public ActionResult InActiveUser([FromBody] SysUserTableActivationParm model)
        {
            try
            {
                ISysUserService sysUserService = new SysUserService(db, SysTransactionLogService);
                return Ok(sysUserService.UpdateInActiveUser(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpPost]
        [Route("ValidateUserEmployeeMapping")]
        public ActionResult ValidateUserEmployeeMapping([FromBody]UserEmployeeMappingView model)
        {
            try
            {
                ISysUserService sysUserTableService = new SysUserService(db);
                return Ok(sysUserTableService.ValidateUserEmployeeMapping(model));
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ValidateUserEmployeeMappings")]
        public ActionResult ValidateUserEmployeeMappings([FromBody]IEnumerable<UserEmployeeMappingView> model)
        {
            try
            {
                ISysUserService sysUserTableService = new SysUserService(db);
                return Ok(sysUserTableService.ValidateUserEmployeeMappings(model));
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        [HttpPost]
        [Route("CheckExistingUser")]
        public ActionResult CheckExistingUser([FromBody] SysUserTableParm model)
        {
            try
            {
                ISysUserService sysUserTableService = new SysUserService(db);
                return Ok(sysUserTableService.CheckExistingUser(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region user settings
        [HttpGet]
        [Route("UserSettings/{userId}")]
        public ActionResult GetUserSettingsById(string userId)
        {
            try
            {
                ISysUserService sysUserTableService = new SysUserService(db);
                return Ok(sysUserTableService.GetUserSettingsById(userId));
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [HttpPost]
        [Route("UpdateSettings")]
        public ActionResult UpdateUserSettings([FromBody] SysUserSettingsItemView model)
        {
            try
            {
                ISysUserService sysUserTableService = new SysUserService(db, SysTransactionLogService);
                model = sysUserTableService.UpdateUserSettings(model);

                return Ok(model);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region UI
        [HttpPost]
        [Route("GetSysUserTableList")]
        public ActionResult GetSysUserTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetSysUserTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetSysUserTableById/id={id}")]
        public ActionResult GetSysUserTableById(string id)
        {
            try
            {
                ISysUserService sysUserTableService = new SysUserService(db);
                //return Ok(sysUserTableService.GetSysUserTableById(id));
                return Ok(sysUserTableService.GetSysUserTableByIdWithEmpRolesBranchCompany(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateSysUserTable")]
        public async Task<ActionResult> CreateSysUserTable([FromBody] SysUserTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ISysUserService sysUserTableService = new SysUserService(db, SysTransactionLogService);
                    return Ok(await sysUserTableService.CreateSysUserTable(vwModel, GetToken(), GetRequestIdFromRequest()));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateSysUserTable")]
        public ActionResult UpdateSysUserTable([FromBody] SysUserTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ISysUserService sysUserTableService = new SysUserService(db, SysTransactionLogService);
                    return Ok(sysUserTableService.UpdateSysUserTable(vwModel, GetSiteHeader()));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteSysUserTable")]
        public ActionResult DeleteSysUserTable([FromBody] RowIdentity parm)
        {
            try
            {
                ISysUserService sysUserTableService = new SysUserService(db, SysTransactionLogService);
                return Ok(sysUserTableService.DeleteSysUserTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("GetAdUserDropDown")]
        public async Task<ActionResult> GetAdUserForCreate([FromBody] SearchParameter search)
        {
            try
            {
                ISysUserService sysUserService = new SysUserService(db);

                string token = GetToken();
                string reqId = GetRequestIdFromRequest();
                string userApiEndpoint = "UserApi:BaseUrl".GetConfigurationValue() + "UserApi:User".GetConfigurationValue();
                
                return Ok(await sysUserService.GetAdUserDropDown(userApiEndpoint, token, reqId));
                //var result = new List<SelectItem<AdUserView>>()
                //{
                //    new SelectItem<AdUserView> { Value = "chanikarn.pr", Label = "chanikarn.pr" },
                //    new SelectItem<AdUserView> { Value = "AXIVZ01", Label = "AXIVZ01" },
                //    new SelectItem<AdUserView> { Value = "AXIVZ02", Label = "AXIVZ02" },
                //    new SelectItem<AdUserView> { Value = "AXIVZ03", Label = "AXIVZ03" },
                //    new SelectItem<AdUserView> { Value = "AXIVZ04", Label = "AXIVZ04" },
                //    new SelectItem<AdUserView> { Value = "AXIVZ05", Label = "AXIVZ05" },
                //    new SelectItem<AdUserView> { Value = "k2admin", Label = "k2admin" },
                //};
                //return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("GetCompanyDropDown")]
        public ActionResult GetCompanyDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService dropDownService = new SysDropDownService(db);
                return Ok(dropDownService.GetDropDownItemCompany(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("GetBranchDropDown")]
        public ActionResult GetBranchDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBranchService branchService = new BranchService(db);
                return Ok(branchService.GetDropDownItemNotFiltered(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("GetEmployeeTableDropDown")]
        public ActionResult GetEmployeeTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetEmployeeTableByInputCompanyDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("GetSysRoleTableDropDown")]
        public ActionResult GetSysRoleTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysAccessRightService sysAccessRightService = new SysAccessRightService(db);
                return Ok(sysAccessRightService.GetSysRoleTableDropDownNotFiltered(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        
        
    }
}
