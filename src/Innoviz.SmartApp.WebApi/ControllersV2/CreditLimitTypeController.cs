using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class CreditLimitTypeController : BaseControllerV2
	{

		public CreditLimitTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetCreditLimitTypeList/ByCompany")]
		public ActionResult GetCreditLimitTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetCreditLimitTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetCreditLimitTypeById/id={id}")]
		public ActionResult GetCreditLimitTypeById(string id)
		{
			try
			{
				ICreditLimitTypeService creditLimitTypeService = new CreditLimitTypeService(db);
				return Ok(creditLimitTypeService.GetCreditLimitTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateCreditLimitType")]
		public ActionResult CreateCreditLimitType([FromBody] CreditLimitTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICreditLimitTypeService creditLimitTypeService = new CreditLimitTypeService(db, SysTransactionLogService);
					return Ok(creditLimitTypeService.CreateCreditLimitType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateCreditLimitType")]
		public ActionResult UpdateCreditLimitType([FromBody] CreditLimitTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICreditLimitTypeService creditLimitTypeService = new CreditLimitTypeService(db, SysTransactionLogService);
					return Ok(creditLimitTypeService.UpdateCreditLimitType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteCreditLimitType")]
		public ActionResult DeleteCreditLimitType([FromBody] RowIdentity parm)
		{
			try
			{
				ICreditLimitTypeService creditLimitTypeService = new CreditLimitTypeService(db, SysTransactionLogService);
				return Ok(creditLimitTypeService.DeleteCreditLimitType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetCreditLimitTypeDropDown")]
		public ActionResult GetCreditLimitTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCreditLimitType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
	}
}
