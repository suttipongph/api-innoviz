using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class CustomerTableController : BaseControllerV2
    {

        public CustomerTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
        [HttpPost]
        [Route("GetCustomerTableList/ByCompany")]
        public ActionResult GetCustomerTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustomerTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetCustomerTableById/id={id}")]
        public ActionResult GetCustomerTableById(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetCustomerTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateCustomerTable")]
        public ActionResult CreateCustomerTable([FromBody] CustomerTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustomerTableService customerTableService = new CustomerTableService(db, SysTransactionLogService);
                    return Ok(customerTableService.CreateCustomerTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateCustomerTable")]
        public ActionResult UpdateCustomerTable([FromBody] CustomerTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustomerTableService customerTableService = new CustomerTableService(db, SysTransactionLogService);
                    return Ok(customerTableService.UpdateCustomerTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteCustomerTable")]
        public ActionResult DeleteCustomerTable([FromBody] RowIdentity parm)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db, SysTransactionLogService);
                return Ok(customerTableService.DeleteCustomerTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetCustomerTableInitialData/companyId={companyId}")]
        public ActionResult GetCustomerTableInitialData(string companyId)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetCustomerTableInitialData(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown

        [HttpPost]
        [Route("GetBlacklistStatusDropDown")]
        public ActionResult GetBlacklistStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBlacklistStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetBusinessSegmentDropDown")]
        public ActionResult GetBusinessSegmentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessSegment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetBusinessSizeDropDown")]
        public ActionResult GetBusinessSizeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessSize(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetBusinessTypeDropDown")]
        public ActionResult GetBusinessTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetCreditScoringDropDown")]
        public ActionResult GetCreditScoringDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditScoring(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetCurrencyDropDown")]
        [Route("RelatedInfo/CustTransaction/GetCurrencyDropDown")]
        public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCurrency(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetCustGroupDropDown")]
        public ActionResult GetCustGroupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Function/UpdateCustomerTableStatus/GetDocumentStatusDropDown")]
        [Route("GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetDropDownItemCustomerStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetEmployeeTableDropDown")]
        public ActionResult GetEmployeeTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetExposureGroupDropDown")]
        public ActionResult GetExposureGroupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemExposureGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetGenderDropDown")]
        public ActionResult GetGenderDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemGender(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetGradeClassificationDropDown")]
        public ActionResult GetGradeClassificationDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemGradeClassification(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetIntroducedByDropDown")]
        public ActionResult GetIntroducedByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemIntroducedBy(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetKYCSetupDropDown")]
        public ActionResult GetKYCSetupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemKYCSetup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetLedgerDimensionDropDown")]
        public ActionResult GetLedgerDimensionDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetLineOfBusinessDropDown")]
        public ActionResult GetLineOfBusinessDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLineOfBusiness(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetMaritalStatusDropDown")]
        public ActionResult GetMaritalStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMaritalStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetNationalityDropDown")]
        public ActionResult GetNationalityDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemNationality(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetNCBAccountStatusDropDown")]
        public ActionResult GetNCBAccountStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemNCBAccountStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetOccupationDropDown")]
        public ActionResult GetOccupationDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemOccupation(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetParentCompanyDropDown")]
        public ActionResult GetParentCompanyDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemParentCompany(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetRaceDropDown")]
        public ActionResult GetRaceDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRace(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetRegistrationTypeDropDown")]
        public ActionResult GetRegistrationTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRegistrationType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetTerritoryDropDown")]
        public ActionResult GetTerritoryDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTerritory(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetVendorTableDropDown")]
        public ActionResult GetVendorTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemVendorTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetWithholdingTaxGroupDropDown")]
        public ActionResult GetWithholdingTaxGroupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        [HttpGet]
        [Route("ValidateIsManualNumberSeq/custGroupId={custGroupId}")]
        public ActionResult ValidateIsManualNumberSeq(string custGroupId)
        {
            try
            {
                ICustomerTableService customerService = new CustomerTableService(db);
                return Ok(customerService.IsManualByCustGroup(custGroupId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("GetEmployeeTableByUserNameAndCompany/{userId}/{companyGUID}")]
        public ActionResult<EmployeeTableItemView> GetEmployeeTableByUserNameAndCompany(string userId, string companyGUID)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetEmployeeTableByUserIdAndCompany(userId, companyGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ValidatePassportIdDuplicate/{customerId}/{passPortId}")]
        public ActionResult<CustomerTableItemView> ValidatePassportIdDuplicate(string customerId, string passPortId)
        {
            try
            {
                ICustomerTableService customerService = new CustomerTableService(db);
                return Ok(customerService.IsPassportIdDuplicate(customerId, passPortId));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpGet]
        [Route("ValidateTaxIdDuplicate/{customerId}/{taxId}")]
        public ActionResult<CustomerTableItemView> ValidateTaxIdDuplicate(string customerId, string taxId)
        {
            try
            {
                ICustomerTableService customerService = new CustomerTableService(db);
                return Ok(customerService.IsTaxIdDuplicate(customerId, taxId));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        [HttpPost]
        [Route("GetCustomerTableByCalcAge")]
        public ActionResult GetCustomerTableByCalcAge([FromBody] CustomerTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustomerTableService customerTableService = new CustomerTableService(db, SysTransactionLogService);
                    return Ok(customerTableService.GetAge(vwModel.DateOfBirth));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region relatedInfo
        #region GuarantorTrans
        [HttpPost]
        [Route("RelatedInfo/GuarantorTrans/GetGuarantorTransList/ByCompany")]
        public ActionResult GetGuarantorTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetGuarantorTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/GuarantorTrans/GetGuarantorTransById/id={id}")]
        public ActionResult GetGuarantorTransById(string id)
        {
            try
            {
                IGuarantorTransService guarantorTransService = new GuarantorTransService(db);
                return Ok(guarantorTransService.GetGuarantorTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/GuarantorTrans/CreateGuarantorTrans")]
        public ActionResult CreateGuarantorTrans([FromBody] GuarantorTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorTransService guarantorTransService = new GuarantorTransService(db, SysTransactionLogService);
                    return Ok(guarantorTransService.CreateGuarantorTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/GuarantorTrans/UpdateGuarantorTrans")]
        public ActionResult UpdateGuarantorTrans([FromBody] GuarantorTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IGuarantorTransService guarantorTransService = new GuarantorTransService(db, SysTransactionLogService);
                    return Ok(guarantorTransService.UpdateGuarantorTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/GuarantorTrans/DeleteGuarantorTrans")]
        public ActionResult DeleteGuarantorTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IGuarantorTransService guarantorTransService = new GuarantorTransService(db, SysTransactionLogService);
                return Ok(guarantorTransService.DeleteGuarantorTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/GuarantorTrans/GetGuarantorTransInitialData/id={id}")]
        public ActionResult GetGuarantorInitialData(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetGuarantorTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        #region Dropdown
        [HttpPost]
        [Route("RelatedInfo/GuarantorTrans/GetGuarantorTypeDropDown")]
        public ActionResult GetGuarantorTransGuarantorTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemGuarantorType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/GuarantorTrans/GetRelatedPersonTableDropDown")]
        public ActionResult GetGuarantorTransRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region JointVentureTrans
        [HttpPost]
        [Route("RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        public ActionResult GetJointVentureTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetJointVentureTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        public ActionResult GetJointVentureTransById(string id)
        {
            try
            {
                IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                return Ok(jointVentureTransService.GetJointVentureTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        public ActionResult CreateJointVentureTrans([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService jointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(jointVentureTransService.CreateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        public ActionResult UpdateJointVentureTrans([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService jointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(jointVentureTransService.UpdateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        public ActionResult DeleteJointVentureTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IJointVentureTransService jointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                return Ok(jointVentureTransService.DeleteJointVentureTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        public ActionResult GetJointVentureInitialData(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetJointVentureTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetJointVentureTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region CustVisitingTrans
        [HttpPost]
        [Route("RelatedInfo/CustVisitingTrans/GetCustVisitingTransList/ByCompany")]
        public ActionResult GetCustVisitingTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustVisitingTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/CustVisitingTrans/GetCustVisitingTransById/id={id}")]
        public ActionResult GetCustVisitingTransById(string id)
        {
            try
            {
                ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db);
                return Ok(custVisitingTransService.GetCustVisitingTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CustVisitingTrans/CreateCustVisitingTrans")]
        public ActionResult CreateCustVisitingTrans([FromBody] CustVisitingTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db, SysTransactionLogService);
                    return Ok(custVisitingTransService.CreateCustVisitingTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CustVisitingTrans/UpdateCustVisitingTrans")]
        public ActionResult UpdateCustVisitingTrans([FromBody] CustVisitingTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db, SysTransactionLogService);
                    return Ok(custVisitingTransService.UpdateCustVisitingTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CustVisitingTrans/DeleteCustVisitingTrans")]
        public ActionResult DeleteCustVisitingTrans([FromBody] RowIdentity parm)
        {
            try
            {
                ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db, SysTransactionLogService);
                return Ok(custVisitingTransService.DeleteCustVisitingTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/CustVisitingTrans/GetCustVisitingTransInitialData/id={id}")]
        public ActionResult GetCustVisitingTransInitialData(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetCustVisitingTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("RelatedInfo/CustVisitingTrans/GetEmployeeTableDropDown")]
        public ActionResult GetCustVisitingTransEmployeeTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region OwnerTrans
        [HttpPost]
        [Route("RelatedInfo/OwnerTrans/GetOwnerTransList/ByCompany")]
        public ActionResult GetOwnerTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetOwnerTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/OwnerTrans/GetOwnerTransById/id={id}")]
        public ActionResult GetOwnerTransById(string id)
        {
            try
            {
                IOwnerTransService ownerTransService = new OwnerTransService(db);
                return Ok(ownerTransService.GetOwnerTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/OwnerTrans/CreateOwnerTrans")]
        public ActionResult CreateOwnerTrans([FromBody] OwnerTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IOwnerTransService ownerTransService = new OwnerTransService(db, SysTransactionLogService);
                    return Ok(ownerTransService.CreateOwnerTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/OwnerTrans/UpdateOwnerTrans")]
        public ActionResult UpdateOwnerTrans([FromBody] OwnerTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IOwnerTransService ownerTransService = new OwnerTransService(db, SysTransactionLogService);
                    return Ok(ownerTransService.UpdateOwnerTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/OwnerTrans/DeleteOwnerTrans")]
        public ActionResult DeleteOwnerTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IOwnerTransService ownerTransService = new OwnerTransService(db, SysTransactionLogService);
                return Ok(ownerTransService.DeleteOwnerTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/OwnerTrans/GetOwnerTransInitialData/id={id}")]
        public ActionResult GetOwnerTransInitialData(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetOwnerTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("RelatedInfo/OwnerTrans/GetRelatedPersonTableDropDown")]
        public ActionResult GetOwnerTransRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region AuthorizedPersonTrans
        [HttpPost]
        [Route("RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]
        public ActionResult GetAuthorizedPersonListByCompany([FromBody] SearchParameter search)

        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAuthorizedPersonTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]
        public ActionResult GetAuthorizedPersonById(string id)
        {
            try
            {
                IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(AuthorizedPersonTransService.GetAuthorizedPersonTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AuthorizedPersonTrans/CreateAuthorizedPersonTrans")]
        public ActionResult CreateAuthorizedPerson([FromBody] AuthorizedPersonTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db, SysTransactionLogService);
                    return Ok(AuthorizedPersonTransService.CreateAuthorizedPersonTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/AuthorizedPersonTrans/UpdateAuthorizedPersonTrans")]
        public ActionResult UpdateAuthorizedPerson([FromBody] AuthorizedPersonTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db, SysTransactionLogService);
                    return Ok(AuthorizedPersonTransService.UpdateAuthorizedPersonTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpPost]
        [Route("RelatedInfo/AuthorizedPersonTrans/DeleteAuthorizedPersonTrans")]
        public ActionResult DeleteAuthorizedPerson([FromBody] RowIdentity parm)
        {
            try
            {
                IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db, SysTransactionLogService);
                return Ok(AuthorizedPersonTransService.DeleteAuthorizedPersonTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransInitialData/id={id}")]
        public ActionResult GetAuthorizedPersonTransInitialData(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetAuthorizedPersonTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetRelatedInfoAuthorizedPersonTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]
        public ActionResult GetRelatedInfoAuthorizedPersonTransRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AuthorizedPersonTrans
        #region ContactPersonTrans
        [HttpPost]
        [Route("RelatedInfo/ContactPersonTrans/GetContactPersonTransList/ByCompany")]
        public ActionResult GetContactPersonTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetContactPersonTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/ContactPersonTrans/GetContactPersonTransById/id={id}")]
        public ActionResult GetContactPersonTransById(string id)
        {
            try
            {
                IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db);
                return Ok(contactPersonTransService.GetContactPersonTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactPersonTrans/CreateContactPersonTrans")]
        public ActionResult CreateContactPersonTrans([FromBody] ContactPersonTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db, SysTransactionLogService);
                    return Ok(contactPersonTransService.CreateContactPersonTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactPersonTrans/UpdateContactPersonTrans")]
        public ActionResult UpdateContactPersonTrans([FromBody] ContactPersonTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db, SysTransactionLogService);
                    return Ok(contactPersonTransService.UpdateContactPersonTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactPersonTrans/DeleteContactPersonTrans")]
        public ActionResult DeleteContactPersonTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db, SysTransactionLogService);
                return Ok(contactPersonTransService.DeleteContactPersonTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/ContactPersonTrans/GetContactPersonTransInitialData/id={id}")]
        public ActionResult GetContactPersonTransInitialData(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetContactPersonTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactPersonTrans/GetRelatedPersonTableDropDown")]
        public ActionResult GetContactPersonTransRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region ContactTrans
        [HttpPost]
        [Route("RelatedInfo/ContactTrans/GetContactTransList/ByCompany")]
        public ActionResult GetContactListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetContactTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/ContactTrans/GetContactTransById/id={id}")]
        public ActionResult GetContactById(string id)
        {
            try
            {
                IContactTransService ContactTransService = new ContactTransService(db);
                return Ok(ContactTransService.GetContactTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactTrans/CreateContactTrans")]
        public ActionResult CreateContact([FromBody] ContactTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
                    return Ok(ContactTransService.CreateContactTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactTrans/UpdateContactTrans")]
        public ActionResult UpdateContact([FromBody] ContactTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
                    return Ok(ContactTransService.UpdateContactTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactTrans/DeleteContactTrans")]
        public ActionResult DeleteContact([FromBody] RowIdentity parm)
        {
            try
            {
                IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
                return Ok(ContactTransService.DeleteContactTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/ContactTrans/GetContactTransInitialData/id={id}")]
        public ActionResult GetContactTransInitialData(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetContactTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Contact
        #region Memo
        [HttpPost]
        [Route("RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult GetMemoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetMemoById(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult DeleteMemo([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialData(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Memo
        #region ProjectReferenceTrans
        [HttpPost]
        [Route("RelatedInfo/ProjectReferenceTrans/GetProjectReferenceTransList/ByCompany")]
        public ActionResult GetProjectReferenceTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);

                return Ok(sysListViewService.GetProjectReferenceTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/ProjectReferenceTrans/GetProjectReferenceTransById/id={id}")]
        public ActionResult GetProjectReferenceTransById(string id)
        {
            try
            {
                IProjectReferenceTransService projectReferenceTransService = new ProjectReferenceTransService(db);
                return Ok(projectReferenceTransService.GetProjectReferenceTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ProjectReferenceTrans/CreateProjectReferenceTrans")]
        public ActionResult CreateProjectReferenceTrans([FromBody] ProjectReferenceTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IProjectReferenceTransService projectReferenceTransService = new ProjectReferenceTransService(db, SysTransactionLogService);
                    return Ok(projectReferenceTransService.CreateProjectReferenceTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ProjectReferenceTrans/UpdateProjectReferenceTrans")]
        public ActionResult UpdateProjectReferenceTrans([FromBody] ProjectReferenceTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IProjectReferenceTransService projectReferenceTransService = new ProjectReferenceTransService(db, SysTransactionLogService);
                    return Ok(projectReferenceTransService.UpdateProjectReferenceTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("RelatedInfo/ProjectReferenceTrans/DeleteProjectReferenceTrans")]
        public ActionResult DeleteProjectReferenceTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IProjectReferenceTransService projectReferenceTransService = new ProjectReferenceTransService(db, SysTransactionLogService);
                return Ok(projectReferenceTransService.DeleteProjectReferenceTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("RelatedInfo/ProjectReferenceTrans/GetProjectReferenceTransInitialData/id={id}")]
        public ActionResult GetProjectReferenceTransInitialData(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetProjectReferenceTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CustomerCreditLimitByProduct
        [HttpPost]
        [Route("RelatedInfo/CustomerCreditLimitByProduct/GetCustomerCreditLimitByProductList/ByCompany")]
        public ActionResult GetCustomerCreditLimitByProductListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustomerCreditLimitByProductListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/CustomerCreditLimitByProduct/GetCustomerCreditLimitByProductById/id={id}")]
        public ActionResult GetCustomerCreditLimitByProductById(string id)
        {
            try
            {
                ICustomerCreditLimitByProductService customerCreditLimitByProductService = new CustomerCreditLimitByProductService(db);
                return Ok(customerCreditLimitByProductService.GetCustomerCreditLimitByProductById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CustomerCreditLimitByProduct/CreateCustomerCreditLimitByProduct")]
        public ActionResult CreateCustomerCreditLimitByProduct([FromBody] CustomerCreditLimitByProductItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustomerCreditLimitByProductService customerCreditLimitByProductService = new CustomerCreditLimitByProductService(db, SysTransactionLogService);
                    return Ok(customerCreditLimitByProductService.CreateCustomerCreditLimitByProduct(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CustomerCreditLimitByProduct/UpdateCustomerCreditLimitByProduct")]
        public ActionResult UpdateCustomerCreditLimitByProduct([FromBody] CustomerCreditLimitByProductItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustomerCreditLimitByProductService customerCreditLimitByProductService = new CustomerCreditLimitByProductService(db, SysTransactionLogService);
                    return Ok(customerCreditLimitByProductService.UpdateCustomerCreditLimitByProduct(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CustomerCreditLimitByProduct/DeleteCustomerCreditLimitByProduct")]
        public ActionResult DeleteCustomerCreditLimitByProduct([FromBody] RowIdentity parm)
        {
            try
            {
                ICustomerCreditLimitByProductService customerCreditLimitByProductService = new CustomerCreditLimitByProductService(db, SysTransactionLogService);
                return Ok(customerCreditLimitByProductService.DeleteCustomerCreditLimitByProduct(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/CustomerCreditLimitByProduct/GetCustomerCreditLimitByProductInitialData/id={id}")]
        public ActionResult GetCustomerCreditLimitByProductInitialData(string id)
        {
            try
            {
                ICustomerCreditLimitByProductService customerCreditLimitByProductService = new CustomerCreditLimitByProductService(db, SysTransactionLogService);
                return Ok(customerCreditLimitByProductService.GetCustomerCreditLimitByProductInitialDataByCustomerTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        #region AddressTrans
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetAddressTransList/ByCompany")]
        public ActionResult GetAddressTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAddressTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/AddressTrans/GetAddressTransById/id={id}")]
        public ActionResult GetAddressTransById(string id)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetAddressTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/CreateAddressTrans")]
        public ActionResult CreateAddressTrans([FromBody] AddressTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAddressTransService addressTransService = new AddressTransService(db, SysTransactionLogService);
                    return Ok(addressTransService.CreateAddressTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/UpdateAddressTrans")]
        public ActionResult UpdateAddressTrans([FromBody] AddressTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAddressTransService addressTransService = new AddressTransService(db, SysTransactionLogService);
                    return Ok(addressTransService.UpdateAddressTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/DeleteAddressTrans")]
        public ActionResult DeleteAddressTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db, SysTransactionLogService);
                return Ok(addressTransService.DeleteAddressTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/AddressTrans/GetAddressTransInitialData/id={id}")]
        public ActionResult GetAddressTransInitialData(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetAddressTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetAddressCountryDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressCountryDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAddressCountry(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetAddressDistrictDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressDistrictDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressDistrict(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetAddressPostalCodeDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressPostalCodeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressPostalCode(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetAddressProvinceDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressProvinceDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressProvince(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetAddressSubDistrictDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressSubDistrictDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressSubDistrict(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetOwnershipDropDown")]
        public ActionResult GetRelatedInfoAddressTransOwnershipDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemOwnership(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetPropertyTypeDropDown")]
        public ActionResult GetRelatedInfoAddressTransPropertyTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemPropertyType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region FinancialStatementTrans
        [HttpPost]
        [Route("RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        public ActionResult GetFinancialStatementTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetFinancialStatementTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        public ActionResult GetFinancialStatementTransById(string id)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                return Ok(financialStatementTransService.GetFinancialStatementTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        public ActionResult CreateFinancialStatementTrans([FromBody] FinancialStatementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                    return Ok(financialStatementTransService.CreateFinancialStatementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        public ActionResult UpdateFinancialStatementTrans([FromBody] FinancialStatementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                    return Ok(financialStatementTransService.UpdateFinancialStatementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        public ActionResult DeleteFinancialStatementTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                return Ok(financialStatementTransService.DeleteFinancialStatementTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        public ActionResult GetFinancialStatementTransInitialData(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetFinancialStatementTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        #endregion
        #endregion
        #region TaxReportTrans
        [HttpPost]
        [Route("RelatedInfo/TaxReportTrans/GetTaxReportTransList/ByCompany")]
        public ActionResult GetTaxReportTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetTaxReportTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/TaxReportTrans/GetTaxReportTransById/id={id}")]
        [Route("RelatedInfo/TaxReportTrans/Function/ImportTaxReport/GetTaxReportTransById/id={id}")]
        public ActionResult GetTaxReportTransById(string id)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db);
                return Ok(taxReportTransService.GetTaxReportTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/TaxReportTrans/CreateTaxReportTrans")]
        public ActionResult CreateTaxReportTrans([FromBody] TaxReportTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ITaxReportTransService taxReportTransService = new TaxReportTransService(db, SysTransactionLogService);
                    return Ok(taxReportTransService.CreateTaxReportTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/TaxReportTrans/UpdateTaxReportTrans")]
        public ActionResult UpdateTaxReportTrans([FromBody] TaxReportTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ITaxReportTransService taxReportTransService = new TaxReportTransService(db, SysTransactionLogService);
                    return Ok(taxReportTransService.UpdateTaxReportTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/TaxReportTrans/DeleteTaxReportTrans")]
        public ActionResult DeleteTaxReportTrans([FromBody] RowIdentity parm)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db, SysTransactionLogService);
                return Ok(taxReportTransService.DeleteTaxReportTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/TaxReportTrans/GetTaxReportTransInitialData/id={id}")]
        public ActionResult GetTaxReportTransInitialData(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetTaxReportTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        #endregion
        #endregion
        #region CustBank
        [HttpPost]
        [Route("RelatedInfo/CustBank/GetCustBankList/ByCompany")]
        public ActionResult GetCustBankListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustBankListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/CustBank/GetCustBankById/id={id}")]
        public ActionResult GetCustBankById(string id)
        {
            try
            {
                ICustBankService custBankService = new CustBankService(db);
                return Ok(custBankService.GetCustBankById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CustBank/CreateCustBank")]
        public ActionResult CreateCustBank([FromBody] CustBankItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustBankService custBankService = new CustBankService(db, SysTransactionLogService);
                    return Ok(custBankService.CreateCustBank(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CustBank/UpdateCustBank")]
        public ActionResult UpdateCustBank([FromBody] CustBankItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustBankService custBankService = new CustBankService(db, SysTransactionLogService);
                    return Ok(custBankService.UpdateCustBank(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CustBank/DeleteCustBank")]
        public ActionResult DeleteCustBank([FromBody] RowIdentity parm)
        {
            try
            {
                ICustBankService custBankService = new CustBankService(db, SysTransactionLogService);
                return Ok(custBankService.DeleteCustBank(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
       
             [HttpGet]
        [Route("RelatedInfo/CustBank/GetCustBankInitialData/id={id}")]
        public ActionResult GetCustBankInitialData(string id)
        {
            try
            {
                ICustBankService custBankService = new CustBankService(db);
                return Ok(custBankService.GetCustBankInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("RelatedInfo/CustBank/GetBankGroupDropDown")]
        public ActionResult GetBankGroupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/CustBank/GetBankTypeDropDown")]
        public ActionResult GetBankTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/CustBank/GetCustomerTableDropDown")]
        [Route("RelatedInfo/Inquiryassignmentagreementoutstanding/GetCustomerTableDropDown")]
        [Route("RelatedInfo/creditoutstanding/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Inquiryassignmentagreementoutstanding/GetBuyerTableDropDown")]
        [Route("RelatedInfo/CustTransaction/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/creditoutstanding/GetCreditLimitTypeDropDown")]
        public ActionResult GetCreditLimitTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditLimitType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region CustBusinessCollateral
        [HttpPost]
        [Route("RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        public ActionResult GetCustBusinessCollateralListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustBusinessCollateralListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        public ActionResult GetCustBusinessCollateralById(string id)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(CreditAppReqBusCollateralInfoService.GetCustBusinessCollateralById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/BusinessCollateral/UpdateCustBusinessCollateral")]
        public ActionResult UpdateCustBusinessCollateral([FromBody] CustBusinessCollateralItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db, SysTransactionLogService);
                    return Ok(CreditAppReqBusCollateralInfoService.UpdateCustBusinessCollateral(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown BusinessCollateral
        [HttpPost]
        [Route("RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        public ActionResult GetBusinessCollateralStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        public ActionResult GetBusinessCollateralTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeDropDown")]
        public ActionResult GetBusinessCollateralSubTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralSubType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #endregion
        #region Attachment
        [HttpPost]
        [Route("RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentById(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Attachment/CreateAttachment")]
        public ActionResult CreateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Attachment/DeleteAttachment")]
        public ActionResult DeleteAttachment([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefId([FromBody]RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion 
        #region CustTrans
        [HttpPost]
        [Route("RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        public ActionResult GetCustTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CustTransaction/GetCustTransListByCustomer/ByCompany")]
        public ActionResult GetCustTransListByCustomer([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        public ActionResult GetCustTransById(string id)
        {
            try
            {
                ICustTransService custTransService = new CustTransService(db);
                return Ok(custTransService.GetCustTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        public ActionResult GetCustomerTablebyCusttrans(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetCustomerTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CustTransaction/GetInvoiceTableDropDown")]
        public ActionResult GetInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CustTransaction/GetInvoiceTypeDropDown")]
        public ActionResult GetInvoiceTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        #region function
        [HttpPost]
        [Route("Function/UpdateCustomerTableBlacklistAndWatchlistStatus/GetBlacklistStatusDropDown")]
        public ActionResult GetBlacklistStatusDropdown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBlacklistStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Function/UpdateCustomerTableBlacklistAndWatchlistStatus/GetDocumentReasonDropdown")]
        [Route("Function/UpdateCustomerTableStatus/GetDocumentReasonDropdown")]
        public ActionResult GetDocumentReasonDropdown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Function/UpdateCustomerTableStatus/GetUpdateCustomerTableStatusById/id={id}")]
        public ActionResult<bool> GetUpdateCustomerTableStatusById(string id)
        {
            try
            {
                ICustomerTableService customerService = new CustomerTableService(db);
                return Ok(customerService.GetUpdateCustomerTableStatusById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Function/UpdateCustomertableStatus")]
        public ActionResult UpdateCustomertableStatus([FromBody] UpdateCustomerTableStatusParamView param)
        {
            try
            {
                ICustomerTableService customerService = new CustomerTableService(db);
                return Ok(customerService.UpdateCustomerTableStatus(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpGet]
        [Route("Function/UpdateCustomerTableBlacklistAndWatchlistStatus/GetUpdateCustomerTableBlacklistStatusById/id={id}")]
        public ActionResult<bool> GetUpdateCustomerTableBlacklistStatusById(string id)
        {
            try
            {
                ICustomerTableService customerService = new CustomerTableService(db);
                return Ok(customerService.GetUpdateCustomerTableBlacklistStatusById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Function/UpdateCustomerTableBlacklistAndWatchlistStatus")]
        public ActionResult UpdateCustomerTableBlacklistAndWatchlistStatus([FromBody] UpdateCustomerTableBlacklistStatusParamView param)
        {
            try
            {
                ICustomerTableService customerService = new CustomerTableService(db);
                return Ok(customerService.UpdateCustomerTableBlacklistStatus(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/TaxReportTrans/Function/ImportTaxReport/CreateImportTaxReport")]
        public ActionResult CreateImportTaxReport([FromBody] ImportTaxReportView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ITaxReportTransService taxReportTransService = new TaxReportTransService(db, SysTransactionLogService);
                    return Ok(taxReportTransService.CreateImportTaxReport(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("RelatedInfo/TaxReportTrans/Function/ImportTaxReport/GetImportTaxReportByRefType/id={id}/refType={refType}")]
        public ActionResult GetImportTaxReportByRefType(string id, int refType)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db);
                return Ok(taxReportTransService.GetImportTaxReportByRefType(id, refType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region
        [HttpPost]
        [Route("RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        [Route("RelatedInfo/RetentionTransaction/GetRetentionTransListByCustomer/ByCompany")]
        public ActionResult GetRetentionTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetRetentionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        public ActionResult GetRetentionTransById(string id)
        {
            try
            {
                IRetentionTransService retentionTransService = new RetentionTransService(db);
                return Ok(retentionTransService.GetRetentionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/RetentionTransaction/CreateRetentionTrans")]
        public ActionResult CreateRetentionTrans([FromBody] RetentionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IRetentionTransService retentionTransService = new RetentionTransService(db, SysTransactionLogService);
                    return Ok(retentionTransService.CreateRetentionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/RetentionTransaction/UpdateRetentionTrans")]
        public ActionResult UpdateRetentionTrans([FromBody] RetentionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IRetentionTransService retentionTransService = new RetentionTransService(db, SysTransactionLogService);
                    return Ok(retentionTransService.UpdateRetentionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/RetentionTransaction/DeleteRetentionTrans")]
        public ActionResult DeleteRetentionTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IRetentionTransService retentionTransService = new RetentionTransService(db, SysTransactionLogService);
                return Ok(retentionTransService.DeleteRetentionTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region dropdown 
        [HttpPost]
        [Route("RelatedInfo/RetentionTransaction/GetCreditAppTableDropDown")]
        [Route("RelatedInfo/creditoutstanding/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/RetentionTransaction/GetBuyerTableDropDown")]
        public ActionResult GetDropDownItemBuyerTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/RetentionTransaction/GetBuyerAgreementTableDropDown")]
      public ActionResult GetBuyerAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #endregion
        #region Outstanding
        [HttpPost]
        [Route("RelatedInfo/RetentionOutstanding/GetInquiryRetentionOutstandingList/ByCompany")]
        public ActionResult GetRetentionOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                IRetentionTransService retentionTransService = new RetentionTransService(db);
                return Ok(retentionTransService.GetRetentionOutstandingByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/Inquiryassignmentagreementoutstanding/GetInquiryAssignmentOutstandingList/ByCompany")]
        public ActionResult GetInquiryAssignmentOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementOutstanding(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion Oustanding
        #endregion
        #region Customer Credit Outstanding
        [HttpGet]
        [Route("RelatedInfo/creditoutstanding/GetInquiryCreditOutstandingListByCustByGUID/id={id}")]
        public ActionResult GetInquiryCreditOutstandingListByCustByGUID(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetCreditOutstandingByCustomer(new Guid(id), DateTime.Now));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/creditoutstanding/getInquiryCreditOutstandingListByCustToList/ByCompany")]
        public ActionResult GetInquiryCreditOutstandingListByCust([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInquiryCreditOutstandingListByCustvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region ImportFinancialStatementTrans
        [HttpGet]
        [Route("RelatedInfo/FinancialStatementTrans/Function/ImportFinancialStatementTrans/GetFinancialStatementTransRefId/refGUID={refGUID}/refType={refType}")]
        public ActionResult GetFinancialStatementTransRefId(string refGUID, int reftype)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                return Ok(financialStatementTransService.GetFinancialStatementTransRefId(refGUID, reftype));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/FinancialStatementTrans/Function/ImportFinancialStatementTrans/ImportFinancialStatement")]
        public ActionResult ImportFinancialStatement([FromBody] ImportFinancialStatementView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                    return Ok(financialStatementTransService.ImportFinancialStatement(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
