using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class DepartmentController : BaseControllerV2
	{

		public DepartmentController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetDepartmentList/ByCompany")]
		public ActionResult GetDepartmentListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetDepartmentListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetDepartmentById/id={id}")]
		public ActionResult GetDepartmentById(string id)
		{
			try
			{
				IDepartmentService departmentService = new DepartmentService(db);
				return Ok(departmentService.GetDepartmentById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateDepartment")]
		public ActionResult CreateDepartment([FromBody] DepartmentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDepartmentService departmentService = new DepartmentService(db, SysTransactionLogService);
					return Ok(departmentService.CreateDepartment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateDepartment")]
		public ActionResult UpdateDepartment([FromBody] DepartmentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDepartmentService departmentService = new DepartmentService(db, SysTransactionLogService);
					return Ok(departmentService.UpdateDepartment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteDepartment")]
		public ActionResult DeleteDepartment([FromBody] RowIdentity parm)
		{
			try
			{
				IDepartmentService departmentService = new DepartmentService(db, SysTransactionLogService);
				return Ok(departmentService.DeleteDepartment(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
