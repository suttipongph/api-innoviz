using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class BookmarkDocumentTemplateTableController : BaseControllerV2
	{

		public BookmarkDocumentTemplateTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		#region BookmarkDocumentTemplateLine
		[HttpPost]
		[Route("GetBookmarkDocumentTemplateLineList/ByCompany")]
		public ActionResult GetBookmarkDocumentTemplateLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBookmarkDocumentTemplateLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBookmarkDocumentTemplateLineById/id={id}")]
		public ActionResult GetBookmarkDocumentTemplateLineById(string id)
		{
			try
			{
				IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
				return Ok(bookmarkDocumentTemplateTableService.GetBookmarkDocumentTemplateLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBookmarkDocumentTemplateLine")]
		public ActionResult CreateBookmarkDocumentTemplateLine([FromBody] BookmarkDocumentTemplateLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db, SysTransactionLogService);
					return Ok(bookmarkDocumentTemplateTableService.CreateBookmarkDocumentTemplateLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBookmarkDocumentTemplateLine")]
		public ActionResult UpdateBookmarkDocumentTemplateLine([FromBody] BookmarkDocumentTemplateLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db, SysTransactionLogService);
					return Ok(bookmarkDocumentTemplateTableService.UpdateBookmarkDocumentTemplateLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBookmarkDocumentTemplateLine")]
		public ActionResult DeleteBookmarkDocumentTemplateLine([FromBody] RowIdentity parm)
		{
			try
			{
				IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db, SysTransactionLogService);
				return Ok(bookmarkDocumentTemplateTableService.DeleteBookmarkDocumentTemplateLine(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region BookmarkDocumentTemplateTable
		[HttpPost]
		[Route("GetBookmarkDocumentTemplateTableList/ByCompany")]
		public ActionResult GetBookmarkDocumentTemplateTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBookmarkDocumentTemplateTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBookmarkDocumentTemplateTableById/id={id}")]
		public ActionResult GetBookmarkDocumentTemplateTableById(string id)
		{
			try
			{
				IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
				return Ok(bookmarkDocumentTemplateTableService.GetBookmarkDocumentTemplateTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBookmarkDocumentTemplateTable")]
		public ActionResult CreateBookmarkDocumentTemplateTable([FromBody] BookmarkDocumentTemplateTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db, SysTransactionLogService);
					return Ok(bookmarkDocumentTemplateTableService.CreateBookmarkDocumentTemplateTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBookmarkDocumentTemplateTable")]
		public ActionResult UpdateBookmarkDocumentTemplateTable([FromBody] BookmarkDocumentTemplateTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db, SysTransactionLogService);
					return Ok(bookmarkDocumentTemplateTableService.UpdateBookmarkDocumentTemplateTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBookmarkDocumentTemplateTable")]
		public ActionResult DeleteBookmarkDocumentTemplateTable([FromBody] RowIdentity parm)
		{
			try
			{
				IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db, SysTransactionLogService);
				return Ok(bookmarkDocumentTemplateTableService.DeleteBookmarkDocumentTemplateTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion
        #region Dropdown

        [HttpPost]
		[Route("GetBookmarkDocumentDropDown")]
		public ActionResult GetBookmarkDocumentDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBookmarkDocument(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetBookmarkDocumentTemplateTableDropDown")]
		public ActionResult GetBookmarkDocumentTemplateTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBookmarkDocumentTemplateTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetDocumentTemplateTableByDocumentTypeDropDown")]
		public ActionResult GetDocumentTemplateTableByDocumentTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
				return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetDocumentTemplateTableDropDown")]
		public ActionResult GetDocumentTemplateTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
		[HttpGet]
		[Route("ValidateIsManualNumberSeq")]
		public ActionResult ValidateIsManualNumberSeq()
		{
			try
			{
				IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
				return Ok(bookmarkDocumentTemplateTableService.IsManualByDocumentConditionTemplateTable());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetInitialData/id={bookmarkDocumentTemplateTableGUID}")]
		public ActionResult GetBookmarkDocumentTemplateTableInitialData(string bookmarkDocumentTemplateTableGUID)
		{
			try
			{
				IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
				return Ok(bookmarkDocumentTemplateTableService.GetBookmarkDocumentTemplateTableInitialData(bookmarkDocumentTemplateTableGUID));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
