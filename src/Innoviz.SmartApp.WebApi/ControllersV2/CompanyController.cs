﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModels;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class CompanyController : BaseControllerV2
	{

		public CompanyController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetCompanyList/ByCompany")]
		public ActionResult GetCompanyListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetCompanyListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetCompanyById/id={id}")]
		public ActionResult GetCompanyById(string id)
		{
			try
			{
				ICompanyService companyService = new CompanyService(db);
				return Ok(companyService.GetCompanyById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateCompany")]
		public ActionResult CreateCompany([FromBody] CreateCompanyParamView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICompanyService companyService = new CompanyService(db, SysTransactionLogService);
					return Ok(companyService.CreateCompany(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateCompany")]
		public ActionResult UpdateCompany([FromBody] CompanyItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICompanyService companyService = new CompanyService(db, SysTransactionLogService);
					return Ok(companyService.UpdateCompany(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteCompany")]
		public ActionResult DeleteCompany([FromBody] RowIdentity parm)
		{
			try
			{
				ICompanyService companyService = new CompanyService(db, SysTransactionLogService);
				return Ok(companyService.DeleteCompany(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown
		[HttpPost]
		[Route("GetBranchDropDown")]
		public ActionResult GetBranchDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBranch(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion
        #region CompanyBank
        [HttpPost]
        [Route("RelatedInfo/CompanyBank/GetCompanyBankList/ByCompany")]
        public ActionResult GetCompanyBankListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCompanyBankListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/CompanyBank/GetCompanyBankById/id={id}")]
        public ActionResult GetCompanyBankById(string id)
        {
            try
            {
                ICompanyBankService companyBankService = new CompanyBankService(db);
                return Ok(companyBankService.GetCompanyBankById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CompanyBank/CreateCompanyBank")]
        public ActionResult CreateCompanyBank([FromBody] CompanyBankItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICompanyBankService companyBankService = new CompanyBankService(db, SysTransactionLogService);
                    return Ok(companyBankService.CreateCompanyBank(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CompanyBank/UpdateCompanyBank")]
        public ActionResult UpdateCompanyBank([FromBody] CompanyBankItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICompanyBankService companyBankService = new CompanyBankService(db, SysTransactionLogService);
                    return Ok(companyBankService.UpdateCompanyBank(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CompanyBank/DeleteCompanyBank")]
        public ActionResult DeleteCompanyBank([FromBody] RowIdentity parm)
        {
            try
            {
                ICompanyBankService companyBankService = new CompanyBankService(db, SysTransactionLogService);
                return Ok(companyBankService.DeleteCompanyBank(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("RelatedInfo/CompanyBank/GetBankGroupDropDown")]
        public ActionResult GetBankGroupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/CompanyBank/GetBankTypeDropDown")]
        public ActionResult GetBankTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/CompanyBank/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region CompanyBranch
        [HttpPost]
        [Route("RelatedInfo/branch/GetBranchList/ByCompany")]
        public ActionResult GetCompanyBranchListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBranchListvw(search));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/branch/GetBranchById/id={id}")]
        public ActionResult GetCompanyBranchById(string id)
        {
            try
            {
                IBranchService branchService = new BranchService(db);
                return Ok(branchService.GetBranchById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/GetTaxBranchDropDown")]
        public ActionResult GetRefInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBranchService branchService = new BranchService(db);
                return Ok(branchService.GetTaxBranchDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/CreateCompanyBranch")]
        public ActionResult CreateCompanyBranch([FromBody] BranchItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBranchService branchService = new BranchService(db, SysTransactionLogService);
                    return Ok(branchService.CreateBranch(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/UpdateBranch")]
        public ActionResult UpdateCompanyBranch([FromBody] BranchItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBranchService branchService = new BranchService(db, SysTransactionLogService);
                    return Ok(branchService.UpdateBranch(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region AddressTrans
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/AddressTrans/GetAddressTransList/ByCompany")]
        public ActionResult GetAddressTransListByCompany([FromBody] SearchParameter search)
        {
             try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);

                return Ok(sysListViewService.GetAddressTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region ContactTrans
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/ContactTrans/GetContactTransList/ByCompany")]
        public ActionResult GetContactListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetContactTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/branch/RelatedInfo/AddressTrans/GetAddressTransById/id={id}")]
        public ActionResult GetAddressTransById(string id)
        {
            try
            {
                IAddressTransService AddressTransService = new AddressTransService(db);
                return Ok(AddressTransService.GetAddressTransById(id));
                  }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/branch/RelatedInfo/ContactTrans/GetContactTransById/id={id}")]
        public ActionResult GetContactById(string id)
        {
            try
            {
                IContactTransService ContactTransService = new ContactTransService(db);
                return Ok(ContactTransService.GetContactTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/AddressTrans/CreateAddressTrans")]
        public ActionResult CreateAddressTrans([FromBody] AddressTransItemView vwModel){
            try
            {
                if (ModelState.IsValid)
                {
                    IAddressTransService AddressTransService = new AddressTransService(db, SysTransactionLogService);
                    return Ok(AddressTransService.CreateAddressTrans(vwModel));
                    }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/ContactTrans/CreateContactTrans")]
        public ActionResult CreateContact([FromBody] ContactTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
                    return Ok(ContactTransService.CreateContactTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/AddressTrans/UpdateAddressTrans")]
        public ActionResult UpdateAddressTrans([FromBody] AddressTransItemView vwModel)
         {
            try
            {
                if (ModelState.IsValid)
                {
                    IAddressTransService AddressTransService = new AddressTransService(db, SysTransactionLogService);
                    return Ok(AddressTransService.UpdateAddressTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/ContactTrans/UpdateContactTrans")]
        public ActionResult UpdateContact([FromBody] ContactTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
                    return Ok(ContactTransService.UpdateContactTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/AddressTrans/DeleteAddressTrans")]
        public ActionResult DeleteAddressTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IAddressTransService AddressTransService = new AddressTransService(db, SysTransactionLogService);
                return Ok(AddressTransService.DeleteAddressTrans(parm.Guid));
                 }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/ContactTrans/DeleteContactTrans")]
        public ActionResult DeleteContact([FromBody] RowIdentity parm)
        {
            try
            {
                IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
                return Ok(ContactTransService.DeleteContactTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/branch/RelatedInfo/AddressTrans/GetAddressTransInitialData/id={id}")]
        public ActionResult GetAddressTransInitialData(string id)
        {
            try
            {
                IBranchService branchService = new BranchService(db);
                return Ok(branchService.GetAddressTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/branch/RelatedInfo/ContactTrans/GetContactTransInitialData/id={id}")]
        public ActionResult GetContactTransInitialData(string id)
        {
            try
            {
                IBranchService branchService = new BranchService(db);
                return Ok(branchService.GetContactTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/AddressTrans/GetAddressCountryDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressCountryDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAddressCountry(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/AddressTrans/GetAddressDistrictDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressDistrictDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressDistrict(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/AddressTrans/GetAddressPostalCodeDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressPostalCodeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressPostalCode(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/AddressTrans/GetAddressProvinceDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressProvinceDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressProvince(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/AddressTrans/GetAddressSubDistrictDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressSubDistrictDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressSubDistrict(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/AddressTrans/GetOwnershipDropDown")]
        public ActionResult GetRelatedInfoAddressTransOwnershipDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemOwnership(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/branch/RelatedInfo/AddressTrans/GetPropertyTypeDropDown")]
        public ActionResult GetRelatedInfoAddressTransPropertyTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemPropertyType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion AddressTrans
        #endregion Contact
    }
}
