using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class BusinessUnitController : BaseControllerV2
	{

		public BusinessUnitController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetBusinessUnitList/ByCompany")]
		public ActionResult GetBusinessUnitListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBusinessUnitListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBusinessUnitById/id={id}")]
		public ActionResult GetBusinessUnitById(string id)
		{
			try
			{
				IBusinessUnitService businessUnitService = new BusinessUnitService(db);
				return Ok(businessUnitService.GetBusinessUnitById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBusinessUnit")]
		public ActionResult CreateBusinessUnit([FromBody] BusinessUnitItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessUnitService businessUnitService = new BusinessUnitService(db, SysTransactionLogService);
					return Ok(businessUnitService.CreateBusinessUnit(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBusinessUnit")]
		public ActionResult UpdateBusinessUnit([FromBody] BusinessUnitItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessUnitService businessUnitService = new BusinessUnitService(db, SysTransactionLogService);
					return Ok(businessUnitService.UpdateBusinessUnit(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBusinessUnit")]
		public ActionResult DeleteBusinessUnit([FromBody] RowIdentity parm)
		{
			try
			{
				IBusinessUnitService businessUnitService = new BusinessUnitService(db, SysTransactionLogService);
				return Ok(businessUnitService.DeleteBusinessUnit(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown
		[HttpPost]
		[Route("GetBusinessUnitDropDown")]
		public ActionResult GetBusinessUnitDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBusinessUnit(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetBusinessUnitFilterExcludeBusinessUnitDropDown")]
		public ActionResult GetBusinessUnitFilterExcludeBusinessUnitDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IBusinessUnitService businessUnitService = new BusinessUnitService(db);
				return Ok(businessUnitService.GetBusinessUnitFilterExcludeBusinessUnitDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
