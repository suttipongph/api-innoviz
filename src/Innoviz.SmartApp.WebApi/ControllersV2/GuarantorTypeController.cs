using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class GuarantorTypeController : BaseControllerV2
	{

		public GuarantorTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetGuarantorTypeList/ByCompany")]
		public ActionResult GetGuarantorTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetGuarantorTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetGuarantorTypeById/id={id}")]
		public ActionResult GetGuarantorTypeById(string id)
		{
			try
			{
				IGuarantorTypeService guarantorTypeService = new GuarantorTypeService(db);
				return Ok(guarantorTypeService.GetGuarantorTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateGuarantorType")]
		public ActionResult CreateGuarantorType([FromBody] GuarantorTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IGuarantorTypeService guarantorTypeService = new GuarantorTypeService(db, SysTransactionLogService);
					return Ok(guarantorTypeService.CreateGuarantorType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateGuarantorType")]
		public ActionResult UpdateGuarantorType([FromBody] GuarantorTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IGuarantorTypeService guarantorTypeService = new GuarantorTypeService(db, SysTransactionLogService);
					return Ok(guarantorTypeService.UpdateGuarantorType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteGuarantorType")]
		public ActionResult DeleteGuarantorType([FromBody] RowIdentity parm)
		{
			try
			{
				IGuarantorTypeService guarantorTypeService = new GuarantorTypeService(db, SysTransactionLogService);
				return Ok(guarantorTypeService.DeleteGuarantorType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
