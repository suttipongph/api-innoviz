﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public partial class BusinessCollateralAgmTableController : BaseControllerV2
    {

        public BusinessCollateralAgmTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        public ActionResult GetBusinessCollateralAgmLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBusinessCollateralAgmLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        public ActionResult GetBusinessCollateralAgmLineById(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        public ActionResult CreateBusinessCollateralAgmLine([FromBody] BusinessCollateralAgmLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.CreateBusinessCollateralAgmLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        public ActionResult UpdateBusinessCollateralAgmLine([FromBody] BusinessCollateralAgmLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.UpdateBusinessCollateralAgmLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        public ActionResult DeleteBusinessCollateralAgmLine([FromBody] RowIdentity parm)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.DeleteBusinessCollateralAgmLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        public ActionResult GetBusinessCollateralAgmLineInitialData(string businessCollateralAgmId)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmLineInitialData(businessCollateralAgmId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        // Memo RelatedInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        public ActionResult GetBusinessCollateralAgmTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBusinessCollateralAgmTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        // Memo RelatedInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        public ActionResult GetBusinessCollateralAgmTableById(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        public ActionResult CreateBusinessCollateralAgmTable([FromBody] BusinessCollateralAgmTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.CreateBusinessCollateralAgmTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        public ActionResult UpdateBusinessCollateralAgmTable([FromBody] BusinessCollateralAgmTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.UpdateBusinessCollateralAgmTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        public ActionResult DeleteBusinessCollateralAgmTable([FromBody] RowIdentity parm)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.DeleteBusinessCollateralAgmTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        public ActionResult GetBusinessCollateralAgmTableInitialData(int productType)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmTableInitialData(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Bond/AllBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Bond/NewBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        public ActionResult ValidateIsManualNumberSeqInternalBusinessCollateralAgm(string companyId)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.IsManualByInternalBusinessCollateralAgreement(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Bond/AllBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Bond/NewBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        public ActionResult ValidateIsManualNumberSeqBusinessCollateralAgm(string companyId)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.IsManualByBusinessCollateralAgreement(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Memo
        [HttpPost]

        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult GetMemoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult DeleteMemo([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetMemoById(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialData(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeMemoTransByBusinessCollateralAgmTable(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Memo

        #region Dropdown

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetBankGroupDropDown")]
        public ActionResult GetBankGroupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetBankTypeDropDown")]
        public ActionResult GetBankTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        public ActionResult GetBusinessCollateralSubTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralSubType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        public ActionResult GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db);
                return Ok(businessCollateralTypeService.GetDropDownItemBusinessCollateralSubTypeByBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        public ActionResult GetBusinessCollateralTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        // AddInvoiceTable RelatedInfo
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        public ActionResult GetCreditAppReqBusinessCollateralDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppReqBusinessCollateral(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        public ActionResult GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetDropDownItemCreditAppRequestTableByBusinessCollateralAgmTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        public ActionResult GetDropDownItemCreditAppRequestTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        // AddeInvoiceTable RelatedInfo
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
				return Ok(businessCollateralAgmTableService.GetDropDownItemBusinessCollateralAgmStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}


        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        public ActionResult GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allbusinesscollateralagmtable/GetBusinessCollateralStatusDropDown")]
        [Route("Bond/newbusinesscollateralagmtable/GetBusinessCollateralStatusDropDown")]
        [Route("Factoring/allbusinesscollateralagmtable/GetBusinessCollateralStatusDropDown")]
        [Route("Factoring/newbusinesscollateralagmtable/GetBusinessCollateralStatusDropDown")]
        [Route("Leasing/allbusinesscollateralagmtable/GetBusinessCollateralStatusDropDown")]
        [Route("Leasing/newbusinesscollateralagmtable/GetBusinessCollateralStatusDropDown")]
        [Route("LCDLC/allbusinesscollateralagmtable/GetBusinessCollateralStatusDropDown")]
        [Route("LCDLC/newbusinesscollateralagmtable/GetBusinessCollateralStatusDropDown")]
        [Route("HirePurchase/allbusinesscollateralagmtable/GetBusinessCollateralStatusDropDown")]
        [Route("HirePurchase/newbusinesscollateralagmtable/GetBusinessCollateralStatusDropDown")]
        [Route("ProjectFinance/allbusinesscollateralagmtable/GetBusinessCollateralStatusDropDown")]
        [Route("ProjectFinance/newbusinesscollateralagmtable/GetBusinessCollateralStatusDropDown")]
        public ActionResult GetBusinessCollateralStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
		[Route("Factoring/AllBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
		[Route("Factoring/NewBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
		[Route("Bond/AllBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
		[Route("Bond/NewBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
		[Route("Leasing/AllBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
		[Route("Leasing/NewBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeByBusinessCollateralAgmTable(string id)
		{
			try
			{
				IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
				return Ok(businessCollateralAgmTableService.GetAccessModeByBusinessCollateralAgmTable(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        //-----BookmarkDocumentTrans
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeBookmarkDocumentTransByBusinessCollateralAgmTable(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByBusinessCollateralAgmTableForBookmarkDocumentTrans(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Bond/allbusinesscollateralagmtable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Bond/newbusinesscollateralagmtable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Factoring/allbusinesscollateralagmtable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Factoring/newbusinesscollateralagmtable/GetBusinessCollateralAgmLineDropDown")]
        [Route("HirePurchase/allbusinesscollateralagmtable/GetBusinessCollateralAgmLineDropDown")]
        [Route("HirePurchase/newbusinesscollateralagmtable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Leasing/allbusinesscollateralagmtable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Leasing/newbusinesscollateralagmtable/GetBusinessCollateralAgmLineDropDown")]
        [Route("LCDLC/allbusinesscollateralagmtable/GetBusinessCollateralAgmLineDropDown")]
        [Route("LCDLC/newbusinesscollateralagmtable/GetBusinessCollateralAgmLineDropDown")]
        [Route("ProjectFinance/allbusinesscollateralagmtable/GetBusinessCollateralAgmLineDropDown")]
        [Route("ProjectFinance/newbusinesscollateralagmtable/GetBusinessCollateralAgmLineDropDown")]
        public ActionResult GetBusinessCollateralAgmLineDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralAgmLine(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
       
        [Route("Bond/allbusinesscollateralagmtable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Bond/newbusinesscollateralagmtable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Factoring/allbusinesscollateralagmtable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Factoring/newbusinesscollateralagmtable/GetBusinessCollateralAgmTableDropDown")]
        [Route("HirePurchase/allbusinesscollateralagmtable/GetBusinessCollateralAgmTableDropDown")]
        [Route("HirePurchase/newbusinesscollateralagmtable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Leasing/allbusinesscollateralagmtable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Leasing/newbusinesscollateralagmtable/GetBusinessCollateralAgmTableDropDown")]
        [Route("LCDLC/allbusinesscollateralagmtable/GetBusinessCollateralAgmTableDropDown")]
        [Route("LCDLC/newbusinesscollateralagmtable/GetBusinessCollateralAgmTableDropDown")]
        [Route("ProjectFinance/allbusinesscollateralagmtable/GetBusinessCollateralAgmTableDropDown")]
        [Route("ProjectFinance/newbusinesscollateralagmtable/GetBusinessCollateralAgmTableDropDown")]
        public ActionResult GetBusinessCollateralAgmTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region Function
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        public ActionResult GetVisibleManageMenu(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetVisibleManageMenu(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        public ActionResult GetVisibleGenerateMenu(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetVisibleGenerateMenu(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Manage agreement
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        public ActionResult ManageAgreement([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.ManageAgreement(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        public ActionResult GetManageAgreementValidation([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        public ActionResult GetManageAgreementByRefType([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementByRefType(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByManageAgreement([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #endregion

        #region AgreementTableInfo
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        public ActionResult GetAgreementTableInfoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAgreementTableInfoListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        public ActionResult GetAgreementTableInfoById(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        public ActionResult CreateAgreementTableInfo([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.CreateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        public ActionResult UpdateAgreementTableInfo([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.UpdateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        public ActionResult DeleteAgreementTableInfo([FromBody] RowIdentity parm)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                return Ok(AgreementTableInfoService.DeleteAgreementTableInfo(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Hire/AllBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Hire/NewBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAgreementTableInfoByBusinessCollateralAgmTable(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoByBusinessCollateralAgm(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        public ActionResult GetAgreementTableInfoInitialData(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetAgreementTableInfoInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        // AgreementInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeAgreementTableInfoByBusinessCollateralAgmTable(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
		public ActionResult GetAgreementTableInfoAddressTransDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IAddressTransService addressTransService = new AddressTransService(db);
				return Ok(addressTransService.GetDropDownItemAddressTransByCustomer(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTransByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(authorizedPersonTransService.GetDropDownItemAuthorizedPersonTransByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTransByCreditAppDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(authorizedPersonTransService.GetDropDownItemAuthorizedPersonTransByCreditApp(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown/")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
		public ActionResult GetAgreementTableInfoCompanyBankDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCompanyBank(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
		public ActionResult GetAgreementTableInfoCompanySignatureDropDown([FromBody] SearchParameter search)
		{
			try
			{
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetEmployeeTableWithCompanySignatureDropDown(search));
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
		public ActionResult GetAgreementTableInfoEmployeeTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion AgreementTableInfo

		#region BookmarkDocument
		[HttpPost]
		[Route("Bond/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
		[Route("Bond/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
		[Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
		[Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
		[Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
		[Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
		[Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
		[Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
		[Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
		[Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
		[Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
		[Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
		public ActionResult GetBookmarkDocumentTransListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBookmarkDocumentTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("Bond/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
		[Route("Bond/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
		[Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
		[Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
		[Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
		[Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
		[Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
		[Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
		[Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
		[Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
		[Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
		[Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        public ActionResult GetBookmarkDocumentTransById(string id)
		{
			try
			{
				IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db);
				return Ok(BookmarkDocumentTransService.GetBookmarkDocumentTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Bond/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
		[Route("Bond/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
		[Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
		[Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
		[Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
		[Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
		[Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
		[Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
		[Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
		[Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
		[Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
		[Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
		public ActionResult CreateBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
					return Ok(BookmarkDocumentTransService.CreateBookmarkDocumentTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Bond/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
		[Route("Bond/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
		[Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
		[Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
		[Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
		[Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
		[Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
		[Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
		[Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
		[Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
		[Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
		[Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
		public ActionResult UpdateBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
					return Ok(BookmarkDocumentTransService.UpdateBookmarkDocumentTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Bond/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
		[Route("Bond/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
		[Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
		[Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
		[Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
		[Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
		[Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
		[Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
		[Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
		[Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
		[Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
		[Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
		public ActionResult DeleteBookmarkDocumentTrans([FromBody] RowIdentity parm)
		{
			try
			{
				IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
				return Ok(BookmarkDocumentTransService.DeleteBookmarkDocumentTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("Bond/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
		[Route("Bond/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
		[Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
		[Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
		[Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
		[Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
		[Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
		[Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
		[Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
		[Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
		[Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
		[Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
		public ActionResult GetBookmarkDocumentTransInitialData(string id)
		{
			try
			{
				IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
				return Ok(businessCollateralAgmTableService.GetBookmarkDocumentTransInitialDataByGuarantorAgreement(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
		public ActionResult GetBookmarkDocumentTransInitialDataGuarantorAgreement(string id)
		{
			try
			{
				IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
				return Ok(businessCollateralAgmTableService.GetBookmarkDocumentTransInitialDataByGuarantorAgreement(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        public ActionResult GetBookmarkDocumentDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
				return Ok(bookmarkDocumentService.GetDropDownItemBookmarkDocumentByBusinessAGM(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetDocumentTemplateTableByDocumentTypeDropDown")]
		[Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
		public ActionResult GetDocumentTemplateTableByDocumentTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
				return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        public ActionResult GetBookmarkDocumentTransDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetDropDownItemBookMarkDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        public ActionResult GetDocumentTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region PrintBookDocTrans
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        public ActionResult GetPrintBookDocTransValidation([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintBookDocTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransaction([FromBody] PrintBookDocTransParm printBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion BookmarkDocument        
        #region ServiceFeeTrans
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        public ActionResult GetServiceFeeTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeTransListvw(search));
                }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BookmarkDocument
        #region ConsortiumTrans
        [HttpPost]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]

        public ActionResult GetConsortiumTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetConsortiumTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        public ActionResult GetServiceFeeTransById(string id)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetServiceFeeTransById(id));
                }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeServiceFeeTransByBusinessCollateralAgmTable(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        public ActionResult GetConsortiumTransTransById(string id)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db);
                return Ok(ConsortiumTransService.GetConsortiumTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        public ActionResult CreateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.CreateServiceFeeTrans(vwModel));
                    }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        public ActionResult CreateConsortiumTrans([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.CreateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        public ActionResult UpdateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.UpdateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        public ActionResult UpdateConsortiumTrans([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.UpdateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        public ActionResult DeleteServiceFeeTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                return Ok(ServiceFeeTransService.DeleteServiceFeeTrans(parm.Guid));
                }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        public ActionResult DeleteConsortiumTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                return Ok(ConsortiumTransService.DeleteConsortiumTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeConsortiumTransByBusinessCollateralAgmTable(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        public ActionResult GetServiceFeeTransInitialData(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetServiceFeeTransInitialDataByBusinessCollateralAgmTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]

        public ActionResult GetCalculateField([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetCalculateField(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        public ActionResult ValidateServiceFeeTrans([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.ValidateServiceFeeTrans(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        public ActionResult GetTaxValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetTaxValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        public ActionResult GetWHTValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetWHTValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]

        public ActionResult GetInvoiceRevenueTypeByProductTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeByProductTypeAndDefaultNone(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        //Add InvoiceTable RelelatedInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]

        public ActionResult GetLedgerDimensionDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        //Add InvoiceTable RelatedInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
                 }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        public ActionResult GetConsortiumTransInitialData(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db , SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetConsortuimTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        //Add InvoiceTable ReleatedInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]

        public ActionResult GetWithholdingTaxTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByServiceFeeTrans([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        public ActionResult GetOriginalInvoiceByServiceFeeTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetOriginalInvoiceByServiceFeeTransDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ServiceFeeTrans
        [HttpPost]
        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        public ActionResult GetConsortiumTransConsortiumLineDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IConsortiumTableService consortiumTableService = new ConsortiumTableService(db);
                return Ok(consortiumTableService.GetDropDownItemConsortiumLineByConsortiumTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Consortium
        #region Attachment
        [HttpPost]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Lcdlc/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Projectfinance/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Lcdlc/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Projectfinance/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Projectfinance/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentById(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Lcdlc/allbusinesscollateralagmtable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Lcdlc/newbusinesscollateralagmtable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Projectfinance/allbusinesscollateralagmtable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Projectfinance/newbusinesscollateralagmtable/RelatedInfo/Attachment/CreateAttachment")]
        public ActionResult CreateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Lcdlc/allbusinesscollateralagmtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/newbusinesscollateralagmtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Projectfinance/allbusinesscollateralagmtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Projectfinance/newbusinesscollateralagmtable/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Lcdlc/allbusinesscollateralagmtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/newbusinesscollateralagmtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Projectfinance/allbusinesscollateralagmtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Projectfinance/newbusinesscollateralagmtable/RelatedInfo/Attachment/DeleteAttachment")]

        public ActionResult DeleteAttachment([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Lcdlc/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Projectfinance/allbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Projectfinance/newbusinesscollateralagmtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefId([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region InvoiceTable
        [HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		public ActionResult GetInvoiceLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		public ActionResult GetInvoiceLineById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetInvoiceLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		public ActionResult CreateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.CreateInvoiceLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		public ActionResult UpdateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.UpdateInvoiceLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		public ActionResult DeleteInvoiceLine([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
				return Ok(invoiceTableService.DeleteInvoiceLine(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		public ActionResult GetInvoiceTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		public ActionResult GetInvoiceTableById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetInvoiceTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		public ActionResult CreateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.CreateInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		public ActionResult UpdateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.UpdateInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		public ActionResult DeleteInvoiceTable([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
				return Ok(invoiceTableService.DeleteInvoiceTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion InvoiceTable
		#region DropdownInvoiceTable
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		public ActionResult GetInvoiceTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		public ActionResult GetDocumentStatusByInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		public ActionResult GetBuyerInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
		public ActionResult GetMethodOfPaymentDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		public ActionResult GetCreditAppTableDropDownByInvoiceTable([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
		public ActionResult GetBuyerAgreementTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
		public ActionResult GetDocumentReasonDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCurrency(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		public ActionResult GetInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		public ActionResult GetProdUnitTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		public ActionResult GetTaxInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		[Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		[Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		[Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		[Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		[Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		[Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		[Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		[Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		[Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		[Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		[Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		public ActionResult GetRefInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetRefInvoiceTableDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
        }
        #endregion DropdownInvoiceTable
        #region jointventure
        [HttpPost]

        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        public ActionResult GetJointVentureTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetJointVentureTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        public ActionResult JointVentureTransById(string id)
        {
            try
            {
                IJointVentureTransService JointVentureTransService = new JointVentureTransService(db);
                return Ok(JointVentureTransService.GetJointVentureTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetJointVentureTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        public ActionResult CreateJointVentureTrans([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.CreateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        public ActionResult UpdateJointVentureTrans([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.UpdateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        public ActionResult Deletejointventuretrans([FromBody] RowIdentity parm)
        {
            try
            {
                IJointVentureTransService jointventuretransService = new JointVentureTransService(db, SysTransactionLogService);
                return Ok(jointventuretransService.DeleteJointVentureTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        public ActionResult GetJointVentureTransInitialData(string Id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableleService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableleService.GetJointVentureTransInitialDataByBusinessCollateralAgmTable(Id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeJointVentureTransByBusinessCollateralAgmTable(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion jointventure
        #region Function CopyActiveBusinessCollateralAgreementLine
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        public ActionResult UpdateCopyBusinessCollateralAgmLine([FromBody] CopyBusinessCollateralAgmLine vwModel)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.UpdateCopyBusinessCollateralAgmLine(vwModel));

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Function CopyActiveBusinessCollateralAgreementLine
        #region PrintSetBookDocTrans
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransaction([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByRefGUID(printSetBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region copybookmarkdocumentfromtemplate

        [HttpPost]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("Lcdlc/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Lcdlc/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("Projectfinance/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Projectfinance/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        public ActionResult GetBookmarkDocumentTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetDropDownItemBookmarkDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("Lcdlc/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Lcdlc/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("Projectfinance/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Projectfinance/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        public ActionResult CopyBookmarkDocumentFromTemplate([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.CopyBookmarkDocumentFromTemplate(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        
        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("Lcdlc/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Lcdlc/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("Projectfinance/allbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Projectfinance/newbusinesscollateralagmtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        public ActionResult GetCopyBookmarkDocumentValidation([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.GetCopyBookmarkDocumentValidation(param));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion

    }
}
