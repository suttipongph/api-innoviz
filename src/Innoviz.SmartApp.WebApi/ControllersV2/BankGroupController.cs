﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;


namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class BankGroupController : BaseControllerV2
    {
		public BankGroupController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetBankGroupList/ByCompany")]
		public ActionResult GetBankGroupListByCompany([FromBody] SearchParameter search)
	{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBankGroupListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBankGroupById/id={id}")]
		public ActionResult GetBankGroupById(string id)
		{
			try
			{
				IBankGroupService bankGroupService = new BankGroupService(db);
				return Ok(bankGroupService.GetBankGroupById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBankGroup")]
		public ActionResult CreateBankType([FromBody] BankGroupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBankGroupService bankGroupService = new BankGroupService(db, SysTransactionLogService);
					return Ok(bankGroupService.CreateBankGroup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBankGroup")]
		public ActionResult UpdateBankType([FromBody] BankGroupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBankGroupService bankGroupService = new BankGroupService(db, SysTransactionLogService);
					return Ok(bankGroupService.UpdateBankGroup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBankGroup")]
		public ActionResult DeleteBankType([FromBody] RowIdentity parm)
		{
			try
			{
				IBankGroupService bankGroupService = new BankGroupService(db, SysTransactionLogService);
				return Ok(bankGroupService.DeleteBankGroup(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}

