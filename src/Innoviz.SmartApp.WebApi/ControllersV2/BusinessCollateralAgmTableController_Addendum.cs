﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public partial class BusinessCollateralAgmTableController : BaseControllerV2
    {
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        public ActionResult GetBusinessCollateralAgmLineListByCompanyByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBusinessCollateralAgmLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        public ActionResult GetBusinessCollateralAgmLineByIdByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        public ActionResult CreateBusinessCollateralAgmLineByAddendum([FromBody] BusinessCollateralAgmLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.CreateBusinessCollateralAgmLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        public ActionResult UpdateBusinessCollateralAgmLineByAddendum([FromBody] BusinessCollateralAgmLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.UpdateBusinessCollateralAgmLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        public ActionResult DeleteBusinessCollateralAgmLineByAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.DeleteBusinessCollateralAgmLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        public ActionResult GetBusinessCollateralAgmLineInitialDataByAddendum(string businessCollateralAgmId)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmLineInitialData(businessCollateralAgmId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        public ActionResult GetBusinessCollateralAgmTableListByCompanyByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBusinessCollateralAgmTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        public ActionResult GetBusinessCollateralAgmTableByIdByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        public ActionResult CreateBusinessCollateralAgmTableByAddendum([FromBody] BusinessCollateralAgmTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.CreateBusinessCollateralAgmTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        public ActionResult UpdateBusinessCollateralAgmTableByAddendum([FromBody] BusinessCollateralAgmTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.UpdateBusinessCollateralAgmTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        public ActionResult DeleteBusinessCollateralAgmTableByAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.DeleteBusinessCollateralAgmTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        public ActionResult GetBusinessCollateralAgmTableInitialDataByAddendum(int productType)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmTableInitialData(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        // AddendumBusinessCollateralAgmTable Function
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/ValidateIsManualNumberSeq/companyId={companyId}")]
        public ActionResult ValidateIsManualNumberSeqInternalBusinessCollateralAgmByAddendum(string companyId)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.IsManualByInternalBusinessCollateralAgreement(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        public ActionResult ValidateIsManualNumberSeqBusinessCollateralAgmByAddendum(string companyId)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.IsManualByBusinessCollateralAgreement(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankGroupDropDown")]
        public ActionResult GetBankGroupDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBankTypeDropDown")]
        public ActionResult GetBankTypeDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        public ActionResult GetBusinessCollateralSubTypeDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralSubType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        public ActionResult GetBusinessCollateralSubTypeByBusinessCollateralTypDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db);
                return Ok(businessCollateralTypeService.GetDropDownItemBusinessCollateralSubTypeByBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        public ActionResult GetBusinessCollateralTypeDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        public ActionResult GetCreditAppReqBusinessCollateralDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppReqBusinessCollateral(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        public ActionResult GetCreditAppRequestTableByBusinessCollateralAgmTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetDropDownItemCreditAppRequestTableByBusinessCollateralAgmTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        // AddendumBusinessCollateralAgmTable Function Gen
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDown")]
        public ActionResult GetDropDownItemCreditAppRequestTableByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetDropDownItemBusinessCollateralAgmStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        public ActionResult GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        public ActionResult GetBusinessCollateralStatusDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeByBusinessCollateralAgmTableByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByBusinessCollateralAgmTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTableByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        public ActionResult GetBusinessCollateralAgmLineDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralAgmLine(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]

        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        public ActionResult GetBusinessCollateralAgmTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #region GenerateBusinessCollateralAddendum
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetGenBusCollateralAgmAddendumById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetGenBusCollateralAgmAddendumById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetGenBusCollateralAgmAddendumById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetGenBusCollateralAgmAddendumById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetGenBusCollateralAgmAddendumById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetGenBusCollateralAgmAddendumById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetGenBusCollateralAgmAddendumById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetGenBusCollateralAgmAddendumById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetGenBusCollateralAgmAddendumById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetGenBusCollateralAgmAddendumById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetGenBusCollateralAgmAddendumById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetGenBusCollateralAgmAddendumById/id={id}")]
        public ActionResult GetGenBusCollateralAgmAddendumByIdByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetGenBusCollateralAgmAddendumById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/addendumbusinesscollateralagmtable/GetGenerateAddendumValidation/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/addendumbusinesscollateralagmtable/GetGenerateAddendumValidation/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/addendumbusinesscollateralagmtable/GetGenerateAddendumValidation/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/addendumbusinesscollateralagmtable/GetGenerateAddendumValidation/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/addendumbusinesscollateralagmtable/GetGenerateAddendumValidation/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/addendumbusinesscollateralagmtable/GetGenerateAddendumValidation/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/addendumbusinesscollateralagmtable/GetGenerateAddendumValidation/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/addendumbusinesscollateralagmtable/GetGenerateAddendumValidation/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/addendumbusinesscollateralagmtable/GetGenerateAddendumValidation/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/addendumbusinesscollateralagmtable/GetGenerateAddendumValidation/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/addendumbusinesscollateralagmtable/GetGenerateAddendumValidation/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/addendumbusinesscollateralagmtable/GetGenerateAddendumValidation/id={id}")]
        public ActionResult GetGenerateAddendumValidationByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetGenerateAddendumValidation(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDownAddendum/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDownAddendum/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDownAddendum/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDownAddendum/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDownAddendum/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDownAddendum/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDownAddendum/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDownAddendum/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDownAddendum/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDownAddendum/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDownAddendum/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestTableDropDownAddendum/id={id}")]
        public ActionResult GetCreditAppRequestTableDropDownAddendumByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetDropDownItemCreditAppRequestTableForBusinessCollateralAGM(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestIDCById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/GetCreditAppRequestIDCById/id={id}")]
        public ActionResult GetCreditAppRequestIDCByIdByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetDatabyCreditAppRequestIDforBusiness(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/CreateGenBusCollateralAgmAddendum")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/CreateGenBusCollateralAgmAddendum")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/CreateGenBusCollateralAgmAddendum")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/CreateGenBusCollateralAgmAddendum")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/CreateGenBusCollateralAgmAddendum")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendumCreateGenBusCollateralAgmAddendum")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/CreateGenBusCollateralAgmAddendum")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/CreateGenBusCollateralAgmAddendum")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/CreateGenBusCollateralAgmAddendum")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/CreateGenBusCollateralAgmAddendum")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/CreateGenBusCollateralAgmAddendum")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/generateaddendum/CreateGenBusCollateralAgmAddendum")]
        public ActionResult CreateGenBusCollateralAgmAddendumByAddendum([FromBody] GenBusCollateralAgmAddendumView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.CreateGenBusCollateralAgmAddendum(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Notice of Cancellation (Function)
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        public ActionResult GetCancelBusinessCollateralAgmByIdByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetGenCancelBusCollateralAgmById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownBusinessCollateralAGMByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        public ActionResult ValidateIsManual_NumberSeqnoticeofcancellationByAddendum(string companyId)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.IsManualByInternalBusinessCollateralAgreement(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        public ActionResult CreateCancelBusinessCollateralAgmByAddendum([FromBody] CancelBusinessCollateralAgmView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.CreateCancelBusinessCollateralAgm(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region RelatedInfo
        #region BookmarkDocument
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        public ActionResult GetBookmarkDocumentTransListByCompanyByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBookmarkDocumentTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        public ActionResult GetBookmarkDocumentTransByIdByAddendum(string id)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(BookmarkDocumentTransService.GetBookmarkDocumentTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        public ActionResult CreateBookmarkDocumentTransByAddendum([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.CreateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        public ActionResult UpdateBookmarkDocumentTransByAddendum([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.UpdateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        public ActionResult DeleteBookmarkDocumentTransByAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                return Ok(BookmarkDocumentTransService.DeleteBookmarkDocumentTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        public ActionResult GetBookmarkDocumentTransInitialDataByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetBookmarkDocumentTransInitialDataByGuarantorAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetBookmarkDocumentTransInitialDataGuarantorAgreementByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetBookmarkDocumentTransInitialDataByGuarantorAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        public ActionResult GetBookmarkDocumentDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
                return Ok(bookmarkDocumentService.GetDropDownItemBookmarkDocumentByBusinessAGM(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        public ActionResult GetDocumentTemplateTableByDocumentTypeDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
                return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        public ActionResult GetBookmarkDocumentTransDocumentStatusDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetDropDownItemBookMarkDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        public ActionResult GetDocumentTemplateTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeBookmarkDocumentTransByBusinessCollateralAgmTableByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByBusinessCollateralAgmTableForBookmarkDocumentTrans(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region PrintBookDocTrans
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        public ActionResult GetPrintBookDocTransValidationByAddendum([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintBookDocTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransactionByAddendum([FromBody] PrintBookDocTransParm printBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region copybookmarkdocumentfromtemplate

        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        public ActionResult GetBookmarkDocumentTemplateTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetDropDownItemBookmarkDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        public ActionResult CopyBookmarkDocumentFromTemplateByAddendum([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.CopyBookmarkDocumentFromTemplate(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        public ActionResult GetCopyBookmarkDocumentValidationByAddendum([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.GetCopyBookmarkDocumentValidation(param));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        #endregion BookmarkDocument        
        #region ServiceFeeTrans
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        public ActionResult GetServiceFeeTransListByCompanyByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        public ActionResult GetServiceFeeTransByIdByAddendum(string id)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetServiceFeeTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeServiceFeeTransByBusinessCollateralAgmTableByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        public ActionResult CreateServiceFeeTransByAddendum([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.CreateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        public ActionResult UpdateServiceFeeTransByAddendum([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.UpdateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        public ActionResult DeleteServiceFeeTransByAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                return Ok(ServiceFeeTransService.DeleteServiceFeeTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        public ActionResult GetServiceFeeTransInitialDataByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetServiceFeeTransInitialDataByBusinessCollateralAgmTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]

        public ActionResult GetCalculateFieldByAddendum([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetCalculateField(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        public ActionResult ValidateServiceFeeTransByAddendum([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.ValidateServiceFeeTrans(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        public ActionResult GetTaxValueByAddendum([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetTaxValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        public ActionResult GetWHTValueByAddendum([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetWHTValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]

        public ActionResult GetInvoiceRevenueTypeByProductTypeDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeByProductTypeAndDefaultNone(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        //Add InvoiceTable RelelatedInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]

        public ActionResult GetLedgerDimensionDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        //Add InvoiceTable RelatedInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        //Add InvoiceTable ReleatedInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]

        public ActionResult GetWithholdingTaxTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByServiceFeeTransByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        public ActionResult GetOriginalInvoiceByServiceFeeTransDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetOriginalInvoiceByServiceFeeTransDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion ServiceFeeTrans
        #region ConsortiumTrans
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]

        public ActionResult GetConsortiumTransListByCompanyByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetConsortiumTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        public ActionResult GetConsortiumTransTransByIdByAddendum(string id)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db);
                return Ok(ConsortiumTransService.GetConsortiumTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        public ActionResult CreateConsortiumTransByAddendum([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.CreateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        public ActionResult UpdateConsortiumTransByAddendum([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.UpdateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        public ActionResult DeleteConsortiumTransByAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                return Ok(ConsortiumTransService.DeleteConsortiumTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        public ActionResult GetConsortiumTransInitialDataByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetConsortuimTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        public ActionResult GetConsortiumTransConsortiumLineDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IConsortiumTableService consortiumTableService = new ConsortiumTableService(db);
                return Ok(consortiumTableService.GetDropDownItemConsortiumLineByConsortiumTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeConsortiumTransByBusinessCollateralAgmTableByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown/")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTypeDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Consortium
        #region Attachment
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompanyByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentByIdByAddendum(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        public ActionResult CreateAttachmentByAddendum([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachmentByAddendum([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]

        public ActionResult DeleteAttachmentByAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefIdByAddendum([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region InvoiceTable
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        public ActionResult GetInvoiceLineListByCompanyByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        public ActionResult GetInvoiceLineByIdByAddendum(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        public ActionResult CreateInvoiceLineByAddendum([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        public ActionResult UpdateInvoiceLineByAddendum([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        public ActionResult DeleteInvoiceLineByAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        public ActionResult GetInvoiceTableListByCompanyByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        public ActionResult GetInvoiceTableByIdByAddendum(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        public ActionResult CreateInvoiceTableByAddendum([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        public ActionResult UpdateInvoiceTableByAddendum([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        public ActionResult DeleteInvoiceTableByAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceTable
        #region DropdownInvoiceTable
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        public ActionResult GetInvoiceTypeDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        public ActionResult GetDocumentStatusByInvoiceTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        public ActionResult GetBuyerInvoiceTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDownByInvoiceTableByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        public ActionResult GetCurrencyDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCurrency(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        public ActionResult GetInvoiceTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        public ActionResult GetProdUnitTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        public ActionResult GetTaxInvoiceTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        public ActionResult GetRefInvoiceTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetRefInvoiceTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropdownInvoiceTable
        #region jointventure
        [HttpPost]

        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        public ActionResult GetJointVentureTransListByCompanyByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetJointVentureTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        public ActionResult JointVentureTransByIdByAddendum(string id)
        {
            try
            {
                IJointVentureTransService JointVentureTransService = new JointVentureTransService(db);
                return Ok(JointVentureTransService.GetJointVentureTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetJointVentureTransAuthorizedPersonTypeDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        public ActionResult CreateJointVentureTransByAddendum([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.CreateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        public ActionResult UpdateJointVentureTransByAddendum([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.UpdateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        public ActionResult DeletejointventuretransByAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IJointVentureTransService jointventuretransService = new JointVentureTransService(db, SysTransactionLogService);
                return Ok(jointventuretransService.DeleteJointVentureTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        public ActionResult GetJointVentureTransInitialDataByAddendum(string Id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableleService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableleService.GetJointVentureTransInitialDataByBusinessCollateralAgmTable(Id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeJointVentureTransByBusinessCollateralAgmTableByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion jointventure
        #region Memo
        [HttpPost]

        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult GetMemoListByCompanyByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateMemoByAddendum([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateMemoByAddendum([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult DeleteMemoByAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetMemoByIdByAddendum(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialDataByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeMemoTransByBusinessCollateralAgmTableByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        public ActionResult GetBusinessCollateralAgmTableListByCompanyByMemoTransByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBusinessCollateralAgmTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        public ActionResult GetBusinessCollateralAgmTableByIdByMemoTransByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Memo
        #region AgreementTableInfo
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        public ActionResult GetAgreementTableInfoListByCompanyByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAgreementTableInfoListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        public ActionResult GetAgreementTableInfoByIdByAddendum(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        public ActionResult CreateAgreementTableInfoByAddendum([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.CreateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        public ActionResult UpdateAgreementTableInfoByAddendum([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.UpdateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        public ActionResult DeleteAgreementTableInfoByAddendum([FromBody] RowIdentity parm)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                return Ok(AgreementTableInfoService.DeleteAgreementTableInfo(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAgreementTableInfoByBusinessCollateralAgmTableByAddendum(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoByBusinessCollateralAgm(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        public ActionResult GetAgreementTableInfoInitialDataByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetAgreementTableInfoInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        // AgreementInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeAgreementTableInfoByBusinessCollateralAgmTableByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        public ActionResult GetAgreementTableInfoAddressTransDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressTransByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTransByCustomerDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(authorizedPersonTransService.GetDropDownItemAuthorizedPersonTransByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTransByCreditAppDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(authorizedPersonTransService.GetDropDownItemAuthorizedPersonTransByCreditApp(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTypeDropDownByAddendumByAgreementTableInfo([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        public ActionResult GetAgreementTableInfoCompanyBankDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCompanyBank(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        public ActionResult GetAgreementTableInfoCompanySignatureDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetEmployeeTableWithCompanySignatureDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        public ActionResult GetAgreementTableInfoEmployeeTableDropDownByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion AgreementTableInfo
        #endregion RelatedInfo
        #region Function
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        public ActionResult GetVisibleManageMenuByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetVisibleManageMenu(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        public ActionResult GetVisibleGenerateMenuByAddendum(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetVisibleGenerateMenu(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region PrintSetBookDocTrans
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransactionByAddendum([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByRefGUID(printSetBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Manage agreement
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        public ActionResult ManageAgreementByAddendum([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.ManageAgreement(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        public ActionResult GetManageAgreementValidationByAddendum([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        public ActionResult GetManageAgreementByRefTypeByAddendum([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementByRefType(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/AddendumBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByManageAgreementByAddendum([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #endregion Function
    }
}
