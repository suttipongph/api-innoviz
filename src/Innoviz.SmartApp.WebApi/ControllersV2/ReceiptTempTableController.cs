using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using static Innoviz.SmartApp.Data.Models.Enum;
using System.Threading.Tasks;
using Innoviz.SmartApp.ReportViewer;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class ReceiptTempTableController : BaseControllerV2
	{

		public ReceiptTempTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		//[HttpPost]
		//[Route("GetInvoiceSettlementDetailList/ByCompany")]
		//public ActionResult GetInvoiceSettlementDetailListByCompany([FromBody] SearchParameter search)
		//{
		//	try
		//	{
		//		ISysListViewService sysListViewService = new SysListViewService(db);
		//		return Ok(sysListViewService.GetInvoiceSettlementDetailListvw(search));
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//[HttpGet]
		//[Route("GetInvoiceSettlementDetailById/id={id}")]
		//public ActionResult GetInvoiceSettlementDetailById(string id)
		//{
		//	try
		//	{
		//		IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
		//		return Ok(receiptTempTableService.GetInvoiceSettlementDetailById(id));
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//[HttpPost]
		//[Route("CreateInvoiceSettlementDetail")]
		//public ActionResult CreateInvoiceSettlementDetail([FromBody] InvoiceSettlementDetailItemView vwModel)
		//{
		//	try
		//	{
		//		if (ModelState.IsValid)
		//		{
		//			IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
		//			return Ok(receiptTempTableService.CreateInvoiceSettlementDetail(vwModel));
		//		}
		//		else
		//		{
		//			SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
		//			foreach (var k in ModelState.Keys)
		//			{
		//				ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
		//			}
		//			throw SmartAppUtil.AddStackTrace(ex);
		//		}
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//[HttpPost]
		//[Route("UpdateInvoiceSettlementDetail")]
		//public ActionResult UpdateInvoiceSettlementDetail([FromBody] InvoiceSettlementDetailItemView vwModel)
		//{
		//	try
		//	{
		//		if (ModelState.IsValid)
		//		{
		//			IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
		//			return Ok(receiptTempTableService.UpdateInvoiceSettlementDetail(vwModel));
		//		}
		//		else
		//		{
		//			SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
		//			foreach (var k in ModelState.Keys)
		//			{
		//				ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
		//			}
		//			throw SmartAppUtil.AddStackTrace(ex);
		//		}
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//[HttpPost]
		//[Route("DeleteInvoiceSettlementDetail")]
		//public ActionResult DeleteInvoiceSettlementDetail([FromBody] RowIdentity parm)
		//{
		//	try
		//	{
		//		IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
		//		return Ok(receiptTempTableService.DeleteInvoiceSettlementDetail(parm.Guid));
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}

		[HttpPost]
		[Route("GetReceiptTempPaymDetailList/ByCompany")]
		public ActionResult GetReceiptTempPaymDetailListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetReceiptTempPaymDetailListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetReceiptTempPaymDetailById/id={id}")]
		public ActionResult GetReceiptTempPaymDetailById(string id)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				return Ok(receiptTempTableService.GetReceiptTempPaymDetailById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateReceiptTempPaymDetail")]
		public ActionResult CreateReceiptTempPaymDetail([FromBody] ReceiptTempPaymDetailItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
					return Ok(receiptTempTableService.CreateReceiptTempPaymDetail(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateReceiptTempPaymDetail")]
		public ActionResult UpdateReceiptTempPaymDetail([FromBody] ReceiptTempPaymDetailItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
					return Ok(receiptTempTableService.UpdateReceiptTempPaymDetail(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteReceiptTempPaymDetail")]
		public ActionResult DeleteReceiptTempPaymDetail([FromBody] RowIdentity parm)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
				return Ok(receiptTempTableService.DeleteReceiptTempPaymDetail(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("GetReceiptTempPaymDetailInitialData/companyId={companyId}")]
		public ActionResult GetReceiptTempPaymDetailInitialData(string companyId)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				return Ok(receiptTempTableService.GetReceiptTempPaymDetailInitialData(companyId));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetReceiptTempTableList/ByCompany")]
		public ActionResult GetReceiptTempTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetReceiptTempTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetReceiptTempTableById/id={id}")]
		public ActionResult GetReceiptTempTableById(string id)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				return Ok(receiptTempTableService.GetReceiptTempTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateReceiptTempTable")]
		public ActionResult CreateReceiptTempTable([FromBody] ReceiptTempTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
					return Ok(receiptTempTableService.CreateReceiptTempTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateReceiptTempTable")]
		public ActionResult UpdateReceiptTempTable([FromBody] ReceiptTempTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
					return Ok(receiptTempTableService.UpdateReceiptTempTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteReceiptTempTable")]
		public ActionResult DeleteReceiptTempTable([FromBody] RowIdentity parm)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
				return Ok(receiptTempTableService.DeleteReceiptTempTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("GetReceiptTempTableInitialData/companyId={companyId}")]
		public ActionResult GetReceiptTempTableInitialData(string companyId)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				return Ok(receiptTempTableService.GetReceiptTempTableInitialData(companyId));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("ValidateIsManualNumberSeqReceiptTempTable/companyId={companyId}")]
		public ActionResult ValidateIsManualNumberSeqReceiptTempTable(string companyId)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				return Ok(receiptTempTableService.IsManualByReceiptTempTable(companyId));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("ValidateIsInvoiceSettlementOrPaymentDetailCreated/id={id}")]
		public ActionResult ValidateIsInvoiceSettlementOrPaymentDetailCreated(string id)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				return Ok(receiptTempTableService.IsInvoiceSettlementOrPaymentDetailCreated(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetExChangeRate")]
		public ActionResult GetExChangeRate([FromBody] ReceiptTempTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db, SysTransactionLogService);
					return Ok(receiptTempTableService.GetExChangeRate(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetReceiptTempTableAccessMode/id={id}")]
		public ActionResult GetReceiptTempTableAccessMode(string id)
		{
			try
			{
				IReceiptTempTableService receiptTempTable = new ReceiptTempTableService(db);
				return Ok(receiptTempTable.GetReceiptTempTableAccessMode(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region receiptTable
		[HttpPost]
		[Route("RelatedInfo/receipttable/GetReceiptLineList/ByCompany")]
		public ActionResult GetReceiptLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetReceiptLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/receipttable/GetReceiptLineById/id={id}")]

		public ActionResult GetReceiptLineById(string id)
		{
			try
			{
				IReceiptTableService receiptTableService = new ReceiptTableService(db);
				return Ok(receiptTableService.GetReceiptLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}


		[HttpPost]

		[Route("RelatedInfo/receipttable/GetReceiptTableList/ByCompany")]

		public ActionResult GetReceiptTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetReceiptTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/receipttable/GetReceiptTableById/id={id}")]
		public ActionResult GetReceiptTableById(string id)
		{
			try
			{
				IReceiptTableService receiptTableService = new ReceiptTableService(db);
				return Ok(receiptTableService.GetReceiptTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown
		[HttpPost]
		[Route("RelatedInfo/receipttable/GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IReceiptTableService receiptTableService = new ReceiptTableService(db);
				return Ok(receiptTableService.GetDropDownItemReceiptDocumentStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion
		#region Dropdown

		[HttpPost]
		[Route("GetBankGroupDropDown")]
		public ActionResult GetBankGroupDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetBuyerReceiptTableDropDown")]
		public ActionResult GetBuyerReceiptTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db);
				return Ok(buyerReceiptTableService.GetDropDownItemByReceiptTempTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetBuyerTableDropDown")]
		public ActionResult GetBuyerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetChequeTableDropDown")]
		[Route("GetChequeTableByReceiptTempPaymDetailDropDown")]
		public ActionResult GetChequeTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				return Ok(chequeTableService.GetDropDownItemChequeTableByReceiptTempPaymDetail(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetCreditAppTableDropDownByReceiptTempTable")]
		public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
				return Ok(creditAppTableService.GetDropDownItemCreditAppTableByReceiptTempTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetCurrencyDropDown")]
		[Route("RelatedInfo/receipttable/GetCurrencyDropDown")]
		public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCurrency(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetCustomerTableDropDown")]
		[Route("RelatedInfo/receipttable/GetCustomerTableByDropDown")]
		public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetInvoiceTypeDropDownByReceiptTempTableNoneProductType")]
		public ActionResult GetInvoiceTypeDropDownByReceiptTempTableNoneProductType([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTypeService invoiceTypeService = new InvoiceTypeService(db);
				return Ok(invoiceTypeService.GetInvoiceTypeByReceiptTempTableNoneProductType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceTypeDropDownByReceiptTempTableProductType")]
		public ActionResult GetInvoiceTypeDropDownByReceiptTempTableProductType([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTypeService invoiceTypeService = new InvoiceTypeService(db);
				return Ok(invoiceTypeService.GetInvoiceTypeByReceiptTempTableProductType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetLedgerDimensionDropDown")]
		public ActionResult GetLedgerDimensionDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetMethodOfPaymentDropDown")]
		[Route("RelatedInfo/buyerreceipttable/GetMethodOfPaymentDropDown")]
		public ActionResult GetMethodOfPaymentDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetReceiptTempTableDropDown")]
		public ActionResult GetReceiptTempTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemReceiptTempTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetDocumentStatusDropDown")]
		public ActionResult GetDropDownItemReceiptTempDocumentStatus([FromBody] SearchParameter search)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				return Ok(receiptTempTableService.GetDropDownItemReceiptTempDocumentStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/receipttable/GetInvoiceSettlementDetailDropDown")]
		public ActionResult GetInvoiceSettlementDetailDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceSettlementDetail(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region InvoiceSettlement
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetInvoiceSettlementDetailList/ByCompany")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailList/ByCompany")]
		public ActionResult GetInvoiceSettlementDetailListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceSettlementDetailListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetListInvoiceOutstandingList/ByCompany")]
		public ActionResult GetInvoiceOutstandingListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceOutstandingListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/GetListInvoiceOutstandingList/ByCompany")]
		public ActionResult GetSuspenseOutstandingListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceOutstandingListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/InvoiceSettlement/GetInvoiceSettlementDetailById/id={id}")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailById/id={id}")]
		public ActionResult GetInvoiceSettlementDetailById(string id)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.GetInvoiceSettlementDetailById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/CreateInvoiceSettlementDetail")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/CreateInvoiceSettlementDetail")]
		public ActionResult CreateInvoiceSettlementDetail([FromBody] InvoiceSettlementDetailItemView vwModel)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.CreateInvoiceSettlementDetail(vwModel));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/UpdateInvoiceSettlementDetail")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/UpdateInvoiceSettlementDetail")]
		public ActionResult UpdateInvoiceSettlementDetail([FromBody] InvoiceSettlementDetailItemView vwModel)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.UpdateInvoiceSettlementDetail(vwModel));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/DeleteInvoiceSettlementDetail")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/DeleteInvoiceSettlementDetail")]
		public ActionResult DeletePInvoiceSettlementDetail([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.DeleteInvoiceSettlementDetail(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetInvoiceSettlementDetailInitialData")]
		public ActionResult GetInvoiceSettlementDetailInitialData([FromBody] InvoiceSettlementDetailItemView vwModel)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.GetInvoiceSettlementDetailInitialData(vwModel));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/GetInvoiceSettlementDetailInitialData")]
		public ActionResult GetSuspenseSettlementDetailInitialData([FromBody] InvoiceSettlementDetailItemView vwModel)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.GetInvoiceSettlementDetailInitialData(vwModel));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetCalcSettlePurchaseAmount")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/GetCalcSettlePurchaseAmount")]
		public ActionResult GetCalcSettlePurchaseAmount([FromBody] InvoiceSettlementDetailItemView vwModel)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.CalcSettlePurchaseAmount(vwModel.InvoiceTableGUID, vwModel.SettleAmount));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetCalcSettleInvoiceAmount/EditWHT={editWHT}")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/GetCalcSettleInvoiceAmount/EditWHT={editWHT}")]
		public ActionResult GetCalcSettleInvoiceAmountEditWHT([FromBody] InvoiceSettlementDetailItemView vwModel, bool editWHT)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.GetCalcSettleInvoiceAmount(vwModel, editWHT));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/InvoiceSettlement/GetReceiptTempTableAccessMode/id={id}")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/GetReceiptTempTableAccessMode/id={id}")]
		public ActionResult GetInvoiceSettlementReceiptTempTableAccessMode(string id)
		{
			try
			{
				IReceiptTempTableService receiptTempTable = new ReceiptTempTableService(db);
				return Ok(receiptTempTable.GetReceiptTempTableAccessMode(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetInvoiceTableDropDown")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/GetInvoiceTableDropDown")]
		public ActionResult GetInvoiceSettlementInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetInvoiceTypeDropDown")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/GetInvoiceTypeDropDown")]
		public ActionResult GetInvoiceSettlementInvoiceTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetBuyerInvoiceTableDropDown")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/GetBuyerInvoiceTableDropDown")]
		public ActionResult GetInvoiceSettlementBuyerInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetBuyerAgreementTableDropDown")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/GetBuyerAgreementTableDropDown")]
		public ActionResult GetInvoiceSettlementBuyerAgreementTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetCustomerTableDropDown")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/GetCustomerTableDropDown")]
		public ActionResult GetInvoiceSettlementCustomerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetBuyerTableDropDown")]
		[Route("ReceiptTempPaymDetail-Child/RelatedInfo/suspensesettlement/GetBuyerTableDropDown")]
		public ActionResult GetInvoiceSettlementBuyerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Dropdown
		#endregion InvoiceSettlement
		#region ProductSettledTrans
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/RelatedInfo/ProductSettlement/GetProductSettledTransList/ByCompany")]
		public ActionResult GetProductSettlementListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetProductSettledTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/InvoiceSettlement/RelatedInfo/ProductSettlement/GetProductSettledTransById/id={id}")]
		public ActionResult GetProductSettledTransById(string id)
		{
			try
			{
				IProductSettledTransService productSettledTransService = new ProductSettledTransService(db);
				return Ok(productSettledTransService.GetProductSettledTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/RelatedInfo/ProductSettlement/UpdateProductSettledTrans")]
		public ActionResult UpdateProductSettledTrans([FromBody] ProductSettledTransItemView vmModel)
		{
			try
			{
				IProductSettledTransService productSettledTransService = new ProductSettledTransService(db);
				return Ok(productSettledTransService.UpdateProductSettledTrans(vmModel));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/RelatedInfo/ProductSettlement/GetInvoiceSettlementDetailDropDown")]
		public ActionResult GetProductSettlementInvoiceSettlementDetailDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceSettlementDetail(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[Route("RelatedInfo/InvoiceSettlement/RelatedInfo/ProductSettlement/GetPurchaseTableAccessMode/purchaseId={purchaseId}/isWorkflowMode={isWorkflowMode}")]
		public ActionResult GetPurchaseTableAccessModeByProductSettlement(string purchaseId, string isWorkflowMode)
		{
			try
			{
				IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
				return Ok(purchaseTableService.GetPurchaseTableAccessMode(purchaseId, isWorkflowMode));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/InvoiceSettlement/RelatedInfo/ProductSettlement/GetReceiptTempTableAccessMode/id={id}")]
		public ActionResult GetProductSettlementReceiptTempTableAccessMode(string id)
		{
			try
			{
				IReceiptTempTableService receiptTempTable = new ReceiptTempTableService(db);
				return Ok(receiptTempTable.GetReceiptTempTableAccessMode(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion ProductSettledTrans
		#region MemoTrans
		[HttpPost]
		[Route("RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
		public ActionResult GetMemoListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetMemoTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
		public ActionResult GetMemoById(string id)
		{
			try
			{
				IMemoTransService MemoTransService = new MemoTransService(db);
				return Ok(MemoTransService.GetMemoTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/MemoTrans/CreateMemoTrans")]
		public ActionResult CreateMemo([FromBody] MemoTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
					return Ok(MemoTransService.CreateMemoTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/MemoTrans/UpdateMemoTrans")]
		public ActionResult UpdateMemo([FromBody] MemoTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
					return Ok(MemoTransService.UpdateMemoTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/MemoTrans/DeleteMemoTrans")]
		public ActionResult DeleteMemo([FromBody] RowIdentity parm)
		{
			try
			{
				IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
				return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
		public ActionResult GetMemoTransInitialData(string id)
		{
			try
			{
				IReceiptTableService receiptTableService = new ReceiptTableService(db);
				return Ok(receiptTableService.GetMemoTransInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Attachment
		[HttpPost]
		[Route("RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
		public ActionResult GetAttachmentListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAttachmentListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/Attachment/GetAttachmentById/id={id}")]
		public async Task<ActionResult> GetAttachmentById(string id)
		{
			try
			{
				IAttachmentService attachmentService = new AttachmentService(db);
				return Ok(await attachmentService.GetAttachmentById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/CreateAttachment")]
		public ActionResult CreateAttachment([FromBody] AttachmentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
					return Ok(attachmentService.CreateAttachment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/UpdateAttachment")]
		public ActionResult UpdateAttachment([FromBody] AttachmentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
					return Ok(attachmentService.UpdateAttachment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/DeleteAttachment")]
		public ActionResult DeleteAttachment([FromBody] RowIdentity parm)
		{
			try
			{
				IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
				return Ok(attachmentService.DeleteAttachment(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/GetAttachmentRefId")]
		public ActionResult GetAttachmentRefId([FromBody] RefIdParm parm)
		{
			try
			{
				IAttachmentService attachmentService = new AttachmentService(db);
				return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#region ServiceFeeTrans
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
		public ActionResult GetServiceFeeTransListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetServiceFeeTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
		public ActionResult GetServiceFeeTransById(string id)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				return Ok(serviceFeeTransService.GetServiceFeeTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
		public ActionResult CreateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
					return Ok(serviceFeeTransService.CreateServiceFeeTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
		public ActionResult UpdateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
					return Ok(serviceFeeTransService.UpdateServiceFeeTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
		public ActionResult DeleteServiceFeeTrans([FromBody] RowIdentity parm)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
				return Ok(serviceFeeTransService.DeleteServiceFeeTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
		public ActionResult GetServiceFeeTransInitialData(string id)
		{
			try
			{
				IReceiptTempTableService receiptTempTable = new ReceiptTempTableService(db);
				return Ok(receiptTempTable.GetServiceFeeTransInitialDataByReceiptTempTable(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/GetReceiptTempTableAccessMode/id={id}")]
		public ActionResult GetReceiptTempTableAccessModeByServiceFeeTrans(string id)
		{
			try
			{
				IReceiptTempTableService receiptTempTable = new ReceiptTempTableService(db);
				return Ok(receiptTempTable.GetReceiptTempTableAccessMode(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetCalculateField")]
		public ActionResult GetCalculateField([FromBody] ServiceFeeTransItemView vwModel)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				return Ok(serviceFeeTransService.GetCalculateField(vwModel));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
		public ActionResult ValidateServiceFeeTrans([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				return Ok(serviceFeeTransService.ValidateServiceFeeTrans(getTaxValueAndWHTValueParm));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetTaxValue")]
		public ActionResult GetTaxValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				return Ok(serviceFeeTransService.GetTaxValue(getTaxValueAndWHTValueParm));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetWHTValue")]
		public ActionResult GetWHTValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				return Ok(serviceFeeTransService.GetWHTValue(getTaxValueAndWHTValueParm));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeByProductTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
				return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeByProductTypeAndDefaultNone(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
		public ActionResult GetLedgerDimensionDropDownByServiceFeeTrans([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
		public ActionResult GetTaxTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
		public ActionResult GetWithholdingTaxTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
		public ActionResult GetDocumentReasonDropDownByServiceFeeTrans([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
		public ActionResult GetOriginalInvoiceByServiceFeeTransDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetOriginalInvoiceByServiceFeeTransDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion ServiceFeeTrans

		#region InvoiceTable
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		public ActionResult GetInvoiceLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		public ActionResult GetInvoiceLineById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetInvoiceLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		public ActionResult CreateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.CreateInvoiceLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		public ActionResult UpdateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.UpdateInvoiceLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		public ActionResult DeleteInvoiceLine([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
				return Ok(invoiceTableService.DeleteInvoiceLine(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		public ActionResult UpdateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.UpdateInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		public ActionResult DeleteInvoiceTable([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
				return Ok(invoiceTableService.DeleteInvoiceTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		public ActionResult GetInvoiceTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		public ActionResult GetInvoiceTableById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetInvoiceTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		public ActionResult CreateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.CreateInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region CustTrans
		[HttpPost]
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
		public ActionResult GetInvoiceTablebyCusttrans(string id)
		{
			try
			{

				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				var result = invoiceTableService.GetInvoiceTableById(id);
				return Ok(result);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
		public ActionResult GetCustTransListByCustomer([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetCustTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion CustTrans
		#region TaxInvoice
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
		public ActionResult GetTaxInvoiceListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetTaxInvoiceTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
		public ActionResult GetTaxInvoiceTableById(string id)
		{
			try
			{
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetTaxInvoiceTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
		public ActionResult GetTaxInvoiceLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetTaxInvoiceLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
		public ActionResult GetTaxInvoiceLineById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetTaxInvoiceLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
		public ActionResult GetTaxTableDropDownTaxInvoice([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDownTaxInvoiceLine([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Report
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
		public ActionResult GetPrintTaxInvoiceCopyById(string id)
		{
			try
			{
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetPrintTaxInvoiceCopyById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
		public ActionResult PrintTaxInvoiceCopy([FromBody] PrintTaxInvoiceReportView model)
		{
			try
			{
				IReportViewerService reportViewerService = new ReportViewerService();
				return Ok(reportViewerService.RenderReport(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
		public ActionResult GetPrintTaxInvoiceOriginalById(string id)
		{
			try
			{
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetPrintTaxInvoiceOriginalById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
		public ActionResult PrintTaxInvoiceOriginal([FromBody] PrintTaxInvoiceReportView model)
		{
			try
			{
				IReportViewerService reportViewerService = new ReportViewerService();
				return Ok(reportViewerService.RenderReport(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Report
		#endregion TaxInvoice
		#region payment history
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
		public ActionResult GetPaymentHistoryListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				var result = sysListViewService.GetPaymentHistoryListvw(search);
				return Ok(result);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
		public ActionResult GetPaymentHistoryById(string id)
		{
			try
			{
				IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db);
				return Ok(paymentHistoryService.GetPaymentHistoryById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Dropdown
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		public ActionResult GetInvoiceTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		public ActionResult GetBuyerInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		public ActionResult GetCurrencyDropDownByInvoiceTable([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCurrency(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		public ActionResult GetInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		public ActionResult GetProdUnitTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		public ActionResult GetTaxInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		public ActionResult GetRefInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetRefInvoiceTableDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		public ActionResult GetDocumentStatusByInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
		public ActionResult GetReceiptTempTableDropDownByInvoiceTable([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemReceiptTempTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
		public ActionResult GetLedgerDimensionDropDownByInvoiceTable([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
		public ActionResult GetDocumentReasonDropDownByInvoiceTable([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
		//cust trans
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetBuyerTableDropDown")]
		public ActionResult GetBuyerTableDropDownByInvoiceTable([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
		public ActionResult GetMethodOfPaymentDropDownByInvoiceTable([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		//paymenmt history
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
		public ActionResult GetCreditAppTableDropDownByInvoiceTable([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusDropDownByInvoiceTable([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
		public ActionResult GetBuyerAgreementTableDropDownByInvoiceTable([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		//payment history
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
		//cust trans
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTableDropDown")]
		public ActionResult GetCustomerTableDropDownByInvoiceTable([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#endregion InvoiceTable

		#region function 

		#region cancel receipt temp
		[HttpGet]
		[Route("Function/CancelReceiptTemp/GetCancelReceiptTempById/id={id}")]
		public ActionResult GetCancelReceiptTempById(string id)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				return Ok(receiptTempTableService.GetCancelReceiptTempById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("Function/CancelReceiptTemp/CancelReceiptTemp")]
		public ActionResult CancelWithdrawal([FromBody] CancelReceiptTempView view)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				return Ok(receiptTempTableService.CancelReceiptTemp(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Function/CancelReceiptTemp/GetDocumentReasonDropDown")]
		public ActionResult GetDocumentReasonDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#region RecalculateProductSettledTrans
		[HttpGet]
		[Route("RelatedInfo/InvoiceSettlement/RelatedInfo/ProductSettlement/Function/Recalculate/GetRecalProductSettledTransById/id={id}")]
		public ActionResult GetRecalProductSettledTransById(string id)
		{
			try
			{
				IProductSettledTransService productSettledTransService = new ProductSettledTransService(db);
				return Ok(productSettledTransService.GetRecalProductSettledTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/RelatedInfo/ProductSettlement/Function/Recalculate")]
		public ActionResult RecalculateProductSettledTrans([FromBody] RecalProductSettledTransView parm)
		{
			try
			{
				IProductSettledTransService productSettledTransService = new ProductSettledTransService(db);
				return Ok(productSettledTransService.RecalculateProductSettledTrans(parm));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#region Post receipt temp
		[HttpGet]
		[Route("Function/PostReceiptTemp/GetPostReceiptTempById/id={id}")]
		public ActionResult GetPostReceiptTempById(string id)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				return Ok(receiptTempTableService.GetPostReceiptTempById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("Function/PostReceiptTemp/PostReceiptTemp")]
		public ActionResult PostReceiptTemp([FromBody] PostReceiptTempParamView view)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				return Ok(receiptTempTableService.PostReceiptTemp(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#endregion
		#region Report
		[HttpGet]
		[Route("Report/PrintReceiptTemp/GetPrintReceiptTempById/id={id}")]
		public ActionResult GetReceiptTempById(string id)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				return Ok(receiptTempTableService.GetPrintReceiptTempTableById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("Report/PrintReceiptTemp/RenderReport")]
		public ActionResult PrintReceiptTemp([FromBody] PrintReceiptTempReportView model)
		{
			try
			{
				IReportViewerService reportViewerService = new ReportViewerService();
				return Ok(reportViewerService.RenderReport(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region BuyerReceiptTable
		[HttpPost]
		[Route("RelatedInfo/buyerreceipttable/GetBuyerReceiptTableList/ByCompany")]
		public ActionResult GetBuyerReceiptTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBuyerRecepiptTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/buyerreceipttable/GetBuyerReceiptTableById/id={id}")]
		public ActionResult GetBuyerReceiptTableById(string id)
		{
			try
			{
				IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db);
				return Ok(buyerReceiptTableService.GetBuyerReceiptTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/buyerreceipttable/ValidateIsManualNumberSeq/companyId={companyId}")]
		public ActionResult ValidateIsManualNumberSeq(string companyId)
		{
			try
			{
				IBuyerReceiptTableService buyerReceiptTable = new BuyerReceiptTableService(db);
				return Ok(buyerReceiptTable.IsManualBuyerReceiptTable(companyId));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}