﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModels;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.ControllersV2.Helper
{
    public interface IControllerDiscovery
    {
        IEnumerable<SysControllerTable> GetController();
        List<SysControllerEntityMappingView> GetListControllers(IEnumerable<string> tableNames);
    }

    public class ControllerDiscovery : IControllerDiscovery
    {
        private List<SysControllerTable> controllerList;
        private readonly IActionDescriptorCollectionProvider _actionDescriptorCollectionProvider;

        public ControllerDiscovery
            (IActionDescriptorCollectionProvider actionDescriptorCollectionProvider)
        {
            _actionDescriptorCollectionProvider = actionDescriptorCollectionProvider;
        }

        public IEnumerable<SysControllerTable> GetController()
        {
            if (controllerList != null)
            {
                return controllerList;
            }

            controllerList = new List<SysControllerTable>();

            var items = _actionDescriptorCollectionProvider
                        .ActionDescriptors.Items
                        .Where(d => d.GetType() == typeof(ControllerActionDescriptor))
                        .Select(d => (ControllerActionDescriptor)d)
                        .OrderBy(o => o.AttributeRouteInfo.Template)
                        .GroupBy(d => d.ControllerTypeInfo.FullName)
                        .ToList();

            List<ControllerActionDescriptor> testList = new List<ControllerActionDescriptor>();
            //for (int i = 0; i < 5; i++) {
            //    testList.Add(items[i]);
            //}
            Console.WriteLine();


            //foreach(var item in items) {
            //    foreach(var i in item) {

            //    }
            //    //Console.WriteLine();
            //    Console.WriteLine("---------------------");

            //}
            //Console.WriteLine();

            using (StreamWriter writer = System.IO.File.CreateText("D:\\LeasingTest\\controllerList.txt"))
            {
                writer.WriteLine("[");
                foreach (var item in items)
                {
                    foreach (var i in item)
                    {
                        var controllerName = "/" + i.ControllerName + "/";
                        var routePath = i.AttributeRouteInfo.Template.Split(controllerName, 2) ;
                        var methodmetadata = (HttpMethodMetadata)i.EndpointMetadata.Where(x => x.GetType() == typeof(HttpMethodMetadata)).FirstOrDefault();
                        var httpMethod = methodmetadata != null ? methodmetadata.HttpMethods.FirstOrDefault() : null;

                      
                        if(routePath.Length > 1)
                        {
                            writer.Write("{ ");
                            writer.Write("\"ControllerName\": ");
                            writer.Write("\"" + i.ControllerName + "\", ");
                            writer.Write("\"RouteAttribute\": ");
                            writer.Write("\"" + routePath.Last() + "\", ");
                            writer.Write("\"FullPath\": ");
                            writer.Write("\"" + i.AttributeRouteInfo.Template + "\", ");
                            writer.Write("\"SplitLength\": ");
                            writer.Write("\"" + routePath.Length + "\", ");
                            writer.Write(" },");
                            writer.WriteLine();
                        }
                        
                    }
                    
                }
                writer.WriteLine("]");
            }
            return null;
        }
        public List<SysControllerEntityMappingView> GetListControllers(IEnumerable<string> tableNames)
        {
            try
            {
                var items = _actionDescriptorCollectionProvider
                        .ActionDescriptors.Items
                        .Where(d => d.GetType() == typeof(ControllerActionDescriptor))
                        .Select(d => (ControllerActionDescriptor)d)
                        .OrderBy(o => o.AttributeRouteInfo.Template)
                        .GroupBy(d => d.ControllerTypeInfo.FullName)
                        .SelectMany(s => s)
                        .Where(w => w.AttributeRouteInfo.Template.Split(string.Concat("/", w.ControllerName, "/"), 2).Length > 1)
                        .Select(s => new SysControllerEntityMappingView
                        {
                            SysControllerTable_ControllerName = s.ControllerName,
                            SysControllerTable_RouteAttribute = s.AttributeRouteInfo.Template.Split(string.Concat("/", s.ControllerName, "/"), 2).Last()
                        })
                        .Where(w => w.SysControllerTable_RouteAttribute.Contains("Get", StringComparison.InvariantCultureIgnoreCase) &&
                                    !w.SysControllerTable_RouteAttribute.Contains("Validate", StringComparison.InvariantCultureIgnoreCase) &&
                                    !w.SysControllerTable_RouteAttribute.Contains("DropDown", StringComparison.InvariantCultureIgnoreCase));

                var getlistRegex = new Regex("(?<prefix>.*?/(?i)Get|(?i)Get)(?<tablename>(?i)(?!Batch|Sys|Migration|Staging_).*)(?i)(?=List/|list$)");

                List<SysControllerEntityMappingView> result = new List<SysControllerEntityMappingView>();
                foreach (var item in items)
                {
                    var matches = getlistRegex.Matches(item.SysControllerTable_RouteAttribute);
                    if(matches.Count > 0)
                    {
                        var match = matches.Select(s => new SysControllerEntityMappingView
                        {
                            ModelName = s.Groups["tablename"].Value,
                            SysControllerTable_RouteAttribute = s.Groups["prefix"].Value,
                            SysControllerTable_ControllerName = item.SysControllerTable_ControllerName
                        });
                        if(match.All(a => tableNames.Any(any => a.ModelName.ToUpper() == any.ToUpper())))
                        {
                            result.AddRange(match);
                        }
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
