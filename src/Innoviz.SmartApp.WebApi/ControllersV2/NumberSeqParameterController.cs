﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class NumberSeqParameterController : BaseControllerV2
    {
        public NumberSeqParameterController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }

		#region NumberSeqTable
		[HttpPost]
		[Route("GetNumberSeqParameterList/ByCompany")]
		public ActionResult GetNumberSeqParameterListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetNumberSeqParameterListvw(search));
				//return Ok();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetNumberSeqParameterById/id={id}")]
		public ActionResult GetNumberSeqParameterById(string id)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return Ok(numberSequenceService.GetNumberSeqParameterById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("CreateNumberSeqParameter")]
		public ActionResult CreateNumberSeqParameter([FromBody] NumberSeqParameterItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					INumberSequenceService numberSequenceService = new NumberSequenceService(db, SysTransactionLogService);
					return Ok(numberSequenceService.CreateNumberSeqParameter(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateNumberSeqParameter")]
		public ActionResult UpdateNumberSeqParameter([FromBody] NumberSeqParameterItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					INumberSequenceService numberSequenceService = new NumberSequenceService(db, SysTransactionLogService);
					return Ok(numberSequenceService.UpdateNumberSeqParameter(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteNumberSeqParameter")]
		public ActionResult DeleteNumberSeqParameter([FromBody] RowIdentity parm)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db, SysTransactionLogService);
				return Ok(numberSequenceService.DeleteNumberSeqParameter(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region DropDown
		[HttpPost]
		[Route("GetNumberSeqTableDropDown")]
		public ActionResult GetNumberSeqTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemNumberSeqTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
