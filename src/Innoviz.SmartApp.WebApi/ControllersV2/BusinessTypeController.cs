using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class BusinessTypeController : BaseControllerV2
	{

		public BusinessTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetBusinessTypeList/ByCompany")]
		public ActionResult GetBusinessTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBusinessTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBusinessTypeById/id={id}")]
		public ActionResult GetBusinessTypeById(string id)
		{
			try
			{
				IBusinessTypeService businessTypeService = new BusinessTypeService(db);
				return Ok(businessTypeService.GetBusinessTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBusinessType")]
		public ActionResult CreateBusinessType([FromBody] BusinessTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessTypeService businessTypeService = new BusinessTypeService(db, SysTransactionLogService);
					return Ok(businessTypeService.CreateBusinessType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBusinessType")]
		public ActionResult UpdateBusinessType([FromBody] BusinessTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessTypeService businessTypeService = new BusinessTypeService(db, SysTransactionLogService);
					return Ok(businessTypeService.UpdateBusinessType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBusinessType")]
		public ActionResult DeleteBusinessType([FromBody] RowIdentity parm)
		{
			try
			{
				IBusinessTypeService businessTypeService = new BusinessTypeService(db, SysTransactionLogService);
				return Ok(businessTypeService.DeleteBusinessType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
