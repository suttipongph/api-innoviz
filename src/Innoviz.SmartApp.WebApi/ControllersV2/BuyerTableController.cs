using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class BuyerTableController : BaseControllerV2
    {

        public BuyerTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
        [HttpPost]
        [Route("GetBuyerTableList/ByCompany")]
        public ActionResult GetBuyerTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBuyerTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetBuyerTableById/id={id}")]
        public ActionResult GetBuyerTableById(string id)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetBuyerTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateBuyerTable")]
        public ActionResult CreateBuyerTable([FromBody] BuyerTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerTableService buyerTableService = new BuyerTableService(db, SysTransactionLogService);
                    return Ok(buyerTableService.CreateBuyerTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateBuyerTable")]
        public ActionResult UpdateBuyerTable([FromBody] BuyerTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerTableService buyerTableService = new BuyerTableService(db, SysTransactionLogService);
                    return Ok(buyerTableService.UpdateBuyerTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteBuyerTable")]
        public ActionResult DeleteBuyerTable([FromBody] RowIdentity parm)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db, SysTransactionLogService);
                return Ok(buyerTableService.DeleteBuyerTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown

        [HttpPost]
        [Route("GetBlacklistStatusDropDown")]
        public ActionResult GetBlacklistStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBlacklistStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetBusinessSegmentDropDown")]
        public ActionResult GetBusinessSegmentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessSegment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetBusinessSizeDropDown")]
        public ActionResult GetBusinessSizeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessSize(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetBusinessTypeDropDown")]
        public ActionResult GetBusinessTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetCreditScoringDropDown")]
        public ActionResult GetCreditScoringDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditScoring(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetCurrencyDropDown")]
        public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCurrency(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetDropDownItemBuyerStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetKYCSetupDropDown")]
        public ActionResult GetKYCDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemKYCSetup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetLineOfBusinessDropDown")]
        public ActionResult GetLineOfBusinessDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLineOfBusiness(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        [HttpGet]
        [Route("ValidateIsManualNumberSeq/companyId={companyId}")]
        public ActionResult ValidateIsManualNumberSeq(string companyId)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.IsManualByBuyer(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetBuyerTableInitialData/companyId={companyId}")]
        public ActionResult GetBuyerTableInitialData(string companyId)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetBuyerTableInitialData(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Memo
        [HttpPost]
        [Route("RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult GetMemoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetMemoById(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult DeleteMemo([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialData(string id)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Memo
        #region ContactPersonTrans
        [HttpPost]
        [Route("RelatedInfo/ContactPersonTrans/GetContactPersonTransList/ByCompany")]
        public ActionResult GetContactPersonTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetContactPersonTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/ContactPersonTrans/GetContactPersonTransById/id={id}")]
        public ActionResult GetContactPersonTransById(string id)
        {
            try
            {
                IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db);
                return Ok(contactPersonTransService.GetContactPersonTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactPersonTrans/CreateContactPersonTrans")]
        public ActionResult CreateContactPersonTrans([FromBody] ContactPersonTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db, SysTransactionLogService);
                    return Ok(contactPersonTransService.CreateContactPersonTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactPersonTrans/UpdateContactPersonTrans")]
        public ActionResult UpdateContactPersonTrans([FromBody] ContactPersonTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db, SysTransactionLogService);
                    return Ok(contactPersonTransService.UpdateContactPersonTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactPersonTrans/DeleteContactPersonTrans")]
        public ActionResult DeleteContactPersonTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db, SysTransactionLogService);
                return Ok(contactPersonTransService.DeleteContactPersonTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/ContactPersonTrans/GetContactPersonTransInitialData/id={id}")]
        public ActionResult GetContactPersonTransInitialData(string id)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db, SysTransactionLogService);
                return Ok(buyerTableService.GetContactPersonTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactPersonTrans/GetRelatedPersonTableDropDown")]
        public ActionResult GetRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region AuthorizedPersonTrans
        [HttpPost]
        [Route("RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransList/ByCompany")]
        public ActionResult GetAuthorizedPersonListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAuthorizedPersonTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransById/id={id}")]
        public ActionResult GetAuthorizedPersonById(string id)
        {
            try
            {
                IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(AuthorizedPersonTransService.GetAuthorizedPersonTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AuthorizedPersonTrans/CreateAuthorizedPersonTrans")]
        public ActionResult CreateAuthorizedPerson([FromBody] AuthorizedPersonTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db, SysTransactionLogService);
                    return Ok(AuthorizedPersonTransService.CreateAuthorizedPersonTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AuthorizedPersonTrans/UpdateAuthorizedPersonTrans")]
        public ActionResult UpdateAuthorizedPerson([FromBody] AuthorizedPersonTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db, SysTransactionLogService);
                    return Ok(AuthorizedPersonTransService.UpdateAuthorizedPersonTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AuthorizedPersonTrans/DeleteAuthorizedPersonTrans")]
        public ActionResult DeleteAuthorizedPerson([FromBody] RowIdentity parm)
        {
            try
            {
                IAuthorizedPersonTransService AuthorizedPersonTransService = new AuthorizedPersonTransService(db, SysTransactionLogService);
                return Ok(AuthorizedPersonTransService.DeleteAuthorizedPersonTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTransInitialData/id={id}")]
        public ActionResult GetAuthorizedPersonTransInitialData(string id)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetAuthorizedPersonTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AuthorizedPersonTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetRelatedInfoAuthorizedPersonTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AuthorizedPersonTrans/GetRelatedPersonTableDropDown")]
        public ActionResult GetRelatedInfoAuthorizedPersonTransRelatedPersonTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRelatedPersonTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AuthorizedPersonTrans
        #region ContactTrans
        [HttpPost]
        [Route("RelatedInfo/ContactTrans/GetContactTransList/ByCompany")]
        public ActionResult GetContactListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetContactTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/ContactTrans/GetContactTransById/id={id}")]
        public ActionResult GetContactById(string id)
        {
            try
            {
                IContactTransService ContactTransService = new ContactTransService(db);
                return Ok(ContactTransService.GetContactTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactTrans/CreateContactTrans")]
        public ActionResult CreateContact([FromBody] ContactTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
                    return Ok(ContactTransService.CreateContactTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactTrans/UpdateContactTrans")]
        public ActionResult UpdateContact([FromBody] ContactTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
                    return Ok(ContactTransService.UpdateContactTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/ContactTrans/DeleteContactTrans")]
        public ActionResult DeleteContact([FromBody] RowIdentity parm)
        {
            try
            {
                IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
                return Ok(ContactTransService.DeleteContactTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/ContactTrans/GetContactTransInitialData/id={id}")]
        public ActionResult GetContactTransInitialData(string id)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetContactTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Contact
        #region BuyerCreditLimitByProduct
        [HttpPost]
        [Route("RelatedInfo/BuyerCreditLimitByProduct/GetBuyerCreditLimitByProductList/ByCompany")]
        public ActionResult GetBuyerCreditLimitByProductListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBuyerCreditLimitByProductListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/BuyerCreditLimitByProduct/GetBuyerCreditLimitByProductById/id={id}")]
        public ActionResult GetBuyerCreditLimitByProductById(string id)
        {
            try
            {
                IBuyerCreditLimitByProductService BuyerCreditLimitByProductService = new BuyerCreditLimitByProductService(db);
                return Ok(BuyerCreditLimitByProductService.GetBuyerCreditLimitByProductById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/BuyerCreditLimitByProduct/CreateBuyerCreditLimitByProduct")]
        public ActionResult CreateBuyerCreditLimitByProduct([FromBody] BuyerCreditLimitByProductItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerCreditLimitByProductService BuyerCreditLimitByProductService = new BuyerCreditLimitByProductService(db, SysTransactionLogService);
                    return Ok(BuyerCreditLimitByProductService.CreateBuyerCreditLimitByProduct(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/BuyerCreditLimitByProduct/UpdateBuyerCreditLimitByProduct")]
        public ActionResult UpdateBuyerCreditLimitByProduct([FromBody] BuyerCreditLimitByProductItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerCreditLimitByProductService BuyerCreditLimitByProductService = new BuyerCreditLimitByProductService(db, SysTransactionLogService);
                    return Ok(BuyerCreditLimitByProductService.UpdateBuyerCreditLimitByProduct(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/BuyerCreditLimitByProduct/DeleteBuyerCreditLimitByProduct")]
        public ActionResult DeleteBuyerCreditLimitByProduct([FromBody] RowIdentity parm)
        {
            try
            {
                IBuyerCreditLimitByProductService BuyerCreditLimitByProductService = new BuyerCreditLimitByProductService(db, SysTransactionLogService);
                return Ok(BuyerCreditLimitByProductService.DeleteBuyerCreditLimitByProduct(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/BuyerCreditLimitByProduct/GetBuyerTableDropDown")]
        [Route("RelatedInfo/Inquiryassignmentagreementoutstanding/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerCreditLimitByProduct
        #region BuyerAgreementTable
        [HttpPost]
        [Route("RelatedInfo/BuyerAgreementTable/GetBuyerAgreementTableList/ByCompany")]
        public ActionResult GetBuyerAgreementTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);

                return Ok(sysListViewService.GetBuyerAgreementTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/BuyerAgreementTable/GetBuyerAgreementTableById/id={id}")]
        public ActionResult GetBuyerAgreementTableById(string id)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetBuyerAgreementTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("RelatedInfo/BuyerAgreementTable/CreateBuyerAgreementTable")]
        public ActionResult CreateBuyerAgreementTable([FromBody] BuyerAgreementTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db, SysTransactionLogService);
                    return Ok(buyerAgreementTableService.CreateBuyerAgreementTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("RelatedInfo/BuyerAgreementTable/UpdateBuyerAgreementTable")]
        public ActionResult UpdateBuyerAgreementTable([FromBody] BuyerAgreementTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db, SysTransactionLogService);
                    return Ok(buyerAgreementTableService.UpdateBuyerAgreementTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("RelatedInfo/BuyerAgreementTable/DeleteBuyerAgreementTable")]
        public ActionResult DeleteBuyerAgreementTable([FromBody] RowIdentity parm)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db, SysTransactionLogService);
                return Ok(buyerAgreementTableService.DeleteBuyerAgreementTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/BuyerAgreementTable/ValidateIsManualNumberSeq/companyId={companyId}")]
        public ActionResult ValidateBuyerAgreementTableIsManualNumberSeq(string companyId)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db, SysTransactionLogService);
                return Ok(buyerAgreementTableService.IsManualByBuyerAgreement(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        [HttpPost]
        [Route("RelatedInfo/BuyerAgreementTable/GetBuyerTableDropDown")]
        public ActionResult GetBuyerAgreementTableBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/BuyerAgreementTable/GetCustomerTableDropDown")]
        [Route("RelatedInfo/Inquiryassignmentagreementoutstanding/GetCustomerTableDropDown")]
        public ActionResult GetBuyerAgreementTableCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #endregion
        #region BuyerAgreementLine
        [HttpPost]
        [Route("RelatedInfo/BuyerAgreementTable/GetBuyerAgreementLineList/ByCompany")]
        public ActionResult GetBuyerAgreementLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);

                return Ok(sysListViewService.GetBuyerAgreementLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/BuyerAgreementTable/GetBuyerAgreementLineById/id={id}")]
        public ActionResult GetBuyerAgreementLineById(string id)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetBuyerAgreementLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("RelatedInfo/BuyerAgreementTable/CreateBuyerAgreementLine")]
        public ActionResult CreateBuyerAgreementLine([FromBody] BuyerAgreementLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db, SysTransactionLogService);
                    return Ok(buyerAgreementTableService.CreateBuyerAgreementLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("RelatedInfo/BuyerAgreementTable/UpdateBuyerAgreementLine")]
        public ActionResult UpdateBuyerAgreementLine([FromBody] BuyerAgreementLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db, SysTransactionLogService);
                    return Ok(buyerAgreementTableService.UpdateBuyerAgreementLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("RelatedInfo/BuyerAgreementTable/DeleteBuyerAgreementLine")]
        public ActionResult DeleteBuyerAgreementLine([FromBody] RowIdentity parm)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db, SysTransactionLogService);
                return Ok(buyerAgreementTableService.DeleteBuyerAgreementLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("RelatedInfo/BuyerAgreementTable/GetBuyerAgreementLineInitialData/id={id}")]
        public ActionResult GetBuyerAgreementLineInitialData(string id)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db, SysTransactionLogService);
                return Ok(buyerTableService.GetBuyerAgreementLineInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropDown
        [HttpPost]
        [Route("RelatedInfo/BuyerAgreementTable/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableBuyerAgreementDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region AddressTrans
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetAddressTransList/ByCompany")]
        public ActionResult GetAddressTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);

                return Ok(sysListViewService.GetAddressTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/AddressTrans/GetAddressTransById/id={id}")]
        public ActionResult GetAddressTransById(string id)
        {
            try
            {
                IAddressTransService AddressTransService = new AddressTransService(db);
                return Ok(AddressTransService.GetAddressTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/CreateAddressTrans")]
        public ActionResult CreateAddressTrans([FromBody] AddressTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAddressTransService AddressTransService = new AddressTransService(db, SysTransactionLogService);
                    return Ok(AddressTransService.CreateAddressTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/UpdateAddressTrans")]
        public ActionResult UpdateAddressTrans([FromBody] AddressTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAddressTransService AddressTransService = new AddressTransService(db, SysTransactionLogService);
                    return Ok(AddressTransService.UpdateAddressTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/DeleteAddressTrans")]
        public ActionResult DeleteAddressTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IAddressTransService AddressTransService = new AddressTransService(db, SysTransactionLogService);
                return Ok(AddressTransService.DeleteAddressTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/AddressTrans/GetAddressTransInitialData/id={id}")]
        public ActionResult GetAddressTransInitialData(string id)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetAddressTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetAddressCountryDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressCountryDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAddressCountry(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetAddressDistrictDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressDistrictDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressDistrict(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetAddressPostalCodeDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressPostalCodeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressPostalCode(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetAddressProvinceDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressProvinceDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressProvince(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetAddressSubDistrictDropDown")]
        public ActionResult GetRelatedInfoAddressTransAddressSubDistrictDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressSubDistrict(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetOwnershipDropDown")]
        public ActionResult GetRelatedInfoAddressTransOwnershipDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemOwnership(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/AddressTrans/GetPropertyTypeDropDown")]
        public ActionResult GetRelatedInfoAddressTransPropertyTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemPropertyType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion AddressTrans
        #region DocumentConditionTrans
        [HttpPost]
        [Route("RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        public ActionResult GetDocumentConditionTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);

				return Ok(sysListViewService.GetDocumentConditionTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
		public ActionResult GetDocumentConditionTransById(string id)
		{
			try
			{
				IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db);
				return Ok(DocumentConditionTransService.GetDocumentConditionTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
		public ActionResult CreateDocumentConditionTrans([FromBody] DocumentConditionTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
					return Ok(DocumentConditionTransService.CreateDocumentConditionTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
		public ActionResult UpdateDocumentConditionTrans([FromBody] DocumentConditionTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
					return Ok(DocumentConditionTransService.UpdateDocumentConditionTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
		public ActionResult DeleteDocumentConditionTrans([FromBody] RowIdentity parm)
		{
			try
			{
				IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
				return Ok(DocumentConditionTransService.DeleteDocumentConditionTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
		public ActionResult GetDocumentConditionTransInitialData(string id)
		{
			try
			{
				IBuyerTableService buyerTableService = new BuyerTableService(db);
				return Ok(buyerTableService.GetDocumentConditionTransInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		[HttpPost]
		[Route("RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
		public ActionResult GetRelatedInfoDocumentConditionTransDocumentTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion DocumentConditionTrans
		#region FinancialStatementTrans
		[HttpPost]
		[Route("RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
		public ActionResult GetFinancialStatementTransListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetFinancialStatementTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
		public ActionResult GetFinancialStatementTransById(string id)
		{
			try
			{
				IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
				return Ok(financialStatementTransService.GetFinancialStatementTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
		public ActionResult CreateFinancialStatementTrans([FromBody] FinancialStatementTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
					return Ok(financialStatementTransService.CreateFinancialStatementTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
		public ActionResult UpdateFinancialStatementTrans([FromBody] FinancialStatementTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
					return Ok(financialStatementTransService.UpdateFinancialStatementTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
		public ActionResult DeleteFinancialStatementTrans([FromBody] RowIdentity parm)
		{
			try
			{
				IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
				return Ok(financialStatementTransService.DeleteFinancialStatementTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
		public ActionResult GetFinancialStatementTransInitialData(string id)
		{
			try
			{
				IBuyerTableService buyerTableService = new BuyerTableService(db);
				return Ok(buyerTableService.GetFinancialStatementTransInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region Dropdown
        #endregion
        #endregion
        #region function		
        [HttpPost]
        [Route("Function/UpdateBuyerTableBlacklistAndWatchlistStatus/GetBlacklistStatusDropDown")]
        public ActionResult GetBlacklistStatusDropdown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBlacklistStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Function/UpdateBuyerTableBlacklistAndWatchlistStatus/GetDocumentReasonDropdown")]
        public ActionResult GetDocumentReasonDropdown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Function/UpdateBuyerTableBlacklistAndWatchlistStatus/GetUpdateBuyerTableBlacklistAndWatchlistStatusById/id={id}")]
        public ActionResult<bool> GetUpdateBuyerTableBlacklistAndWatchlistStatusById(string id)
        {
            try
            {
                IBuyerTableService buyerService = new BuyerTableService(db);
                return Ok(buyerService.GetUpdateBuyerTableBlacklistStatusById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Function/UpdateBuyerTableBlacklistAndWatchlistStatus")]
        public ActionResult UpdateBuyertableBlacklistAndWatchlistStatus([FromBody] UpdateBuyerTableBlacklistStatusParamView param)
        {
            try
            {
                IBuyerTableService buyerService = new BuyerTableService(db);
                return Ok(buyerService.UpdateBuyerTableBlacklistStatus(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/DocumentConditionTrans/Function/CopyDocumentConditionTemplateReceipt")]
        [Route("RelatedInfo/DocumentConditionTrans/Function/CopyDocumentConditionTemplateBilling")]
        public ActionResult CopyDocumentConditionTemplate([FromBody] CopyDocumentConditionTemplateParamView param)
        {
            try
            {
                IDocumentConditionTemplateTableService documentConditionTemplateTableService = new DocumentConditionTemplateTableService(db);
                return Ok(documentConditionTemplateTableService.CopyDocumentConditionTemplate(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/DocumentConditionTrans/Function/CopyDocumentConditionTemplateReceipt/GetDocumentConditonTransValidation")]
        [Route("RelatedInfo/DocumentConditionTrans/Function/CopyDocumentConditionTemplateBilling/GetDocumentConditonTransValidation")]
        public ActionResult GetDocumentConditonTransValidation([FromBody] CopyDocumentConditionTemplateParamView param)
        {
            try
            {
                IDocumentConditionTransService documentConditionTransService = new DocumentConditionTransService(db);
                return Ok(documentConditionTransService.GetDocumentTransByDocTypeValidation(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/DocumentConditionTrans/Function/CopyDocumentConditionTemplateReceipt/GetDocumentConditionTemplateTableDropDown")]
        [Route("RelatedInfo/DocumentConditionTrans/Function/CopyDocumentConditionTemplateBilling/GetDocumentConditionTemplateTableDropDown")]
        [Route("GetDocumentConditionTemplateTableDropDown")]
        public ActionResult GetDocumentConditionTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentConditionTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CustTransaction
        [HttpPost]
        [Route("RelatedInfo/BuyerTransaction/GetCustTransList/ByCompany")]
        public ActionResult GetCustTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/BuyerTransaction/GetCustTransListByCustomer/ByCompany")]
        public ActionResult GetCustTransListByCustomer([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/BuyerTransaction/GetCustTransListByBuyer/ByCompany")]
        public ActionResult GetCustTransListByBuyer([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/BuyerTransaction/GetCustTransById/id={id}")]
        public ActionResult GetCustById(string id)
        {
            try
            {
                ICustTransService CustTransService = new CustTransService(db);
                return Ok(CustTransService.GetCustTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/BuyerTransaction/CreateCustTrans")]
        public ActionResult CreateCustTrans([FromBody] CustTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustTransService CustTransService = new CustTransService(db, SysTransactionLogService);
                    return Ok(CustTransService.CreateCustTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/BuyerTransaction/UpdateCustTrans")]
        public ActionResult UpdateCustTrans([FromBody] CustTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICustTransService CustTransService = new CustTransService(db, SysTransactionLogService);
                    return Ok(CustTransService.UpdateCustTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/BuyerTransaction/DeleteCustTrans")]
        public ActionResult DeleteCustTrans([FromBody] RowIdentity parm)
        {
            try
            {
                ICustTransService CustTransService = new CustTransService(db, SysTransactionLogService);
                return Ok(CustTransService.DeleteCustTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("RelatedInfo/BuyerTransaction/GetCustTransInitialData/id={id}")]
        public ActionResult GetCustTransInitialData(string id)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/BuyerTransaction/GetBuyerTablebyCusttrans/id={id}")]
        public ActionResult GetBuyerTablebyCusttrans(string id)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetBuyerTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    
        #endregion CustTransaction
        #region Attachment
        [HttpPost]
        [Route("RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentById(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Attachment/CreateAttachment")]
        public ActionResult CreateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Attachment/DeleteAttachment")]
        public ActionResult DeleteAttachment([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefId([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region Outstanding
        [HttpPost]
        [Route("RelatedInfo/Inquiryassignmentagreementoutstanding/GetInquiryAssignmentOutstandingList/ByCompany")]
        public ActionResult GetInquiryAssignmentOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementOutstanding(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Oustanding
        #region Customer Credit Outstanding
        [HttpPost]
        [Route("RelatedInfo/InquiryBuyerCreditofAllCustomer/GetInquiryCALineOutstandingByCAList/ByCompany")]
        public ActionResult GetInquiryCreditOutstandingListByCust([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetGetCALineOutstandingByCA(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/InquiryBuyerCreditofAllCustomer/GetCreditAppTableDropDown")]
    
        public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/InquiryBuyerCreditofAllCustomer/GetBuyerTableDropDown")]

        public ActionResult GetBuyerTableDropDownInquiryBuyerCreditofAllCustomer([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/InquiryBuyerCreditofAllCustomer/GetCustomerTableDropDown")]

        public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/InquiryBuyerCreditofAllCustomer/GetCreditLimitTypeDropDown")]

        public ActionResult GetCreditLimitTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditLimitType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region ImportFinancialStatementTrans
        [HttpGet]
        [Route("RelatedInfo/FinancialStatementTrans/Function/ImportFinancialStatementTrans/GetFinancialStatementTransRefId/RefGUID={refGUID}/RefType={reftype}")]
        public ActionResult GetFinancialStatementTransRefId(string refGUID, int reftype)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                return Ok(financialStatementTransService.GetFinancialStatementTransRefId(refGUID, reftype));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/FinancialStatementTrans/Function/ImportFinancialStatementTrans/ImportFinancialStatement")]
        public ActionResult ImportFinancialStatement([FromBody] ImportFinancialStatementView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                    return Ok(financialStatementTransService.ImportFinancialStatement(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
