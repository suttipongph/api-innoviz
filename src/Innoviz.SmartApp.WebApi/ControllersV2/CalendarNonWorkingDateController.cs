using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class CalendarNonWorkingDateController : BaseControllerV2
	{

		public CalendarNonWorkingDateController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetCalendarNonWorkingDateList/ByCompany")]
		public ActionResult GetCalendarNonWorkingDateListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetCalendarNonWorkingDateListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetCalendarNonWorkingDateById/id={id}")]
		public ActionResult GetCalendarNonWorkingDateById(string id)
		{
			try
			{
				ICalendarNonWorkingDateService calendarNonWorkingDateService = new CalendarNonWorkingDateService(db);
				return Ok(calendarNonWorkingDateService.GetCalendarNonWorkingDateById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateCalendarNonWorkingDate")]
		public ActionResult CreateCalendarNonWorkingDate([FromBody] CalendarNonWorkingDateItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICalendarNonWorkingDateService calendarNonWorkingDateService = new CalendarNonWorkingDateService(db, SysTransactionLogService);
					return Ok(calendarNonWorkingDateService.CreateCalendarNonWorkingDate(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateCalendarNonWorkingDate")]
		public ActionResult UpdateCalendarNonWorkingDate([FromBody] CalendarNonWorkingDateItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICalendarNonWorkingDateService calendarNonWorkingDateService = new CalendarNonWorkingDateService(db, SysTransactionLogService);
					return Ok(calendarNonWorkingDateService.UpdateCalendarNonWorkingDate(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteCalendarNonWorkingDate")]
		public ActionResult DeleteCalendarNonWorkingDate([FromBody] RowIdentity parm)
		{
			try
			{
				ICalendarNonWorkingDateService calendarNonWorkingDateService = new CalendarNonWorkingDateService(db, SysTransactionLogService);
				return Ok(calendarNonWorkingDateService.DeleteCalendarNonWorkingDate(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetCalendarGroupDropDown")]
		public ActionResult GetCalendarGroupDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCalendarGroup(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
	}
}
