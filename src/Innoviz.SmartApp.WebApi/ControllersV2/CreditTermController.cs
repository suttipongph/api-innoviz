using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class CreditTermController : BaseControllerV2
	{

		public CreditTermController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetCreditTermList/ByCompany")]
		public ActionResult GetCreditTermListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetCreditTermListvw(search));
				//return Ok();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetCreditTermById/id={id}")]
		public ActionResult GetCreditTermById(string id)
		{
			try
			{
				ICreditTermService creditTermService = new CreditTermService(db);
				return Ok(creditTermService.GetCreditTermById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateCreditTerm")]
		public ActionResult CreateCreditTerm([FromBody] CreditTermItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICreditTermService creditTermService = new CreditTermService(db, SysTransactionLogService);
					return Ok(creditTermService.CreateCreditTerm(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateCreditTerm")]
		public ActionResult UpdateCreditTerm([FromBody] CreditTermItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICreditTermService creditTermService = new CreditTermService(db, SysTransactionLogService);
					return Ok(creditTermService.UpdateCreditTerm(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteCreditTerm")]
		public ActionResult DeleteCreditTerm([FromBody] RowIdentity parm)
		{
			try
			{
				ICreditTermService creditTermService = new CreditTermService(db, SysTransactionLogService);
				return Ok(creditTermService.DeleteCreditTerm(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
