using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class CreditScoringController : BaseControllerV2
	{

		public CreditScoringController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetCreditScoringList/ByCompany")]
		public ActionResult GetCreditScoringListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditScoringListvw(search));
                //return Ok();
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetCreditScoringById/id={id}")]
		public ActionResult GetCreditScoringById(string id)
		{
			try
			{
				ICreditScoringService creditScoringService = new CreditScoringService(db);
				return Ok(creditScoringService.GetCreditScoringById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateCreditScoring")]
		public ActionResult CreateCreditScoring([FromBody] CreditScoringItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICreditScoringService creditScoringService = new CreditScoringService(db, SysTransactionLogService);
					return Ok(creditScoringService.CreateCreditScoring(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateCreditScoring")]
		public ActionResult UpdateCreditScoring([FromBody] CreditScoringItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICreditScoringService creditScoringService = new CreditScoringService(db, SysTransactionLogService);
					return Ok(creditScoringService.UpdateCreditScoring(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteCreditScoring")]
		public ActionResult DeleteCreditScoring([FromBody] RowIdentity parm)
		{
			try
			{
				ICreditScoringService creditScoringService = new CreditScoringService(db, SysTransactionLogService);
				return Ok(creditScoringService.DeleteCreditScoring(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
