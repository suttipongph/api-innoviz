using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class JobTypeController : BaseControllerV2
	{

		public JobTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetJobTypeList/ByCompany")]
		public ActionResult GetJobTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetJobTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetJobTypeById/id={id}")]
		public ActionResult GetJobTypeById(string id)
		{
			try
			{
				IJobTypeService jobTypeService = new JobTypeService(db);
				return Ok(jobTypeService.GetJobTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateJobType")]
		public ActionResult CreateJobType([FromBody] JobTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IJobTypeService jobTypeService = new JobTypeService(db, SysTransactionLogService);
					return Ok(jobTypeService.CreateJobType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateJobType")]
		public ActionResult UpdateJobType([FromBody] JobTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IJobTypeService jobTypeService = new JobTypeService(db, SysTransactionLogService);
					return Ok(jobTypeService.UpdateJobType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteJobType")]
		public ActionResult DeleteJobType([FromBody] RowIdentity parm)
		{
			try
			{
				IJobTypeService jobTypeService = new JobTypeService(db, SysTransactionLogService);
				return Ok(jobTypeService.DeleteJobType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
