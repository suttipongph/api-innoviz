using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class BusinessCollateralTypeController : BaseControllerV2
	{

		public BusinessCollateralTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		#region BusinessCollateralSubType
		[HttpPost]
		[Route("GetBusinessCollateralSubTypeList/ByCompany")]
		public ActionResult GetBusinessCollateralSubTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBusinessCollateralSubTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBusinessCollateralSubTypeById/id={id}")]
		public ActionResult GetBusinessCollateralSubTypeById(string id)
		{
			try
			{
				IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db);
				return Ok(businessCollateralTypeService.GetBusinessCollateralSubTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBusinessCollateralSubType")]
		public ActionResult CreateBusinessCollateralSubType([FromBody] BusinessCollateralSubTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db, SysTransactionLogService);
					return Ok(businessCollateralTypeService.CreateBusinessCollateralSubType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBusinessCollateralSubType")]
		public ActionResult UpdateBusinessCollateralSubType([FromBody] BusinessCollateralSubTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db, SysTransactionLogService);
					return Ok(businessCollateralTypeService.UpdateBusinessCollateralSubType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBusinessCollateralSubType")]
		public ActionResult DeleteBusinessCollateralSubType([FromBody] RowIdentity parm)
		{
			try
			{
				IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db, SysTransactionLogService);
				return Ok(businessCollateralTypeService.DeleteBusinessCollateralSubType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBusinessCollateralSubTypeInitialdata/businessCollateralTypeGUID={businessCollateralTypeGUID}")]
		public ActionResult Getdocumentconditiontemplatelineinitialdata(string businessCollateralTypeGUID)
		{
			try
			{
				IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db);
				return Ok(businessCollateralTypeService.GetBusinessCollateralSubTypeInitialdata(businessCollateralTypeGUID));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
		#region BusinessCollateralType
		[HttpPost]
		[Route("GetBusinessCollateralTypeList/ByCompany")]
		public ActionResult GetBusinessCollateralTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBusinessCollateralTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBusinessCollateralTypeById/id={id}")]
		public ActionResult GetBusinessCollateralTypeById(string id)
		{
			try
			{
				IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db);
				return Ok(businessCollateralTypeService.GetBusinessCollateralTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBusinessCollateralType")]
		public ActionResult CreateBusinessCollateralType([FromBody] BusinessCollateralTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db, SysTransactionLogService);
					return Ok(businessCollateralTypeService.CreateBusinessCollateralType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBusinessCollateralType")]
		public ActionResult UpdateBusinessCollateralType([FromBody] BusinessCollateralTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db, SysTransactionLogService);
					return Ok(businessCollateralTypeService.UpdateBusinessCollateralType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBusinessCollateralType")]
		public ActionResult DeleteBusinessCollateralType([FromBody] RowIdentity parm)
		{
			try
			{
				IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db, SysTransactionLogService);
				return Ok(businessCollateralTypeService.DeleteBusinessCollateralType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Dropdown
		[HttpPost]
		[Route("GetBusinessCollateralTypeDropDown")]
		public ActionResult GetBusinessCollateralTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBusinessCollateralType(search));
				//return Ok(null);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
