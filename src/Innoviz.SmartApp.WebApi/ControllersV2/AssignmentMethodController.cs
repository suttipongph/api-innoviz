using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class AssignmentMethodController : BaseControllerV2
	{

		public AssignmentMethodController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetAssignmentMethodList/ByCompany")]
		public ActionResult GetAssignmentMethodListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAssignmentMethodListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetAssignmentMethodById/id={id}")]
		public ActionResult GetAssignmentMethodById(string id)
		{
			try
			{
				IAssignmentMethodService assignmentMethodService = new AssignmentMethodService(db);
				return Ok(assignmentMethodService.GetAssignmentMethodById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateAssignmentMethod")]
		public ActionResult CreateAssignmentMethod([FromBody] AssignmentMethodItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAssignmentMethodService assignmentMethodService = new AssignmentMethodService(db, SysTransactionLogService);
					return Ok(assignmentMethodService.CreateAssignmentMethod(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateAssignmentMethod")]
		public ActionResult UpdateAssignmentMethod([FromBody] AssignmentMethodItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAssignmentMethodService assignmentMethodService = new AssignmentMethodService(db, SysTransactionLogService);
					return Ok(assignmentMethodService.UpdateAssignmentMethod(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteAssignmentMethod")]
		public ActionResult DeleteAssignmentMethod([FromBody] RowIdentity parm)
		{
			try
			{
				IAssignmentMethodService assignmentMethodService = new AssignmentMethodService(db, SysTransactionLogService);
				return Ok(assignmentMethodService.DeleteAssignmentMethod(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
