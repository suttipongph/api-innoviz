using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class IntercompanyController : BaseControllerV2
	{

		public IntercompanyController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetIntercompanyList/ByCompany")]
		public ActionResult GetIntercompanyListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetIntercompanyListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetIntercompanyById/id={id}")]
		public ActionResult GetIntercompanyById(string id)
		{
			try
			{
				IIntercompanyService intercompanyService = new IntercompanyService(db);
				return Ok(intercompanyService.GetIntercompanyById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateIntercompany")]
		public ActionResult CreateIntercompany([FromBody] IntercompanyItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IIntercompanyService intercompanyService = new IntercompanyService(db, SysTransactionLogService);
					return Ok(intercompanyService.CreateIntercompany(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateIntercompany")]
		public ActionResult UpdateIntercompany([FromBody] IntercompanyItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IIntercompanyService intercompanyService = new IntercompanyService(db, SysTransactionLogService);
					return Ok(intercompanyService.UpdateIntercompany(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteIntercompany")]
		public ActionResult DeleteIntercompany([FromBody] RowIdentity parm)
		{
			try
			{
				IIntercompanyService intercompanyService = new IntercompanyService(db, SysTransactionLogService);
				return Ok(intercompanyService.DeleteIntercompany(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
