using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class StagingTableController : BaseControllerV2
	{

		public StagingTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetStagingTableList/ByCompany")]
		public ActionResult GetStagingTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetStagingTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetStagingTableById/id={id}")]
		public ActionResult GetStagingTableById(string id)
		{
			try
			{
				IStagingTableService stagingTableService = new StagingTableService(db);
				return Ok(stagingTableService.GetStagingTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateStagingTable")]
		public ActionResult CreateStagingTable([FromBody] StagingTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IStagingTableService stagingTableService = new StagingTableService(db, SysTransactionLogService);
					return Ok(stagingTableService.CreateStagingTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateStagingTable")]
		public ActionResult UpdateStagingTable([FromBody] StagingTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IStagingTableService stagingTableService = new StagingTableService(db, SysTransactionLogService);
					return Ok(stagingTableService.UpdateStagingTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteStagingTable")]
		public ActionResult DeleteStagingTable([FromBody] RowIdentity parm)
		{
			try
			{
				IStagingTableService stagingTableService = new StagingTableService(db, SysTransactionLogService);
				return Ok(stagingTableService.DeleteStagingTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        [HttpPost]
        [Route("GetProcessTransDropDown")]
        public ActionResult GetProcessTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemProcesTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("GetInvoiceTableDropDown")]
       
        public ActionResult GetInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region StagingTableVendorInfo
        [HttpPost]
        [Route("RelatedInfo/StagingTableVendorInfo/GetStagingTableVendorInfoList/ByCompany")]
        public ActionResult GetStagingTableVendorInfoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetStagingTableVendorInfoListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/StagingTableVendorInfo/GetStagingTableVendorInfoById/id={id}")]
        public ActionResult GetStagingTableVendorInfoById(string id)
        {
            try
            {
                IStagingTableVendorInfoService StagingTableVendorInfoService = new StagingTableVendorInfoService(db);
                return Ok(StagingTableVendorInfoService.GetStagingTableVendorInfoById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/StagingTableVendorInfo/CreateStagingTableVendorInfo")]
        public ActionResult CreateStagingTableVendorInfo([FromBody] StagingTableVendorInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IStagingTableVendorInfoService StagingTableVendorInfoService = new StagingTableVendorInfoService(db, SysTransactionLogService);
                    return Ok(StagingTableVendorInfoService.CreateStagingTableVendorInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/StagingTableVendorInfo/UpdateStagingTableVendorInfo")]
        public ActionResult UpdateStagingTableVendorInfo([FromBody] StagingTableVendorInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IStagingTableVendorInfoService StagingTableVendorInfoService = new StagingTableVendorInfoService(db, SysTransactionLogService);
                    return Ok(StagingTableVendorInfoService.UpdateStagingTableVendorInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/StagingTableVendorInfo/DeleteStagingTableVendorInfo")]
        public ActionResult DeleteStagingTableVendorInfo([FromBody] RowIdentity parm)
        {
            try
            {
                IStagingTableVendorInfoService StagingTableVendorInfoService = new StagingTableVendorInfoService(db, SysTransactionLogService);
                return Ok(StagingTableVendorInfoService.DeleteStagingTableVendorInfo(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion StagingTableVendorInfo
    }
}
