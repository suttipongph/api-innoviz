using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class ExposureGroupController : BaseControllerV2
	{

		public ExposureGroupController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetExposureGroupList/ByCompany")]
		public ActionResult GetExposureGroupListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetExposureGroupListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetExposureGroupById/id={id}")]
		public ActionResult GetExposureGroupById(string id)
		{
			try
			{
				IExposureGroupService exposureGroupService = new ExposureGroupService(db);
				return Ok(exposureGroupService.GetExposureGroupById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateExposureGroup")]
		public ActionResult CreateExposureGroup([FromBody] ExposureGroupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IExposureGroupService exposureGroupService = new ExposureGroupService(db, SysTransactionLogService);
					return Ok(exposureGroupService.CreateExposureGroup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateExposureGroup")]
		public ActionResult UpdateExposureGroup([FromBody] ExposureGroupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IExposureGroupService exposureGroupService = new ExposureGroupService(db, SysTransactionLogService);
					return Ok(exposureGroupService.UpdateExposureGroup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteExposureGroup")]
		public ActionResult DeleteExposureGroup([FromBody] RowIdentity parm)
		{
			try
			{
				IExposureGroupService exposureGroupService = new ExposureGroupService(db, SysTransactionLogService);
				return Ok(exposureGroupService.DeleteExposureGroup(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetExposureGroupByProductList/ByCompany")]
		public ActionResult GetExposureGroupByProductListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetExposureGroupByProductListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetExposureGroupByProductById/id={id}")]
		public ActionResult GetExposureGroupByProductById(string id)
		{
			try
			{
				IExposureGroupService exposureGroupService = new ExposureGroupService(db);
				return Ok(exposureGroupService.GetExposureGroupByProductById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateExposureGroupByProduct")]
		public ActionResult CreateExposureGroupByProduct([FromBody] ExposureGroupByProductItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IExposureGroupService exposureGroupService = new ExposureGroupService(db, SysTransactionLogService);
					return Ok(exposureGroupService.CreateExposureGroupByProduct(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateExposureGroupByProduct")]
		public ActionResult UpdateExposureGroupByProduct([FromBody] ExposureGroupByProductItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IExposureGroupService exposureGroupService = new ExposureGroupService(db, SysTransactionLogService);
					return Ok(exposureGroupService.UpdateExposureGroupByProduct(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteExposureGroupByProduct")]
		public ActionResult DeleteExposureGroupByProduct([FromBody] RowIdentity parm)
		{
			try
			{
				IExposureGroupService exposureGroupService = new ExposureGroupService(db, SysTransactionLogService);
				return Ok(exposureGroupService.DeleteExposureGroupByProduct(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("GetExposureGroupByProductInitialData/id={id}")]
		public ActionResult GetExposureGroupByProductInitialData(string id)
		{
			try
			{
				IExposureGroupService exposureGroupService = new ExposureGroupService(db, SysTransactionLogService);
				return Ok(exposureGroupService.GetExposureGroupByProductInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
