using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class RetentionTransController : BaseControllerV2
	{

		public RetentionTransController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetRetentionTransList/ByCompany")]
		public ActionResult GetRetentionTransListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetRetentionTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetRetentionTransById/id={id}")]
		public ActionResult GetRetentionTransById(string id)
		{
			try
			{
				IRetentionTransService retentionTransService = new RetentionTransService(db);
				return Ok(retentionTransService.GetRetentionTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateRetentionTrans")]
		public ActionResult CreateRetentionTrans([FromBody] RetentionTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IRetentionTransService retentionTransService = new RetentionTransService(db, SysTransactionLogService);
					return Ok(retentionTransService.CreateRetentionTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateRetentionTrans")]
		public ActionResult UpdateRetentionTrans([FromBody] RetentionTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IRetentionTransService retentionTransService = new RetentionTransService(db, SysTransactionLogService);
					return Ok(retentionTransService.UpdateRetentionTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteRetentionTrans")]
		public ActionResult DeleteRetentionTrans([FromBody] RowIdentity parm)
		{
			try
			{
				IRetentionTransService retentionTransService = new RetentionTransService(db, SysTransactionLogService);
				return Ok(retentionTransService.DeleteRetentionTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
