using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class RetentionConditionSetupController : BaseControllerV2
	{

		public RetentionConditionSetupController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetRetentionConditionSetupList/ByCompany")]
		public ActionResult GetRetentionConditionSetupListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetRetentionConditionSetupListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetRetentionConditionSetupById/id={id}")]
		public ActionResult GetRetentionConditionSetupById(string id)
		{
			try
			{
				IRetentionConditionSetupService retentionConditionSetupService = new RetentionConditionSetupService(db);
				return Ok(retentionConditionSetupService.GetRetentionConditionSetupById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateRetentionConditionSetup")]
		public ActionResult CreateRetentionConditionSetup([FromBody] RetentionConditionSetupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IRetentionConditionSetupService retentionConditionSetupService = new RetentionConditionSetupService(db, SysTransactionLogService);
					return Ok(retentionConditionSetupService.CreateRetentionConditionSetup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateRetentionConditionSetup")]
		public ActionResult UpdateRetentionConditionSetup([FromBody] RetentionConditionSetupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IRetentionConditionSetupService retentionConditionSetupService = new RetentionConditionSetupService(db, SysTransactionLogService);
					return Ok(retentionConditionSetupService.UpdateRetentionConditionSetup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteRetentionConditionSetup")]
		public ActionResult DeleteRetentionConditionSetup([FromBody] RowIdentity parm)
		{
			try
			{
				IRetentionConditionSetupService retentionConditionSetupService = new RetentionConditionSetupService(db, SysTransactionLogService);
				return Ok(retentionConditionSetupService.DeleteRetentionConditionSetup(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
