using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class BankTypeController : BaseControllerV2
	{

		public BankTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetBankTypeList/ByCompany")]
		public ActionResult GetBankTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBankTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBankTypeById/id={id}")]
		public ActionResult GetBankTypeById(string id)
		{
			try
			{
				IBankTypeService bankTypeService = new BankTypeService(db);
				return Ok(bankTypeService.GetBankTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBankType")]
		public ActionResult CreateBankType([FromBody] BankTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBankTypeService bankTypeService = new BankTypeService(db, SysTransactionLogService);
					return Ok(bankTypeService.CreateBankType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBankType")]
		public ActionResult UpdateBankType([FromBody] BankTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBankTypeService bankTypeService = new BankTypeService(db, SysTransactionLogService);
					return Ok(bankTypeService.UpdateBankType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBankType")]
		public ActionResult DeleteBankType([FromBody] RowIdentity parm)
		{
			try
			{
				IBankTypeService bankTypeService = new BankTypeService(db, SysTransactionLogService);
				return Ok(bankTypeService.DeleteBankType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
