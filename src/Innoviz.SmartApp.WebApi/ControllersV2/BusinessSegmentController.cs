using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class BusinessSegmentController : BaseControllerV2
	{

		public BusinessSegmentController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetBusinessSegmentList/ByCompany")]
		public ActionResult GetBusinessSegmentListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBusinessSegmentListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBusinessSegmentById/id={id}")]
		public ActionResult GetBusinessSegmentById(string id)
		{
			try
			{
				IBusinessSegmentService businessSegmentService = new BusinessSegmentService(db);
				return Ok(businessSegmentService.GetBusinessSegmentById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBusinessSegment")]
		public ActionResult CreateBusinessSegment([FromBody] BusinessSegmentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessSegmentService businessSegmentService = new BusinessSegmentService(db, SysTransactionLogService);
					return Ok(businessSegmentService.CreateBusinessSegment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBusinessSegment")]
		public ActionResult UpdateBusinessSegment([FromBody] BusinessSegmentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBusinessSegmentService businessSegmentService = new BusinessSegmentService(db, SysTransactionLogService);
					return Ok(businessSegmentService.UpdateBusinessSegment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBusinessSegment")]
		public ActionResult DeleteBusinessSegment([FromBody] RowIdentity parm)
		{
			try
			{
				IBusinessSegmentService businessSegmentService = new BusinessSegmentService(db, SysTransactionLogService);
				return Ok(businessSegmentService.DeleteBusinessSegment(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
