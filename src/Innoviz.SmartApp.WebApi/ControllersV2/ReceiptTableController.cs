using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class ReceiptTableController : BaseControllerV2
	{

		public ReceiptTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetReceiptLineList/ByCompany")]
		public ActionResult GetReceiptLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetReceiptLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceTypeDropDown")]
		public ActionResult GetInvoiceTypeDropDownByPaymentDetail([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTypeService invoiceTypeService = new InvoiceTypeService(db);
				return Ok(invoiceTypeService.GetInvoiceTypeByPaymentDetail(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetReceiptLineById/id={id}")]
		public ActionResult GetReceiptLineById(string id)
		{
			try
			{
				IReceiptTableService receiptTableService = new ReceiptTableService(db);
				return Ok(receiptTableService.GetReceiptLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateReceiptLine")]
		public ActionResult CreateReceiptLine([FromBody] ReceiptLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IReceiptTableService receiptTableService = new ReceiptTableService(db, SysTransactionLogService);
					return Ok(receiptTableService.CreateReceiptLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateReceiptLine")]
		public ActionResult UpdateReceiptLine([FromBody] ReceiptLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IReceiptTableService receiptTableService = new ReceiptTableService(db, SysTransactionLogService);
					return Ok(receiptTableService.UpdateReceiptLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteReceiptLine")]
		public ActionResult DeleteReceiptLine([FromBody] RowIdentity parm)
		{
			try
			{
				IReceiptTableService receiptTableService = new ReceiptTableService(db, SysTransactionLogService);
				return Ok(receiptTableService.DeleteReceiptLine(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetReceiptTableList/ByCompany")]
		public ActionResult GetReceiptTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetReceiptTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetReceiptTableById/id={id}")]
		public ActionResult GetReceiptTableById(string id)
		{
			try
			{
				IReceiptTableService receiptTableService = new ReceiptTableService(db);
				return Ok(receiptTableService.GetReceiptTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateReceiptTable")]
		public ActionResult CreateReceiptTable([FromBody] ReceiptTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IReceiptTableService receiptTableService = new ReceiptTableService(db, SysTransactionLogService);
					return Ok(receiptTableService.CreateReceiptTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateReceiptTable")]
		public ActionResult UpdateReceiptTable([FromBody] ReceiptTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IReceiptTableService receiptTableService = new ReceiptTableService(db, SysTransactionLogService);
					return Ok(receiptTableService.UpdateReceiptTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteReceiptTable")]
		public ActionResult DeleteReceiptTable([FromBody] RowIdentity parm)
		{
			try
			{
				IReceiptTableService receiptTableService = new ReceiptTableService(db, SysTransactionLogService);
				return Ok(receiptTableService.DeleteReceiptTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region TaxInvoice
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
		public ActionResult GetTaxInvoiceListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetTaxInvoiceTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
		public ActionResult GetTaxInvoiceTableById(string id)
		{
			try
			{
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetTaxInvoiceTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
		public ActionResult GetTaxInvoiceLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetTaxInvoiceLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
		public ActionResult GetTaxInvoiceLineById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetTaxInvoiceLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
		public ActionResult GetTaxInvoiceTableDropDownTaxInvoiceLine([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDownTaxInvoiceLine([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion TaxInvoice

		#region Dropdown
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusTaxDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetDropDownItemTaxInvoiceDocumentStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IReceiptTableService receiptTableService = new ReceiptTableService(db);
				return Ok(receiptTableService.GetDropDownItemReceiptDocumentStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetCustomerTableByDropDown")]
		public ActionResult GetCustomerTableByDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTableBy(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceSettlementDetailDropDown")]
		public ActionResult GetInvoiceSettlementDetailDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceSettlementDetail(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetCurrencyDropDown")]
		public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCurrency(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceTableDropDown")]
		public ActionResult GetInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion

		#region report
		[HttpGet]
		[Route("Report/PrintReceiptCopy/GetPrintReceiptById/id={id}")]
		public ActionResult GetPrintReceiptCopyById(string id)
		{
			try
			{
				IReceiptTableService receiptTableService = new ReceiptTableService(db);
				return Ok(receiptTableService.GetPrintReceiptCopyById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("Report/PrintReceiptCopy/RenderReport")]
		public ActionResult PrintReceiptCopy([FromBody] PrintReceiptReportView model)
		{
			try
			{
				IReportViewerService reportViewerService = new ReportViewerService();
				return Ok(reportViewerService.RenderReport(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("Report/PrintReceiptOriginal/GetPrintReceiptById/id={id}")]
		public ActionResult GetPrintOriginalById(string id)
		{
			try
			{
				IReceiptTableService receiptTableService = new ReceiptTableService(db);
				return Ok(receiptTableService.GetPrintReceiptOriginalById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("Report/PrintReceiptOriginal/RenderReport")]
		public ActionResult PrintOriginal([FromBody] PrintReceiptReportView model)
		{
			try
			{
				IReportViewerService reportViewerService = new ReportViewerService();
				return Ok(reportViewerService.RenderReport(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion report
	}
}
