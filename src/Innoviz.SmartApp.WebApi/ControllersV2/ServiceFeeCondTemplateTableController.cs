using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class ServiceFeeCondTemplateTableController : BaseControllerV2
	{

		public ServiceFeeCondTemplateTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		#region ServiceFeeCondTemplateLine
		[HttpPost]
		[Route("GetServiceFeeCondTemplateLineList/ByCompany")]
		public ActionResult GetServiceFeeCondTemplateLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetServiceFeeCondTemplateLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetServiceFeeCondTemplateLineById/id={id}")]
		public ActionResult GetServiceFeeCondTemplateLineById(string id)
		{
			try
			{
				IServiceFeeCondTemplateTableService serviceFeeCondTemplateTableService = new ServiceFeeCondTemplateTableService(db);
				return Ok(serviceFeeCondTemplateTableService.GetServiceFeeCondTemplateLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateServiceFeeCondTemplateLine")]
		public ActionResult CreateServiceFeeCondTemplateLine([FromBody] ServiceFeeCondTemplateLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IServiceFeeCondTemplateTableService serviceFeeCondTemplateTableService = new ServiceFeeCondTemplateTableService(db, SysTransactionLogService);
					return Ok(serviceFeeCondTemplateTableService.CreateServiceFeeCondTemplateLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateServiceFeeCondTemplateLine")]
		public ActionResult UpdateServiceFeeCondTemplateLine([FromBody] ServiceFeeCondTemplateLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IServiceFeeCondTemplateTableService serviceFeeCondTemplateTableService = new ServiceFeeCondTemplateTableService(db, SysTransactionLogService);
					return Ok(serviceFeeCondTemplateTableService.UpdateServiceFeeCondTemplateLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteServiceFeeCondTemplateLine")]
		public ActionResult DeleteServiceFeeCondTemplateLine([FromBody] RowIdentity parm)
		{
			try
			{
				IServiceFeeCondTemplateTableService serviceFeeCondTemplateTableService = new ServiceFeeCondTemplateTableService(db, SysTransactionLogService);
				return Ok(serviceFeeCondTemplateTableService.DeleteServiceFeeCondTemplateLine(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetServiceFeeCondTemplateLineInitialData/id={id}")]
		public ActionResult GetServiceFeeCondTemplateLineInitialData(string id)
		{
			try
			{
				IServiceFeeCondTemplateTableService serviceFeeCondTemplateTableService = new ServiceFeeCondTemplateTableService(db);
				return Ok(serviceFeeCondTemplateTableService.GetServiceFeeCondTemplateLineInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetInvoiceRevenueTypeInitialData/id={id}")]
		public ActionResult GetInvoiceRevenueTypeInitialData(string id)
		{
			try
			{
				IServiceFeeCondTemplateTableService serviceFeeCondTemplateTableService = new ServiceFeeCondTemplateTableService(db);
					return Ok(serviceFeeCondTemplateTableService.GetInvoiceRevenueTypeInitialData(id));
				
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region ServiceFeeCondTemplateTable
		[HttpPost]
		[Route("GetServiceFeeCondTemplateTableList/ByCompany")]
		public ActionResult GetServiceFeeCondTemplateTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetServiceFeeCondTemplateTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetServiceFeeCondTemplateTableById/id={id}")]
		public ActionResult GetServiceFeeCondTemplateTableById(string id)
		{
			try
			{
				IServiceFeeCondTemplateTableService serviceFeeCondTemplateTableService = new ServiceFeeCondTemplateTableService(db);
				return Ok(serviceFeeCondTemplateTableService.GetServiceFeeCondTemplateTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateServiceFeeCondTemplateTable")]
		public ActionResult CreateServiceFeeCondTemplateTable([FromBody] ServiceFeeCondTemplateTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IServiceFeeCondTemplateTableService serviceFeeCondTemplateTableService = new ServiceFeeCondTemplateTableService(db, SysTransactionLogService);
					return Ok(serviceFeeCondTemplateTableService.CreateServiceFeeCondTemplateTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateServiceFeeCondTemplateTable")]
		public ActionResult UpdateServiceFeeCondTemplateTable([FromBody] ServiceFeeCondTemplateTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IServiceFeeCondTemplateTableService serviceFeeCondTemplateTableService = new ServiceFeeCondTemplateTableService(db, SysTransactionLogService);
					return Ok(serviceFeeCondTemplateTableService.UpdateServiceFeeCondTemplateTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteServiceFeeCondTemplateTable")]
		public ActionResult DeleteServiceFeeCondTemplateTable([FromBody] RowIdentity parm)
		{
			try
			{
				IServiceFeeCondTemplateTableService serviceFeeCondTemplateTableService = new ServiceFeeCondTemplateTableService(db, SysTransactionLogService);
				return Ok(serviceFeeCondTemplateTableService.DeleteServiceFeeCondTemplateTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("ValidateIsServiceFeeCondTemplateLine/serviceFeeCondTemplateTableGUID={serviceFeeCondTemplateTableGUID}")]
		public ActionResult ValidateIsServiceFeeCondTemplateLine(string serviceFeeCondTemplateTableGUID)
		{
			try
			{
				IServiceFeeCondTemplateTableService serviceFeeCondTemplateTableService = new ServiceFeeCondTemplateTableService(db, SysTransactionLogService);
				return Ok(serviceFeeCondTemplateTableService.IsServiceFeeCondTemplateLine(serviceFeeCondTemplateTableGUID));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Dropdown

        [HttpPost]
		[Route("GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceRevenueTypeByProductTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeByProductTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceRevenueTypeService invoiceRevenueType = new InvoiceRevenueTypeService(db);
				return Ok(invoiceRevenueType.GetInvoiceRevenueTypeByProductType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetServiceFeeCondTemplateTableDropDown")]
		public ActionResult GetServiceFeeCondTemplateTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemServiceFeeCondTemplateTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}


		#endregion
	}
}
