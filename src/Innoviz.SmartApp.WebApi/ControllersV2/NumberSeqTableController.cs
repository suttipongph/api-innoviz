﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class NumberSeqTableController : BaseControllerV2
    {
		public NumberSeqTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		#region NumberSeqTable
		[HttpPost]
		[Route("GetNumberSeqTableList/ByCompany")]
		public ActionResult GetNumberSeqTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetNumberSeqTableListvw(search));
				//return Ok();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetNumberSeqTableById/id={id}")]
		public ActionResult GetNumberSeqTableById(string id)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return Ok(numberSequenceService.GetNumberSeqTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("CreateNumberSeqTable")]
		public ActionResult CreateNumberSeqTable([FromBody] NumberSeqTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					INumberSequenceService numberSequenceService = new NumberSequenceService(db, SysTransactionLogService);
					return Ok(numberSequenceService.CreateNumberSeqTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateNumberSeqTable")]
		public ActionResult UpdateNumberSeqTable([FromBody] NumberSeqTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					INumberSequenceService numberSequenceService = new NumberSequenceService(db, SysTransactionLogService);
					return Ok(numberSequenceService.UpdateNumberSeqTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteNumberSeqTable")]
		public ActionResult DeleteNumberSeqTable([FromBody] RowIdentity parm)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db, SysTransactionLogService);
				return Ok(numberSequenceService.DeleteNumberSeqTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
#endregion
        #region NumberSeqSegment
        [HttpPost]
		[Route("GetNumberSeqSegmentList/ByCompany")]
		public ActionResult GetNumberSeqSegmentListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetNumberSeqSegmentListvw(search));
				//return Ok();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetNumberSeqSegmentById/id={id}")]
		public ActionResult GetNumberSeqSegmentById(string id)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return Ok(numberSequenceService.GetNumberSeqSegmentById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("CreateNumberSeqSegment")]
		public ActionResult CreateNumberSeqSegment([FromBody] NumberSeqSegmentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					INumberSequenceService numberSequenceService = new NumberSequenceService(db, SysTransactionLogService);
					return Ok(numberSequenceService.CreateNumberSeqSegment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateNumberSeqSegment")]
		public ActionResult UpdateNumberSeqSegment([FromBody] NumberSeqSegmentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					INumberSequenceService numberSequenceService = new NumberSequenceService(db, SysTransactionLogService);
					return Ok(numberSequenceService.UpdateNumberSeqSegment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteNumberSeqSegment")]
		public ActionResult DeleteNumberSeqSegment([FromBody] RowIdentity parm)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db, SysTransactionLogService);
				return Ok(numberSequenceService.DeleteNumberSeqSegment(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetNumberSeqSegmentInitialData/id={id}")]
		public ActionResult GetNumberSeqSegmentInitialData(string id)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db, SysTransactionLogService);
				return Ok(numberSequenceService.GetNumberSeqSegmentInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region DropDown
		[HttpPost]
		[Route("GetNumberSeqTableDropDown")]
		public ActionResult GetNumberSeqTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemNumberSeqTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
