﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class EmplTeamController : BaseControllerV2
	{
		public EmplTeamController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetEmplTeamList/ByCompany")]
		public ActionResult GetEmplTeamListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetEmplTeamListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetEmplTeamById/id={id}")]
		public ActionResult GetEmplTeamById(string id)
		{
			try
			{
				IEmplTeamService emplTeamService = new EmplTeamService(db);
				return Ok(emplTeamService.GetEmplTeamById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateEmplTeam")]
		public ActionResult CreateEmplTeam([FromBody] EmplTeamItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IEmplTeamService emplTeamService = new EmplTeamService(db, SysTransactionLogService);
					return Ok(emplTeamService.CreateEmplTeam(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateEmplTeam")]
		public ActionResult UpdateEmplTeam([FromBody] EmplTeamItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IEmplTeamService emplTeamService = new EmplTeamService(db, SysTransactionLogService);
					return Ok(emplTeamService.UpdateEmplTeam(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteEmplTeam")]
		public ActionResult DeleteEmplTeam([FromBody] RowIdentity parm)
		{
			try
			{
				IEmplTeamService emplTeamService = new EmplTeamService(db, SysTransactionLogService);
				return Ok(emplTeamService.DeleteEmplTeam(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		[HttpPost]
		[Route("GetEmplTeamDropDown")]
		public ActionResult GetEmplTeamDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemEmplTeam(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
