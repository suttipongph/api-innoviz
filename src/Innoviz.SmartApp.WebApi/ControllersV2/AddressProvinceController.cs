﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class AddressProvinceController : BaseControllerV2
    {
		public AddressProvinceController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetAddressProvinceList/ByCompany")]
		public ActionResult GetAddressProvinceListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAddressProvinceListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetAddressProvinceById/id={id}")]
		public ActionResult GetAddressProvinceById(string id)
		{
			try
			{
				IAddressService addressService = new AddressService(db);
				return Ok(addressService.GetAddressProvinceById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateAddressProvince")]
		public ActionResult CreateAddressProvince([FromBody] AddressProvinceItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressService addressService = new AddressService(db, SysTransactionLogService);
					return Ok(addressService.CreateAddressProvince(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateAddressProvince")]
		public ActionResult UpdateAddressProvince([FromBody] AddressProvinceItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressService addressService = new AddressService(db, SysTransactionLogService);
					return Ok(addressService.UpdateAddressProvince(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteAddressProvince")]
		public ActionResult DeleteAddressProvince([FromBody] RowIdentity parm)
		{
			try
			{
				IAddressService addressService = new AddressService(db, SysTransactionLogService);
				return Ok(addressService.DeleteAddressProvince(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown
		[HttpPost]
		[Route("GetAddressCountryDropDown")]
		public ActionResult GetAddressCountryDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemAddressCountry(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
	}
}
