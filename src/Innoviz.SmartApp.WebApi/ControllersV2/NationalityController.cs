using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class NationalityController : BaseControllerV2
	{

		public NationalityController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetNationalityList/ByCompany")]
		public ActionResult GetNationalityListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetNationalityListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetNationalityById/id={id}")]
		public ActionResult GetNationalityById(string id)
		{
			try
			{
				INationalityService nationalityService = new NationalityService(db);
				return Ok(nationalityService.GetNationalityById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateNationality")]
		public ActionResult CreateNationality([FromBody] NationalityItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					INationalityService nationalityService = new NationalityService(db, SysTransactionLogService);
					return Ok(nationalityService.CreateNationality(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateNationality")]
		public ActionResult UpdateNationality([FromBody] NationalityItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					INationalityService nationalityService = new NationalityService(db, SysTransactionLogService);
					return Ok(nationalityService.UpdateNationality(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteNationality")]
		public ActionResult DeleteNationality([FromBody] RowIdentity parm)
		{
			try
			{
				INationalityService nationalityService = new NationalityService(db, SysTransactionLogService);
				return Ok(nationalityService.DeleteNationality(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
