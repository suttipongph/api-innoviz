using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class ProductSubTypeController : BaseControllerV2
	{

		public ProductSubTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetProductSubTypeList/ByCompany")]
		public ActionResult GetProductSubTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetProductSubTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetProductSubTypeById/id={id}")]
		public ActionResult GetProductSubTypeById(string id)
		{
			try
			{
				IProductSubTypeService productSubTypeService = new ProductSubTypeService(db);
				return Ok(productSubTypeService.GetProductSubTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateProductSubType")]
		public ActionResult CreateProductSubType([FromBody] ProductSubTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IProductSubTypeService productSubTypeService = new ProductSubTypeService(db, SysTransactionLogService);
					return Ok(productSubTypeService.CreateProductSubType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateProductSubType")]
		public ActionResult UpdateProductSubType([FromBody] ProductSubTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IProductSubTypeService productSubTypeService = new ProductSubTypeService(db, SysTransactionLogService);
					return Ok(productSubTypeService.UpdateProductSubType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteProductSubType")]
		public ActionResult DeleteProductSubType([FromBody] RowIdentity parm)
		{
			try
			{
				IProductSubTypeService productSubTypeService = new ProductSubTypeService(db, SysTransactionLogService);
				return Ok(productSubTypeService.DeleteProductSubType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
