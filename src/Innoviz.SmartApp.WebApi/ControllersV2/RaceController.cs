using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class RaceController : BaseControllerV2
	{

		public RaceController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetRaceList/ByCompany")]
		public ActionResult GetRaceListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetRaceListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetRaceById/id={id}")]
		public ActionResult GetRaceById(string id)
		{
			try
			{
				IRaceService raceService = new RaceService(db);
				return Ok(raceService.GetRaceById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateRace")]
		public ActionResult CreateRace([FromBody] RaceItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IRaceService raceService = new RaceService(db, SysTransactionLogService);
					return Ok(raceService.CreateRace(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateRace")]
		public ActionResult UpdateRace([FromBody] RaceItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IRaceService raceService = new RaceService(db, SysTransactionLogService);
					return Ok(raceService.UpdateRace(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteRace")]
		public ActionResult DeleteRace([FromBody] RowIdentity parm)
		{
			try
			{
				IRaceService raceService = new RaceService(db, SysTransactionLogService);
				return Ok(raceService.DeleteRace(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
