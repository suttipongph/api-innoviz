using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class RelatedPersonTableController : BaseControllerV2
	{

		public RelatedPersonTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetRelatedPersonTableList/ByCompany")]
		public ActionResult GetRelatedPersonTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetRelatedPersonTableListvw(search));
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetRelatedPersonTableById/id={id}")]
		public ActionResult GetRelatedPersonTableById(string id)
		{
			try
			{
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
				return Ok(relatedPersonTableService.GetRelatedPersonTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateRelatedPersonTable")]
		public ActionResult CreateRelatedPersonTable([FromBody] RelatedPersonTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db, SysTransactionLogService);
					return Ok(relatedPersonTableService.CreateRelatedPersonTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateRelatedPersonTable")]
		public ActionResult UpdateRelatedPersonTable([FromBody] RelatedPersonTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db, SysTransactionLogService);
					return Ok(relatedPersonTableService.UpdateRelatedPersonTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteRelatedPersonTable")]
		public ActionResult DeleteRelatedPersonTable([FromBody] RowIdentity parm)
		{
			try
			{
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db, SysTransactionLogService);
				return Ok(relatedPersonTableService.DeleteRelatedPersonTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetCustomerTableDropDown")]
		public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetGenderDropDown")]
		public ActionResult GetGenderDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemGender(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetMaritalStatusDropDown")]
		public ActionResult GetMaritalStatusDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMaritalStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetNationalityDropDown")]
		public ActionResult GetNationalityDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemNationality(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetOccupationDropDown")]
		public ActionResult GetOccupationDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemOccupation(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetRaceDropDown")]
		public ActionResult GetRaceDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRace(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetRegistrationTypeDropDown")]
		public ActionResult GetRegistrationTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemRegistrationType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion

		[Route("ValidateIsManualNumberSeq/companyId={companyId}")]
		public ActionResult ValidateIsManualNumberSeq(string companyId)
		{
			try
			{
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
				return Ok(relatedPersonTableService.IsManualByRelatedPerson(companyId));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Memo
		[HttpPost]
		[Route("RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
		public ActionResult GetMemoListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetMemoTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
		public ActionResult GetMemoById(string id)
		{
			try
			{
				IMemoTransService MemoTransService = new MemoTransService(db);
				return Ok(MemoTransService.GetMemoTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/MemoTrans/CreateMemoTrans")]
		public ActionResult CreateMemo([FromBody] MemoTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
					return Ok(MemoTransService.CreateMemoTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/MemoTrans/UpdateMemoTrans")]
		public ActionResult UpdateMemo([FromBody] MemoTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
					return Ok(MemoTransService.UpdateMemoTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/MemoTrans/DeleteMemoTrans")]
		public ActionResult DeleteMemo([FromBody] RowIdentity parm)
		{
			try
			{
				IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
				return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
		public ActionResult GetMemoTransInitialData(string id)
		{
			try
			{
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db, SysTransactionLogService);
				return Ok(relatedPersonTableService.GetMemoTransInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Memo

		#region AddressTrans
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetAddressTransList/ByCompany")]
		public ActionResult GetAddressTransListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAddressTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/AddressTrans/GetAddressTransById/id={id}")]
		public ActionResult GetAddressTransById(string id)
		{
			try
			{
				IAddressTransService AddressTransService = new AddressTransService(db);
				return Ok(AddressTransService.GetAddressTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/CreateAddressTrans")]
		public ActionResult CreateAddressTrans([FromBody] AddressTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressTransService AddressTransService = new AddressTransService(db, SysTransactionLogService);
					return Ok(AddressTransService.CreateAddressTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/UpdateAddressTrans")]
		public ActionResult UpdateAddressTrans([FromBody] AddressTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressTransService AddressTransService = new AddressTransService(db, SysTransactionLogService);
					return Ok(AddressTransService.UpdateAddressTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/DeleteAddressTrans")]
		public ActionResult DeleteAddressTrans([FromBody] RowIdentity parm)
		{
			try
			{
				IAddressTransService AddressTransService = new AddressTransService(db, SysTransactionLogService);
				return Ok(AddressTransService.DeleteAddressTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("RelatedInfo/AddressTrans/GetAddressTransInitialData/id={id}")]
		public ActionResult GetAddressTransInitialData(string id)
		{
			try
			{
				IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
				return Ok(relatedPersonTableService.GetAddressTransInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetAddressCountryDropDown")]
		public ActionResult GetRelatedInfoAddressTransAddressCountryDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemAddressCountry(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetAddressDistrictDropDown")]
		public ActionResult GetRelatedInfoAddressTransAddressDistrictDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IAddressTransService addressTransService = new AddressTransService(db);
				return Ok(addressTransService.GetDropDownItemAddressDistrict(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetAddressPostalCodeDropDown")]
		public ActionResult GetRelatedInfoAddressTransAddressPostalCodeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IAddressTransService addressTransService = new AddressTransService(db);
				return Ok(addressTransService.GetDropDownItemAddressPostalCode(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetAddressProvinceDropDown")]
		public ActionResult GetRelatedInfoAddressTransAddressProvinceDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IAddressTransService addressTransService = new AddressTransService(db);
				return Ok(addressTransService.GetDropDownItemAddressProvince(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetAddressSubDistrictDropDown")]
		public ActionResult GetRelatedInfoAddressTransAddressSubDistrictDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IAddressTransService addressTransService = new AddressTransService(db);
				return Ok(addressTransService.GetDropDownItemAddressSubDistrict(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetOwnershipDropDown")]
		public ActionResult GetRelatedInfoAddressTransOwnershipDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemOwnership(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/AddressTrans/GetPropertyTypeDropDown")]
		public ActionResult GetRelatedInfoAddressTransPropertyTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemPropertyType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion AddressTrans
	}
}
