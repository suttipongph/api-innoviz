﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class VendGroupController : BaseControllerV2
	{

		public VendGroupController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetVendGroupList/ByCompany")]
		public ActionResult GetVendGroupListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetVendGroupListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetVendGroupById/id={id}")]
		public ActionResult GetVendGroupById(string id)
		{
			try
			{
				IVendGroupService vendGroupService = new VendGroupService(db);
				return Ok(vendGroupService.GetVendGroupById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateVendGroup")]
		public ActionResult CreateVendGroup([FromBody] VendGroupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IVendGroupService vendGroupService = new VendGroupService(db, SysTransactionLogService);
					return Ok(vendGroupService.CreateVendGroup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateVendGroup")]
		public ActionResult UpdateVendGroup([FromBody] VendGroupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IVendGroupService vendGroupService = new VendGroupService(db, SysTransactionLogService);
					return Ok(vendGroupService.UpdateVendGroup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteVendGroup")]
		public ActionResult DeleteVendGroup([FromBody] RowIdentity parm)
		{
			try
			{
				IVendGroupService vendGroupService = new VendGroupService(db, SysTransactionLogService);
				return Ok(vendGroupService.DeleteVendGroup(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
