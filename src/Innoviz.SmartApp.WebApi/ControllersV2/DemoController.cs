﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class DemoController : BaseControllerV2
    {
        public DemoController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
        [HttpGet]
        [Route("GetDemoById/id={id}")]
        public ActionResult GetBlacklistStatusById(string id)
        {
            try
            {
                IDemoService demoService = new DemoService(db);
                DemoItemView demoItemView = new DemoItemView()
                {
                    DemoGUID = new Guid("F6FA0182-6B17-4C1C-83E2-80311FE086A0").ToString(),
                    DemoID = "DemoID001",
                    Description = "Description001",
                    DocumentProcessGUID = new Guid("00000096-0000-0000-0000-000000000060").ToString(),
                    DocumentStatusGUID = new Guid("00000098-0000-0000-0000-000000060100").ToString(),
                    EmployeeTableGUID = new Guid("1051E995-9432-4A97-8700-6A0B9D7D3C9F").ToString(),
                    DemoDate = "15/05/2020",
                    DemoAmount = 300.15m,
                    DemoPeriod = 250,
                    DemoFact = true
                };
                return Ok(demoService.GetTimeTest( demoItemView));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        [HttpPost]
        [Route("GetDocumentProcessDropDown")]
        public ActionResult GetDocumentProcessDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IDemoService demoService = new DemoService(db);
                return Ok(demoService.GetDocumentProcessDropdown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetDocumentStatusDocumentProcessDropdown")]
        public ActionResult GetDocumentStatusDocumentProcess([FromBody] SearchParameter search)
        {
            try
            {
                IDemoService demoService = new DemoService(db);
                return Ok(demoService.GetDocumentStatusByProcess(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetEmployeeFilterActiveStatusDropdown")]
        public ActionResult GetEmployeeFilterActiveStatus([FromBody] SearchParameter search)
        {
            try
            {
                IDemoService demoService = new DemoService(db);
                return Ok(demoService.GetEmployeeFilterActiveStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("UpdateDemo")]
        public ActionResult UpdateDemo([FromBody] DemoItemView vwModel)
        {
            if (ModelState.IsValid)
            {
                IDemoService demoService = new DemoService(db);
                return Ok(demoService.SetTimeTest(vwModel));
                //DemoItemView demoItemView = new DemoItemView()
                //{
                //    DemoGUID = new Guid("F6FA0182-6B17-4C1C-83E2-80311FE086A0").ToString(),
                //    DemoID = "DemoID001",
                //    Description = "Description001",
                //    DocumentProcessGUID = new Guid("00000096-0000-0000-0000-000000000060").ToString(),
                //    DocumentStatusGUID = new Guid("00000098-0000-0000-0000-000000060100").ToString(),
                //    EmployeeTableGUID = new Guid("1051E995-9432-4A97-8700-6A0B9D7D3C9F").ToString(),
                //    DemoDate = "15/05/2020",
                //    DemoAmount = 300.15m,
                //    DemoPeriod = 250,
                //    DemoFact = true,
                //    DemoDocument = vwModel.DemoDocument
                //};
                // return Ok(demoItemView);
            }
            else
            {
                throw new Exception();
            }
        }
        #region workflow
        #region item view
        [HttpPost]
        [Route("GetTaskBySerialNumber")]
        public async Task<ActionResult> GetTaskBySerialNumber([FromBody] WorkflowInstance workflowInstance)
        {
            try
            {
                IK2Service k2Service = new K2Service(db);
                return Ok(await k2Service.GetTaskBySerialNumber(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region action history
        [HttpGet]
        [Route("Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        public ActionResult GetActionHistoryByRefGUID(string refGUID)
        {
            try
            {
                IActionHistoryService actionHistoryService = new ActionHistoryService(db);
                return Ok(actionHistoryService.GetActionHistoryByRefGUID(refGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region workflow component
        [HttpPost]
        [Route("Workflow/StartWorkflow/GetTaskBySerialNumber")]
        public async Task<ActionResult> StartWorkflowGetTaskBySerialNumber([FromBody] WorkflowInstance workflowInstance)
        {
            try
            {
                IK2Service k2Service = new K2Service(db);
                return Ok(await k2Service.GetTaskBySerialNumber(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Workflow/StartWorkflow/GetStartWorkflowInitialData/id={id}")]
        public ActionResult GetStartWorkflowInitialData(string id)
        {
            try
            {
                DemoWorkflowParamView result = new DemoWorkflowParamView
                {
                    DemoGUID = id,
                    DemoId = "DemoID001",
                    RowData = "byteArrayToStringFromDatabase"
                };
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Workflow/StartWorkflow")]
        public async Task<ActionResult> StartWorkflow([FromBody]WorkflowInstance workflowInstance)
        {
            try
            {
                IK2Service k2Service = new K2Service(db, SysTransactionLogService);
                return Ok(await k2Service.StartWorkflow(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        public async Task<ActionResult> ActionWorkflowGetTaskBySerialNumber([FromBody] WorkflowInstance workflowInstance)
        {
            try
            {
                IK2Service k2Service = new K2Service(db);
                return Ok(await k2Service.GetTaskBySerialNumber(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Workflow/ActionWorkflow/GetActionWorkflowInitialData/id={id}")]
        public ActionResult GetActionWorkflowInitialData(string id)
        {
            try
            {
                DemoWorkflowParamView result = new DemoWorkflowParamView
                {
                    DemoGUID = id,
                    DemoId = "DemoID001",
                    RowData = "byteArrayToStringFromDatabase"
                };
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Workflow/ActionWorkflow")]
        public async Task<ActionResult> ActionWorkflow([FromBody] WorkflowInstance workflowInstance)
        {
            try
            {
                IK2Service k2Service = new K2Service(db, SysTransactionLogService);
                return Ok(await k2Service.ActionWorkflow(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        // case call function then workflow
        [HttpPost]
        [Route("Workflow/ActionCallFunctionThenK2/GetTaskBySerialNumber")]
        public async Task<ActionResult> ActionCallFunctionThenK2GetTaskBySerialNumber([FromBody] WorkflowInstance workflowInstance)
        {
            try
            {
                IK2Service k2Service = new K2Service(db);
                return Ok(await k2Service.GetTaskBySerialNumber(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Workflow/ActionCallFunctionThenK2/GetActionCallFunctionThenK2InitialData/id={id}")]
        public ActionResult GetActionCallFunctionThenK2InitialData(string id)
        {
            try
            {
                DemoWorkflowParamView result = new DemoWorkflowParamView
                {
                    DemoGUID = id,
                    DemoId = "DemoID001",
                    RowData = "byteArrayToStringFromDatabase"
                };
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Workflow/ActionCallFunctionThenK2")]
        public async Task<ActionResult> ActionCallFunctionThenK2([FromBody] DemoFunctionParm parm)
        {
            try
            {
                IDemoService demoService = new DemoService(db, SysTransactionLogService);
                return Ok(await demoService.CallFunctionThenActionK2(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #endregion

        #region test print bookmark document
        [HttpGet]
        [Route("GetBookmarkDocFileSingle")]
        public async Task<ActionResult> GetBookmarkDocFileSingle()
        {
            try
            {
                PrintBookDocTransParm printBookDocTransParm = new PrintBookDocTransParm()
                {
                    // input DocumentTemplateTableGUID
                    DocumentTemplateTableGUID = "00000000-0000-0000-0000-000000000000",
                    // input key for query
                    RefGUID = "00000000-0000-0000-0000-000000000000",
                    // input BookmarkDocumentTransGUID
                    BookmarkDocumentTransGUID = "00000000-0000-0000-0000-000000000000"
                };

                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetBookmarkDocFileMultiple")]
        public async Task<ActionResult> GetBookmarkDocFileMultiple()
        {
            try
            {
                PrintSetBookDocTransParm printSetBookDocTransParm = new PrintSetBookDocTransParm()
                {
                    // input key for query
                    RefGUID = "00000000-0000-0000-0000-000000000000",
                    // input RefType
                    RefType = (int)RefType.GuarantorAgreement
                };

                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(await bookmarkDocumentTransService.GetDocumentTemplateByRefGUID(printSetBookDocTransParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region report
        [HttpPost]
        [Route("Report/DemoReportDemo/RenderReport")]
        public ActionResult TestReport([FromBody]RptDemoReportView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Report/DemoReportDemo/GetCustomerTableDropdown")]
        public ActionResult GetCustomerTableDropdownTestReport([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropdownService = new SysDropDownService(db);
                return Ok(sysDropdownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }

}
