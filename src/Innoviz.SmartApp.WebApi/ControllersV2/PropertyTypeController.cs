using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class PropertyTypeController : BaseControllerV2
	{

		public PropertyTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetPropertyTypeList/ByCompany")]
		public ActionResult GetPropertyTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetPropertyTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetPropertyTypeById/id={id}")]
		public ActionResult GetPropertyTypeById(string id)
		{
			try
			{
				IPropertyTypeService propertyTypeService = new PropertyTypeService(db);
				return Ok(propertyTypeService.GetPropertyTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreatePropertyType")]
		public ActionResult CreatePropertyType([FromBody] PropertyTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IPropertyTypeService propertyTypeService = new PropertyTypeService(db, SysTransactionLogService);
					return Ok(propertyTypeService.CreatePropertyType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdatePropertyType")]
		public ActionResult UpdatePropertyType([FromBody] PropertyTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IPropertyTypeService propertyTypeService = new PropertyTypeService(db, SysTransactionLogService);
					return Ok(propertyTypeService.UpdatePropertyType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeletePropertyType")]
		public ActionResult DeletePropertyType([FromBody] RowIdentity parm)
		{
			try
			{
				IPropertyTypeService propertyTypeService = new PropertyTypeService(db, SysTransactionLogService);
				return Ok(propertyTypeService.DeletePropertyType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
