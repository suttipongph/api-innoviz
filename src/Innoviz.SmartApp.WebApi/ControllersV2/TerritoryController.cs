﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class TerritoryController : BaseControllerV2
    {
        public TerritoryController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
        [HttpPost]
        [Route("GetTerritoryList/ByCompany")]
        public ActionResult GetTerritoryListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetTerritoryListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetTerritoryById/id={id}")]
        public ActionResult GetTerritoryById(string id)
        {
            try
            {
                ITerritoryService territoryService = new TerritoryService(db);
                return Ok(territoryService.GetTerritoryById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateTerritory")]
        public ActionResult CreateTerritory([FromBody] TerritoryItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ITerritoryService territoryService = new TerritoryService(db, SysTransactionLogService);
                    return Ok(territoryService.CreateTerritory(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateTerritory")]
        public ActionResult UpdateTerritory([FromBody] TerritoryItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ITerritoryService territoryService = new TerritoryService(db, SysTransactionLogService);
                    return Ok(territoryService.UpdateTerritory(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteTerritory")]
        public ActionResult DeleteTerritory([FromBody] RowIdentity parm)
        {
            try
            {
                ITerritoryService territoryService = new TerritoryService(db, SysTransactionLogService);
                return Ok(territoryService.DeleteTerritory(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        #endregion
    }
}
