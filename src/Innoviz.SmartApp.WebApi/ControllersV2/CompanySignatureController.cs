﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class CompanySignatureController : BaseControllerV2
    {
        public CompanySignatureController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
		[HttpPost]
		[Route("GetCompanySignatureList/ByCompany")]
		public ActionResult GetCompanySignatureListByCompany([FromBody] SearchParameter search)
		
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetCompanySignatureListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetCompanySignatureById/id={id}")]
		public ActionResult GetCompanySignatureById(string id)
		{
			try
			{
				ICompanySignatureService companySignatureService = new CompanySignatureService(db);
				return Ok(companySignatureService.GetCompanySignatureById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateCompanySignature")]
		public ActionResult CreateCompanySignature([FromBody] CompanySignatureItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICompanySignatureService companySignatureService = new CompanySignatureService(db);
					return Ok(companySignatureService.CreateCompanySignature(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("UpdateCompanySignature")]
		public ActionResult UpdateCompanySignature([FromBody] CompanySignatureItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICompanySignatureService companySignatureService = new CompanySignatureService(db, SysTransactionLogService);
					return Ok(companySignatureService.UpdateCompanySignature(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteCompanySignature")]
		public ActionResult DeleteCompanySignature([FromBody] RowIdentity parm)
		{
			try
			{
				ICompanySignatureService companySignatureService = new CompanySignatureService(db, SysTransactionLogService);
				return Ok(companySignatureService.DeleteCompanySignature(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetMaxOrderingByRefTypeAndBranch/{refType}/{branch}")]
		public ActionResult GetMaxOrderingByRefTypeAndBranch(int refType, string branch)
		{
			try
			{
				ICompanySignatureService companySignatureService = new CompanySignatureService(db, SysTransactionLogService);
                return Ok(companySignatureService.Getmaxorderingbyreftypeandbranch(refType, branch));
                //return Ok(null);
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("GetInitialDataBranch")]
		public ActionResult GetInitialDataBranch()
		{
			try
			{
				ICompanySignatureService companySignatureService = new CompanySignatureService(db);
				return Ok(companySignatureService.GetInitialDataBranch());
				//return Ok(null);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown
		[HttpPost]
		[Route("GetBranchDropDown")]
		public ActionResult GetBranchDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBranch(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetEmployeeFilterActiveStatusDropdown")]
		public ActionResult GetActiveEmployeeTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IEmployeeTableService employeeTableService = new EmployeeTableService(db);
				return Ok(employeeTableService.GetActiveEmployeeTableDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
