using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.InterfaceWithOtherSystems;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class CustomerRefundTableController : BaseControllerV2
	{
		private readonly InterfaceAXDbContext axContext;

		public CustomerRefundTableController(SmartAppContext context, ISysTransactionLogService transactionLogService, InterfaceAXDbContext axContext)
			: base(context, transactionLogService)
		{
			this.axContext = axContext;
		}
		[HttpPost]
		[Route("GetCustomerRefundTableList/ByCompany")]
		public ActionResult GetCustomerRefundTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetCustomerRefundTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetCustomerRefundTableById/id={id}")]
		[Route("RelatedInfo/InvoiceSettlement/GetCustomerRefundTableById/id={id}")]
		public ActionResult GetCustomerRefundTableById(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				return Ok(customerRefundTableService.GetCustomerRefundTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateCustomerRefundTable")]
		public ActionResult CreateCustomerRefundTable([FromBody] CustomerRefundTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db, SysTransactionLogService);
					return Ok(customerRefundTableService.CreateCustomerRefundTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateCustomerRefundTable")]
		public ActionResult UpdateCustomerRefundTable([FromBody] CustomerRefundTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db, SysTransactionLogService);
					return Ok(customerRefundTableService.UpdateCustomerRefundTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteCustomerRefundTable")]
		public ActionResult DeleteCustomerRefundTable([FromBody] RowIdentity parm)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db, SysTransactionLogService);
				return Ok(customerRefundTableService.DeleteCustomerRefundTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        [HttpPost]
        [Route("GetInvoiceSettlementDetailList/ByCompany")]
        public ActionResult GetInvoiceSettlementDetailList([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceSettlementDetailListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
		[Route("GetCustomerRefundTableInitialData/companyId={companyId}/suspenseInvoiceType={suspenseInvoiceType}")]
		public ActionResult GetCustomerRefundTableInitialData(string companyId, int suspenseInvoiceType)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				return Ok(customerRefundTableService.GetCustomerRefundTableInitialData(companyId, suspenseInvoiceType));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("ValidateIsManualNumberSeqCustomerRefundTable/companyId={companyId}/suspenseInvoiceType={suspenseInvoiceType}")]
		public ActionResult ValidateIsManualNumberSeqCustomerRefundTable(string companyId,int suspenseInvoiceType)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				return Ok(customerRefundTableService.IsManualByCustomerRefundTable(companyId, customerRefundTableService.GetReferenceIdBySuspenseInvoiceType(suspenseInvoiceType)));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetExChangeRate")]
		public ActionResult GetExChangeRate([FromBody] CustomerRefundTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db, SysTransactionLogService);
					return Ok(customerRefundTableService.GetExChangeRate(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetCustomerRefundTableAccessMode/id={id}")]
		public ActionResult GetCustomerRefundTableAccessMode(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTable = new CustomerRefundTableService(db);
				return Ok(customerRefundTable.GetCustomerRefundTableAccessMode(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetRetentionConditionTransByCustomerRefund/id={id}")]
		public ActionResult GetRetentionConditionTransByCustomerRefund(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTable = new CustomerRefundTableService(db);
				return Ok(customerRefundTable.GetRetentionConditionTransByCustomerRefund(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("ValidateIsNotInvoiceSettlementDetail/id={id}")]
		public ActionResult ValidateIsNotInvoiceSettlementDetail(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				return Ok(customerRefundTableService.ValidateIsNotInvoiceSettlementDetail(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown
		[HttpPost]
		[Route("GetCurrencyDropDown")]
		public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCurrency(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetCustomerTableDropDown")]
		public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ICustomerRefundTableService customerRefundTable = new CustomerRefundTableService(db);
				return Ok(customerRefundTable.GetDropDownItemCustomerRefundStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetEmployeeFilterActiveStatusDropdown")]
		public ActionResult GetActiveEmployeeTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IEmployeeTableService employeeTableService = new EmployeeTableService(db);
				return Ok(employeeTableService.GetActiveEmployeeTableDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetLedgerDimensionDropDown")]
		public ActionResult GetLedgerDimensionDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("getCreditAppTableByCustomerRefundTableDropDown")]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
		public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
				return Ok(creditAppTableService.GetDropDownItemCreditAppTableByCustomerRefundTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#region function 

		#region cancel customerRefund
		[HttpGet]
		[Route("Function/CancelCustomerRefund/GetCancelCustomerRefundById/id={id}")]
		public ActionResult GetCancelCustomerRefundById(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				return Ok(customerRefundTableService.GetCancelCustomerRefundTableById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("Function/CancelCustomerRefund/CancelCustomerRefund")]
		public ActionResult CancelWithdrawal([FromBody] CancelCustomerRefundView view)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				return Ok(customerRefundTableService.CancelCustomerRefundTable(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Function/CancelCustomerRefund/GetDocumentReasonDropDown")]
		public ActionResult GetDocumentReasonDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region post customerRefund
		[HttpGet]
		[Route("Function/PostCustomerRefund/GetPostCustomerRefundById/id={id}")]
		public ActionResult GetPostCustomerRefundById(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				return Ok(customerRefundTableService.GetPostCustomerRefundTableById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("Function/PostCustomerRefund/PostCustomerRefund")]
		public ActionResult PostCustomerRefund([FromBody] PostCustomerRefundView view)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				return Ok(customerRefundTableService.PostCustomerRefundTable(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion

		#region InvoiceSettlement
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetInvoiceSettlementDetailList/ByCompany")]
		public ActionResult GetInvoiceSettlementDetailListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceSettlementDetailListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetListInvoiceOutstandingList/ByCompany")]
		[Route("GetListInvoiceOutstandingList/ByCompany")]
		public ActionResult GetInvoiceOutstandingListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceOutstandingListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/InvoiceSettlement/GetInvoiceSettlementDetailById/id={id}")]
		[Route("GetInvoiceSettlementDetailById/id={id}")]
		public ActionResult GetInvoiceSettlementDetailById(string id)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.GetInvoiceSettlementDetailById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/CreateInvoiceSettlementDetail")]
		[Route("CreateInvoiceSettlementDetail")]
		public ActionResult CreateInvoiceSettlementDetail([FromBody] InvoiceSettlementDetailItemView vwModel)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.CreateInvoiceSettlementDetail(vwModel));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/UpdateInvoiceSettlementDetail")]
		[Route("UpdateInvoiceSettlementDetail")]
		public ActionResult UpdateInvoiceSettlementDetail([FromBody] InvoiceSettlementDetailItemView vwModel)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.UpdateInvoiceSettlementDetail(vwModel));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/DeleteInvoiceSettlementDetail")]
		[Route("DeleteInvoiceSettlementDetail")]
		public ActionResult DeletePInvoiceSettlementDetail([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.DeleteInvoiceSettlementDetail(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetInvoiceSettlementDetailInitialData")]
		[Route("GetInvoiceSettlementDetailInitialData")]
		public ActionResult GetInvoiceSettlementDetailInitialData([FromBody] InvoiceSettlementDetailItemView vwModel)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.GetInvoiceSettlementDetailInitialData(vwModel));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/InvoiceSettlement/GetCustomerRefundTableAccessMode/id={id}")]
		public ActionResult GetInvoiceSettlementCustomerRefundTableAccessMode(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTable = new CustomerRefundTableService(db);
				return Ok(customerRefundTable.GetCustomerRefundTableAccessMode(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetCalcSettlePurchaseAmount")]
		[Route("GetCalcSettlePurchaseAmount")]
		public ActionResult GetCalcSettlePurchaseAmount([FromBody] InvoiceSettlementDetailItemView vwModel)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.CalcSettlePurchaseAmount(vwModel.InvoiceTableGUID, vwModel.SettleAmount));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetCalcSettleInvoiceAmount/EditWHT={editWHT}")]
		[Route("GetCalcSettleInvoiceAmount/EditWHT={editWHT}")]
		public ActionResult GetCalcSettleInvoiceAmountEditWHT([FromBody] InvoiceSettlementDetailItemView vwModel, bool editWHT)
		{
			try
			{
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				return Ok(invoiceSettlementDetailService.GetCalcSettleInvoiceAmount(vwModel, editWHT));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetInvoiceTableDropDown")]
		[Route("GetInvoiceTableDropDown")]
		public ActionResult GetInvoiceSettlementInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetInvoiceTypeDropDown")]
		[Route("GetInvoiceTypeDropDown")]
		public ActionResult GetInvoiceSettlementInvoiceTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetBuyerInvoiceTableDropDown")]
		[Route("GetBuyerInvoiceTableDropDown")]
		public ActionResult GetInvoiceSettlementBuyerInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetBuyerAgreementTableDropDown")]
		[Route("GetBuyerAgreementTableDropDown")]
		public ActionResult GetInvoiceSettlementBuyerAgreementTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/InvoiceSettlement/GetBuyerTableDropDown")]
		[Route("GetBuyerTableDropDown")]
		public ActionResult GetInvoiceSettlementBuyerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Dropdown
		#endregion InvoiceSettlement

		#region MemoTrans
		[HttpPost]
		[Route("RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
		public ActionResult GetMemoListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetMemoTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
		public ActionResult GetMemoById(string id)
		{
			try
			{
				IMemoTransService MemoTransService = new MemoTransService(db);
				return Ok(MemoTransService.GetMemoTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/MemoTrans/CreateMemoTrans")]
		public ActionResult CreateMemo([FromBody] MemoTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
					return Ok(MemoTransService.CreateMemoTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/MemoTrans/UpdateMemoTrans")]
		public ActionResult UpdateMemo([FromBody] MemoTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
					return Ok(MemoTransService.UpdateMemoTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/MemoTrans/DeleteMemoTrans")]
		public ActionResult DeleteMemo([FromBody] RowIdentity parm)
		{
			try
			{
				IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
				return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
		public ActionResult GetMemoTransInitialData(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				return Ok(customerRefundTableService.GetMemoTransInitialData(id ));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#region PaymentDetail
		[HttpPost]
		[Route("RelatedInfo/PaymentDetail/GetPaymentDetailList/ByCompany")]
		public ActionResult GetPaymentDetailListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetPaymentDetailListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/PaymentDetail/GetPaymentDetailById/id={id}")]
		public ActionResult GetPaymentDetailById(string id)
		{
			try
			{
				IPaymentDetailService paymentDetailService = new PaymentDetailService(db);
				return Ok(paymentDetailService.GetPaymentDetailById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/PaymentDetail/CreatePaymentDetail")]
		public ActionResult CreatePaymentDetail([FromBody] PaymentDetailItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IPaymentDetailService paymentDetailService = new PaymentDetailService(db, SysTransactionLogService);
					return Ok(paymentDetailService.CreatePaymentDetail(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/PaymentDetail/UpdatePaymentDetail")]
		public ActionResult UpdatePaymentDetail([FromBody] PaymentDetailItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IPaymentDetailService paymentDetailService = new PaymentDetailService(db, SysTransactionLogService);
					return Ok(paymentDetailService.UpdatePaymentDetail(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/PaymentDetail/DeletePaymentDetail")]
		public ActionResult DeletePaymentDetail([FromBody] RowIdentity parm)
		{
			try
			{
				IPaymentDetailService paymentDetailService = new PaymentDetailService(db, SysTransactionLogService);
				return Ok(paymentDetailService.DeletePaymentDetail(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/PaymentDetail/GetPaymentDetailInitialData/id={id}")]
		public ActionResult GetPaymentDetailInitialData(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTable = new CustomerRefundTableService(db);
				return Ok(customerRefundTable.GetPaymentDetailInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/PaymentDetail/GetCustomerRefundTableAccessMode/id={id}")]
		public ActionResult GetCustomerRefundTableAccessModeByPaymnetDetail(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTable = new CustomerRefundTableService(db);
				return Ok(customerRefundTable.GetCustomerRefundTableAccessMode(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		[HttpPost]
		[Route("RelatedInfo/PaymentDetail/GetCustomerTableDropDown")]
		public ActionResult GetCustomerTableDropDownByPaymentDetail([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/PaymentDetail/GetVendorTableDropDown")]
		public ActionResult GetVendorTableDropDownByPaymentDetail([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemVendorTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/PaymentDetail/GetInvoiceTypeDropDown")]
		public ActionResult GetInvoiceTypeDropDownByPaymentDetail([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTypeService invoiceTypeService = new InvoiceTypeService(db);
				return Ok(invoiceTypeService.GetInvoiceTypeByPaymentDetail(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion PaymentDetail

		#region ServiceFeeTrans
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
		public ActionResult GetServiceFeeTransListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetServiceFeeTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
		public ActionResult GetServiceFeeTransById(string id)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				return Ok(serviceFeeTransService.GetServiceFeeTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
		public ActionResult CreateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
					return Ok(serviceFeeTransService.CreateServiceFeeTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
		public ActionResult UpdateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
					return Ok(serviceFeeTransService.UpdateServiceFeeTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
		public ActionResult DeleteServiceFeeTrans([FromBody] RowIdentity parm)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
				return Ok(serviceFeeTransService.DeleteServiceFeeTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
		public ActionResult GetServiceFeeTransInitialData(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTable = new CustomerRefundTableService(db);
				return Ok(customerRefundTable.GetServiceFeeTransInitialDataByCustomerRefundTable(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/GetCustomerRefundTableAccessMode/id={id}")]
		public ActionResult GetCustomerRefundTableAccessModeByServiceFeeTrans(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTable = new CustomerRefundTableService(db);
				return Ok(customerRefundTable.GetCustomerRefundTableAccessMode(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetCalculateField")]
		public ActionResult GetCalculateField([FromBody] ServiceFeeTransItemView vwModel)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				return Ok(serviceFeeTransService.GetCalculateField(vwModel));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
		public ActionResult ValidateServiceFeeTrans([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				return Ok(serviceFeeTransService.ValidateServiceFeeTrans(getTaxValueAndWHTValueParm));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetTaxValue")]
		public ActionResult GetTaxValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				return Ok(serviceFeeTransService.GetTaxValue(getTaxValueAndWHTValueParm));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetWHTValue")]
		public ActionResult GetWHTValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				return Ok(serviceFeeTransService.GetWHTValue(getTaxValueAndWHTValueParm));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeByProductTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
				return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeByProductTypeAndDefaultNone(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
		public ActionResult GetLedgerDimensionDropDownByServiceFeeTrans([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
		public ActionResult GetTaxTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
		public ActionResult GetWithholdingTaxTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
		public ActionResult GetDocumentReasonDropDownByServiceFeeTrans([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
		public ActionResult GetOriginalInvoiceByServiceFeeTransDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetOriginalInvoiceByServiceFeeTransDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion ServiceFeeTrans

		#region InvoiceTable
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
		public ActionResult GetInvoiceLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
		public ActionResult GetInvoiceLineById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetInvoiceLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
		public ActionResult CreateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.CreateInvoiceLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
		public ActionResult UpdateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.UpdateInvoiceLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
		public ActionResult DeleteInvoiceLine([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
				return Ok(invoiceTableService.DeleteInvoiceLine(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
		public ActionResult UpdateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.UpdateInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
		public ActionResult DeleteInvoiceTable([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
				return Ok(invoiceTableService.DeleteInvoiceTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
		public ActionResult GetInvoiceTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
		public ActionResult GetInvoiceTableById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetInvoiceTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
		public ActionResult CreateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.CreateInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		[HttpPost]
		[Route("RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
		public ActionResult GetInvoiceTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
		public ActionResult GetBuyerInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
		public ActionResult GetCurrencyDropDownByInvoiceTable([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCurrency(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
		public ActionResult GetInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
		public ActionResult GetProdUnitTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
		public ActionResult GetTaxInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
		public ActionResult GetRefInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetRefInvoiceTableDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
		public ActionResult GetDocumentStatusByInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetReceiptTempTableDropDown")]
		public ActionResult GetReceiptTempTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemReceiptTempTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#endregion InvoiceTable

		#region Attachment
		[HttpPost]
		[Route("RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
		public ActionResult CreditAppTableGetAttachmentListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAttachmentListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/Attachment/GetAttachmentById/id={id}")]
		public async Task <ActionResult> CreditAppTableGetAttachmentById(string id)
		{
			try
			{
				IAttachmentService attachmentService = new AttachmentService(db);
				return Ok(await attachmentService.GetAttachmentById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/CreateAttachment")]
		public ActionResult CreditAppTableCreateAttachment([FromBody] AttachmentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
					return Ok(attachmentService.CreateAttachment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/UpdateAttachment")]
		public ActionResult CreditAppTableUpdateAttachment([FromBody] AttachmentItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
					return Ok(attachmentService.UpdateAttachment(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/DeleteAttachment")]
		public ActionResult CreditAppTableDeleteAttachment([FromBody] RowIdentity parm)
		{
			try
			{
				IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
				return Ok(attachmentService.DeleteAttachment(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/Attachment/GetAttachmentRefId")]
		public ActionResult CreditAppTableGetAttachmentRefId([FromBody] RefIdParm parm)
		{
			try
			{
				IAttachmentService attachmentService = new AttachmentService(db);
				return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Attachment
		#region report CustomerRefund
		[HttpGet]
		[Route("Report/printcustomerrefundreserve/GetPrintCustomerRefundReserveById/id={id}")]
		public ActionResult GetPrintCustomerRefundReserveById(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				return Ok(customerRefundTableService.GetPrintCustomerRefundReserveById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpGet]
		[Route("Report/printcustomerrefundretention/GetPrintCustomerRefundRetentionById/id={id}")]
		public ActionResult GetPrintCustomerRefundRetentionById(string id)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				return Ok(customerRefundTableService.GetPrintCustomerRefundRetentionById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("Report/printcustomerrefundreserve/RenderReport")]
		public ActionResult PrintCustomerRefund([FromBody] PrintCustomerReserveRefundReporttView model)
		{
			try
			{
				IReportViewerService reportViewerService = new ReportViewerService();
				return Ok(reportViewerService.RenderReport(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
		[HttpPost]
		[Route("Report/printcustomerrefundretention/RenderReport")]
		public ActionResult PrintCustomerRetentionRefund([FromBody] PrintCustomerRetentionRefundViewReport model)
		{
			try
			{
				IReportViewerService reportViewerService = new ReportViewerService();
				return Ok(reportViewerService.RenderReport(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region InvoiceTableDropdown
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
		public ActionResult GetCustomerTableServiceFeeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]

		public ActionResult GetBuyerTableServiceFeeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}


		[HttpPost]
		[Route("RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
		[Route("RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
		public ActionResult GetLedgerDimensionServiceFeeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetMethodOfPaymentDropDown")]

		public ActionResult GetMethodOfPaymentDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerAgreementTableDropDown")]
		public ActionResult GetBuyerAgreementTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetDocumentReasonDropDown")]
		public ActionResult GetDocumentReasonDropdown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetTaxTableDropDown")]
		public ActionResult GetTaxTableLinInvoiceDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetWithholdingTaxTableDropDown")]
		public ActionResult GetWithholdingTaxTableLineInvoiceDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion InvoiceTableDropDown
		#region VendorPaymentTrans
		[HttpPost]
		[Route("RelatedInfo/VendorPaymentTrans/GetVendorPaymentTransList/ByCompany")]
		public ActionResult GetVendorPaymentTransListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetVendorPaymentTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/VendorPaymentTrans/GetVendorPaymentTransById/id={id}")]
		public ActionResult GetVendorPaymentTransById(string id)
		{
			try
			{
				IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db);
				return Ok(vendorPaymentTransService.GetVendorPaymentTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/VendorPaymentTrans/CreateVendorPaymentTrans")]
		public ActionResult CreateVendorPaymentTrans([FromBody] VendorPaymentTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db, SysTransactionLogService);
					return Ok(vendorPaymentTransService.CreateVendorPaymentTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/VendorPaymentTrans/UpdateVendorPaymentTrans")]
		public ActionResult UpdateVendorPaymentTrans([FromBody] VendorPaymentTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db, SysTransactionLogService);
					return Ok(vendorPaymentTransService.UpdateVendorPaymentTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/VendorPaymentTrans/DeleteVendorPaymentTrans")]
		public ActionResult DeleteVendorPaymentTrans([FromBody] RowIdentity parm)
		{
			try
			{
				IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db, SysTransactionLogService);
				return Ok(vendorPaymentTransService.DeleteVendorPaymentTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		[HttpPost]
		[Route("RelatedInfo/VendorPaymentTrans/GetLedgerDimensionDropDown")]
		public ActionResult GetLedgerDimensionDropDownByVendorPaymentTrans([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/VendorPaymentTrans/GetTaxTableDropDown")]
		public ActionResult GetTaxTableDropDownByVendorPaymentTrans([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/VendorPaymentTrans/GetCreditAppTableDropDown")]
		public ActionResult GetCreditAppTableDropDownByVendorPaymentTrans([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/VendorPaymentTrans/GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusDropDownByVendorPaymentTrans([FromBody] SearchParameter search)
		{
			try
			{
				IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db);
				return Ok(vendorPaymentTransService.GetDropDownItemVendorPaymentTransStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/VendorPaymentTrans/GetVendorTableDropDown")]
		public ActionResult GetVendorTableDropDownByVendorPaymentTrans([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemVendorTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region VendorPaymentTrans function
		[HttpPost]
		[Route("RelatedInfo/VendorPaymentTrans/ValidateSendVendorInvoiceStagingMenuBtn")]
		public ActionResult ValidateSendVendorInvoiceStagingMenuBtn([FromBody] SendVendorInvoiceStagingParamView model)
		{
			try
			{
				IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db);
				return Ok(vendorPaymentTransService.ValidateSendVendorInvoiceStagingMenuBtn(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/VendorPaymentTrans/Function/SendVendorInvoiceStaging/GetSendVendorInvoiceStagingInitialData")]
		public ActionResult GetSendVendorInvoiceStagingInitialData([FromBody] SendVendorInvoiceStagingParamView model)
		{
			try
			{
				IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db);
				return Ok(vendorPaymentTransService.GetSendVendorInvoiceStagingInitialData(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/VendorPaymentTrans/Function/SendVendorInvoiceStaging")]
		public ActionResult SendVendorInvoiceStaging([FromBody] SendVendorInvoiceStagingParamView model)
		{
			try
			{
				List<DbContext> contexts = new List<DbContext>() { db, axContext };
				IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(contexts, SysTransactionLogService);
				return Ok(vendorPaymentTransService.SendVendorInvoiceStaging(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion VendorPaymentTrans
	}
}
