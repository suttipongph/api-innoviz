﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public partial class MainAgreementTableController : BaseControllerV2
    {
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableList/ByCompany")]

        public ActionResult GetMainAgreementTableListByCompanyByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMainAgreementTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableById/id={id}")]
        public ActionResult GetMainAgreementTableByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetMainAgreementTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/CreateMainAgreementTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/CreateMainAgreementTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/CreateMainAgreementTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/CreateMainAgreementTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/CreateMainAgreementTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/CreateMainAgreementTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/CreateMainAgreementTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/CreateMainAgreementTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/CreateMainAgreementTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/CreateMainAgreementTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/CreateMainAgreementTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/CreateMainAgreementTable")]
        public ActionResult CreateMainAgreementTableByNoticeOfCancellation([FromBody] MainAgreementTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                    return Ok(mainAgreementTableService.CreateMainAgreementTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/UpdateMainAgreementTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/UpdateMainAgreementTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/UpdateMainAgreementTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/UpdateMainAgreementTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/UpdateMainAgreementTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/UpdateMainAgreementTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/UpdateMainAgreementTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/UpdateMainAgreementTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/UpdateMainAgreementTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/UpdateMainAgreementTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/UpdateMainAgreementTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/UpdateMainAgreementTable")]


        public ActionResult UpdateMainAgreementTableByNoticeOfCancellation([FromBody] MainAgreementTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                    return Ok(mainAgreementTableService.UpdateMainAgreementTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/DeleteMainAgreementTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/DeleteMainAgreementTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/DeleteMainAgreementTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/DeleteMainAgreementTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/DeleteMainAgreementTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/DeleteMainAgreementTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/DeleteMainAgreementTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/DeleteMainAgreementTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/DeleteMainAgreementTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/DeleteMainAgreementTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/DeleteMainAgreementTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/DeleteMainAgreementTable")]


        public ActionResult DeleteMainAgreementTableByNoticeOfCancellation([FromBody] RowIdentity parm)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.DeleteMainAgreementTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerTableDropDown")]

        // noticeofcancellationmainagreementtable RelatedInfo > InvoiceTable RelatedInfo
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetDropDownItemByMainAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDownByMainAgreement")]

        public ActionResult GetCreditAppTableDropDownByMainAgreementByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetDropDownItemCreditAppTableByMainAgreement(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCreditAppTableDropDown")]

        // noticeofcancellationmainagreementtable RelatedInfo > InvoiceTable RelatedInfo
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCustomerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCustomerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCustomerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCustomerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCustomerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCustomerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCustomerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetCustomerTableDropDown")]
        // noticeofcancellationmainagreementtable RelatedInfo > InvoiceTable RelatedInfo
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetWithdrawalTableDropDownByMainAgreement")]

        public ActionResult GetWithdrawalTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetDropDownItemByMainAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetDocumentStatusDropDown")]

        public ActionResult GetDocumentStatusDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetDropDownItemMainTainAgreementStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqInternalMainAgreement/productType={productType}")]
        // Function Notice of cancelation main agreement
        [Route("Factoring/AllMainAgreementTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/productType={productType}")]
        public ActionResult ValidateIsManualNumberSeqInternalMainAgreementByNoticeOfCancellation(int productType)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.IsManualInternalMainAgreement(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/ValidateIsManualNumberSeqMainAgreement/productType={productType}")]
        public ActionResult ValidateIsManualNumberSeqMainAgreementByNoticeOfCancellation(int productType)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.IsManualMainAgreement(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetMainAgreementTableInitialData/productType={productType}")]

        public ActionResult GetMainAgreementTableInitialDataByNoticeOfCancellation(int productType)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetMainAgreementTableInitialData(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/GetBuyerAgreementTableByBuyerTable")]
        public ActionResult GetBuyerAgreementTableByBuyerTableByNoticeOfCancellation([FromBody] MainAgreementTableItemView model)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetBuyerAgreementTableByBuyerTable(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAccessModeByMainAgreementTable/id={id}")]

        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAccessModeByMainAgreementTable/id={id}")]

        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAccessModeByMainAgreementTable/id={id}")]

        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetAccessModeByMainAgreementTable/id={id}")]
        public ActionResult GetAccessModeByMainAgreementTableByNoticeOfCancellation(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByMainAgreementTable/id={id}")]
        public ActionResult GetAccessModeByMainAgreementTableByNoticeOfCancellationBookmarkDocumentTrans(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetAccessModeByStatusLessThanEqSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetVisibleManageMenu/id={id}")]
        public ActionResult GetVisibleManageMenuByNoticeOfCancellation(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);

                return Ok(mainAgreementTableService.GetVisibleManageMenu(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]

        public ActionResult GetDocumentReasonDropDownByManageAgreementByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Notice Of Cancellation
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/GetGenerateNoticeOfCancellationValidation")]
        [Route("Factoring/NewMainAgreementTable/GetGenerateNoticeOfCancellationValidation")]
        [Route("Bond/AllMainAgreementTable/GetGenerateNoticeOfCancellationValidation")]
        [Route("Bond/NewMainAgreementTable/GetGenerateNoticeOfCancellationValidation")]
        [Route("HirePurchase/AllMainAgreementTable/GetGenerateNoticeOfCancellationValidation")]
        [Route("HirePurchase/NewMainAgreementTable/GetGenerateNoticeOfCancellationValidation")]
        [Route("LCDLC/AllMainAgreementTable/GetGenerateNoticeOfCancellationValidation")]
        [Route("LCDLC/NewMainAgreementTable/GetGenerateNoticeOfCancellationValidation")]
        [Route("Leasing/AllMainAgreementTable/GetGenerateNoticeOfCancellationValidation")]
        [Route("Leasing/NewMainAgreementTable/GetGenerateNoticeOfCancellationValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/GetGenerateNoticeOfCancellationValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/GetGenerateNoticeOfCancellationValidation")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/GetGenerateNoticeOfCancellationValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetGenerateNoticeOfCancellationValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetGenerateNoticeOfCancellationValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetGenerateNoticeOfCancellationValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetGenerateNoticeOfCancellationValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetGenerateNoticeOfCancellationValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetGenerateNoticeOfCancellationValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetGenerateNoticeOfCancellationValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetGenerateNoticeOfCancellationValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetGenerateNoticeOfCancellationValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetGenerateNoticeOfCancellationValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetGenerateNoticeOfCancellationValidation")]
        public ActionResult GetGenerateNoticeOfCancellationValidation([FromBody] MainAgreementTableItemView view)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetGenerateNoticeOfCancellationValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/allmainagreementtable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("Bond/AllMainAgreementTable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("Bond/NewMainAgreementTable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("Factoring/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/Function/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/Function/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/Function/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/Function/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/GetGenMainAgmNoticeOfCancelById/id={id}")]
        public ActionResult GetGenMainAgmNoticeOfCancelById(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetGenMainAgmNoticeOfCancelById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("Factoring/NewMainAgreementTable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("Bond/AllMainAgreementTable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("Bond/NewMainAgreementTable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("HirePurchase/AllMainAgreementTable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("HirePurchase/NewMainAgreementTable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("LCDLC/AllMainAgreementTable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("LCDLC/NewMainAgreementTable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("Leasing/AllMainAgreementTable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("Leasing/NewMainAgreementTable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("ProjectFinance/AllMainAgreementTable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("ProjectFinance/NewMainAgreementTable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/noticeofcancellation/CreateGenMainAgmNoticeOfCancel")]
        public ActionResult CreateGenMainAgmNoticeOfCancel([FromBody] GenMainAgmNoticeOfCancelView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                    return Ok(mainAgreementTableService.CreateGenMainAgmNoticeOfCancel(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Function - ManageAgreement
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/ManageAgreement")]
        public ActionResult ManageAgreementByNoticeofcancellationmainagreementtable([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.ManageAgreement(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementValidation")]
        public ActionResult GetManageAgreementValidationByNoticeofcancellationmainagreementtable([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetManageAgreementByRefType")]
        public ActionResult GetManageAgreementByRefTypeByNoticeofcancellationmainagreementtable([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementByRefType(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/SignAgreement/GetDocumentReasonDropDown")]

        public ActionResult GetDocumentReasonDropDownByManageAgreementByNoticeofcancellationmainagreementtable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion Function - ManageAgreement
        #region Related info - NoticeOfCancel
        #region ServiceFeeTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        public ActionResult GetServiceFeeTransListByCompanyByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        public ActionResult GetServiceFeeTransByIdByRelatedInfoNoticeOfCancel(string id)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetServiceFeeTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        public ActionResult CreateServiceFeeTransByRelatedInfoNoticeOfCancel([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.CreateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        public ActionResult UpdateServiceFeeTransByRelatedInfoNoticeOfCancel([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.UpdateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        public ActionResult DeleteServiceFeeTransByRelatedInfoNoticeOfCancel([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                return Ok(ServiceFeeTransService.DeleteServiceFeeTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetAccessModeByMainAgreementTable/id={id}")]
        public ActionResult GetAccessModeServiceFeeTransByMainAgreementTableByRelatedInfoNoticeOfCancel(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        public ActionResult GetServiceFeeTransInitialDataByRelatedInfoNoticeOfCancel(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetServiceFeeTransInitialDataByMainAgreementTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]

        public ActionResult GetCalculateFieldByRelatedInfoNoticeOfCancel([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetCalculateField(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        public ActionResult ValidateServiceFeeTransByRelatedInfoNoticeOfCancel([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.ValidateServiceFeeTrans(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        public ActionResult GetTaxValueByRelatedInfoNoticeOfCancel([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetTaxValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        public ActionResult GetWHTValueByRelatedInfoNoticeOfCancel([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetWHTValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]

        public ActionResult GetInvoiceRevenueTypeByProductTypeDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeByProductTypeAndDefaultNone(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        // AddRelatedInfoInvoiceTable
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetLedgerDimensionDropDown")]

        public ActionResult GetLedgerDimensionDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        public ActionResult GetWithholdingTaxTableDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByServiceFeeTransByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        public ActionResult GetOriginalInvoiceByServiceFeeTransDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetOriginalInvoiceByServiceFeeTransDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #region Related info - service fee trans
        #region InvoiceTable
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        public ActionResult GetInvoiceTableListByCompanyByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        public ActionResult GetInvoiceTableByIdByRelatedInfoNoticeOfCancel(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        public ActionResult CreateInvoiceTableByRelatedInfoNoticeOfCancel([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceTable(vwModel));

                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        public ActionResult UpdateInvoiceTableByRelatedInfoNoticeOfCancel([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        public ActionResult DeleteInvoiceTableByRelatedInfoNoticeOfCancel([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        public ActionResult GetInvoiceLineListByCompanyByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        public ActionResult DeleteInvoiceLineByRelatedInfoNoticeOfCancel([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        public ActionResult CreateInvoiceLineByRelatedInfoNoticeOfCancel([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        public ActionResult UpdateInvoiceLineByRelatedInfoNoticeOfCancel([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        public ActionResult GetInvoiceLineByIdByRelatedInfoNoticeOfCancel(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDownInvoiceLineByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropdownInvoiceTable
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        public ActionResult GetInvoiceTypeDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        public ActionResult GetBuyerInvoiceTableDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByInvoiceTableByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        public ActionResult GetCurrencyDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCurrency(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        public ActionResult GetInvoiceTableDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        public ActionResult GetProdUnitTableDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        public ActionResult GetTaxInvoiceTableDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        public ActionResult GetRefInvoiceTableDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetRefInvoiceTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        public ActionResult GetDocumentStatusByInvoiceTableDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]

        public ActionResult GetWithholdingTaxTableDropDownByInvoiceTableByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropdownInvoiceTable
        #region Related info - invoice table
        #region TaxInvoice
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
        public ActionResult GetTaxInvoiceListByCompanyByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetTaxInvoiceTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
        public ActionResult GetTaxInvoiceTableByIdByRelatedInfoNoticeOfCancel(string id)
        {
            try
            {
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetTaxInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/InvoiceTaTaxInvoiceble/GetTaxInvoiceLineList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
        public ActionResult GetTaxInvoiceLineListByCompanyByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetTaxInvoiceLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
        public ActionResult GetTaxInvoiceLineByIdByRelatedInfoNoticeOfCancel(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetTaxInvoiceLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusTaxInvoiceDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetDropDownItemTaxInvoiceDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDownTaxInvoiceLineByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeDropDownTaxInvoiceLineByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Report
        [HttpGet]
        //[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
        public ActionResult GetPrintTaxInvoiceCopyByIdByRelatedInfoNoticeOfCancel(string id)
        {
            try
            {
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetPrintTaxInvoiceCopyById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        //[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
        public ActionResult PrintTaxInvoiceCopyByRelatedInfoNoticeOfCancel([FromBody] PrintTaxInvoiceReportView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
        public ActionResult GetPrintTaxInvoiceOriginalByIdByRelatedInfoNoticeOfCancel(string id)
        {
            try
            {
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                return Ok(taxInvoiceTableService.GetPrintTaxInvoiceOriginalById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        //[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
        public ActionResult PrintTaxInvoiceOriginalByRelatedInfoNoticeOfCancel([FromBody] PrintTaxInvoiceReportView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Report

        #endregion Tax Invoice
        #region payment history
        [HttpPost]
        //[Route("GetPaymentHistoryList/ByCompany")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
        public ActionResult GetPaymentHistoryListByCompanyByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                var result = sysListViewService.GetPaymentHistoryListvw(search);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("GetPaymentHistoryById/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
        public ActionResult GetPaymentHistoryByIdByRelatedInfoNoticeOfCancel(string id)
        {
            try
            {
                IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db);
                return Ok(paymentHistoryService.GetPaymentHistoryById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/CreatePaymentHistory")]
        public ActionResult CreatePaymentHistoryByRelatedInfoNoticeOfCancel([FromBody] PaymentHistoryItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db, SysTransactionLogService);
                    return Ok(paymentHistoryService.CreatePaymentHistory(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        //[Route("UpdatePaymentHistory")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/UpdatePaymentHistory")]
        public ActionResult UpdatePaymentHistoryByRelatedInfoNoticeOfCancel([FromBody] PaymentHistoryItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db, SysTransactionLogService);
                    return Ok(paymentHistoryService.UpdatePaymentHistory(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        //[Route("/PaymentHistory/DeletePaymentHistory")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/DeletePaymentHistory")]
        public ActionResult DeletePaymentHistoryByRelatedInfoNoticeOfCancel([FromBody] RowIdentity parm)
        {
            try
            {
                IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db, SysTransactionLogService);
                return Ok(paymentHistoryService.DeletePaymentHistory(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDownByPaymentHistoryByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
        public ActionResult GetInvoiceTableDropDownByPaymentHistoryByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDownByPaymentHistoryByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CustTrans
        [HttpPost]
        //[Route("RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransList/ByCompany")]
        public ActionResult GetCustTransListByCompanyByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        //[Route("RelatedInfo/CustTransaction/GetCustTransListByCustomer/ByCompany")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
        public ActionResult GetCustTransListByCustomerByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustTransById/id={id}")]
        public ActionResult GetCustTransByIdByRelatedInfoNoticeOfCancel(string id)
        {
            try
            {
                ICustTransService custTransService = new CustTransService(db);
                return Ok(custTransService.GetCustTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetCustomerTablebyCusttrans/id={id}")]
        public ActionResult GetCustomerTablebyCusttransByRelatedInfoNoticeOfCancel(string id)
        {
            try
            {
                ICustomerTableService customerTableService = new CustomerTableService(db);
                return Ok(customerTableService.GetCustomerTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        //[Route("RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
        public ActionResult GetInvoiceTablebyCusttransByRelatedInfoNoticeOfCancel(string id)
        {
            try
            {

                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetReceiptTempTableDropDown")]
        public ActionResult GetReceiptTempTableDropDownByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemReceiptTempTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        // InvoiceTable RelatedInfo
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]

        public ActionResult GetDocumentStatusDropDownInvoiceTableByRelatedInfoNoticeOfCancel([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Related info - invoice table
        #endregion InvoiceTable
        #endregion Related info - service fee trans
        #region Function
        #region Copy Service fee from CA
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCopyServiceFeeCondCARequestInitialData/id={id}")]
        public ActionResult GetCopyServiceFeeCondCARequestInitialDataByNoticeOfCancellation(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetCopyServiceFeeCondCARequestInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/GetCreditAppRequestTableByCreditAppTableDropDown")]
        public ActionResult GetCreditAppRequestTableByCreditAppTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetDropDownItemCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/NoticeOfCancellationMainagreementTable/RelatedInfo/servicefeetrans/Function/copyservicefeeconditiontransfromca/CreateCopyServiceFeeCondCARequest")]
        public ActionResult CreateCopyServiceFeeCondCARequestByNoticeOfCancellation([FromBody] MainAgreementTableItemView view)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.CreateCopyServiceFeeCondCARequest(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Copy Service fee from CA
        #endregion Function
        #endregion ServiceFeeTrans
        #region Memo
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult GetNoticeOfCancelMemoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetNoticeOfCancelMemoById(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateNoticeOfCancelMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateNoticeOfCancelMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult DeleteNoticeOfCancelMemo([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetNoticeOfCancelMemoTransInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Memo
        #region JointVentureTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        public ActionResult GetNoticeOfCancelJointVentureTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetJointVentureTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        public ActionResult GetNoticeOfCancelJointVentureTransById(string id)
        {
            try
            {
                IJointVentureTransService JointVentureTransService = new JointVentureTransService(db);
                return Ok(JointVentureTransService.GetJointVentureTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        public ActionResult CreateNoticeOfCancelJointVentureTrans([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.CreateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        public ActionResult UpdateNoticeOfCancelJointVentureTrans([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.UpdateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        public ActionResult DeleteNoticeOfCancelJointVentureTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                return Ok(JointVentureTransService.DeleteJointVentureTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        public ActionResult GetNoticeOfCancelJointVentureTransInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetJointVentureTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetNoticeOfCancelJointVentureTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion JointVentureTrans        
        #region ConsortiumTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        public ActionResult GetNoticeOfCancelConsortiumTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetConsortiumTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        public ActionResult GetNoticeOfCancelConsortiumTransById(string id)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db);
                return Ok(ConsortiumTransService.GetConsortiumTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        public ActionResult CreateNoticeOfCancelConsortiumTrans([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.CreateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        public ActionResult UpdateNoticeOfCancelConsortiumTrans([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.UpdateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        public ActionResult DeleteNoticeOfCancelConsortiumTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                return Ok(ConsortiumTransService.DeleteConsortiumTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        public ActionResult GetNoticeOfCancelConsortiumTransInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetConsortiumTransInitialDataByMainAgreementTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetNoticeOfCancelConsortiumTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        public ActionResult GetNoticeOfCancelConsortiumTransConsortiumLineDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IConsortiumTableService consortiumTableService = new ConsortiumTableService(db);
                return Ok(consortiumTableService.GetDropDownItemConsortiumLineByConsortiumTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion ConsortiumTrans
        #region BookmarkDocumentTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]

        public ActionResult GetNoticeOfCancelBookmarkDocumentTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBookmarkDocumentTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        public ActionResult GetNoticeOfCancelBookmarkDocumentTransById(string id)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(BookmarkDocumentTransService.GetBookmarkDocumentTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        public ActionResult CreateNoticeOfCancelBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.CreateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        public ActionResult UpdateNoticeOfCancelBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.UpdateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        public ActionResult DeleteNoticeOfCancelBookmarkDocumentTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                return Ok(BookmarkDocumentTransService.DeleteBookmarkDocumentTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        public ActionResult GetNoticeOfCancelBookmarkDocumentTransInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetBookmarkDocumentTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataGuarantorAgreement/id={id}")]
        public ActionResult GetNoticeOfCancelBookmarkDocumentTransInitialDataGuarantorAgreement(string id)
        {
            try
            {
                IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db, SysTransactionLogService);
                return Ok(guarantorAgreementTableService.GetBookmarkDocumentTransInitialDataByGuarantorAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        public ActionResult GetNoticeOfCancelBookmarkDocumentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
                return Ok(bookmarkDocumentService.GetDropDownItemBookmarkDocumentByMainAgreement(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        public ActionResult GetNoticeOfCancelDocumentTemplateTableDropDownByDocumentType([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
                return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        public ActionResult GetNoticeOfCancelDocumentTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        public ActionResult GetNoticeOfCancelBookmarkDocumentTransDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetDropDownItemBookMarkDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region PrintBookDocTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        public ActionResult GetNoticeOfCancelPrintBookDocTransValidation([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintBookDocTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        public async Task<ActionResult> PrintNoticeOfCancelBookmarkDocumentTransaction([FromBody] PrintBookDocTransParm printBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region copy bookmark document from template

        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Bond/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Bond/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        public ActionResult GetNoticeOfCancelBookmarkDocumentTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                return Ok(mainAgreementTableService.GetDropDownItemBookmarkDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Bond/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Bond/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetCopyBookmarkDocumentValidation")]
        public ActionResult GetNoticeOfCancelCopyBookmarkDocumentValidation([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.GetCopyBookmarkDocumentValidation(param));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Bond/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Bond/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("HirePurchase/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Leasing/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Lcdlc/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Projectfinance/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        public ActionResult CopyNoticeOfCancelBookmarkDocumentFromTemplate([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.CopyBookmarkDocumentFromTemplate(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion

        #endregion BookmarkDocumentTrans

        #region AgreementTableInfo
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        public ActionResult GetNoticeOfCancelAgreementTableInfoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAgreementTableInfoListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        public ActionResult GetNoticeOfCancelAgreementTableInfoById(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        public ActionResult CreateNoticeOfCancelAgreementTableInfo([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.CreateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        public ActionResult UpdateNoticeOfCancelAgreementTableInfo([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.UpdateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        public ActionResult DeleteNoticeOfCancelAgreementTableInfo([FromBody] RowIdentity parm)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                return Ok(AgreementTableInfoService.DeleteAgreementTableInfo(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Hire/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Hire/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetAgreementTableInfoByMainAgreement/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetAgreementTableInfoByMainAgreement/id={id}")]
        public ActionResult GetNoticeOfCancelAgreementTableInfoByMainAgreement(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoByMainAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        public ActionResult GetNoticeOfCancelAgreementTableInfoInitialData(string id)
        {
            try
            {
                IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db, SysTransactionLogService);
                return Ok(mainAgreementTableService.GetAgreementTableInfoInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        public ActionResult GetNoticeOfCancelAgreementTableInfoAddressTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressTransByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByAgreementTableInfoDropDown")]
        public ActionResult GetNoticeOfCancelAgreementTableInfoAuthorizedPersonTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(agreementTableInfoService.GetDropDownItemAuthorizedPersonTransByAgreementInfo(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        public ActionResult GetNoticeOfCancelAgreementTableInfoAuthorizedPersonTransByBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(authorizedPersonTransService.GetDropDownItemAuthorizedPersonTransByBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetNoticeOfCancelAgreementTableInfoAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        public ActionResult GetNoticeOfCancelAgreementTableInfoCompanyBankDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCompanyBank(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        public ActionResult GetNoticeOfCancelAgreementTableInfoCompanySignatureDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetEmployeeTableWithCompanySignatureDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        public ActionResult GetNoticeOfCancelAgreementTableInfoEmployeeTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion AgreementTableInfo

        #endregion Related info - NoticeOfCancel
        #endregion Notice Of Cancellation
        #region PrintSetBookDocTrans
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetPrintSetBookDocTransValidation")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetPrintSetBookDocTransValidation")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetPrintSetBookDocTransValidation")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetPrintSetBookDocTransValidation")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetPrintSetBookDocTransValidation")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetPrintSetBookDocTransValidation")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetPrintSetBookDocTransValidation")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetPrintSetBookDocTransValidation")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetPrintSetBookDocTransValidation")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetPrintSetBookDocTransValidation")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetPrintSetBookDocTransValidation")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/GetPrintSetBookDocTransValidation")]
        public ActionResult GetPrintSetBookDocTransValidationByNoticeOfCancellation([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintSetBookDocTransValidation(printSetBookDocTransParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Factoring/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/AllMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/NewMainAgreementTable/RelatedInfo/noticeofcancellationmainagreementtable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransactionByNoticeOfCancellation([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByRefGUID(printSetBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Attachment
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Bond/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Bond/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Hirepurchase/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Hirepurchase/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompanyByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Bond/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Bond/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Hirepurchase/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Hirepurchase/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/CreateAttachment")]

        [Route("Bond/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Bond/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/CreateAttachment")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/CreateAttachment")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/CreateAttachment")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/CreateAttachment")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/attachment/CreateAttachment")]
        public ActionResult CreateAttachmentByNoticeOfCancellation([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Bond/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Bond/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachmentByNoticeOfCancellation([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Bond/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Bond/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/DeleteAttachment")]
        public ActionResult DeleteAttachmentByNoticeOfCancellation([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Bond/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Bond/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("HirePurchase/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Leasing/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Lcdlc/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Projectfinance/allmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Projectfinance/newmainagreementtable/RelatedInfo/noticeofcancellationmainagreementtable/RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefIdByNoticeOfCancellation([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}