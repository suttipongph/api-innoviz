﻿using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class SysUserLogController : BaseControllerV2
    {
        public SysUserLogController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
        [HttpPost]
        [Route("GetSysUserLogList")]
        public async Task<ActionResult> GetSysUserLogList([FromBody] SearchParameter search)
        {
            try
            {
                ISysUserService sysUserService = new SysUserService(db);
                string token = GetToken();
                string reqId = GetRequestIdFromRequest();
                string userApiEndpoint = "UserApi:BaseUrl".GetConfigurationValue() + "UserApi:User".GetConfigurationValue();
                return Ok(await sysUserService.GetSysUserLogs(userApiEndpoint, token, reqId, search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Function/UserLogCleanUp")]
        public async Task<ActionResult> CleanUpSysUserLog([FromBody] SysUserLogCleanUpParm model)
        {
            try
            {
                ISysUserService sysUserService = new SysUserService(db);
                string token = GetToken();
                string reqId = GetRequestIdFromRequest();
                string userApiEndpoint = "UserApi:BaseUrl".GetConfigurationValue() + "UserApi:User".GetConfigurationValue();
                return Ok(await sysUserService.CleanUpUserLog(userApiEndpoint, token, reqId, model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Function/UserLogCleanUp/GetSysUserTableDropDown")]
        public ActionResult GetSysUserTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysUserService sysUserService = new SysUserService(db);
                return Ok(sysUserService.GetDropDownItemUserNameValue(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
    }

}
