using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class DocumentConditionTemplateTableController : BaseControllerV2
	{

		public DocumentConditionTemplateTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		#region DocumentConditionTemplateLine
		[HttpPost]
		[Route("GetDocumentConditionTemplateLineList/ByCompany")]
		public ActionResult GetDocumentConditionTemplateLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetDocumentConditionTemplateLineListvw(search));
				//return Ok(null);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetDocumentConditionTemplateLineById/id={id}")]
		public ActionResult GetDocumentConditionTemplateLineById(string id)
		{
			try
			{
				IDocumentConditionTemplateTableService documentConditionTemplateTableService = new DocumentConditionTemplateTableService(db);
				return Ok(documentConditionTemplateTableService.GetDocumentConditionTemplateLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateDocumentConditionTemplateLine")]
		public ActionResult CreateDocumentConditionTemplateLine([FromBody] DocumentConditionTemplateLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentConditionTemplateTableService documentConditionTemplateTableService = new DocumentConditionTemplateTableService(db, SysTransactionLogService);
					return Ok(documentConditionTemplateTableService.CreateDocumentConditionTemplateLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateDocumentConditionTemplateLine")]
		public ActionResult UpdateDocumentConditionTemplateLine([FromBody] DocumentConditionTemplateLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentConditionTemplateTableService documentConditionTemplateTableService = new DocumentConditionTemplateTableService(db, SysTransactionLogService);
					return Ok(documentConditionTemplateTableService.UpdateDocumentConditionTemplateLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetDocumentConditionTemplateInitialData/documentConditionTemplateTableGUID={documentConditionTemplateTableGUID}")]
		public ActionResult Getdocumentconditiontemplatelineinitialdata(string documentConditionTemplateTableGUID)
		{
			try
			{
				IDocumentConditionTemplateTableService documentConditionTemplateTableService = new DocumentConditionTemplateTableService(db);
				return Ok(documentConditionTemplateTableService.GetDocumentconditiontemplatelineInitialdata(documentConditionTemplateTableGUID));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteDocumentConditionTemplateLine")]
		public ActionResult DeleteDocumentConditionTemplateLine([FromBody] RowIdentity parm)
		{
			try
			{
				IDocumentConditionTemplateTableService documentConditionTemplateTableService = new DocumentConditionTemplateTableService(db, SysTransactionLogService);
				return Ok(documentConditionTemplateTableService.DeleteDocumentConditionTemplateLine(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region DocumentConditionTemplateTable
		[HttpPost]
		[Route("GetDocumentConditionTemplateTableList/ByCompany")]
		public ActionResult GetDocumentConditionTemplateTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetDocumentConditionTemplateTableListvw(search));
				//return Ok(null);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetDocumentConditionTemplateTableById/id={id}")]
		public ActionResult GetDocumentConditionTemplateTableById(string id)
		{
			try
			{
				IDocumentConditionTemplateTableService documentConditionTemplateTableService = new DocumentConditionTemplateTableService(db);
				return Ok(documentConditionTemplateTableService.GetDocumentConditionTemplateTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateDocumentConditionTemplateTable")]
		public ActionResult CreateDocumentConditionTemplateTable([FromBody] DocumentConditionTemplateTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentConditionTemplateTableService documentConditionTemplateTableService = new DocumentConditionTemplateTableService(db, SysTransactionLogService);
					return Ok(documentConditionTemplateTableService.CreateDocumentConditionTemplateTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateDocumentConditionTemplateTable")]
		public ActionResult UpdateDocumentConditionTemplateTable([FromBody] DocumentConditionTemplateTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentConditionTemplateTableService documentConditionTemplateTableService = new DocumentConditionTemplateTableService(db, SysTransactionLogService);
					return Ok(documentConditionTemplateTableService.UpdateDocumentConditionTemplateTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteDocumentConditionTemplateTable")]
		public ActionResult DeleteDocumentConditionTemplateTable([FromBody] RowIdentity parm)
		{
			try
			{
				IDocumentConditionTemplateTableService documentConditionTemplateTableService = new DocumentConditionTemplateTableService(db, SysTransactionLogService);
				return Ok(documentConditionTemplateTableService.DeleteDocumentConditionTemplateTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion
        #region Dropdown

        [HttpPost]
		[Route("GetDocumentConditionTemplateTableDropDown")]
		public ActionResult GetDocumentConditionTemplateTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentConditionTemplateTable(search));
				//return Ok(null);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetDocumentTypeDropDown")]
		public ActionResult GetDocumentTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
		[HttpGet]
		[Route("ValidateIsManualNumberSeq")]
		public ActionResult ValidateIsManualNumberSeq()
		{
			try
			{
				IDocumentConditionTemplateTableService documentConditionTemplateTable = new DocumentConditionTemplateTableService(db);
				return Ok(documentConditionTemplateTable.IsManualByDocumentConditionTemplateTable());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
