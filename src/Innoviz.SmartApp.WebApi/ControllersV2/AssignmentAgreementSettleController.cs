using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class AssignmentAgreementSettleController : BaseControllerV2
	{

		public AssignmentAgreementSettleController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		
		#region AgreementTableInfoSettlement
		[HttpPost]
		[Route("AssignmentAgreementSettlement/GetAssignmentAgreementSettleList/ByCompany")]
		[Route("AssignmentAgreement/all/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementSettleList/ByCompany")]
		[Route("All/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementSettleList/ByCompany")]
		[Route("AssignmentAgreement/new/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementSettleList/ByCompany")]
		[Route("new/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementSettleList/ByCompany")]
		public ActionResult GetAssignmentAgreementSettleListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAssignmentAgreementSettleListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion AgreementTableInfoSettlement
		[HttpGet]
		[Route("GetAssignmentAgreementSettleById/id={id}")]
		public ActionResult GetAssignmentAgreementSettleById(string id)
		{
			try
			{
				IAssignmentAgreementSettleService assignmentAgreementSettleService = new AssignmentAgreementSettleService(db);
				return Ok(assignmentAgreementSettleService.GetAssignmentAgreementSettleById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost] 
		[Route("CreateAssignmentAgreementSettle")]
		public ActionResult CreateAssignmentAgreementSettle([FromBody] AssignmentAgreementSettleItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAssignmentAgreementSettleService assignmentAgreementSettleService = new AssignmentAgreementSettleService(db, SysTransactionLogService);
					return Ok(assignmentAgreementSettleService.CreateAssignmentAgreementSettle(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateAssignmentAgreementSettle")]
		public ActionResult UpdateAssignmentAgreementSettle([FromBody] AssignmentAgreementSettleItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAssignmentAgreementSettleService assignmentAgreementSettleService = new AssignmentAgreementSettleService(db, SysTransactionLogService);
					return Ok(assignmentAgreementSettleService.UpdateAssignmentAgreementSettle(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteAssignmentAgreementSettle")]
		public ActionResult DeleteAssignmentAgreementSettle([FromBody] RowIdentity parm)
		{
			try
			{
				IAssignmentAgreementSettleService assignmentAgreementSettleService = new AssignmentAgreementSettleService(db, SysTransactionLogService);
				return Ok(assignmentAgreementSettleService.DeleteAssignmentAgreementSettle(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetAssignmentAgreementSettleDropDown")]
		[Route("AssignmentAgreementTable/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementSettleDropDown")]
		public ActionResult GetAssignmentAgreementSettleDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementSettle(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetAssignmentAgreementTableDropDown")]
		[Route("AssignmentAgreementTable/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementTableDropDown")]
		public ActionResult GetAssignmentAgreementTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetBuyerAgreementTableDropDown")]
		[Route("AssignmentAgreementTable/RelatedInfo/AssignmentAgreementSettlement/GetBuyerAgreementTableDropDown")]
		public ActionResult GetBuyerAgreementTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetDocumentReasonDropDown")]
		[Route("AssignmentAgreementTable/RelatedInfo/AssignmentAgreementSettlement/GetDocumentReasonDropDown")]
		public ActionResult GetDocumentReasonDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
	}
}
