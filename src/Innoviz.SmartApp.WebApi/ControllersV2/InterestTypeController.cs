using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class InterestTypeController : BaseControllerV2
	{

		public InterestTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetInterestTypeList/ByCompany")]
		public ActionResult GetInterestTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInterestTypeListvw(search));
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetInterestTypeById/id={id}")]
		public ActionResult GetInterestTypeById(string id)
		{
			try
			{
				IInterestTypeService interestTypeService = new InterestTypeService(db);
				return Ok(interestTypeService.GetInterestTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateInterestType")]
		public ActionResult CreateInterestType([FromBody] InterestTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInterestTypeService interestTypeService = new InterestTypeService(db, SysTransactionLogService);
					return Ok(interestTypeService.CreateInterestType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateInterestType")]
		public ActionResult UpdateInterestType([FromBody] InterestTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInterestTypeService interestTypeService = new InterestTypeService(db, SysTransactionLogService);
					return Ok(interestTypeService.UpdateInterestType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteInterestType")]
		public ActionResult DeleteInterestType([FromBody] RowIdentity parm)
		{
			try
			{
				IInterestTypeService interestTypeService = new InterestTypeService(db, SysTransactionLogService);
				return Ok(interestTypeService.DeleteInterestType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetInterestTypeValueList/ByCompany")]
		public ActionResult GetInterestTypeValueListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInterestTypeValueListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetInterestTypeValueById/id={id}")]
		public ActionResult GetInterestTypeValueById(string id)
		{
			try
			{
				IInterestTypeService interestTypeService = new InterestTypeService(db);
				return Ok(interestTypeService.GetInterestTypeValueById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateInterestTypeValue")]
		public ActionResult CreateInterestTypeValue([FromBody] InterestTypeValueItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInterestTypeService interestTypeService = new InterestTypeService(db, SysTransactionLogService);
					return Ok(interestTypeService.CreateInterestTypeValue(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateInterestTypeValue")]
		public ActionResult UpdateInterestTypeValue([FromBody] InterestTypeValueItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInterestTypeService interestTypeService = new InterestTypeService(db, SysTransactionLogService);
					return Ok(interestTypeService.UpdateInterestTypeValue(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteInterestTypeValue")]
		public ActionResult DeleteInterestTypeValue([FromBody] RowIdentity parm)
		{
			try
			{
				IInterestTypeService interestTypeService = new InterestTypeService(db, SysTransactionLogService);
				return Ok(interestTypeService.DeleteInterestTypeValue(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetInterestTypeValueInitialData/id={id}")]
		public ActionResult GetInterestTypeValueInitialData(string id)
		{
			try
			{
				IInterestTypeService interestTypeService = new InterestTypeService(db, SysTransactionLogService);
				return Ok(interestTypeService.GetInterestTypeValueInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown

		[HttpPost]
		[Route("GetInterestTypeDropDown")]
		public ActionResult GetInterestTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInterestType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
	}
}
