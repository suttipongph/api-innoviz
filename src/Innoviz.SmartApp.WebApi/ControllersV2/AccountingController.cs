﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.BatchProcessing.Service;
using Innoviz.SmartApp.WebApi.BatchProcessing.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class AccountingController : BaseControllerV2
    {

        public AccountingController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
        #region Function
        #region GenInterestRealizeProcessTrans
        [HttpPost]
        [Route("Function/IncomeRealization/GetGenInterestRealizeProcessTrans")]
        public ActionResult GetGenInterestRealizeProcessTrans([FromBody] GenInterestRealizeProcessTransView vwModel)
        {
            try
            {
                IProcessTransService processTransService = new ProcessTransService(db);
                return Ok(processTransService.GenInterestRealizeProcessTrans(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Function/IncomeRealization/GetGenInterestRealizeProcessTransValidation")]
        public ActionResult GetGenInterestRealizeProcessTransValidation([FromBody] GenInterestRealizeProcessTransView vwModel)
        {
            try
            {
                IProcessTransService processTransService = new ProcessTransService(db);
                return Ok(processTransService.GenInterestRealizeProcessTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Interface AX
        [HttpPost]
        [Route("Function/InterfaceAccountingStaging/ScheduleBatchJob")]
        public async Task<ActionResult> InterfaceAccoutingStaging([FromBody]BatchObjectView batchObjectView)
        {
            try
            {
                IBatchService batchService = new BatchService(db);
                return Ok(await batchService.CreateScheduledJob(batchObjectView));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Function/InterfaceVendorInvoiceStaging/ScheduleBatchJob")]
        public async Task<ActionResult> InterfaceVendorInvoiceStaging([FromBody] BatchObjectView batchObjectView)
        {
            try
            {
                IBatchService batchService = new BatchService(db);
                return Ok(await batchService.CreateScheduledJob(batchObjectView));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Function/InterfaceIntercoInvSettlement/ScheduleBatchJob")]
        public async Task<ActionResult> InterfaceIntercoInvSettlement([FromBody] BatchObjectView batchObjectView)
        {
            try
            {
                IBatchService batchService = new BatchService(db);
                return Ok(await batchService.CreateScheduledJob(batchObjectView));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region GenerateAccountingStaging
        [HttpPost]
        [Route("Function/GenerateAccountingStaging/ScheduleBatchJob")]
        public async Task<ActionResult> GenerateAccountingStaging([FromBody] BatchObjectView batchObjectView)
        {
            try
            {
                IBatchService batchService = new BatchService(db);
                return Ok(await batchService.CreateScheduledJob(batchObjectView));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GenerateAccountingStaging
        #region GenerateVendorInvoiceStaging
        [HttpPost]
        [Route("Function/GenerateVendorInvoiceStaging/ScheduleBatchJob")]
        public async Task<ActionResult> GenerateVendorInvoiceStaging([FromBody] BatchObjectView batchObjectView)
        {
            try
            {
                IBatchService batchService = new BatchService(db);
                return Ok(await batchService.CreateScheduledJob(batchObjectView));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GenerateVendorInvoiceStaging
        #endregion
    }
}
