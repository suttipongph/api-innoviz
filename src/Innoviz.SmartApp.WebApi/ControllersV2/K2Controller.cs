﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class K2Controller : BaseControllerV2
    {
        public K2Controller(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
        #region test
        [HttpPost]
        [Route("Function/DoSomeFunction")]
        public ActionResult<SysUserTableItemView> DoSomeFunction(string companyGUID, [FromBody] SysUserTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ISysUserService sysUserService = new SysUserService(db, SysTransactionLogService);
                    return Ok(sysUserService.GetSysUserTableById(vwModel.Id));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion test
        [HttpGet]
        [Route("GetCompanyGUIDByCompanyId/id={companyId}")]
        public ActionResult<string> GetCompanyGUIDByCompanyId(string companyId)
        {
            try
            {
                ICompanyService companyService = new CompanyService(db);
                return Ok(companyService.GetCompanyGUIDByCompanyId(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("GetActionHistoryForK2")]
        public ActionResult<ActionHistoryItemView> GetActionHistoryForK2(string companyGUID, [FromBody]ActionHistoryK2Parm parm)
        {
            try
            {
                IActionHistoryService actionHistoryService = new ActionHistoryService(db);
                return Ok(actionHistoryService.GetActionHistoryForK2(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Credit application request 
        [HttpPost]
        [Route("Function/UpdateCreditApplicationRequestK2")]
        public ActionResult<string> UpdateCreditApplicationRequestK2(string companyGUID, [FromBody] UpdateCreditApplicationRequestK2ParmView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    creditAppRequestTableService.UpdateCreditApplicationRequestK2(vwModel);
                    return Ok("success");
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Function/ApproveCreditApplicationRequestK2")]
        public ActionResult<string> ApproveCreditApplicationRequestK2(string companyGUID, [FromBody] ApproveCreditApplicationRequestK2ParmView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    creditAppRequestTableService.ApproveCreditApplicationRequestK2(vwModel);
                    return Ok("success");
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                IActionHistoryService actionHistoryService = new ActionHistoryService(db);
                actionHistoryService.CreateErrorActionHistories(
                    vwModel.ParmDocGUID, 
                    vwModel.WorkflowName, 
                    "Validate approve", 
                    string.Empty, 
                    vwModel.CompanyGUID, 
                    vwModel.Owner, vwModel.
                    OwnerBusinessUnitGUID, 
                    e);
                throw e;
            }
        }
        [HttpPost]
        [Route("Function/ApproveAmendcustomerCreditLimitK2")]
        public ActionResult<string> ApproveAmendcustomerCreditLimitK2(string companyGUID, [FromBody] ApproveAmendcustomerCreditLimitK2ParmView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    creditAppRequestTableService.ApproveAmendcustomerCreditLimitK2(vwModel);
                    return Ok("success");
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                IActionHistoryService actionHistoryService = new ActionHistoryService(db);
                actionHistoryService.CreateErrorActionHistories(
                    vwModel.ParmDocGUID,
                    vwModel.WorkflowName,
                    "Validate approve",
                    string.Empty,
                    vwModel.CompanyGUID,
                    vwModel.Owner, vwModel.
                    OwnerBusinessUnitGUID,
                    e);
                throw e;
            }
        }
        [HttpPost]
        [Route("Function/ApproveAmendCustomerInformationK2")]
        public ActionResult<string> ApproveAmendCustomerInformationK2(string companyGUID, [FromBody] ApproveAmendCustomerInformationK2ParmView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    creditAppRequestTableService.ApproveAmendCustomerInformationK2(vwModel);
                    return Ok("success");
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                IActionHistoryService actionHistoryService = new ActionHistoryService(db);
                actionHistoryService.CreateErrorActionHistories(
                    vwModel.ParmDocGUID,
                    vwModel.WorkflowName,
                    "Validate approve",
                    string.Empty,
                    vwModel.CompanyGUID,
                    vwModel.Owner, vwModel.
                    OwnerBusinessUnitGUID,
                    e);
                throw e;
            }
        }
        [HttpPost]
        [Route("Function/AmendCustomerBuyerCreditLimitK2")]
        public ActionResult<string> AmendCustomerBuyerCreditLimitK2(string companyGUID, [FromBody] AmendCustomerBuyerCreditLimitK2ParmView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    creditAppRequestTableService.AmendCustomerBuyerCreditLimitK2(vwModel);
                    return Ok("success");
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                IActionHistoryService actionHistoryService = new ActionHistoryService(db);
                actionHistoryService.CreateErrorActionHistories(
                    vwModel.ParmDocGUID,
                    vwModel.WorkflowName,
                    "Validate approve",
                    string.Empty,
                    vwModel.CompanyGUID,
                    vwModel.Owner, vwModel.
                    OwnerBusinessUnitGUID,
                    e);
                throw e;
            }
        }
        [HttpPost]
        [Route("Function/ApproveAmendBuyerInformationK2")]
        public ActionResult<string> ApproveAmendBuyerInformationK2(string companyGUID, [FromBody] ApproveAmendBuyerInformationK2ParmView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    creditAppRequestTableService.ApproveAmendBuyerInformationK2(vwModel);
                    return Ok("success");
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                IActionHistoryService actionHistoryService = new ActionHistoryService(db);
                actionHistoryService.CreateErrorActionHistories(
                    vwModel.ParmDocGUID,
                    vwModel.WorkflowName,
                    "Validate approve",
                    string.Empty,
                    vwModel.CompanyGUID,
                    vwModel.Owner, vwModel.
                    OwnerBusinessUnitGUID,
                    e);
                throw e;
            }
        }
        [HttpPost]
        [Route("Function/GetCreditApplicationRequestEmailContentK2")]
        public ActionResult<GetCreditApplicationRequestEmailContentK2ResultView> GetCreditApplicationRequestEmailContentK2(string companyGUID, [FromBody] GetCreditApplicationRequestEmailContentK2ParmView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableService.GetCreditApplicationRequestEmailContentK2(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                IActionHistoryService actionHistoryService = new ActionHistoryService(db);
                actionHistoryService.CreateErrorActionHistories(
                    vwModel.ParmDocGUID,
                    vwModel.WorkflowName,
                    "Get email content",
                    string.Empty,
                    vwModel.CompanyGUID,
                    vwModel.Owner, vwModel.
                    OwnerBusinessUnitGUID,
                    e);
                throw e;
            }
        }
        #endregion Credit application request 
        #region Purchase table
        [HttpPost]
        [Route("Function/UpdatePurchaseWorkflowConditionK2")]
        public ActionResult<PurchaseTableItemView> UpdatePurchaseWorkflowConditionK2(string companyGUID, [FromBody] UpdatePurchaseWorkflowConditionK2ParmView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                    return Ok(purchaseTableService.UpdatePurchaseWorkflowConditionK2(vwModel.PurchaseTableGUID));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Function/UpdatePurchaseTableK2")]
        public ActionResult<string> UpdatePurchaseTableK2(string companyGUID, [FromBody] UpdatePurchaseTableK2ParmView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                    purchaseTableService.UpdatePurchaseTableK2(vwModel.PurchaseTableGUID,vwModel.StatusId,vwModel.ProcessInstanceId,vwModel.DocumentReasonGUID);
                    return Ok("success");
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Attechment
        [HttpPost]
        [Route("Function/GetAttachmentByFileName")]
        public async Task<ActionResult<AttachmentItemView>> GetAttachmentByFileName(string companyGUID, [FromBody] AttachmentK2Parm vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(await attachmentService.GetAttachmentByFileName(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Attechment
        #region Respoense Error Code
        [HttpGet]
        [Route("GetTestErrorCode500")]
        public ActionResult GetTestErrorCode500(string companyGUID)
        {
            try
            {
                throw new Exception("TestErrorCode.500") ;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetTestErrorCode400")]
        public ActionResult GetTestErrorCode400(string companyGUID)
        {
            try
            {
                throw new SmartAppException("TestErrorCode.400.CustomErrorCode400") ;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetTestErrorCode444")]
        public ActionResult GetTestErrorCode444(string companyGUID)
        {
            try
            {
                throw new SmartAppException("TestErrorCode.444.CustomErrorCode444");
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetTestErrorCode555")]
        public ActionResult GetTestErrorCode555(string companyGUID)
        {
            try
            {
                throw new SmartAppException("TestErrorCode.555.CustomErrorCode555");
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Respoense Error Code
    }
}
