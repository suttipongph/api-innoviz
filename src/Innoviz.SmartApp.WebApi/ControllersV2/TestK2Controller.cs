﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    [Produces("application/json")]
    [Route("api/v2/[controller]")]
    public class TestK2Controller: ControllerBase
    {
        [HttpGet]
        [Route("TestInput500ForErrorCode500/code={code}")]
        public ActionResult<TestK2Object> GetTestErrorCode500(string code)
        {
            try
            {
                int num = 0;
                bool parsed = Int32.TryParse(code, out num);
                if(parsed && num == 500)
                {
                    throw new Exception("TestErrorCode.500");
                }
                else if (code.ToLower() == "empty")
                {
                    return Ok();
                }
                else
                {
                    return Ok(new TestK2Object { Message = code });
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("TestInput400ForErrorCode400/code={code}")]
        public ActionResult GetTestErrorCode400(string code)
        {
            try
            {
                int num = 0;
                bool parsed = Int32.TryParse(code, out num);
                if (parsed && num == 400)
                {
                    throw new SmartAppException("TestErrorCode.400.CustomErrorCode400");
                }
                else if (code.ToLower() == "empty")
                {
                    return Ok();
                }
                else
                {
                    return Ok(new TestK2Object { Message = code });
                }
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                throw new SmartAppException("From 400", e);
            }
        }
        [HttpGet]
        [Route("TestInput444ForErrorCode444/code={code}")]
        public ActionResult GetTestErrorCode444(string code)
        {
            try
            {
                int num = 0;
                bool parsed = Int32.TryParse(code, out num);
                if (parsed && num == 444)
                {
                    throw new SmartAppException("TestErrorCode.444.CustomErrorCode444");
                }
                else if (code.ToLower() == "empty")
                {
                    return Ok();
                }
                else
                {
                    return Ok(new TestK2Object { Message = code });
                }
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                throw new SmartAppException("From 444", e);
            }
        }
        [HttpGet]
        [Route("TestInput555ForErrorCode555/code={code}")]
        public ActionResult<TestK2Object> GetTestErrorCode555(string code)
        {
            try
            {
                int num = 0;
                bool parsed = Int32.TryParse(code, out num);
                if (parsed && num == 555)
                {
                    throw new SmartAppException("TestErrorCode.555.CustomErrorCode555");
                }
                else if(code.ToLower() == "empty")
                {
                    return Ok();
                }
                else
                {
                    return Ok(new TestK2Object { Message = code });
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
    public class TestK2Object
    {
        public string Message { get; set; }
    }
}
