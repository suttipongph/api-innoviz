﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic; 
namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class AddressDistrictController : BaseControllerV2
	{
		public AddressDistrictController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetAddressDistrictList/ByCompany")]
		public ActionResult GetAddressDistrictByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetAddressDistrictListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetAddressDistrictById/id={id}")]
		public ActionResult GetAddressSubDistrictById(string id)
		{
			try
			{
				IAddressService addressService = new AddressService(db);
				return Ok(addressService.GetAddressDistrictById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateAddressDistrict")]
		public ActionResult CreateAddressDistrict([FromBody] AddressDistrictItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressService addressService = new AddressService(db, SysTransactionLogService);
					return Ok(addressService.CreateAddressDistrict(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateAddressDistrict")]
		public ActionResult UpdateAddressDistrict([FromBody] AddressDistrictItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IAddressService addressService = new AddressService(db, SysTransactionLogService);
					return Ok(addressService.UpdateAddressDistrict(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteAddressDistrict")]
		public ActionResult DeleteAddressDistrict([FromBody] RowIdentity parm)

		{
			try
			{
				IAddressService addressService = new AddressService(db, SysTransactionLogService);
				return Ok(addressService.DeleteAddressDistrict(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetAddressProvinceDropDown")]
		public ActionResult GetAddressProvinceDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemAddressProvince(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
