using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class DocumentReturnMethodController : BaseControllerV2
	{

		public DocumentReturnMethodController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetDocumentReturnMethodList/ByCompany")]
		public ActionResult GetDocumentReturnMethodListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetDocumentReturnMethodListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetDocumentReturnMethodById/id={id}")]
		public ActionResult GetDocumentReturnMethodById(string id)
		{
			try
			{
				IDocumentReturnMethodService documentReturnMethodService = new DocumentReturnMethodService(db);
				return Ok(documentReturnMethodService.GetDocumentReturnMethodById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateDocumentReturnMethod")]
		public ActionResult CreateDocumentReturnMethod([FromBody] DocumentReturnMethodItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentReturnMethodService documentReturnMethodService = new DocumentReturnMethodService(db, SysTransactionLogService);
					return Ok(documentReturnMethodService.CreateDocumentReturnMethod(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateDocumentReturnMethod")]
		public ActionResult UpdateDocumentReturnMethod([FromBody] DocumentReturnMethodItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentReturnMethodService documentReturnMethodService = new DocumentReturnMethodService(db, SysTransactionLogService);
					return Ok(documentReturnMethodService.UpdateDocumentReturnMethod(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteDocumentReturnMethod")]
		public ActionResult DeleteDocumentReturnMethod([FromBody] RowIdentity parm)
		{
			try
			{
				IDocumentReturnMethodService documentReturnMethodService = new DocumentReturnMethodService(db, SysTransactionLogService);
				return Ok(documentReturnMethodService.DeleteDocumentReturnMethod(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
