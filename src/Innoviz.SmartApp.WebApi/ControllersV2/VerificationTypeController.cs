using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class VerificationTypeController : BaseControllerV2
	{

		public VerificationTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetVerificationTypeList/ByCompany")]
		public ActionResult GetVerificationTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetVerificationTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetVerificationTypeById/id={id}")]
		public ActionResult GetVerificationTypeById(string id)
		{
			try
			{
				IVerificationTypeService verificationTypeService = new VerificationTypeService(db);
				return Ok(verificationTypeService.GetVerificationTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateVerificationType")]
		public ActionResult CreateVerificationType([FromBody] VerificationTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IVerificationTypeService verificationTypeService = new VerificationTypeService(db, SysTransactionLogService);
					return Ok(verificationTypeService.CreateVerificationType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateVerificationType")]
		public ActionResult UpdateVerificationType([FromBody] VerificationTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IVerificationTypeService verificationTypeService = new VerificationTypeService(db, SysTransactionLogService);
					return Ok(verificationTypeService.UpdateVerificationType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteVerificationType")]
		public ActionResult DeleteVerificationType([FromBody] RowIdentity parm)
		{
			try
			{
				IVerificationTypeService verificationTypeService = new VerificationTypeService(db, SysTransactionLogService);
				return Ok(verificationTypeService.DeleteVerificationType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
