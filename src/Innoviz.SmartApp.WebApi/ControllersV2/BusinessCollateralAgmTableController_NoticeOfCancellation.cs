﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public partial class BusinessCollateralAgmTableController : BaseControllerV2
    {
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineList/ByCompany")]
        public ActionResult GetBusinessCollateralAgmLineListByCompanyByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBusinessCollateralAgmLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineById/id={id}")]
        public ActionResult GetBusinessCollateralAgmLineByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmLine")]
        public ActionResult CreateBusinessCollateralAgmLineByNoticeOfCancellation([FromBody] BusinessCollateralAgmLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.CreateBusinessCollateralAgmLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmLine")]
        public ActionResult UpdateBusinessCollateralAgmLineByNoticeOfCancellation([FromBody] BusinessCollateralAgmLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.UpdateBusinessCollateralAgmLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmLine")]
        public ActionResult DeleteBusinessCollateralAgmLineByNoticeOfCancellation([FromBody] RowIdentity parm)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.DeleteBusinessCollateralAgmLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineInitialData/businessCollateralAgmId={businessCollateralAgmId}")]
        public ActionResult GetBusinessCollateralAgmLineInitialDataByNoticeOfCancellation(string businessCollateralAgmId)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmLineInitialData(businessCollateralAgmId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableList/ByCompany")]
        public ActionResult GetBusinessCollateralAgmTableListByCompanyByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBusinessCollateralAgmTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableById/id={id}")]
        public ActionResult GetBusinessCollateralAgmTableByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/CreateBusinessCollateralAgmTable")]
        public ActionResult CreateBusinessCollateralAgmTableByNoticeOfCancellation([FromBody] BusinessCollateralAgmTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.CreateBusinessCollateralAgmTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/UpdateBusinessCollateralAgmTable")]
        public ActionResult UpdateBusinessCollateralAgmTableByNoticeOfCancellation([FromBody] BusinessCollateralAgmTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.UpdateBusinessCollateralAgmTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/DeleteBusinessCollateralAgmTable")]
        public ActionResult DeleteBusinessCollateralAgmTableByNoticeOfCancellation([FromBody] RowIdentity parm)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.DeleteBusinessCollateralAgmTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableInitialData/productType={productType}")]
        public ActionResult GetBusinessCollateralAgmTableInitialDataByNoticeOfCancellation(int productType)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmTableInitialData(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqInternalBusinessCollateralAgm/companyId={companyId}")]
        public ActionResult ValidateIsManualNumberSeqInternalBusinessCollateralAgmByNoticeOfCancellation(string companyId)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.IsManualByInternalBusinessCollateralAgreement(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/ValidateIsManualNumberSeqBusinessCollateralAgm/companyId={companyId}")]
        public ActionResult ValidateIsManualNumberSeqBusinessCollateralAgmByNoticeOfCancellation(string companyId)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.IsManualByBusinessCollateralAgreement(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankGroupDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankGroupDropDown")]
        public ActionResult GetBankGroupDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBankTypeDropDown")]
        public ActionResult GetBankTypeDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeDropDown")]
        public ActionResult GetBusinessCollateralSubTypeDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralSubType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        public ActionResult GetBusinessCollateralSubTypeByBusinessCollateralTypDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db);
                return Ok(businessCollateralTypeService.GetDropDownItemBusinessCollateralSubTypeByBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralTypeDropDown")]
        public ActionResult GetBusinessCollateralTypeDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralDropDown")]
        public ActionResult GetCreditAppReqBusinessCollateralDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppReqBusinessCollateral(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableByBusinessCollateralAgmTableDropDown")]
        public ActionResult GetCreditAppRequestTableByBusinessCollateralAgmTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetDropDownItemCreditAppRequestTableByBusinessCollateralAgmTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppRequestTableDropDown")]
        public ActionResult GetDropDownItemCreditAppRequestTableByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetDropDownItemBusinessCollateralAgmStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown")]
        public ActionResult GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        [Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralStatusDropDown")]
        public ActionResult GetBusinessCollateralStatusDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        [Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmLineDropDown")]
        public ActionResult GetBusinessCollateralAgmLineDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralAgmLine(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]

        [Route("Bond/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Bond/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Factoring/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("HirePurchase/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("HirePurchase/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Leasing/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("Leasing/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("LCDLC/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("LCDLC/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("ProjectFinance/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        [Route("ProjectFinance/newbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetBusinessCollateralAgmTableDropDown")]
        public ActionResult GetBusinessCollateralAgmTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeByBusinessCollateralAgmTableByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByBusinessCollateralAgmTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeBusinessCollateralAgmLineByBusinessCollateralAgmTableByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateAddendumValidation/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateAddendumValidation/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateAddendumValidation/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateAddendumValidation/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateAddendumValidation/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateAddendumValidation/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateAddendumValidation/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateAddendumValidation/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateAddendumValidation/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateAddendumValidation/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateAddendumValidation/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateAddendumValidation/id={id}")]
        public ActionResult GetGenerateAddendumValidationByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetGenerateAddendumValidation(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetGenerateNoticeOfCancellationValidation/id={id}")]
        public ActionResult GetGenerateNoticeOfCancellationValidationByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetGenerateNoticeOfCancellationValidation(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Notice of Cancellation (Function)
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetCancelBusinessCollateralAgmById/id={id}")]
        public ActionResult GetCancelBusinessCollateralAgmByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetGenCancelBusCollateralAgmById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownBusinessCollateralAGMByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/ValidateIsManualNumberSeq/companyId={companyId}")]
        public ActionResult ValidateIsManual_NumberSeqnoticeofcancellationByNoticeOfCancellation(string companyId)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.IsManualByInternalBusinessCollateralAgreement(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/noticeofcancellationbusinesscollateralagmtable/Function/noticeofcancellation/CreateCancelBusinessCollateralAgm")]
        public ActionResult CreateCancelBusinessCollateralAgmByNoticeOfCancellation([FromBody] CancelBusinessCollateralAgmView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                    return Ok(businessCollateralAgmTableService.CreateCancelBusinessCollateralAgm(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/GetCopyBusinessCollateralAgmLineById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/GetCopyBusinessCollateralAgmLineById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/GetCopyBusinessCollateralAgmLineById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/GetCopyBusinessCollateralAgmLineById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/GetCopyBusinessCollateralAgmLineById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/GetCopyBusinessCollateralAgmLineById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/GetCopyBusinessCollateralAgmLineById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/GetCopyBusinessCollateralAgmLineById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/GetCopyBusinessCollateralAgmLineById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/GetCopyBusinessCollateralAgmLineById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/GetCopyBusinessCollateralAgmLineById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/GetCopyBusinessCollateralAgmLineById/id={id}")]
        public ActionResult GetCopyBusinessCollateralAgmLineByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetCopyBusinessCollateralAgmLineById(id));

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/UpdateCopyBusinessCollateralAgmLine")]
        public ActionResult UpdateCopyBusinessCollateralAgmLineByNoticeOfCancellation([FromBody] CopyBusinessCollateralAgmLine vwModel)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.UpdateCopyBusinessCollateralAgmLine(vwModel));

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/ValidateCopyBusinessCollateralAgmLine/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/ValidateCopyBusinessCollateralAgmLine/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/ValidateCopyBusinessCollateralAgmLine/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/ValidateCopyBusinessCollateralAgmLine/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/ValidateCopyBusinessCollateralAgmLine/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/ValidateCopyBusinessCollateralAgmLine/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/ValidateCopyBusinessCollateralAgmLine/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/ValidateCopyBusinessCollateralAgmLine/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/ValidateCopyBusinessCollateralAgmLine/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/ValidateCopyBusinessCollateralAgmLine/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/ValidateCopyBusinessCollateralAgmLine/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CopyActiveBusinessCollateralAgreementLine/ValidateCopyBusinessCollateralAgmLine/id={id}")]
        public ActionResult ValidateCopyBusinessCollateralAgmLineByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.ValidateCopyBusinessCollateralAgmLine(id));

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region RelatedInfo
        #region BookmarkDocument
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        public ActionResult GetBookmarkDocumentTransListByCompanyByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBookmarkDocumentTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        public ActionResult GetBookmarkDocumentTransByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(BookmarkDocumentTransService.GetBookmarkDocumentTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        public ActionResult CreateBookmarkDocumentTransByNoticeOfCancellation([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.CreateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        public ActionResult UpdateBookmarkDocumentTransByNoticeOfCancellation([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.UpdateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        public ActionResult DeleteBookmarkDocumentTransByNoticeOfCancellation([FromBody] RowIdentity parm)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                return Ok(BookmarkDocumentTransService.DeleteBookmarkDocumentTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        public ActionResult GetBookmarkDocumentTransInitialDataByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetBookmarkDocumentTransInitialDataByGuarantorAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/GuarantorAgreementTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetBookmarkDocumentTransInitialDataGuarantorAgreementByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetBookmarkDocumentTransInitialDataByGuarantorAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        public ActionResult GetBookmarkDocumentDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
                return Ok(bookmarkDocumentService.GetDropDownItemBookmarkDocumentByBusinessAGM(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        public ActionResult GetDocumentTemplateTableByDocumentTypeDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
                return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        public ActionResult GetBookmarkDocumentTransDocumentStatusDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetDropDownItemBookMarkDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        public ActionResult GetDocumentTemplateTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeBookmarkDocumentTransByBusinessCollateralAgmTableByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByBusinessCollateralAgmTableForBookmarkDocumentTrans(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region PrintBookDocTrans
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        public ActionResult GetPrintBookDocTransValidationByNoticeOfCancellation([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintBookDocTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransactionByNoticeOfCancellation([FromBody] PrintBookDocTransParm printBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region copybookmarkdocumentfromtemplate

        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        public ActionResult GetBookmarkDocumentTemplateTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetDropDownItemBookmarkDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage")]
        public ActionResult CopyBookmarkDocumentFromTemplateByNoticeOfCancellation([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.CopyBookmarkDocumentFromTemplate(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplatePage/GetCopyBookmarkDocumentValidation")]

        public ActionResult GetCopyBookmarkDocumentValidationByNoticeOfCancellation([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.GetCopyBookmarkDocumentValidation(param));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        #endregion BookmarkDocument        
        #region ServiceFeeTrans
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        public ActionResult GetServiceFeeTransListByCompanyByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        public ActionResult GetServiceFeeTransByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetServiceFeeTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeServiceFeeTransByBusinessCollateralAgmTableByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        public ActionResult CreateServiceFeeTransByNoticeOfCancellation([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.CreateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        public ActionResult UpdateServiceFeeTransByNoticeOfCancellation([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.UpdateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        public ActionResult DeleteServiceFeeTransByNoticeOfCancellation([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                return Ok(ServiceFeeTransService.DeleteServiceFeeTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        public ActionResult GetServiceFeeTransInitialDataByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetServiceFeeTransInitialDataByBusinessCollateralAgmTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetCalculateField")]

        public ActionResult GetCalculateFieldByNoticeOfCancellation([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetCalculateField(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        public ActionResult ValidateServiceFeeTransByNoticeOfCancellation([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.ValidateServiceFeeTrans(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        public ActionResult GetTaxValueByNoticeOfCancellation([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetTaxValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        public ActionResult GetWHTValueByNoticeOfCancellation([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetWHTValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]

        public ActionResult GetInvoiceRevenueTypeByProductTypeDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeByProductTypeAndDefaultNone(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        //Add InvoiceTable RelelatedInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]

        public ActionResult GetLedgerDimensionDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        //Add InvoiceTable RelatedInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        //Add InvoiceTable ReleatedInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]

        public ActionResult GetWithholdingTaxTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByServiceFeeTransByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        public ActionResult GetOriginalInvoiceByServiceFeeTransDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetOriginalInvoiceByServiceFeeTransDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion ServiceFeeTrans
        #region ConsortiumTrans
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]

        public ActionResult GetConsortiumTransListByCompanyByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetConsortiumTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        public ActionResult GetConsortiumTransTransByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db);
                return Ok(ConsortiumTransService.GetConsortiumTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        public ActionResult CreateConsortiumTransByNoticeOfCancellation([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.CreateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        public ActionResult UpdateConsortiumTransByNoticeOfCancellation([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.UpdateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        public ActionResult DeleteConsortiumTransByNoticeOfCancellation([FromBody] RowIdentity parm)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                return Ok(ConsortiumTransService.DeleteConsortiumTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        public ActionResult GetConsortiumTransInitialDataByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetConsortuimTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("LCDLC/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("LCDLC/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("ProjectFinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("ProjectFinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        public ActionResult GetConsortiumTransConsortiumLineDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IConsortiumTableService consortiumTableService = new ConsortiumTableService(db);
                return Ok(consortiumTableService.GetDropDownItemConsortiumLineByConsortiumTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeConsortiumTransByBusinessCollateralAgmTableByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allbusinesscollateralagmtable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown/")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTypeDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Consortium
        #region Attachment
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompanyByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/CreateAttachment")]
        public ActionResult CreateAttachmentByNoticeOfCancellation([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachmentByNoticeOfCancellation([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/DeleteAttachment")]

        public ActionResult DeleteAttachmentByNoticeOfCancellation([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Bond/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Bond/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("HirePurchase/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Leasing/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Lcdlc/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]

        [Route("Projectfinance/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Projectfinance/newBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefIdByNoticeOfCancellation([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region InvoiceTable
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        public ActionResult GetInvoiceLineListByCompanyByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        public ActionResult GetInvoiceLineByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        public ActionResult CreateInvoiceLineByNoticeOfCancellation([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        public ActionResult UpdateInvoiceLineByNoticeOfCancellation([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        public ActionResult DeleteInvoiceLineByNoticeOfCancellation([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        public ActionResult GetInvoiceTableListByCompanyByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        public ActionResult GetInvoiceTableByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        public ActionResult CreateInvoiceTableByNoticeOfCancellation([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        public ActionResult UpdateInvoiceTableByNoticeOfCancellation([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        public ActionResult DeleteInvoiceTableByNoticeOfCancellation([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceTable
        #region DropdownInvoiceTable
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        public ActionResult GetInvoiceTypeDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        public ActionResult GetDocumentStatusByInvoiceTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        public ActionResult GetBuyerInvoiceTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDownByInvoiceTableByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        public ActionResult GetCurrencyDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCurrency(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        public ActionResult GetInvoiceTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        public ActionResult GetProdUnitTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        public ActionResult GetTaxInvoiceTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        public ActionResult GetRefInvoiceTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetRefInvoiceTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropdownInvoiceTable
        #region jointventure
        [HttpPost]

        [Route("Factoring/allBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/GetJointVentureTransList/ByCompany")]
        public ActionResult GetJointVentureTransListByCompanyByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetJointVentureTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        public ActionResult JointVentureTransByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IJointVentureTransService JointVentureTransService = new JointVentureTransService(db);
                return Ok(JointVentureTransService.GetJointVentureTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetJointVentureTransAuthorizedPersonTypeDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        public ActionResult CreateJointVentureTransByNoticeOfCancellation([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.CreateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        public ActionResult UpdateJointVentureTransByNoticeOfCancellation([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.UpdateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/jointventuretrans/DeleteJointVentureTrans")]
        public ActionResult DeletejointventuretransByNoticeOfCancellation([FromBody] RowIdentity parm)
        {
            try
            {
                IJointVentureTransService jointventuretransService = new JointVentureTransService(db, SysTransactionLogService);
                return Ok(jointventuretransService.DeleteJointVentureTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        public ActionResult GetJointVentureTransInitialDataByNoticeOfCancellation(string Id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableleService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableleService.GetJointVentureTransInitialDataByBusinessCollateralAgmTable(Id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/JointVentureTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeJointVentureTransByBusinessCollateralAgmTableByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion jointventure
        #region Memo
        [HttpPost]

        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult GetMemoListByCompanyByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateMemoByNoticeOfCancellation([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateMemoByNoticeOfCancellation([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult DeleteMemoByNoticeOfCancellation([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetMemoByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialDataByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/MemoTrans/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeMemoTransByBusinessCollateralAgmTableByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableList/ByCompany")]
        public ActionResult GetBusinessCollateralAgmTableListByCompanyByMemoTransByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBusinessCollateralAgmTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/MemoTrans/GetBusinessCollateralAgmTableById/id={id}")]
        public ActionResult GetBusinessCollateralAgmTableByIdByMemoTransByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetBusinessCollateralAgmTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Memo
        #region AgreementTableInfo
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        public ActionResult GetAgreementTableInfoListByCompanyByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAgreementTableInfoListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        public ActionResult GetAgreementTableInfoByIdByNoticeOfCancellation(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        public ActionResult CreateAgreementTableInfoByNoticeOfCancellation([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.CreateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        public ActionResult UpdateAgreementTableInfoByNoticeOfCancellation([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.UpdateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        public ActionResult DeleteAgreementTableInfoByNoticeOfCancellation([FromBody] RowIdentity parm)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                return Ok(AgreementTableInfoService.DeleteAgreementTableInfo(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Hire/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Hire/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetAgreementTableInfoByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAgreementTableInfoByBusinessCollateralAgmTableByNoticeOfCancellation(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoByBusinessCollateralAgm(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        public ActionResult GetAgreementTableInfoInitialDataByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db, SysTransactionLogService);
                return Ok(businessCollateralAgmTableService.GetAgreementTableInfoInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        // AgreementInfo
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAccessModeByBusinessCollateralAgmTable/id={id}")]
        public ActionResult GetAccessModeAgreementTableInfoByBusinessCollateralAgmTableByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        public ActionResult GetAgreementTableInfoAddressTransDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressTransByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTransByCustomerDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(authorizedPersonTransService.GetDropDownItemAuthorizedPersonTransByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCreditAppDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTransByCreditAppDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(authorizedPersonTransService.GetDropDownItemAuthorizedPersonTransByCreditApp(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTypeDropDownByNoticeOfCancellationByAgreementTableInfo([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        public ActionResult GetAgreementTableInfoCompanyBankDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCompanyBank(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        public ActionResult GetAgreementTableInfoCompanySignatureDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetEmployeeTableWithCompanySignatureDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        public ActionResult GetAgreementTableInfoEmployeeTableDropDownByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion AgreementTableInfo
        #endregion RelatedInfo
        #region Function
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleManageMenu/id={id}")]
        public ActionResult GetVisibleManageMenuByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetVisibleManageMenu(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/GetVisibleGenerateMenu/id={id}")]
        public ActionResult GetVisibleGenerateMenuByNoticeOfCancellation(string id)
        {
            try
            {
                IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                return Ok(businessCollateralAgmTableService.GetVisibleGenerateMenu(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region PrintSetBookDocTrans
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransactionByNoticeOfCancellation([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByRefGUID(printSetBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Manage agreement
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/ManageAgreement")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/ManageAgreement")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/ManageAgreement")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/ManageAgreement")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/ManageAgreement")]
        public ActionResult ManageAgreementByNoticeOfCancellation([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.ManageAgreement(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementValidation")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementValidation")]
        public ActionResult GetManageAgreementValidationByNoticeOfCancellation([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetManageAgreementByRefType")]
        public ActionResult GetManageAgreementByRefTypeByNoticeOfCancellation([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementByRefType(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/PostAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CancelAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/CloseAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SendAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Factoring/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Bond/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("HirePurchase/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("LCDLC/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("Leasing/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/AllBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        [Route("ProjectFinance/NewBusinessCollateralAgmTable/RelatedInfo/NoticeOfCancellationBusinessCollateralAgmTable/Function/SignAgreement/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByManageAgreementByNoticeOfCancellation([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #endregion Function
    }
}
