﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class OwnershipController : BaseControllerV2
	{

		public OwnershipController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetOwnershipList/ByCompany")]
		public ActionResult GetOwnershipListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetOwnershipListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetOwnershipById/id={id}")]
		public ActionResult GetOwnershipById(string id)
		{
			try
			{
				IOwnershipService ownershipService = new OwnershipService(db);
				return Ok(ownershipService.GetOwnershipById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateOwnership")]
		public ActionResult CreateOwnership([FromBody] OwnershipItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IOwnershipService ownershipService = new OwnershipService(db, SysTransactionLogService);
					return Ok(ownershipService.CreateOwnership(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateOwnership")]
		public ActionResult UpdateOwnership([FromBody] OwnershipItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IOwnershipService ownershipService = new OwnershipService(db, SysTransactionLogService);
					return Ok(ownershipService.UpdateOwnership(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteOwnership")]
		public ActionResult DeleteOwnership([FromBody] RowIdentity parm)
		{
			try
			{
				IOwnershipService ownershipService = new OwnershipService(db, SysTransactionLogService);
				return Ok(ownershipService.DeleteOwnership(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
