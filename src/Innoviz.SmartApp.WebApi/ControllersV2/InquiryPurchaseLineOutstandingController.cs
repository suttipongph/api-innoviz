﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using Innoviz.SmartApp.ReportViewer;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class InquiryPurchaseLineOutstandingController : BaseControllerV2
    {

        public InquiryPurchaseLineOutstandingController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
        [HttpPost]
        [Route("GetInquiryPurchaseLineOutstandingList/ByCompany")]
        public ActionResult GetInquiryPurchaseLineOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInquiryPurchaseLineOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetInquiryPurchaseLineOutstandingById/id={id}")]
        public ActionResult GetInquiryPurchaseLineOutstandingById(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return Ok(purchaseTableService.GetInquiryPurchaseLineOutstandingById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("GetCustomerTableDropDown")]
        [Route("RelatedInfo/messengerrequest/GetCustomerTableDropDown")]
        [Route("RelatedInfo/Pdc/GetCustomerTableDropDown")]

        public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("GetBuyerTableDropDown")]
        [Route("RelatedInfo/MessengerRequest/GetBuyerTableDropDown")]

        [Route("RelatedInfo/Pdc/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("GetPurchaseTableDropDown")]
        public ActionResult GetPurchaseTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemPurchaseTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CollectionFollowUp
        [HttpPost]
        [Route("RelatedInfo/CollectionFollowUp/GetCollectionFollowUpList/ByCompany")]
        public ActionResult GetCollectionFollowUpListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCollectionFollowUpListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/CollectionFollowUp/GetCollectionFollowUpById/id={id}")]
        public ActionResult GetCollectionFollowUpById(string id)
        {
            try
            {
                ICollectionFollowUpService collectionFollowUpService = new CollectionFollowUpService(db);
                return Ok(collectionFollowUpService.GetCollectionFollowUpById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CollectionFollowUp/CreateCollectionFollowUp")]
        public ActionResult CreateCollectionFollowUp([FromBody] CollectionFollowUpItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICollectionFollowUpService collectionFollowUpService = new CollectionFollowUpService(db, SysTransactionLogService);
                    return Ok(collectionFollowUpService.CreateCollectionFollowUp(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CollectionFollowUp/UpdateCollectionFollowUp")]
        public ActionResult UpdateCollectionFollowUp([FromBody] CollectionFollowUpItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICollectionFollowUpService collectionFollowUpService = new CollectionFollowUpService(db, SysTransactionLogService);
                    return Ok(collectionFollowUpService.UpdateCollectionFollowUp(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CollectionFollowUp/DeleteCollectionFollowUp")]
        public ActionResult DeleteCollectionFollowUp([FromBody] RowIdentity parm)
        {
            try
            {
                ICollectionFollowUpService collectionFollowUpService = new CollectionFollowUpService(db, SysTransactionLogService);
                return Ok(collectionFollowUpService.DeleteCollectionFollowUp(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/CollectionFollowUp/GetCollectionFollowUpInitialData/id={id}")]
        public ActionResult GetCollectionFollowUpInitialData(string id)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db, SysTransactionLogService);
                return Ok(purchaseTableService.GetCollectionFollowUpInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("RelatedInfo/CollectionFollowUp/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDownByCollectionFollowUp([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CollectionFollowUp/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDownByCollectionFollowUp([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CollectionFollowUp/GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDownByCollectionFollowUp([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CollectionFollowUp/GetBankGroupDropDown")]
        public ActionResult GetBankGroupDropDownByCollectionFollowUp([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        #endregion
        #region memo
        [HttpPost]
        [Route("RelatedInfo/CollectionFollowUp/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult GetMemoTransList([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                var result = sysListViewService.GetMemoTransListvw(search);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpGet]
        [Route("RelatedInfo/CollectionFollowUp/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetMemoTransById(string id)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                return Ok(memoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/CollectionFollowUp/RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("RelatedInfo/CollectionFollowUp/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [Route("RelatedInfo/CollectionFollowUp/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult DeleteMemo([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("RelatedInfo/CollectionFollowUp/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialData(string id)
        {
            try
            {
                ICollectionFollowUpService collectionFollowUpService = new CollectionFollowUpService(db);
                return Ok(collectionFollowUpService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region Pdc
        [HttpPost]
        [Route("RelatedInfo/pdc/GetChequeTableList/ByCompany")]
        public ActionResult GetChequeTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetChequeTableListvwByLine(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region BuyerReceiptTable
        [HttpPost]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/GetBuyerReceiptTableList/ByCompany")]
        public ActionResult GetBuyerReceiptTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBuyerRecepiptTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/Pdc/GetChequeTableById/id={id}")]
        public ActionResult GetChequeTableById(string id)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetChequeTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/GetBuyerReceiptTableById/id={id}")]
        public ActionResult GetBuyerReceiptTableById(string id)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db);
                return Ok(buyerReceiptTableService.GetBuyerReceiptTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Pdc/CreateChequeTable")]
        public ActionResult CreateChequeTable([FromBody] ChequeTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IChequeTableService chequeTableService = new ChequeTableService(db, SysTransactionLogService);
                    return Ok(chequeTableService.CreateChequeTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/CreateBuyerReceiptTable")]
        public ActionResult CreateBuyerReceiptTable([FromBody] BuyerReceiptTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db, SysTransactionLogService);
                    return Ok(buyerReceiptTableService.CreateBuyerReceiptTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/Pdc/UpdateChequeTable")]
        public ActionResult UpdateChequeTable([FromBody] ChequeTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IChequeTableService chequeTableService = new ChequeTableService(db, SysTransactionLogService);
                    return Ok(chequeTableService.UpdateChequeTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/UpdateBuyerReceiptTable")]
        public ActionResult UpdateBuyerReceiptTable([FromBody] BuyerReceiptTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db, SysTransactionLogService);
                    return Ok(buyerReceiptTableService.UpdateBuyerReceiptTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/Pdc/DeleteChequeTable")]
        public ActionResult DeleteChequeTable([FromBody] RowIdentity parm)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db, SysTransactionLogService);
                return Ok(chequeTableService.DeleteChequeTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/ Deletebuyerreceipttable")]
        public ActionResult DeleteBuyerReceiptTable([FromBody] RowIdentity parm)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db, SysTransactionLogService);
                return Ok(buyerReceiptTableService.DeleteBuyerReceiptTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/Pdc/GetRefPDCDropDown")]

        public ActionResult GetRefPDCDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetRefPDCDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("RelatedInfo/Pdc/GetChequeTableInitialData/id={id}")]
        public ActionResult GetChequeTableInitialData(string id)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                var chequeTableServiceInitaildata = chequeTableService.GetChequeTableInitialDataByPurchase(id);
                return Ok(chequeTableServiceInitaildata);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/GetBuyerReceiptInitialPurchaseLineData/id={id}")]
        public ActionResult GetBuyerReceiptInitialPurchaseLineData(string id)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTable = new BuyerReceiptTableService(db, SysTransactionLogService);
                return Ok(buyerReceiptTable.GetBuyerReceiptInitialPurchaseLineData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/Pdc/GetBankGroupDropDown")]
        public ActionResult GetBankGroupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region dropdown
        [HttpPost]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDownByBuyerReceiptTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/Pdc/GetDocumentStatusDropDown")]

        public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDocumentStatusDocumentProcessDropdown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/GetDocumentStatusDropDown")]

        public ActionResult GetDocumentStatusDropDownMessengerRequest([FromBody] SearchParameter search)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.GetDocumentStatusDocumentProcessDropdown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region function
        #region cancel buyer receipt table
        [HttpGet]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/Function/CancelBuyerReceiptTable/GetCancelBuyerReceiptTableById/id={id}")]
        public ActionResult GetCancelBuyerReceiptById(string id)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db);
                return Ok(buyerReceiptTableService.GetCancelBuyerReceiptTableById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/Function/CancelBuyerReceiptTable/CancelBuyerReceiptTable")]
        public ActionResult CancelWithdrawal([FromBody] CancelBuyerReceiptTableView view)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db);
                return Ok(buyerReceiptTableService.CancelBuyerReceiptTable(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        #endregion

        #endregion
        #region NumberSeq
        [HttpGet]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/ValidateIsManualNumberSeq/companyId={companyId}")]
        public ActionResult ValidateIsManualNumberSeq(string companyId)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTable = new BuyerReceiptTableService(db);
                return Ok(buyerReceiptTable.IsManualBuyerReceiptTable(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Attachment
        [HttpPost]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentById(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/Attachment/CreateAttachment")]
        public ActionResult CreateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/Attachment/DeleteAttachment")]
        public ActionResult DeleteAttachment([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefId([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Attachment
        #region messenger job request
        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/GetMessengerJobTableList/ByCompany")]
        public ActionResult GetMessengerJobTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMessengerJobTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/MessengerRequest/GetMessengerJobTableById/id={id}")]
        public ActionResult GetMessengerJobTableById(string id)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.GetMessengerJobTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/CreateMessengerJobTable")]
        public ActionResult CreateMessengerJobTable([FromBody] MessengerJobTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db, SysTransactionLogService);
                    return Ok(messengerJobTableService.CreateMessengerJobTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/UpdateMessengerJobTable")]
        public ActionResult UpdateMessengerJobTable([FromBody] MessengerJobTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db, SysTransactionLogService);
                    return Ok(messengerJobTableService.UpdateMessengerJobTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/DeleteMessengerJobTable")]
        public ActionResult DeleteMessengerJobTable([FromBody] RowIdentity parm)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db, SysTransactionLogService);
                return Ok(messengerJobTableService.DeleteMessengerJobTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown

        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/GetAddressTransDropDown")]
        public ActionResult GetAddressTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAddressTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/GetContactPersonTransDropDown")]
        public ActionResult GetContactPersonTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemContactPersonTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/GetCreditAppLineDropDown")]
        public ActionResult GetCreditAppLineDropDown([FromBody] SearchParameter search)
        {
            try
            {

                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                var result = creditAppTableService.GetDropDownItemCreditAppLine(search);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/GetEmployeeTableDropDown")]
        public ActionResult GetEmployeeTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/GetJobTypeDropDown")]
        public ActionResult GetJobTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemJobType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/GetMessengerJobTableDropDown")]

        public ActionResult GetMessengerJobTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMessengerJobTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/GetMessengerTableDropDown")]
        public ActionResult GetMessengerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMessengerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/GetContactPersonDropDown")]
        public ActionResult GetContactPersonDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                var result = messengerJobTableService.GetContactPersonDropDown(search);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/GetAddressTransByDropDown")]
        public ActionResult GetAddressTransByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                var result = messengerJobTableService.GetDropDownItemAddressTrans(search);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        [HttpGet]

        [Route("RelatedInfo/MessengerRequest/GetNumberSeqParameter/companyId={companyId}")]
        public ActionResult GetNumberSeqParameter(string companyId)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.GetNumberSeqParameter(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("RelatedInfo/MessengerRequest/GetMessengerJobRequestInitialData/refGUID={refGUID}/userId={userId}/companyId={companyId}")]
        public ActionResult GetMessengerJobInitialData(string refGUID, string userId, string companyId)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.GetMessengerJobRequestPuchaseLineInitialData(refGUID, userId, companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/GetMessengerTableByCustomerDropDown")]
        public ActionResult GetMessengerTableByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                search.GetParentCondition(MessengerJobCondition.CustomertableGUID);
                return Ok(sysDropDownService.GetDropDownItemMessengerJobTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/getAssignmentAgreementTableByCustomerDropDown")]
        public ActionResult GettAssignmentAgreementTableByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                search.GetParentCondition(AssignmentAgreementCondition.CustomerTableGUID);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/GetAssignmentAgreementTableByBuyerDropDown")]
        public ActionResult GetAssignmentAgreementTableByBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementTableByBuyerDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/GetAssignmentAgreementTableBySpecificDropDown")]
        public ActionResult GetAssignmentAgreementTableBySpecificDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/GetAssignmentAgreementTableDropDown")]
        public ActionResult GetAssignmentAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        #region cheque job
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/JobChequeTable/GetJobChequeList/ByCompany")]
        public ActionResult GetJobChequeListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetJobChequeListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/JobChequeTable/GetJobChequeById/id={id}")]
        public ActionResult GetJobChequeById(string id)
        {
            try
            {
                IJobChequeService jobChequeService = new JobChequeService(db);
                return Ok(jobChequeService.GetJobChequeById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/JobChequeTable/CreateJobCheque")]
        public ActionResult CreateJobCheque([FromBody] JobChequeItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJobChequeService jobChequeService = new JobChequeService(db, SysTransactionLogService);
                    return Ok(jobChequeService.CreateJobCheque(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/JobChequeTable/UpdateJobCheque")]
        public ActionResult UpdateJobCheque([FromBody] JobChequeItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJobChequeService jobChequeService = new JobChequeService(db, SysTransactionLogService);
                    return Ok(jobChequeService.UpdateJobCheque(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/JobChequeTable/DeleteJobCheque")]
        public ActionResult DeleteJobCheque([FromBody] RowIdentity parm)
        {
            try
            {
                IJobChequeService jobChequeService = new JobChequeService(db, SysTransactionLogService);
                return Ok(jobChequeService.DeleteJobCheque(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/JobChequeTable/GetBankGroupDropDown")]
        public ActionResult GetBankGroupDropDownByJobChequeTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/JobChequeTable/GetChequeTableByCustomerDropDown")]
        public ActionResult GetChequeTableByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDropDownItemChequeTableByCustomerAndDocStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/JobChequeTable/GetChequeTableByCustomerAndBuyerDropDown")]
        public ActionResult GetChequeTableByCustomerAndBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDropDownItemChequeTableByCustomerBuyerStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/JobChequeTable/GetChequeTableDropDown")]
        public ActionResult GetChequeTableByStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IChequeTableService chequeTableService = new ChequeTableService(db);
                return Ok(chequeTableService.GetDropDownItemChequeTableByDocStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/JobChequeTable/GetCollectionFollowUpByRefDropDown")]
        public ActionResult GetCollectionFollowUpDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICollectionFollowUpService collectionFollowUpService = new CollectionFollowUpService(db);
                return Ok(collectionFollowUpService.GetDropDownItemCollectionFollowUpRefAndPaymentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/JobChequeTable/GetJobChequeInitialdata/id={id}")]
        public ActionResult InitialData(string id)
        {
            try
            {
                IJobChequeService jobChequeService = new JobChequeService(db);
                var result = jobChequeService.InitialCreateJobChequeData(id);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MessengerRequest/RelatedInfo/JobChequeTable/GetMessengerJobTableDropDown")]
        public ActionResult GetMessengerJobTableDropDownByJobChequeTable([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMessengerJobTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion cheque job
        #region servicefeetrans
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/GetServiceFeeTransList/ByCompany")]
        public ActionResult GetServiceFeeTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeTransListvw(search));

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]

        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/GetServiceFeeTransById/id={id}")]
        public ActionResult GetServiceFeeTransById(string id)
        {
            try
            {
                IServiceFeeTransService serviceFeeTrans = new ServiceFeeTransService(db);
                return Ok(serviceFeeTrans.GetServiceFeeTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/GetServiceFeeTransInitialData/id={id}")]

        public ActionResult GetServiceFeeTransInitialData(string id)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.GetServiceFeeTransInitialDataByMessengerJobTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/GetInvoiceRevenueTypeByProductTypeDropDown")]

        public ActionResult GetInvoiceRevenueTypeByProductTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeByProductTypeAndDefaultNone(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/GetLedgerDimensionDropDown")]

        public ActionResult GetLedgerDimensionDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                var result = sysDropDownService.GetDropDownItemLedgerDimension(search);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/GetWithholdingTaxTableDropDown")]
        public ActionResult GetWithholdingTaxTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/GetMessengerJobTableAcessMode/messengerjobtableId={messengerjobtableId}")]
        public ActionResult GetMessengerJobTableAcessModeByServiceFeeTransByMessengerJobTable(string messengerjobtableId)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.GetMessengerJobTableAcessMode(messengerjobtableId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/GetCalculateField")]

        public ActionResult GetCalculateField([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetCalculateField(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]

        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/CreateServiceFeeTrans")]
        public ActionResult CreateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.CreateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/UpdateServiceFeeTrans")]

        public ActionResult UpdateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.UpdateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]

        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/DeleteServiceFeeTrans")]
        public ActionResult DeleteServiceFeeTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                return Ok(ServiceFeeTransService.DeleteServiceFeeTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        public ActionResult ValidateServiceFeeTrans([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.ValidateServiceFeeTrans(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        public ActionResult GetTaxValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetTaxValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        public ActionResult GetWHTValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetWHTValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByServiceFeeTrans([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        public ActionResult GetOriginalInvoiceByServiceFeeTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetOriginalInvoiceByServiceFeeTransDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region function
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/Function/AssignMessengerJobTable/UpdateAssignMessengerJob")]
        public ActionResult UpdateAssignMessengerJob([FromBody] MessengerJobTableItemView vwModel)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.UpdateAssignMessengerJob(vwModel.MessengerJobTableGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/Function/CancelMessengerJobTable/UpdateCancelMessengerJob")]

        public ActionResult UpdateCancelMessengerJob([FromBody] MessengerJobTableItemView vwModel)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.UpdateCancelMessengerJob(vwModel.MessengerJobTableGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/Function/PostMessengerJobTable/UpdatePostMessengerJob")]
        public ActionResult UpdatePostMessengerJob([FromBody] MessengerJobTableItemView vwModel)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.UpdatePostMessengerJob(vwModel.MessengerJobTableGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("RelatedInfo/messengerrequest/Function/AssignMessengerJobTable/GetAssignMessengerJobById/id={id}")]
        public ActionResult GetAssignMessengerJobById(string id)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.GetMessengerJobByIdForFunction(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/messengerrequest/Function/PostMessengerJobTable/GetPostMessengerJobById/id={id}")]
        public ActionResult GetPostMessengerJobById(string id)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.GetMessengerJobByIdForFunction(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/messengerrequest/Function/CancelMessengerJobTable/GetCancelMessengerJobById/id={id}")]
        public ActionResult GetCancelMessengerJobById(string id)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.GetMessengerJobByIdForFunction(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion function
        #region Attachment
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompanyByMessengerJob([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentByIdByMessengerJob(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/Attachment/CreateAttachment")]
        public ActionResult CreateAttachmentByMessengerJob([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachmentByMessengerJob([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/Attachment/DeleteAttachment")]
        public ActionResult DeleteAttachmentByMessengerJob([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefIdByMessengerJob([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region report
        [HttpGet]
        [Route("RelatedInfo/messengerrequest/Report/PrintMessengerJob/GetPrintMessengerJobById/id={id}")]
        public ActionResult GetPrintMessengerJobById(string id)
        {
            try
            {
                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                return Ok(messengerJobTableService.GetPrintMessengerJobById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/Report/PrintMessengerJob/RenderReport")]
        public ActionResult PrintMessengerJobReport([FromBody] PrintMessengerJobReportView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region InvoiceTable
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        public ActionResult GetInvoiceLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        public ActionResult GetInvoiceLineById(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        public ActionResult CreateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        public ActionResult UpdateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        public ActionResult DeleteInvoiceLine([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        public ActionResult UpdateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        public ActionResult DeleteInvoiceTable([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        public ActionResult GetInvoiceTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        public ActionResult GetInvoiceTableById(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        public ActionResult CreateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceTable
        #region InvoiceTableDropdown
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetInvoiceTypeDropDown")]

        public ActionResult GetInvoiceTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        public ActionResult GetBuyerInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCurrency(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        public ActionResult GetInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        public ActionResult GetProdUnitTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        public ActionResult GetTaxInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        public ActionResult GetRefInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetRefInvoiceTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusByInvoiceTableDropDown")]
        public ActionResult GetDocumentStatusByInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableServiceFeeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableServiceFeeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/GetCustomerTableByDropDown")]
       
        public ActionResult GetLoanRequestCustomerTableByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTableBy(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        public ActionResult GetLedgerDimensionServiceFeeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDownByMessengerJob([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropdown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetTaxTableDropDown")]
        public ActionResult GetTaxTableLinInvoiceDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetWithholdingTaxTableDropDown")]
        public ActionResult GetWithholdingTaxTableLineInvoiceDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetReceiptTempTableDropDown")]
        public ActionResult GetReceiptTempTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemReceiptTempTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/messengerrequest/RelatedInfo/servicefeetrans/RelatedInfo/invoicetable/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceTableDropDown

        #endregion messenger job request
        #region report
        [HttpGet]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/Report/PrintBuyerReceipt/GetPrintBuyerReceiptById/id={id}")]
        public ActionResult GetPrintBuyerReceiptById(string id)
        {
            try
            {
                IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db);
                return Ok(buyerReceiptTableService.GetPrintBuyerReceiptById(id));

            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/collectionfollowup/RelatedInfo/buyerreceipttable/Report/PrintBuyerReceipt/RenderReport")]
        public ActionResult PrintBuyerReceipt([FromBody] PrintBuyerReceiptReportView model)
        {
            try
            {
                IReportViewerService reportViewerService = new ReportViewerService();
                return Ok(reportViewerService.RenderReport(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
