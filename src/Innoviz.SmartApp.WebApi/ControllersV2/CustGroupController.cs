﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class CustGroupController : BaseControllerV2
    {
        public CustGroupController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
		[HttpPost]
		[Route("GetCustGroupList/ByCompany")]
		public ActionResult GetCustGroupListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetCustGroupListvw(search));
				//return Ok();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetCustGroupById/id={id}")]
		public ActionResult GetCustGroupById(string id)
		{
			try
			{
				ICustGroupService custGroupService = new CustGroupService(db);
				return Ok(custGroupService.GetCustGroupById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("CreateCustGroup")]
		public ActionResult CreateCustGroup([FromBody] CustGroupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICustGroupService custGroupService = new CustGroupService(db, SysTransactionLogService);
					return Ok(custGroupService.CreateCustGroup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateCustGroup")]
		public ActionResult UpdateCustGroup([FromBody] CustGroupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICustGroupService custGroupService = new CustGroupService(db, SysTransactionLogService);
					return Ok(custGroupService.UpdateCustGroup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteCustGroup")]
		public ActionResult DeleteCustGroup([FromBody] RowIdentity parm)
		{
			try
			{
				ICustGroupService custGroupService = new CustGroupService(db, SysTransactionLogService);
				return Ok(custGroupService.DeleteCustGroup(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown
		[HttpPost]
		[Route("GetNumberSeqTableDropDown")]
		public ActionResult GetNumberSeqTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemNumberSeqTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}

}

