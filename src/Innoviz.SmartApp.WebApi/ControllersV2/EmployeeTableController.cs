﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class EmployeeTableController : BaseControllerV2
	{
		public EmployeeTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetEmployeeTableList/ByCompany")]
		public ActionResult GetEmployeeTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetEmployeeTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetEmployeeTableById/id={id}")]
		public ActionResult GetEmployeeTableById(string id)
		{
			try
			{
				IEmployeeTableService employeeTableService = new EmployeeTableService(db);
				return Ok(employeeTableService.GetEmployeeTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateEmployeeTable")]
		public ActionResult CreateEmployeeTable([FromBody] EmployeeTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IEmployeeTableService employeeTableService = new EmployeeTableService(db, SysTransactionLogService);
					return Ok(employeeTableService.CreateEmployeeTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateEmployeeTable")]
		public ActionResult UpdateEmployeeTable([FromBody] EmployeeTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IEmployeeTableService employeeTableService = new EmployeeTableService(db, SysTransactionLogService);
					return Ok(employeeTableService.UpdateEmployeeTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteEmployeeTable")]
		public ActionResult DeleteEmployeeTable([FromBody] RowIdentity parm)
		{
			try
			{
				IEmployeeTableService employeeTableService = new EmployeeTableService(db, SysTransactionLogService);
				return Ok(employeeTableService.DeleteEmployeeTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("ValidateIsManualNumberSeq/companyId={companyId}")]
		public ActionResult ValidateIsManualNumberSeq(string companyId)
		{
			try
			{
				IEmployeeTableService employeeTableService = new EmployeeTableService(db);
				return Ok(employeeTableService.IsManual(companyId));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		[HttpPost]
		[Route("GetEmplTeamDropDown")]
		public ActionResult GetEmplTeamDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemEmplTeam(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetDepartmentDropDown")]
		public ActionResult GetDepartmentDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDepartment(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetLedgerDimensionDropDown")]
		public ActionResult GetLedgerDimensionDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetBusinessUnitDropDown")]
		public ActionResult GetBusinessUnitDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBusinessUnit(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetEmployeeTableDropDown")]
		public ActionResult GetEmployeeTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetEmployeeTableByNotCurrentEmpDropDown")]
		public ActionResult GetEmployeeTableByNotCurrentEmpDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IEmployeeTableService employeeTableService = new EmployeeTableService(db);
				return Ok(employeeTableService.GetEmployeeTableByNotCurrentEmpDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#region ContactTrans
		[HttpPost]
		[Route("RelatedInfo/ContactTrans/GetContactTransList/ByCompany")]
		public ActionResult GetContactListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetContactTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ContactTrans/GetContactTransById/id={id}")]
		public ActionResult GetContactById(string id)
		{
			try
			{
				IContactTransService ContactTransService = new ContactTransService(db);
				return Ok(ContactTransService.GetContactTransById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ContactTrans/CreateContactTrans")]
		public ActionResult CreateContact([FromBody] ContactTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
					return Ok(ContactTransService.CreateContactTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ContactTrans/UpdateContactTrans")]
		public ActionResult UpdateContact([FromBody] ContactTransItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
					return Ok(ContactTransService.UpdateContactTrans(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/ContactTrans/DeleteContactTrans")]
		public ActionResult DeleteContact([FromBody] RowIdentity parm)
		{
			try
			{
				IContactTransService ContactTransService = new ContactTransService(db, SysTransactionLogService);
				return Ok(ContactTransService.DeleteContactTrans(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/ContactTrans/GetContactTransInitialData/id={id}")]
		public ActionResult GetContactTransInitialData(string id)
		{
			try
			{
				IEmployeeTableService employeeTableService = new EmployeeTableService(db);
				return Ok(employeeTableService.GetContactTransInitialData(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Contact
	}
}
