using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;
namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public partial class CreditAppTableController : BaseControllerV2
    {
        #region BuyerMatcing / Loan Request
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetAccessModeByCreditAppTable/id={id}")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetAccessModeByCreditAppTable/id={id}")]
        //RelelatedInfo BookmarkDocumentTrans
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppTable/id={id}")]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppTable/id={id}")]
        public ActionResult GetAccessModeLoanRequestByCreditAppTable(string id)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetAccessModeLoanRequestByCreditAppTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCreditAppRequestTableList/ByCompany")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCreditAppRequestTableList/ByCompany")]
        public ActionResult GetLoanRequestCreditAppRequestTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppRequestTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCreditAppRequestTableById/id={id}")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCreditAppRequestTableById/id={id}")]
        public ActionResult GetLoanRequestCreditAppRequestTableById(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                var a = creditAppRequestTableService.GetCreditAppRequestTableById(id);
                return Ok(a);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/CreateCreditAppRequestTable")]
        [Route("Factoring/RelatedInfo/BuyerMatching/CreateCreditAppRequestTable")]
        public ActionResult CreateLoanRequestCreditAppRequestTable([FromBody] CreditAppRequestTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableService.CreateCreditAppRequestTable(vwModel, true));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/UpdateCreditAppRequestTable")]
        [Route("Factoring/RelatedInfo/BuyerMatching/UpdateCreditAppRequestTable")]
        public ActionResult UpdatetLoanRequestCreditAppRequestTable([FromBody] CreditAppRequestTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableService.UpdateCreditAppRequestTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/DeleteCreditAppRequestTable")]
        [Route("Factoring/RelatedInfo/BuyerMatching/DeleteCreditAppRequestTable")]
        public ActionResult UpdateLoanRequestCreditAppRequestTable([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.DeleteCreditAppRequestTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetLoanRequestCreditAppRequestTableInitialData/CreditAppId={CreditAppId}")]
        public ActionResult GetLoanRequestCreditAppRequestTableInitialData(string CreditAppId)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.GetLoanRequestOrBuyerMatchingCreditAppRequestTableInitialData(CreditAppId, CreditAppRequestType.LoanRequest));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetBuyerMatchingCreditAppRequestTableInitialData/CreditAppId={CreditAppId}")]
        public ActionResult GetBuyerMatchingCreditAppRequestTableInitialData(string CreditAppId)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.GetLoanRequestOrBuyerMatchingCreditAppRequestTableInitialData(CreditAppId, CreditAppRequestType.BuyerMatching));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Factoring/RelatedInfo/BuyerMatching/ValidateIsManualNumberSeq/productType={productType}")]
        public ActionResult ValidateLoanRequestIsManualNumberSeq(int productType)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.IsManualByCreditAppRequest(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }    
        #region Dropdown
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetAddressTransByCustomerDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetAddressTransByCustomerDropDown")]
        public ActionResult GetLoanRequestAddressTransByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressTransByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetDocumentStatusDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetDocumentStatusDropDown")]
        public ActionResult GetLoanRequestDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetDropDownItemCreditAppRequestStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetAddressTransByBuyerDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetAddressTransByBuyerDropDown")]
        public ActionResult GetLoanRequestAddressTransByBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressTransByBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetNotCancelApplicationTableByCustomerDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetNotCancelApplicationTableByCustomerDropDown")]
        public ActionResult GetLoanRequestNotCancelApplicationTableByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IApplicationService applicationService = new ApplicationService(db);
                return Ok(applicationService.GetDropDownItemNotCancelApplicationTableByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetAssignmentAgreementTableDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetAssignmentAgreementTableDropDown")]
        public ActionResult GetLoanRequestAssignmentAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetAssignmentAgreementTableByBuyerDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetAssignmentAgreementTableByBuyerDropDown")]
        public ActionResult GetLoanRequestAssignmentAgreementTableByBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDropDownItemAssignmentAgreementItemtNotStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetAssignmentMethodDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetAssignmentMethodDropDown")]
        public ActionResult GetLoanRequestAssignmentMethodDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentMethod(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetBillingResponsibleByDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetBillingResponsibleByDropDown")]
        public ActionResult GetLoanRequestBillingResponsibleByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBillingResponsibleBy(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetBlacklistStatusDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetBlacklistStatusDropDown")]
        public ActionResult GetLoanRequestBlacklistStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBlacklistStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetBuyerTableDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetBuyerTableDropDown")]
        public ActionResult GetLoanRequestBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetBuyerTableByCreditAppRequestDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetBuyerTableByCreditAppRequestDropDown")]
        public ActionResult GetLoanRequestBuyerTableByCreditAppRequestDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return Ok(buyerTableService.GetDropDownItemByCreditAppRequest(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetConsortiumTableDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetConsortiumTableDropDown")]
        public ActionResult GetLoanRequestConsortiumTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemConsortiumTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetContactPersonTransByCustomerDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetContactPersonTransByCustomerDropDown")]
        public ActionResult GetLoanRequestContactPersonTransByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db);
                return Ok(contactPersonTransService.GetDropDownItemContactPersonTransItemByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetContactPersonTransByBuyerDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetContactPersonTransByBuyerDropDown")]
        public ActionResult GetLoanRequestContactPersonTransByBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db);
                return Ok(contactPersonTransService.GetDropDownItemContactPersonTransItemByBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCreditAppRequestTableDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCreditAppRequestTableDropDown")]
        public ActionResult GetLoanRequestCreditAppRequestTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCreditAppTableByCreditAppRequestTableDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCreditAppTableByCreditAppRequestTableDropDown")]
        public ActionResult GetLoanRequestCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetDropDownItemCreditAppTableByCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCreditLimitTypeByProductTypeDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCreditLimitTypeByProductTypeDropDown")]
        public ActionResult GetLoanRequestCreditLimitTypeByProductTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditLimitTypeService creditLimitTypeService = new CreditLimitTypeService(db);
                return Ok(creditLimitTypeService.GetDropDownItemCreditLimitType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCreditLimitTypeDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCreditLimitTypeDropDown")]
        public ActionResult GetLoanRequestCreditLimitTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditLimitType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCreditScoringDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCreditScoringDropDown")]
        public ActionResult GetLoanRequestCreditScoringDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditScoring(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCreditTermDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCreditTermDropDown")]
        public ActionResult GetLoanRequestCreditTermDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditTerm(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCustBankDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCustBankDropDown")]
        public ActionResult GetLoanRequestCustBankDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustBank(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCustomerTableDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCustomerTableDropDown")]
        public ActionResult GetLoanRequestCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCustomerTableByDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCustomerTableByDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/VerificationTable/GetCustomerTableByDropDown")]
        public ActionResult GetLoanRequestCustomerTableByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTableBy(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetDocumentReasonDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetDocumentReasonDropDown")]
        public ActionResult GetLoanRequestDocumentReasonDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetInterestTypeDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetInterestTypeDropDown")]
        public ActionResult GetLoanRequestInterestTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInterestType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetKYCSetupDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetKYCSetupDropDown")]
        public ActionResult GetLoanRequestKYCSetupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemKYCSetup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetLedgerDimensionDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetLedgerDimensionDropDown")]
        public ActionResult GetLoanRequestLedgerDimensionDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetMethodOfPaymentDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetMethodOfPaymentDropDown")]
        public ActionResult GetLoanRequestMethodOfPaymentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetProductSubTypeByProductTypeDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetProductSubTypeByProductTypeDropDown")]
        public ActionResult GetLoanRequestProductSubTypeByProductTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IProductSubTypeService productSubTypeService = new ProductSubTypeService(db);
                return Ok(productSubTypeService.GetDropDownItemProductSubType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetProductSubTypeDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetProductSubTypeDropDown")]
        public ActionResult GetLoanRequestProductSubTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemProductSubType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetBusinessSegmentDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetBusinessSegmentDropDown")]
        public ActionResult GetLoanRequestBusinessSegmentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessSegment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetServiceFeeCondTemplateTableByProductTypeDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetServiceFeeCondTemplateTableByProductTypeDropDown")]
        public ActionResult GetLoanRequestServiceFeeCondTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IServiceFeeCondTemplateTableService serviceFeeCondTemplateTableService = new ServiceFeeCondTemplateTableService(db);
                return Ok(serviceFeeCondTemplateTableService.GetDropDownItemServiceFeeCondTemplateTableByProductType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCustBankByBankAccountControlDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCustBankByBankAccountControlDropDown")]
        public ActionResult GetLoanRequestCustBankByBankAccountControlDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICustBankService custBankService = new CustBankService(db);
                return Ok(custBankService.GetDropDownItemCustBankItemByBankAccountControl(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCustBankByPDCDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCustBankByPDCDropDown")]
        public ActionResult GetLoanRequestCustBankByPDCDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICustBankService custBankService = new CustBankService(db);
                return Ok(custBankService.GetDropDownItemCustBankItemByPDC(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetInterestTypeValueByCreditAppRequest")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetInterestTypeValueByCreditAppRequest")]
        public ActionResult GetLoanRequestInterestTypeValueByCreditAppRequest([FromBody] CreditAppRequestTableItemView model)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetInterestTypeValueByCreditAppRequest(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Loan Request Line
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetAccessModeByCreditAppRequestTable/CreditAppRequestLine/id={id}")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetAccessModeByCreditAppRequestTable/CreditAppRequestLine/id={id}")]
        public ActionResult GetLoanRequestAccessModeByCreditAppRequestTableStatusIsLessThanWaitingForMarketingStaff(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCreditAppRequestLineList/ByCompany")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCreditAppRequestLineList/ByCompany")]
        public ActionResult GetLoanRequestCreditAppRequestLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppRequestLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCreditAppRequestLineById/id={id}")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCreditAppRequestLineById/id={id}")]
        public ActionResult GetLoanRequestCreditAppRequestLineById(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetCreditAppRequestLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/CreateCreditAppRequestLine")]
        [Route("Factoring/RelatedInfo/BuyerMatching/CreateCreditAppRequestLine")]
        public ActionResult CreateLoanRequestCreditAppRequestLine([FromBody] CreditAppRequestLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableService.CreateCreditAppRequestLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/UpdateCreditAppRequestLine")]
        [Route("Factoring/RelatedInfo/BuyerMatching/UpdateCreditAppRequestLine")]
        public ActionResult UpdateLoanRequestCreditAppRequestLine([FromBody] CreditAppRequestLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableService.UpdateCreditAppRequestLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/DeleteCreditAppRequestLine")]
        [Route("Factoring/RelatedInfo/BuyerMatching/DeleteCreditAppRequestLine")]
        public ActionResult DeleteLoanRequestCreditAppRequestLine([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.DeleteCreditAppRequestLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetLoanRequestOrBuyerMatchingCreditAppRequestLineInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetLoanRequestOrBuyerMatchingCreditAppRequestLineInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        public ActionResult GetLoanRequestOrBuyerMatchingCreditAppRequestLineInitialData(string creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.GetLoanRequestOrBuyerMatchingCreditAppRequestLineInitialData(creditAppRequestTableGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetCreditLimitByProductByCreditAppRequest")]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetCreditLimitByProductByCreditAppRequest")]
        public ActionResult GetCreditLimitByProductByCreditAppRequest([FromBody] CreditAppRequestLineItemView model)
        {
            try
            {
                IBuyerCreditLimitByProductService buyerCreditLimitByProductService = new BuyerCreditLimitByProductService(db);
                return Ok(buyerCreditLimitByProductService.GetCreditLimitByProductByCreditAppRequest(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Loan Request Line
        #region Related BuyerMatcing,LoanRequest
        #region Attachment
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/attachment/GetAttachmentList/ByCompany")]
        
        // child
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        //creditappTable
        [Route("Factoring/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Bond/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("LCDLC/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        //creditappLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/attachment/GetAttachmentById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        // child
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        //creditappTable
        [Route("Factoring/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Bond/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("HirePurchase/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("LCDLC/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/Attachment/GetAttachmentById/id={id}")]

        //creditappLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentById(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Attachment/CreateAttachment")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/attachment/CreateAttachment")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress/RelatedInfo/Attachment/CreateAttachment")]
        // child
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment/CreateAttachment")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment/CreateAttachment")]
        //creditappTable
        [Route("Factoring/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Bond/RelatedInfo/Attachment/CreateAttachment")]
        [Route("HirePurchase/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Leasing/RelatedInfo/Attachment/CreateAttachment")]
        [Route("LCDLC/RelatedInfo/Attachment/CreateAttachment")]
        [Route("ProjectFinance/RelatedInfo/Attachment/CreateAttachment")]
        //creditappLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/Attachment/CreateAttachment")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/Attachment/CreateAttachment")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/Attachment/CreateAttachment")]

        public ActionResult CreateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/attachment/UpdateAttachment")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress/RelatedInfo/Attachment/UpdateAttachment")]
        // child
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment/UpdateAttachment")]
        //creditappTable
        [Route("Bond/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("LCDLC/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("ProjectFinance/RelatedInfo/Attachment/UpdateAttachment")]
        //creditappLine
        [Route("Bond/CreditAppLine-Child/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/attachment/DeleteAttachment")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress/RelatedInfo/Attachment/DeleteAttachment")]
        // child
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment/DeleteAttachment")]
        //creditappTable
        [Route("Bond/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("LCDLC/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("ProjectFinance/RelatedInfo/Attachment/DeleteAttachment")]
        //creditappLine
        [Route("Bond/CreditAppLine-Child/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/Attachment/DeleteAttachment")]

        public ActionResult DeleteAttachment([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/RelatedInfo/purchasetablepurchase/RelatedInfo/attachment/GetAttachmentRefId")]
        [Route("ProjectFinance/RelatedInfo/WithdrawalTableWithdrawal/RelatedInfo/ProjectProgress/RelatedInfo/Attachment/GetAttachmentRefId")]
        // child
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        //creditappTable
        [Route("Bond/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("LCDLC/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("ProjectFinance/RelatedInfo/Attachment/GetAttachmentRefId")]
        //creditappLine
        [Route("Bond/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Lcdlc/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("Projectfinance/CreditAppLine-Child/RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefId([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Attachment
        #region Memo
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        // child
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult CreditAppRequestGetMemoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        // child
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult CreditAppRequestGetMemoById(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/MemoTrans/CreateMemoTrans")]
        //child
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreditAppRequestCreateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        // child
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult CreditAppRequestUpdateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        // child
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult CreditAppRequestDeleteMemo([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult CreditAppRequestGetMemoTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult CreditAppRequestGetMemoTransChildInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.GetMemoTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Memo
        #region BookmarkDocumentTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]

        #region CloseCreditLinmitBookmarkDocument
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        #endregion
        public ActionResult GetBookmarkDocumentTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBookmarkDocumentTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        
        #region CloseCreditLinmitBookmarkDocument
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/RelatedInfo/CloseCreditLimit/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        #endregion 
        public ActionResult GetBookmarkDocumentTransById(string id)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(BookmarkDocumentTransService.GetBookmarkDocumentTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        
        #region CloseCreditLinmitBookmarkDocument
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]

        #endregion
        public ActionResult CreateBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.CreateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        
        #region CloseCreditLinmitBookmarkDocument
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]

        #endregion
        public ActionResult UpdateBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.UpdateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        
        #region CloseCreditLinmitBookmarkDocument
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]

        #endregion
        public ActionResult DeleteBookmarkDocumentTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                return Ok(BookmarkDocumentTransService.DeleteBookmarkDocumentTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        
        #region CloseCreditLinmitBookmarkDocument
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]

        #endregion
        public ActionResult GetBookmarkDocumentTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetBookmarkDocumentTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        
        #region CloseCreditLinmitBookmarkDocument
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]

        #endregion
        public ActionResult GetBookMarkDocumentTransAccessModeByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetBookMarkDocumentTransAccessModeByCreditAppRequestTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        
        #region CloseCreditLinmitBookmarkDocument
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]

        #endregion
        public ActionResult GetBookmarkDocumentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
                return Ok(bookmarkDocumentService.GetDropDownItemBookmarkDocumentByCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        
        #region CloseCreditLinmitBookmarkDocument
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]

        #endregion
        public ActionResult GetDocumentTemplateTableDropDownByDocumentType([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
                return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        
        #region CloseCreditLinmitBookmarkDocument
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]

        #endregion
        public ActionResult GetDocumentTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        
        #region CloseCreditLinmitBookmarkDocument
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]

        #endregion
        public ActionResult GetBookmarkDocumentTransDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetDropDownItemBookMarkDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #region PrintBookDocTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]

        #region CloseCreditLinmitBookmarkDocument
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]


        #endregion
        public ActionResult GetPrintBookDocTransValidation([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintBookDocTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]

        #region CloseCreditLinmitBookmarkDocument

        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/Function/printbookdoctrans/PrintBookDocTrans")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/Function/printbookdoctrans/PrintBookDocTrans")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/Function/printbookdoctrans/PrintBookDocTrans")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/Function/printbookdoctrans/PrintBookDocTrans")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/Function/printbookdoctrans/PrintBookDocTrans")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/BookmarkDocumentTrans/Function/printbookdoctrans/PrintBookDocTrans")]
        #endregion
        public async Task<ActionResult> PrintBookmarkDocumentTransaction([FromBody] PrintBookDocTransParm printBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion BookmarkDocumentTrans
        #region CreditAppReqBusinessCollateral
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralList/ByCompany")]
        public ActionResult GetCreditAppReqBusinessCollateralListByCompanyBuyerMatching([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppReqBusinessCollateralListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetCreditAppReqBusinessCollateralById/id={id}")]
        public ActionResult GetCreditAppReqBusinessCollateralByIdBuyerMatching(string id)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(CreditAppReqBusCollateralInfoService.GetCreditAppReqBusinessCollateralById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/CreateCreditAppReqBusinessCollateral")]
        public ActionResult CreateCreditAppReqBusinessCollateralBuyerMatching([FromBody] CreditAppReqBusinessCollateralItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                    return Ok(CreditAppReqBusCollateralInfoService.CreateCreditAppReqBusinessCollateral(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/UpdateCreditAppReqBusinessCollateral")]
        public ActionResult UpdateCreditAppReqBusinessCollateralBuyerMatching([FromBody] CreditAppReqBusinessCollateralItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                    return Ok(CreditAppReqBusCollateralInfoService.UpdateCreditAppReqBusinessCollateral(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/DeleteCreditAppReqBusinessCollateral")]
        public ActionResult DeleteCreditAppReqBusinessCollateralBuyerMatching([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(CreditAppReqBusCollateralInfoService.DeleteCreditAppReqBusinessCollateral(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetCreditAppReqBusColateralInitialData/creditAppRequestTableGUID={creditAppRequestTableGUID}")]
        public ActionResult GetCreditAppReqBusColateralInitialDataBuyerMatching(string creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetCreditAppReqBusinessCollateralInitialData(creditAppRequestTableGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropdownBusinessCollateral
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetBankGroupDropDown")]
        public ActionResult GetBankGroupDropDownBuyerAndLoan([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetBankTypeDropDown")]
        public ActionResult GetBankTypeDropDownBuyerAndLoan([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBankType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetNotCancelCustBusinessCollateralByCustomerDropDown")]
        public ActionResult GetNotCancelCustBusinessCollateralByCustomerDropDownBuyerAndLoan([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService creditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(creditAppReqBusCollateralInfoService.GetDropDownItemNotCancelCustBusinessCollateralByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetBusinessCollateralSubTypeByBusinessCollateralTypDropDown")]
        public ActionResult GetBusinessCollateralSubTypeByBusinessCollateralTypDropDownBuyerAndLoan([FromBody] SearchParameter search)
        {
            try
            {
                IBusinessCollateralTypeService businessCollateralTypeService = new BusinessCollateralTypeService(db);
                return Ok(businessCollateralTypeService.GetDropDownItemBusinessCollateralSubTypeByBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region CustBusinessCollateral
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralList/ByCompany")]
        public ActionResult GetCustBusinessCollateralListByCompanyBuyerMatching([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCustBusinessCollateralListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetCustBusinessCollateralById/id={id}")]
        public ActionResult GetCustBusinessCollateralByIdBuyerMatching(string id)
        {
            try
            {
                ICreditAppReqBusCollateralInfoService CreditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                return Ok(CreditAppReqBusCollateralInfoService.GetCustBusinessCollateralById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown BusinessCollateral
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetBusinessCollateralStatusDropDown")]
        public ActionResult GetBusinessCollateralStatusDropDownBuyerMatching([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetBusinessCollateralTypeDropDown")]
        public ActionResult GetBusinessCollateralTypeDropDownBuyerMatching([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBusinessCollateralType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetCreditAppRequestTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetCreditAppRequestTableDropDown")]
        public ActionResult GetBusinessCollateralCreditAppRequestTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetCustomerTableByDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetCustomerTableByDropDown")]
        public ActionResult GetBusinessCollateralCustomerTableByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region InquiryRetentionTransaction

        #endregion InquiryRetentionTransaction
        #region InquiryParentCompany

        #endregion InquiryParentCompany
        #region Assignment
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/GetCreditAppReqAssignmentList/ByCompany")]

        public ActionResult GetCreditAppReqAssignmentListByCompanyBuyerMatchingAndLoan([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppReqAssignmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/GetCreditAppReqAssignmentById/id={id}")]
        public ActionResult GetCreditAppReqAssignmentByIdBuyerMatchingAndLoan(string id)
        {
            try
            {
                ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db);
                return Ok(creditAppReqAssignmentService.GetCreditAppReqAssignmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/CreateCreditAppReqAssignment")]

        public ActionResult CreateCreditAppReqAssignmentBuyerMatchingAndLoan([FromBody] CreditAppReqAssignmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db, SysTransactionLogService);
                    return Ok(creditAppReqAssignmentService.CreateCreditAppReqAssignment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/UpdateCreditAppReqAssignment")]

        public ActionResult UpdateCreditAppReqAssignmentBuyerMatchingAndLoan([FromBody] CreditAppReqAssignmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db, SysTransactionLogService);
                    return Ok(creditAppReqAssignmentService.UpdateCreditAppReqAssignment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/DeleteCreditAppReqAssignment")]

        public ActionResult DeleteCreditAppReqAssignmentBuyerMatchingAndLoan([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db, SysTransactionLogService);
                return Ok(creditAppReqAssignmentService.DeleteCreditAppReqAssignment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/GetAssignmentAgreementOutstanding/RefGUID={refGUID}/RefType={reftype}")]
        public ActionResult GetAssignmentAgreementOutstandingBuyerMatchingAndLoan(string refGUID, int reftype)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementOutstanding(new Guid(refGUID), reftype));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/GetAccessModeByCreditAppRequestTable/id={id}")]
        public ActionResult GetAssignmentAccessModeByCreditAppRequestTableBuyerMatchingAndLoan(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        public ActionResult GetAssignmentOutstandingListByCompanyBuyerLoan([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAssignmentOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region ExistingAssignmentAgreement
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/GetExistingAssignmentAgreementList/ByCompany")]
        public ActionResult GetExistingAssignmentAgreementListByCompanyBuyerMatchingAndLoan([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementOutstanding(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ExistingAssignmentAgreement
        #region DropDown
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/GetDocumentStatusDropDown")]
        public ActionResult GetAssignmentDocumentStatusDropDownBuyerMatchingAndLoan([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDropDownItemAssignmentAgreementStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/GetBuyerTableDropDown")]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetBuyerTableDropDown")]
        public ActionResult GetAssignmentBuyerTableDropDownBuyerMatchingAndLoan([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/GetAssignmentMethodDropDown")]
        public ActionResult GetAssignmentAssignmentMethodDropDownBuyerMatchingAndLoan([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentMethod(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableDropDownBuyerMatchingAndLoan([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementLineTableByBuyerAndCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/GetAssignmentAgreementTableByCustomerAndStatusDropDown")]
        public ActionResult GetAssignmentAgreementeDropDownBuyerMatchingAndLoan([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDropDownItemAssignmentAgreementByCustomerAndStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/Assignment/GetAssignmentAgreementTableDropDown")]
        public ActionResult GetAssignmentAssignmentAgreementTableDropDownBuyerMatchingAndLoan([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropDown
        #endregion Assignment     
        #region DocumentConditionTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransList/ByCompany")]
        public ActionResult GetDocumentConditionTransListByCompanyBuyerMatching([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);

                return Ok(sysListViewService.GetDocumentConditionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransById/id={id}")]
        public ActionResult GetDocumentConditionTransByIdBuyerMatching(string id)
        {
            try
            {
                IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db);
                return Ok(DocumentConditionTransService.GetDocumentConditionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/CreateDocumentConditionTrans")]
        public ActionResult CreateDocumentConditionTransBuyerMatching([FromBody] DocumentConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
                    return Ok(DocumentConditionTransService.CreateDocumentConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/UpdateDocumentConditionTrans")]
        public ActionResult UpdateDocumentConditionTransBuyerMatching([FromBody] DocumentConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
                    return Ok(DocumentConditionTransService.UpdateDocumentConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/Workflow/StartWorkflow")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/Workflow/StartWorkflow")]
        public async Task<ActionResult> GetLoanRequestOrBuyerMatchingStartWorkflow([FromBody] WorkFlowCreditAppRequestView workflowInstance)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(await creditAppRequestTableService.ValidateAndStartWorkflow(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/DeleteDocumentConditionTrans")]
        public ActionResult DeleteDocumentConditionTransBuyerMatching([FromBody] RowIdentity parm)
        {
            try
            {
                IDocumentConditionTransService DocumentConditionTransService = new DocumentConditionTransService(db, SysTransactionLogService);
                return Ok(DocumentConditionTransService.DeleteDocumentConditionTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentConditionTransInitialData/id={id}")]
        public ActionResult GetDocumentConditionTransChildInitialDataBuyerMatching(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetDocumentConditionTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region DropdownDocumentConditionTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetDocumentTypeDropDown")]
        public ActionResult GetRelatedInfoDocumentConditionTransDocumentTypeDropDownBuyerMatching([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropdownDocumentConditionTrans
        #endregion DocumentConditionTrans
        #region ServiceFeeConditionTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        public ActionResult GetServiceFeeConditionTransListByCompanyBuyerMatcing([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeConditionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        public ActionResult GetServiceFeeConditionTransByIdBuyerMatcing(string id)
        {
            try
            {
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db);
                return Ok(serviceFeeConditionTransService.GetServiceFeeConditionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        public ActionResult CreateServiceFeeConditionTransBuyerMatcing([FromBody] ServiceFeeConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db, SysTransactionLogService);
                    return Ok(serviceFeeConditionTransService.CreateServiceFeeConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        public ActionResult UpdateServiceFeeConditionTransBuyerMatcing([FromBody] ServiceFeeConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db, SysTransactionLogService);
                    return Ok(serviceFeeConditionTransService.UpdateServiceFeeConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/Workflow/ActionWorkflow")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/Workflow/ActionWorkflow")]
        public async Task<ActionResult> GetLoanRequestOrBuyerMatchingActionWorkflow([FromBody] WorkFlowCreditAppRequestView workflowInstance)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(await creditAppRequestTableService.ValidateAndActionWorkflow(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        public ActionResult DeleteServiceFeeConditionTransBuyerMatcing([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db, SysTransactionLogService);
                return Ok(serviceFeeConditionTransService.DeleteServiceFeeConditionTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        public ActionResult GetServiceFeeConditionTransChildInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetServiceFeeConditionTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        public ActionResult GetCreditAppRequestTableProductTypeByIdServiceFeeConditionBuyerMatcing(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                var a = creditAppRequestTableService.GetCreditAppRequestTableById(id).ProductType;
                return Ok(a);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        public ActionResult GetDropDownInvoiceRevenueTypeByProductTypeServiceFeeConditionBuyerMatcing([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueType = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueType.GetInvoiceRevenueTypeByProductType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region FinancialStatementTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]

        #region FinancialStatement
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        #endregion
        #region FinancialStatement [Amend CA line]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        #endregion
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetFinancialStatementTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetFinancialStatementTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]

        #region FinancialStatement
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        #endregion
        #region FinancialStatement [Amend CA line]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        #endregion
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetFinancialStatementTransById(string id)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                return Ok(financialStatementTransService.GetFinancialStatementTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]

        #region FinancialStatement
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        #endregion
        #region FinancialStatement [Amend CA line]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        #endregion
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult CreateFinancialStatementTrans([FromBody] FinancialStatementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                    return Ok(financialStatementTransService.CreateFinancialStatementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]

        #region FinancialStatement
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        #endregion
        #region FinancialStatement [Amend CA line]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        #endregion
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult UpdateFinancialStatementTrans([FromBody] FinancialStatementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                    return Ok(financialStatementTransService.UpdateFinancialStatementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]

        #region FinancialStatement
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        #endregion
        #region FinancialStatement [Amend CA line]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        #endregion
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult DeleteFinancialStatementTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                return Ok(financialStatementTransService.DeleteFinancialStatementTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        #region FinancialStatement
        [Route("Factoring/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/AmendCa/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        #endregion
        #region BuyerMatching / LoanRequest
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        #endregion BuyerMatching / LoanRequest
        public ActionResult GetFinancialStatementTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetFinancialStatementTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        #region FinancialStatement [Amend CA line]
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        #endregion
        public ActionResult GetFinancialStatementTransChildInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetFinancialStatementTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion FinancialStatementTrans
        #region CreditOutStanding
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        public ActionResult GetCreditOutStandingListByCompanyRelated([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditOutStandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditOutStanding

        #region BuyerAgreementTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransList/ByCompany")]

        public ActionResult GetBuyerAgreementTransListByCompanyBuyerMatcing([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBuyerAgreementTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransById/id={id}")]

        public ActionResult GetBuyerAgreementTransByIdBuyerAndLoan(string id)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                return Ok(buyerAgreementTransService.GetBuyerAgreementTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/Workflow/ActionWorkflow/GetEmployeeTableWithUserDropDown")]
        public ActionResult GetLoanRequestOrBuyerMatchingEmployeeTableWithUserDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetEmployeeTableWithUserDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/CreateBuyerAgreementTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/CreateBuyerAgreementTrans")]
        public ActionResult CreateBuyerAgreementTransBuyerMatcing([FromBody] BuyerAgreementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db, SysTransactionLogService);
                    return Ok(buyerAgreementTransService.CreateBuyerAgreementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/UpdateBuyerAgreementTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/UpdateBuyerAgreementTrans")]
        public ActionResult UpdateBuyerAgreementTrans([FromBody] BuyerAgreementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db, SysTransactionLogService);
                    return Ok(buyerAgreementTransService.UpdateBuyerAgreementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/DeleteBuyerAgreementTrans")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/DeleteBuyerAgreementTrans")]
        public ActionResult DeleteBuyerAgreementTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db, SysTransactionLogService);
                return Ok(buyerAgreementTransService.DeleteBuyerAgreementTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTransInitialData/id={id}")]
        public ActionResult GetBuyerAgreementTransChildInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetBuyerAgreementTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerAgreementTrans
        #region RetensionTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/RetentionTransaction/GetRetentionTransList/ByCompany")]
        public ActionResult GetRetentionTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetRetentionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/RetentionTransaction/GetRetentionTransById/id={id}")]
        public ActionResult GetRetentionTransById(string id)
        {
            try
            {
                IRetentionTransService retentionTransService = new RetentionTransService(db);
                return Ok(retentionTransService.GetRetentionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion RetensionTrans
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/DocumentConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        // BusinessCollateral
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/BusinessCollateral/GetAccessModeByCreditAppRequestTable/id={id}")]

        public ActionResult GetCreditAppReqBusinessCollateralAccessModeByCreditAppRequestTableBuyerMatching(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetReviewCreditAppAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/FinancialStatementTrans/GetReviewCreditAppAccessModeByCreditAppRequestTable/id={id}")]
        public ActionResult GetReviewCreditAppAccessModeByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetReviewCreditAppAccessModeByCreditAppRequestTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region ServiceFeeConditionTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransList/ByCompany")]

        public ActionResult GetBuyerMatchingOrLoanRequestServiceFeeConditionTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeConditionTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransById/id={id}")]

        public ActionResult GetBuyerMatchingOrLoanRequestServiceFeeConditionTransById(string id)
        {
            try
            {
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db);
                return Ok(serviceFeeConditionTransService.GetServiceFeeConditionTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/ServiceFeeConditionTrans/CreateServiceFeeConditionTrans")]

        public ActionResult CreateBuyerMatchingOrLoanRequestServiceFeeConditionTrans([FromBody] ServiceFeeConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db, SysTransactionLogService);
                    return Ok(serviceFeeConditionTransService.CreateServiceFeeConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/ServiceFeeConditionTrans/UpdateServiceFeeConditionTrans")]

        public ActionResult UpdateBuyerMatchingOrLoanRequestServiceFeeConditionTrans([FromBody] ServiceFeeConditionTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db, SysTransactionLogService);
                    return Ok(serviceFeeConditionTransService.UpdateServiceFeeConditionTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/ServiceFeeConditionTrans/DeleteServiceFeeConditionTrans")]

        public ActionResult DeleteBuyerMatchingOrLoanRequestServiceFeeConditionTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db, SysTransactionLogService);
                return Ok(serviceFeeConditionTransService.DeleteServiceFeeConditionTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/ServiceFeeConditionTrans/GetServiceFeeConditionTransInitialData/id={id}")]

        public ActionResult GetBuyerMatchingOrLoanRequestServiceFeeConditionTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetServiceFeeConditionTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/ServiceFeeConditionTrans/GetCreditAppRequestTableProductTypeById/id={id}")]

        public ActionResult GetBuyerMatchingOrLoanRequestCreditAppRequestTableProductTypeByIdServiceFeeCondition(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                var a = creditAppRequestTableService.GetCreditAppRequestTableById(id).ProductType;
                return Ok(a);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]

        public ActionResult GetBuyerMatchingOrLoanRequestDropDownInvoiceRevenueTypeByProductTypeServiceFeeCondition([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueType = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueType.GetInvoiceRevenueTypeByProductType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/ServiceFeeConditionTrans/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetBuyerMatchingOrLoanRequestDropDownInvoiceRevenueTypeServiceFeeCondition([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/ServiceFeeConditionTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        public ActionResult GetAccessModeByCreditAppRequestTableStatusIsLessThanWaitingForMarketingStaff(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region CaBuyerCreditOutstanding
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        public ActionResult BuyerMatchingOrLoanRequestGetCAReqBuyerCreditOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCAReqBuyerCreditOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown 
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]

        public ActionResult BuyerMatchingOrLoanRequestCaBuyerCreditOutstandingGetBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        public ActionResult BuyerMatchingOrLoanRequestCaBuyerCreditOutstandingGetAssignmentAssignmentMethodDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentMethod(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        public ActionResult BuyerMatchingOrLoanRequestCaBuyerCreditOutstandingGetBillingResponsibleByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBillingResponsibleBy(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        public ActionResult BuyerMatchingOrLoanRequestCaBuyerCreditOutstandingGetMethodOfPaymentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion CaBuyerCreditOutstanding

        #endregion RelatedBuyerMatcing,LoanRequest
        #region Function BuyerMatcing / Loan Request

        #endregion Function BuyerMatcing / Loan Request
        #region Cancel credit application request
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        #region AmendCa
        [Route("Factoring/relatedinfo/AmendCa/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("Bond/relatedinfo/AmendCa/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("HirePurchase/relatedinfo/AmendCa/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("LCDLC/relatedinfo/AmendCa/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("Leasing/relatedinfo/AmendCa/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/relatedinfo/AmendCa/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        #endregion AmendCa
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        #endregion CloseCreditLimit
        public ActionResult<bool> GetCancelCreditApplicationRequestByAppReqGUID(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetCancelCreditApplicationRequestByAppReqGUID(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/Function/CancelCreditAppRequest")]
        [Route("Factoring/RelatedInfo/BuyerMatching/Function/CancelCreditAppRequest")]
        #region AmendCa
        [Route("Factoring/relatedinfo/AmendCa/Function/CancelCreditAppRequest")]
        [Route("Bond/relatedinfo/AmendCa/Function/CancelCreditAppRequest")]
        [Route("HirePurchase/relatedinfo/AmendCa/Function/CancelCreditAppRequest")]
        [Route("LCDLC/relatedinfo/AmendCa/Function/CancelCreditAppRequest")]
        [Route("Leasing/relatedinfo/AmendCa/Function/CancelCreditAppRequest")]
        [Route("ProjectFinance/relatedinfo/AmendCa/Function/CancelCreditAppRequest")]
        #endregion AmendCa
        #region AmendCaLine
        [Route("Factoring/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/CancelCreditAppRequest")]
        [Route("Bond/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/CancelCreditAppRequest")]
        [Route("HirePurchase/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/CancelCreditAppRequest")]
        [Route("LCDLC/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/CancelCreditAppRequest")]
        [Route("Leasing/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/CancelCreditAppRequest")]
        [Route("ProjectFinance/CreditAppLine-Child/RelatedInfo/AmendCaLine/Function/CancelCreditAppRequest")]
        #endregion AmendCaLine
        #region CloseCreditLimit
        [Route("Factoring/RelatedInfo/CloseCreditLimit/Function/CancelCreditAppRequest")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/Function/CancelCreditAppRequest")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/Function/CancelCreditAppRequest")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/Function/CancelCreditAppRequest")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/Function/CancelCreditAppRequest")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/Function/CancelCreditAppRequest")]
        #endregion CloseCreditLimit
        public ActionResult CancelCreditAppRequest([FromBody] CancelCreditAppRequestParamView param)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.CancelCreditApplicationRequest(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion Cancel credit application request
        #endregion Function BuyerMatcing / Loan Request
        #region WorkFlow
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetTaskBySerialNumber")]
        [Route("Factoring/RelatedInfo/BuyerMatching/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("Factoring/RelatedInfo/BuyerMatching/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetTaskBySerialNumber")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/Workflow/ActionWorkflow/GetTaskBySerialNumber")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/Workflow/StartWorkflow/GetTaskBySerialNumber")]
        public async Task<ActionResult> GetLoanRequestOrBuyerMatchingGetTaskBySerialNumber([FromBody] WorkflowInstance workflowInstance)
        {
            try
            {
                IK2Service k2Service = new K2Service(db);
                return Ok(await k2Service.GetTaskBySerialNumber(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("Factoring/RelatedInfo/BuyerMatching/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/Workflow/ActionWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/Workflow/StartWorkflow/GetWorkFlowCreditAppRequestById/id={id}")]
        public ActionResult GetLoanRequestOrBuyerMatchingWorkFlowCreditAppRequestById(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetWorkFlowCreditAppRequestById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion WorkFlow
        #region action history
        [HttpGet]
        [Route("Factoring/RelatedInfo/BuyerMatching/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/Workflow/ActionHistory/GetActionHistoryByRefGUID/id={refGUID}")]

        public ActionResult GetLoanRequestOrBuyerMatchingActionHistoryByRefGUID(string refGUID)
        {
            try
            {
                IActionHistoryService actionHistoryService = new ActionHistoryService(db);
                return Ok(actionHistoryService.GetActionHistoryByRefGUID(refGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetCreditAppRequestTableProductTypeById/id={id}")]

        public ActionResult GetCreditAppRequestTableProductTypeByIdBuyerAgreementTransBuyerMatcing(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                var a = creditAppRequestTableService.GetCreditAppRequestTableById(id).ProductType;
                return Ok(a);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CreditAppRequestLine-Child/RelatedInfo/BuyerAgreementTrans/GetBuyerAgreementTableDropDown")]

        public ActionResult GetDropDownBuyerAgreementTableBuyerAgreementTransBuyerMatcing([FromBody] SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetDropDownItemBuyerAgreementTableByBuyerAgreementTrans(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        #endregion
        #endregion action history
        #region release task k2
        [HttpPost]
        [Route("Factoring/RelatedInfo/BuyerMatching/GetReleaseTask")]
        [Route("ProjectFinance/RelatedInfo/LoanRequest/GetReleaseTask")]
        public async Task<ActionResult> GetLoanRequestOrBuyerMatchingGetReleaseTask([FromBody] WorkflowInstance workflowInstance)
        {
            try
            {
                IK2Service k2Service = new K2Service(db);
                return Ok(await k2Service.ReleaseTask(workflowInstance));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region CloseCreditLimit
        [HttpGet]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/GetCloseCreditLimitCreditAppRequestTableInitialData/CreditAppId={CreditAppId}")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/GetCloseCreditLimitCreditAppRequestTableInitialData/CreditAppId={CreditAppId}")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/GetCloseCreditLimitCreditAppRequestTableInitialData/CreditAppId={CreditAppId}")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/GetCloseCreditLimitCreditAppRequestTableInitialData/CreditAppId={CreditAppId}")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/GetCloseCreditLimitCreditAppRequestTableInitialData/CreditAppId={CreditAppId}")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/GetCloseCreditLimitCreditAppRequestTableInitialData/CreditAppId={CreditAppId}")]

        public ActionResult GetCloseCreditLimitCreditAppRequestTableInitialData(string CreditAppId)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.GetCloseCustomerCreditLimitInitialData(CreditAppId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/ValidateIsManualNumberSeq/productType={productType}")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/ValidateIsManualNumberSeq/productType={productType}")]

        public ActionResult ValidateCloseCreditLimitIsManualNumberSeq(int productType)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.IsManualByCreditAppRequest(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/CreateClosingCreditLimitRequest")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/CreateClosingCreditLimitRequest")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/CreateClosingCreditLimitRequest")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/CreateClosingCreditLimitRequest")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/CreateClosingCreditLimitRequest")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/CreateClosingCreditLimitRequest")]

        public ActionResult CreateCreditAppRequestTable([FromBody] CreditAppRequestTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableService.CreateCloseCustomerCreditLimitCreditAppRequestTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/GetClosingCreditLimitRequestList/ByCompany")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/GetClosingCreditLimitRequestList/ByCompany")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/GetClosingCreditLimitRequestList/ByCompany")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/GetClosingCreditLimitRequestList/ByCompany")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/GetClosingCreditLimitRequestList/ByCompany")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/GetClosingCreditLimitRequestList/ByCompany")]

        public ActionResult GetCreditAppReuqestListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetClosingCreditAppRequestListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/UpdateClosingCreditLimitRequest")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/UpdateClosingCreditLimitRequest")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/UpdateClosingCreditLimitRequest")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/UpdateClosingCreditLimitRequest")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/UpdateClosingCreditLimitRequest")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/UpdateClosingCreditLimitRequest")]

        public ActionResult UpdateCreditAppRequestTable([FromBody] CreditAppRequestTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableService.UpdateCreditAppRequestTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/DeleteClosingCreditLimitRequest")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/DeleteClosingCreditLimitRequest")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/DeleteClosingCreditLimitRequest")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/DeleteClosingCreditLimitRequest")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/DeleteClosingCreditLimitRequest")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/DeleteClosingCreditLimitRequest")]

        public ActionResult DeleteCreditAppRequestTable([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.DeleteCreditAppRequestLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/GetClosingCreditLimitRequestById/id={id}")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/GetClosingCreditLimitRequestById/id={id}")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/GetClosingCreditLimitRequestById/id={id}")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/GetClosingCreditLimitRequestById/id={id}")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/GetClosingCreditLimitRequestById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/GetClosingCreditLimitRequestById/id={id}")]

        public ActionResult GetCreditAppRequestTableById(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetClosingCreditAppRequestById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/GetAccessModeByCreditAppTable/id={id}")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/GetAccessModeByCreditAppTable/id={id}")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/GetAccessModeByCreditAppTable/id={id}")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/GetAccessModeByCreditAppTable/id={id}")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/GetAccessModeByCreditAppTable/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/GetAccessModeByCreditAppTable/id={id}")]

        public ActionResult GetAccessModeLoanCloseCreditLimit(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetAccessModeLoanCloseCreditLimit(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/GetDocumentStatusDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/GetDocumentStatusDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/GetDocumentStatusDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/GetDocumentStatusDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/GetDocumentStatusDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/GetDocumentStatusDropDown")]

        public ActionResult GetCloseCreditLimitDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetDropDownItemCreditAppRequestStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/GetCustomerTableDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/GetCustomerTableDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/GetCustomerTableDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/GetCustomerTableDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/GetCustomerTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/GetCustomerTableDropDown")]

        public ActionResult GetCloseCreditLimitCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/GetCreditAppTableDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/GetCreditAppTableDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/GetCreditAppTableDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/GetCreditAppTableDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/GetCreditAppTableDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/GetCreditAppTableDropDown")]

        public ActionResult GetCloseCreditLimitCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/GetCreditScoringDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/GetCreditScoringDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/GetCreditScoringDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/GetCreditScoringDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/GetCreditScoringDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/GetCreditScoringDropDown")]
        public ActionResult GetCloseCreditLimitCreditScoringDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditScoring(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/GetKYCSetupDropDown")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/GetKYCSetupDropDown")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/GetKYCSetupDropDown")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/GetKYCSetupDropDown")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/GetKYCSetupDropDown")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/GetKYCSetupDropDown")]
        public ActionResult GetCloseCreditLimitKYCSetupDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemKYCSetup(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region related
        [HttpGet]
        [Route("Factoring/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Bond/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("HirePurchase/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("LCDLC/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("Leasing/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("ProjectFinance/RelatedInfo/CloseCreditLimit/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialDataByCloseCreditLimit(string id)
        {

            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetMemoTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion related
        #endregion CloseCreditLimit

    }
}