﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class DocumentTemplateTableController : BaseControllerV2
    {
        public DocumentTemplateTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }

		[HttpPost]
		[Route("GetDocumentTemplateTableList/ByCompany")]
		public ActionResult GetDocumentTemplateTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetDocumentTemplateTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetDocumentTemplateTableById/id={id}")]
		public async Task<ActionResult> GetDocumentTemplateTableById(string id)
		{
			try
			{
				IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
				return Ok(await documentTemplateTableService.GetDocumentTemplateTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateDocumentTemplateTable")]
		public ActionResult CreateDocumentTemplateTable([FromBody] DocumentTemplateTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db, SysTransactionLogService);
					return Ok(documentTemplateTableService.CreateDocumentTemplateTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateDocumentTemplateTable")]
		public ActionResult UpdateDocumentTemplateTable([FromBody] DocumentTemplateTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db, SysTransactionLogService);
					return Ok(documentTemplateTableService.UpdateDocumentTemplateTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteDocumentTemplateTable")]
		public ActionResult DeleteDocumentTemplateTable([FromBody] RowIdentity parm)
		{
			try
			{
				IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db, SysTransactionLogService);
				return Ok(documentTemplateTableService.DeleteDocumentTemplateTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
