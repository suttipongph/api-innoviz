using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class InvoiceTableController : BaseControllerV2
	{

		public InvoiceTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetInvoiceLineList/ByCompany")]
		public ActionResult GetInvoiceLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetInvoiceLineById/id={id}")]
		public ActionResult GetInvoiceLineById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetInvoiceLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateInvoiceLine")]
		public ActionResult CreateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.CreateInvoiceLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateInvoiceLine")]
		public ActionResult UpdateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.UpdateInvoiceLine(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteInvoiceLine")]
		public ActionResult DeleteInvoiceLine([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
				return Ok(invoiceTableService.DeleteInvoiceLine(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetInvoiceTableList/ByCompany")]
		public ActionResult GetInvoiceTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				var result = sysListViewService.GetInvoiceTableListvw(search);
				return Ok(result);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetInvoiceTableById/id={id}")]
		public ActionResult GetInvoiceTableById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetInvoiceTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateInvoiceTable")]
		public ActionResult CreateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.CreateInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateInvoiceTable")]
		public ActionResult UpdateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
					return Ok(invoiceTableService.UpdateInvoiceTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteInvoiceTable")]
		public ActionResult DeleteInvoiceTable([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
				return Ok(invoiceTableService.DeleteInvoiceTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region CustTrans
        [HttpPost]
		[HttpGet]
		[Route("RelatedInfo/CustTransaction/GetInvoiceTablebyCusttrans/id={id}")]
		public ActionResult GetInvoiceTablebyCusttrans(string id)
		{
			try
			{

				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				var result = invoiceTableService.GetInvoiceTableById(id);
				return Ok(result);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/CustTransaction/GetCustTransListByInvoice/ByCompany")]
		public ActionResult GetCustTransListByCustomer([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetCustTransListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion CustTrans
        #region TaxInvoice
        [HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetTaxInvoiceTableList/ByCompany")]
		public ActionResult GetTaxInvoiceListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetTaxInvoiceTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/TaxInvoice/GetTaxInvoiceTableById/id={id}")]
		public ActionResult GetTaxInvoiceTableById(string id)
		{
			try
			{
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetTaxInvoiceTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetTaxInvoiceLineList/ByCompany")]
		public ActionResult GetTaxInvoiceLineListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetTaxInvoiceLineListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/TaxInvoice/GetTaxInvoiceLineById/id={id}")]
		public ActionResult GetTaxInvoiceLineById(string id)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetTaxInvoiceLineById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetTaxTableDropDown")]
		public ActionResult GetTaxTableDropDownTaxInvoice([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDownTaxInvoiceLine([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Report
		[HttpGet]
		[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/GetPrintTaxInvoiceById/id={id}")]
		public ActionResult GetPrintTaxInvoiceCopyById(string id)
		{
			try
			{
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetPrintTaxInvoiceCopyById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceCopy/RenderReport")]
		public ActionResult PrintTaxInvoiceCopy([FromBody] PrintTaxInvoiceReportView model)
		{
			try
			{
				IReportViewerService reportViewerService = new ReportViewerService();
				return Ok(reportViewerService.RenderReport(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/GetPrintTaxInvoiceById/id={id}")]
		public ActionResult GetPrintTaxInvoiceOriginalById(string id)
		{
			try
			{
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetPrintTaxInvoiceOriginalById(id));
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/Report/PrintTaxInvoiceOriginal/RenderReport")]
		public ActionResult PrintTaxInvoiceOriginal([FromBody] PrintTaxInvoiceReportView model)
		{
			try
			{
				IReportViewerService reportViewerService = new ReportViewerService();
				return Ok(reportViewerService.RenderReport(model));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Report
		#endregion TaxInvoice
		#region payment history
		[HttpPost]
		[Route("RelatedInfo/PaymentHistory/GetPaymentHistoryList/ByCompany")]
		public ActionResult GetPaymentHistoryListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				var result = sysListViewService.GetPaymentHistoryListvw(search);
				return Ok(result);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("RelatedInfo/PaymentHistory/GetPaymentHistoryById/id={id}")]
		public ActionResult GetPaymentHistoryById(string id)
		{
			try
			{
				IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db);
				return Ok(paymentHistoryService.GetPaymentHistoryById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Dropdown
		[Route("GetCustomerTableDropDown")]
		//payment history
		[Route("RelatedInfo/PaymentHistory/GetCustomerTableDropDown")]
		//cust trans
		[Route("RelatedInfo/CustTransaction/GetCustomerTableDropDown")]
		public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceTypeDropDown")]
		//cust trans
		[Route("RelatedInfo/CustTransaction/GetInvoiceTypeDropDown")]
		public ActionResult GetInvoiceTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetDocumentStatusDropDown")]
		[Route("GetDocumentStatusByInvoiceTableDropDown")]
		public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetBuyerTableDropDown")]
		//cust trans
		[Route("RelatedInfo/CustTransaction/GetBuyerTableDropDown")]
		public ActionResult GetBuyerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetBuyerInvoiceTableDropDown")]
		public ActionResult GetBuyerInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetMethodOfPaymentDropDown")]
		public ActionResult GetMethodOfPaymentDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetCreditAppTableDropDown")]
		//paymenmt history
		[Route("RelatedInfo/PaymentHistory/GetCreditAppTableDropDown")]
		public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetBuyerAgreementTableDropDown")]
		public ActionResult GetBuyerAgreementTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetDocumentReasonDropDown")]
		public ActionResult GetDocumentReasonDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetCurrencyDropDown")]
		//cust trans
		[Route("RelatedInfo/CustTransaction/GetCurrencyDropDown")]
		public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCurrency(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetLedgerDimensionDropDown")]
		public ActionResult GetLedgerDimensionDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetInvoiceTableDropDown")]
		//payment history
		[Route("RelatedInfo/PaymentHistory/GetInvoiceTableDropDown")]
		//cust trans
		[Route("RelatedInfo/CustTransaction/GetInvoiceTableDropDown")]
		public ActionResult GetInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetTaxTableDropDown")]
		public ActionResult GetTaxTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetWithholdingTaxTableDropDown")]
		public ActionResult GetWithholdingTaxTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetProdUnitTableDropDown")]
		public ActionResult GetProdUnitTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetTaxInvoiceTableDropDown")]
		public ActionResult GetTaxInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxInvoiceTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetRefInvoiceTableDropDown")]
		public ActionResult GetRefInvoiceTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
				return Ok(invoiceTableService.GetRefInvoiceTableDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetReceiptTempTableDropDown")]
		public ActionResult GetReceiptTempTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemReceiptTempTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
		[HttpPost]
		[Route("RelatedInfo/TaxInvoice/GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusTaxInvoiceDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				return Ok(taxInvoiceTableService.GetDropDownItemTaxInvoiceDocumentStatus(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}

}
