using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class DocumentTypeController : BaseControllerV2
	{

		public DocumentTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetDocumentTypeList/ByCompany")]
		public ActionResult GetDocumentTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetDocumentTypeListvw(search));
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetDocumentTypeById/id={id}")]
		public ActionResult GetDocumentTypeById(string id)
		{
			try
			{
				IDocumentTypeService documentTypeService = new DocumentTypeService(db);
				return Ok(documentTypeService.GetDocumentTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateDocumentType")]
		public ActionResult CreateDocumentType([FromBody] DocumentTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentTypeService documentTypeService = new DocumentTypeService(db, SysTransactionLogService);
					return Ok(documentTypeService.CreateDocumentType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateDocumentType")]
		public ActionResult UpdateDocumentType([FromBody] DocumentTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentTypeService documentTypeService = new DocumentTypeService(db, SysTransactionLogService);
					return Ok(documentTypeService.UpdateDocumentType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteDocumentType")]
		public ActionResult DeleteDocumentType([FromBody] RowIdentity parm)
		{
			try
			{
				IDocumentTypeService documentTypeService = new DocumentTypeService(db, SysTransactionLogService);
				return Ok(documentTypeService.DeleteDocumentType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
