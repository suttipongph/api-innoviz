using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class ChequeTableController : BaseControllerV2
	{

		public ChequeTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetChequeTableList/ByCompany")]
		public ActionResult GetChequeTableListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetChequeTableListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetChequeTableById/id={id}")]
		[Route("Function/Complete/GetCompleteChequeById/id={id}")]
		public ActionResult GetChequeTableById(string id)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				return Ok(chequeTableService.GetChequeTableById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateChequeTable")]
		public ActionResult CreateChequeTable([FromBody] ChequeTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IChequeTableService chequeTableService = new ChequeTableService(db, SysTransactionLogService);
					return Ok(chequeTableService.CreateChequeTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateChequeTable")]
		public ActionResult UpdateChequeTable([FromBody] ChequeTableItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IChequeTableService chequeTableService = new ChequeTableService(db, SysTransactionLogService);
					return Ok(chequeTableService.UpdateChequeTable(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteChequeTable")]
		public ActionResult DeleteChequeTable([FromBody] RowIdentity parm)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db, SysTransactionLogService);
				return Ok(chequeTableService.DeleteChequeTable(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetBankGroupDropDown")]
		public ActionResult GetBankGroupDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBankGroup(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetBuyerTableDropDown")]
		public ActionResult GetBuyerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetChequeTableDropDown")]
		public ActionResult GetChequeTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithDocumentStatusChequeTable(search, "130140"));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetCustomerTableDropDown")]
		public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetDocumentStatusDropDown")]
		public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
		{
			try
			{
					IChequeTableService chequeTableService = new ChequeTableService(db);
					return Ok(chequeTableService.GetDocumentStatusDocumentProcessDropdown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetChequeTableInitialData/id={id}")]
		public ActionResult GetChequeTableInitialData(string id)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				var temp_chequeTableService = chequeTableService.GetChequeTableInitialData(id);
				return Ok(temp_chequeTableService);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetAccessModeChequeTable/id={id}")]
		public ActionResult GetAccessModeChequeTable(string id)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				var data = chequeTableService.GetAccessModeChequeTable(id);
				return Ok(data);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetRefPDCDropDown")]
		public ActionResult GetRefPDCDropDown([FromBody] SearchParameter search)
        {
            try
            {
				IChequeTableService chequeTableService = new ChequeTableService(db);
				return Ok(chequeTableService.GetRefPDCDropDown(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("GetChequeTableByCustomerBuyerReplacedDropDown")]
		public ActionResult GetChequeRefPDCDropdown([FromBody] SearchParameter search)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				return Ok(chequeTableService.GetDropDownItemChequeTableByCustomerBuyerReplaced(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Manage Function
		#region complete
		[HttpPost]
		[Route("GetDocumentStatusValidation")]
		public ActionResult GetDocumentStatusValidation([FromBody] ChequeTableItemView view)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				return Ok(chequeTableService.GetDocumentStatusValidation(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Function/Complete/UpdateCompleteCheque")]
		public ActionResult UpdateCompleteCheque([FromBody] ChequeTableItemView view)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);

				return Ok(chequeTableService.UpdateCompleteCheque(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("Function/Complete/getInitialData/id={id}")]
		public ActionResult GetInitialDataCompleteFunction(string id)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				var data = chequeTableService.GetInitialDataCompleteFunction(id);
				return Ok(data);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion
        #region deposit
        [Route("GetDeposittStatusValidation")]
		public ActionResult GetDeposittStatusValidation([FromBody] ChequeTableItemView view)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				return Ok(chequeTableService.GetDeposittStatusValidation(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	
		[HttpGet]
		[Route("Function/Deposit/getInitialDataDepositStatus/id={id}")]
		public ActionResult GetInitialDataDepositStatus(string id)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				var data = chequeTableService.GetInitialDataDepositStatus(id);
				return Ok(data);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Function/Deposit/updateDepositStatusCheque")]
		public ActionResult UpdateDepositStatusCheque([FromBody] ChequeTableItemView view)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);

				return Ok(chequeTableService.UpdateDepositStatus(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region bounce
		[Route("GetBounceStatusValidation")]
		public ActionResult GetBounceStatusValidation([FromBody] ChequeTableItemView view)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				return Ok(chequeTableService.GetBouncetStatusValidation(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("Function/Bounce/getInitialDataBounceStatus/id={id}")]
		public ActionResult GetInitialDatBounceStatus(string id)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				var data = chequeTableService.GetInitialDataBounceStatus(id);
				return Ok(data);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Function/Bounce/updateBounceStatusCheque")]
		public ActionResult UpdateBouncetatusCheque([FromBody] ChequeTableItemView view)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);

				return Ok(chequeTableService.UpdateBounceStatus(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region replace
		[Route("GetReplaceStatusValidation")]
		public ActionResult GetReplaceStatusValidation([FromBody] ChequeTableItemView view)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				return Ok(chequeTableService.GetReplaceStatusValidation(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("Function/Replace/GetInitialDataReplaceStatus/id={id}")]
		public ActionResult GetInitialDatReplaceStatus(string id)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				var data = chequeTableService.GetInitialDataReplaceStatus(id);
				return Ok(data);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Function/Replace/UpdateReplaceStatusCheque")]
		public ActionResult UpdateReplacetatusCheque([FromBody] ChequeTableItemView view)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);

				return Ok(chequeTableService.UpdateReplaceStatus(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region return
		[Route("GetreturnStatusValidation")]
		public ActionResult GetReturnStatusValidation([FromBody] ChequeTableItemView view)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				return Ok(chequeTableService.GetReturnStatusValidation(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("Function/ReturnCheque/GetInitialDataReturnStatus/id={id}")]
		public ActionResult GetInitialDatReturnStatus(string id)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				var data = chequeTableService.GetInitialDataReturnStatus(id);
				return Ok(data);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Function/ReturnCheque/UpdateReturnStatusCheque")]
		public ActionResult UpdateReturnStatusCheque([FromBody] ChequeTableItemView view)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);

				return Ok(chequeTableService.UpdateReturnStatus(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region cancel
		[Route("GetcancelStatusValidation")]
		public ActionResult GetCancelStatusValidation([FromBody] ChequeTableItemView view)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				return Ok(chequeTableService.GetCancelStatusValidation(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("Function/CancelCheque/GetInitialDataCancelStatus/id={id}")]
		public ActionResult GetInitialDatCancelStatus(string id)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);
				var data = chequeTableService.GetInitialDataCancelStatus(id);
				return Ok(data);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("Function/CancelCheque/UpdateCancelStatusCheque")]
		public ActionResult UpdateCancelStatusCheque([FromBody] ChequeTableItemView view)
		{
			try
			{
				IChequeTableService chequeTableService = new ChequeTableService(db);

				return Ok(chequeTableService.UpdateCancelStatus(view));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion
	}
}
