﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using Innoviz.SmartApp.ReportViewer.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class ReportController : BaseControllerV2
    {
        public ReportController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }

        #region ReportWithdrawalTermExtension
        [HttpPost]
        [Route("Report/ReportWithdrawalTermExtension/RenderReport")]
        public ActionResult ReportWithdrawalTermExtension([FromBody] RptReportWithdrawalTermExtensionReportView model)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.RenderReportWithdrawalTermExtension(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Report/ReportWithdrawalTermExtension/ExportReport")]
        public ActionResult ExportReportByReportWithdrawalTermExtension([FromBody] RptReportWithdrawalTermExtensionReportView model)
        {
            try
            {
                IReportViewerService service = new ReportViewerService();
                var result = service.ExportReport(model);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Report/ReportWithdrawalTermExtension/GetCustomerTableDropdown")]
        public ActionResult GetCustomerTableDropdownByReportWithdrawalTermExtension([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropdownService = new SysDropDownService(db);
                return Ok(sysDropdownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Report/ReportWithdrawalTermExtension/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDownByReportWithdrawalTermExtension([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Report/ReportWithdrawalTermExtension/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDownByReportWithdrawalTermExtension([FromBody] SearchParameter search)
        {
            try
            {
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return Ok(withdrawalTableService.GetDropDownItemWithdrawalTableStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion ReportWithdrawalTermExtension

        #region ReportServiceFeeFactRealization
        [HttpPost]
        [Route("Report/ReportServiceFeeFactRealization/RenderReport")]
        public ActionResult ReportServiceFeeFactRealization([FromBody] RptReportServiceFeeFactRealizationReportView model)
        {
            try
            {
                IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db);
                return Ok(paymentHistoryService.RenderReportServiceFeeFactRealization(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Report/ReportServiceFeeFactRealization/ExportReport")]
        public ActionResult ExportReportByReportServiceFeeFactRealization([FromBody] RptReportServiceFeeFactRealizationReportView model)
        {
            try
            {
                IReportViewerService service = new ReportViewerService();
                var result = service.ExportReport(model);
                return Ok(result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Report/ReportServiceFeeFactRealization/GetCustomerTableDropdown")]
        public ActionResult GetCustomerTableDropdownByReportServiceFeeFactRealization([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropdownService = new SysDropDownService(db);
                return Ok(sysDropdownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Report/ReportServiceFeeFactRealization/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDownByReportServiceFeeFactRealization([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion ReportServiceFeeFactRealization
        #region ExportAging
        [HttpPost]
        [Route("Report/ExportAgingFactoring/GetExportReportAging")]
        public ActionResult ExportAgingFactoring([FromBody] ReportAgingView model)
        {
            try
            {
                ICustomerAgingService customerAgingService = new CustomerAgingService(db);
                return Ok(customerAgingService.GetReportAging(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Report/ExportAgingProjectFinance/GetExportReportAging")]
        public ActionResult ExportAgingProjectFinance([FromBody] ReportAgingView model)
        {
            try
            {
                ICustomerAgingService customerAgingService = new CustomerAgingService(db);
                return Ok(customerAgingService.GetReportAging(model));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Report/ExportAgingFactoring/GetReportAgingInitialData/companyId={companyId}")]
        public ActionResult GetReportAgingInitialDataByAgingFactoring(string companyId)
        {
            try
            {
                IAgingReportSetupService agingReportSetupService = new AgingReportSetupService(db);
                return Ok(agingReportSetupService.GetReportAgingInitialDataByAgingFactoring(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Report/ExportAgingProjectFinance/GetReportAgingInitialData/companyId={companyId}")]
        public ActionResult GetReportAgingInitialDataByAgingProjectFinance(string companyId)
        {
            try
            {
                IAgingReportSetupService agingReportSetupService = new AgingReportSetupService(db);
                return Ok(agingReportSetupService.GetReportAgingInitialDataByAgingProjectFinance(companyId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Report/ExportAgingFactoring/GetCustomerTableDropdown")]
        [Route("Report/ExportAgingProjectFinance/GetCustomerTableDropdown")]
        public ActionResult GetCustomerTableDropdownByExportAging([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropdownService = new SysDropDownService(db);
                return Ok(sysDropdownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Report/ExportAgingFactoring/GetBuyerTableDropDown")]
        [Route("Report/ExportAgingProjectFinance/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDownByExportAging([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion ExportAging
        #region ExportAccruedInterest
        [HttpPost]
        [Route("Report/ExportAccruedInterest/GetExportReportAccruedInterest")]
        public ActionResult ExportAccruedInterest([FromBody] ReportAccruedInterestView model)
        {
            try
            {
                return Ok();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("Report/ExportAccruedInterest/GetCustomerTableDropdown")]
        public ActionResult GetCustomerTableDropdownByExportAccruedInterest([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropdownService = new SysDropDownService(db);
                return Ok(sysDropdownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Report/ExportAccruedInterest/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDownByExportAccruedInterest([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Report/ExportAccruedInterest/GetInvoiceTypeByProductTypeDropDown")]
        public ActionResult GetInvoiceTypeByProductTypeDropDownByExportAccruedInterest([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTypeService invoiceTypeService = new InvoiceTypeService(db);
                return Ok(invoiceTypeService.GetDropDownItemInvoiceTypeTableByProduct(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion ExportAccruedInterest
    }
}
