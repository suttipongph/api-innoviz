using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class CalendarGroupController : BaseControllerV2
	{

		public CalendarGroupController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetCalendarGroupList/ByCompany")]
		public ActionResult GetCalendarGroupListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetCalendarGroupListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetCalendarGroupById/id={id}")]
		public ActionResult GetCalendarGroupById(string id)
		{
			try
			{
				ICalendarGroupService calendarGroupService = new CalendarGroupService(db);
				return Ok(calendarGroupService.GetCalendarGroupById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateCalendarGroup")]
		public ActionResult CreateCalendarGroup([FromBody] CalendarGroupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICalendarGroupService calendarGroupService = new CalendarGroupService(db, SysTransactionLogService);
					return Ok(calendarGroupService.CreateCalendarGroup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateCalendarGroup")]
		public ActionResult UpdateCalendarGroup([FromBody] CalendarGroupItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					ICalendarGroupService calendarGroupService = new CalendarGroupService(db, SysTransactionLogService);
					return Ok(calendarGroupService.UpdateCalendarGroup(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteCalendarGroup")]
		public ActionResult DeleteCalendarGroup([FromBody] RowIdentity parm)
		{
			try
			{
				ICalendarGroupService calendarGroupService = new CalendarGroupService(db, SysTransactionLogService);
				return Ok(calendarGroupService.DeleteCalendarGroup(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
