using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class InvoiceRevenueTypeController : BaseControllerV2
	{

		public InvoiceRevenueTypeController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetInvoiceRevenueTypeList/ByCompany")]
		public ActionResult GetInvoiceRevenueTypeListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetInvoiceRevenueTypeListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetInvoiceRevenueTypeById/id={id}")]
		public ActionResult GetInvoiceRevenueTypeById(string id)
		{
			try
			{
				IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
				return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateInvoiceRevenueType")]
		public ActionResult CreateInvoiceRevenueType([FromBody] InvoiceRevenueTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db, SysTransactionLogService);
					return Ok(invoiceRevenueTypeService.CreateInvoiceRevenueType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateInvoiceRevenueType")]
		public ActionResult UpdateInvoiceRevenueType([FromBody] InvoiceRevenueTypeItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db, SysTransactionLogService);
					return Ok(invoiceRevenueTypeService.UpdateInvoiceRevenueType(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteInvoiceRevenueType")]
		public ActionResult DeleteInvoiceRevenueType([FromBody] RowIdentity parm)
		{
			try
			{
				IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db, SysTransactionLogService);
				return Ok(invoiceRevenueTypeService.DeleteInvoiceRevenueType(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpGet]
		[Route("GetTaxValueByFeeTax/feeTaxId={feeTaxGUID}")]
		public ActionResult GetTaxValueByFeeTax(string feeTaxGUID)
		{
			try
			{
				IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
				return Ok(invoiceRevenueTypeService.GetTaxValueByFeeTax(feeTaxGUID));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		[HttpPost]
		[Route("GetIntercompanyTableDropDown")]
		public ActionResult GetIntercompanyTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemIntercompanyTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetInvoiceRevenueTypeDropDown")]
		public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetTaxTableDropDown")]
		public ActionResult GetTaxTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		[HttpPost]
		[Route("GetWithholdingTaxTableDropDown")]
		public ActionResult GetWithholdingTaxTableDropDown([FromBody] SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
	}
}
