using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
	public class BillingResponsibleByController : BaseControllerV2
	{

		public BillingResponsibleByController(SmartAppContext context, ISysTransactionLogService transactionLogService)
			: base(context, transactionLogService)
		{

		}
		[HttpPost]
		[Route("GetBillingResponsibleByList/ByCompany")]
		public ActionResult GetBillingResponsibleByListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetBillingResponsibleByListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetBillingResponsibleByById/id={id}")]
		public ActionResult GetBillingResponsibleByById(string id)
		{
			try
			{
				IBillingResponsibleByService billingResponsibleByService = new BillingResponsibleByService(db);
				return Ok(billingResponsibleByService.GetBillingResponsibleByById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateBillingResponsibleBy")]
		public ActionResult CreateBillingResponsibleBy([FromBody] BillingResponsibleByItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBillingResponsibleByService billingResponsibleByService = new BillingResponsibleByService(db, SysTransactionLogService);
					return Ok(billingResponsibleByService.CreateBillingResponsibleBy(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateBillingResponsibleBy")]
		public ActionResult UpdateBillingResponsibleBy([FromBody] BillingResponsibleByItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IBillingResponsibleByService billingResponsibleByService = new BillingResponsibleByService(db, SysTransactionLogService);
					return Ok(billingResponsibleByService.UpdateBillingResponsibleBy(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteBillingResponsibleBy")]
		public ActionResult DeleteBillingResponsibleBy([FromBody] RowIdentity parm)
		{
			try
			{
				IBillingResponsibleByService billingResponsibleByService = new BillingResponsibleByService(db, SysTransactionLogService);
				return Ok(billingResponsibleByService.DeleteBillingResponsibleBy(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown

		#endregion
	}
}
