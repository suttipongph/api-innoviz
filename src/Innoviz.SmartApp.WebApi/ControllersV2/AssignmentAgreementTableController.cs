using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class AssignmentAgreementTableController : BaseControllerV2
    {

        public AssignmentAgreementTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }

        #region assignment agreement line
        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetAssignmentAgreementLineList/ByCompany")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetAssignmentAgreementLineList/ByCompany")]
        #endregion
        [Route("AssignmentAgreement/All/GetAssignmentAgreementLineList/ByCompany")]
        [Route("AssignmentAgreement/New/GetAssignmentAgreementLineList/ByCompany")]
        public ActionResult GetAssignmentAgreementLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAssignmentAgreementLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetAssignmentAgreementLineById/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetAssignmentAgreementLineById/id={id}")]
        #endregion
        [Route("AssignmentAgreement/All/GetAssignmentAgreementLineById/id={id}")]
        [Route("AssignmentAgreement/New/GetAssignmentAgreementLineById/id={id}")]
        public ActionResult GetAssignmentAgreementLineById(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/CreateAssignmentAgreementLine")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/CreateAssignmentAgreementLine")]
        #endregion
        [Route("AssignmentAgreement/All/CreateAssignmentAgreementLine")]
        [Route("AssignmentAgreement/New/CreateAssignmentAgreementLine")]
        public ActionResult CreateAssignmentAgreementLine([FromBody] AssignmentAgreementLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db, SysTransactionLogService);
                    return Ok(assignmentAgreementTableService.CreateAssignmentAgreementLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/UpdateAssignmentAgreementLine")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/UpdateAssignmentAgreementLine")]
        #endregion
        [Route("AssignmentAgreement/All/UpdateAssignmentAgreementLine")]
        [Route("AssignmentAgreement/New/UpdateAssignmentAgreementLine")]
        public ActionResult UpdateAssignmentAgreementLine([FromBody] AssignmentAgreementLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db, SysTransactionLogService);
                    return Ok(assignmentAgreementTableService.UpdateAssignmentAgreementLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/DeleteAssignmentAgreementLine")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/DeleteAssignmentAgreementLine")]
        #endregion
        [Route("AssignmentAgreement/New/DeleteAssignmentAgreementLine")]
        public ActionResult DeleteAssignmentAgreementLine([FromBody] RowIdentity parm)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db, SysTransactionLogService);
                return Ok(assignmentAgreementTableService.DeleteAssignmentAgreementLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion

        #region assignment agreement
        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetAssignmentAgreementTableList/ByCompany")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetAssignmentAgreementTableList/ByCompany")]
        #endregion
        [Route("AssignmentAgreement/All/GetAssignmentAgreementTableList/ByCompany")]
        [Route("AssignmentAgreement/New/GetAssignmentAgreementTableList/ByCompany")]
        public ActionResult GetAssignmentAgreementTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAssignmentAgreementTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetAssignmentAgreementTableById/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetAssignmentAgreementTableById/id={id}")]
        #endregion
        [Route("AssignmentAgreement/All/GetAssignmentAgreementTableById/id={id}")]
        [Route("AssignmentAgreement/New/GetAssignmentAgreementTableById/id={id}")]
        [Route("AssignmentAgreement/all/Function/UpdateAssignmentAgreementAmount/GetUpdateAssignmentAgreementAmountById/id={id}")]
        [Route("AssignmentAgreement/new/Function/UpdateAssignmentAgreementAmount/GetUpdateAssignmentAgreementAmountById/id={id}")]
        public ActionResult GetAssignmentAgreementTableById(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/CreateAssignmentAgreementTable")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/CreateAssignmentAgreementTable")]
        #endregion
        [Route("AssignmentAgreement/All/CreateAssignmentAgreementTable")]
        [Route("AssignmentAgreement/New/CreateAssignmentAgreementTable")]
        public ActionResult CreateAssignmentAgreementTable([FromBody] AssignmentAgreementTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db, SysTransactionLogService);
                    return Ok(assignmentAgreementTableService.CreateManagementAssignmentAgreement(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/UpdateAssignmentAgreementTable")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/UpdateAssignmentAgreementTable")]
        #endregion
        [Route("AssignmentAgreement/All/UpdateAssignmentAgreementTable")]
        [Route("AssignmentAgreement/New/UpdateAssignmentAgreementTable")]
        public ActionResult UpdateAssignmentAgreementTable([FromBody] AssignmentAgreementTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db, SysTransactionLogService);
                    return Ok(assignmentAgreementTableService.UpdateAssignmentAgreementTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/DeleteAssignmentAgreementTable")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/DeleteAssignmentAgreementTable")]
        #endregion
        [Route("AssignmentAgreement/New/DeleteAssignmentAgreementTable")]
        public ActionResult DeleteAssignmentAgreementTable([FromBody] RowIdentity parm)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db, SysTransactionLogService);
                return Ok(assignmentAgreementTableService.DeleteAssignmentAgreementTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        #region notice
        [Route("AssignmentAgreement/All/Relatedinfo/Noticeofcancellation/Function/GenerateNoticeOfCancellation/IsManualNumberSeqByAssignmentAgreement")]
        [Route("AssignmentAgreement/New/Relatedinfo/Noticeofcancellation/Function/GenerateNoticeOfCancellation/IsManualNumberSeqByAssignmentAgreement")]
        #endregion
        public ActionResult IsManualNumberSeqByAssignmentAgreement()
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.IsManualByAssignmentAgreement(db.GetCompanyFilter()));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("AssignmentAgreement/All/GetVisibleManageMenu/id={id}")]
        [Route("AssignmentAgreement/New/GetVisibleManageMenu/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetVisibleManageMenu/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetVisibleManageMenu/id={id}")]
        public ActionResult GetVisibleManageMenu(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetVisibleManageMenu(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region Dropdown

        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetAssignmentMethodDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetAssignmentMethodDropDown")]
        #endregion
        [Route("AssignmentAgreement/New/RelatedInfo/Noticeofcancellation/Function/GenerateNoticeOfCancellation/GetAssignmentMethodDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/Noticeofcancellation/Function/GenerateNoticeOfCancellation/GetAssignmentMethodDropDown")]
        [Route("AssignmentAgreement/All/GetAssignmentMethodDropDown")]
        [Route("AssignmentAgreement/New/GetAssignmentMethodDropDown")]
        public ActionResult GetAssignmentMethodDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentMethod(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetBuyerAgreementTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetBuyerAgreementTableDropDown")]
        #endregion
        [Route("AssignmentAgreement/All/GetBuyerAgreementTableDropDown")]
        [Route("AssignmentAgreement/New/GetBuyerAgreementTableDropDown")]
        [Route("AssignmentAgreement/all/RelatedInfo/AssignmentAgreementSettlement/GetBuyerAgreementTableDropDown")]
        [Route("AssignmentAgreement/new/RelatedInfo/AssignmentAgreementSettlement/GetBuyerAgreementTableDropDown")]
        //Add related InvoiceTable
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerAgreementTableDropDown")]
        public ActionResult GetBuyerAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("AssignmentAgreement/All/GetBuyerAgeementTableByCustomerAndBuyerDropDown")]
        [Route("AssignmentAgreement/New/GetBuyerAgeementTableByCustomerAndBuyerDropDown")]
        public ActionResult GetBuyerAgeementTableByCustomerAndBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDropDownItemBuyerAgeementTableByCustomerAndBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetBuyerTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetBuyerTableDropDown")]
        #endregion
        [Route("AssignmentAgreement/All/GetBuyerTableDropDown")]
        [Route("AssignmentAgreement/New/GetBuyerTableDropDown")]
        //Add related InvoiceTable
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/Inquiryassignmentagreementoutstanding/GetBuyerTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/Inquiryassignmentagreementoutstanding/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetConsortiumTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetConsortiumTableDropDown")]
        #endregion
        [Route("AssignmentAgreement/All/GetConsortiumTableDropDown")]
        [Route("AssignmentAgreement/New/GetConsortiumTableDropDown")]
        public ActionResult GetConsortiumTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemConsortiumTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetCustBankByCustomerDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetCustBankByCustomerDropDown")]
        #endregion
        [Route("AssignmentAgreement/All/GetCustBankByCustomerDropDown")]
        [Route("AssignmentAgreement/New/GetCustBankByCustomerDropDown")]
        public ActionResult GetCustBankByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICustBankService custBankService = new CustBankService(db);
                return Ok(custBankService.GetDropDownItemCustBankItemByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetCustomerTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetCustomerTableDropDown")]
        #endregion
        [Route("AssignmentAgreement/All/GetCustomerTableDropDown")]
        [Route("AssignmentAgreement/New/GetCustomerTableDropDown")]
        //Add related InvoiceTable
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCustomerTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/Inquiryassignmentagreementoutstanding/GetCustomerTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/Inquiryassignmentagreementoutstanding/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/All/GetCreditAppRequestTableDropDown")]
        [Route("AssignmentAgreement/New/GetCreditAppRequestTableDropDown")]
        public ActionResult GetCreditAppRequestTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/All/getCreditAppReqAssignmentDropDown")]
        [Route("AssignmentAgreement/New/getCreditAppReqAssignmentDropDown")]
        public ActionResult getCreditAppReqAssignmentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db);
                return Ok(creditAppReqAssignmentService.GetDropDownItemCreditAppReqAssignment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/All/GetCreditAppRequestTableByAssignmentAgreementTableDropDown")]
        [Route("AssignmentAgreement/New/GetCreditAppRequestTableByAssignmentAgreementTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetCreditAppRequestTableByAssignmentAgreementTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetCreditAppRequestTableByAssignmentAgreementTableDropDown")]
        public ActionResult GetCreditAppRequestTableByAssignmentAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetDropDownItemCreditAppRequestTableByAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("AssignmentAgreement/All/GetCreditAppReqAssignmentByCreditAppRequestTableDropDown")]
        [Route("AssignmentAgreement/New/GetCreditAppReqAssignmentByCreditAppRequestTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetCreditAppReqAssignmentByCreditAppRequestTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetCreditAppReqAssignmentByCreditAppRequestTableDropDown")]
        public ActionResult GetCreditAppReqAssignmentByCreditAppRequestTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppReqAssignmentService creditAppReqAssignmentService = new CreditAppReqAssignmentService(db);
                return Ok(creditAppReqAssignmentService.GetDropDownItemCreditAppReqAssignmentByByCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion

        #region function
        [HttpGet]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetAssignmentAgreementTableInitialData")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetAssignmentAgreementTableInitialData")]
        #endregion
        [Route("AssignmentAgreement/All/GetAssignmentAgreementTableInitialData")]
        [Route("AssignmentAgreement/New/GetAssignmentAgreementTableInitialData")]
        public ActionResult GetAssignmentAgreementTableInitialData()
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementInitialData());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        #region notice
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/GetAccessModeByAssignmentTable/id={id}")]
        #endregion
        [Route("AssignmentAgreement/New/RelatedInfo/JointVentureTrans/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/JointVentureTrans/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/ConsortiumTrans/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/ConsortiumTrans/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/All/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/New/GetAccessModeByAssignmentTable/id={id}")]
        public ActionResult GetAccessModeByAssignmentTable(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAccessModeByAssignmentTableTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByAssignmentTable/id={id}")]
        public ActionResult GetAccessModeByAssignmentTableByBookmarkDocumentTrans(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAccessModeByAssignmentTableByBookmarkDocumentTrans(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("AssignmentAgreement/All/GetAssignmentAgreementLineInitialDataByParentId/id={id}")]
        [Route("AssignmentAgreement/New/GetAssignmentAgreementLineInitialDataByParentId/id={id}")]
        public ActionResult GetAssignmentAgreementLineInitialData(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementLineInitialDataByParentId(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("AssignmentAgreement/All/GetBuyerAgreementTableSumLineById/id={id}")]
        [Route("AssignmentAgreement/New/GetBuyerAgreementTableSumLineById/id={id}")]
        public ActionResult GetBuyerAgreementTableSumLineById(string id)
        {
            try
            {
                IBuyerAgreementTableService buyerAgreementTableService = new BuyerAgreementTableService(db);
                return Ok(buyerAgreementTableService.GetBuyerAgreementTableSumLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("AssignmentAgreement/All/GetDocumentStatusDropDown")]
        [Route("AssignmentAgreement/New/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusByAssignmentAgreementDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDropDownItemAssignmentAgreementStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region gen noticeOfCancellation
        [HttpGet]
        [Route("AssignmentAgreement/All/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("AssignmentAgreement/New/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetGenerateNoticeOfCancellationValidation/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetGenerateNoticeOfCancellationValidation/id={id}")]
        public ActionResult GetGenerateNoticeOfCancellationValidation(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetGenerateNoticeOfCancellationValidation(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/Relatedinfo/Noticeofcancellation/Function/GenerateNoticeOfCancellation")]
        [Route("AssignmentAgreement/All/Relatedinfo/Noticeofcancellation/Function/GenerateNoticeOfCancellation")]
        public ActionResult GenerateNoticeOfCancellation([FromBody] GenAssignmentAgmNoticeOfCancelView view)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GenerateNoticeOfCancellation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region manage agreement
        [HttpPost]
        [Route("AssignmentAgreement/New/Function/PostAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/All/Function/PostAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/New/Function/SendAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/All/Function/SendAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/New/Function/SignAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/All/Function/SignAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/New/Function/CancelAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/All/Function/CancelAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/New/Function/CloseAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/All/Function/CloseAgreement/ManageAgreement")]
        // notice
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/PostAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/PostAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/SendAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/SendAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/SignAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/SignAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/CancelAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/CancelAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/CloseAgreement/ManageAgreement")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/CloseAgreement/ManageAgreement")]
        public ActionResult ManageAgreement([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.ManageAgreement(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("AssignmentAgreement/All/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        // Notice
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/PrintSetBookDocTrans/PrintSetBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransaction([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByRefGUID(printSetBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("AssignmentAgreement/New/Function/PostAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/All/Function/PostAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/New/Function/SendAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/All/Function/SendAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/New/Function/SignAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/All/Function/SignAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/New/Function/CancelAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/All/Function/CancelAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/New/Function/CloseAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/All/Function/CloseAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/PostAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/PostAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/SendAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/SendAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/SignAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/SignAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/CancelAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/CancelAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/CloseAgreement/GetManageAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/CloseAgreement/GetManageAssignmentAgreementValidation")]
        public ActionResult GetManageAssignmentAgreementValidation([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAssignmentAgreementValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/All/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/New/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/All/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/New/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/All/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/New/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/All/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/New/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/All/Function/CloseAgreement/GetManageAgreementByRefType")]
        // Notice
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/PostAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/SendAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/SignAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/CancelAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/CloseAgreement/GetManageAgreementByRefType")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/CloseAgreement/GetManageAgreementByRefType")]
        public ActionResult GetManageAgreementByRefType([FromBody] ManageAgreementView view)
        {
            try
            {
                IAgreementService agreementService = new AgreementService(db);
                return Ok(agreementService.GetManageAgreementByRefType(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region updateAssignmentagreement
        [HttpPost]
        [Route("AssignmentAgreement/all/GetUpdateAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/new/GetUpdateAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/all/RelatedInfo/NoticeOfCancellation/GetUpdateAssignmentAgreementValidation")]
        [Route("AssignmentAgreement/new/RelatedInfo/NoticeOfCancellation/GetUpdateAssignmentAgreementValidation")]
        public ActionResult GetUpdateAssignmentagreementAmountValidation([FromBody] AssignmentAgreementTableItemView view)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetUpdateAssignmentagreementAmountValidation(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("AssignmentAgreement/all/Function/UpdateAssignmentAgreementAmount/GetDocumentReasonDropDown")]
        [Route("AssignmentAgreement/new/Function/UpdateAssignmentAgreementAmount/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByUpdateFunction([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentReasonService documentReasonService = new DocumentReasonService(db);

               
                return Ok(documentReasonService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        

        [HttpPost]
        [Route("AssignmentAgreement/all/Function/UpdateAssignmentAgreementAmount/UpdateUpdateAssignmentAgreementAmount")]
        [Route("AssignmentAgreement/new/Function/UpdateAssignmentAgreementAmount/UpdateUpdateAssignmentAgreementAmount")]

        public ActionResult UpdateUpdateAssignmentAgreementAmount([FromBody] UpdateAssignmentAgreementTableAmountView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                    return Ok(assignmentAgreementTableService.UpdateAssignmentAgreementTableAmount(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region copybookmarkdocumentfromtemplate

        [HttpPost]
        [Route("AssignmentAgreement/all/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("AssignmentAgreement/new/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("AssignmentAgreement/all/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]
        [Route("AssignmentAgreement/new/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/GetbookmarkDocumentTemplateTableDropDownByRefType")]

        public ActionResult GetBookmarkDocumentTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDropDownItemBookmarkDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/all/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("AssignmentAgreement/new/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("AssignmentAgreement/all/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]
        [Route("AssignmentAgreement/new/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate")]

        public ActionResult CopyBookmarkDocumentFromTemplate([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.CopyBookmarkDocumentFromTemplate(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/all/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/getCopyBookmarkDocumentValidation")]
        [Route("AssignmentAgreement/new/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/getCopyBookmarkDocumentValidation")]
        [Route("AssignmentAgreement/all/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/getCopyBookmarkDocumentValidation")]
        [Route("AssignmentAgreement/new/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/Function/CopyBookmarkDocumentFromTemplate/getCopyBookmarkDocumentValidation")]
        public ActionResult GetCopyBookmarkDocumentValidation([FromBody] BookmarkDocumentTemplateTableItemView param)
        {
            try
            {
                IBookmarkDocumentTemplateTableService bookmarkDocumentTemplateTableService = new BookmarkDocumentTemplateTableService(db);
                return Ok(bookmarkDocumentTemplateTableService.GetCopyBookmarkDocumentValidation(param));
            }
                catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        #endregion

        #region bookmark document tran
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        public ActionResult GetBookmarkDocumentTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBookmarkDocumentTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        public ActionResult GetBookmarkDocumentTransById(string id)
        {
            try
            {
                IBookmarkDocumentTransService assignmentMethodService = new BookmarkDocumentTransService(db);
                return Ok(assignmentMethodService.GetBookmarkDocumentTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        public ActionResult CreateBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService assignmentMethodService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(assignmentMethodService.CreateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        public ActionResult UpdateBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService assignmentMethodService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(assignmentMethodService.UpdateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        public ActionResult DeleteBookmarkDocumentTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IBookmarkDocumentTransService assignmentMethodService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                return Ok(assignmentMethodService.DeleteBookmarkDocumentTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        public ActionResult GetBookmarkDocumentTransInitialData(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetBookmarkDocumentTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        public ActionResult GetBookmarkDocumentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
                return Ok(bookmarkDocumentService.GetDropDownItemBookmarkDocumentByAssignmentAgreement(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        public ActionResult GetBookmarkDocumentTransDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetDropDownItemBookMarkDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        public ActionResult GetDocumentTemplateTableDropDownByDocumentType([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
                return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        public ActionResult GetDocumentTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region PrintBookDocTrans
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("AssignmentAgreement/All/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        public ActionResult GetPrintBookDocTransValidation([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintBookDocTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookDocTrans/PrintBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransaction([FromBody] PrintBookDocTransParm printBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion

        #region ConsortiumTrans
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/GetConsortiumTransList/ByCompany")]
        public ActionResult GetConsortiumTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetConsortiumTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/GetConsortiumTransById/id={id}")]
        public ActionResult GetConsortiumTransById(string id)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db);
                return Ok(ConsortiumTransService.GetConsortiumTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/CreateConsortiumTrans")]
        public ActionResult CreateConsortiumTrans([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.CreateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/UpdateConsortiumTrans")]
        public ActionResult UpdateConsortiumTrans([FromBody] ConsortiumTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                    return Ok(ConsortiumTransService.UpdateConsortiumTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/DeleteConsortiumTrans")]
        public ActionResult DeleteConsortiumTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IConsortiumTransService ConsortiumTransService = new ConsortiumTransService(db, SysTransactionLogService);
                return Ok(ConsortiumTransService.DeleteConsortiumTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/GetConsortiumTransInitialData/id={id}")]
        public ActionResult GetConsortiumTransInitialData(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db, SysTransactionLogService);
                return Ok(assignmentAgreementTableService.GetConsortiumTransInitialDataByAssignmentAgreementTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetConsortiumTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ConsortiumTrans/GetConsortiumLineByConsortiumTableDropDown")]
        public ActionResult GetConsortiumTransConsortiumLineDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IConsortiumTableService consortiumTableService = new ConsortiumTableService(db);
                return Ok(consortiumTableService.GetDropDownItemConsortiumLineByConsortiumTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion ConsortiumTrans

        #region AgreementTableInfo
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoList/ByCompany")]
        public ActionResult GetAgreementTableInfoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAgreementTableInfoListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoById/id={id}")]
        public ActionResult GetAgreementTableInfoById(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]

        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/CreateAgreementTableInfo")]
        public ActionResult CreateAgreementTableInfo([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.CreateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]

        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/UpdateAgreementTableInfo")]
        public ActionResult UpdateAgreementTableInfo([FromBody] AgreementTableInfoItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                    return Ok(AgreementTableInfoService.UpdateAgreementTableInfo(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]

        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/DeleteAgreementTableInfo")]
        public ActionResult DeleteAgreementTableInfo([FromBody] RowIdentity parm)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db, SysTransactionLogService);
                return Ok(AgreementTableInfoService.DeleteAgreementTableInfo(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("AssignmentAgreement/New/GetAgreementTableInfoByAssignmentAgreement/id={id}")]
        [Route("AssignmentAgreement/All/GetAgreementTableInfoByAssignmentAgreement/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/GetAgreementTableInfoByAssignmentAgreement/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/GetAgreementTableInfoByAssignmentAgreement/id={id}")]
        public ActionResult GetAgreementTableInfoByAssignmentAgreement(string id)
        {
            try
            {
                IAgreementTableInfoService AgreementTableInfoService = new AgreementTableInfoService(db);
                return Ok(AgreementTableInfoService.GetAgreementTableInfoByAssignmentAgreement(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAgreementTableInfoInitialData/id={id}")]
        public ActionResult GetAgreementTableInfoInitialData(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db, SysTransactionLogService);
                return Ok(assignmentAgreementTableService.GetAgreementTableInfoInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AgreementTableInfo
        #region AgreementTableSettlement
        [HttpPost]
        [Route("AssignmentAgreement/all/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementSettleList/ByCompany")]
        [Route("AssignmentAgreement/new/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementSettleList/ByCompany")]
        public ActionResult GetAssignmentAgreementSettleListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAssignmentAgreementSettleListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("AssignmentAgreement/all/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementSettleById/id={id}")]
        [Route("AssignmentAgreement/new/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementSettleById/id={id}")]
        [Route("AssignmentAgreement/all/RelatedInfo/AssignmentAgreementSettlement/Function/UpdateAssignmentAgreementSettle/GetUpdateAssignmentAgreementSettleById/id={id}")]
        [Route("AssignmentAgreement/new/RelatedInfo/AssignmentAgreementSettlement/Function/UpdateAssignmentAgreementSettle/GetUpdateAssignmentAgreementSettleById/id={id}")]
        public ActionResult GetAssignmentAgreementSettleById(string id)
        {
            try
            {
                IAssignmentAgreementSettleService assignmentAgreementSettleService = new AssignmentAgreementSettleService(db);
                return Ok(assignmentAgreementSettleService.GetAssignmentAgreementSettleById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/all/RelatedInfo/AssignmentAgreementSettlement/CreateAssignmentAgreementSettle")]
        [Route("AssignmentAgreement/new/RelatedInfo/AssignmentAgreementSettlement/CreateAssignmentAgreementSettle")]
        public ActionResult CreateAssignmentAgreementSettle([FromBody] AssignmentAgreementSettleItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAssignmentAgreementSettleService assignmentAgreementSettleService = new AssignmentAgreementSettleService(db, SysTransactionLogService);
                    return Ok(assignmentAgreementSettleService.CreateAssignmentAgreementSettle(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/all/RelatedInfo/AssignmentAgreementSettlement/UpdateAssignmentAgreementSettle")]
        [Route("AssignmentAgreement/new/RelatedInfo/AssignmentAgreementSettlement/UpdateAssignmentAgreementSettle")]
        public ActionResult UpdateAssignmentAgreementSettle([FromBody] AssignmentAgreementSettleItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAssignmentAgreementSettleService assignmentAgreementSettleService = new AssignmentAgreementSettleService(db, SysTransactionLogService);
                    return Ok(assignmentAgreementSettleService.UpdateAssignmentAgreementSettle(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("AssignmentAgreement/all/RelatedInfo/AssignmentAgreementSettlement/Function/UpdateAssignmentAgreementSettle/UpdateUpdateAssignmentAgreementSettle")]
        [Route("AssignmentAgreement/new/RelatedInfo/AssignmentAgreementSettlement/Function/UpdateAssignmentAgreementSettle/UpdateUpdateAssignmentAgreementSettle")]
        public ActionResult UpdateUpdateAssignmentAgreementAmount([FromBody] UpdateAssignmentAgreementSettleView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAssignmentAgreementSettleService assignmentAgreementSettleService = new AssignmentAgreementSettleService(db);
                    return Ok(assignmentAgreementSettleService.UpdateAssignmentSettlement(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AgreementTableSettlement
        #region Dropdown
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAddressTransByCustomerDropDown")]
        public ActionResult GetAgreementTableInfoAddressTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return Ok(addressTransService.GetDropDownItemAddressTransByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByCustomerDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTransByCustomerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(authorizedPersonTransService.GetDropDownItemAuthorizedPersonTransByCustomer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTransByBuyerDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTransByBuyerDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                return Ok(authorizedPersonTransService.GetDropDownItemAuthorizedPersonTransByBuyer(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetAgreementTableInfoAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetCompanyBankDropDown")]
        public ActionResult GetAgreementTableInfoCompanyBankDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCompanyBank(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetEmployeeTableWithCompanySignature")]
        public ActionResult GetAgreementTableInfoCompanySignatureDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                return Ok(employeeTableService.GetEmployeeTableWithCompanySignatureDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/AgreementTableInfo/GetEmployeeTableDropDown")]
        public ActionResult GetAgreementTableInfoEmployeeTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemEmployeeTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetAssignmentAgreementSettleDropDown")]
        [Route("AssignmentAgreement/all/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementSettleDropDown")]
        [Route("AssignmentAgreement/new/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementSettleDropDown")]
        public ActionResult GetAssignmentAgreementSettleDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementSettle(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetAssignmentAgreementTableDropDown")]
        [Route("AssignmentAgreement/all/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementTableDropDown")]
        [Route("AssignmentAgreement/new/RelatedInfo/AssignmentAgreementSettlement/GetAssignmentAgreementTableDropDown")]
        public ActionResult GetAssignmentAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("GetDocumentReasonDropDown")]
        [Route("AssignmentAgreement/all/RelatedInfo/AssignmentAgreementSettlement/GetDocumentReasonDropDown")]
        [Route("AssignmentAgreement/new/RelatedInfo/AssignmentAgreementSettlement/GetDocumentReasonDropDown")]
        //Add related InvoiceTable
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentReasonDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/AssignmentAgreementSettlement/Function/UpdateAssignmentAgreementSettle/GetDocumentReasonDropDown")]
        [Route("AssignmentAgreement/new/RelatedInfo/AssignmentAgreementSettlement/Function/UpdateAssignmentAgreementSettle/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region JointVentureTrans
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/GetJointVentureTransList/ByCompany")]
        public ActionResult GetJointVentureTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetJointVentureTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/GetJointVentureTransById/id={id}")]
        public ActionResult GetJointVentureTransById(string id)
        {
            try
            {
                IJointVentureTransService JointVentureTransService = new JointVentureTransService(db);
                return Ok(JointVentureTransService.GetJointVentureTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/CreateJointVentureTrans")]
        public ActionResult CreateJointVentureTrans([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.CreateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/UpdateJointVentureTrans")]
        public ActionResult UpdateJointVentureTrans([FromBody] JointVentureTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                    return Ok(JointVentureTransService.UpdateJointVentureTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/DeleteJointVentureTrans")]
        public ActionResult DeleteJointVentureTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IJointVentureTransService JointVentureTransService = new JointVentureTransService(db, SysTransactionLogService);
                return Ok(JointVentureTransService.DeleteJointVentureTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/GetJointVentureTransInitialData/id={id}")]
        public ActionResult GetJointVentureTransInitialData(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db, SysTransactionLogService);
                return Ok(assignmentAgreementTableService.GetJointVentureTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/JointVentureTrans/GetAuthorizedPersonTypeDropDown")]
        public ActionResult GetJointVentureTransAuthorizedPersonTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAuthorizedPersonType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("AssignmentAgreement/New/Function/PostAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/All/Function/PostAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/New/Function/SendAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/All/Function/SendAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/New/Function/SignAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/All/Function/SignAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/New/Function/CancelAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/All/Function/CancelAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/New/Function/CloseAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/All/Function/CloseAgreement/GetDocumentReasonDropdown")]
        // notice
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/PostAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/PostAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/SendAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/SendAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/SignAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/SignAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/CancelAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/CancelAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/Function/CloseAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/Function/CloseAgreement/GetDocumentReasonDropdown")]
        [Route("AssignmentAgreement/New/Relatedinfo/Noticeofcancellation/Function/GenerateNoticeOfCancellation/GetDocumentReasonDropDown")]
        [Route("AssignmentAgreement/All/Relatedinfo/Noticeofcancellation/Function/GenerateNoticeOfCancellation/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropdown([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetDocumentReasonDropdown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion JointVentureTrans

        #region ServiceFeeTrans
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransList/ByCompany")]
        public ActionResult GetServiceFeeTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetServiceFeeTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransById/id={id}")]
        public ActionResult GetServiceFeeTransById(string id)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetServiceFeeTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/CreateServiceFeeTrans")]
        public ActionResult CreateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.CreateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/UpdateServiceFeeTrans")]
        public ActionResult UpdateServiceFeeTrans([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                    return Ok(ServiceFeeTransService.UpdateServiceFeeTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/DeleteServiceFeeTrans")]
        public ActionResult DeleteServiceFeeTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db, SysTransactionLogService);
                return Ok(ServiceFeeTransService.DeleteServiceFeeTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetServiceFeeTransInitialData/id={id}")]
        public ActionResult GetServiceFeeTransInitialData(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db, SysTransactionLogService);
                return Ok(assignmentAgreementTableService.GetServiceFeeTransInitialDataByAssignmentAgreementTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/GetAccessModeByAssignmentAgreementTable/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetAccessModeByAssignmentAgreementTable/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/GetAccessModeByAssignmentAgreementTable/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetAccessModeByAssignmentAgreementTable/id={id}")]
        public ActionResult GetAccessModeByAssignmentAgreementTable(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAccessModeByStatusLessThanSigned(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/GetCalculateField")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetCalculateField")]

        public ActionResult GetCalculateField([FromBody] ServiceFeeTransItemView vwModel)
        {
            try
            {
                IServiceFeeTransService ServiceFeeTransService = new ServiceFeeTransService(db);
                return Ok(ServiceFeeTransService.GetCalculateField(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/ValidateServiceFeeTrans")]
        public ActionResult ValidateServiceFeeTrans([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.ValidateServiceFeeTrans(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetTaxValue")]
        public ActionResult GetTaxValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetTaxValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetWHTValue")]
        public ActionResult GetWHTValue([FromBody] GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                return Ok(serviceFeeTransService.GetWHTValue(getTaxValueAndWHTValueParm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetInvoiceRevenueTypeByProductTypeDropDown")]

        public ActionResult GetInvoiceRevenueTypeByProductTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeService invoiceRevenueTypeService = new InvoiceRevenueTypeService(db);
                return Ok(invoiceRevenueTypeService.GetInvoiceRevenueTypeByProductTypeAndDefaultNone(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetLedgerDimensionDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetLedgerDimensionDropDown")]
        public ActionResult GetLedgerDimensionDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemLedgerDimension(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetTaxTableDropDown")]
        //Add related InvoiceTable
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxTableDropDown")]
        public ActionResult GetTaxTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetWithholdingTaxTableDropDown")]
        //Add related InvoiceTable
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetWithholdingTaxTableDropDown")]
        public ActionResult GetWithholdingTaxTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetDocumentReasonDropDown")]
        public ActionResult GetDocumentReasonDropDownByServiceFeeTrans([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/GetOriginalInvoiceDropDown")]
        public ActionResult GetOriginalInvoiceByServiceFeeTransDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetOriginalInvoiceByServiceFeeTransDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion ServiceFeeTrans

        #region InvoiceTable
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableList/ByCompany")]
        public ActionResult GetInvoiceTableListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceTableListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableById/id={id}")]
        public ActionResult GetInvoiceTableById(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceTable")]
        public ActionResult CreateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceTable(vwModel));

                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceTable")]
        public ActionResult UpdateInvoiceTable([FromBody] InvoiceTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceTable")]
        public ActionResult DeleteInvoiceTable([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceTable(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/DeleteInvoiceLine")]
        public ActionResult DeleteInvoiceLine([FromBody] RowIdentity parm)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                return Ok(invoiceTableService.DeleteInvoiceLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineList/ByCompany")]
        public ActionResult GetInvoiceLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetInvoiceLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceLineById/id={id}")]
        public ActionResult GetInvoiceLineById(string id)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetInvoiceLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/CreateInvoiceLine")]
        public ActionResult CreateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.CreateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/UpdateInvoiceLine")]
        public ActionResult UpdateInvoiceLine([FromBody] InvoiceLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IInvoiceTableService invoiceTableService = new InvoiceTableService(db, SysTransactionLogService);
                    return Ok(invoiceTableService.UpdateInvoiceLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceTable
        #region DropdownInvoiceTable
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTypeDropDown")]
        public ActionResult GetInvoiceTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetDropDownItemInvoiceStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetBuyerInvoiceTableDropDown")]
        public ActionResult GetBuyerInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetMethodOfPaymentDropDown")]
        public ActionResult GetMethodOfPaymentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetCurrencyDropDown")]
        public ActionResult GetCurrencyDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCurrency(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceRevenueTypeDropDown")]
        public ActionResult GetInvoiceRevenueTypeDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceRevenueType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetInvoiceTableDropDown")]
        public ActionResult GetInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetProdUnitTableDropDown")]
        public ActionResult GetProdUnitTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemWithholdingTaxTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetTaxInvoiceTableDropDown")]
        public ActionResult GetTaxInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemTaxInvoiceTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/New/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/ServiceFeeTrans/RelatedInfo/InvoiceTable/GetRefInvoiceTableDropDown")]
        public ActionResult GetRefInvoiceTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                return Ok(invoiceTableService.GetRefInvoiceTableDropDown(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DropdownInvoiceTable

        #region MemoTrans
        [HttpGet]
        [Route("AssignmentAgreement/all/RelatedInfo/MemoTrans/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/all/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/new/RelatedInfo/MemoTrans/GetAccessModeByAssignmentTable/id={id}")]
        [Route("AssignmentAgreement/new/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/GetAccessModeByAssignmentTable/id={id}")]
        public ActionResult GetAccessModeByAssignmentTableTable(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);

                return Ok(assignmentAgreementTableService.GetAccessModeByAssignmentTableTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("AssignmentAgreement/new/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("AssignmentAgreement/new/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("AssignmentAgreement/all/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        [Route("AssignmentAgreement/all/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/GetMemoTransList/ByCompany")]
        public ActionResult GetMemoListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("AssignmentAgreement/new/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("AssignmentAgreement/new/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("AssignmentAgreement/all/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        [Route("AssignmentAgreement/all/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/GetMemoTransById/id={id}")]
        public ActionResult GetMemoById(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/MemoTrans/CreateMemoTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/CreateMemoTrans")]
        public ActionResult CreateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/UpdateMemoTrans")]
        public ActionResult UpdateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/DeleteMemoTrans")]
        public ActionResult DeleteMemo([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("AssignmentAgreement/New/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        [Route("AssignmentAgreement/All/RelatedInfo/NoticeOfCancellation/RelatedInfo/MemoTrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialData(string id)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetMemoTransInitialData(id));

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region Attachment
        [HttpPost]
        [Route("AssignmentAgreement/new/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("AssignmentAgreement/new/RelatedInfo/NoticeOfCancellation/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("AssignmentAgreement/all/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        [Route("AssignmentAgreement/all/RelatedInfo/NoticeOfCancellation/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("AssignmentAgreement/all/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("AssignmentAgreement/all/RelatedInfo/NoticeOfCancellation/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("AssignmentAgreement/new/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        [Route("AssignmentAgreement/new/RelatedInfo/NoticeOfCancellation/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentById(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/all/RelatedInfo/Attachment/CreateAttachment")]
        [Route("AssignmentAgreement/all/RelatedInfo/NoticeOfCancellation/RelatedInfo/Attachment/CreateAttachment")]
        [Route("AssignmentAgreement/new/RelatedInfo/Attachment/CreateAttachment")]
        [Route("AssignmentAgreement/new/RelatedInfo/NoticeOfCancellation/RelatedInfo/Attachment/CreateAttachment")]
        public ActionResult CreateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/all/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("AssignmentAgreement/all/RelatedInfo/NoticeOfCancellation/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("AssignmentAgreement/new/RelatedInfo/Attachment/UpdateAttachment")]
        [Route("AssignmentAgreement/new/RelatedInfo/NoticeOfCancellation/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/all/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("AssignmentAgreement/all/RelatedInfo/NoticeOfCancellation/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("AssignmentAgreement/new/RelatedInfo/Attachment/DeleteAttachment")]
        [Route("AssignmentAgreement/new/RelatedInfo/NoticeOfCancellation/RelatedInfo/Attachment/DeleteAttachment")]
        public ActionResult DeleteAttachment([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("AssignmentAgreement/all/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("AssignmentAgreement/all/RelatedInfo/NoticeOfCancellation/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("AssignmentAgreement/new/RelatedInfo/Attachment/GetAttachmentRefId")]
        [Route("AssignmentAgreement/new/RelatedInfo/NoticeOfCancellation/RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefId([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion 

        #region Outstanding
        [HttpPost]
        [Route("AssignmentAgreement/All/RelatedInfo/Inquiryassignmentagreementoutstanding/GetInquiryAssignmentOutstandingList/ByCompany")]
        [Route("AssignmentAgreement/New/RelatedInfo/Inquiryassignmentagreementoutstanding/GetInquiryAssignmentOutstandingList/ByCompany")]
        public ActionResult GetInquiryAssignmentOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                return Ok(assignmentAgreementTableService.GetAssignmentAgreementOutstanding(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Oustanding
    }
}
