﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class DocumentReasonController : BaseControllerV2
    {
        public DocumentReasonController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
		[HttpPost]
		[Route("GetDocumentReasonList/ByCompany")]
		public ActionResult GetDocumentReasonListByCompany([FromBody] SearchParameter search)
		{
			try
			{
				ISysListViewService sysListViewService = new SysListViewService(db);
				return Ok(sysListViewService.GetDocumentReasonListvw(search));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpGet]
		[Route("GetDocumentReasonById/id={id}")]
		public ActionResult GetDocumentReasonById(string id)
		{
			try
			{
				IDocumentReasonService documentReasonService = new DocumentReasonService(db);
				return Ok(documentReasonService.GetDocumentReasonById(id));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("CreateDocumentReason")]
		public ActionResult CreateDocumentReason([FromBody] DocumentReasonItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentReasonService documentReasonService = new DocumentReasonService(db, SysTransactionLogService);
					return Ok(documentReasonService.CreateDocumentReason(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("UpdateDocumentReason")]
		public ActionResult UpdateDocumentReason([FromBody] DocumentReasonItemView vwModel)
		{
			try
			{
				if (ModelState.IsValid)
				{
					IDocumentReasonService documentReasonService = new DocumentReasonService(db, SysTransactionLogService);
					return Ok(documentReasonService.UpdateDocumentReason(vwModel));
				}
				else
				{
					SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
					foreach (var k in ModelState.Keys)
					{
						ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
					}
					throw SmartAppUtil.AddStackTrace(ex);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		[HttpPost]
		[Route("DeleteDocumentReason")]
		public ActionResult DeleteDocumentReason([FromBody] RowIdentity parm)
		{
			try
			{
				IDocumentReasonService documentReasonService = new DocumentReasonService(db, SysTransactionLogService);
				return Ok(documentReasonService.DeleteDocumentReason(parm.Guid));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Dropdown
		#endregion
	}
}
