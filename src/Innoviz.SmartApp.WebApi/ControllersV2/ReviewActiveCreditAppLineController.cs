﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public partial class ReviewActiveCreditAppLineController : BaseControllerV2
    {

        public ReviewActiveCreditAppLineController(SmartAppContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService)
        {

        }
        [HttpPost]
        [Route("Factoring/GetCreditAppLineList/ByCompany")]
        [Route("Bond/GetCreditAppLineList/ByCompany")]
        [Route("HirePurchase/GetCreditAppLineList/ByCompany")]
        [Route("LCDLC/GetCreditAppLineList/ByCompany")]
        [Route("Leasing/GetCreditAppLineList/ByCompany")]
        [Route("ProjectFinance/GetCreditAppLineList/ByCompany")]
        public ActionResult GetCreditAppLineListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditAppLineListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/GetCreditAppLineById/id={id}")]
        [Route("Bond/GetCreditAppLineById/id={id}")]
        [Route("HirePurchase/GetCreditAppLineById/id={id}")]
        [Route("LCDLC/GetCreditAppLineById/id={id}")]
        [Route("Leasing/GetCreditAppLineById/id={id}")]
        [Route("ProjectFinance/GetCreditAppLineById/id={id}")]
        public ActionResult GetCreditAppLineById(string id)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                return Ok(creditAppTableService.GetCreditAppLineById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/CreateCreditAppLine")]
        [Route("Bond/CreateCreditAppLine")]
        [Route("HirePurchase/CreateCreditAppLine")]
        [Route("LCDLC/CreateCreditAppLine")]
        [Route("Leasing/CreateCreditAppLine")]
        [Route("ProjectFinance/CreateCreditAppLine")]
        public ActionResult CreateCreditAppLine([FromBody] CreditAppLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                    return Ok(creditAppTableService.CreateCreditAppLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/UpdateCreditAppLine")]
        [Route("Bond/UpdateCreditAppLine")]
        [Route("HirePurchase/UpdateCreditAppLine")]
        [Route("LCDLC/UpdateCreditAppLine")]
        [Route("Leasing/UpdateCreditAppLine")]
        [Route("ProjectFinance/UpdateCreditAppLine")]
        public ActionResult UpdateCreditAppLine([FromBody] CreditAppLineItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                    return Ok(creditAppTableService.UpdateCreditAppLine(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/DeleteCreditAppLine")]
        [Route("Bond/DeleteCreditAppLine")]
        [Route("HirePurchase/DeleteCreditAppLine")]
        [Route("LCDLC/DeleteCreditAppLine")]
        [Route("Leasing/DeleteCreditAppLine")]
        [Route("ProjectFinance/DeleteCreditAppLine")]
        public ActionResult DeleteCreditAppLine([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db, SysTransactionLogService);
                return Ok(creditAppTableService.DeleteCreditAppLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region ReviewCreditAppReuqest
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/GetReviewCreditAppRequestList/ByCompany")]
        public ActionResult GetReviewCreditAppReuqestListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetReviewCreditAppRequestListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/GetReviewCreditAppRequestInitialData/creditAppLineId={creditAppLineId}")]
        public ActionResult GetCreditAppRequestTableInitialData(string creditAppLineId)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.GetReviewCreditAppRequestInitialData(creditAppLineId));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/ValidateIsManualNumberSeq/productType={productType}")]
        public ActionResult ValidateIsManualNumberSeq(int productType)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.IsManualByCreditAppRequest(productType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/CreateReviewCreditAppRequest")]
        public ActionResult CreateCreditAppRequestTable([FromBody] CreditAppRequestTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableService.CreateReviewCreditAppRequestTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/UpdateReviewCreditAppRequest")]
        public ActionResult UpdateCreditAppRequestTable([FromBody] CreditAppRequestTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                    return Ok(creditAppRequestTableService.UpdateReviewCreditAppRequestTable(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/DeleteReviewCreditAppRequest")]
        public ActionResult DeleteCreditAppRequestTable([FromBody] RowIdentity parm)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db, SysTransactionLogService);
                return Ok(creditAppRequestTableService.DeleteReviewCreditAppRequestLine(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/GetReviewCreditAppRequestById/id={id}")]
        public ActionResult GetCreditAppRequestTableById(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetReviewCreditAppRequestById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/GetDocumentStatusDropDown")]
        public ActionResult GetDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetDropDownItemCreditAppRequestStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetBuyerTableDropDown")]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/GetBuyerTableDropDown")]
        public ActionResult GetBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/GetCustomerTableDropDown")]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/GetCustomerTableDropDown")]
        public ActionResult GetCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/GetCreditAppTableDropDown")]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/GetCreditAppTableDropDown")]
        public ActionResult GetCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ReviewCreditAppReuqest
        #region BookmarkDocumentTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransList/ByCompany")]
        public ActionResult GetBookmarkDocumentTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetBookmarkDocumentTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region PrintBookDocTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/GetPrintBookDocTransValidation")]
        public ActionResult GetPrintBookDocTransValidation([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetPrintBookDocTransValidation(vwModel));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookdoctrans/GetPrintBookmarkByCreditAppReqId/id={id}")]
        public ActionResult GetPrintBookmarkByCreditAppReqId(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);

                return Ok(creditAppRequestTableService.GetCreditAppRequestTableById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/Function/PrintBookdoctrans/PrintBookDocTrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransaction([FromBody] PrintBookDocTransParm printBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByDocumentTemplateTableGUID(printBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #endregion BookmarkDocumentTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/memotrans/GetMemoTransList/ByCompany")]
        public ActionResult GetMemoTransList([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMemoTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransById/id={id}")]
        public ActionResult GetBookmarkDocumentTransById(string id)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(BookmarkDocumentTransService.GetBookmarkDocumentTransById(id));
                 }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/memotrans/GetMemoTransById/id={id}")]
        public ActionResult GetMemoById(string id)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db);
                return Ok(MemoTransService.GetMemoTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/CreateBookmarkDocumentTrans")]
        public ActionResult CreateBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.CreateBookmarkDocumentTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/memotrans/CreateMemoTrans")]
        public ActionResult CreateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.CreateMemoTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/UpdateBookmarkDocumentTrans")]
        public ActionResult UpdateBookmarkDocumentTrans([FromBody] BookmarkDocumentTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                    return Ok(BookmarkDocumentTransService.UpdateBookmarkDocumentTrans(vwModel));
                }
                else{
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/memotrans/UpdateMemoTrans")]
        public ActionResult UpdateMemo([FromBody] MemoTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                    return Ok(MemoTransService.UpdateMemoTrans(vwModel));
                }
                else{
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/DeleteBookmarkDocumentTrans")]
        public ActionResult DeleteBookmarkDocumentTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IBookmarkDocumentTransService BookmarkDocumentTransService = new BookmarkDocumentTransService(db, SysTransactionLogService);
                return Ok(BookmarkDocumentTransService.DeleteBookmarkDocumentTrans(parm.Guid));
                 }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/memotrans/DeleteMemoTrans")]
        public ActionResult DeleteMemo([FromBody] RowIdentity parm)
        {
            try
            {
                IMemoTransService MemoTransService = new MemoTransService(db, SysTransactionLogService);
                return Ok(MemoTransService.DeleteMemoTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialData/id={id}")]
        public ActionResult GetBookmarkDocumentTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetBookmarkDocumentTransInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentTransInitialDataByCreditAppLine/id={id}")]
        public ActionResult GetBookmarkDocumentTransInitialDataByCreditAppLine(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetBookmarkDocumentTransInitialDataByCreditAppLine(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/GetBookmarkDocumentDropDown")]
        public ActionResult GetBookmarkDocumentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentService bookmarkDocumentService = new BookmarkDocumentService(db);
                return Ok(bookmarkDocumentService.GetDropDownItemBookmarkDocumentByCreditAppRequestTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableByDocumentTypeDropDown")]
        public ActionResult GetDocumentTemplateTableDropDownByDocumentType([FromBody] SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableService documentTemplateTableService = new DocumentTemplateTableService(db);
                return Ok(documentTemplateTableService.GetDocumentTemplateTableByDocumentType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion

        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/GetDocumentTemplateTableDropDown")]
        public ActionResult GetDocumentTemplateTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentTemplateTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/GetDocumentStatusDropDown")]
        public ActionResult GetBookmarkDocumentTransDocumentStatusDropDown([FromBody] SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok(bookmarkDocumentTransService.GetDropDownItemBookMarkDocumentStatus(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestTable/id={id}")]
        public ActionResult GetBookMarkDocumentTransAccessModeByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetBookMarkDocumentTransAccessModeByCreditAppRequestTable(id));
                }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/BookmarkDocumentTrans/GetAccessModeByCreditAppRequestLine/id={id}")]
        public ActionResult GetAccessModeByCreditAppRequestLine(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetBookMarkDocumentTransAccessModeByCreditAppRequestLine(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/memotrans/GetMemoTransInitialData/id={id}")]
        public ActionResult GetMemoTransInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetMemoTransChildInitialData(id));

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region FinancialStatementTrans
        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/FinancialStatementTrans/GetReviewCreditAppAccessModeByCreditAppRequestTable/id={id}")]
        public ActionResult GetReviewCreditAppAccessModeByCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetReviewCreditAppAccessModeByCreditAppRequestTable(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransList/ByCompany")]
        public ActionResult GetFinancialStatementTransListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetFinancialStatementTransListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransById/id={id}")]
        public ActionResult GetFinancialStatementTransById(string id)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                return Ok(financialStatementTransService.GetFinancialStatementTransById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/FinancialStatementTrans/CreateFinancialStatementTrans")]
        public ActionResult CreateFinancialStatementTrans([FromBody] FinancialStatementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                    return Ok(financialStatementTransService.CreateFinancialStatementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/FinancialStatementTrans/UpdateFinancialStatementTrans")]
        public ActionResult UpdateFinancialStatementTrans([FromBody] FinancialStatementTransItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                    return Ok(financialStatementTransService.UpdateFinancialStatementTrans(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/FinancialStatementTrans/DeleteFinancialStatementTrans")]
        public ActionResult DeleteFinancialStatementTrans([FromBody] RowIdentity parm)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db, SysTransactionLogService);
                return Ok(financialStatementTransService.DeleteFinancialStatementTrans(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/FinancialStatementTrans/GetFinancialStatementTransInitialData/id={id}")]
        public ActionResult GetFinancialStatementTransChildInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetFinancialStatementTransChildInitialData(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion FinancialStatementTrans   
        #region Attachment
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/Attachment/GetAttachmentList/ByCompany")]
        public ActionResult GetAttachmentListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAttachmentListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/Attachment/GetAttachmentById/id={id}")]
        public async Task<ActionResult> GetAttachmentById(string id)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(await attachmentService.GetAttachmentById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/Attachment/CreateAttachment")]
        public ActionResult CreateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.CreateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/Attachment/UpdateAttachment")]
        public ActionResult UpdateAttachment([FromBody] AttachmentItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                    return Ok(attachmentService.UpdateAttachment(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/Attachment/DeleteAttachment")]
        public ActionResult DeleteAttachment([FromBody] RowIdentity parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db, SysTransactionLogService);
                return Ok(attachmentService.DeleteAttachment(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/Attachment/GetAttachmentRefId")]
        public ActionResult GetAttachmentRefId([FromBody] RefIdParm parm)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                return Ok(attachmentService.GetRefIdByRefTypeRefGUID(parm));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region PrintSetBookDocTrans
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/Function/printsetbookdoctrans/printsetbookdoctrans")]
        public async Task<ActionResult> PrintBookmarkDocumentTransaction([FromBody] PrintSetBookDocTransParm printSetBookDocTransParm)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                return Ok((await bookmarkDocumentTransService.GetDocumentTemplateByRefGUID(printSetBookDocTransParm)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region update review result 
        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/Function/UpdateReviewResult/GetUpdateReviewResultById/id={id}")]
        public ActionResult GetUpdateReviewResult(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetUpdateReviewResultById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/Function/UpdateReviewResult/GetDocumentReasonDropDown")]
        public ActionResult GetDropDownItemDocumentReason([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemDocumentReason(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/Function/UpdateReviewResult/UpdateReviewResult")]
        public ActionResult UpdateReviewResult([FromBody] UpdateReviewResultView view)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.UpdateReviewResult(view));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans/GetCopyFinancialStatementTransByRefTypeValidation")]
       
        public ActionResult GetCopyFinancialStatementTransByRefTypeValidationCARL([FromBody] CopyFinancialStatementTransView param)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                return Ok(financialStatementTransService.GetCopyFinancialStatementTransByRefTypeValidationCARL(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/FinancialStatementTrans/Function/CopyFinancialStatementTrans")]
        
        public ActionResult CopyFinancialStatementTrans([FromBody] CopyFinancialStatementTransView param)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.CopyFinancialStatementTransByCARL(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #region Cancel credit application request
        [HttpGet]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("Bond/RelatedInfo/ReviewCreditAppRequest/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("HirePurchase/RelatedInfo/ReviewCreditAppRequest/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("LCDLC/RelatedInfo/ReviewCreditAppRequest/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("Leasing/RelatedInfo/ReviewCreditAppRequest/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        [Route("ProjectFinance/RelatedInfo/ReviewCreditAppRequest/Function/CancelCreditAppRequest/GetCancelCreditAppRequestById/id={id}")]
        public ActionResult<bool> GetCancelCreditApplicationRequestByAppReqGUID(string id)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.GetCancelCreditApplicationRequestByAppReqGUID(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/Function/CancelCreditAppRequest")]
        [Route("Bond/RelatedInfo/ReviewCreditAppRequest/Function/CancelCreditAppRequest")]
        [Route("HirePurchase/RelatedInfo/ReviewCreditAppRequest/Function/CancelCreditAppRequest")]
        [Route("LCDLC/RelatedInfo/ReviewCreditAppRequest/Function/CancelCreditAppRequest")]
        [Route("Leasing/RelatedInfo/ReviewCreditAppRequest/Function/CancelCreditAppRequest")]
        [Route("ProjectFinance/RelatedInfo/ReviewCreditAppRequest/Function/CancelCreditAppRequest")]
        public ActionResult CancelCreditAppRequest([FromBody] CancelCreditAppRequestParamView param)
        {
            try
            {
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                return Ok(creditAppRequestTableService.CancelCreditApplicationRequest(param));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion Cancel credit application request
        #region CaBuyerCreditOutstanding
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/CaBuyerCreditOutstanding/GetCAReqBuyerCreditOutstandingList/ByCompany")]
        public ActionResult GetCAReqBuyerCreditOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCAReqBuyerCreditOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Dropdown 
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/CaBuyerCreditOutstanding/GetBuyerTableDropDown")]
        public ActionResult CaBuyerCreditOutstandingGetBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/CaBuyerCreditOutstanding/GetAssignmentMethodDropDown")]
        public ActionResult CaBuyerCreditOutstandingGetAssignmentAssignmentMethodDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentMethod(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/CaBuyerCreditOutstanding/GetBillingResponsibleByDropDown")]
        public ActionResult CaBuyerCreditOutstandingGetBillingResponsibleByDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBillingResponsibleBy(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/CaBuyerCreditOutstanding/GetMethodOfPaymentDropDown")]
        public ActionResult CaBuyerCreditOutstandingGetMethodOfPaymentDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemMethodOfPayment(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Dropdown
        #endregion CaBuyerCreditOutstanding
        #region CreditOutStanding
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/CreditOutStanding/GetCAReqCreditOutStandingList/ByCompany")]
        public ActionResult GetCreditOutStandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetCreditOutStandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/CreditOutStanding/GetCustomerTableDropDown")]
        public ActionResult GetCreditOutStandingCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/CreditOutStanding/GetCreditAppTableDropDown")]
        public ActionResult GetCreditOutStandingCreditAppTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditAppTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/CreditOutStanding/GetCreditLimitTypeDropDown")]
        public ActionResult GetCreditOutStandingCreditLimitTypeDropDownn([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCreditLimitType(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion  CreditOutStanding
        #region AssignmentAgreementOutstanding
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/AssignmentAgreementOutstanding/GetCAReqAssignmentOutstandingList/ByCompany")]
        public ActionResult GetAssignmentOutstandingListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetAssignmentOutstandingListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/AssignmentAgreementOutstanding/GetCustomerTableDropDown")]
        public ActionResult GetAssignmentOutstandingCustomerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemCustomerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/AssignmentAgreementOutstanding/GetBuyerTableDropDown")]
        public ActionResult GetAssignmentOutstandingBuyerTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemBuyerTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("Factoring/RelatedInfo/ReviewCreditAppRequest/RelatedInfo/AssignmentAgreementOutstanding/GetAssignmentAgreementTableDropDown")]
        public ActionResult GetAssignmentOutstandingAssignmentAgreementTableDropDown([FromBody] SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return Ok(sysDropDownService.GetDropDownItemAssignmentAgreementTable(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AssignmentAgreementOutstanding        

    }

}
