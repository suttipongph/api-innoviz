﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class MigrationTableController : BaseControllerV2
    {

        public MigrationTableController(SmartAppContext context, ISysTransactionLogService transactionLogService)
            : base(context, transactionLogService)
        {

        }
        [HttpPost]
        [Route("GetMigrationTableList")]
        public ActionResult GetMigrationTableList([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetMigrationTableListvw(search));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpGet]
        [Route("GetMigrationTableById/id={id}")]
        public ActionResult GetMigrationById(string id)
        {
            try
            {
                IMigrationTableService migrationTableService = new MigrationTableService(db);
                return Ok(migrationTableService.GetMigrationTableById(id));
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        [HttpPost]
        [Route("GetExecuteMigration")]
        public ActionResult ExecuteMigration([FromBody] MigrationTableItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMigrationTableService migrationTableService = new MigrationTableService(db);
                    return Ok(migrationTableService.ExecuteMigration(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
            
        }
        #region GetToken
        private string GetToken()
        {
            var request = HttpContext.Request;
            string reqAuthHeader = request.Headers["Authorization"];
            if (reqAuthHeader != null)
            {
                string token = reqAuthHeader.Split(" ").Last();
                return (token != null && token != "") ? token : null;
            }
            return null;
        }
        #endregion
        #region Function
        #region Generate Dummy Record
        [HttpPost]
        [Route("Function/GenerateDummyRecord/GetGenerateDummyRecord")]
        public ActionResult GenerateDummyRecord([FromBody] CompanyItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMigrationTableService migrationTableService = new MigrationTableService(db);
                    return Ok(migrationTableService.GenMigrationTransaction(vwModel.CompanyGUID));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region Update Cheque Reference
        [HttpGet]
        [Route("Function/UpdateChequeReference/UpdateChequeReference")]
        public ActionResult UpdateChequeReference()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMigrationTableService migrationTableService = new MigrationTableService(db);
                    return Ok(migrationTableService.UpdateChequeReference());
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Function/UpdateChequeReference/ValidateUpdateChequeReference")]
        public ActionResult ValidateUpdateChequeReference()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMigrationTableService migrationTableService = new MigrationTableService(db);
                    return Ok(migrationTableService.ValidateUpdateChequeReference());
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region GenPULineInvoice
        [HttpGet]
        [Route("Function/GenPurchaseLineInvoice/ValidateGenPULineInvoice/companyGUID={id}")]
        public ActionResult ValidateGenPULineInvoice(string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMigrationTableService migrationTableService = new MigrationTableService(db);
                    return Ok(migrationTableService.ValidateGenPULineInvoice(id));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Function/GenPurchaseLineInvoice/GenPULineInvoice/companyGUID={id}")]
        public ActionResult GenPULineInvoice(string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMigrationTableService migrationTableService = new MigrationTableService(db);
                    return Ok(migrationTableService.GenPULineInvoice(id));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region GenWDLineInvoice
        [HttpGet]
        [Route("Function/GenWithdrawalLineInvoice/ValidateGenWDLineInvoice/companyGUID={id}")]
        public ActionResult ValidateGenWDLineInvoice(string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMigrationTableService migrationTableService = new MigrationTableService(db);
                    return Ok(migrationTableService.ValidateGenWDLineInvoice(id));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        [HttpGet]
        [Route("Function/GenWithdrawalLineInvoice/GenWDLineInvoice/companyGUID={id}")]
        public ActionResult GenWDLineInvoice(string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IMigrationTableService migrationTableService = new MigrationTableService(db);
                    return Ok(migrationTableService.GenWDLineInvoice(id));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
    }
}