﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Mvc;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Innoviz.SmartApp.WebApi.ControllersV2
{
    public class OccupationController : BaseControllerV2
    {
        public OccupationController(SmartAppContext context, ISysTransactionLogService transactionLogService)
           : base(context, transactionLogService)
        {

        }
        [HttpPost]
        [Route("GetOccupationList/ByCompany")]
        public ActionResult GetOccupationListByCompany([FromBody] SearchParameter search)
        {
            try
            {
                ISysListViewService sysListViewService = new SysListViewService(db);
                return Ok(sysListViewService.GetOccupationListvw(search));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpGet]
        [Route("GetOccupationById/id={id}")]
        public ActionResult GetOccupationById(string id)
        {
            try
            {
                IOccupationService occupationService = new OccupationService(db);
                return Ok(occupationService.GetOccupationById(id));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("CreateOccupation")]
        public ActionResult CreateOccupation([FromBody] OccupationItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IOccupationService occupationService = new OccupationService(db, SysTransactionLogService);
                    return Ok(occupationService.CreateOccupation(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("UpdateOccupation")]
        public ActionResult UpdateOccupation([FromBody] OccupationItemView vwModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IOccupationService occupationService = new OccupationService(db, SysTransactionLogService);
                    return Ok(occupationService.UpdateOccupation(vwModel));
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.MODELSTATE");
                    foreach (var k in ModelState.Keys)
                    {
                        ex.Data.Add(k, ModelState.GetValueOrDefault(k).Errors);
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        [HttpPost]
        [Route("DeleteOccupation")]
        public ActionResult DeleteOccupation([FromBody] RowIdentity parm)
        {
            try
            {
                IOccupationService occupationService = new OccupationService(db, SysTransactionLogService);
                return Ok(occupationService.DeleteOccupation(parm.Guid));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region Dropdown
        #endregion
    }
}
