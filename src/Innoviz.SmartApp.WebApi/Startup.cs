﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.BatchProcessing;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelHandler;
using Innoviz.SmartApp.WebApi.AuthPolicy;
using Innoviz.SmartApp.WebApi.Middleware;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json.Linq;
using Innoviz.SmartApp.Data.InterfaceWithOtherSystems;
using Innoviz.SmartApp.WebApi.ControllersV2.Helper;

namespace Innoviz.SmartApp.WebApi
{
    public class Startup
    {
        private string authServer;

        //private readonly Microsoft.Extensions.Logging.ILogger _logger;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            authServer = configuration["JWT:Authority"];

            //_logger = logger;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            bool enableEncryption = Convert.ToBoolean(Configuration["DeploymentConfig:EnableEncryption"]);
            if (!enableEncryption)
            {
                // show PII in logs
                IdentityModelEventSource.ShowPII = true;
            }

            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            string connectionString = Configuration.GetConnectionString("SmartApp");
            int connectionTimeout = Convert.ToInt32(Configuration["CommandTimeout:Timeout"]);

            services.AddDbContext<SmartAppContext>(options =>
                options.UseSqlServer(connectionString, sql => {
                    sql.MigrationsAssembly(migrationsAssembly);
                    sql.CommandTimeout(connectionTimeout);
                })
            );

            var interfaceAXMigrationAssembly = typeof(InterfaceAXDbContext).GetTypeInfo().Assembly.GetName().Name;
            string interfaceWithAXConnectionString = Configuration.GetConnectionString("AXInterface");
            services.AddDbContext<InterfaceAXDbContext>(options =>
                options.UseSqlServer(interfaceWithAXConnectionString, sql => {
                    sql.MigrationsAssembly(interfaceAXMigrationAssembly);
                    sql.CommandTimeout(connectionTimeout);
                }),
                ServiceLifetime.Transient
            );

            services.AddControllers(options =>
            {
                options.Filters.Add(new SystemParameterActionFilter(Configuration));
            })
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });
            
            // configures IIS out-of-proc settings (see https://github.com/aspnet/AspNetCore/issues/14882)
            services.Configure<IISOptions>(options =>
            {
                options.AutomaticAuthentication = false;
                options.AuthenticationDisplayName = "Windows";
            });

            services.AddSwaggerDocument(doc =>
            {
                string apiScope = Configuration["BusinessApi:Scope"];
                doc.AddSecurity("bearer", Enumerable.Empty<string>(), new NSwag.OpenApiSecurityScheme
                {
                    Type = NSwag.OpenApiSecuritySchemeType.OAuth2,
                    Description = "Web Api Authorization Code Flow",
                    Flow = NSwag.OpenApiOAuth2Flow.AccessCode,
                    Flows = new NSwag.OpenApiOAuthFlows()
                    {
                        AuthorizationCode = new NSwag.OpenApiOAuthFlow()
                        {
                            
                            Scopes = new Dictionary<string, string>
                            {
                                { apiScope, "Access to Business Api" },
                            },
                            AuthorizationUrl = authServer + "/connect/authorize",
                            TokenUrl = authServer + "/connect/token",
                            RefreshUrl = authServer + "/connect/token"
                        }
                    }
                });

                doc.OperationProcessors.Add(new NSwag.Generation.Processors.Security.OperationSecurityScopeProcessor("bearer"));

                doc.OperationProcessors.Add(new SwaggerHeaderOperationProcessor());
            });

            string apiAuthority = Configuration["JWT:Authority"];
            string apiAudience = Configuration["JWT:Audience"];

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => {
                    options.Authority = apiAuthority;
                    options.RequireHttpsMetadata = false;
                    options.Audience = apiAudience;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ClockSkew = TimeSpan.FromSeconds(10)
                    };

                });

            // accessing http headers in middlewares
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTransient<IControllerDiscovery, ControllerDiscovery>();

            services.AddAuthorization(options => {
                // use custom policy
                options.AddPolicy("roles_required", policy =>
                                    policy.Requirements.Add(new CustomRolesRequirement()));
            });

            //auth policy
            services.AddScoped<IAuthorizationHandler, CustomRolesRequirementHandler>();

            services.AddScoped<IBatchLogService, BatchLogService>();

            services.AddTransient<ISysTransactionLogService, SysTransactionLogService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, ILogger<Startup> logger)
        {
            string authorityUrl = Configuration["AuthConfig:Authority"];
            bool isProduction = Convert.ToBoolean(Configuration["DeploymentConfig:IsProduction"]);
            // force using https scheme if auth Authority use https
            if (authorityUrl != null && authorityUrl.StartsWith("https"))
            {
                app.Use((context, next) =>
                {
                    context.Request.Scheme = "https";
                    return next();
                });
            }
            loggerFactory.AddSerilog();

            app.UseMiddleware<RequestResponseLoggingMiddleware>();
            app.UseMiddleware<WebApiErrorHandler>();

            app.UseWhen(context => context.Request.Path.StartsWithSegments("/api/v2/TestK2") && 
                                    !context.Request.Path.Value.Contains("TestInput444ForErrorCode444") &&
                                    !context.Request.Path.Value.Contains("TestInput500ForErrorCode500"), 
                                    appBuilder =>
            {
                appBuilder.UseDeveloperExceptionPage();

            });

            
            app.UseCors(configure => {
                configure.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                name: "api-v1",
                pattern: "api/v1/{controller}");
            });
            
            app.UseOpenApi();
            app.UseSwaggerUi3(settings =>
            {
                string swaggerClientId = Configuration["BusinessApi:SwaggerClientId"];
                string swaggerClientSecret = Configuration["BusinessApi:SwaggerClientSecret"];
                settings.OAuth2Client = new NSwag.AspNetCore.OAuth2ClientSettings
                {
                    ClientId = swaggerClientId,
                    ClientSecret = swaggerClientSecret,
                };
                settings.TransformToExternalPath = (internalUiRoute, request) =>
                {
                    if (internalUiRoute.StartsWith("/") == true &&
                        internalUiRoute.StartsWith(request.PathBase) == false)
                    {
                        return request.PathBase + internalUiRoute;
                    }
                    else
                    {
                        return internalUiRoute;
                    }
                };
            });

            InitializeDatabase(app, logger, env) ;

        }

        private void InitializeDatabase(IApplicationBuilder app, Microsoft.Extensions.Logging.ILogger logger, IWebHostEnvironment env)
        {            
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<SmartAppContext>();
               // context.Database.Migrate();

                string username = SmartAppUtil.GetMachineName();
                bool isDevelopment = env.IsDevelopment();
                ISysDataInitializationHistoryService dataInitService = new SysDataInitializationHistoryService(context);
                InitializeDataParm initDataParm = new InitializeDataParm
                {
                    DbContext = context,
                    Username = username,
                    IsDevelopment = isDevelopment,
                    DataInitService = dataInitService,
                    Logger = logger
                };

                string apiVersion = SmartAppUtil.GetApiVersion().Split(";").Last(); 
                List<SysDataInitializationHistory> newVersions = new List<SysDataInitializationHistory>();

                InitializeSysDuplicateDetection(apiVersion, initDataParm, newVersions);
                InitializeSysMessage(apiVersion, initDataParm, newVersions);

                if (context.SysMessage.Count() != 0 &&
                    context.SysDuplicateDetection.Count() != 0)
                {
                    InitializeSystemStaticData(context, logger);
                }

                var controllerDisco = serviceScope.ServiceProvider.GetRequiredService<IControllerDiscovery>();

                InitializeSysLabelReport(apiVersion, initDataParm, newVersions);
                InitializeDocumentProcessStatus(apiVersion, initDataParm, newVersions);
                InitializeSysEnumTable(apiVersion, initDataParm, newVersions);
                InitializeSysRoleTable(apiVersion, initDataParm, newVersions);
                InitializeSysFeatureGroupControllerMapping(apiVersion, initDataParm, newVersions, controllerDisco);
                InitializeMigrationTable(apiVersion, initDataParm, newVersions);

              //  InitializeStoredProcedureAndFunction(apiVersion, initDataParm, newVersions);

                if(newVersions != null && newVersions.Count > 0)
                {
                    dataInitService.AddNewVersionHistories(newVersions);
                }
            }
            
        }
        private void InitializeSystemStaticData(SmartAppContext context, Microsoft.Extensions.Logging.ILogger logger)
        {
            try
            {
                // ob groupIds
                var siteObFeatureGroupIds = SysFeatureTableData.GetOBGroupId();
                SystemStaticData.SetOBFeatureGroupIds(siteObFeatureGroupIds);

                // sysMessage
                if(!SystemStaticData.HasStaticSysMessageData())
                {
                    ISysMessageService sysMessageService = new SysMessageService(context);
                    var dbSysMessage = sysMessageService.GetSysMessageNoTracking();
                    List<StaticSysMessageData> messages = new List<StaticSysMessageData>();
                    foreach (var item in dbSysMessage)
                    {
                        messages.Add(item.ToStaticSysMessageData());
                    }
                    SystemStaticData.SetSysMessageData(messages);
                }
                
                // sysDuplicate
                if(!SystemStaticData.HasStaticSysDuplicateData())
                {
                    ISysDuplicateDetectionService sysDuplicateDetectionService = new SysDuplicateDetectionService(context);
                    List<SysDuplicateDetection> duplicateDetections = sysDuplicateDetectionService.GetSysDuplicateDetectionNoTracking();
                    SystemStaticData.SetSysDuplicateDetectionData((duplicateDetections.ToStaticSysDuplicateDetections()).ToList());
                }

                // dropdown lookup table names
                if (!SystemStaticData.HasStaticDropdownLookup())
                {
                    ISysAccessRightService sysAccessRightService = new SysAccessRightService(context);
                    SystemStaticData.SetDropdownLookupData(sysAccessRightService.GetTableNamesForControllerEntityMapping());
                }

                // modelType lookup by model name
                if (!SystemStaticData.HasStaticModelTypeLookup())
                {
                    ISharedService sharedService = new SharedService();
                    SystemStaticData.SetModelTypeLookupData(sharedService.InitializeModelTypeLookupStaticData());
                }
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e, logger);
                throw e;
            }
        }
        private void InitializeSysMessage(string version, InitializeDataParm initDataParm, List<SysDataInitializationHistory> newVersionList)
        {
            try 
            {
                var newVersion = CheckNewVersion(initDataParm.DataInitService, SysDataInitType.SysMessage, version);
                if (initDataParm.IsDevelopment || newVersion != null)
                {
                    string text = File.ReadAllText("./Helper/StaticData/SysMessage.json");

                    ISysMessageService sysMessageService = new SysMessageService(initDataParm.DbContext);
                    sysMessageService.InitializeSysMessage(text);

                    if (newVersion != null)
                    {
                        
                        newVersionList.Add(newVersion);
                    }

                }
                
            }
            catch (Exception e) 
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e, initDataParm.Logger);
                throw e;
            }
        }
        private void InitializeSysRoleTable(string version, InitializeDataParm initDataParm, List<SysDataInitializationHistory> newVersionList)
        {
            try
            {
                var newVersion = CheckNewVersion(initDataParm.DataInitService, SysDataInitType.SysRoleTable, version);
                if (initDataParm.IsDevelopment || newVersion != null)
                {
                    ISysAccessRightService accessRightService = new SysAccessRightService(initDataParm.DbContext);
                    accessRightService.InitializeDevelopmentSysRoleTable(initDataParm.Username);
                    accessRightService.InitializeDevelopmentSysScopeRole(initDataParm.Username);

                    if (newVersion != null)
                    {
                        
                        newVersionList.Add(newVersion);
                    }

                }
                
            }
            catch(Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e, initDataParm.Logger);
                throw e;
            }
        }
        
        
        private void LogErrorAtStartup(Exception e, Microsoft.Extensions.Logging.ILogger logger) {
            logger.LogCritical("#Error at WebApi Startup");
            var msgList = e.Data[ExceptionDataKey.MessageList];
            if (msgList != null) {
                var list = (List<string>)msgList;
                if (list.Count() != 0) {
                    foreach (var item in list) {
                        logger.LogCritical(item);
                    }
                }
            }
            logger.LogCritical(e.Data[ExceptionDataKey.StackTrace].ToString());
        }

        private void InitializeStoredProcedureAndFunction(string version, InitializeDataParm initDataParm, List<SysDataInitializationHistory> newVersionList)
        {
            try
            {
                var newVersion = CheckNewVersion(initDataParm.DataInitService, SysDataInitType.SQLStoredProcAndFunction, version);
                if (initDataParm.IsDevelopment || newVersion != null)
                {
                    string connectionString = Configuration.GetConnectionString("SmartApp");
                    string spFnFolder = "./StoredProceduresAndFunctions";
                    SharedService sharedService = new SharedService();
                    sharedService.InitializeStoredProceduresAndFunctions(spFnFolder, connectionString);

                    if (newVersion != null)
                    {
                        
                        newVersionList.Add(newVersion);
                    }

                }
                
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e, initDataParm.Logger);
                throw e;
            }
        }

        private void InitializeSysDuplicateDetection(string version, InitializeDataParm initDataParm, List<SysDataInitializationHistory> newVersionList)
        {
            try
            {
                var newVersion = CheckNewVersion(initDataParm.DataInitService, SysDataInitType.SysDuplicateDetection, version);
                if (initDataParm.IsDevelopment || newVersion != null)
                {
                    string text = File.ReadAllText("./Helper/StaticData/SysDuplicateDetection.json");
                    ISysDuplicateDetectionService sysDuplicateDetectionService = new SysDuplicateDetectionService(initDataParm.DbContext);
                    sysDuplicateDetectionService.InitializeSysDuplicateDetection(text, initDataParm.Username);

                    if (newVersion != null)
                    {
                        
                        newVersionList.Add(newVersion);
                    }

                }
                
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e, initDataParm.Logger);
                throw e;
            }
        }
        private void InitializeSysLabelReport(string version, InitializeDataParm initDataParm, List<SysDataInitializationHistory> newVersionList)
        {
            try
            {
                var newVersion = CheckNewVersion(initDataParm.DataInitService, SysDataInitType.SysLabelReport, version);
                if (initDataParm.IsDevelopment || newVersion != null)
                {
                    string sysLabelReportjson = File.ReadAllText("./Helper/StaticData/SysLabelReport.json");
                    List<SysLabelReport> sysLabelReports = JsonConvert.DeserializeObject<List<SysLabelReport>>(sysLabelReportjson);

                    string sysLabelReportCustomjson = File.ReadAllText("./Helper/StaticData/SysLabelReport_CUSTOM.json");
                    List<SysLabelReport_CUSTOM> sysLabelReport_CUSTOMs = JsonConvert.DeserializeObject<List<SysLabelReport_CUSTOM>>(sysLabelReportCustomjson);

                    ISysLabelReportService labelReportService = new SysLabelReportService(initDataParm.DbContext);
                    labelReportService.InitializeSysLabelReport(sysLabelReports, sysLabelReport_CUSTOMs);

                    if (newVersion != null)
                    {
                        
                        newVersionList.Add(newVersion);
                    }

                }
                
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e, initDataParm.Logger);
                throw e;
            }
        }
        private void InitializeDocumentProcessStatus(string version, InitializeDataParm initDataParm, List<SysDataInitializationHistory> newVersionList)
        {
            try
            {
                var newVersion = CheckNewVersion(initDataParm.DataInitService, SysDataInitType.DocumentProcessStatus, version);
                if (initDataParm.IsDevelopment || newVersion != null)
                {
                    IDocumentService documentService = new DocumentService(initDataParm.DbContext);
                    string text = File.ReadAllText("./Helper/StaticData/DocumentProcessStatus.json");
                    string text_custom = File.ReadAllText("./Helper/StaticData/DocumentProcessStatus_CUSTOM.json");
                    documentService.InitializeDocumentProcessStatus(text, text_custom, initDataParm.Username);

                    if (newVersion != null)
                    {
                        
                        newVersionList.Add(newVersion);
                    }

                }
                
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e, initDataParm.Logger);
                throw e;
            }
        }
        private void InitializeSysEnumTable(string version, InitializeDataParm initDataParm, List<SysDataInitializationHistory> newVersionList)
        {
            try
            {
                var newVersion = CheckNewVersion(initDataParm.DataInitService, SysDataInitType.SysEnumTable, version);
                if (initDataParm.IsDevelopment || newVersion != null)
                {
                    string sysEnumTablejson = File.ReadAllText("./Helper/StaticData/SysEnumTable.json");
                    string sysEnumTableCustomjson = File.ReadAllText("./Helper/StaticData/SysEnumTable_CUSTOM.json");

                    ISysEnumTableService enumTableService = new SysEnumTableService(initDataParm.DbContext);
                    enumTableService.InitializeSysEnumTable(sysEnumTablejson, sysEnumTableCustomjson, initDataParm.Username);

                    if (newVersion != null)
                    {
                        
                        newVersionList.Add(newVersion);
                    }

                }
                
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e, initDataParm.Logger);
                throw e;
            }
        }

        private void InitializeSysFeatureGroupControllerMapping(string version, InitializeDataParm initDataParm, List<SysDataInitializationHistory> newVersionList,
                                                                    IControllerDiscovery controllerDisco)
        {
            try
            {
                var newVersion = CheckNewVersion(initDataParm.DataInitService, SysDataInitType.SysFeatureTableData, version);
                if (initDataParm.IsDevelopment || newVersion != null)
                {
                    string sysFeatureGroupMenujson = File.ReadAllText("./Helper/StaticData/SysFeatureGroup_Menu_Mappings.json");
                    ISysAccessRightService accessRightService = new SysAccessRightService(initDataParm.DbContext);
                    var tableNames = accessRightService.GetTableNamesForControllerEntityMapping();
                    var cem = controllerDisco.GetListControllers(tableNames);

                    accessRightService.InitializeSysFeatureGroupControllerMapping(SysFeatureTableData.GetAddFeatureControllerMapping(),
                                                                                    SysFeatureTableData.GetOBGroupId(), sysFeatureGroupMenujson,
                                                                                    cem, initDataParm.Username, initDataParm.IsDevelopment);

                    if (newVersion != null)
                    {
                        
                        newVersionList.Add(newVersion);
                    }

                }
                
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e, initDataParm.Logger);
                throw e;
            }
        }
        private void InitializeMigrationTable(string version, InitializeDataParm initDataParm, List<SysDataInitializationHistory> newVersionList)
        {
            try
            {
                var newVersion = CheckNewVersion(initDataParm.DataInitService, SysDataInitType.MigrationTable, version);
                if(initDataParm.IsDevelopment || newVersion != null)
                {
                    string migrationTablejson = File.ReadAllText("./Helper/StaticData/MigrationTable.json");
                    string migrationTableCustomjson = File.ReadAllText("./Helper/StaticData/MigrationTable_CUSTOM.json");

                    IMigrationTableService migrationTableService = new MigrationTableService(initDataParm.DbContext);
                    migrationTableService.InitializeMigrationTable(migrationTablejson, migrationTableCustomjson, initDataParm.Username);

                    if(newVersion != null)
                    {
                        
                        newVersionList.Add(newVersion);
                    }
                    
                }
            }
            catch (Exception e)
            {
                e = SmartAppUtil.AddStackTrace(e);
                LogErrorAtStartup(e, initDataParm.Logger);
                throw e;
            }
        }
        private SysDataInitializationHistory CheckNewVersion(ISysDataInitializationHistoryService dataInitService, SysDataInitType initType, string version)
        {
            try
            {
                bool isNewVersion = !dataInitService.CheckVersionExists(initType, version);
                return isNewVersion ? new SysDataInitializationHistory { DataInitializationType = (int)initType, Version = version } : null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private class InitializeDataParm
        {
            public SmartAppContext DbContext { get; set; }
            public string Username { get; set; }
            public bool IsDevelopment { get; set; }
            public Microsoft.Extensions.Logging.ILogger Logger { get; set; }
            public ISysDataInitializationHistoryService DataInitService { get; set; }
        }
    }
}
