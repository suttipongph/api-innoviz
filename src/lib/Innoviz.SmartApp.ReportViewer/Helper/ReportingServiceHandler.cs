﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.ReportViewer.ReportHelper
{
    public static class ReportingServiceHandler
    {
        public static Dictionary<string, string[]> ToReportParameters<T> (this T reportParm) where T : ViewReportBaseEntity
        {
            try
            {
                if(reportParm == null)
                {
                    return null;
                }
                else 
                {
                    Dictionary<string, string[]> result = new Dictionary<string, string[]>();
                    
                    List<string> skipFields = new List<string>();
                    skipFields.Add(ReportConst.ReportFormat.ToUpper());
                    skipFields.Add(ReportConst.ReportName.ToUpper());
                    skipFields.Add(ReportConst.ServicePath.ToUpper());

                    var props = typeof(T).GetProperties();
                    foreach (var prop in props)
                    {
                        if (!skipFields.Any(skip => skip == prop.Name.ToUpper()))
                        {
                            var value = prop.GetValue(reportParm);
                            bool isArrayOrCollectionType = prop.PropertyType != typeof(string) && (prop.PropertyType.IsArray ||
                                (prop.PropertyType.IsGenericType && (prop.PropertyType.GetGenericTypeDefinition() == typeof(IEnumerable<>) ||
                                     prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))));
                            
                            if (value == null)
                            {
                                result.Add(prop.Name, new string[0]);
                            }
                            else
                            {
                                if(isArrayOrCollectionType)
                                {
                                    Type propType = prop.PropertyType.IsArray ? prop.PropertyType.GetElementType() :
                                                                            prop.PropertyType.GetGenericArguments()[0];
                                    var enumerator = ((IEnumerable)value).GetEnumerator();
                                    List<string> convertToString = new List<string>();
                                    while(enumerator.MoveNext())
                                    {
                                        dynamic convertVal = Convert.ChangeType(enumerator.Current, propType);
                                        convertToString.Add(propType != typeof(string) ? convertVal.ToString() : convertVal);
                                    }
                                    // case enum
                                    if(propType == typeof(int) && convertToString.Any(a => a == "-1"))
                                    {
                                        convertToString.Clear();
                                        convertToString.Add(Guid.Empty.ToString());
                                    }
                                    result.Add(prop.Name, convertToString.ToArray());
                                }
                                else
                                {
                                    Type propType = prop.PropertyType;
                                    dynamic convertVal = Convert.ChangeType(value, propType);
                                    string addValue = propType != typeof(string) ? convertVal.ToString() : convertVal;
                                    result.Add(prop.Name, new string[] { addValue });
                                }
                            }
                        }
                    }
                    return result;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
