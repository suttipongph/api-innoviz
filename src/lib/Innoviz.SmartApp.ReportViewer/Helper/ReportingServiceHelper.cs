﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.ReportViewer.Helper;
using Innoviz.SmartApp.ReportViewer.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Innoviz.SmartApp.ReportViewer.ReportHelper
{
    public class ReportingServiceHelper
    {
        public ReportingServiceHelper()
        {
			string ssrsUrl = DataTypeHandler.GetConfigurationValue("ReportServer:Url");
			string ssrsFolderPath = DataTypeHandler.GetConfigurationValue("ReportServer:FolderPath");
			string ssrsUser = DataTypeHandler.GetConfigurationValue("ReportServer:User");
			string ssrsPassword = DataTypeHandler.GetConfigurationValue("ReportServer:Password");
			string ssrsDomain = DataTypeHandler.GetConfigurationValue("ReportServer:Domain");
			string ssrsHttpClientCredentialType = DataTypeHandler.GetConfigurationValue("ReportServer:HttpClientCredentialType");
			string ssrsTimeout = DataTypeHandler.GetConfigurationValue("ReportServer:Timeout");

			NetworkCredentials = new System.Net.NetworkCredential(ssrsUser, ssrsPassword, ssrsDomain);
			ReportServerUrl = ssrsUrl;
			ReportFolderPath = ssrsFolderPath;
			Encoding = Encoding.UTF8;
			HttpClientCredentialType = ssrsHttpClientCredentialType;
			Timeout = ssrsTimeout != null ? (int?)Convert.ToInt32(ssrsTimeout) : null;
		}
		protected string ReportFolderPath { get; }
		#region ReportViewer.ReportController
		protected System.Net.ICredentials NetworkCredentials { get; }
		protected string ReportServerUrl { get; }
		protected string HttpClientCredentialType { get; }

		/// <summary>
		/// This indicates whether or not to replace image urls from your report server to image urls on your local site to act as a proxy
		/// *useful if your report server is not accessible publicly*
		/// </summary>
		protected virtual bool UseCustomReportImagePath { get { return false; } }
		protected virtual bool AjaxLoadInitialReport { get { return true; } }
		protected virtual System.Text.Encoding Encoding { get; }
		protected virtual int? Timeout { get; set; }
		
		protected virtual string ReportImagePath
		{
			get
			{
				return "/Report/ReportImage/?originalPath={0}";
			}
		}

		protected virtual System.ServiceModel.HttpClientCredentialType ClientCredentialType
		{
			get
			{
				if (HttpClientCredentialType.ToLower() == "windows")
					return System.ServiceModel.HttpClientCredentialType.Windows;
				else
					return System.ServiceModel.HttpClientCredentialType.Ntlm;
			}
		}

		
		protected ReportViewerModel GetReportViewerModel(HttpRequest request = null)
		{
			var model = new ReportViewerModel();
			model.AjaxLoadInitialReport = this.AjaxLoadInitialReport;
			model.ClientCredentialType = this.ClientCredentialType;
			model.Credentials = this.NetworkCredentials;

			var enablePagingResult = request != null ? _getRequestValue(request, "ReportViewerEnablePaging") : string.Empty;
			if (enablePagingResult.HasValue())
			{
				model.EnablePaging = enablePagingResult.ToBoolean();
			}
			else
			{
				model.EnablePaging = true;
			}
			model.Encoding = this.Encoding;
			model.ServerUrl = this.ReportServerUrl;
			model.ReportImagePath = this.ReportImagePath;
			model.Timeout = this.Timeout;
			model.UseCustomReportImagePath = this.UseCustomReportImagePath;
			if(request != null)
            {
				model.BuildParameters(request);
			}

			return model;
		}

		private string _getRequestValue(HttpRequest request, string key)
		{
			if (request.Query != null && request.Query.Keys != null && request.Query.Keys.Contains(key))
			{
				var values = request.Query[key].ToSafeString().Split(',');
				if (values != null && values.Count() > 0)
				{
					return values[0].ToSafeString();
				}
			}

			try
			{
				if (request.Form != null && request.Form.Keys != null && request.Form.Keys.Contains(key))
				{
					return request.Form[key].ToSafeString();
				}
			}
			catch
			{
				//No need to throw errors, just no Form was passed in and it's unhappy about that
			}

			return String.Empty;
		}
		#endregion
		public FileInformation ExportReport<T>(T reportParam, string format = null) where T: ViewReportBaseEntity
        {
            try
            {
				ReportViewerModel model = GetReportViewerModelWithParameters(reportParam);
				model.ViewMode = ReportViewModes.Export;

				format = format ?? GetExportReportFileFormat(reportParam);
				string reportName = GetReportNameFromParam(reportParam);
				string fileExt = GetExportReportFileExtension(format);

				var contentData = ReportServiceHelpers.ExportReportToFormat(model, format);

				FileInformation fileResult = new FileInformation()
				{
					Base64 = Convert.ToBase64String(contentData.ReportData),
					FileName = string.Concat(reportName, fileExt)
				};

				return fileResult;
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		private ReportViewerModel GetReportViewerModelWithParameters<T>(T reportParams) where T : ViewReportBaseEntity
        {
            try
            {
				string reportName = GetReportNameFromParam(reportParams);

				ReportViewerModel model = GetReportViewerModel();
				model.ReportPath = string.Concat(this.ReportFolderPath, "/", reportName);

				var parameters = reportParams.ToReportParameters();
				return GetReportViewerModelWithParameters(parameters, model);
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		private ReportViewerModel GetReportViewerModelWithParameters(Dictionary<string, string[]> reportParams, ReportViewerModel model)
        {
            try
            {
				if(model == null)
                {
					model = GetReportViewerModel();
                }

				if(string.IsNullOrWhiteSpace(model.ReportPath))
                {
					throw new Exception("Report path cannot be null.");
                }

				if(reportParams.Count > 0)
                {
                    foreach (var item in reportParams)
                    {
						model.AddParameter(item.Key, item.Value);
                    }
                }
				return model;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public static string GetReportNameFromParam<T>(T reportParams) where T : ViewReportBaseEntity
		{
			try
			{
				string reportName = GetReportParamValue(reportParams, ReportConst.ReportName);
				return reportName;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public static string GetExportReportFileFormat<T>(T reportParams) where T: ViewReportBaseEntity
        {
            try
            {
				string reportFormat = GetReportParamValue(reportParams, ReportConst.ReportFormat);
				return GetExportReportFileFormat(reportFormat);
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public static string GetExportReportFileExtension(string format)
		{
			try
			{
				format = format ?? string.Empty;
				switch (format.ToUpper())
				{
					case ReportConst.CSV_Format: return FileEXT.CSV_EXTENSION;
					case ReportConst.MHTML_Format: return FileEXT.MHT_EXTENSION;
					case ReportConst.PDF_Format: return FileEXT.PDF_EXTENSION;
					case ReportConst.TIFF_Format: return FileEXT.TIF_EXTENSION;
					case ReportConst.XML_Format: return FileEXT.XML_EXTENSION;
					case ReportConst.WORDOPENXML_Format: return FileEXT.DOCX_EXTENSION;

					case ReportConst.EXCELOPENXML_Format:
					default:
						return FileEXT.XLSX_EXTENSION;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private static string GetExportReportFileFormat(string format)
        {
            try
            {
				format = format ?? string.Empty;
				switch(format.ToUpper())
                {
					case ReportConst.CSV_Format: return ReportConst.CSV_Format;
					case ReportConst.MHTML_Format: return ReportConst.MHTML_Format;
					case ReportConst.PDF_Format: return ReportConst.PDF_Format;
					case ReportConst.TIFF_Format: return ReportConst.TIFF_Format;
					case ReportConst.XML_Format: return ReportConst.XML_Format;
					case ReportConst.WORDOPENXML_Format: return ReportConst.WORDOPENXML_Format;
					
					case ReportConst.EXCELOPENXML_Format:
					default: 
						return ReportConst.EXCELOPENXML_Format;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		private static string GetReportParamValue<T>(T reportParams, string propName) where T: ViewReportBaseEntity
        {
            try
            {
				if (reportParams == null)
				{
					throw new Exception("Report parameters cannot be null.");
				}

				PropertyInfo reportProp = typeof(T).GetProperty(propName);

				string reportParamValue = reportProp.PropertyType == typeof(string) ? 
								(string)reportProp.GetValue(reportParams) : 
								(reportProp.GetValue(reportParams))?.ToString();
				return reportParamValue;
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
	}
}
