﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.ReportViewer.ReportHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Innoviz.SmartApp.ReportViewer
{
    public interface IReportViewerService
    {
        FileInformation RenderReport<T>(T reportParam) where T: ViewReportBaseEntity;
        FileInformation ExportReport<T>(T reportParam, string format = null) where T : ViewReportBaseEntity;
    }
    public class ReportViewerService: IReportViewerService
    {
        /*
         *  Renders only PDF mode
         */
        public FileInformation RenderReport<T>(T reportParam) where T: ViewReportBaseEntity
        {
            try
            {
                return ExportReport(reportParam, ReportConst.PDF_Format);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public FileInformation ExportReport<T>(T reportParam, string format = null) where T : ViewReportBaseEntity
        {
            try
            {
                ReportingServiceHelper helper = new ReportingServiceHelper();
                FileInformation result = helper.ExportReport(reportParam, format);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
    }
}
