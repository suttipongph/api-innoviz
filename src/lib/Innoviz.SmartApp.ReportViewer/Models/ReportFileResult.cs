﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.ReportViewer.Models
{
   public class ReportFileResult
    {
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public byte[] FileContents { get; set; }
    }
}
