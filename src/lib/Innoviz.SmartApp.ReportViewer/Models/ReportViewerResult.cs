﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.ReportViewer.Models
{
    public class ReportViewerResult
    {
        public List<string> StreamIDs { get; set; }
        public string Format { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public List<ReportParameterInfo> Parameters { get; set; }
        public byte[] ReportData { get; set; }
        public List<ReportServiceExecution.Warning> Warnings { get; set; }
        public ReportServiceExecution.ExecutionInfo ExecutionInfo { get; set; }

        public ReportViewerResult()
        {
            this.Parameters = new List<ReportParameterInfo>();
            this.StreamIDs = new List<string>();
            this.Warnings = new List<ReportServiceExecution.Warning>();
        }
    }
}
