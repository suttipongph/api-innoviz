﻿using System;

namespace Innoviz.SmartApp.ReportViewer.Models
{
	public class EnumNameAttribute : Attribute
	{
		public string Name { get; set; }

		public EnumNameAttribute(string name)
		{
			this.Name = name;
		}
	}
}
