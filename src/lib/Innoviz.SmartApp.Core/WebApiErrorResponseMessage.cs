﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Innoviz.SmartApp.Core
{
    [Serializable]
    public class WebApiErrorResponseMessage {
        public string Code { get; set; }
        public string Source { get; set; }
        public string Message { get; set; }
        public IDictionary ErrorMessage { get; set; }
        public IDictionary ErrorParameter { get; set; }
        public String StackTrace { get; set; }
        public List<string> MessageList { get; set; }
        public IDictionary RowParameter { get; set; }
        //public IDictionary AccumError { get; set; }
        //public IDictionary AccumErrorParameter { get; set; }
    }
}
