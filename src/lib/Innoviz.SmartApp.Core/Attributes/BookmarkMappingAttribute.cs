﻿using System;

namespace Innoviz.SmartApp.Core.Attributes
{
    public class BookmarkMappingAttribute : Attribute
    {
        public string[] BookmarkNames { get; set; }
        
        public BookmarkMappingAttribute(params string[] bookmarkNames)
        {
            BookmarkNames = bookmarkNames;
        }
    }
}
