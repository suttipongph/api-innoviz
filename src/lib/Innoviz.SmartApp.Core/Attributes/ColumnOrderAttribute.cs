﻿using System;

namespace Innoviz.SmartApp.Core.Attributes
{
    public class ColumnOrderAttribute : Attribute
    {
        public ColumnOrderAttribute(int order)
        {
            Order = order;
        }

        public int Order { get; set; }
    }
}
