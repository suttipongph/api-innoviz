﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Innoviz.SmartApp.Core.Models
{
    public interface IEntity
    {
        [Required]
        [StringLength(60)]
        String CreatedBy { get; set; }
        [Required]
        [Column(TypeName = "datetime")]
        DateTime CreatedDateTime { get; set; }
        [Required]
        [StringLength(60)]
        String ModifiedBy { get; set; }
        [Required]
        [Column(TypeName = "datetime")]
        DateTime ModifiedDateTime { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }

    public interface ICompanyEntity
    {
        Guid CompanyGUID { get; set; }
    }

    public interface IBranchEntity
    {
        Guid BranchGUID { get; set; }
    }

    public interface IDateEffectiveEntity
    {
        [Required]
        [Column(TypeName = "datetime")]
        DateTime EffectiveFrom { get; set; }
        [Column(TypeName = "datetime")]
        DateTime? EffectiveTo { get; set; }
    }
    public interface IOwnerEntity
    {
        string Owner { get; set; }
    }
    public interface IOwnerBusinessUnitEntity
    {
        Guid? OwnerBusinessUnitGUID { get; set; }
    }
}
