﻿using Innoviz.SmartApp.Core.Constants;
using System;
using System.ComponentModel.DataAnnotations;

namespace Innoviz.SmartApp.Core.Models
{
    public class BranchCompanyBaseEntity : IEntity, IBranchEntity, ICompanyEntity, IOwnerEntity, IOwnerBusinessUnitEntity
    {
        public virtual Guid BranchGUID { get; set; }
        public virtual Guid CompanyGUID { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public string Owner { get; set; }
        public Guid? OwnerBusinessUnitGUID { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
