﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Innoviz.SmartApp.Core.Models
{
    public class CompanyBaseEntity : IEntity, ICompanyEntity, IOwnerEntity, IOwnerBusinessUnitEntity
    {
        public virtual Guid CompanyGUID { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public string Owner { get; set; }
        public Guid? OwnerBusinessUnitGUID { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
