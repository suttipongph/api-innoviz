﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Innoviz.SmartApp.Core.Models
{
    public abstract class DateEffectiveBaseEntity : IEntity, ICompanyEntity, IDateEffectiveEntity, IOwnerEntity, IOwnerBusinessUnitEntity
    {
        public virtual Guid CompanyGUID { get; set; }
        public DateTime EffectiveFrom { get; set; }
        public DateTime? EffectiveTo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public string Owner { get; set; }
        public Guid? OwnerBusinessUnitGUID { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
