﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.Models
{
    public class SysTransactionLog
    {
        public string Data { get; set; }
        public DateTime Timestamp { get; set; }
        public int TransactionType { get; set; }
    }
}
