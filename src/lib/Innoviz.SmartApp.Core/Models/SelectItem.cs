﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.Models
{
    public class SelectItem<T>
    {
        public string Label { get; set; }
        public string Value { get; set; }
        public string StyleClass { get; set; }
        public string Icon { get; set; }
        public string Title { get; set; }
        public bool Disabled { get; set; }
        public T RowData { get; set; }
    }
}
