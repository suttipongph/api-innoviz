﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.Models
{
    public class AccessModeView
    {
        public bool CanCreate { get; set; }
        public bool CanView { get; set; }
        public bool CanDelete { get; set; }
        public string AccessStatus { get; set; }

    }
}
