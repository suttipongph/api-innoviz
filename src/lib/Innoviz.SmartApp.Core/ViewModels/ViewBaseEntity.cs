﻿using Innoviz.SmartApp.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModels
{
  public class ViewBaseEntity : IViewEntity
    {
        public int RowAuthorize { get; set; } = AccessMode.NoAccess.GetAttrValue();
        public string CreatedBy { get; set; }
        public string CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDateTime { get; set; }
        public byte[] RowVersion { get; set; }
    }

    public class ViewBaseEntityMap : IViewEntityMap
    {
        public int RowAuthorize { get; set; } = AccessMode.NoAccess.GetAttrValue();
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
