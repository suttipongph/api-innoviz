﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModels
{
    public class RelatedOwner
    {
        public Guid? SourcePK { get; set; } // pk of table to get owner from
        public string Owner { get; set; }
        public Guid? OwnerBusinessGUID { get; set; }
    }
}
