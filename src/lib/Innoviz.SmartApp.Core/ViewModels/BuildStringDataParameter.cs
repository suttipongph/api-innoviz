﻿namespace Innoviz.SmartApp.Core.ViewModels
{
    public class BuildStringDataParameter
    {
        public bool WriteHeader { get; set; }
        public string Delimiter { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public bool DoubleQuoteData { get; set; } = false;
    }
}
