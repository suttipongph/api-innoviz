﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModels
{
    public interface IResultBaseEntity
    {
        NotificationResponse Notification { get; set; }
    }
    public class ResultBaseEntity
    {
        public NotificationResponse Notification { get; set; } = new NotificationResponse();
    }
}
