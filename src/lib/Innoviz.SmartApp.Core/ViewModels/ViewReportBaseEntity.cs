﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModels
{
    public class ViewReportBaseEntity : IViewReportBaseEntity
    {
        public string ReportName { get; set; }
        public string ReportFormat { get; set; }
        public string CompanyGUID { get; set; }
    }
}
