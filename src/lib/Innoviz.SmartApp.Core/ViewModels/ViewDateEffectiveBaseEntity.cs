﻿using Innoviz.SmartApp.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModels
{
    public class ViewDateEffectiveBaseEntity : IViewEntity, IViewCompanyEntity, IViewDateEffectiveEntity, IViewOwnerEntity, IViewOwnerBusinessUnitEntity
    {
        public int RowAuthorize { get; set; } = AccessMode.NoAccess.GetAttrValue();
        public string CreatedBy { get; set; }
        public string CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDateTime { get; set; }
        public string CompanyGUID { get; set; }
        public string CompanyId { get; set; }
        public string EffectiveFrom { get; set; }
        public string EffectiveTo { get; set; }
        public string Owner { get; set; }
        public string OwnerBusinessUnitGUID { get; set; }
        public string OwnerBusinessUnitId { get; set; }
        public byte[] RowVersion { get; set; }
    }

    public class ViewDateEffectiveBaseEntityMap : IViewEntityMap, IViewCompanyEntityMap, IViewDateEffectiveEntityMap, IViewOwnerEntityMap, IViewOwnerBusinessUnitEntityMap
    {
        public int RowAuthorize { get; set; } = AccessMode.NoAccess.GetAttrValue();
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public Guid CompanyGUID { get; set; }
        public string CompanyId { get; set; }
        public DateTime EffectiveFrom { get; set; }
        public DateTime? EffectiveTo { get; set; }
        public string Owner { get; set; }
        public Guid? OwnerBusinessUnitGUID { get; set; }
        public string OwnerBusinessUnitId { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
