﻿using Innoviz.SmartApp.Core.Constants;
using NJsonSchema.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModels
{
    public class SearchParameter
    {
        public List<SearchCondition> Conditions { get; set; } = new List<SearchCondition>();
        public Paginator Paginator { get; set; } = new Paginator();
        public bool IsAsc { get; set; }
        public string SortColumn { get; set; }
        public string RefTable { get; set; }
        public string BranchFilterMode { get; set; }
        public string CompanyBaseGUID { get; set; }
        public string BranchBaseGUID { get; set; }
        public List<bool> IsAscs { get; set; }
        public List<string> SortColumns { get; set; }
        public bool ExcludeRowData { get; set; } = false;

    }
    public class SearchCondition
    {
        public string ColumnName { get; set; }
        public string SubColumnName { get; set; }
        public string ParameterName { get; set; }
        public string Value { get; set; }
        public List<string> Values { get; set; }
        public string Type { get; set; } = ColumnType.STRING;
        public string Operator { get; set; } = Operators.AND;
        public string EqualityOperator { get; set; } = Operators.EQUAL;
        public int Bracket { get; set; }
    }
    public class Paginator
    {
        public int Page { get; set; } = 0;
        public int First { get; set; } = 0;
        public int Rows { get; set; } = -1;
        public int PageCount { get; set; } = 0;
        public int TotalRecord { get; set; } = 0;

    }
    [JsonSchemaFlatten]
    public class SearchResult<T> : ResultBaseEntity
    {
        public List<T> Results { get; set; }
        public Paginator Paginator { get; set; }
    }
    public class SearchParameter1
    {
        public List<SearchCondition> Conditions { get; set; }
        public Paginator Paginator { get; set; }
        public bool IsAsc { get; set; }
        public string SortColumn { get; set; }
    }
    public class SearchPredicate
    {
        public string Predicates { get; set; } = TextConstants.PredicateAll;
        public object[] Values { get; set; } = null;
        public string Sorting { get; set; } = "";
        public string TotalPredicates { get; set; } = TextConstants.PredicateAll;
    }
}
