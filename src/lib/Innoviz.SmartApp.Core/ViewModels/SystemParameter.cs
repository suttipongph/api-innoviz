﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModels
{
    public class SystemParameter
    {
        public string UserName { get; set; }
        public string InstanceHistoryId { get; set; }
        public AccessLevelParm AccessLevel { get; set; }
        public AccessLevelParm DeleteAccessLevel { get; set; }
        public string CompanyGUID { get; set; }
        public string BranchGUID { get; set; }
        public int SiteLogin { get; set; }
        public string UserId { get; set; }
        public string BusinessUnitGUID { get; set; }
        public string RequestId { get; set; }
        public string BearerToken { get; set; }
        public Dictionary<string, List<RelatedOwner>> CacheRelatedOwner { get; set; }
    }
    public class AccessLevelParm
    {
        public bool SkipCheck { get; set; } = false;
        public int AccessLevel { get; set; }
        public string AccessValue { get; set; }
        public List<string> AccessValues { get; set; }
    }
}
