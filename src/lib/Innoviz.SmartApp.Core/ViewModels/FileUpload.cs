﻿using System.Collections.Generic;

namespace Innoviz.SmartApp.Core.ViewModels
{
    public class FileUpload
    {
        public List<FileInformation> FileInfos { get; set; }
        public string JsonStringParm { get; set; }
    }
    public class FileInformation
    {
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string Base64 { get; set; }
        public string Path { get; set; }
    }
    public class FileImportSingleTypeResult<T>
    {
        public List<T> ModelResult { get; set; }
        public string SerializedFromFile { get; set; }
    }
}
