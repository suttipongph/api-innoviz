﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModels
{
    public interface ITree<T>
    {
        T Data { get; set; }
        ITree<T> Parent { get; set; }
        ICollection<ITree<T>> Children { get; set; }
        bool Leaf { get; }
        bool Expanded { get; set; }

    }
}
