﻿using Innoviz.SmartApp.Core.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModels
{
    public class ViewBranchCompanyBaseEntity : IViewEntity, IViewBranchEntity, IViewCompanyEntity, IViewOwnerEntity, IViewOwnerBusinessUnitEntity
    {
        public int RowAuthorize { get; set; } = AccessMode.NoAccess.GetAttrValue();
        public string CreatedBy { get; set; }
        public string CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDateTime { get; set; }
        public string BranchGUID { get; set; }
        public string CompanyGUID { get; set; }
        public string BranchId { get; set; }
        public string CompanyId { get; set; }
        public string Owner { get; set; }
        public string OwnerBusinessUnitGUID { get; set; }
        public string OwnerBusinessUnitId { get; set; }
        public byte[] RowVersion { get; set; }
    }

    public class ViewBranchCompanyBaseEntityMap : IViewEntityMap, IViewBranchEntityMap, IViewCompanyEntityMap, IViewOwnerEntityMap, IViewOwnerBusinessUnitEntityMap
    {
        public int RowAuthorize { get; set; } = AccessMode.NoAccess.GetAttrValue();
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public Guid BranchGUID { get; set; }
        public Guid CompanyGUID { get; set; }
        public string BranchId { get; set; }
        public string CompanyId { get; set; }
        public string Owner { get; set; }
        public Guid? OwnerBusinessUnitGUID { get; set; }
        public string OwnerBusinessUnitId { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
