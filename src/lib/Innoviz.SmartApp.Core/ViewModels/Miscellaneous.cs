﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModels
{
    public class RowIdentity
    {
        public string Guid { get; set; }
        public int? RowIndex { get; set; }
    }
    public class Deletion
    {
        public string Guid { get; set; }
        public SearchParameter searchParameter { get; set; }
        public DecisionInformation decisionInformation { get; set; }
    }
    public class DecimalLength
    {
        public int Above { get; set; }
        public int Below { get; set; }
    }
    public class TranslateModel
    {
        public string Code { get; set; }
        public object[] Parameters { get; set; }
    }
    public class BatchLogReference
    {
        public string Reference { get; set; }
        public string ItemValues { get; set; }
        public string Key { get; set; }

        public BatchLogReference()
        {

        }
        public BatchLogReference(TranslateModel referenceTranslate, TranslateModel itemValuesTranslate)
        {
            Reference = SystemStaticData.GetTranslatedMessage(referenceTranslate);
            ItemValues = SystemStaticData.GetTranslatedMessage(itemValuesTranslate);
        }
    }
    public class BatchFunctionClassMapping
    {
        public string ClassName { get; set; }
        public string MethodName { get; set; }
    }
    
    public class NotificationResponse
    {
        public List<string> NotificationType { get; set; }
        public IDictionary NotificationMessage { get; set; }
        public IDictionary NotificationParameter { get; set; }
        public void AddData(string code, string[] parameters = null)
        {
            parameters = parameters ?? new string[0];
            NotificationType = NotificationType ?? new List<string>();
            NotificationMessage = NotificationMessage ?? new Dictionary<string, string>();
            NotificationParameter = NotificationParameter ?? new Dictionary<string, string[]>();
            if (code.Contains("SUCCESS."))
            {
                NotificationType.Add(NotiType.SUCCESSING);
            }
            else if (code.Contains("ERROR.") || code.Contains("LABEL."))
            {
                NotificationType.Add(NotiType.WARNING);
            }
            else
            {
                NotificationType.Add(NotiType.INFORMING);
            }
            int codeCount = 0;
            foreach (var item in NotificationMessage.Keys)
            {
                if (((string)item).Contains(code.ToLower() + "."))
                {
                    codeCount++;
                }
            }
            string newCode = code.ToLower() + "." + codeCount;
            NotificationMessage.Add(newCode, newCode.ToUpper());
            if (parameters.Length > 0)
            {
                List<string> indexs = new List<string>();
                foreach (var item in parameters)
                {
                    indexs.Add(item);
                }
                NotificationParameter.Add(newCode, indexs.ToArray());
            }
        }
        public void AddData(string code, string type, string[] parameters = null)
        {
            parameters = parameters ?? new string[0];
            NotificationType = NotificationType ?? new List<string>();
            NotificationMessage = NotificationMessage ?? new Dictionary<string, string>();
            NotificationParameter = NotificationParameter ?? new Dictionary<string, string[]>();
            NotificationType.Add(type);
            if (code.Contains("SUCCESS."))
            {
                NotificationType.Add(NotiType.SUCCESSING);
            }
            else if (code.Contains("ERROR.") || code.Contains("LABEL."))
            {
                NotificationType.Add(NotiType.WARNING);
            }
            else
            {
                NotificationType.Add(NotiType.INFORMING);
            }
            NotificationMessage.Add(code, code);
            if (parameters.Length > 0)
            {
                List<string> indexs = new List<string>();
                foreach (var item in parameters)
                {
                    indexs.Add(item);
                }
                NotificationParameter.Add(code, indexs.ToArray());
            }
        }
        public void AddData(string code)
        {
            NotificationType = NotificationType ?? new List<string>();
            NotificationMessage = NotificationMessage ?? new Dictionary<string, string>();
            NotificationParameter = NotificationParameter ?? new Dictionary<string, string[]>();
            if (code.Contains("SUCCESS."))
            {
                NotificationType.Add(NotiType.SUCCESSING);
            }
            else if (code.Contains("ERROR.") || code.Contains("LABEL."))
            {
                NotificationType.Add(NotiType.WARNING);
            }
            else
            {
                NotificationType.Add(NotiType.INFORMING);
            }
            NotificationMessage.Add(code, code);
        }
    }
    public class RefTypeModel
    {
        public int RefType { get; set; }
        public Guid RefGUID { get; set; }
    }
    public class DocConVerifyTypeModel
    {
        public int DocConVerifyType { get; set; }
        public Guid RefGUID { get; set; }
    }
}
