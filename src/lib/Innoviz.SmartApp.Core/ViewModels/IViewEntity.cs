﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModels
{
    public interface IViewEntity
    {
        String CreatedBy { get; set; }
        String CreatedDateTime { get; set; }
        String ModifiedBy { get; set; }
        String ModifiedDateTime { get; set; }
        byte[] RowVersion { get; set; }
    }

    public interface IViewCompanyEntity
    {
        String CompanyGUID { get; set; }
        String CompanyId { get; set; }
    }

    public interface IViewBranchEntity
    {
        String BranchGUID { get; set; }
        String BranchId { get; set; }
    }

    public interface IViewDateEffectiveEntity
    {
        String EffectiveFrom { get; set; }
        String EffectiveTo { get; set; }
    }

    public interface IViewEntityMap
    {
        String CreatedBy { get; set; }
        DateTime CreatedDateTime { get; set; }
        String ModifiedBy { get; set; }
        DateTime ModifiedDateTime { get; set; }
        byte[] RowVersion { get; set; }
    }

    public interface IViewCompanyEntityMap
    {        
        Guid CompanyGUID { get; set; }
        String CompanyId { get; set; }
    }

    public interface IViewBranchEntityMap
    {
        Guid BranchGUID { get; set; }
        String BranchId { get; set; }
    }
    public interface IViewDateEffectiveEntityMap
    {
        DateTime EffectiveFrom { get; set; }
        DateTime? EffectiveTo { get; set; }
    }
    public interface IViewOwnerEntity
    {
        string Owner { get; set; }
    }
    public interface IViewOwnerBusinessUnitEntity
    {
        string OwnerBusinessUnitGUID { get; set; }
        string OwnerBusinessUnitId { get; set; }
    }
    public interface IViewOwnerEntityMap
    {
        string Owner { get; set; }
    }
    public interface IViewOwnerBusinessUnitEntityMap
    {
        Guid? OwnerBusinessUnitGUID { get; set; }
        string OwnerBusinessUnitId { get; set; }
    }
    public interface IViewReportBaseEntity
    {
        string ReportName { get; set; }
        string ReportFormat { get; set; }
        string CompanyGUID { get; set; }
    }
}
