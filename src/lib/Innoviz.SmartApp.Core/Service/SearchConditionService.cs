﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Core.Service
{
    public static class SearchConditionService
    {
        public static SearchParameter GetParentCondition(this SearchParameter search, string parentColumn)
        {
            try
            {
                search.Conditions.ForEach(cond =>
                {
                    if (cond.ColumnName == TextConstants.ParentColumn)
                    {
                        cond.ColumnName = parentColumn;
                    }
                });
                return search;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchParameter GetParentCondition(this SearchParameter search, string[] parentColumnNames)
        {
            try
            {
                for (int i = 0; i < search.Conditions.Count(); i++)
                {
                    if (search.Conditions[i].ColumnName == TextConstants.SearchConditionValue)
                    {
                        search.Conditions[i].ColumnName = parentColumnNames[i];
                        if (search.Conditions[i].Type == ColumnType.DATERANGE)
                        {
                            search.Conditions[i].SubColumnName = parentColumnNames[i];
                        }
                    }
                }
                return search;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchParameter GetParentRankProductTypeCondition(this SearchParameter search, string[] parentColumnNames)
        {
            try
            {

                search.Conditions[0].ColumnName = parentColumnNames[0];
                search.Conditions[0].SubColumnName = parentColumnNames[0];
                search.Conditions[0].Bracket = (int)BracketType.SingleStart;
                search.Conditions[0].Type = ColumnType.INT;

                search.Conditions[1].ColumnName = parentColumnNames[1];
                search.Conditions[1].SubColumnName = parentColumnNames[1];
                search.Conditions[1].Bracket = (int)BracketType.SingleEnd;
                search.Conditions[1].Type = ColumnType.INT;
                search.Conditions[1].Operator = Operators.OR;
                return search;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetEmployeeActiveCondition()
        {
            try
            {
                SearchCondition condition = new SearchCondition()
                {
                    ColumnName = EmployeeCondition.InActive,
                    Value = "false",
                    Type = ColumnType.BOOLEAN
                };
                return condition;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetAddressTransRefTypeCondition(int refType)
        {
            try
            {
                SearchCondition condition = new SearchCondition()
                {
                    ColumnName = AddressTransCondition.RefType,
                    Value = refType.ToString(),
                    Type = ColumnType.INT
                };
                return condition;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetDraftStatusCondition(string statusId, string processId)
        {
            try
            {


                List<SearchCondition> conditions = new List<SearchCondition>();
                conditions.Add(new SearchCondition
                {
                    ColumnName = DocumentStatusAsignmentAgreementCondition.StatusId,
                    Value = statusId,
                    Type = ColumnType.STRING,
                    EqualityOperator = Operators.NOT_EQUAL
                });
                conditions.Add(new SearchCondition
                {
                    ColumnName = DocumentStatusAsignmentAgreementCondition.ProcessId,
                    Value = processId,
                    Type = ColumnType.STRING,
                });
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetCustBankItemByBankAccountControlCondition(bool inactive, bool bankAccountControl)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                conditions.Add(new SearchCondition
                {
                    ColumnName = CustBankCondition.Inactive,
                    Value = inactive.ToString(),
                    Type = ColumnType.BOOLEAN
                });
                conditions.Add(new SearchCondition
                {
                    ColumnName = CustBankCondition.BankAccountControl,
                    Value = bankAccountControl.ToString(),
                    Type = ColumnType.BOOLEAN
                });
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
       
        public static List<SearchCondition> GetCustBankItemByPDCCondition(bool inactive, bool PDC)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                conditions.Add(new SearchCondition
                {
                    ColumnName = CustBankCondition.Inactive,
                    Value = inactive.ToString(),
                    Type = ColumnType.BOOLEAN
                });
                conditions.Add(new SearchCondition
                {
                    ColumnName = CustBankCondition.PDC,
                    Value = PDC.ToString(),
                    Type = ColumnType.BOOLEAN
                });
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> SetAllNotEqual(this List<SearchCondition> conds)
        {
            try
            {
                if (conds != null && conds.Count() > 0)
                {
                    foreach (var item in conds)
                    {
                        item.EqualityOperator = Operators.NOT_EQUAL;
                    }
                }
                return conds;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetContactPersonTransItemByRefTypeCondition(bool inactive, int refType)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                conditions.Add(new SearchCondition
                {
                    ColumnName = ContactPersonTransCondition.Inactive,
                    Value = inactive.ToString(),
                    Type = ColumnType.BOOLEAN
                });
                conditions.Add(new SearchCondition
                {
                    ColumnName = ContactPersonTransCondition.RefType,
                    Value = refType.ToString(),
                    Type = ColumnType.BOOLEAN
                });
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetApplicationNotStatusCondition(string[] statuses)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                foreach (var status in statuses)
                {
                    conditions.Add(new SearchCondition
                    {
                        ColumnName = ApplicationTableCondition.StatusId,
                        Value = status.ToString(),
                        Type = ColumnType.STRING,
                        EqualityOperator = Operators.NOT_EQUAL
                    });
                }

                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetMainAgreementIsStatusCondition(string[] statuses)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                foreach (var status in statuses)
                {
                    conditions.Add(new SearchCondition
                    {
                        ColumnName = MainAgreementCondition.AgreementDocType,
                        Value = status.ToString(),
                        Type = ColumnType.STRING,
                        EqualityOperator = Operators.EQUAL
                    });
                }

                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetAssignmentAgreementNotStatusCondition(string[] statuses)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                foreach (var status in statuses)
                {
                    conditions.Add(new SearchCondition
                    {
                        ColumnName = ApplicationTableCondition.StatusId,
                        Value = status.ToString(),
                        Type = ColumnType.STRING,
                        EqualityOperator = Operators.NOT_EQUAL
                    });
                }
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetAssignmentAgreementStatusCondition(string status)
        {
            try
            {
               return new SearchCondition
                {
                    ColumnName = AssignmentAgreementCondition.StatusId,
                    Value = status.ToString(),
                    Type = ColumnType.STRING,
                    EqualityOperator = Operators.EQUAL
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetWithdrawalTableNotStatusCondition(string[] statuses)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                foreach (var status in statuses)
                {
                    conditions.Add(new SearchCondition
                    {
                        ColumnName = WithdrawalTableCondition.StatusId,
                        Value = status.ToString(),
                        Type = ColumnType.STRING,
                        EqualityOperator = Operators.NOT_EQUAL
                    });
                }

                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetBookmarkDocumentByBookmarkDocumentRefTypeNotCondition(string[] statuses)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                foreach (var status in statuses)
                {
                    conditions.Add(new SearchCondition
                    {
                        ColumnName = BookmarkDocumentCondition.BookmarkDocumentRefType,
                        Value = status.ToString(),
                        Type = ColumnType.ENUM,
                        EqualityOperator = Operators.NOT_EQUAL
                    });
                }

                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetBookmarkDocumentByBookmarkDocumentRefTypeCondition(string refType)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                conditions.Add(new SearchCondition
                {
                    ColumnName = BookmarkDocumentCondition.BookmarkDocumentRefType,
                    Value = "0", // none
                    Type = ColumnType.INT,
                    Bracket = (int)BracketType.SingleStart
                });
                conditions.Add(new SearchCondition
                {
                    ColumnName = BookmarkDocumentCondition.BookmarkDocumentRefType,
                    Value = refType,
                    Type = ColumnType.INT,
                    Operator = Operators.OR,
                    Bracket = (int)BracketType.SingleEnd
                });
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public static List<SearchCondition> GetDocumentTemplateTableByDocumentTemplateTypeCondition(string[] statuses)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                foreach (var status in statuses)
                {
                    conditions.Add(new SearchCondition
                    {
                        ColumnName = DocumentTemplateCondition.DocumentTemplateType,
                        Value = status.ToString(),
                        Type = ColumnType.ENUM,
                        EqualityOperator = Operators.EQUAL
                    });
                }

                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetCancelledCustBusinessCollateralCondition(bool cancelled)
        {
            try
            {
                SearchCondition condition = new SearchCondition()
                {
                    ColumnName = CustBusinessCollateralCondition.Cancelled,
                    Value = cancelled.ToString(),
                    Type = ColumnType.BOOLEAN
                };
                return condition;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetAuthorizedPersonTransRefTypeCondition(int refType)
        {
            try
            {
                SearchCondition condition = new SearchCondition()
                {
                    ColumnName = AuthorizedPersonTransCondition.RefType,
                    Value = refType.ToString(),
                    Type = ColumnType.INT
                };
                return condition;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetSiteLoginCondition(int siteLoginType)
        {
            try
            {
                SearchCondition condition = new SearchCondition()
                {
                    ColumnName = SysRoleTableCondition.SiteLoginType,
                    Value = siteLoginType.ToString(),
                    Type = ColumnType.INT
                };
                return condition;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public static SearchCondition GetCreditAppRequestTableStatusCondition(string status)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = CreditAppRequestTableCondition.StatusId,
                    Value = status.ToString(),
                    Type = ColumnType.STRING,
                    EqualityOperator = Operators.EQUAL
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetCreditAppTableStatusCondition(string status)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = CreditAppTableCondition.StatusId,
                    Value = status.ToString(),
                    Type = ColumnType.STRING,
                    EqualityOperator = Operators.EQUAL
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetSysFeatureGroupRoleByRoleIdCondition(Guid id)
        {
            try
            {
                SearchCondition condition = new SearchCondition()
                {
                    ColumnName = SysFeatureGroupRoleCondition.RoleGUID,
                    Value = id.GuidNullToString(),
                    Type = ColumnType.MASTER
                };
                return condition;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetCreditAppReqBusinessCollateralIsNewCondition(bool isNew)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = CreditAppReqBusinessCollateralCondition.IsNew,
                    Value = isNew.ToString(),
                    Type = ColumnType.BOOLEAN,
                    EqualityOperator = Operators.EQUAL
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetRelatedPersonTableByRefTypeCondition(bool inactive, int refType)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                conditions.Add(new SearchCondition
                {
                    ColumnName = RelatedPersonTableCondition.Inactive,
                    Value = inactive.ToString(),
                    Type = ColumnType.BOOLEAN
                });
                conditions.Add(new SearchCondition
                {
                    ColumnName = RelatedPersonTableCondition.RefType,
                    Value = refType.ToString(),
                    Type = ColumnType.BOOLEAN
                });
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public static SearchCondition GetInvoiceRevenueTypeByProductTypeCondition(List<string> enumValues)
        {
            try
            {
                SearchCondition conditions = new SearchCondition();

                conditions = new SearchCondition
                {
                    ColumnName = InvoiceRevenueTypeCondition.ProductType,
                    Values = enumValues,
                    Type = ColumnType.MASTER,
                    EqualityOperator = Operators.EQUAL
                };
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetInvoiceTypeBySuspenseInvoiceType(List<string> suspenseInvoiceType)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = InvoiceTypeCondition.SuspenseInvoiceType,
                    Values = suspenseInvoiceType,
                    Type = ColumnType.MASTER,
                    EqualityOperator = Operators.EQUAL
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetDocumentStatusByVerificationTableDropDown(int ProcessId)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = DocumentStatusCondition.ProcessId,
                    Value = ProcessId.ToString(),
                    Type = ColumnType.STRING,
                    EqualityOperator = Operators.EQUAL
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public static List<SearchCondition> GetSysFeatureGroupRoleByAdminRoleCondition(Guid companyGUID, int site)
        {
            try
            {
                List<SearchCondition> result = new List<SearchCondition>();
                SearchCondition roleDisplayName = new SearchCondition
                {
                    ColumnName = SysFeatureGroupRoleCondition.SysRoleTable_DisplayName,
                    Value = TextConstants.ADMIN,
                    Type = ColumnType.INT
                };
                result.Add(roleDisplayName);

                SearchCondition company = new SearchCondition
                {
                    ColumnName = SysFeatureGroupRoleCondition.SysRoleTable_CompanyGUID,
                    Value = companyGUID.GuidNullToString(),
                    Type = ColumnType.MASTER
                };
                result.Add(company);

                SearchCondition siteLoginType = new SearchCondition
                {
                    ColumnName = SysFeatureGroupRoleCondition.SysRoleTable_SiteLoginType,
                    Value = site.ToString(),
                    Type = ColumnType.INT
                };
                result.Add(siteLoginType);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetChequeTableByReceivedFromCondition(List<string> enumValues)
        {
            try
            {
                SearchCondition conditions = new SearchCondition();

                conditions = new SearchCondition
                {
                    ColumnName = ChequeTableCondition.ReceivedFrom,
                    Values = enumValues,
                    Type = ColumnType.MASTER,
                    EqualityOperator = Operators.EQUAL
                };
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public static SearchCondition GetChequeTableByRefTypeCondition(List<string> enumValues)
        {
            try
            {
                SearchCondition conditions = new SearchCondition();

                conditions = new SearchCondition
                {
                    ColumnName = ChequeTableCondition.RefType,
                    Values = enumValues,
                    Type = ColumnType.MASTER,
                    EqualityOperator = Operators.EQUAL
                };
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public static SearchCondition GetChequeTableByStatusIdCondition(List<string> statusValues)
        {
            try
            {
                SearchCondition conditions = new SearchCondition();

                conditions = new SearchCondition
                {
                    ColumnName = ChequeTableCondition.StatusId,
                    Values = statusValues,
                    Type = ColumnType.MASTER,
                    EqualityOperator = Operators.EQUAL
                };
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetVerificationTableByStatusIdDropDown(string statusId)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = VerificationTableCondition.StatusId,
                    Value = statusId.ToString(),
                    Type = ColumnType.STRING,
                    EqualityOperator = Operators.EQUAL
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetActionHistoryRefGUIDCondition(Guid refGUID)
        {
            try
            {
                SearchCondition cond = new SearchCondition
                {
                    ColumnName = ActionHistoryCondition.RefGUID,
                    Value = refGUID.GuidNullToString(),
                    Type = ColumnType.MASTER
                };
                return cond;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
     
        public static List<SearchCondition> GetRetentionTransByCreditAppCondition(string refType)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                conditions.Add(new SearchCondition
                {
                    ColumnName = RetentionTransByCreditAppCondition.RefType,
                    Value = refType,
                    Type = ColumnType.STRING
                });
               
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetActionHistoryForK2Condition(Guid refGUID, string actionName, string activityName)
        {
            try
            {
                List<SearchCondition> result = new List<SearchCondition>();
                result.Add(GetActionHistoryRefGUIDCondition(refGUID));
                SearchCondition actionCond = new SearchCondition
                {
                    ColumnName = ActionHistoryCondition.ActionName,
                    Value = actionName,
                    Type = ColumnType.STRING
                };
                result.Add(actionCond);
                SearchCondition activityCond = new SearchCondition
                {
                    ColumnName = ActionHistoryCondition.ActivityName,
                    Value = activityName,
                    Type = ColumnType.STRING
                };
                result.Add(activityCond);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public static SearchCondition GetBuyerAgreementTransByRefTypeCondition(string enumValue)
        {
            try
            {
                SearchCondition conditions = new SearchCondition();

                conditions = new SearchCondition
                {
                    ColumnName = ChequeTableCondition.RefType,
                    Value = enumValue,
                    Type = ColumnType.MASTER,
                    EqualityOperator = Operators.EQUAL
                };
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetBuyerReceiptTableByCancelCondition(bool isCancelled)
        {
            try
            {
                SearchCondition condition = new SearchCondition()
                {
                    ColumnName = BuyerReceiptTableCondition.Cancel,
                    Value = isCancelled.ToString(),
                    Type = ColumnType.BOOLEAN
                };
                return condition;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetInvoiceTypeByDirectReceiptCondition(bool isDirectReceipt)
        {
            try
            {
                SearchCondition condition = new SearchCondition()
                {
                    ColumnName = InvoiceTypeCondition.DirectReceipt,
                    Value = isDirectReceipt.ToString(),
                    Type = ColumnType.BOOLEAN
                };
                return condition;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetBookmarkDocumentTemplateCondition(string refType)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                conditions.Add(new SearchCondition
                {
                    ColumnName = BookmarkDocumentCondition.BookmarkDocumentRefType,
                    Value = "0", // none
                    Type = ColumnType.INT,
                    Bracket = (int)BracketType.SingleStart
                });
                conditions.Add(new SearchCondition
                {
                    ColumnName = BookmarkDocumentCondition.BookmarkDocumentRefType,
                    Value = refType,
                    Type = ColumnType.INT,
                    Operator = Operators.OR,
                    Bracket = (int)BracketType.SingleEnd
                });
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetRefTypeConditionValue(this SearchParameter search)
        {
            try
            {

                SearchCondition conditions = search.Conditions.Where(w => w.ColumnName.ToLower() == TextConstants.RefType.ToLower()).FirstOrDefault();                
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetVerificationTableNotEqualDropDown(List<string> verificationTableGUIDs)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = VerificationTableCondition.VerificationTableGUID,
                    Values = verificationTableGUIDs,
                    Type = ColumnType.MASTER,
                    EqualityOperator = Operators.NOT_EQUAL
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetBuyerAgreementTransByRefTypeConditionForBuyerAgreementTable(string enumValue)
        {
            try
            {
                SearchCondition conditions = new SearchCondition();

                conditions = new SearchCondition
                {
                    ColumnName = BuyerAgreementTableCondition.BuyerAgreementTransRefType,
                    Value = enumValue,
                    Type = ColumnType.MASTER,
                    EqualityOperator = Operators.EQUAL
                };
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetOriginalBusinessCollateralAgmIDCondition(Guid guid ,Guid status ,int doctype)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                conditions.Add(new SearchCondition
                {
                    ColumnName = BusinessCollateralAgmTableCondition.RefBusinessCollateralAgmTableGUID,
                    Value = guid.ToString(),
                    Type = ColumnType.MASTER,
                    Bracket = (int)BracketType.SingleStart,
                    Operator = Operators.OR
                });
                conditions.Add(new SearchCondition
                {
                    ColumnName = BusinessCollateralAgmTableCondition.DocumentStatusGUID,
                    Value = status.ToString(),
                    Type = ColumnType.MASTER,
                });
                conditions.Add(new SearchCondition
                {
                    ColumnName = BusinessCollateralAgmTableCondition.AgreementDocType,
                    Value = doctype.ToString(),
                    Type = ColumnType.INT,
                    Bracket = (int)BracketType.DoubleEnd
                });
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetCreditOutstandingByCustomerCondition(string customerTableGUID ,Guid? ParentCreditLimitType)
        {
            try
            {
                List<SearchCondition> conditions = new List<SearchCondition>();
                conditions.Add(new SearchCondition
                {
                    ColumnName = CustomerTableCondition.CustomerTableGUID,
                    Value = customerTableGUID,
                    Type = ColumnType.MASTER,
                });
                conditions.Add(new SearchCondition
                {
                    ColumnName = CustomerTableCondition.ParentCreditLimitType,
                    Value = ParentCreditLimitType.ToString(),
                    Type = ColumnType.MASTER,
                });

                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetInvoiceTableBySuspenseInvoiceType(string suspenseInvoiceType)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = InvoiceTableCondition.SuspenseInvoiceType,
                    Value = suspenseInvoiceType,
                    Type = ColumnType.INT,
                    EqualityOperator = Operators.EQUAL
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetInvoiceTableByProductInvoice(string productInvoice)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = InvoiceTableCondition.ProductInvoice,
                    Value = productInvoice,
                    Type = ColumnType.BOOLEAN,
                    EqualityOperator = Operators.EQUAL
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        public static SearchCondition GetInvoiceRevenueTypeByIntercompany(string intercompany)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = InvoiceRevenueTypeCondition.IntercompanyTableGUID,
                    Value = intercompany,
                    Type = ColumnType.NULLABLE,
                    EqualityOperator = Operators.NOT_EQUAL
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetCALineOutstandingByCAByrefTypeBuyerTable(string buyertableguid)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = CreditAppLineCondition.BuyerTableGUID,
                    Value = buyertableguid,
                    Type = ColumnType.MASTER,
                    EqualityOperator = Operators.EQUAL
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetCALineOutstandingByCAByrefTypeCreditAppTable(string creditapptableguid)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = CreditAppLineCondition.CreditAppTableGUID,
                    Value = creditapptableguid,
                    Type = ColumnType.MASTER,
                    EqualityOperator = Operators.EQUAL
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<SearchCondition> GetVerificationDocumentStatusCondition(string statusId)
        {
            try
            {


                List<SearchCondition> conditions = new List<SearchCondition>();
                conditions.Add(new SearchCondition
                {
                    ColumnName = VerificationTableCondition.StatusId,
                    Value = statusId,
                    Type = ColumnType.STRING,
                    EqualityOperator = Operators.EQUAL
                });
                return conditions;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetContactPersonTransItemByInActiveCondition(bool inactive)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = ContactPersonTransCondition.Inactive,
                    Value = inactive.ToString(),
                    Type = ColumnType.BOOLEAN
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetMessengerJobTableItemByResultCondition(int result)
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = MessengerJobCondition.Result,
                    Value = result.ToString(),
                    Type = ColumnType.INT
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
