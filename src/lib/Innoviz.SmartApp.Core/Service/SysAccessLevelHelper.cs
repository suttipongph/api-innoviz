﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Innoviz.SmartApp.Core.Service
{
    public static class SysAccessLevelHelper
    {
        public static SearchCondition GetFilterCondByAccessLevel(AccessLevelParm accessLevel)
        {
            try
            {
                if (accessLevel == null)
                {
                    return GetPredicateNotCondition();
                }

                AccessLevel access = (AccessLevel)accessLevel.AccessLevel;
                switch (access)
                {
                    case AccessLevel.None:
                        return GetPredicateNotCondition();
                    case AccessLevel.User:
                        return GetOwnerFilterCondition(accessLevel.AccessValue);
                    case AccessLevel.BusinessUnit:
                        return GetOwnerBUFilterCondition(accessLevel.AccessValue);
                    case AccessLevel.ParentChildBU:
                        return GetParentChildBUFilterCondition(accessLevel.AccessValues);
                    case AccessLevel.Company:
                        return null;
                    default:
                        throw new Exception("Invalid access level.");
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetParentChildBUFilterCondition(List<string> parentChildBU)
        {
            try
            {
                SearchCondition result = new SearchCondition();
                if (parentChildBU.Count() > 0)
                {
                    result = new SearchCondition
                    {
                        ColumnName = TextConstants.OwnerBusinessUnitGUID,
                        Operator = Operators.AND,
                        Values = parentChildBU,
                        Type = ColumnType.MASTER
                    };
                }
                else
                {
                    result = GetPredicateNotCondition();
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetOwnerBUFilterCondition(string buGUID)
        {
            try
            {
                SearchCondition result = new SearchCondition();
                if (buGUID != null)
                {
                    result = new SearchCondition
                    {
                        ColumnName = TextConstants.OwnerBusinessUnitGUID,
                        Operator = Operators.AND,
                        Value = buGUID,
                        Type = ColumnType.MASTERSINGLE
                    };
                }
                else
                {
                    result = GetPredicateNotCondition();
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetOwnerFilterCondition(string userName)
        {
            try
            {
                SearchCondition result =
                new SearchCondition
                {
                    ColumnName = TextConstants.Owner,
                    Operator = Operators.AND,
                    Value = userName,
                    Type = typeof(string).Name
                };
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchCondition GetPredicateNotCondition()
        {
            try
            {
                return new SearchCondition
                {
                    ColumnName = TextConstants.Owner,
                    Operator = Operators.AND,
                    Value = null,
                    Type = ColumnType.PREDICATE_NOT
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public static void CheckOwnerBUByAccessLevel<T>(T item, AccessLevelParm accessLevel, string username)
        {
            try
            {
                AccessLevel access = (AccessLevel)accessLevel.AccessLevel;
                PropertyInfo ownerBUPropInfo = item.GetType().GetProperty(TextConstants.OwnerBusinessUnitGUID);
                PropertyInfo ownerPropInfo = item.GetType().GetProperty(TextConstants.Owner);

                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                ex.AddData("ERROR.00850", username);

                switch (access)
                {
                    case AccessLevel.None:
                        throw ex;
                    case AccessLevel.User:
                        if (ownerPropInfo.GetValue(item)?.ToString() == accessLevel.AccessValue)
                            break;
                        else
                            throw ex;
                    case AccessLevel.BusinessUnit:
                        if (ownerBUPropInfo.GetValue(item).GuidNullToString() == accessLevel.AccessValue)
                            break;
                        else
                            throw ex;
                    case AccessLevel.ParentChildBU:
                        if (accessLevel.AccessValues.Any(a => a == ownerBUPropInfo.GetValue(item).GuidNullToString()))
                            break;
                        else
                            throw ex;
                    case AccessLevel.Company:
                        break;
                    default:
                        throw ex;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static void CheckOwnerBUByAccessLevel<T>(List<T> list, AccessLevelParm accessLevel, string username)
        {
            try
            {
                AccessLevel access = (AccessLevel)accessLevel.AccessLevel;
                PropertyInfo ownerBUPropInfo = list.FirstOrDefault().GetType().GetProperty(TextConstants.OwnerBusinessUnitGUID);
                PropertyInfo ownerPropInfo = list.FirstOrDefault().GetType().GetProperty(TextConstants.Owner);

                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                ex.AddData("ERROR.00850", username);

                switch (access)
                {
                    case AccessLevel.None:
                        throw ex;
                    case AccessLevel.User:
                        if (list.All(item => ownerPropInfo.GetValue(item)?.ToString() == accessLevel.AccessValue))
                            break;
                        else
                            throw ex;
                    case AccessLevel.BusinessUnit:
                        if (list.All(item => ownerBUPropInfo.GetValue(item).GuidNullToString() == accessLevel.AccessValue))
                            break;
                        else
                            throw ex;
                    case AccessLevel.ParentChildBU:
                        if (list.All(item => accessLevel.AccessValues.Any(a => a == ownerBUPropInfo.GetValue(item).GuidNullToString())))
                            break;
                        else
                            throw ex;
                    case AccessLevel.Company:
                        break;
                    default:
                        throw ex;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static int GetLoginSiteValue(string site)
        {
            try
            {
                switch (site)
                {
                    case SiteLoginType.FRONT: return SiteLoginValue.Front;
                    case SiteLoginType.BACK: return SiteLoginValue.Back;
                    case SiteLoginType.CASHIER: return SiteLoginValue.Cashier;
                    case SiteLoginType.OB: return SiteLoginValue.OB;

                    default: return 1;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string GetSiteHeaderFromEnumValue(int siteVal)
        {
            try
            {
                switch(siteVal)
                {
                    case SiteLoginValue.Front: return SiteLoginType.FRONT;
                    case SiteLoginValue.Back: return SiteLoginType.BACK;
                    case SiteLoginValue.Cashier: return SiteLoginType.CASHIER;
                    case SiteLoginValue.OB: return SiteLoginType.OB;

                    default: return null;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static int GetCRUDActionByFeatureType(string action, int inputVal, int featureType)
        {
            try
            {
                switch(action)
                {
                    case CRUDActionConst.ACTION:
                        return featureType == (int)SysFeatureType.Primary || featureType == (int)SysFeatureType.RelatedInfo ||
                                featureType == (int)SysFeatureType.WorkflowActionHistory ? 0 : 
                                    (inputVal > (int)AccessLevel.None ? (int)AccessLevel.Company : (int)AccessLevel.None );

                    case CRUDActionConst.READ:
                        return featureType == (int)SysFeatureType.Primary || featureType == (int)SysFeatureType.RelatedInfo ||
                                featureType == (int)SysFeatureType.WorkflowActionHistory ? inputVal : 0;

                    case CRUDActionConst.CREATE:
                    case CRUDActionConst.UPDATE:
                    case CRUDActionConst.DELETE:
                        return featureType == (int)SysFeatureType.Primary || featureType == (int)SysFeatureType.RelatedInfo ? inputVal : 0;

                    default: return -1;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
