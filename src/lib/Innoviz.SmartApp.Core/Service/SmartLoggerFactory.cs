﻿using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.Service
{
    public class SmartLoggerFactory
    {
        private static ILoggerFactory _Factory = null;
        public static void ConfigureLogger(ILoggerFactory factory)
        {
            // use serilog as log provider
            factory.AddSerilog();
        }
        public static ILoggerFactory LoggerFactory {
            get
            {
                if (_Factory == null)
                {
                    _Factory = new LoggerFactory();
                    ConfigureLogger(_Factory);
                }
                return _Factory;
            }
            set { _Factory = value; }
        }
        public static Microsoft.Extensions.Logging.ILogger CreateLogger<T>() => LoggerFactory.CreateLogger<T>();

    }
}
