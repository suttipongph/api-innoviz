﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Core.Service
{
    public class OnlyInputPropsJsonResolver : DefaultContractResolver
    {
        List<string> InputPropNames = null;
        public OnlyInputPropsJsonResolver(List<string> inputPropNames)
        {
            InputPropNames = inputPropNames;
        }
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            var allProps = base.CreateProperties(type, memberSerialization);
            if (InputPropNames != null && InputPropNames.Count() > 0)
            {
                return allProps.Where(w => InputPropNames.Any(a => a == w.PropertyName)).ToList();
            }
            else
            {
                return allProps;
            }
        }
    }
    public class JsonCustomDateTimeConverter : DateTimeConverterBase
    {
        private List<string> dateFormats;
        public JsonCustomDateTimeConverter()
        {
            dateFormats = null;
        }
        public JsonCustomDateTimeConverter(string dateFormat)
        {
            this.dateFormats = new List<string>() { dateFormat };
        }
        public JsonCustomDateTimeConverter(List<string> dateFormats)
        {
            this.dateFormats = dateFormats;
        }
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if(reader.TokenType == JsonToken.Null)
            {
                return null;
            }
            else
            {
                if(dateFormats != null && dateFormats.Count() > 0)
                {
                    string dateString = (string)reader.Value;
                    DateTime dt;
                    bool success = DateTime.TryParseExact(dateString, dateFormats.ToArray(), CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                    if(success)
                    {
                        return dt;
                    }
                    else
                    {
                        return existingValue;
                    }
                }
                else
                {
                    return existingValue;
                }
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            // implement in case you're serializing it back
        }
    }
}
