﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Newtonsoft.Json;
using Serilog;
using Serilog.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Innoviz.SmartApp.Core.Service
{
    public static class SmartAppUtil
    {
        #region version
        public static string GetApiVersion()
        {
            return "032abbf9bcd5f9801db6d0d9db619cd0b0cb1b7c;24.12.2021T18:00";
        }
        #endregion version

        #region Exception
        #region ThrowException
        public static SmartAppException ThrowException(string topic, string errorMessage)
        {
            SmartAppException smartAppException = new SmartAppException(topic);
            smartAppException.AddData(errorMessage);
            throw AddStackTrace(smartAppException);
        }
        public static SmartAppException ThrowException(string topic, string code, string errorMessage)
        {
            SmartAppException smartAppException = new SmartAppException(topic);
            smartAppException.AddData(code, errorMessage);
            throw AddStackTrace(smartAppException);
        }
        public static SmartAppException ThrowException(string topic, string errorMessage, Exception e)
        {
            SmartAppException smartAppException = new SmartAppException(topic, e);
            smartAppException.AddData(errorMessage);
            throw AddStackTrace(smartAppException);
        }
        public static SmartAppException ThrowException(string topic, string code, string errorMessage, Exception e)
        {
            SmartAppException smartAppException = new SmartAppException(topic, e);
            smartAppException.AddData(code, errorMessage);
            throw AddStackTrace(smartAppException);
        }
        public static Exception ThrowDBException(Exception e, string topic = "ERROR.ERROR")
        {
            if (ConditionService.DBExeption(e))
            {
                if(e != null && !string.IsNullOrEmpty(e.Message) && e.Message.Contains("DELETE"))
                {
                    return ThrowException(topic, "ERROR.00034", e);
                }
            }
            if (ConditionService.Concurrency(e))
            {
                return ThrowException(topic, "ERROR.CONCURRENCY", e);
            }
            return AddStackTrace(e);
        }
        #endregion
        #region AddStackTrace
        public static Exception AddStackTrace(Exception e, SmartAppException expectedError = null)
        {
            if(e == null)
            {
                return null;
            }

            if (IsLinqRuntimeException(e))
            {
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR", e);
                smartAppException.AddData("ERROR.LINQ_RUNTIME_ERROR", ExtractFromString(e.Message, "The LINQ expression", "could not be translated.").ToArray());
                return AddStackTrace(smartAppException);
            }
            else if (ConditionService.Concurrency(e))
            {
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR", e);
                smartAppException.AddData("ERROR.CONCURRENCY");
                return AddStackTrace(smartAppException);
            }

            string stackTraceData = (string)e.Data[ExceptionDataKey.StackTrace];
            string innerStackTrace = (string)e.Data[ExceptionDataKey.InnerStackTrace];

            //no existing Data["InnerStackTrace"]
            if (innerStackTrace == null || innerStackTrace == "")
            {
                innerStackTrace = GetInnerStackTrace(e);
            }

            Log.Logger.Error(e, "<{ExceptionLevel}>", "Exception");
            // existing Data["StackTrace"]
            if (stackTraceData != null && !string.IsNullOrWhiteSpace(e.StackTrace))
            {
                int idx = stackTraceData.IndexOf(e.StackTrace);
                if(idx == -1)
                {
                    stackTraceData += "\n" + e.StackTrace;
                }
            }
            // no Data["StackTrace"]
            else
            {
                if (e.InnerException != null && !string.IsNullOrWhiteSpace(e.StackTrace))
                {
                    if(innerStackTrace.IndexOf(e.StackTrace) == -1) innerStackTrace += "\n" + e.StackTrace;
                    stackTraceData = innerStackTrace;
                }
                else
                {
                    stackTraceData = e.StackTrace;
                }
            }

            AddErrorMessageToList(e, expectedError);
            AddBatchLogReference(e, expectedError);

            e.Data.Remove(ExceptionDataKey.InnerStackTrace);
            e.Data.Add(ExceptionDataKey.InnerStackTrace, innerStackTrace);
            e.Data.Remove(ExceptionDataKey.StackTrace);
            e.Data.Add(ExceptionDataKey.StackTrace, stackTraceData);

            return e;
        }
        private static void AddErrorMessageToList(Exception e, SmartAppException expectedError)
        {
            List<string> msgList = null;
            if (e.Data[ExceptionDataKey.MessageList] != null)
            {
                msgList = (List<string>)e.Data[ExceptionDataKey.MessageList];
            }
            else
            {
                msgList = new List<string>();
            }

            msgList.Clear();

            if (expectedError != null)
            {
                msgList.AddRange(expectedError.MessageList);
            }
            
            if (e.GetType() == typeof(SmartAppException))
            {
                msgList.AddRange(((SmartAppException)e).MessageList);
            }
            else
            {
                var list = GetInnerMessageList(e);
                if(list != null && list.Count > 0)
                {
                    msgList.AddRange(list);
                }
                msgList.Add(SystemStaticData.GetTranslatedMessage(e.Message));
            }

            msgList = msgList.Distinct().ToList();

            e.Data.Remove(ExceptionDataKey.MessageList);
            e.Data.Add(ExceptionDataKey.MessageList, msgList);
        }
        private static void AddBatchLogReference(Exception e, SmartAppException expectedError)
        {
            IDictionary batchLogReferenceData;
            if(e.GetType() == typeof(SmartAppException))
            {
                batchLogReferenceData = ((SmartAppException)e).BatchLogReference;
            }
            else
            {
                batchLogReferenceData = (IDictionary)e.Data[ExceptionDataKey.BatchLogReference];
            }

            if(expectedError != null)
            {
                // merge batchLogError
                if(batchLogReferenceData != null && batchLogReferenceData.Count > 0)
                {
                    foreach (var k in batchLogReferenceData.Keys)
                    {
                        SmartAppExceptionData data = (SmartAppExceptionData)batchLogReferenceData[k];
                        if(data != null && data.BatchLogReference != null)
                        {
                            expectedError.AddData(new BatchLogReference
                            {
                                Key = data.BatchLogReference.Key,
                                Reference = data.BatchLogReference.Reference,
                                ItemValues = data.BatchLogReference.ItemValues
                            }, 
                            data.ErrorCode, data.Parameters);
                        }
                    }
                }

                if(expectedError.BatchLogReference.Count > 0)
                {
                    e.Data.Remove(ExceptionDataKey.BatchLogReference);
                    e.Data.Add(ExceptionDataKey.BatchLogReference, expectedError.BatchLogReference);
                }
            }
            else
            {
                if(batchLogReferenceData != null && batchLogReferenceData.Count > 0)
                {
                    e.Data.Remove(ExceptionDataKey.BatchLogReference);
                    e.Data.Add(ExceptionDataKey.BatchLogReference, batchLogReferenceData);
                }
            }
        }
        private static string GetInnerStackTrace(Exception e)
        {
            try
            {
                string result = null;
                if(e != null && e.InnerException != null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb = GetInnerStackTrace(sb, e.InnerException);
                    result = sb.ToString();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private static StringBuilder GetInnerStackTrace(StringBuilder sb, Exception e)
        {
            try
            {
                if(e == null)
                {
                    return sb;
                }
                else
                {
                    if(e.InnerException != null)
                    {
                        sb = GetInnerStackTrace(sb, e.InnerException);
                    }
                    
                    string stackTraceData = e.Data[ExceptionDataKey.StackTrace]?.ToString();
                    if(!string.IsNullOrWhiteSpace(stackTraceData))
                    {
                        sb.AppendLine(stackTraceData);
                    }

                    if(!string.IsNullOrWhiteSpace(e.StackTrace))
                    {
                        sb.AppendLine(e.StackTrace);
                        Log.Logger.Error(e, "<{ExceptionLevel}>", "InnerException");
                    }
                    return sb;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private static List<string> GetInnerMessageList(Exception e)
        {
            try
            {
                List<string> result = null;
                if (e != null && e.InnerException != null)
                {
                    result = new List<string>();
                    result = GetInnerMessageList(result, e.InnerException);
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private static List<string> GetInnerMessageList(List<string> list, Exception e)
        {
            try
            {
                if(e == null)
                {
                    return list;
                }
                else
                {
                    if (e.InnerException != null)
                    {
                        list = GetInnerMessageList(list, e.InnerException);
                    }

                    List<string> msgList = (List<string>)e.Data[ExceptionDataKey.MessageList];
                    if(msgList != null && msgList.Count > 0)
                    {
                        list.AddRange(msgList);
                    }

                    if (!string.IsNullOrWhiteSpace(e.Message))
                    {
                        if(!(list.Any(a => a == "Error" || a == "ERROR.ERROR") && (e.Message == "Error" || e.Message == "ERROR.ERROR"))) 
                        {
                            list.Add(SystemStaticData.GetTranslatedMessage(e.Message));
                        }
                    }
                    return list;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        private static bool IsLinqRuntimeException(Exception e)
        {
            if (e != null && e.GetType() == typeof(InvalidOperationException) &&
                (e.Message.Contains("The LINQ expression") && e.Message.Contains("could not be translated.") &&
                e.Message.Contains("https://go.microsoft.com/fwlink/?linkid=2101038")))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Encryption
        public static readonly int iterations = 10;

        public static string GenerateEncryptedSaltIV(string encKey)
        {
            try
            {
                byte[] salt = GetSalt();
                byte[] IV, resultByteArr;
                using (Aes aesAlg = Aes.Create())
                {
                    aesAlg.GenerateIV();
                    IV = aesAlg.IV;

                    resultByteArr = new byte[salt.Length + IV.Length];
                    Array.Copy(salt, 0, resultByteArr, 0, salt.Length);
                    Array.Copy(IV, 0, resultByteArr, salt.Length, IV.Length);

                    string resultSaltIV = Convert.ToBase64String(resultByteArr.ToArray());
                    
                    return EncryptString(resultSaltIV, encKey);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }



        public static string EncryptString(string text, string encKey, string encSaltIV = null)
        {
            
            try
            {
                byte[] encrypted, IV;

                byte[] salt;
                
                if(encSaltIV != null)
                {
                    
                    string decryptedSaltIV = DecryptString(encSaltIV, encKey);
                    byte[] saltIVByte = Convert.FromBase64String(decryptedSaltIV);
                    salt = new byte[32];
                    IV = new byte[16];
                    Array.Copy(saltIVByte, 0, salt, 0, salt.Length);
                    Array.Copy(saltIVByte, salt.Length, IV, 0, IV.Length);
                    
                }
                else
                {
                    salt = GetSalt();
                    IV = null;
                }

                byte[] key = CreateKey(encKey, salt);

                using (Aes aesAlg = Aes.Create())
                {
                    aesAlg.Key = key;
                    aesAlg.Padding = PaddingMode.PKCS7;
                    aesAlg.Mode = CipherMode.CBC;

                    if(IV == null)
                    {
                        aesAlg.GenerateIV();
                        IV = aesAlg.IV;
                    }
                    else
                    {
                        aesAlg.IV = IV;
                    }
                    

                    var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using(var swEncrypt = new StreamWriter(csEncrypt))
                            {
                                swEncrypt.Write(text);
                            }
                            encrypted = msEncrypt.ToArray();
                        }
                    }
                }
                byte[] combinedIVSaltCt = new byte[salt.Length + IV.Length + encrypted.Length];
                Array.Copy(salt, 0, combinedIVSaltCt, 0, salt.Length);
                Array.Copy(IV, 0, combinedIVSaltCt, salt.Length, IV.Length);
                Array.Copy(encrypted, 0, combinedIVSaltCt, (salt.Length + IV.Length), encrypted.Length);

                return Convert.ToBase64String(combinedIVSaltCt.ToArray());
            }
            catch(Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public static string DecryptString(string cipherText, string encKey)
        {
            byte[] inputAsByteArray;
            string textResult = null;
            try
            {
                inputAsByteArray = Convert.FromBase64String(cipherText);

                byte[] salt = new byte[32];
                byte[] IV = new byte[16];
                byte[] encodedContent = new byte[inputAsByteArray.Length - salt.Length - IV.Length];

                Array.Copy(inputAsByteArray, 0, salt, 0, salt.Length);
                Array.Copy(inputAsByteArray, salt.Length, IV, 0, IV.Length);
                Array.Copy(inputAsByteArray, (salt.Length + IV.Length), encodedContent, 0, encodedContent.Length);

                byte[] key = CreateKey(encKey, salt);

                using(Aes aesAlg = Aes.Create())
                {
                    aesAlg.Key = key;
                    aesAlg.IV = IV;
                    aesAlg.Mode = CipherMode.CBC;
                    aesAlg.Padding = PaddingMode.PKCS7;

                    ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                    using (var msDecyrpt = new MemoryStream(encodedContent))
                    {
                        using(var csDecrypt = new CryptoStream(msDecyrpt, decryptor, CryptoStreamMode.Read))
                        {
                            using(var srDecrypt = new StreamReader(csDecrypt))
                            {
                                textResult = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }
                return textResult;
            }
            catch(Exception e)
            {
                throw AddStackTrace(e);
            }
        }

        private static byte[] CreateKey(string key, byte[] salt)
        {
            try
            {
                using(var rfc2898DeriveBytes = new Rfc2898DeriveBytes(key, salt, iterations))
                {
                    return rfc2898DeriveBytes.GetBytes(32);
                }
            }
            catch(Exception e)
            {
                throw AddStackTrace(e);
            }
        }

        private static byte[] GetSalt()
        {
            var salt = new byte[32];
            using (var random = new RNGCryptoServiceProvider())
            {
                random.GetNonZeroBytes(salt);
            }
            return salt;
        }
        #endregion
        
        #region Get Label
        public static string GetDropDownLabel(string code)
        {
            return code;
        }
        public static string GetDropDownLabel(string code, string value)
        {
            return code != null ? string.Format("[{0}] {1}", code, value) : null;
        }
        public static string GetAddressLabel(string address1, string address2)
        {
            return string.Format("{0} {1}", address1, address2);
        }
        #endregion
        public static string GetUserNameWithoutDomain(string inputName)
        {
            try
            {
                return inputName == null ? null : inputName.Split("\\").LastOrDefault();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static void LogMessage(LogEventLevel logLevel, string message)
        {
            Log.Logger.Write(logLevel, message);
        }
        public static string GenIDByDateTime(DateTime dateTime)
        {
            return dateTime.ToString("yyyyMMddHHmmssfff");
        }
        public static List<string> ExtractFromString(string body, string start, string end)
        {
            List<string> matched = new List<string>();

            int indexStart = 0;
            int indexEnd = 0;

            bool exit = false;
            while (!exit)
            {
                indexStart = body.IndexOf(start);

                if (indexStart != -1)
                {
                    indexEnd = indexStart + body.Substring(indexStart).IndexOf(end);

                    matched.Add(body.Substring(indexStart + start.Length, indexEnd - indexStart - start.Length));

                    body = body.Substring(indexEnd + end.Length);
                }
                else
                {
                    exit = true;
                }
            }
            return matched;
        }
        public static string GetMachineName()
        {
            return Environment.MachineName;
        }
        #region Interface value helpers
        public static string HandleInterfaceStringValue(string str, int strlen)
        {
            try
            {
                return str == null ? "" : str.Truncate(strlen);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static Int64 GetRecIdStagingTable(int index, int recordCount)
        {
            return GetRecIdStagingTable(index + recordCount);
        }
        public static Int64 GetRecIdStagingTable(int index)
        {
            try
            {
                const int maxLength = 19;
                long timeStamp = new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds();
                string timeStampString = timeStamp.ToString();
                int timeStampLen = timeStampString.Length;
                string indexString = index.ToString();
                int indexLen = indexString.Length;
                int addZeroes = maxLength - (timeStampLen + indexLen);

                string recId = "";

                if (addZeroes < 0)
                {
                    recId = timeStampString.Substring(0, (timeStampLen - (-addZeroes))) + indexString;
                }
                else
                {
                    recId = timeStampString;
                    for (int i = 0; i < addZeroes; i++)
                    {
                        recId += "0";
                    }
                    recId += indexString;
                }
                return Convert.ToInt64(recId);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
