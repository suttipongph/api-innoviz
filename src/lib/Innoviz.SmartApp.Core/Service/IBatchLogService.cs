﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.Service
{
    public interface IBatchLogService
    {
        void LogBatchErrors(Exception e, string instanceHistoryId);
        void LogBatchError(string instanceHistoryId, string message, string reference, string itemValues, string stackTrace);
        void LogBatchError(string instanceHistoryId,
                                string reference,
                                string[] reference_parm,
                                string message,
                                string[] message_parm,
                                string itemValue,
                                string[] itemValue_parm,
                                string stackTrace);
        void LogBatchSuccess(string instanceHistoryId, string message, string reference, string itemValues);
        void LogBatchSuccess(string instanceHistoryId,
                                string reference,
                                string[] reference_parm,
                                string message,
                                string[] message_parm,
                                string itemValue,
                                string[] itemValue_parm);
    }
}
