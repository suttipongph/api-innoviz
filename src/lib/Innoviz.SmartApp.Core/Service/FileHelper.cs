﻿using Innoviz.SmartApp.Core.Attributes;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Innoviz.SmartApp.Core.Service
{
    public class FileHelper
    {
        #region map excel
        public FileImportSingleTypeResult<T> MapExcelSheetToFileImportResult<T>(ExcelWorksheet workSheet, IReadOnlyDictionary<string, string> mapper, bool multipleSheets = false) where T : new()
        {
            try
            {
                FileImportSingleTypeResult<T> result = new FileImportSingleTypeResult<T>();
                List<string> serialized = new List<string>();
                StringBuilder sb = new StringBuilder();
                sb.Append("[");
                var modelResult = new List<T>();

                PropertyInfo[] props = typeof(T).GetProperties();
                var EmptyModel = new T();

                int totalRows = workSheet.Dimension.Rows;
                var start = workSheet.Dimension.Start;
                var end = workSheet.Dimension.End;
                var indexMapper = new Dictionary<int, string>();
                
                for (int r = start.Row; r <= end.Row; r++)
                {
                    if (r == start.Row)
                    {
                        for (int c = start.Column; c <= end.Column; c++)
                        {
                            var headerCol = workSheet.Cells[r, c].Value?.ToString();
                            if(headerCol == null)
                            {
                                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                                ex.AddData("Invalid header value at column position {{0}}.", c.ToString());
                                throw ex;
                            }
                            foreach (KeyValuePair<string, string> entry in mapper)
                            {
                                if (entry.Key.ToUpper() == headerCol.Trim().ToUpper())
                                {
                                    indexMapper.Add(c, entry.Value);
                                }
                            }

                        }
                        r++;
                    }
                    string str = DataType.GUID.GetAttrDesc();
                    var model = new T();

                    for (int c = start.Column; c <= end.Column; c++)
                    {
                        string val = workSheet.Cells[r, c].Value.ToNullString()?.Trim();
                        val = (val == "null" || val == "NULL") ? null : val;
                        object cellValue = workSheet.Cells[r, c].Value;

                        var fieldMapper = indexMapper.FirstOrDefault(m => m.Key == c).Value;

                        if (fieldMapper == null)
                        {
                            SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                            var fieldName = workSheet.Cells[start.Row, c].Value.ToString();
                            string errorParam = multipleSheets ? typeof(T).Name + "." + fieldName : fieldName; 
                            smartAppException.AddData("ERROR.00662", new string[] { errorParam });
                            throw SmartAppUtil.AddStackTrace(smartAppException);
                        }

                        PropertyInfo prop = props.Where(p => p.Name.ToUpper() == fieldMapper.ToUpper()).FirstOrDefault();
                        if (prop == null)
                        {
                            SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                            string errorParam = multipleSheets ? typeof(T).Name + "." + fieldMapper : fieldMapper;
                            smartAppException.AddData("ERROR.00662", new string[] { errorParam });
                            throw SmartAppUtil.AddStackTrace(smartAppException);
                        }

                        bool isNullable = prop.PropertyType.ToString().Contains("System.Nullable");

                        if (prop.PropertyType.ToString().Contains(DataType.GUID.GetAttrDesc()))
                        {
                            bool success = Guid.TryParse(val, out Guid id);
                            if (success)
                            {
                                prop.SetValue(model, id);
                            }
                            else
                            {
                                if (isNullable)
                                {
                                    prop.SetValue(model, null);
                                }
                                else
                                {
                                    prop.SetValue(model, Guid.Empty);
                                }
                            }
                        }
                        else if (prop.PropertyType.ToString().Contains(DataType.DATETIME.GetAttrDesc()))
                        {

                            if (string.IsNullOrEmpty(val))
                            {
                                if (isNullable)
                                {
                                    prop.SetValue(model, null);
                                }
                                else
                                {
                                    prop.SetValue(model, DateTime.MinValue);
                                }
                            }
                            else
                            {
                                bool isSerialDate = Double.TryParse(val, out double valDate);
                                if (isSerialDate)
                                {
                                    DateTime dt = DateTime.FromOADate(valDate);
                                    prop.SetValue(model, dt);
                                }
                                else
                                {
                                    List<string> dateFormats = DataTypeHandler.GetConfigurationValues("AppSetting:ImportExcelDateFormat");
                                    dateFormats.Add(DataTypeHandler.GetConfigurationValue("AppSetting:DateFormat"));
                                    dateFormats.Add(DataTypeHandler.GetConfigurationValue("AppSetting:DateTimeFormat"));
                                    
                                    DateTime dt;
                                    bool fromFormat = DateTime.TryParseExact(val, dateFormats.ToArray(), CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                                    dt = fromFormat ? dt : (DateTime)cellValue;
                                    prop.SetValue(model, dt);
                                }
                            }

                        }
                        //else if (prop.PropertyType.ToString().Contains(DataType.STRING.GetAttrDesc()) &&
                        //            prop.Name.ToUpper().Contains(DataType.DATE.GetAttrCode().ToUpper()))
                        //{
                        //    if (string.IsNullOrEmpty(val))
                        //    {
                        //        prop.SetValue(model, null);
                        //    }
                        //    else
                        //    {
                        //        string dateFormat = DataTypeHandler.GetConfigurationValue("AppSetting:DateFormat");
                        //        if (prop.Name.ToUpper().Contains(DataType.DATETIME.GetAttrCode().ToUpper()))
                        //        {
                        //            dateFormat = DataTypeHandler.GetConfigurationValue("AppSetting:DateTimeFormat");
                        //        }
                        //        bool isSerialDate = Double.TryParse(val, out double valDate);
                        //        if (isSerialDate)
                        //        {
                        //            DateTime dt = DateTime.FromOADate(valDate);
                        //            prop.SetValue(model, dt.ToString(dateFormat));
                        //        }
                        //        else
                        //        {
                        //            DateTime dt = (DateTime)cellValue;
                        //            prop.SetValue(model, dt.ToString(dateFormat));
                        //        }
                        //    }
                        //}
                        else if (prop.PropertyType.ToString().Contains(DataType.INT.GetAttrDesc()))
                        {
                            bool success = Int32.TryParse(val, out int number);
                            if (success)
                            {
                                prop.SetValue(model, number);
                            }
                            else
                            {
                                if (isNullable)
                                {
                                    prop.SetValue(model, null);
                                }
                                else
                                {
                                    prop.SetValue(model, 0);
                                }
                            }
                        }
                        else if (prop.PropertyType.ToString().Contains(DataType.STRING.GetAttrDesc()))
                        {
                            bool substringNotContainsDate = (prop.Name.Length >= 4) 
                                ? prop.Name.ToUpper().Contains("VALIDATE") && prop.Name.Substring(prop.Name.Length - 4, 4).ToUpper() == "DATE"
                                : false;
                            if ((prop.Name.ToUpper().Contains("VALIDATE") && Regex.Matches(prop.Name.ToUpper(), "DATE").Count > 1) ||
                                (!prop.Name.ToUpper().Contains("VALIDATE") && substringNotContainsDate))
                            {
                                bool isSerialDate = Int32.TryParse(val, out int serialDate);
                                if (isSerialDate && prop.Name.ToUpper().Contains("DATE"))
                                {
                                    if (serialDate > 59)
                                        serialDate -= 1; //Excel/Lotus 2/29/1900 bug   
                                    val = new DateTime(1899, 12, 31).AddDays(serialDate).DateToString();
                                    prop.SetValue(model, val);
                                }
                                else
                                {
                                    List<string> dateFormats = DataTypeHandler.GetConfigurationValues("AppSetting:ImportExcelDateFormat");
                                    dateFormats.Add(DataTypeHandler.GetConfigurationValue("AppSetting:DateFormat"));
                                    dateFormats.Add(DataTypeHandler.GetConfigurationValue("AppSetting:DateTimeFormat"));
                                    bool success = false;
                                    DateTime dt;
                                    success = DateTime.TryParseExact(val, dateFormats.ToArray(), CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                                    if (success)
                                    {
                                        if(prop.Name.ToUpper().Contains("DATETIME"))
                                        {
                                            prop.SetValue(model, dt.ToString(dateFormats[dateFormats.Count - 1]));
                                        }
                                        else
                                        {
                                            prop.SetValue(model, dt.ToString(dateFormats[0]));
                                        }
                                    }
                                    else if (!success && val != null)
                                    {
                                        SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                                        smartAppException.AddData("ERROR.00846");
                                        throw SmartAppUtil.AddStackTrace(smartAppException);
                                    }
                                }
                            }
                            else
                            {
                                prop.SetValue(model, val);
                            }
                        }
                        else if (prop.PropertyType.ToString().Contains(DataType.BOOLEAN.GetAttrDesc()))
                        {
                            bool success = Boolean.TryParse(val, out bool fact);
                            if (success)
                            {
                                prop.SetValue(model, fact);
                            }
                            else
                            {
                                bool successParseInt = Int32.TryParse(val, out int number);
                                bool? successFromInt = successParseInt && (number == 0 || number == 1) ? (bool?)Convert.ToBoolean(number) : null;
                                if (isNullable)
                                {
                                    prop.SetValue(model, successFromInt);
                                }
                                else
                                {
                                    if (successFromInt != null)
                                    {
                                        prop.SetValue(model, successFromInt);
                                    }
                                    else
                                    {
                                        prop.SetValue(model, false);
                                    }
                                }
                            }
                        }
                        else if (prop.PropertyType.ToString().Contains(DataType.DECIMAL.GetAttrDesc()))
                        {
                            bool success = Decimal.TryParse(val, NumberStyles.Float, CultureInfo.InvariantCulture, out decimal number);
                            if (success)
                            {
                                prop.SetValue(model, number);
                            }
                            else
                            {
                                if (isNullable)
                                {
                                    prop.SetValue(model, null);
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(val))
                                    {
                                        prop.SetValue(model, Convert.ToDecimal(0));
                                    }
                                    else
                                    {
                                        SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                                        smartAppException.AddData("ERROR.00857");
                                        throw SmartAppUtil.AddStackTrace(smartAppException);
                                    }
                                }
                            }
                        }
                    }
                    var json = JsonConvert.SerializeObject(model);
                    var emptyjson = JsonConvert.SerializeObject(EmptyModel);

                    if (json != emptyjson)
                    {
                        var serializerSettings = new JsonSerializerSettings
                        {
                            ContractResolver = new OnlyInputPropsJsonResolver(indexMapper.Values.ToList()),
                            DateFormatString = DataTypeHandler.GetConfigurationValue("AppSetting:DateTimeFormat")
                        };
                        var serializedInput = JsonConvert.SerializeObject(model, serializerSettings);
                        serialized.Add(serializedInput);
                        modelResult.Add(model);
                    }
                }
                sb.AppendJoin(",", serialized);
                sb.Append("]");

                result.ModelResult = modelResult;
                result.SerializedFromFile = sb.ToString();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region import
        public List<T> MapInputExcelFileToSingleEntityType<T>(FileUpload fileUpload, IReadOnlyDictionary<string, string> mapper) where T : new()
        {
            try
            {
                // validate null
                SmartAppException e = new SmartAppException("ERROR.ERROR");
                if (fileUpload == null || fileUpload.FileInfos == null || fileUpload.FileInfos.Count() == 0)
                {
                    e.AddData("ERROR.00264");
                    throw e;
                }
                // create 
                string dirPath = WebHostDefaults.ContentRootKey.GetConfigurationValue();
                dirPath = string.IsNullOrWhiteSpace(dirPath) ? "." : dirPath;
                dirPath = EnsureDailyPathExisted(Path.Combine(dirPath, "Uploads"));
                fileUpload.FileInfos = fileUpload.FileInfos.Select(s => s.CreateFile(dirPath)).ToList();

                // map excel data to  model
                List<T> result = fileUpload.FileInfos.MapExcelDataToSingleEntityType<T>(mapper);
                // delete excel file
                fileUpload.DeleteFiles();
                // return
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        public string EnsureDailyPathExisted(string rootPath)
        {
            string dateString = DateTime.Now.ToString("ddMMyyyy");
            if (!Directory.Exists(Path.Combine(rootPath, dateString)))
            {
                Directory.CreateDirectory(Path.Combine(rootPath, dateString));
            }
            return Path.Combine(rootPath, dateString);
        }

        #endregion

        #region export
        // colOrder = excel file will contain column specified in colOrder (in-order)
        // if colOrder == null => will check column order from ExcelColOrderAttribute
        // (only properties with ExcelColOrderAttribute will appear in excel file)
        // if both colOrder and ExcelColOrderAttribute were not specified,
        // excel file will contain all properties of T
        public FileInformation ExportDataToExcel<T>(IEnumerable<T> data,
                                                    string fileName,
                                                    string sheetName = null,
                                                    IEnumerable<string> colOrder = null)
            where T : new()
        {
            try
            {
                if (sheetName == null)
                {
                    sheetName = "Sheet1";
                }

                FileInformation file = new FileInformation();
                MemberInfo[] members = null;

                if (colOrder != null && colOrder.Count() > 0)
                {
                    List<PropertyInfo> props = new List<PropertyInfo>();
                    foreach (var col in colOrder)
                    {
                        PropertyInfo prop = typeof(T).GetProperty(col);
                        props.Add(prop);
                    }
                    members = props
                                .Cast<MemberInfo>()
                                .ToArray();
                }
                else
                {
                    var props = typeof(T).GetProperties()
                                        .Select(prop => new
                                        {
                                            Property = prop,
                                            Attribute = (ColumnOrderAttribute)Attribute
                                                            .GetCustomAttribute(prop, typeof(ColumnOrderAttribute))
                                        });
                    if (props.Where(prop => prop.Attribute != null).Count() != props.Count())
                    {
                        if (props.Where(prop => prop.Attribute != null).Count() != 0)
                        {
                            props = props.Where(prop => prop.Attribute != null);
                        }
                    }

                    members = props
                                .OrderBy(a => a.Attribute?.Order ?? -1)
                                .Select(a => a.Property)
                                .Cast<MemberInfo>()
                                .ToArray();
                }

                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.Commercial; // EPPlus 5 and above
                using (var stream = new MemoryStream())
                using (var package = new ExcelPackage(stream))
                {
                    var workSheet = package.Workbook.Worksheets.Add(sheetName);
                    workSheet.Cells.LoadFromCollection(data, true,
                                                        OfficeOpenXml.Table.TableStyles.None,
                                                        BindingFlags.Instance | BindingFlags.Public,
                                                        members);
                    package.Save();

                    stream.Position = 0;
                    var fileBytes = stream.ToArray();
                    string base64Data = Convert.ToBase64String(fileBytes);
                    file.Base64 = base64Data;
                    file.FileName = string.IsNullOrWhiteSpace(fileName) ?
                        DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xlsx" : fileName;
                }
                return file;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        // inputOutputMapping = csv file will contain column specified in inputOutputMapping (in-order)
        // inputOutputMapping => Dictionary<string, string>, 
        //                          key => model (type T) property name, 
        //                          value => output column name
        // if inputOutputMapping == null => will check column order from ExcelColOrderAttribute
        // (only properties with ExcelColOrderAttribute will appear in csv file)
        // if both colOrder and ExcelColOrderAttribute were not specified,
        // csv file will contain all properties of T
        public FileInformation ExportDataToCSV<T>(IEnumerable<T> data,
                                                string fileName,
                                                bool writeHeader = true,
                                                bool doubleQuoteData = false,
                                                string delimiter = ",",
                                                IDictionary<string, string> inputOutputMapping = null) where T : new()
        {
            try
            {
                BuildStringDataParameter parm = new BuildStringDataParameter();
                parm.WriteHeader = writeHeader;
                parm.Delimiter = delimiter;
                parm.FileName = fileName;
                parm.FileExtension = ".csv";
                parm.DoubleQuoteData = doubleQuoteData;
                return ExportDataToFile<T>(data, parm, inputOutputMapping);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public FileInformation ExportDataToTXT<T>(IEnumerable<T> data,
                                        string fileName,
                                        bool writeHeader = true,
                                        bool doubleQuoteData = false,
                                        string delimiter = "|",
                                        IDictionary<string, string> inputOutputMapping = null) where T : new()
        {
            try
            {
                BuildStringDataParameter parm = new BuildStringDataParameter();
                parm.WriteHeader = writeHeader;
                parm.Delimiter = delimiter;
                parm.FileName = fileName;
                parm.FileExtension = ".txt";
                parm.DoubleQuoteData = doubleQuoteData;
                return ExportDataToFile<T>(data, parm, inputOutputMapping);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public FileInformation ExportDataToFile<T>(IEnumerable<T> data,
                                        BuildStringDataParameter buildStringDataParm,
                                        IDictionary<string, string> inputOutputMapping = null) where T : new()
        {
            try
            {
                FileInformation file = new FileInformation();
                inputOutputMapping = GetInputOutputMapping<T>(inputOutputMapping);

                string stringData = BuildStringData<T>(data, inputOutputMapping, buildStringDataParm);


                using (MemoryStream ms = new MemoryStream())
                using (StreamWriter writer = new StreamWriter(ms, Encoding.UTF8))
                {
                    writer.Write(stringData);
                    writer.Flush();

                    ms.Position = 0;
                    var fileBytes = ms.ToArray();
                    string base64Data = Convert.ToBase64String(fileBytes);
                    file.Base64 = base64Data;
                    file.FileName = string.IsNullOrWhiteSpace(buildStringDataParm.FileName) ?
                        DateTime.Now.ToString("yyyyMMddHHmmssfff") + buildStringDataParm.FileExtension : buildStringDataParm.FileName;
                }

                return file;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string BuildStringData<T>(IEnumerable<T> data, IDictionary<string, string> inputOutputMapping, 
                                        BuildStringDataParameter buildStringDataParm) where T : new()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                int keysCount = inputOutputMapping.Keys.Count;

                //column names
                if (buildStringDataParm.WriteHeader)
                {
                    for (int i = 0; i < keysCount; i++)
                    {
                        string key = inputOutputMapping.Keys.ElementAt(i);
                        sb.Append(inputOutputMapping[key]);
                        if (i < keysCount - 1)
                        {
                            sb.Append(buildStringDataParm.Delimiter);
                        }
                    }
                    sb.AppendLine();
                }

                // data
                foreach (var item in data)
                {
                    for (int i = 0; i < keysCount; i++)
                    {
                        string key = inputOutputMapping.Keys.ElementAt(i);
                        PropertyInfo prop = item.GetType().GetProperty(key);
                        string value = item.GetPropStringValue(prop) == null ? "" : item.GetPropStringValue(prop);
                        value = value.Replace("\n", " ").Replace("\r", "");

                        if(buildStringDataParm.DoubleQuoteData)
                        {
                            value = "\"" + value + "\"";
                        }
                        else
                        {
                            // check case data contains delimiter
                            if (value.Contains(buildStringDataParm.Delimiter))
                            {
                                value = "\"" + value + "\"";
                            }
                        }

                        sb.Append(value);
                        if (i < keysCount - 1)
                        {
                            sb.Append(buildStringDataParm.Delimiter);
                        }
                    }
                    sb.AppendLine();
                }
                return sb.ToString();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private IDictionary<string, string> GetInputOutputMapping<T>(IDictionary<string, string> inputOutputMapping) where T : new()
        {
            try
            {
                if (inputOutputMapping == null || inputOutputMapping.Count == 0)
                {
                    inputOutputMapping = new Dictionary<string, string>();
                    var props = typeof(T).GetProperties()
                                        .Select(prop => new
                                        {
                                            Property = prop,
                                            OrderAttribute = (ColumnOrderAttribute)Attribute
                                                            .GetCustomAttribute(prop, typeof(ColumnOrderAttribute)),
                                            DescriptionAttribute = prop.GetCustomAttribute<DescriptionAttribute>()
                                        });
                    if (props.Where(prop => prop.OrderAttribute != null).Count() != props.Count())
                    {
                        if (props.Where(prop => prop.OrderAttribute != null).Count() != 0)
                        {
                            props = props.Where(prop => prop.OrderAttribute != null);
                        }

                    }

                    inputOutputMapping = props.OrderBy(o => o.OrderAttribute?.Order ?? -1)
                                              .ToDictionary(k => k.Property.Name,
                                                            v => v.DescriptionAttribute?.Description ?? v.Property.Name);
                }
                return inputOutputMapping;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region unit test
        /* 
         * T => { Table1: [ { Prop1: pk1, Prop2: val1 }, { Prop1: pk2, Prop2: val2 } ], Table2: ... }
         * returns JSON string
        */
        public string ReadMultiTableExcelTestDataToJson<T>(string excelFilePath, string outputFilePath)
        {
            try
            {
                PropertyInfo[] rootProps = typeof(T).GetProperties();
                FileInfo file = new FileInfo(excelFilePath);
                List<string> objectStrings = new List<string>();
                StringBuilder sb = new StringBuilder();
                sb.Append("{");
                #region read excel sheets to json string
                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.Commercial;
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    foreach (var rootProp in rootProps)
                    {
                        ExcelWorksheet workSheet = null;
                        if ((rootProp.PropertyType.IsGenericType && rootProp.PropertyType.GetGenericTypeDefinition() == typeof(List<>)) ||
                            (rootProp.PropertyType.IsGenericType && typeof(T).BaseType == typeof(SmartAppDbContext)))
                        {
                            Type itemType = rootProp.PropertyType.GetGenericArguments()[0];
                            string sheetName = (rootProp.PropertyType.IsGenericType && typeof(T).BaseType == typeof(SmartAppDbContext)) ? itemType.Name : rootProp.Name;
                            workSheet = package.Workbook.Worksheets.FirstOrDefault(f => f.Name == sheetName);

                            if(workSheet != null)
                            {
                                string jsonString = GetJsonStringFromExcelSheet(workSheet, itemType, true);
                                if (!string.IsNullOrWhiteSpace(jsonString))
                                {
                                    objectStrings.Add("\"" + sheetName + "\":" + jsonString);
                                }
                            }
                        }
                        else
                        {
                            // type single object
                            Type itemType = rootProp.PropertyType;
                            workSheet = package.Workbook.Worksheets.FirstOrDefault(f => f.Name == itemType.Name);

                            if(workSheet != null)
                            {
                                string jsonString = GetJsonStringFromExcelSheet(workSheet, itemType, true);
                                string singleObj = JsonConvert.SerializeObject(JArray.Parse(jsonString).FirstOrDefault());
                                if (!string.IsNullOrWhiteSpace(jsonString))
                                {
                                    objectStrings.Add("\"" + rootProp.Name + "\":" + singleObj);
                                }
                            }
                        }

                    }
                }
                #endregion

                sb.AppendJoin(",", objectStrings);
                sb.Append("}");
                var result = JToken.Parse(sb.ToString()).ToString(Formatting.Indented);

                #region write to json file
                using (StreamWriter writer = System.IO.File.CreateText(outputFilePath))
                {
                    writer.WriteLine(result);
                }
                #endregion
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string ReadTestDataParameterToJson<T>(string excelFilePath, string outputFilePath)
        {
            try
            {
                string result = null;
                FileInfo file = new FileInfo(excelFilePath);
                #region read excel sheet to json string
                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.Commercial;
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets.First(f => f.Name == UnitTestConst.Sheet_PARAMETER);
                    // type single object
                    Type itemType = typeof(T);
                    string jsonString = GetJsonStringFromExcelSheet(workSheet, itemType, false);
                    string singleObj = JsonConvert.SerializeObject(JArray.Parse(jsonString).FirstOrDefault());
                    if (!string.IsNullOrWhiteSpace(jsonString))
                    {
                        result = singleObj;
                    }
                }
                #endregion

                result = JToken.Parse(result).ToString(Formatting.Indented);

                #region write to json file
                using (StreamWriter writer = System.IO.File.CreateText(outputFilePath))
                {
                    writer.WriteLine(result);
                }
                #endregion
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GetJsonStringFromExcelSheet(ExcelWorksheet workSheet, Type entityType, bool multipleSheets)
        {
            try
            {
                var mi = typeof(FileHelper).GetMethod("MapExcelSheetToFileImportResult");
                var mapExcelSheetRef = mi.MakeGenericMethod(entityType);
                var entityProps = entityType.GetProperties();
                var modelFromExcel = mapExcelSheetRef.Invoke(this, new object[] { workSheet, entityProps.GetEntityMapper(), multipleSheets });

                string result = modelFromExcel.GetType().GetProperty("SerializedFromFile").GetValue(modelFromExcel)?.ToString();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
