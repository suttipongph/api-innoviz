﻿using Innoviz.SmartApp.Core.ViewModelHandler;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Core.Service
{
    public static class ConditionService
    {
        public static bool IsNullOrEmpty(string str)
        {
            return str == null || str == string.Empty;
        }
        public static bool IsNotNullAndNotEmpty(string str)
        {
            return str != null && str != string.Empty && str != "null";
        }
        public static bool IsNullOrEmptyGUID(Guid? guid)
        {
            return guid == null || guid == new Guid();
        }
        public static bool IsNotNullAndNotEmptyGUID(Guid? guid)
        {
            return guid != null && guid != new Guid();
        }
        public static bool IsNotNull(int? i)
        {
            return i != null;
        }
        public static bool IsNotNull(object obj)
        {
            return obj != null;
        }
        public static bool IsNotNull(decimal? i)
        {
            return i != null;
        }
        public static bool IsNotZero(int i)
        {
            return i != 0;
        }
        public static bool IsNotZero(decimal i)
        {
            return i != 0;
        }
        public static bool IsEqualZero(decimal i)
        {
            return i == 0;
        }
        public static bool IsUniqeOrZero(int i)
        {
            return i == 1 || i == 0;
        }
        public static bool IsEnumDefined<T>(int value)
        {
            return Enum.IsDefined(typeof(T), value);
        }
        public static T ToEnum<T>(this string enumString)
        {
            return (T)Enum.Parse(typeof(T), enumString);
        }
        public static List<ListOfEnum> IsGreaterThanEqual<T>(int value)
        {
            List<ListOfEnum> listOfEnum = EnumToList<T>();
            return listOfEnum.Where(o => o.code >= value).ToList();
        }
        public static List<ListOfEnum> IsNotEqual<T>(int[] values)
        {
            List<ListOfEnum> listOfEnum = EnumToList<T>();
            return listOfEnum.Where(o => !values.Contains(o.code)).ToList();
        }

        public static List<ListOfEnum> EnumToList<T>(bool lowerCase = false)
        {
            List<ListOfEnum> listOfEnum = new List<ListOfEnum>();
            var NameOfEnum = Enum.GetNames(typeof(T));
            foreach (var item in NameOfEnum)
            {
                object _value = Enum.Parse(typeof(T), item);
                int idx = Convert.ToInt32(_value);
                listOfEnum.Add(new ListOfEnum
                {
                    status = (lowerCase) ? item.ToLower().Replace("_", "") : item,
                    code = idx,
                });
            }
            return listOfEnum;
        }
        public static DateTime? SelectedHasValue(this bool id1, DateTime? id2, DateTime id3)
        {
            return (id2 == null) ? id2 : (id1 == false) ? id3 : id2;
        }
        public static Guid? SelectedHasValue(this Guid? id1, string id2)
        {
            return (id2 == null) ? id1 : new Guid(id2);
        }
        public static Guid SelectedHasValue(this Guid id1, string id2)
        {
            return (id2 == null) ? id1 : new Guid(id2);
        }
        public static Guid? SelectedHasValue(this Guid? id1, Guid? id2)
        {
            return (id2 == null) ? id1 : id2;
        }
        public static void AddTo<T>(this List<T> self, List<T> destination)
        {
            if (destination != null) self.AddRange(destination);
        }
        public static bool IsFromDateGreaterThanToDate(string fromDate, string toDate)
        {
            if (IsNotNullAndNotEmpty(fromDate) && IsNotNullAndNotEmpty(toDate))
            {
                DateTime fromDateTypeDate = fromDate.StringToDate();
                DateTime toDateTypeDate = toDate.StringToDate();
                return IsFromDateGreaterThanToDate(fromDateTypeDate, toDateTypeDate);
            }
            else
            {
                return false;
            }
        }
        public static bool IsFromDateGreaterThanToDate(DateTime? fromDate, DateTime? toDate)
        {
            if (IsNotNull(fromDate) && IsNotNull(toDate))
            {
                return IsFromDateGreaterThanToDate(fromDate.Value, toDate.Value);
            }
            else
            {
                return false;
            }
        }
        public static bool IsFromDateGreaterThanToDate(DateTime fromDate, DateTime toDate)
        {
            return fromDate > toDate;
        }
        #region Exception

        public static bool DBExeption(Exception e)
        {
            if (typeof(Microsoft.Data.SqlClient.SqlException) == e.GetType())
            {
                //Foreign Key constraint violations while attempting a delete
                if (((Microsoft.Data.SqlClient.SqlException)e).Number == 547) 
                {
                    return true;
                }
            }
            return false;
        }
        public static bool MODELNOTNULL(Exception e)
        {
            if (typeof(Microsoft.Data.SqlClient.SqlException) == e.GetType())
            {
                if (((Microsoft.Data.SqlClient.SqlException)e).Number == 515)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool Concurrency(Exception e)
        {
            if (typeof(Microsoft.Data.SqlClient.SqlException) == e.GetType())
            {
                if (((Microsoft.Data.SqlClient.SqlException)e).Number == 1205)
                {
                    return true;
                }
            }
            else if(e.GetType() == typeof(DbUpdateException) || e.GetType() == typeof(DbUpdateConcurrencyException))
            {
                if(e.Message.Contains("Database operation expected to affect") && 
                    e.Message.Contains("Data may have been modified or deleted since entities were loaded. See http://go.microsoft.com/fwlink/?LinkId=527962 for information on understanding and handling optimistic concurrency exceptions."))
                {
                    return true;
                }
            }
            return false;
        }

        public class ListOfEnum
        {
            public string status { get; set; }
            public int code { get; set; }
        }
        #endregion
    }
}
