﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.Service
{
    public interface ISysAccessLevelService
    {
        Guid? GetOwnerBusinessUnitGUIDByUserName(string userName, string companyGUID);
        IEnumerable<Guid?> GetParentChildBusinessUnitGUIDsByUserName(string userName, string companyGUID);
        IEnumerable<Guid?> GetParentChildBusinessUnitGUIDsByBusinessUnit(string businessUnitGuid, string companyGuid);
        Guid GetRootBusinessUnitGUIDByCompany(string companyGuid);
        T AssignOwnerBU<T>(T item);
    }
}
