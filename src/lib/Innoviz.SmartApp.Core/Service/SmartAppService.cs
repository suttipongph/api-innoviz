﻿using EFCore.BulkExtensions;

using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Innoviz.SmartApp.Core.Service
{

    public class SmartAppService : ISmartAppService
    {
        protected readonly SmartAppDbContext db;
        protected readonly UnitOfWork UnitOfWork;

        private readonly SystemParameter SysParm;
        protected readonly ISysTransactionLogService transactionLogService;
        protected readonly IBatchLogService batchLogService;

        protected readonly List<DbContext> dbContexts;
        #region ctor
        public SmartAppService(SmartAppDbContext context) {
            db = context;
            UnitOfWork = new UnitOfWork(db);

            SysParm = db.GetSystemParameter();
        }
        public SmartAppService() {
            db = null;
            UnitOfWork = null;
        }
        public SmartAppService(List<DbContext> contexts) {
            var smartAppDbContext = contexts.Where(item => item.GetType().IsSubclassOf(typeof(SmartAppDbContext))).FirstOrDefault();
            if(smartAppDbContext == null) {
                throw new NullReferenceException();
            }
            else {
                db = (SmartAppDbContext)smartAppDbContext;
                UnitOfWork = new UnitOfWork(db);
            }
            dbContexts = contexts;
            SysParm = db.GetSystemParameter();
        }
        
        public SmartAppService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : this(context)
        {
            this.transactionLogService = transactionLogService;
        }

        public SmartAppService(List<DbContext> contexts, ISysTransactionLogService transactionLogService) : this(contexts)
        {
            this.transactionLogService = transactionLogService;
        }
        public SmartAppService(SmartAppDbContext context, SystemParameter systemParameter) : this(context)
        {
            // override
            this.SysParm = systemParameter;
        }

        public SmartAppService(List<DbContext> contexts, SystemParameter systemParameter) : this(contexts)
        {
            // override
            this.SysParm = systemParameter;
        }
        public SmartAppService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : this(context, systemParameter)
        {
            this.transactionLogService = transactionLogService;
        }

        public SmartAppService(List<DbContext> contexts, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : this(contexts, systemParameter)
        {
            this.transactionLogService = transactionLogService;
        }
        public SmartAppService(List<DbContext> contexts, ISysTransactionLogService transactionLogService, IBatchLogService batchLogService) : this(contexts, transactionLogService)
        {
            this.batchLogService = batchLogService;
        }
        public SmartAppService(List<DbContext> contexts, SystemParameter systemParameter, IBatchLogService batchLogService) : this(contexts, systemParameter)
        {
            this.batchLogService = batchLogService;
        }
        public SmartAppService(List<DbContext> contexts, SystemParameter systemParameter, ISysTransactionLogService transactionLogService, IBatchLogService batchLogService) : this(contexts, systemParameter, transactionLogService)
        {
            this.batchLogService = batchLogService;
        }
        #endregion ctor

        public T GetOriginalValues<T> (T item)
        {
            try
            {
                return (T)db.Entry(item).OriginalValues.ToObject();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Bulk
        public void BulkInsert<T>(List<T> entities, bool newGUID = false, bool checkDuplicate = true, BulkConfig bulkConfig = null, Action<decimal> progress = null) where T: class
        {
            try
            {
                BulkInsert<T>(db, entities, newGUID, checkDuplicate, bulkConfig, progress);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void BulkUpdate<T>(List<T> entities, bool checkDuplicate = true, BulkConfig bulkConfig = null, Action<decimal> progress = null) where T: class
        {
            try
            {
                BulkUpdate<T>(db, entities, checkDuplicate, bulkConfig, progress);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void BulkDelete<T>(List<T> entities, BulkConfig bulkConfig = null, Action<decimal> progress = null) where T: class
        {
            try
            {
                BulkDelete<T>(db, entities, bulkConfig, progress);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void BulkInsert<T>(DbContext dbContext, List<T> entities, bool newGUID = false, bool checkDuplicate = true, BulkConfig bulkConfig = null, Action<decimal> progress = null) where T: class
        {
            try
            {
                if(entities != null && entities.Count > 0)
                {
                    SetRelatedOwner(dbContext, entities);
                    AssignOwnerBU(entities, newGUID);
                    ValidateAccessRightLevel(dbContext, entities);
                    if(checkDuplicate)
                    {
                        CheckDuplicate(dbContext, entities);
                    }
                    AssignSystemFields(entities, false);
                    if (bulkConfig != null)
                    {
                        bulkConfig.SqlBulkCopyOptions = Microsoft.Data.SqlClient.SqlBulkCopyOptions.CheckConstraints;
                    }
                    else
                    {
                        bulkConfig = new BulkConfig { SqlBulkCopyOptions = Microsoft.Data.SqlClient.SqlBulkCopyOptions.CheckConstraints };
                    }
                    dbContext.BulkInsert<T>(entities, bulkConfig, progress);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void BulkUpdate<T>(DbContext dbContext, List<T> entities, bool checkDuplicate = true, BulkConfig bulkConfig = null, Action<decimal> progress = null) where T: class
        {
            try
            {
                if (entities != null && entities.Count > 0)
                {
                    string modelName = typeof(T).Name;
                    bool isSystemTable = modelName == SystemTableName.NumberSeqTable || modelName.StartsWith(SystemTableName.StartsWithBatch) ||
                        modelName.StartsWith(SystemTableName.StartsWithMigration) || modelName.StartsWith(SystemTableName.StartsWithStaging_) ||
                        modelName.StartsWith(SystemTableName.StartsWithSys);

                    if(!isSystemTable)
                    {
                        ValidateAccessRightLevel(dbContext, entities);
                    }
                    if (checkDuplicate)
                    {
                        CheckDuplicate(dbContext, entities);
                    }
                    AssignSystemFields(entities, true);
                    if (bulkConfig != null)
                    {
                        bulkConfig.DoNotUpdateIfTimeStampChanged = true;
                        bulkConfig.CalculateStats = true;
                    }
                    else
                    {
                        bulkConfig = new BulkConfig { DoNotUpdateIfTimeStampChanged = true, CalculateStats = true };
                    }
                    dbContext.BulkUpdate<T>(entities, bulkConfig, progress);
                    
                    //if(bulkConfig.TimeStampInfo?.NumberOfSkippedForUpdate > 0)
                    if(bulkConfig.StatsInfo?.StatsNumberUpdated != entities.Count)
                    {
                        SmartAppException ex = new SmartAppException("ERROR.ERROR");
                        ex.AddData("ERROR.CONCURRENCY");
                        throw ex;
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void BulkDelete<T>(DbContext dbContext, List<T> entities, BulkConfig bulkConfig = null, Action<decimal> progress = null) where T: class
        {
            try
            {
                if(entities != null && entities.Count > 0)
                {
                    ValidateAccessRightLevel(dbContext, entities);
                    dbContext.BulkDelete<T>(entities, bulkConfig, progress);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Bulk
        private void ValidateAccessRightLevel<T>(DbContext dbContext, List<T> entities)
        {
            try
            {
                if((SysParm.AccessLevel == null || (SysParm.AccessLevel != null && !SysParm.AccessLevel.SkipCheck)) && 
                    (dbContext.GetType() == typeof(SmartAppDbContext) || 
                    dbContext.GetType().IsSubclassOf(typeof(SmartAppDbContext))))
                {
                    SysAccessLevelHelper.CheckOwnerBUByAccessLevel<T>(entities, SysParm.AccessLevel, SysParm.UserName);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void CheckDuplicate<T>(DbContext context, IEnumerable<T> entities) where T: class
        {
            try
            {
                if (context.GetType() == typeof(SmartAppDbContext) ||
                    context.GetType().IsSubclassOf(typeof(SmartAppDbContext)))
                {
                    var baseRepo = new BaseRepository<T>(context as SmartAppDbContext);
                    baseRepo.CheckDuplicate(entities);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region AssignOwnerBU
        private void SetRelatedOwner<T>(DbContext context, IEnumerable<T> entities) where T : class
        {
            try
            {
                if (context.GetType() == typeof(SmartAppDbContext) ||
                    context.GetType().IsSubclassOf(typeof(SmartAppDbContext)))
                {
                    var baseRepo = new BaseRepository<T>(context as SmartAppDbContext);
                    baseRepo.AssignRelatedOwner(entities);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void AssignOwnerBU<T>(T item, ISysAccessLevelService accessLevelService) where T : class
        {
            try
            {
                var HasCompany = typeof(T).BaseType.GetProperty(TextConstants.CompanyGUID) != null ? true : false;
                var HasOwner = typeof(T).BaseType.GetProperty(TextConstants.Owner) != null;
                var HasOwnerBusinessUnitGUID = typeof(T).BaseType.GetProperty(TextConstants.OwnerBusinessUnitGUID) != null;
                if (HasCompany && (HasOwner && HasOwnerBusinessUnitGUID))
                {
                    PropertyInfo ownerBUPropInfo = item.GetType().GetProperty(TextConstants.OwnerBusinessUnitGUID);
                    PropertyInfo ownerPropInfo = item.GetType().GetProperty(TextConstants.Owner);
                    var owner = ownerPropInfo.GetValue(item)?.ToString();
                    var ownerBU = ownerBUPropInfo.GetValue(item)?.ToString();

                    if (string.IsNullOrEmpty(owner) && string.IsNullOrEmpty(SysParm.UserName))
                    {
                        throw new Exception("Value: Owner must be specified.");
                    }
                    else
                    {
                        if(string.IsNullOrEmpty(owner))
                        {
                            ownerPropInfo.SetValue(item, SysParm.UserName);
                        }

                        if(string.IsNullOrEmpty(ownerBU))
                        {
                            var sysOwnerBU = accessLevelService.GetOwnerBusinessUnitGUIDByUserName(owner, GetCurrentCompany());
                            if (ownerBUPropInfo.PropertyType == typeof(string))
                            {
                                ownerBUPropInfo.SetValue(item, sysOwnerBU.GuidNullToString());
                            }
                            else
                            {
                                ownerBUPropInfo.SetValue(item, sysOwnerBU);
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void AssignOwnerBU<T>(IList<T> entities, bool newGUID = false)
        {
            try
            {
                if(entities != null && entities.Count() > 0)
                {
                    var HasCompany = typeof(T).BaseType.GetProperty(TextConstants.CompanyGUID) != null ? true : false;
                    var HasOwner = typeof(T).BaseType.GetProperty(TextConstants.Owner) != null;
                    var HasOwnerBusinessUnitGUID = typeof(T).BaseType.GetProperty(TextConstants.OwnerBusinessUnitGUID) != null;
                    
                    if (newGUID || (HasCompany && (HasOwner && HasOwnerBusinessUnitGUID)))
                    {
                        var instance = Activator.CreateInstance(typeof(T));
                        var primaryProp = db.Entry(instance).Metadata.FindPrimaryKey().Properties.FirstOrDefault().PropertyInfo;

                        PropertyInfo ownerBUPropInfo = typeof(T).GetProperty(TextConstants.OwnerBusinessUnitGUID);
                        PropertyInfo ownerPropInfo = typeof(T).GetProperty(TextConstants.Owner);
                        
                        entities = entities.Select(s =>
                        {
                            if(HasCompany && (HasOwner && HasOwnerBusinessUnitGUID))
                            {
                                if(ownerPropInfo.GetValue(s) == null && ownerBUPropInfo.GetValue(s) == null)
                                {
                                    ownerPropInfo.SetValue(s, SysParm.UserName);
                                    ownerBUPropInfo.SetValue(s, SysParm.BusinessUnitGUID.StringToGuidNull());
                                }
                            }
                            if(newGUID)
                            {
                                primaryProp.SetValue(s, Guid.NewGuid());
                            }
                            return s;
                        })
                        .ToList();
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        private void AssignSystemFields<T>(IList<T> entities, bool isUpdateMode)
        {
            try
            {
                if (entities != null && entities.Count() > 0)
                {
                    
                    PropertyInfo createdBy = typeof(T).GetProperty(TextConstants.CreatedBy);
                    PropertyInfo createdDateTime = typeof(T).GetProperty(TextConstants.CreatedDateTime);
                    PropertyInfo modifiedBy = typeof(T).GetProperty(TextConstants.ModifiedBy);
                    PropertyInfo modifiedDateTime = typeof(T).GetProperty(TextConstants.ModifiedDateTime);

                    if(createdBy != null && createdDateTime != null && modifiedBy != null && modifiedDateTime != null)
                    {
                        entities = entities.Select(s =>
                        {
                            if (!isUpdateMode)
                            {
                                createdBy.SetValue(s, SysParm.UserName);
                                createdDateTime.SetValue(s, DateTime.Now);
                            }
                            modifiedBy.SetValue(s, SysParm.UserName);
                            modifiedDateTime.SetValue(s, DateTime.Now);
                            return s;
                        }).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region GetSystemParameter
        public AccessLevelParm GetAccessLevel()
        {
            return SysParm?.AccessLevel;
        }
        public string GetUserName()
        {
            if (SysParm == null || SysParm.UserName == null)
            {
                throw new NullReferenceException("System Parameter [UserName] cannot be null.");
            }
            else
            {
                return SysParm.UserName;
            }
        }
        public string GetUserId()
        {
            if (SysParm == null || SysParm.UserId == null)
            {
                throw new NullReferenceException("System Parameter [UserId] cannot be null.");
            }
            else
            {
                return SysParm.UserId;
            }
        }
        public string GetCurrentCompany()
        {
            return SysParm?.CompanyGUID;
        }
        public string GetCurrentBranch()
        {
            return SysParm?.BranchGUID;
        }
        public string GetRequestId()
        {
            return SysParm?.RequestId;
        }
        public string GetBearerToken()
        {
            return SysParm?.BearerToken;
        }
        public string GetBatchInstanceHistoryId()
        {
            return SysParm?.InstanceHistoryId;
        }
        public int GetSiteLogin()
        {
            return (SysParm != null) ? SysParm.SiteLogin : -1;
        }
        public SystemParameter GetSystemParameter()
        {
            return SysParm;
        }
        public string GetCurrentOwnerBusinessUnitGUID()
        {
            return SysParm?.BusinessUnitGUID;
        }
        #endregion

        #region BatchLog
        public void LogBatchErrors(Exception ex)
        {
            try
            {
                if (batchLogService != null)
                {
                    batchLogService.LogBatchErrors(ex, SysParm.InstanceHistoryId);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void LogBatchError(string message, string reference, string itemValues, string stackTrace)
        {
            try
            {
                if (batchLogService != null)
                {
                    batchLogService.LogBatchError(SysParm.InstanceHistoryId, message, reference, itemValues, stackTrace);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void LogBatchError(string reference,
                                string[] reference_parm,
                                string message,
                                string[] message_parm,
                                string itemValue,
                                string[] itemValue_parm,
                                string stackTrace)
        {
            try
            {
                if(batchLogService != null)
                {
                    batchLogService.LogBatchError(SysParm.InstanceHistoryId,
                                                    reference, reference_parm,
                                                    message, message_parm,
                                                    itemValue, itemValue_parm, stackTrace);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void LogBatchSuccess(string message, string reference, string itemValues)
        {
            try
            {
                if (batchLogService != null)
                {
                    batchLogService.LogBatchSuccess(SysParm.InstanceHistoryId, message, reference, itemValues);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void LogBatchSuccess(string reference,
                                string[] reference_parm,
                                string message,
                                string[] message_parm,
                                string itemValue,
                                string[] itemValue_parm)
        {
            try
            {
                if (batchLogService != null)
                {
                    batchLogService.LogBatchSuccess(SysParm.InstanceHistoryId, 
                                                    reference, reference_parm, 
                                                    message, message_parm, 
                                                    itemValue, itemValue_parm);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region SysTransactionLog
        public void LogTransactionUpdate<T>(T orig, T data) where T : class
        {

        }
        public void LogTransactionCreate<T>(T data) where T : class
        {

        }
        public void LogTransactionDelete<T>(T data) where T : class
        {

        }
        public void LogTransactionDelete<T>(IEnumerable<T> data) where T : class
        {

        }
        #endregion SysTransactionLog
    }


}
