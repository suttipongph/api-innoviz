﻿using IdentityModel.Client;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Innoviz.SmartApp.Core.Service
{
    public class HttpHelper
    {
        private SystemParameter sysParm;
        public HttpHelper() 
        { 
            sysParm = null; 
        }
        public HttpHelper(SystemParameter parm) 
        {
            sysParm = parm;
        }
        
        // requestScopes: "scope1 scope2 scope3"
        public async Task<string> GetClientCredentialsToken(string authServer, string clientId, string clientSecret, string requestScopes)
        {
            try
            {
                DiscoveryDocumentResponse discoRes;
                using (var httpClient = new HttpClient())
                {
                    var disco = await httpClient.GetDiscoveryDocumentAsync(authServer);
                    if (disco.IsError)
                    {
                        SmartAppException ex = new SmartAppException("Error loading Dicovery Document.");
                        ex.AddData(disco.Error);
                        throw SmartAppUtil.AddStackTrace(ex);
                    }
                    else
                    {
                        discoRes = disco;
                    }
                }

                TokenResponse tokenResponse;
                using (var httpClient = new HttpClient())
                {
                    var response = await httpClient.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
                    {
                        Address = discoRes.TokenEndpoint,
                        ClientId = clientId,
                        ClientSecret = clientSecret,
                        Scope = requestScopes,

                    });
                    if (response.IsError)
                    {
                        SmartAppException ex = new SmartAppException("Error getting token.");
                        ex.AddData(response.Error);
                        throw SmartAppUtil.AddStackTrace(ex);
                    }
                    else
                    {
                        tokenResponse = response;
                    }
                }
                return tokenResponse?.AccessToken;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string BuildQueryString(string url, string[,] queries = null, int port = -1)
        {
            try
            {
                string uriBuilder = url;
                UriBuilder builder = new UriBuilder(uriBuilder);
                builder.Port = port;
                var query = HttpUtility.ParseQueryString(builder.Query);
                for (int i = 0; i < queries.GetLength(0); i++)
                {
                    query[queries[i, 0]] = queries[i, 1];
                }
                builder.Query = query.ToString();
                uriBuilder = builder.ToString();
                return uriBuilder;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void AddCustomHttpHeaders(HttpClient httpClient, Dictionary<string, string> customHeaders)
        {
            try
            {
                if (httpClient != null)
                {
                    if (customHeaders != null && customHeaders.Count() > 0)
                    {
                        foreach (var item in customHeaders.Keys)
                        {
                            httpClient.DefaultRequestHeaders.Add(item, customHeaders[item]);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<T> CallHttpGet<T>(string url, AuthenticationHeaderValue authHeader = null, Dictionary<string, string> customHeaders = null)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {

                    if (authHeader != null)
                    {
                        httpClient.DefaultRequestHeaders.Authorization = authHeader;
                    }
                    AddCustomHttpHeaders(httpClient, customHeaders);

                    var response = await httpClient.GetAsync(url);

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("ERROR.ERROR", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException(errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        T result =
                            JsonConvert.DeserializeObject<T>(jsonContent);
                        return result;
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<TResponse> CallHttpPost<TParameter, TResponse>(TParameter model, string url, AuthenticationHeaderValue authHeader = null, Dictionary<string, string> customHeaders = null)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {

                    if (authHeader != null)
                    {
                        httpClient.DefaultRequestHeaders.Authorization = authHeader;
                    }
                    AddCustomHttpHeaders(httpClient, customHeaders);

                    var jsonObject = JsonConvert.SerializeObject(model, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    });

                    var response = await httpClient.PostAsync(url, new StringContent(jsonObject,
                                                                Encoding.UTF8, "application/json"));

                    dynamic content = await response.Content.ReadAsAsync<dynamic>();
                    if (!response.IsSuccessStatusCode)
                    {
                        int responseStatusCode = (int)response.StatusCode;
                        if (responseStatusCode == 400 || responseStatusCode == 500)
                        {
                            // failed 
                            var errResp = ((JObject)content).ToObject<WebApiErrorResponseMessage>();
                            SmartAppException ex = new SmartAppException("ERROR.ERROR", errResp);
                            throw ex;
                        }
                        else
                        {
                            // status != 400 or 500
                            string errorMsg = "Request responded with Status: " + responseStatusCode + "(" + response.ReasonPhrase + ").";
                            SmartAppException ex = new SmartAppException(errorMsg);
                            throw ex;
                        }
                    }
                    else
                    {
                        // success
                        string jsonContent = JsonConvert.SerializeObject(content);
                        TResponse result =
                            JsonConvert.DeserializeObject<TResponse>(jsonContent);
                        return result;
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        // url => e.g. Workflow/GetTasks
        public async Task<TResponse> CallConnectApi<TParameter, TResponse>(TParameter model, string url)
        {
            try
            {
                string authServer = DataTypeHandler.GetConfigurationValue("JWT:Authority");
                string clientId = DataTypeHandler.GetConfigurationValue("BusinessApi:Client");
                string clientSecret = DataTypeHandler.GetConfigurationValue("BusinessApi:ClientSecret");
                string requestScopes = DataTypeHandler.GetConfigurationValue("ConnectApi:Scope");

                string token = await GetClientCredentialsToken(authServer, clientId, clientSecret, requestScopes);
                var authHeader = new AuthenticationHeaderValue("Bearer", token);
                var stdHeader = GetSmartAppStandardCustomHeaders();
                url = DataTypeHandler.GetConfigurationValue("ConnectApi:BaseUrl") + url;

                var result = await CallHttpPost<TParameter, TResponse>(model, url, authHeader, stdHeader);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private Dictionary<string, string> GetSmartAppStandardCustomHeaders()
        {
            try
            {
                Dictionary<string, string> result = new Dictionary<string, string>();
                if(!string.IsNullOrWhiteSpace(sysParm.BranchGUID))
                {
                    result.Add(HttpHeaderKeys.BRANCH, sysParm.BranchGUID);
                }
                if(!string.IsNullOrWhiteSpace(sysParm.CompanyGUID))
                {
                    result.Add(HttpHeaderKeys.COMPANY, sysParm.CompanyGUID);
                }
                if(!string.IsNullOrWhiteSpace(sysParm.RequestId))
                {
                    result.Add(HttpHeaderKeys.REQUEST_ID, sysParm.RequestId);
                }
                if(sysParm.SiteLogin >= 0)
                {
                    result.Add(HttpHeaderKeys.SITE, SysAccessLevelHelper.GetSiteHeaderFromEnumValue(sysParm.SiteLogin));
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
