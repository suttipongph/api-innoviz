﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Innoviz.SmartApp.Core.Service
{
    public class BookmarkHelper
    {
        #region bookmarks
        public IDictionary<string, string> GetDocumentBookmarkValues(WordprocessingDocument document, bool includeHiddenBookmarks = false)
        {
            IDictionary<string, string> bookmarks = new Dictionary<string, string>();

            foreach (var bookmark in GetAllBookmarks(document))
            {
                if (includeHiddenBookmarks || !IsHiddenBookmark(bookmark.Name))
                    bookmarks[bookmark.Name] = GetText(bookmark);
            }
            return bookmarks;
        }

        public void SetDocumentBookmarkValues(WordprocessingDocument document, IDictionary<string, string> bookmarkValues)
        {
            foreach (var bookmark in GetAllBookmarks(document))
            {
                SetBookmarkValue(bookmark, bookmarkValues);
            }
        }

        private IEnumerable<BookmarkStart> GetAllBookmarks(WordprocessingDocument document)
        {
            IEnumerable<BookmarkStart> all = document.MainDocumentPart.RootElement.Descendants<BookmarkStart>();

            //Header in word
            foreach(HeaderPart item in document.MainDocumentPart.HeaderParts)
            {
                IEnumerable<BookmarkStart> header = item.RootElement.Descendants<BookmarkStart>();
                all = all.Concat(header);
            }

            //Footer in word
            foreach(FooterPart item in document.MainDocumentPart.FooterParts)
            {
                IEnumerable<BookmarkStart> footer = item.RootElement.Descendants<BookmarkStart>();
                all = all.Concat(footer);
            }

            return all;
        }

        private bool IsHiddenBookmark(string bookmarkName)
        {
            return bookmarkName.StartsWith("_");
        }

        public string GetText(BookmarkStart bookmark)
        {
            var text = FindBookmarkText(bookmark);

            if (text != null)
                return text.Text;
            else
                return string.Empty;
        }

        private void SetBookmarkValue(BookmarkStart bookmark, IDictionary<string, string> bookmarkValues)
        {
            string value;

            if (bookmarkValues.TryGetValue(bookmark.Name, out value))
            {
                SetText(bookmark, value);
            }
        }

        public void SetText(BookmarkStart bookmark, string value)
        {
            var text = FindBookmarkText(bookmark);

            if (text != null)
            {
                text.Text = value;
                RemoveOtherTexts(bookmark, text);
            }
            else
            {
                if (!string.IsNullOrEmpty(value) && value.Contains("\n")) //Text contains new line
                {
                    var rows = value.Split(new string[] { "\n" }, StringSplitOptions.None);
                    //Get last row first because 'InsertAfter' concat new row value into in front of previous value => exp. ... row[2].value row[1].value
                    for (int i = rows.Count() - 1; i >= 0; i--) 
                    {
                        InsertBookmarkText(bookmark, rows[i]);
                        if (i != 0)
                            bookmark.Parent.InsertAfter(new Break(), bookmark);
                    }
                }
                else
                {
                    InsertBookmarkText(bookmark, value);
                }
            }
        }

        private Text FindBookmarkText(BookmarkStart bookmark)
        {
            if (bookmark.ColumnFirst != null)
                return FindTextInColumn(bookmark);
            else
            {
                var run = bookmark.NextSibling<Run>();

                if (run != null)
                    return run.GetFirstChild<Text>();
                else
                {
                    Text text = null;
                    var nextSibling = bookmark.NextSibling();
                    while (text == null && nextSibling != null)
                    {
                        if (nextSibling.IsEndBookmark(bookmark))
                            return null;

                        text = nextSibling.GetFirstDescendant<Text>();
                        nextSibling = nextSibling.NextSibling();
                    }

                    return text;
                }
            }
        }

        private Text FindTextInColumn(BookmarkStart bookmark)
        {
            var cell = bookmark.GetParent<TableRow>().GetFirstChild<TableCell>();

            for (int i = 0; i < bookmark.ColumnFirst; i++)
            {
                cell = cell.NextSibling<TableCell>();
            }

            return cell.GetFirstDescendant<Text>();
        }

        private void RemoveOtherTexts(BookmarkStart bookmark, Text keep)
        {
            if (bookmark.ColumnFirst != null) return;

            Text text = null;
            var nextSibling = bookmark.NextSibling();
            while (text == null && nextSibling != null)
            {
                if (nextSibling.IsEndBookmark(bookmark))
                    break;

                foreach (var item in nextSibling.Descendants<Text>())
                {
                    if (item != keep)
                        item.Remove();
                }
                nextSibling = nextSibling.NextSibling();
            }
        }

        private void InsertBookmarkText(BookmarkStart bookmark, string value)
        {
            bookmark.Parent.InsertAfter(new Run(new Text(value)), bookmark);
        }
        #endregion bookmarks
        #region replace bookmark
        public FileInformation ReplaceBookmark(string base64Content,
                                        IDictionary<string, string> bookmarkValues,
                                        string outputFileName)
        {
            try
            {
                FileInformation result = new FileInformation();
                byte[] bytesContent = Convert.FromBase64String(base64Content);
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(bytesContent, 0, bytesContent.Length);
                    using (WordprocessingDocument wpd = WordprocessingDocument.Open(ms, true))
                    {
                        SetDocumentBookmarkValues(wpd, bookmarkValues);
                    }

                    ms.Position = 0;
                    result.Base64 = Convert.ToBase64String(ms.ToArray());
                    result.FileName = string.IsNullOrWhiteSpace(outputFileName) ?
                                        DateTime.Now.ToString("yyyyMMddHHmmssfff") + FileEXT.DOCX_EXTENSION : outputFileName;
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
