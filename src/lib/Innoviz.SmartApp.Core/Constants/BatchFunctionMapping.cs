﻿using Innoviz.SmartApp.Core.ViewModels;
using System.Collections.Generic;

namespace Innoviz.SmartApp.Core.Constants
{
    public static class BatchFunctionMappingKey
    {
        public const string InterfaceAccountingStaging = "InterfaceAccountingStaging";
        public const string InterfaceVendorInvoice = "InterfaceVendorInvoice";
        public const string GenerateStagingAccounting = "GenerateAccountingStaging";
        public const string GenerateStagingVendorInvoice = "GenerateVendorInvoiceStaging";
        public const string InterfaceIntercoInvSettlement = "InterfaceIntercoInvSettlement";
    }
    public class BatchFunctionMapping
    {
        public static readonly Dictionary<string, BatchFunctionClassMapping> Mappings = new Dictionary<string, BatchFunctionClassMapping>()
        {
            {
                BatchFunctionMappingKey.InterfaceAccountingStaging,
                new BatchFunctionClassMapping
                {
                    ClassName = "Innoviz.SmartApp.Data.InterfaceWithOtherSystems.Services.InterfaceStagingService",
                    MethodName = "InterfaceAccountingStaging"
                }
            },
            {
                BatchFunctionMappingKey.InterfaceVendorInvoice,
                new BatchFunctionClassMapping
                {
                    ClassName = "Innoviz.SmartApp.Data.InterfaceWithOtherSystems.Services.InterfaceStagingService",
                    MethodName = "InterfaceVendorInvoice"
                }
            },
            {
                BatchFunctionMappingKey.GenerateStagingAccounting,
                new BatchFunctionClassMapping
                {
                    ClassName = "Innoviz.SmartApp.Data.ServicesV2.StagingTableService",
                    MethodName = "GenerateStagingAccounting"
                }
            },
            {
                BatchFunctionMappingKey.GenerateStagingVendorInvoice,
                new BatchFunctionClassMapping
                {
                    ClassName = "Innoviz.SmartApp.Data.ServicesV2.StagingTableService",
                    MethodName = "GenerateStagingVendorInvoice"
                }
            },
            {
                BatchFunctionMappingKey.InterfaceIntercoInvSettlement,
                new BatchFunctionClassMapping
                {
                    ClassName = "Innoviz.SmartApp.Data.InterfaceWithOtherSystems.Services.InterfaceStagingService",
                    MethodName = "InterfaceIntercoInvSettlement"

                }
            },
            {
                "AddException",
                new BatchFunctionClassMapping
                {
                    ClassName = "Innoviz.SmartApp.Data.ServicesV2.DemoService",
                    MethodName = "AddException"
                }
            },
        };
    }
}
