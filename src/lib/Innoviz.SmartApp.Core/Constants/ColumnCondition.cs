﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.Constants
{
    public static class BlacklistStatusCondition
    {
        public const string BlacklistStatusId = "BlacklistStatusId";
    }
    public static class AssetGroupCondition
    {
        public const string AssetGroupId = "AssetGroupId";
    }
    public static class EmployeeCondition
    {
        public const string InActive = "InActive";
        public const string CompanyGUID = "CompanyGUID";
        public const string EmployeeTableGUID = "EmployeeTableGUID";
    }
    public static class CompanyParameterCondition
    {
        public const string CompanyGUID = "CompanyGUID";
        public const string FTUnearnedInterestInvTypeGUID = "FTUnearnedInterestInvTypeGUID";
        public const string FTInterestInvTypeGUID = "FTInterestInvTypeGUID"; 
        public const string PFUnearnedInterestInvTypeGUID = "PFUnearnedInterestInvTypeGUID";
        public const string PFInterestInvTypeGUID = "PFInterestInvTypeGUID";
        public const string SettlementMethodOfPaymentGUID = "SettlementMethodOfPaymentGUID";
        public const string FTRetentionInvTypeGUID = "FTRetentionInvTypeGUID";
        public const string FTReserveInvRevenueTypeGUID = "FTReserveInvRevenueTypeGUID";
    }
    public static class DocumentStatusCondition
    {
        public const string ProcessId = "ProcessId";
        public const string StatusId = "StatusId";
        public const string DocumentStatusGUID = "DocumentStatusGUID";
    }
    public static class DocumentStatusAsignmentAgreementCondition
    {
        public const string ProcessId = "DocumentProcess_ProcessId";
        public const string StatusId = "DocumentStatus_StatusId";
    }
    public static class DocumentProcessCondition
    {
        public const string DocumentProcessGUID = "DocumentProcessGUID";
    }
    public static class AddressCountryCondition
    {
        public const string AddressCountryGUID = "AddressCountryGUID";
    }
    public static class AddressProvinceCondition
    {
        public const string AddressProvinceGUID = "AddressProvinceGUID";
    }
    public static class AddressDistrictCondition
    {
        public const string AddressDistrictGUID = "AddressDistrictGUID";
    }
    public static class AddressPostalCodeCondition
    {
        public const string AddressSubDistrictGUID = "AddressSubDistrictGUID";
    }
    public static class ProductSubTypeCondition
    {
        public const string ProductType = "ProductType";
    }
    public static class CreditLimitTypeCondition
    {
        public const string ProductType = "ProductType";
    }
    public static class ServiceFeeCondTemplateTableCondition
    {
        public const string ProductType = "ProductType";
    }
    public static class ContactPersonTransCondition
    {
        public const string RefGUID = "RefGUID";
        public const string RefType = "RefType";
        public const string Inactive = "Inactive";
    }
    public static class AddressTransCondition
    {
        public const string RefGUID = "RefGUID";
        public const string RefType = "RefType";
    }
    public static class CustBankCondition
    {
        public const string CustomerTableGUID = "CustomerTableGUID";
        public const string Inactive = "Inactive";
        public const string BankAccountControl = "BankAccountControl";
        public const string PDC = "PDC";
    }
    public static class CopyGuarantorFromCACondition
    {
        public const string InActive = "GuarantorTransTable_InActive";
        public const string RefGUID = "GuarantorTransTable_RefGUID";
        public const string Affiliate = "Affiliate";

    }
    public static class ApplicationTableCondition
    {
        public const string CustomertableGUID = "CustomertableGUID";
        public const string StatusId = "DocumentStatus_StatusId";
    }
    public static class MessengerJobCondition
    {
        public const string CustomertableGUID = "CustomertableGUID";
        public const string Result = "Result";
    }

    public static class InvoiceRevenueTypeCondition
    {
        public const string ProductType = "ProductType";
        public const string IntercompanyTableGUID = "IntercompanyTableGUID";
    }

    public static class BusinessUnitCondition
    {
        public const string BusinessUnitGUID = "BusinessUnitGUID";
    }
    public static class AssignmentAgreementCondition
    {
        public const string BuyerTableGUID = "BuyerTableGUID";
        public const string CustomerTableGUID = "CustomerTableGUID";
        public const string StatusId = "DocumentStatus_StatusId";
        public const string AssignmentAgreementTableGUID = "AssignmentAgreementTableGUID";
        public const string AssignmentMethodGUID = "AssignmentMethodGUID";
    }
    public static class DocumentTemplateCondition
    {
        public const string TemplateId = "TemplateId";
        public const string Description = "Description";
        public const string DocumentTemplateType = "DocumentTemplateType";
    }
    public static class AttachmentCondition
    {
        public const string FileDescription = "FileDescription";
    }
    public static class BuyerAgreementTableCondition
    {
        public const string CreditAppRequestLineGUID = "CreditAppRequestLine_CreditAppRequestLineGUID";
        public const string BuyerAgreementTableGUID = "BuyerAgreementTableGUID";
        public const string BuyerTableGUID = "BuyerTableGUID";
        public const string CustomerTableGUID = "CustomerTableGUID";
        public const string BuyerAgreementTransRefGUID = "BuyerAgreementTrans_RefGUID";
        public const string BuyerAgreementTransRefType = "BuyerAgreementTrans_RefType";
        public const string CreditAppTable_CreditAppTableGUID = "CreditAppTable_CreditAppTableGUID";
    }
    public static class CreditAppTableCondition
    {
        public const string CustomerTableGUID = "CustomerTableGUID";
        public const string StatusId = "DocumentStatus_StatusId";
        public const string ProductType = "ProductType";
        public const string CreditLimitTypeGUID = "CreditLimitTypeGUID";
        public const string InactiveDate = "InactiveDate";
        public const string ExpiryDate = "ExpiryDate";
        
    }
    public static class CreditAppLineCondition
    {
        public const string ProductType = "CreditAppTable_ProductType";
        public const string BuyerTableGUID = "BuyerTableGUID";
        public const string CreditAppTableGUID = "CreditAppTableGUID";
        public const string ExpiryDate = "ExpiryDate";
    }
    public static class BuyerTableCondition
    {
        public const string BuyerTableGUID = "BuyerTableGUID";
        public const string CreditAppTableGUID = "CreditAppTable_CreditAppTableGUID";
        public const string ExpiryDate = "CreditAppLine_ExpiryDate";
    }
    public static class WithdrawalTableCondition
    {
        public const string WithdrawalTable = "WithdrawalTableGUID";
        public const string CreditAppTableGUID = "CreditAppTableGUID";
        public const string StatusId = "DocumentStatus_StatusId";
    }
    public static class DocumentTemplateTypeCondition
    {
        public const string DocumentTemplateType = "DocumentTemplateType";
    }
    public static class CustomerVisitingTransCondition
    {
        public const string RefGUID = "RefGUID";
    }
    public static class BookmarkDocumentCondition
    {
        public const string BookmarkDocumentRefType = "BookmarkDocumentRefType";
    }
    public static class CustBusinessCollateralCondition
    {
        public const string CustomertableGUID = "CustomertableGUID";
        public const string Cancelled = "Cancelled";
    }
    public static class BusinessCollateralSubTypeCondition
    {
        public const string BusinessCollateralTypeGUID = "BusinessCollateralTypeGUID";
    }
    public static class AuthorizedPersonTransCondition
    {
        public const string RefGUID = "RefGUID";
        public const string RefType = "RefType";
    }
    public static class CompanySignatureCondition
    {
        public const string BranchGUID = "BranchGUID";
    }
    public static class ConsortiumLineCondition
    {
        public const string ConsortiumTableGUID = "ConsortiumTableGUID";
    }
    public static class SysRoleTableCondition
    {
        public const string SiteLoginType = "SiteLoginType";
    }
    public static class ChequeCondition
    {
        public const string CustomerGUID = "CustomerTableGUID";
        public const string BuyerGUID = "BuyerTableGUID";
        public const string RefGUID = "RefGUID";
        public const string DocumentStatusGUID = "DocumentStatusGUID";
    }
    public static class GuarantorTransTableCondition
    {
        public const string RefGUID = "RefGUID";
        public const string SiteLoginType = "SiteLoginType";
        public const string CreditAppGUID = "CreditAppTable_CreditAppTableGUID";
    }
    public static class CreditAppRequestTableCondition
    {
        public const string CustomerTableGUID = "CustomerTableGUID";
        public const string StatusId = "DocumentStatus_StatusId";
        public const string ProductType = "ProductType";
        public const string ConsortiumTableGUID = "ConsortiumTableGUID";
        public const string RefType = "RefType";
        public const string RefCreditAppTableGUID = "RefCreditAppTableGUID";
    }
    public static class CreditAppReqBusinessCollateralCondition
    {
        public const string CreditAppRequestTableGUID = "CreditAppRequestTableGUID";
        public const string IsNew = "IsNew";
    }
    public static class RelatedPersonTableCondition
    {
        public const string CreditAppRequestTableGUID = "CreditAppRequestTable_CreditAppRequestTableGUID";
        public const string RefType = "Reference_RefType";
        public const string Inactive = "Reference_Inactive";
    }

    public static class VerificationTableCondition
    {
        public const string BuyerTableGUID = "BuyerTableGUID";
        public const string CreditAppTableGUID = "CreditAppTableGUID";
        public const string CustomerTableGUID = "CustomerTableGUID";
        public const string DocumentStatusGUID = "DocumentStatusGUID";
        public const string DocumentProcessGUID = "DocumentProcessGUID";
        public const string StatusId = "DocumentStatus_StatusId";
        public const string DocumentId = "VerificationTrans_DocumentId";
        public const string VerificationTableGUID = "VerificationTableGUID";
    }
    public static class ChequeTableCondition
    {
        public const string CustomerTableGUID = "CustomerTableGUID";
        public const string BuyerTableGUID = "BuyerTableGUID";
        public const string RefGUID = "RefGUID";
        public const string ReceivedFrom = "ReceivedFrom";
        public const string RefType = "RefType";
        public const string StatusId = "DocumentStatus_StatusId";
    }
    public static class BuyerInvoiceTableCondition
    {
        public const string CreditAppLineGUID = "CreditAppLineGUID";
    }
    public static class BuyerAgreementTransCondition
    {
        public const string RefGUID = "RefGUID";
        public const string StatusId = "StatusId";
        public const string CreditAppTableGUID = "CreditAppTableGUID";
    }
    public static class InvoiceTypeCondition
    {
        public const string SuspenseInvoiceType = "SuspenseInvoiceType";
        public const string DirectReceipt = "DirectReceipt";
        public const string ProductType = "ProductType";
        public const string AutoGenInvoiceRevenueTypeGUID = "AutoGenInvoiceRevenueTypeGUID ";
    }
    public static class InvoiceTableCondition
    {
        public const string InvoiceTableGUID = "InvoiceTableGUID";
        public const string CreditAppRequestTableGUID = "CreditAppRequestTable_CreditAppRequestTableGUID";
        public const string CustomerTableGUID = "CustomerTableGUID";
        public const string SuspenseInvoiceType = "SuspenseInvoiceType";
        public const string ProductInvoice = "ProductInvoice";
        public const string ProductType = "ProductType";
    }

    public static class CustmerTableCondition
    {
        public const string GuarantorAgreementGUID = "GuarantorAgreement_GuarantorAgreementGUID";
        public const string StatusId = "DocumentStatus_StatusId";
        public const string GuarantorAgreement_GuarantorAgreementGUID = "GuarantorAgreement_GuarantorAgreementGUID";
        public const string ParentCompanyGUID = "ParentCompanyGUID";

    }
    public static class MainAgreementCondition
    {
        public const string MainAgreementGUID = "MainAgreementTableGUID";
        public const string CustomerGUID = "CustomerTableGUID";
        public const string ProductType = "ProductType";
        public const string AgreementDocType = "AgreementDocType";
    }

    public static class SysFeatureGroupRoleCondition
    {
        public const string RoleGUID = "RoleGUID";
        public const string SysRoleTable_DisplayName = "SysRoleTable_DisplayName";
        public const string SysRoleTable_CompanyGUID = "SysRoleTable_CompanyGUID";
        public const string SysRoleTable_SiteLoginType = "SysRoleTable_SiteLoginType";
    }
    public static class ActionHistoryCondition
    {
        public const string RefGUID = "RefGUID";
        public const string ActivityName = "ActivityName";
        public const string ActionName = "ActionName";
    }

    public static class ContactPersonCondition
    {
        public const string RefGUID = "RefGUID";
        public const string RefType = "RefType";
    }
    public static class RetentionTransByCreditAppCondition
    {
        public const string RefType = "RefType";
    }
    public static class JobChequeCondition
    {
        public const string Customer = "CustomerTableGUID";
        public const string Buyer = "BuyerTableGUID";
        public const string DocumentStatusId = "DocumentStatus_StatusId";
        public const string CollectionFollowUpResult = "CollectionFollowUpResult";
        public const string RefGUID = "RefGUID ";
        public const string PaymentType = "MethodOfPayment_PaymentType";
    }
    public static class BuyerReceiptTableCondition
    {
        public const string BuyerTableGUID = "BuyerTableGUID";
        public const string CustomerTableGUID = "CustomerTableGUID";
        public const string Cancel = "Cancel";
    }
    public static class CreditAppReqAssignmentCondition
    {
        public const string CreditAppRequestTableGUID = "CreditAppRequestTableGUID";
        public const string IsNew = "IsNew";
    }
    public static class DocumentReasonCondition
    {
        public const string RefType = "RefType";

    }
    public static class PurchaseLineCondition
    {
        public const string SettleInterestAmount = "SettleInterestAmount";
        public const string SettlePurchaseFeeAmount = "SettlePurchaseFeeAmount";
        public const string SettleReceiptFeeAmount = "SettleReceiptFeeAmount";
        public const string SettleBillingFeeAmount = "SettleBillingFeeAmount";
        public const string SettleRollBillInterestAmount = "SettleRollBillInterestAmount";
    }
    public static class BusinessCollateralAgmTableCondition
    {
        public const string BusinessCollateralAgmTableGUID = "BusinessCollateralAgmTableGUID";
        public const string RefBusinessCollateralAgmTableGUID = "RefBusinessCollateralAgmTableGUID";
        public const string DocumentStatusGUID = "DocumentStatusGUID";
        public const string AgreementDocType = "AgreementDocType";
        public const string OriginalBusinessCollateralAgreementTableGUID = "OriginalBusinessCollateralAgreementTableGUID";
    }
    public static class CustomerTableCondition
    {
        public const string CustomerTableGUID = "CustomerTableGUID";
        public const string ExpiryDate = "ExpiryDate";
        public const string ParentCreditLimitType = "ParentCreditLimitType";
    }
    public static class BranchCondition
    {
        public const string BranchGUID = "BranchGUID";
    }
    public static class StagingTransTextCondition
    {
        public const string ProcessTransType = "ProcessTransType";
    }
    public static class ProcessTransCondition
    {
        public const string TaxTableGUID = "TaxTableGUID";
    }
    public static class InvoiceLineCondition
    {
        public const string InvoiceRevenueTypeGUID = "InvoiceRevenueTypeGUID";
    }
    public static class TaxTableCondition
    {
        public const string TaxTableGUID = "TaxTableGUID";
    }
    public static class VendorPaymentTransCondition
    {
        public const string OffsetAccount = "OffsetAccount";
    }
    public static class StagingTableCondition
    {
        public const string RefGUID = "RefGUID";
    }
    public static class VendBankCondition
    {
        public const string VendorTableGUID = "VendorTableGUID";
    }
    public static class PaymentDetailCondition
    {
        public const string InvoiceTypeGUID = "InvoiceTypeGUID";
    }
    public static class ReceiptTempTableCondition
    {
        public const string SuspenseInvoiceTypeGUID = "SuspenseInvoiceTypeGUID";
    }
}
