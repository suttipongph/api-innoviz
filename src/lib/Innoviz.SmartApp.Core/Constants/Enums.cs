﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Innoviz.SmartApp.Core.Constants
{
    public enum SearchingType
    {
        avoid = -1,
        initial = 0
    }
    public enum AccessMode
    {
        [Lss(Value = 0, Code = "NoAccess")]
        NoAccess,

        [Lss(Value = 1, Code = "Viewer")]
        Viewer,

        [Lss(Value = 2, Code = "Editor")]
        Editor,

        [Lss(Value = 3, Code = "Creator")]
        Creator,

        [Lss(Value = 4, Code = "Full")]
        Full,
    }
    public enum DataType
    {
        [Lss(Value = 0, Code = "Guid", Description = "System.Guid")]
        GUID,

        [Lss(Value = 1, Code = "String", Description = "System.String")]
        STRING,

        [Lss(Value = 2, Code = "Int", Description = "System.Int")]
        INT,

        [Lss(Value = 3, Code = "DateTime", Description = "System.DateTime")]
        DATETIME,

        [Lss(Value = 4, Code = "Boolean", Description = "System.Boolean")]
        BOOLEAN,

        [Lss(Value = 5, Code = "Decimal", Description = "System.Decimal")]
        DECIMAL,

        [Lss(Value = 6, Code = "Date", Description = "System.Date")]
        DATE,
    }
    public enum BranchFilterType
    {
        [Lss(Value = 0, Code = "BySpecificBranch")]
        SpecificBranch,

        [Lss(Value = 1, Code = "ByAllBranch")]
        AllBranch,

        [Lss(Value = 2, Code = "ByCompany")]
        ByCompany,
    }
    public enum BaseType
    {
        [Lss(Value = 0, Code = "BaseEntity")]
        BaseEntity,
        [Lss(Value = 1, Code = "DateEffectiveBaseEntity")]
        DateEffectiveBase,
        [Lss(Value = 2, Code = "CompanyBaseEntity")]
        CompanyBase,
        [Lss(Value = 3, Code = "BranchCompanyBaseEntity")]
        BranchCompanyBase,
    }
    public enum BracketType
    {
        [Lss(Value = 0, Code = "")]
        None,
        [Lss(Value = 1, Code = "(")]
        SingleStart,
        [Lss(Value = 2, Code = ")")]
        SingleEnd,
        [Lss(Value = 3, Code = "((")]
        DoubleStart,
        [Lss(Value = 4, Code = "))")]
        DoubleEnd,

    }
    #region LIT
    public enum AccessLevel
    {
        [Lss(Value = 0, Code = "None")]
        None,
        [Lss(Value = 1, Code = "User")]
        User,
        [Lss(Value = 2, Code = "BusinessUnit")]
        BusinessUnit,
        [Lss(Value = 3, Code = "ParentChildBU")]
        ParentChildBU,
        [Lss(Value = 4, Code = "Company")]
        Company
    }
    public enum SysFeatureType
    {
        Primary,
        RelatedInfo,
        Function,
        Report,
        Workflow,
        WorkflowActionHistory
    }
    public enum AccessRight
    {
        NoAccess,
        Read,
        Update,
        Create,
        Delete,
        Action
    }
    public enum SysDataInitType
    {
        SQLStoredProcAndFunction,
        SysDuplicateDetection,
        SysMessage,
        SysLabelReport,
        DocumentProcessStatus,
        SysEnumTable,
        SysRoleTable,
        SysFeatureTableData,
        MigrationTable
    }
    #endregion LIT
    #region Attribute Function

    public class LssAttribute : Attribute
    {
        public string Code { get; set; }
        public int Value { get; set; }
        public string Description { get; set; }
        public string UserDefine { get; set; }
    }

    public static class LssEnums
    {
        public static string GetAttrCode<T>(this T value)
        {
            string output = null;
            Type type = value.GetType();
            FieldInfo fi = type.GetField(value.ToString());
            LssAttribute[] attrs = fi.GetCustomAttributes(typeof(LssAttribute), false) as LssAttribute[];
            if (attrs.Length > 0)
            {
                output = attrs[0].Code;
            }
            return output;
        }

        public static int GetAttrValue<T>(this T value)
        {
            int output = 0;
            Type type = value.GetType();
            FieldInfo fi = type.GetField(value.ToString());
            LssAttribute[] attrs = fi.GetCustomAttributes(typeof(LssAttribute), false) as LssAttribute[];
            if (attrs.Length > 0)
            {
                output = attrs[0].Value;
            }
            return output;
        }

        public static string GetAttrDesc<T>(this T value)
        {
            string output = null;
            Type type = value.GetType();
            FieldInfo fi = type.GetField(value.ToString());
            LssAttribute[] attrs = fi.GetCustomAttributes(typeof(LssAttribute), false) as LssAttribute[];
            if (attrs.Length > 0)
            {
                output = attrs[0].Description;
            }
            return output;
        }
        public static string GetAttrUserDef(this Enum value)
        {
            string output = null;
            Type type = value.GetType();
            FieldInfo fi = type.GetField(value.ToString());
            LssAttribute[] attrs = fi.GetCustomAttributes(typeof(LssAttribute), false) as LssAttribute[];
            if (attrs.Length > 0)
            {
                output = attrs[0].UserDefine;
            }
            return output;
        }

    }

    #endregion
}
