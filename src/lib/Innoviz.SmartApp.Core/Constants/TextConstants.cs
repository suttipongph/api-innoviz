﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.Constants
{
    public static class TextConstants
    {
        public static string PredicateAll = "1=1";
        public static string PredicateNot = "1!=1";
        public static string CompanyGUID = "CompanyGUID";
        public static string EffectiveFrom = "EffectiveFrom";
        public static string EffectiveTo = "EffectiveTo";
        public static string BranchGUID = "BranchGUID";

        public static string All = "All";
        public static string ADMIN = "ADMIN";
        public static string RevertChar = "_C";

        public static List<string> SqlWordingException = new List<string>() { "Revert" };

        public static string NewLine = System.Environment.NewLine;


        public static string Owner = "Owner";
        public static string OwnerBusinessUnitGUID = "OwnerBusinessUnitGUID";
        public static string CreatedBy = "CreatedBy";
        public static string CreatedDateTime = "CreatedDateTime";
        public static string ModifiedBy = "ModifiedBy";
        public static string ModifiedDateTime = "ModifiedDateTime";
        public static string RowAuthorize = "RowAuthorize";
        public static string BindingItem = "BINDING_ITEM";
        public static string BindingSingleItem = "BINDING_SINGLE_ITEM";
        public static string ParentColumn = "PARENT_COLUMN";
        public static string BasePredicate = "BasePredicate";
        public static string SearchConditionValue = "CONDITION_VALUE";
        public static string RowVersion = "RowVersion";
        public static string Checked = "X";
        public static string UnChecked = "";
        public static string Revolving = "Revolving";
        public static string NonRevolving = "Non Revolving";
        public static string TH = "TH";
        public static string EN = "EN";
        public static string DMY = "DAY MONTH YEAR";
        public static string Yes = "Yes";
        public static string No = "No";
        public static string Government = "Government";
        public static string RefType = "RefType";
        public static string Private = "Private";

        public static string Approve = "Approve";
        public static string AssistMDApprove = "Assist MD approve";
        public static string Board2In3Approve = "Board 2 in 3 approve";
        public static string Board3In4Approve = "Board 3 in 4 approve";
        public static string InterCompanyId_LISM = "LISM";
    }
    public static class ColumnType
    {
        public static string STRING = "STRING";
        public static string INT = "INT";
        public static string DATE = "DATE";
        public static string DATETIME = "DATETIME";
        public static string DATERANGE = "DATERANGE";
        public static string BOOLEAN = "BOOLEAN";
        public static string MASTER = "MASTER";
        public static string MASTERSINGLE = "MASTERSINGLE";
        public static string ENUM = "ENUM";
        public static string VARIABLE = "VARIABLE";
        public static string VARIABLES = "VARIABLES";
        public static string DECIMAL = "DECIMAL";

        public static string PREDICATE_NOT = "PREDICATE_NOT";
        public static string NULLABLE = "Nullable`1";
    }
    public static class SystemType
    {
        public static string STRING = "SYSTEM.STRING";
        public static string INT = "SYSTEM.INT";
        public static string INT32 = "SYSTEM.INT32";
        public static string DATE = "DATE";
        public static string DATETIME = "DATETIME";
        public static string DATERANGE = "DATERANGE";
        public static string BOOLEAN = "BOOLEAN";
        public static string MASTER = "MASTER";
        public static string ENUM = "ENUM";
        public static string VARIABLE = "VARIABLE";
        public static string VARIABLES = "VARIABLES";
        public static string LIST = "System.Collections.Generic.List";
        public static string GUID = "GUID";
        public static string NULLABLE = "System.Nullable`1";
        public static string DECIMAL = "DECIMAL";
        public static string SELECTITEM = "SELECTITEM";
        public static string BYTEARRAY = "SYSTEM.BYTE[]";
    }
    public static class Operators
    {
        public static string AND = " AND ";
        public static string OR = " OR ";
        public static string EQUAL = " == ";
        public static string NOT_EQUAL = " != ";
        public static string NOT = "!";
    }
    public static class Brackets
    {
        public const string AND_SINGLE = " AND (";
        public const string AND_DOUBLE = " AND ((";
        public const string OR_SINGLE = " OR (";
        public const string OR_DOUBLE = " OR ((";
        public const string SINGLE_END = ") ";
        public const string DOUBLE_END = ")) ";
    }
    public static class ReferenceId
    {
        public static string AgentJobJournal = "AgentJobJournal";
        public static string AgreementAssetFees = "AgreementAssetFees";
        public static string AgreementClassificationJournal = "AgreementClassificationJournal";
        public static string CollateralMovementJournal = "CollateralMovementJournal";
        public static string CollectionActivityJournal = "CollectionActivityJournal";
        public static string CollectionFee = "CollectionFee";
        public static string DutyStampJournal = "DutyStampJournal";
        public static string Employee = "Employee";
        public static string Guarantor = "Guarantor";
        public static string LetterOfExpirationJournal = "LetterOfExpirationJournal";
        public static string LetterOfOverdueJournal = "LetterOfOverdueJournal";
        public static string LetterOfRenewalJournal = "LetterOfRenewalJournal";
        public static string ProvisionJournal = "ProvisionJournal";
        public static string PurchAgreement = "PurchAgreement";
        public static string PurchAgreementConfirm = "PurchAgreementConfirm";
        public static string ReceiptTemp = "ReceiptTemp";
        public static string Vendor = "Vendor";
        public static string Waive = "Waive";
        public static string WaiveRequest = "WaiveRequest";
        public static string Buyer = "Buyer";
        public static string RelatedPerson = "RelatedPerson";
        public static string Customer = "Customer";
        public static string BuyerAgreement = "BuyerAgreement";
        public static string Consortium = "Consortium";
        public static string DocumentConditionTemplateTable = "DocumentConditionTemplateTable";
        public static string BookmarkDocumentTemplateTable = "BookmarkDocumentTemplateTable";
        public static string InternalAssignmentAgreement = "InternalAssignmentAgreement";
        public static string AssignmentAgreement = "AssignmentAgreement";
        public static string InternalBusinessCollateralAgreement = "InternalBusinessCollateralAgreement";
        public static string BusinessCollateralAgreement = "BusinessCollateralAgreement";
        public static string Verification = "Verification";
        public static string Purchase = "Purchase";
        public static string MessengerJob = "MessengerJob";
        public static string Withdrawal = "Withdrawal";
        public static string BuyerReceipt = "BuyerReceipt";
        public static string SuspenseRefund = "SuspenseRefund";
        public static string ReserveRefund = "ReserveRefund";
        public static string RetentionRefund = "RetentionRefund";
        public static string CustomerBusinessCollateral = "CustomerBusinessCollateral";
        public static string DocumentReturn = "DocumentReturn";
        public static string IntercompanyInvoice = "IntercompanyInvoice";
        public static string FreeTextInvoice = "FreeTextInvoice";
        public static string IntercompanyInvoiceSettlement = "IntercompanyInvoiceSettlement";
    }

    public static class CustomClaimType
    {
        public const string LOGIN_SITE = "login-site";
        public const string ROLE = "role";
        public const string USUBJ = "usubj";
        public const string USERNAME = "username";
        public const string INACTIVE = "inActive";
        public const string NAME = "name";
        public const string EMAIL = "email";
        public const string SUB = "sub";
        public const string CLIENT_ID = "client_id";
    }

    public static class SiteLoginType
    {
        public const string FRONT = "front";
        public const string BACK = "back";
        public const string CASHIER = "cashier";
        public const string OB = "ob";
    }
    public static class SiteLoginValue
    {
        public const int Front = 0;
        public const int Back = 1;
        public const int Cashier = 2;
        public const int OB = 3;
    }

    public static class ExceptionDataKey
    {
        public const string StackTrace = "StackTrace";
        public const string InnerStackTrace = "InnerStackTrace";
        public const string MessageList = "MessageList";
        public const string BatchLogReference = "BatchLogReference";
    }
    public static class MessageGroup
    {
        public const string LABEL = "LABEL";
        public const string ERROR = "ERROR";
        public const string SUCCESS = "SUCCESS";
        public const string CONFIRM = "CONFIRM";
        public const string ENUM = "ENUM";
        public const string SITE = "SITE";
        public const string MENU = "MENU";
        public const string ROLE_CMPNT_GRP = "ROLE_CMPNT_GRP";
        public const string ROLE_ROUTE_SEGMENT = "ROLE_ROUTE_SEGMENT";
    }
    public static class ExceptionLevel
    {
        public const string InnerInnerException = "InnerExceptionInnerException";
        public const string InnerException = "InnerException";
        public const string Exception = "Exception";
    }
    public static class ActionResultText
    {
        public const string Open = "Open";
        public const string Completed = "Completed";
        public const string Failed = "Failed";
    }
    public static class HttpHeaderKeys
    {
        public const string COMPANY = "CompanyHeader";
        public const string BRANCH = "BranchHeader";
        public const string REQUEST_ID = "RequestHeader";
        public const string SITE = "SiteHeader";
        public const string BATCHINSTANCE_ID = "BatchInstanceId";
    }
    public static class NotiType
    {
        public const string WARNING = "warn";
        public const string INFORMING = "info";
        public const string SUCCESSING = "success";
    }
    public static class AttributeType
    {
        public const string STRINGLENGTH = "System.ComponentModel.DataAnnotations.StringLengthAttribute";
    }

    public static class Migrations
    {
        public static string Migration = "Migrate";
        public static string Fully = "Fully";
    }
    public static class FeatureType
    {
        public const string PRIMARY = "primary";
        public const string RELATEDINFO = "related-info";
        public const string FUNCTION = "function";
        public const string REPORT = "report";
        public const string WORKFLOW = "workflow";
        public const string WORKFLOW_ACTIONHISTORY = "wf-action-history";
    }
    public static class DocumentProcessId
    {
        public const string TemporaryReceipt = "50";
        public const string Customer = "100";
        public const string Buyer = "101";
        public const string Consortium = "102";
        public const string Purchase = "160";
        public const string MainAgreement = "170";
        public const string GuarantorAgreement = "180";
        public const string BookmarkDocument = "240";
        public const string Cheque = "130";
        public const string BusinessCollateralAgreement = "190";
        public const string VendorPaymentTrans = "210";
        public const string Invoice = "60";
        public const string TaxInvoice = "61";
        public const string Receipt = "51";
        public const string Withdrawal = "220";
        public const string CreditAppRequest = "110";
        public const string AssignmentAgreement = "120";
        public const string MessengerJob = "140";
        public const string Verification = "150";
        public const string CustomerRefund = "230";
        public const string DocumentReturn = "250";
    }
    public static class K2DataField
    {
        public const string ParmDocGUID = "ParmDocGUID";
        public const string ParmCompany = "ParmCompany";
    }
    public static class UnitTestConst
    {
        public const string Sheet_PARAMETER = "Parameter";
    }
    public static class DropdownConst
    {
        public const int RowsLimit = 10000;
    }
    public static class ReportConst
    {
        public const string ReportName = "ReportName";
        public const string ReportFormat = "ReportFormat";
        public const string ServicePath = "ServicePath";
        public const string CSV_Format = "CSV";
        public const string MHTML_Format = "MHTML";
        public const string PDF_Format = "PDF";
        public const string TIFF_Format = "TIFF";
        public const string XML_Format = "XML";
        public const string WORDOPENXML_Format = "WORDOPENXML";
        public const string EXCELOPENXML_Format = "EXCELOPENXML";
    }
    public static class RoleRouteConst
    {
        public const string RelatedInfo = "Related info";
        public const string Function = "Function";
        public const string Report = "Report";
        public const string Workflow = "Workflow";
    }
    public static class CRUDActionConst
    {
        public const string ACTION = "Action";
        public const string READ = "Read";
        public const string CREATE = "Create";
        public const string UPDATE = "Update";
        public const string DELETE = "Delete";
    }
    public static class SystemTableName
    {
        public const string StartsWithBatch = "Batch";
        public const string StartsWithSys = "Sys";
        public const string StartsWithMigration = "Migration";
        public const string StartsWithStaging_ = "Staging_";
        public const string NumberSeqTable = "NumberSeqTable";
    }
    public static class GetRelatedOwnerConst
    {
        public const string CustomerTable = "CustomerTable";
        public const string CustomerTableGUID = "CustomerTableGUID";
        public const string RefType = "RefType";
        public const string RefGUID = "RefGUID";
        public const string Owner = "Owner";
        public const string OwnerBusinessUnitGUID = "OwnerBusinessUnitGUID";
        public const string GetRelatedOwner = "GetRelatedOwner";
    }
    public static class MaxValue
    {
        public const decimal PercentMaxValue = 999.99m;
    }
}
