﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.Constants
{
    public static class FileConst
    {
        public const string FileName = "FileName";
        public const string FilePath = "FilePath";
        public const string Base64Data = "Base64Data";
        public const string UploadRoot = "UploadRoot";
        public const string DATABASE = "database";
        public const string DocumentTemplateStorage = "DocumentTemplateStorage";
        public const string FileStorage = "FileStorage";
        public const string LogRootFolder = "LogRootFolder";
        public const string DocumentTemplateTable = "DocumentTemplateTable";
        public const string DocumentTemplateTableItemViewMap = "DocumentTemplateTableItemViewMap";
        public const string DocumentTemplateTableItemView = "DocumentTemplateTableItemView";
        public const string Attachment = "Attachment";
    }
    public static class FileEXT
    {
        public const string TXT_EXTENSION = ".txt";
        public const string PDF_EXTENSION = ".pdf";
        public const string DOC_EXTENSION = ".doc";
        public const string DOCX_EXTENSION = ".docx";
        public const string XLS_EXTENSION = ".xls";
        public const string XLSX_EXTENSION = ".xlsx";
        public const string PNG_EXTENSION = ".png";
        public const string JPG_EXTENSION = ".jpg";
        public const string JPEG_EXTENSION = ".jpeg";
        public const string GIF_EXTENSION = ".gif";
        public const string TIFF_EXTENSION = ".tiff";
        public const string BMP_EXTENSION = ".bmp";
        public const string RAW_EXTENSION = ".raw";
        public const string CSV_EXTENSION = ".csv";
        public const string RAR_EXTENSION = ".rar";
        public const string ZIP_EXTENSION = ".zip";
        public const string SEVENZIP_EXTENSION = ".7z";
        public const string PPT_EXTENSION = ".ppt";
        public const string PPTX_EXTENSION = ".pptx";
        public const string MHT_EXTENSION = ".mht";
        public const string TIF_EXTENSION = ".tif";
        public const string XML_EXTENSION = ".xml";
    }
    public static class MimeHeader
    {
        public const string TXT_MIME_HEADER = "data:text/plain;base64,";
        public const string PDF_MIME_HEADER = "data:application/pdf;base64,";
        public const string DOC_MIME_HEADER = "data:application/vnd.ms-word;base64,";
        public const string DOCX_MIME_HEADER = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,";
        public const string XLS_MIME_HEADER = "data:application/vnd.ms-excel;base64,";
        public const string XLSX_MIME_HEADER = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,";
        public const string PNG_MIME_HEADER = "data:application/vnd.ms-powerpoint;base64,";
        public const string JPG_MIME_HEADER = "data:application/vnd.openxmlformats-officedocument.presentationml.presentation;base64,";
        public const string JPEG_MIME_HEADER = "data:image/png;base64,";
        public const string GIF_MIME_HEADER = "data:image/jpeg;base64,";
        public const string TIFF_MIME_HEADER = "data:image/jpeg;base64,";
        public const string BMP_MIME_HEADER = "data:image/gif;base64,";
        public const string RAW_MIME_HEADER = "data:image/tiff;base64,";
        public const string CSV_MIME_HEADER = "data:image/bmp;base64,";
        public const string RAR_MIME_HEADER = "data:image/raw;base64,";
        public const string ZIP_MIME_HEADER = "data:text/csv;base64,";
        public const string SEVENZIP_MIME_HEADER = "data:application/x-rar-compressed;base64,";
        public const string PPT_MIME_HEADER = "data:application/x-zip-compressed;base64,";
        public const string PPTX_MIME_HEADER = "data:application/x-7z-compressed;base64,";
        public const string OCTET_STREAM_MIME_HEADER = "data:application/octet-stream;base64,";
    }
}
