﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.Repositories
{
    public abstract class SmartAppDbContext : DbContext
    {
        public SmartAppDbContext(DbContextOptions options) 
           : base(options) {
        }

        public abstract string GetUserName();
        public abstract string GetCompanyFilter();
        public abstract string GetBranchFilter();
        public abstract string GetBatchInstanceId();
        public abstract string GetUserId();
        public abstract List<string> GetAvailableBranch();
        public abstract int GetSiteLoginType();
        public abstract void SetSystemParameter(SystemParameter param);
        public abstract SystemParameter GetSystemParameter();

        #region LIT
        public abstract AccessLevelParm GetAccessLevel();
        public abstract AccessLevelParm GetDeleteAccessLevel();
        public abstract SearchCondition GetAccessLevelFilter();
        #endregion LIT
    }

}
