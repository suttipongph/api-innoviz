﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.Core.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        //Synchronous
        TEntity Find(params object[] keyValues);
        bool Exist(params object[] keyValues);
        bool Exist(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> GetList();
        IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate);
        void Add(TEntity item);
        void Add(IEnumerable<TEntity> items);
        void Update(TEntity item);
        void Update(IEnumerable<TEntity> items);
        void Remove(TEntity item);
        void Remove(IEnumerable<TEntity> items);

        void ValidateAdd(TEntity item);
        void ValidateUpdate(TEntity item);
        void ValidateRemove(TEntity item);

        //Asynchronous
        Task<TEntity> FindAsync(params object[] keyValues);
        Task<bool> ExistAsync(params object[] keyValues);
        Task<bool> ExistAsync(Expression<Func<TEntity, bool>> predicate);
        Task<IEnumerable<TEntity>> GetListAsync();
        Task<IEnumerable<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> predicate);
        Task AddAsync(TEntity item);
        Task AddAsync(IEnumerable<TEntity> items);
        Task UpdateAsync(TEntity item);
        Task UpdateAsync(IEnumerable<TEntity> items);
        Task RemoveAsync(TEntity item);
        Task RemoveAsync(IEnumerable<TEntity> items);

    }
}
