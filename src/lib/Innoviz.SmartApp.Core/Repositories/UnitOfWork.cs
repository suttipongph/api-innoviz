﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Data;
using System.Linq;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections.Generic;
using Innoviz.SmartApp.Core.Service;

namespace Innoviz.SmartApp.Core.Repositories
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        private readonly DbContext db;
        private readonly DbContext rDb;
        private IDbContextTransaction transaction;

        public UnitOfWork(DbContext context, DbContext relatedContext)
        {
            db = context;
            rDb = relatedContext;
        }
        public UnitOfWork(DbContext context) {
            db = context;
        }

        public TRepo GetRepository<TRepo>() where TRepo : class
        {
            return (TRepo)Activator.CreateInstance(typeof(TRepo),db);
        }

        public TRepo GetRepoWithRelatedCtx<TRepo>() where TRepo : class {
            return (TRepo)Activator.CreateInstance(typeof(TRepo), db, rDb);
        }
        public IDbTransaction Transaction()
        {
            if (transaction == null)
                transaction = db.Database.BeginTransaction();
            return transaction.GetDbTransaction();
        }
        public IDbContextTransaction ContextTransaction()
        {
            if (transaction == null)
                transaction = db.Database.BeginTransaction();
            return transaction;
        }
        public void Commit(IDbTransaction transaction)
        {
            try
            {
                db.SaveChanges();
                transaction.Commit();
            }
            catch (DbUpdateConcurrencyException e)
            {
                Rollback(transaction);
                SmartAppException ex = new SmartAppException("ERROR.ERROR", e);
                ex.AddData("ERROR.CONCURRENCY");
                throw SmartAppUtil.AddStackTrace(ex);
            }
            catch (DbUpdateException e)
            {
                Rollback(transaction);
                throw SmartAppUtil.ThrowDBException(e.InnerException ?? e);
            }
            catch (Exception e)
            {
                Rollback(transaction);
                //throw SmartAppUtil.AddStackTrace(e);
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public void Commit(IDbContextTransaction transaction)
        {
            try
            {
                db.SaveChanges();
                transaction.Commit();
            }
            catch (DbUpdateConcurrencyException e)
            {
                Rollback(transaction);
                SmartAppException ex = new SmartAppException("ERROR.ERROR", e);
                ex.AddData("ERROR.CONCURRENCY");
                throw SmartAppUtil.AddStackTrace(ex);
            }
            catch (DbUpdateException e)
            {
                Rollback(transaction);
                throw SmartAppUtil.ThrowDBException(e.InnerException ?? e);
            }
            catch (Exception e)
            {
                Rollback(transaction);
                //throw SmartAppUtil.AddStackTrace(e);
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public void Commit()
        {
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                Rollback();
                //throw SmartAppUtil.AddStackTrace(e.InnerException ?? e);
                throw SmartAppUtil.ThrowDBException(e.InnerException ?? e);
            }
            catch (Exception e)
            {
                Rollback();
                //throw SmartAppUtil.AddStackTrace(e);
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public void Rollback(IDbTransaction transaction)
        {
            if (transaction != null) transaction.Rollback();
            Rollback();
        }
        public void Rollback(IDbContextTransaction transaction)
        {
            if (transaction != null) transaction.Rollback();
            Rollback();
        }
        public void Rollback()
        {
            List<EntityEntry> entries = db.ChangeTracker.Entries().ToList();

            foreach (var entry in entries)
            {
                switch (entry.State)
                {
                    case EntityState.Deleted:
                    case EntityState.Modified:
                        entry.State = EntityState.Modified; //Revert changes made to deleted entity.
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    default:
                        break;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                    db.Dispose();
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UnitOfWork() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
