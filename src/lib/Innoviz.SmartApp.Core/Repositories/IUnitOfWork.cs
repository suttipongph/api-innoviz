﻿namespace Innoviz.SmartApp.Core.Repositories
{
    public interface IUnitOfWork
    {
        TEntity GetRepository<TEntity>() where TEntity : class;
        void Commit();
        void Rollback();
    }
}
