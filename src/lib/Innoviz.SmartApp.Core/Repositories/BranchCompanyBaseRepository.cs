﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Core.Repositories
{
    public class BranchCompanyBaseRepository<TEntity> : BaseRepository<TEntity> where TEntity : class
    {
        public BranchCompanyBaseRepository(SmartAppDbContext context)
            :base(context)
        {

        }
        public BranchCompanyBaseRepository(SmartAppDbContext context, SystemParameter systemParameter)
            :base(context, systemParameter)
        {

        }
        public override void Add(TEntity item)
        {
            base.Add(item);
        }

        public override void Add(IEnumerable<TEntity> items) {
            base.Add(items);
        }

        public void Add(TEntity item, string companyGUID, string branchGUID)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<TEntity> GetList() {
            throw new NotImplementedException();
        }

        public IEnumerable<TEntity> GetListByCompany(string companyGUID = null)
        {
            try
            {
                if (companyGUID == null)
                {
                    companyGUID = SysParm.CompanyGUID;
                    if (companyGUID == null)
                    {
                        return new List<TEntity>();
                    }
                }

                var list = Entity.Where(item =>
                             item.GetType().GetProperty("CompanyGUID")
                             .GetValue(item, null) != null &&
                             item.GetType().GetProperty("CompanyGUID")
                             .GetValue(item, null).ToString().ToLower() == companyGUID);

                return list.ToList();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }

    }

}
