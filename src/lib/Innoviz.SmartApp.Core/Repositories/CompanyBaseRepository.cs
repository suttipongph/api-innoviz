﻿using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Text;

namespace Innoviz.SmartApp.Core.Repositories
{
    public class CompanyBaseRepository<TEntity> : BaseRepository<TEntity> where TEntity : class
    {
        public CompanyBaseRepository(SmartAppDbContext context) 
            : base(context)
        {

        }
        
        public CompanyBaseRepository(SmartAppDbContext context, SystemParameter systemParameter)
            : base(context, systemParameter)
        {

        }
        public override IEnumerable<TEntity> GetList() {
            throw new NotImplementedException();
        }
        

        public override void Add(TEntity item)
        {

            base.Add(item);
        }

        public override void Add(IEnumerable<TEntity> items)
        {

            base.Add(items);
        }

        public void Add(TEntity item, string companyGUID)
        {
            throw new NotImplementedException();
        }

        public void Add(IEnumerable<TEntity> items, string companyGUID)
        {
            throw new NotImplementedException();
        }
        
        public IEnumerable<TEntity> GetListByCompany(string companyGUID = null) {
            try {
                if (companyGUID == null) {
                    companyGUID = SysParm.CompanyGUID;
                    if (companyGUID == null) {
                        return new List<TEntity>();
                    }
                }

                var list = Entity.Where(item =>
                             item.GetType().GetProperty("CompanyGUID")
                             .GetValue(item, null) != null &&
                             item.GetType().GetProperty("CompanyGUID")
                             .GetValue(item, null).ToString().ToLower() == companyGUID);

                return list.ToList();
            }
            catch (Exception e) {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
    }
}