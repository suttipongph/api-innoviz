﻿using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Innoviz.SmartApp.Core.Repositories
{
    public class DateEffectiveBaseRepository<TEntity> : BaseRepository<TEntity> where TEntity : class
    {
        public DateEffectiveBaseRepository(SmartAppDbContext context) : base(context)
        {
        }
        public DateEffectiveBaseRepository(SmartAppDbContext context, SystemParameter systemParameter)
            : base(context, systemParameter)
        {

        }
        public override IEnumerable<TEntity> GetList() { 
            var companyFilter = SysParm.CompanyGUID;
            if (companyFilter == null) {
                return new List<TEntity>();
            }
            var list = Entity.Where(item =>
                            item.GetType().GetProperty("CompanyGUID")
                            .GetValue(item, null) != null &&
                            item.GetType().GetProperty("CompanyGUID")
                            .GetValue(item, null).ToString() == companyFilter);
            return list.ToList();
        }

        public override void Add(TEntity item) {
            db.Entry(item).Property("CompanyGUID").CurrentValue = new Guid(db.GetCompanyFilter());

            base.Add(item);
        }

        public override void Add(IEnumerable<TEntity> items) {

            foreach (var item in items) {
                db.Entry(item).Property("CompanyGUID").CurrentValue = new Guid(db.GetCompanyFilter());
            }

            base.Add(items);
        }

        public IEnumerable<TEntity> GetListByCompany(string companyGUID = null)
        {
            try
            {
                if (companyGUID == null)
                {
                    companyGUID = db.GetCompanyFilter();
                    if (companyGUID == null)
                    {
                        return new List<TEntity>();
                    }
                }

                var list = Entity.Where(item =>
                             item.GetType().GetProperty("CompanyGUID")
                             .GetValue(item, null) != null &&
                             item.GetType().GetProperty("CompanyGUID")
                             .GetValue(item, null).ToString().ToLower() == companyGUID);
                return list.ToList();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

    }
}
