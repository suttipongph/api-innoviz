﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Core
{
    public static class SystemStaticData
    {
        private static List<StaticSysFeatureTableData> frontFeatures;
        private static List<StaticSysFeatureTableData> backFeatures;
        private static List<StaticSysFeatureTableData> cashierFeatures;
        private static List<StaticSysFeatureTableData> obFeatures;

        private static List<string> obFeatureGroupIds;

        private static List<StaticSysMessageData> systemMessages;
        private static List<StaticSysDuplicateDetectionData> systemDuplicateDetections;
        private static IConfigurationRoot configuration;

        private static Dictionary<string, string> dropdownLookup;
        private static List<StaticSysControllerEntityTempData> sysControllerEntityTempData;

        private static Dictionary<string, StaticModelTypeLookupData> modelTypeLookup;
        static SystemStaticData()
        {
            frontFeatures = new List<StaticSysFeatureTableData>();
            backFeatures = new List<StaticSysFeatureTableData>();
            cashierFeatures = new List<StaticSysFeatureTableData>();
            obFeatures = new List<StaticSysFeatureTableData>();

            obFeatureGroupIds = new List<string>();

            systemMessages = new List<StaticSysMessageData>();
            systemDuplicateDetections = new List<StaticSysDuplicateDetectionData>();

            dropdownLookup = new Dictionary<string, string>();
            sysControllerEntityTempData = new List<StaticSysControllerEntityTempData>();

            modelTypeLookup = new Dictionary<string, StaticModelTypeLookupData>();
        }

        #region check static data
        public static bool HasStaticSysMessageData()
        {
            return systemMessages != null && systemMessages.Count > 0;
        }
        public static bool HasStaticSysDuplicateData()
        {
            return systemDuplicateDetections != null && systemDuplicateDetections.Count > 0;
        }
        public static bool HasStaticDropdownLookup()
        {
            return dropdownLookup != null && dropdownLookup.Count > 0;
        }
        public static bool HasStaticSysControllerEntityData()
        {
            return sysControllerEntityTempData != null && sysControllerEntityTempData.Count > 0;
        }
        public static bool HasStaticModelTypeLookup()
        {
            return modelTypeLookup != null && modelTypeLookup.Count > 0;
        }
        #endregion

        #region SysMessage
        public static void SetSysMessageData(List<StaticSysMessageData> data)
        {
            systemMessages = data;
        }
        public static string GetTranslatedMessage(TranslateModel translateModel)
        {
            if(translateModel != null)
            {
                return GetTranslatedMessage(translateModel.Code, translateModel.Parameters);
            }
            return string.Empty;
        }
        public static string GetTranslatedMessage(string code, params object[] args)
        {
            try
            {
                if (code != null)
                {
                    var splitCode = code.Split(".");
                    var splitCode0 = splitCode[0].TrimAll();
                    var isMsgCode = splitCode0 == MessageGroup.CONFIRM || splitCode0 == MessageGroup.ENUM || splitCode0 == MessageGroup.ERROR ||
                            splitCode0 == MessageGroup.LABEL || splitCode0 == MessageGroup.MENU || splitCode0 == MessageGroup.ROLE_CMPNT_GRP ||
                            splitCode0 == MessageGroup.SITE || splitCode0 == MessageGroup.SUCCESS || splitCode0 == MessageGroup.ROLE_ROUTE_SEGMENT;

                    code = isMsgCode && splitCode.Length > 1 ? $"{splitCode0}.{splitCode[1].TrimAll()}" : code;
                    
                    StaticSysMessageData msg = systemMessages.Where(item => item.Code.ToLower() == code.ToLower())
                                                        .FirstOrDefault();

                    string result;
                    if (msg != null)
                    {
                        result = msg.Message;
                    }
                    else
                    {
                        result = code;
                    }
                    string[] translateds = TranslateArgs(args);
                    if(args != null && args.Length > 0 && translateds != null)
                    {
                        for (var i = 0; i < args.Length; i++)
                        {
                            var replaceVal = !string.IsNullOrEmpty(translateds[i]) ? translateds[i] : args[i].ToString();
                            if (result.Contains("{{" + i + "}}"))
                            {
                                result = result.Replace("{{" + i + "}}", replaceVal);

                            }
                            else
                            {
                                result += " " + replaceVal;
                            }
                        }
                    }
                    
                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private static string[] TranslateArgs(object[] args)
        {
            try
            {
                StaticSysMessageData argsMsg = new StaticSysMessageData();
                if (args != null && args.Length != 0)
                {
                    string[] translateds = new string[args.Length];
                    for (var i = 0; i < args.Length; i++)
                    {
                        args[i] = args[i] == null ? "" : args[i].ToString();
                        
                        string[] multiLabel = args[i].ToString().Split(',');
                        if (multiLabel.Count() > 1)
                        {
                            foreach (var label in multiLabel)
                            {
                                argsMsg = systemMessages
                               .Where(w => w.Code.ToLower() == label.TrimAll().ToLower())
                               .FirstOrDefault();
                                if (argsMsg != null)
                                    translateds[i] = string.IsNullOrEmpty(translateds[i]) ? argsMsg.Message : string.Format("{0},{1}", translateds[i], argsMsg.Message);
                                else
                                    translateds[i] = string.IsNullOrEmpty(translateds[i]) ? label.TrimAll() : string.Format("{0},{1}", translateds[i], label.TrimAll());
                            }
                        }
                        else
                        {
                            argsMsg = systemMessages
                            .Where(w => w.Code.ToLower() == args[i].ToString().ToLower())
                            .FirstOrDefault();
                            if (argsMsg != null)
                                translateds[i] = argsMsg.Message;
                        }

                    }
                    return translateds;
                }
                return null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string GetCodeFromMessage(string message, params object[] args)
        {
            try
            {
                string templateMsg = message;
                if (args != null && args.Length != 0)
                {
                    for (int i = 0; i < args.Length; i++)
                    {
                        templateMsg = templateMsg.Replace(args[i].ToString(), ("{{" + i + "}}"));
                    }

                }
                StaticSysMessageData sysMessage = systemMessages.Where(item => item.Message == templateMsg).FirstOrDefault();
                if (sysMessage != null)
                {
                    return sysMessage.Code;
                }
                else
                {
                    return message;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion SysMessage

        #region SysFeatureTable
        public static void SetFrontFeatureData(IEnumerable<StaticSysFeatureTableData> list)
        {
            frontFeatures = list.ToList();
        }
        public static void SetBackFeatureData(IEnumerable<StaticSysFeatureTableData> list)
        {
            backFeatures = list.ToList();
        }
        public static void SetCashierFeatureData(IEnumerable<StaticSysFeatureTableData> list)
        {
            cashierFeatures = list.ToList();
        }
        public static void SetOBFeatureData(IEnumerable<StaticSysFeatureTableData> list)
        {
            obFeatures = list.ToList();
        }

        public static void SetOBFeatureGroupIds(IEnumerable<string> list)
        {
            obFeatureGroupIds = list.ToList();
        }
        public static List<string> GetOBFeatureGroupIds()
        {
            return obFeatureGroupIds;
        }

        public static IEnumerable<StaticSysFeatureTableData> GetFrontFeatureData()
        {
            return frontFeatures;
        }
        public static IEnumerable<StaticSysFeatureTableData> GetBackFeatureData()
        {
            return backFeatures;
        }
        public static IEnumerable<StaticSysFeatureTableData> GetCashierFeatureData()
        {
            return cashierFeatures;
        }
        public static IEnumerable<StaticSysFeatureTableData> GetOBFeatureData()
        {
            return obFeatures;
        }
        #endregion SysFeatureTable

        #region SysDuplicateDetection
        public static void SetSysDuplicateDetectionData(List<StaticSysDuplicateDetectionData> data)
        {
            systemDuplicateDetections = data;
        }
        public static IEnumerable<StaticSysDuplicateDetectionData> GetSysDuplicateDetectionData()
        {
            return systemDuplicateDetections;
        }
        #endregion SysDuplicateDetection

        #region Configuration
        public static void SetConfiguration(IConfigurationRoot config)
        {
            configuration = config;
        }
        public static IConfigurationRoot GetConfiguration()
        {
            return configuration;
        }
        #endregion Configuration

        #region Dropdown table name lookup
        public static void SetDropdownLookupData(IEnumerable<string> tableNames)
        {
            if(tableNames != null && tableNames.Count() > 0)
            {
                foreach (var table in tableNames)
                {
                    dropdownLookup.Add(table, table);
                }
            }
            #region map dropdown route to ModelName
            dropdownLookup.Add("AddressTransBy", "AddressTrans");
            dropdownLookup.Add("NotCancelApplicationTable", "ApplicationTable");
            dropdownLookup.Add("TaxBranch", "Branch");
            dropdownLookup.Add("BuyerAgeementTable", "BuyerAgreementTable");
            dropdownLookup.Add("ChequeRefPDC", "ChequeTable");
            dropdownLookup.Add("RefPDC", "ChequeTable");
            dropdownLookup.Add("ChequeTableBuyer", "ChequeTable");
            dropdownLookup.Add("ChequeTableCustomer", "ChequeTable");
            dropdownLookup.Add("ContactPerson", "ContactPersonTrans");
            dropdownLookup.Add("CreditApplicationRequestTable", "CreditAppRequestTable");
            dropdownLookup.Add("CreditApp", "CreditAppTable");
            dropdownLookup.Add("NotCancelCustBusinessCollateral", "CustBusinessCollateral");
            dropdownLookup.Add("CustomerVisitingTrans", "CustVisitingTrans");
            dropdownLookup.Add("CustomerTableBy", "CustomerTable");
            dropdownLookup.Add("DocumentStatusDocumentProcess", "DocumentStatus");
            dropdownLookup.Add("Employee", "EmployeeTable");
            dropdownLookup.Add("IntercompanyTable", "Intercompany");
            dropdownLookup.Add("OriginalInvoice", "InvoiceTable");
            dropdownLookup.Add("RefInvoiceTable", "InvoiceTable");
            dropdownLookup.Add("ProdUnitTable", "ProdUnit");
            #endregion
        }
        public static Dictionary<string, string> GetDropdownLookupData()
        {
            return dropdownLookup;
        }
        #endregion

        #region SysControllerEntityMapping
        public static void SetSysControllerEntityMappingData(List<StaticSysControllerEntityTempData> data)
        {
            sysControllerEntityTempData = data;
        }
        public static List<StaticSysControllerEntityTempData> GetSysControllerEntityMappingData()
        {
            return sysControllerEntityTempData;
        }
        #endregion SysControllerEntityMapping

        #region model type lookup
        public static void SetModelTypeLookupData(Dictionary<string, StaticModelTypeLookupData> data)
        {
            modelTypeLookup = data;
        }
        public static Dictionary<string, StaticModelTypeLookupData> GetModelTypeLookupData()
        {
            return modelTypeLookup;
        }
        public static Type GetModelTypeByModelName(string key)
        {
            if(modelTypeLookup != null && modelTypeLookup.Count > 0)
            {
                var tryGet = modelTypeLookup.TryGetValue(key, out StaticModelTypeLookupData result);
                return tryGet ? result.ModelType : null;
            }
            return null;
        }
        public static int GetModelRefTypeByModelName(string key)
        {
            if (modelTypeLookup != null && modelTypeLookup.Count > 0)
            {
                var tryGet = modelTypeLookup.TryGetValue(key, out StaticModelTypeLookupData result);
                return tryGet ? result.RefType : -1;
            }
            return -1;
        }
        #endregion model type lookup
    }


    public class StaticSysFeatureTableData
    {
        public Guid SysFeatureTableGUID { get; set; }
        public string ParentFeatureId { get; set; }
        public string FeatureId { get; set; }
        public string Path { get; set; }
    }
    public class StaticSysMessageData
    {
        public string Code;
        public string Message;
    }

    public class StaticSysDuplicateDetectionData
    {
        public string ModelName;
        public int RuleNum;
        public string PropertyName;
        public string LabelName;
        public bool System;
        public bool InActive;
    }
    public class StaticSysControllerEntityTempData
    {
        public string ModelName { get; set; }
        public string SysControllerTable_RouteAttribute { get; set; }
        public string SysControllerTable_ControllerName { get; set; }
    }
    public class StaticModelTypeLookupData
    {
        public Type ModelType { get; set; }
        public int RefType { get; set; }
    }
}
