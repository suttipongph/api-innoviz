﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.Core.ViewModelHandler
{
    public static class FileHandler
    {
        #region CRUD File (Attachment, DocumentTemplate)
        private static string[] GetFileNameDescription<T>(T model)
        {
            try
            {
                string fileName, fileDescription;
                Type type = model.GetType();
                if(model.GetType().Name == FileConst.DocumentTemplateTable ||
                    model.GetType().Name == FileConst.DocumentTemplateTableItemViewMap ||
                    model.GetType().Name == FileConst.DocumentTemplateTableItemView)
                {
                    var fileNameProp = type.GetProperty(FileConst.FileName);
                    var descriptionProp = type.GetProperty(DocumentTemplateCondition.Description);
                    fileName = fileNameProp.GetValue(model)?.ToString();
                    fileName = string.IsNullOrWhiteSpace(fileName) ? null : fileName + FileEXT.DOCX_EXTENSION;
                    fileDescription = descriptionProp.GetValue(model)?.ToString();
                }
                else
                {
                    var fileNameProp = type.GetProperty(FileConst.FileName);
                    var fileDescriptionProp = type.GetProperty(AttachmentCondition.FileDescription);
                    fileName = fileNameProp.GetValue(model)?.ToString();
                    fileDescription = fileDescriptionProp.GetValue(model)?.ToString();
                }

                return new string[] { fileName, fileDescription };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string GetFileRootPath(Type modelType)
        {
            try
            {
                string storageConfigKey = (modelType.Name == FileConst.DocumentTemplateTable ||
                    modelType.Name == FileConst.DocumentTemplateTableItemViewMap ||
                    modelType.Name == FileConst.DocumentTemplateTableItemView) ? 
                    FileConst.DocumentTemplateStorage : FileConst.FileStorage;
                string storageConfigValue = storageConfigKey.GetConfigurationValue();
                if(!string.IsNullOrWhiteSpace(storageConfigValue) && storageConfigValue == FileConst.DATABASE)
                {
                    return null;
                }
                else
                {
                    string fileRootPath = FileConst.UploadRoot.GetConfigurationValue();
                    if (string.IsNullOrWhiteSpace(fileRootPath))
                    {
                        fileRootPath = WebHostDefaults.ContentRootKey.GetConfigurationValue();
                    }
                    return fileRootPath;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static T EnsureFileCreated<T>(this T model)
        {
            try
            {
                var HasFilPath = typeof(T).GetProperty(FileConst.FilePath) != null;
                var HasFileName = typeof(T).GetProperty(FileConst.FileName) != null;
                var HasBase64Data = typeof(T).GetProperty(FileConst.Base64Data) != null;

                if (HasFileName && HasFilPath && HasBase64Data)
                {
                    var filePathProp = typeof(T).GetProperty(FileConst.FilePath);

                    string fileRootPath = GetFileRootPath(typeof(T));
                    string filePath = filePathProp.GetValue(model)?.ToString();
                    if (!string.IsNullOrWhiteSpace(fileRootPath))
                    {
                        var base64Prop = typeof(T).GetProperty(FileConst.Base64Data);
                        string base64String = base64Prop.GetValue(model)?.ToString();
                        CreateFile(fileRootPath, filePath, base64String);
                        base64Prop.SetValue(model, null);
                        return model;
                    }
                    else
                    {
                        filePathProp.SetValue(model, null);
                        return model;
                    }
                }
                else
                {
                    return model;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static T EnsureFileUpdated<T>(this T model)
        {
            try
            {
                var HasFilPath = typeof(T).GetProperty(FileConst.FilePath) != null;
                var HasFileName = typeof(T).GetProperty(FileConst.FileName) != null;
                var HasBase64Data = typeof(T).GetProperty(FileConst.Base64Data) != null;


                if (HasFileName && HasFilPath && HasBase64Data)
                {
                    var filePathProp = typeof(T).GetProperty(FileConst.FilePath);

                    string fileRootPath = GetFileRootPath(typeof(T));
                    string filePath = filePathProp.GetValue(model)?.ToString();
                    if (!string.IsNullOrWhiteSpace(fileRootPath))
                    {
                        var base64Prop = typeof(T).GetProperty(FileConst.Base64Data);
                        string base64String = base64Prop.GetValue(model)?.ToString();
                        CreateFile(fileRootPath, filePath, base64String);
                        base64Prop.SetValue(model, null);
                        return model;
                    }
                    else
                    {
                        filePathProp.SetValue(model, null);
                        return model;
                    }
                }
                else
                {
                    return model;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static bool EnsureFileDeleted<T>(this T model)
        {
            try
            {
                var HasFilPath = typeof(T).GetProperty(FileConst.FilePath) != null;
                var HasFileName = typeof(T).GetProperty(FileConst.FileName) != null;
                var HasBase64Data = typeof(T).GetProperty(FileConst.Base64Data) != null;

                if (HasFileName && HasFilPath && HasBase64Data)
                {
                    string fileRootPath = GetFileRootPath(typeof(T));
                    var filePathProp = typeof(T).GetProperty(FileConst.FilePath);
                    string filePath = filePathProp.GetValue(model)?.ToString();
                    if (!string.IsNullOrWhiteSpace(fileRootPath))
                    {
                        string path = Path.Combine(fileRootPath, filePath);
                        File.Delete(path);
                    }
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static bool DeleteFiles(this FileUpload fileUpload)
        {
            try
            {
                if (fileUpload != null)
                {
                    DeleteFiles(fileUpload.FileInfos);
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static bool DeleteFiles(this List<FileInformation> files)
        {
            try
            {
                if (files != null && files.Count() > 0)
                {
                    foreach (var file in files)
                    {
                        File.Delete(file.Path);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static async Task<T> GetFileContent<T>(this T model)
        {
            try
            {
                var HasFilPath = typeof(T).GetProperty(FileConst.FilePath) != null;
                var HasFileName = typeof(T).GetProperty(FileConst.FileName) != null;
                var HasBase64Data = typeof(T).GetProperty(FileConst.Base64Data) != null;

                if (HasFileName && HasFilPath && HasBase64Data)
                {
                    string fileRootPath = GetFileRootPath(typeof(T));
                    var filePathProp = typeof(T).GetProperty(FileConst.FilePath);
                    string filePath = filePathProp.GetValue(model)?.ToString();

                    if (!string.IsNullOrWhiteSpace(fileRootPath))
                    {
                        string path = Path.Combine(fileRootPath, filePath);
                        if (!File.Exists(path))
                        {
                            var fileNameDescription = GetFileNameDescription(model);
                            SmartAppException ex = new SmartAppException("ERROR.00152");
                            ex.AddData("ERROR.00365", fileNameDescription[0], fileNameDescription[1]);
                            throw ex;
                        }
                        else
                        {
                            var base64Prop = typeof(T).GetProperty(FileConst.Base64Data);
                            Byte[] bytes = await File.ReadAllBytesAsync(path);
                            var base64 = Convert.ToBase64String(bytes);
                            base64Prop.SetValue(model, base64);
                            return model;
                        }
                    }
                    else
                    {
                        return model;
                    }
                }
                else
                {
                    return model;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static async Task<FileInformation> GetFileContent(this FileInformation file)
        {
            try
            {
                if(file == null || string.IsNullOrWhiteSpace(file.Path))
                {
                    return null;
                }

                if(!File.Exists(file.Path))
                {
                    throw new FileNotFoundException("File not found: " + file.Path);
                }
                else
                {
                    Byte[] bytes = await File.ReadAllBytesAsync(file.Path);
                    file.Base64 = Convert.ToBase64String(bytes);
                    if(file.Path.Contains("/"))
                    {
                        file.FileName = file.Path.Split("/").LastOrDefault();
                    }
                    else if(file.Path.Contains("\\"))
                    {
                        file.FileName = file.Path.Split("\\").LastOrDefault();
                    }
                    file.ContentType = GetMimeHeaderFromExtension(file.FileName.Split(".").LastOrDefault());
                    return file;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static FileInformation CreateFile(string rootPath, string fileName, string base64Data)
        {
            try
            {
                if(!string.IsNullOrWhiteSpace(base64Data))
                {
                    var base64string = base64Data.Split(",")?.Last();
                    string filePath = Path.Combine(rootPath, fileName);
                    Byte[] bytes = Convert.FromBase64String(base64string);
                    System.IO.Directory.CreateDirectory(rootPath);
                    File.WriteAllBytes(filePath, bytes);
                    return new FileInformation { Base64 = base64Data, FileName = fileName, Path = filePath };
                }
                return null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static FileInformation CreateFile(this FileInformation file, string path)
        {
            try
            {
                string base64Data = file.Base64;
                if (file != null && !string.IsNullOrWhiteSpace(base64Data))
                {
                    file = CreateFile(path, file.FileName, file.Base64);
                    return file;
                }
                return null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string GetMimeHeaderFromExtension(string ext)
        {
            try
            {
                string inputExt = "." + ext.Replace(".", "");
                switch(inputExt)
                {
                    case FileEXT.TXT_EXTENSION: return MimeHeader.TXT_MIME_HEADER;
                    case FileEXT.PDF_EXTENSION: return MimeHeader.PDF_MIME_HEADER;
                    case FileEXT.DOC_EXTENSION: return MimeHeader.DOC_MIME_HEADER;
                    case FileEXT.DOCX_EXTENSION: return MimeHeader.DOCX_MIME_HEADER;
                    case FileEXT.XLS_EXTENSION: return MimeHeader.XLS_MIME_HEADER;
                    case FileEXT.XLSX_EXTENSION: return MimeHeader.XLSX_MIME_HEADER;
                    case FileEXT.PNG_EXTENSION: return MimeHeader.PNG_MIME_HEADER;
                    case FileEXT.JPG_EXTENSION: return MimeHeader.JPG_MIME_HEADER;
                    case FileEXT.JPEG_EXTENSION: return MimeHeader.JPEG_MIME_HEADER;
                    case FileEXT.GIF_EXTENSION: return MimeHeader.GIF_MIME_HEADER;
                    case FileEXT.TIFF_EXTENSION: return MimeHeader.TIFF_MIME_HEADER;
                    case FileEXT.BMP_EXTENSION: return MimeHeader.BMP_MIME_HEADER;
                    case FileEXT.RAW_EXTENSION: return MimeHeader.RAW_MIME_HEADER;
                    case FileEXT.CSV_EXTENSION: return MimeHeader.CSV_MIME_HEADER;
                    case FileEXT.RAR_EXTENSION: return MimeHeader.RAR_MIME_HEADER;
                    case FileEXT.ZIP_EXTENSION: return MimeHeader.ZIP_MIME_HEADER;
                    case FileEXT.SEVENZIP_EXTENSION: return MimeHeader.SEVENZIP_MIME_HEADER;
                    case FileEXT.PPT_EXTENSION: return MimeHeader.PPT_MIME_HEADER;
                    case FileEXT.PPTX_EXTENSION: return MimeHeader.PPTX_MIME_HEADER;
                    
                    default: return MimeHeader.OCTET_STREAM_MIME_HEADER;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region import excel
        
        public static List<T> MapExcelDataToSingleEntityType<T>(this List<FileInformation> files, IReadOnlyDictionary<string, string> mapper) where T : new()
        {
            try
            {
                var result = new List<T>();
                FileHelper helper = new FileHelper();
                foreach (FileInformation _file in files)
                {
                    FileInfo file = new FileInfo(_file.Path);
                    ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.Commercial; // EPPlus 5 and above
                    using (ExcelPackage package = new ExcelPackage(file))
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
                        var workSheetResult = helper.MapExcelSheetToFileImportResult<T>(workSheet, mapper);
                        result.AddRange(workSheetResult.ModelResult);
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
    }
}
