﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Innoviz.SmartApp.Core.ViewModelHandler
{
    public static class DataTypeHandler
    {
        #region standard
        public static string GetConfigurationValue(this string key)
        {
            try
            {
                string result = string.Empty;
                IConfigurationRoot configuration = SystemStaticData.GetConfiguration();
                result = configuration.GetValue<string>(key);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static List<string> GetConfigurationValues(this string key)
        {
            try
            {
                string result = string.Empty;
                IConfigurationRoot configuration = SystemStaticData.GetConfiguration();
                IConfigurationSection section = configuration.GetSection(key);
                return section.GetChildren().ToList().Select(a => a.Value).ToList();
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string DateToString(this DateTime date)
        {
            try
            {
                string format = "AppSetting:DateFormat".GetConfigurationValue();
                string result = date.ToString(format, CultureInfo.InvariantCulture);
                return result;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string DateToString(this object date)
        {
            try
            {
                string format = "AppSetting:DateFormat".GetConfigurationValue();
                string result = date == null ? null : ((DateTime)date).ToString(format, CultureInfo.InvariantCulture);
                return result;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string DateTimeToString(this DateTime date)
        {
            try
            {
                string format = "AppSetting:DateTimeFormat".GetConfigurationValue();
                string result = date.ToString(format, CultureInfo.InvariantCulture);
                return result;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string DateTimeToString(this object date)
        {
            try
            {
                string format = "AppSetting:DateTimeFormat".GetConfigurationValue();
                string result = date == null ? null : ((DateTime)date).ToString(format, CultureInfo.InvariantCulture);
                return result;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static DateTime StringToDate(this string str)
        {
            try
            {
                string format = "AppSetting:DateFormat".GetConfigurationValue();
                DateTime result = DateTime.ParseExact(str, format, CultureInfo.InvariantCulture);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static DateTime StringToDateTime(this string str)
        {
            try
            {
                string format = "AppSetting:DateTimeFormat".GetConfigurationValue();
                DateTime result = DateTime.ParseExact(str, format, CultureInfo.InvariantCulture);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static DateTime? StringNullToDateNull(this string str)
        {
            try
            {
                string format = "AppSetting:DateFormat".GetConfigurationValue();
                if (str != null)
                {
                    DateTime? result = DateTime.ParseExact(str, format, CultureInfo.InvariantCulture);
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static DateTime StringToSystemDateTime(this string str)
        {
            try
            {
                string format = "AppSetting:DateTimeFormat".GetConfigurationValue();
                if (str != null)
                {
                    DateTime result = DateTime.ParseExact(str, format, CultureInfo.InvariantCulture);
                    return result;
                }
                return DateTime.MinValue;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string DateNullToString(this DateTime? date)
        {
            try
            {
                string format = "AppSetting:DateFormat".GetConfigurationValue();
                return date.HasValue ? date.Value.ToString(format, CultureInfo.InvariantCulture) : null;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string DateTimeNullToString(this DateTime? date)
        {
            try
            {
                string format = "AppSetting:DateTimeFormat".GetConfigurationValue();
                return date.HasValue ? date.Value.ToString(format, CultureInfo.InvariantCulture) : null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string DateTimeToTimeString(this DateTime date)
        {
            try
            {
                string result = string.Format("{0:00}:{1:00}", date.Hour, date.Minute);
                return result;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static DateTime TimeStringToDateTime(this string str)
        {
            try
            {
                string format = "AppSetting:DateTimeFormat".GetConfigurationValue();
                DateTime date = DateTime.Now;
                DateTime result = new DateTime(date.Year, date.Month, date.Day, int.Parse(str.Split(':')[0]), int.Parse(str.Split(':')[1]), 0);
                return result;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string DateTimeNullToTimeString(this DateTime? date)
        {
            try
            {
                if (date.HasValue)
                {
                    DateTime dateTime = (DateTime)date;
                    string result = string.Format("{0:00}:{1:00}", dateTime.Hour, dateTime.Minute);
                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static DateTime? TimeStringToDateTimeNull(this string str)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    string format = "AppSetting:DateTimeFormat".GetConfigurationValue();
                    DateTime date = DateTime.Now;
                    DateTime result = new DateTime(date.Year, date.Month, date.Day, int.Parse(str.Split(':')[0]), int.Parse(str.Split(':')[1]), 0);
                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string ToNullString(this object obj)
        {
            try
            {
                if (obj == null)
                {
                    return null;
                }
                else
                {
                    return obj.ToString();
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static Guid StringToGuid(this string str)
        {
            try
            {
                if (str == null || str == "" || str == "null")
                {
                    return Guid.Empty;
                }
                else
                {
                    return new Guid(str);
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static Guid? StringToGuidNull(this string str)
        {
            try
            {
                if (str == null || str == "" || str == "null")
                {
                    return null;
                }
                else
                {
                    return new Guid(str);
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string GuidNullToString(this object obj)
        {
            try
            {
                if (obj == null)
                {
                    return null;
                }
                else
                {
                    return obj.ToString();
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string GuidNullToString(this Guid? guid)
        {
            try
            {
                if (guid == null)
                {
                    return null;
                }
                else
                {
                    return guid.ToString().ToLower();
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string ToLowerString(this object guid)
        {
            try
            {
                if (guid == null)
                {
                    return null;
                }
                else
                {
                    return guid.ToString().ToLower();
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string ListStringToString(this List<string> strs)
        {
            try
            {
                string result = string.Empty;
                foreach (var str in strs)
                {
                    result = string.Format("{0},'{1}'", result, str);
                }
                return result.TrimStart(',');
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string ListDateToString(this List<string> strs)
        {
            try
            {
                string result = string.Format(" '{0}' AND '{1}' ", strs[0].GetDateString(), strs[1].GetDateString());
                return result;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static bool UnCountOrNull<T>(this List<T> list)
        {
            try
            {
                if (list == null) return true;
                else if (list.Count() == 0) return true;
                else return false;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string GetDateString(this string str)
        {
            try
            {
                string[] strArr = str.Split('/');
                return string.Format("{0}-{1}-{2}", strArr[2], strArr[1], strArr[0]);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static T vwToModel<Tvw, T>(this Tvw view) where T : new()
        {
            try
            {
                PropertyInfo[] propertyInfos;
                PropertyInfo[] propertyInfosVW;
                T result = new T();
                propertyInfos = typeof(T).GetProperties();
                propertyInfosVW = typeof(Tvw).GetProperties();
                foreach (PropertyInfo info in propertyInfos)
                {
                    foreach (PropertyInfo viewInfo in propertyInfosVW)
                    {
                        if (info.Name.ToUpper() == viewInfo.Name.ToUpper())
                        {
                            string type = info.PropertyType.FullName.ToUpper();

                            if (type == SystemType.STRING.ToUpper())
                            {
                                info.SetValue(result, viewInfo.GetValue(view));
                            }
                            else if (type == SystemType.INT.ToUpper())
                            {
                                info.SetValue(result, Int32.Parse(viewInfo.GetValue(view).ToString()));
                            }
                            else if (type == SystemType.INT32.ToUpper())
                            {
                                info.SetValue(result, Int32.Parse(viewInfo.GetValue(view).ToString()));
                            }
                            else if (type == SystemType.BOOLEAN.ToUpper())
                            {
                                info.SetValue(result, viewInfo.GetValue(view).ToString().ToUpper() == "true".ToUpper() ? true : false);
                            }
                            else
                            {
                                info.SetValue(result, viewInfo.GetValue(view));
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public static List<Tvw> ToMaps<Tmap, Tvw>(this IQueryable<Tmap> mapModels)
            where Tmap : IViewEntityMap
            where Tvw : IViewEntity
        {
            try
            {
                List<Tvw> result = new List<Tvw>();
                foreach (var map in mapModels)
                {
                    result.Add(map.ToMap<Tmap, Tvw>());
                }
                return result;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static Tvw ToMap<Tmap, Tvw>(this Tmap mapModel)
            where Tmap : IViewEntityMap
            where Tvw : IViewEntity
        {
            try
            {
                if (mapModel == null)
                {
                    return default(Tvw);
                }
                Tvw result = (Tvw)Activator.CreateInstance(typeof(Tvw));
                PropertyInfo[] propertyInfosVW = typeof(Tvw).GetProperties();
                PropertyInfo[] propertyInfosMap = typeof(Tmap).GetProperties();
                foreach (PropertyInfo viewInfo in propertyInfosVW)
                {
                    foreach (PropertyInfo mapInfo in propertyInfosMap)
                    {
                        if (mapInfo.Name.ToUpper() == viewInfo.Name.ToUpper())
                        {

                            string viewType = viewInfo.PropertyType.FullName.ToUpper();
                            string mapType = mapInfo.PropertyType.FullName.ToUpper();
                            //SELECTITEM
                            bool nullAble = mapType.Contains(SystemType.NULLABLE.ToUpper());
                            object value = mapInfo.GetValue(mapModel);

                            if (mapType.ToUpper().Contains(SystemType.STRING.ToUpper()))
                            {
                                viewInfo.SetValue(result, value ?? null);
                            }
                            else if (mapType.ToUpper().Contains(SystemType.INT.ToUpper()))
                            {
                                if (nullAble)
                                {
                                    viewInfo.SetValue(result, (value != null) ? (int?)Int32.Parse(value.ToString()) : null);
                                }
                                else
                                {
                                    viewInfo.SetValue(result, Int32.Parse(value.ToString()));
                                }
                            }
                            else if (mapType.ToUpper().Contains(SystemType.INT32.ToUpper()))
                            {
                                viewInfo.SetValue(result, Int32.Parse(value.ToString()));
                            }
                            else if (mapType.ToUpper().Contains(SystemType.BOOLEAN.ToUpper()))
                            {
                                viewInfo.SetValue(result, value.ToString().ToUpper() == "true".ToUpper() ? true : false);
                            }
                            else if (mapType.ToUpper().Contains(SystemType.GUID.ToUpper()))
                            {
                                if (nullAble)
                                {
                                    viewInfo.SetValue(result, (value != null) ? value.ToLowerString() : null);
                                }
                                else
                                {
                                    viewInfo.SetValue(result, value.ToLowerString());
                                }

                            }
                            else if (mapType.ToUpper().Contains(SystemType.DATE.ToUpper()))
                            {
                                bool dateTimeOrTimeStamp = viewInfo.Name.ToUpper().Contains("DATETIME") ||
                                                            viewInfo.Name.ToUpper().Contains("TIMESTAMP");
                                if (dateTimeOrTimeStamp || viewInfo.Name.ToUpper().Contains("TIME"))
                                {
                                    if (dateTimeOrTimeStamp)
                                    {
                                        if (nullAble)
                                        {
                                            viewInfo.SetValue(result, value != null ? value.DateTimeToString() : null);
                                        }
                                        else
                                        {
                                            viewInfo.SetValue(result, value.DateTimeToString());
                                        }
                                    }
                                    else
                                    {
                                        if (nullAble)
                                        {
                                            viewInfo.SetValue(result, value != null ? ((DateTime)value).DateTimeToTimeString() : null);
                                        }
                                        else
                                        {
                                            viewInfo.SetValue(result, ((DateTime)value).DateTimeToTimeString());
                                        }
                                    }
                                }
                                else
                                {
                                    if (nullAble)
                                    {
                                        viewInfo.SetValue(result, value != null ? value.DateToString() : null);
                                    }
                                    else
                                    {
                                        viewInfo.SetValue(result, value.DateToString());
                                    }
                                }
                            }
                            else if (mapType.ToUpper().Contains(SystemType.DECIMAL.ToUpper()))
                            {
                                viewInfo.SetValue(result, Decimal.Parse(value.ToString()));
                            }
                            else if (mapType.ToUpper().Contains(SystemType.SELECTITEM.ToUpper()))
                            {
                                viewInfo.SetValue(result, value);
                            }
                            else if (mapType.ToUpper().Contains(SystemType.BYTEARRAY.ToUpper()))
                            {
                                viewInfo.SetValue(result, value);
                            }
                            else
                            {
                                viewInfo.SetValue(result, value.ToLowerString());
                            }
                        }
                    }

                    if (viewInfo.Name.Contains("RowAuthorize"))
                    {
                        viewInfo.SetValue(result, AccessMode.Full.GetAttrValue());
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        
        public static Tvw MergeModel<Tdb, Tvw>(this Tdb model, Tvw view)
        {
            try
            {
                //Tvw result = (Tvw)Activator.CreateInstance(typeof(Tvw));
                PropertyInfo[] propertyInfosDB = typeof(Tdb).GetProperties();
                PropertyInfo[] propertyInfosVw = typeof(Tvw).GetProperties();
                foreach (PropertyInfo viewInfo in propertyInfosVw)
                {
                    foreach (PropertyInfo dbInfo in propertyInfosDB)
                    {
                        if (dbInfo.Name.ToUpper() == viewInfo.Name.ToUpper())
                        {
                            string viewType = viewInfo.PropertyType.FullName.ToUpper();
                            string dbType = dbInfo.PropertyType.FullName.ToUpper();
                            bool nullAble = dbType.Contains(SystemType.NULLABLE.ToUpper());

                            if (dbType.ToUpper().Contains(SystemType.STRING.ToUpper()))
                            {
                                viewInfo.SetValue(view, dbInfo.GetValue(model) ?? null);
                            }
                            else if (dbType.ToUpper().Contains(SystemType.INT.ToUpper()))
                            {
                                viewInfo.SetValue(view, dbInfo.GetValue(model));
                            }
                            else if (dbType.ToUpper().Contains(SystemType.INT32.ToUpper()))
                            {
                                viewInfo.SetValue(view, dbInfo.GetValue(model));
                            }
                            else if (dbType.ToUpper().Contains(SystemType.BOOLEAN.ToUpper()))
                            {
                                viewInfo.SetValue(view, dbInfo.GetValue(model));
                            }
                            else if (dbType.ToUpper().Contains(SystemType.GUID.ToUpper()))
                            {
                                if (nullAble)
                                {
                                    viewInfo.SetValue(view, dbInfo.GetValue(model).ToLowerString());
                                }
                                else
                                {
                                    viewInfo.SetValue(view, dbInfo.GetValue(model).ToLowerString());
                                }

                            }
                            else if (dbType.ToUpper().Contains(SystemType.DATE.ToUpper()))
                            {
                                if (viewInfo.Name.ToUpper().Contains("DATETIME"))
                                {
                                    viewInfo.SetValue(view, dbInfo.GetValue(model).DateTimeToString());
                                }
                                else
                                {
                                    viewInfo.SetValue(view, dbInfo.GetValue(model).DateToString());
                                }
                            }
                            else if (dbType.ToUpper().Contains(SystemType.DECIMAL.ToUpper()))
                            {
                                viewInfo.SetValue(view, dbInfo.GetValue(model));
                            }
                            else
                            {
                                viewInfo.SetValue(view, dbInfo.GetValue(model));
                            }
                        }
                    }
                }
                return view;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static bool isNull(this string str)
        {
            try
            {
                if (string.IsNullOrEmpty(str) || str == "null")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static int GetPaginatorPages(this int rows, int pages)
        {
            try
            {
                return rows == (int)SearchingType.avoid ? (int)SearchingType.initial : pages;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static int GetPaginatorRows(this int rows, int total)
        {
            try
            {
                return rows == (int)SearchingType.avoid ? total == 0 ? 1 : total : rows;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string TrimAll(this string str)
        {
            return Regex.Replace(str, @"\s+", "");
        }
        public static T MapUpdateValues<T>(this T dbVal, T inputVal, List<string> skipFields = null) where T : class
        {
            try
            {
                skipFields = skipFields == null ? new List<string>() : skipFields;
                //skipFields.Add(TextConstants.RowVersion);
                skipFields.Add(TextConstants.CreatedBy);
                skipFields.Add(TextConstants.CreatedDateTime);
                var props = typeof(T).GetProperties();
                var propsToCheck = props.Where(w => w.GetCustomAttribute<InversePropertyAttribute>() == null &&
                                w.GetCustomAttribute<ForeignKeyAttribute>() == null &&
                                !skipFields.Any(a => a == w.Name));
                foreach (var prop in propsToCheck)
                {
                    var inputPropVal = prop.GetValue(inputVal);
                    prop.SetValue(dbVal, inputPropVal);
                }
                return dbVal;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string LabelToString(this string input)
        {
            try
            {
                if (input == null)
                {
                    return null;
                }
                input = input.ToLower();
                var splits = input.Split("_");
                splits[0] = char.ToUpper(splits[0][0]) + splits[0].Substring(1);
                return string.Join(" ", splits);

            }
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static IQueryable<T> FilterByAccessLevel<T>(this IQueryable<T> items, AccessLevelParm accessLevelParm)
        {
            try
            {
                var HasCompany = typeof(T).BaseType.GetProperty(TextConstants.CompanyGUID) != null ? true : false;
                var HasOwner = typeof(T).BaseType.GetProperty(TextConstants.Owner) != null;
                var HasOwnerBusinessUnitGUID = typeof(T).BaseType.GetProperty(TextConstants.OwnerBusinessUnitGUID) != null;
                if (HasCompany && (HasOwner && HasOwnerBusinessUnitGUID))
                {
                    accessLevelParm.AccessLevel = (int)AccessLevel.Company;
                    var accessLevelCond = SysAccessLevelHelper.GetFilterCondByAccessLevel(accessLevelParm);
                    if (accessLevelCond != null)
                    {
                        List<SearchCondition> searchConds = new List<SearchCondition>();
                        searchConds.Add(accessLevelCond);
                        SearchParameter search = new SearchParameter
                        {
                            Conditions = searchConds
                        };
                        //var predicate = search.GetSearchPredicate(typeof(T), true);
                        var predicate = search.GetSearchPredicate(typeof(T), false);

                        items = items.Where(predicate.Predicates, predicate.Values);
                    }
                }
                CheckDataNotNullOrEmpty(items);
                return items;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static T CheckDataNotNull<T>(this T item)
        {
            try
            {
                if (item != null)
                {
                    return item;
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00851");
                    throw ex;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static IQueryable<T> CheckDataNotNullOrEmpty<T>(this IQueryable<T> items)
        {
            try
            {
                if (items == null || items.Count() == 0)
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00851");
                    throw ex;
                }
                else
                {
                    return items;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string PascalToSnakeCase(this string str, bool toUpperCase = true)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(str)) return str;

                string strRegex = @"(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|(?<=[0-9])(?=[A-Z][a-z])|(?<=[a-zA-Z])(?=[0-9])";
                Regex myRegex = new Regex(strRegex, RegexOptions.Multiline);
                string result = Regex.Replace(str, strRegex, m => "_" + m.Value);
                return toUpperCase ? result.ToUpper() : result.ToLower(); 
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string SnakeCaseToPascal(this string str)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(str))
                {
                    return str;
                }
                else
                {
                    // not snake case
                    if (!str.Contains("_"))
                    {
                        return str;
                    }
                    else
                    {
                        string[] splits = str.ToLower().Split("_");
                        string result = string.Join("", splits.Select(s => s.ConvertFirstLetterToUpperCase()));
                        return result;
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string ConvertFirstLetterToUpperCase(this string str)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(str))
                {
                    return str;
                }
                else
                {
                    var array = str.ToCharArray();
                    array[0] = char.ToUpper(array[0]);
                    return new string(array);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static bool IsSnakeUpperCase(this string str)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(str))
                {
                    return false;
                }
                else
                {
                    if ((str.Contains("_") && str.ToUpper() == str) || str.ToUpper() == str)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static Dictionary<string, string> GetEntityMapper<T>(this T entity)
        {
            try
            {
                return GetEntityMapper<T>();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static Dictionary<string, string> GetEntityMapper<T>()
        {
            try
            {
                Dictionary<string, string> result = null;
                // try invoke method GetMapper()
                var mi = typeof(T).GetMethod("GetMapper");
                if (mi != null)
                {
                    var obj = mi.Invoke(null, null);
                    result = obj != null ? (Dictionary<string, string>)obj : null;
                }
                else
                {
                    PropertyInfo[] props = typeof(T).GetProperties();
                    result = GetEntityMapper(props);
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static Dictionary<string, string> GetEntityMapper(this PropertyInfo[] props)
        {
            try
            {
                Dictionary<string, string> result = null;
                if (props != null && props.Length > 0)
                {
                    var propsToMap = props.Where(w => w.GetCustomAttribute<InversePropertyAttribute>() == null &&
                                                    w.GetCustomAttribute<ForeignKeyAttribute>() == null);
                    result = propsToMap.ToDictionary<PropertyInfo, string, string>(key => key.Name, val => val.Name);
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string ToSQLDateString(this DateTime? dateTime)
        {
            try
            {
                string format = "yyyy-MM-dd";
                return dateTime.HasValue ? dateTime.Value.ToString(format, CultureInfo.InvariantCulture) : null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string ToSQLDateString(this DateTime dateTime)
        {
            try
            {
                string format = "yyyy-MM-dd";
                return dateTime.ToString(format, CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static DateTime StringSQLToDate(this string str)
        {
            try
            {
                string format = "yyyy-MM-dd";
                DateTime result = DateTime.ParseExact(str, format, CultureInfo.InvariantCulture);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static DateTime? StringSQLToDateNull(this string str)
        {
            try
            {
                if (str != null)
                {
                    string format = "yyyy-MM-dd";
                    DateTime result = DateTime.ParseExact(str, format, CultureInfo.InvariantCulture);
                    return result;
                }
                return null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static DateTime GetEndOfMonth(this DateTime dateTime)
        {
            try
            {
                int lastDayOfMonth = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
                return new DateTime(dateTime.Year, dateTime.Month, lastDayOfMonth);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static int GetPropStringLength(this PropertyInfo prop)
        {
            try
            {
                if (prop.PropertyType != typeof(string) || prop.PropertyType != typeof(String)) return 0;
                
                int stringLength = 0;
                var strAttr = prop.GetCustomAttributesData().Where(w => w.AttributeType == typeof(StringLengthAttribute)).FirstOrDefault();
                if (strAttr != null)
                {
                    stringLength = (int)strAttr.ConstructorArguments.First().Value;
                }

                return stringLength;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static int GetPropStringLength<T>(this string fieldName)
        {
            try
            {
                int stringLength = 0;
                PropertyInfo[] propertyInfos = typeof(T).GetProperties();
                foreach (PropertyInfo info in propertyInfos)
                {
                    if (info.Name.ToUpper() == fieldName.ToUpper())
                    {
                        stringLength = info.GetPropStringLength();
                    }
                }
                return stringLength;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static bool IsPropRequired(this PropertyInfo prop)
        {
            try
            {
                Type propType = prop.PropertyType;
                bool isValueType = propType.IsValueType;
                bool isNullableValueType = propType == typeof(Guid?) || propType == typeof(bool?) || propType == typeof(int?) ||
                                            propType == typeof(decimal?) || propType == typeof(DateTime?) || propType == typeof(string);
                bool hasRequiredAttribute = prop.GetCustomAttribute<RequiredAttribute>() != null;
                return hasRequiredAttribute || (isValueType && !isNullableValueType);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static bool IsPropRequired<T>(this string fieldName)
        {
            try
            {
                PropertyInfo[] propertyInfos = typeof(T).GetProperties();
                foreach (PropertyInfo info in propertyInfos)
                {
                    if (info.Name.ToUpper() == fieldName.ToUpper())
                    {
                        return info.IsPropRequired();
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static IDictionary AddMsgCode(this IDictionary dict, string code, dynamic value)
        {
            try
            {
                if(string.IsNullOrWhiteSpace(code))
                {
                    return dict;
                }
                if(value == null)
                {
                    return dict;
                }
                string errorCodeLower = code.ToLower();
                string errorCodeUpper = code.ToUpper();
                
                if (dict.Contains($"{errorCodeLower}.0"))
                { // more than 1 errorCode exist
                    int keyCount = 0;
                    foreach (var item in dict.Keys)
                    {
                        if (((string)item).Contains($"{errorCodeLower}."))
                        {
                            keyCount++;
                        }
                    }

                    string newKey = $"{errorCodeLower}.{keyCount}";
                    
                    if(typeof(string) == value.GetType() && (string)value.ToLower() == errorCodeLower)
                    {
                        dict.Add(newKey, newKey.ToUpper());
                    }
                    else
                    {
                        dict.Add(newKey, value);
                    }
                }
                else if (dict.Contains($"{errorCodeLower}"))
                { // errorCode already exists
                    var origVal = dict[errorCodeLower];

                    if(origVal != null)
                    {
                        dict.Remove(errorCodeLower);
                    }

                    string key1 = $"{errorCodeLower}.0";
                    string key2 = $"{errorCodeLower}.1";

                    if (typeof(string) == value.GetType() && (string)value.ToLower() == errorCodeLower)
                    {
                        dict.Add(key1, key1.ToUpper());
                        dict.Add(key2, key2.ToUpper());
                    }
                    else
                    {
                        dict.Add(key1, origVal);
                        dict.Add(key2, value);
                    }
                }
                else
                { // no errorCode exists in Data
                    dict.Add(errorCodeLower, value);
                }
                return dict;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static PropertyInfo GetSingleKeyAttributeProperty(this Type type)
        {
            try
            {
                var props = type.GetProperties();
                foreach (var prop in props)
                {
                    var attribute = Attribute.GetCustomAttribute(prop, typeof(KeyAttribute))
                        as KeyAttribute;
                    if (attribute != null)
                    {
                        return prop;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region leaseIt
        public static decimal Round(this decimal dec, int digit = 2)
        {
            try
            {
                return Math.Round(dec, digit, MidpointRounding.AwayFromZero);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static decimal RoundUp(this decimal dec, int ceiling = 1)
        {
            try
            {
                return Math.Round(Math.Ceiling(dec / ceiling), MidpointRounding.AwayFromZero) * ceiling;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static decimal RoundDown(this decimal dec, int floor = 1)
        {
            try
            {
                return Math.Round(Math.Ceiling(dec / floor), MidpointRounding.AwayFromZero) * floor;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region unusing
        public static string DateTimeToStringWithT(this DateTime date)
        {
            try
            {
                string format = "AppSetting:DateTimeFormatWithT".GetConfigurationValue();
                string result = date.ToString(format, CultureInfo.InvariantCulture);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static DateTime? StringNullToDateTimeNull(this string str)
        {
            try
            {
                string format = "AppSetting:DateTimeFormat".GetConfigurationValue();
                if (str != null)
                {
                    DateTime result = DateTime.ParseExact(str, format, CultureInfo.InvariantCulture);
                    return result;
                }
                return null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<Guid> StringToGuid(this List<string> str)
        {
            try
            {
                List<Guid> _new = new List<Guid>();
                if (str == null)
                {
                    return _new;
                }
                if (str.Count() == 0)
                {
                    return _new;
                }
                foreach (var s in str)
                {
                    if (s == null || s == "" || s == "null")
                    {
                        _new.Add(Guid.Empty);
                    }
                    else
                    {
                        _new.Add(new Guid(s));
                    }
                }
                return _new;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<Guid> StringToGuid(this List<string> str, Guid guid)
        {
            try
            {
                List<Guid> _new = new List<Guid>();
                if (str == null)
                {
                    return _new;
                }
                if (str.Count() == 0)
                {
                    return _new;
                }
                foreach (var s in str)
                {
                    if (s == null || s == "" || s == "null")
                    {
                        _new.Add(guid);
                    }
                    else
                    {
                        _new.Add(new Guid(s));
                    }
                }
                return _new;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static Guid? StringToCorrectGuid(this string str)
        {
            try
            {
                Guid newGuid;
                if (Guid.TryParse(str, out newGuid))
                {
                    return newGuid;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static List<string> GuidNullToString(this List<Guid> guid)
        {
            try
            {
                List<string> _new = new List<string>();
                if (guid == null)
                {
                    return _new;
                }
                if (guid.Count() == 0)
                {
                    return _new;
                }
                foreach (var g in guid)
                {
                    if (g == null || g == Guid.Empty)
                    {
                        _new.Add(string.Empty);
                    }
                    else
                    {
                        _new.Add(g.GuidNullToString());
                    }
                }
                return _new;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string GuidNullOrEmptyToString(this object obj)
        {
            try
            {
                if (obj == null || new Guid(obj.ToString()) == new Guid())
                {
                    return null;
                }
                else
                {
                    return obj.ToString();
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string ToLowerString(this Guid guid)
        {
            try
            {
                if (guid == null)
                {
                    return null;
                }
                else
                {
                    return guid.ToString().ToLower();
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string IntNullToString(this object obj)
        {
            try
            {
                if (obj == null)
                {
                    return null;
                }
                else
                {
                    return obj.ToString();
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string DecimalToString(this decimal dec)
        {
            try
            {
                string format = "AppSetting:NumberFormatPrecision2".GetConfigurationValue();
                string result = dec.ToString(format);
                return result;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static int IntNullToInt(this object obj)
        {
            try
            {
                if (obj == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt16(obj);
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string EnumToString<T>(this object obj)
        {
            T enumValue = (T)Enum.Parse(typeof(T), obj.ToString());
            return enumValue.ToString();
        }
        public static IEnumerable<T> FromGenericToTypeOf<T>(this IEnumerable source) where T : class
        {
            try
            {
                List<T> ret = new List<T>();

                foreach (var item in source)
                {
                    T output = (T)Activator.CreateInstance(typeof(T));

                    foreach (var prop in item.GetType().GetProperties())
                    {
                        var value = prop.GetValue(item);

                        if (output.GetType().GetProperty(prop.Name) != null)
                        {
                            output.GetType().GetProperty(prop.Name).SetValue(output, prop.GetValue(item));
                        }
                    }
                    ret.Add(output);
                }
                return ret;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        public static Guid? GetDefaultIfEmpty<T>(T model, Guid? property)
        {
            try
            {
                Type type = property.GetType();
                if (model == null)
                {
                    return null;
                }
                else
                {
                    return property;
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static Guid GetDefaultIfEmpty<T>(this T model, Guid property)
        {
            try
            {
                Type type = property.GetType();
                if (model == null)
                {
                    return Guid.Empty;
                }
                else
                {
                    return property;
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static int GetDefaultIfEmpty<T>(this int property, T model = default(T))
        {
            try
            {
                if (model == null)
                {
                    return 0;
                }
                else
                {
                    return property;
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static int? GetDefaultIfEmpty<T>(this T model, int? property)
        {
            try
            {
                if (model == null)
                {
                    return null;
                }
                else
                {
                    return property;
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string GetDefaultIfEmpty<T>(this T model, string property)
        {
            try
            {
                if (model == null)
                {
                    return string.Empty;
                }
                else
                {
                    return property;
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static decimal GetDefaultIfDecimalEmpty<T>(T model, decimal property)
        {
            try
            {
                if (model == null)
                {
                    return 0m;
                }
                else
                {
                    return property;
                }
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string ToDateStringSQL(this string str)
        {
            try
            {
                string[] strArr = str.Split('/');
                return string.Format("{0}/{1}/{2}", strArr[1], strArr[0], strArr[2]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public static bool StringTryToDateTime(this string str)
        {
            try
            {
                string format = "AppSetting:DateTimeFormat".GetConfigurationValue();
                DateTime dateValue;
                if (DateTime.TryParseExact(str, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateValue))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static bool StringTryToDate(this string str)
        {
            try
            {
                List<string> dateFormats = DataTypeHandler.GetConfigurationValues("AppSetting:ImportExcelDateFormat");
                DateTime dateValue;
                if (DateTime.TryParseExact(str, dateFormats.ToArray(), CultureInfo.InvariantCulture, DateTimeStyles.None, out dateValue))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static DecimalLength GetDecimalProp<T>(this string fieldName)
        {
            try
            {
                DecimalLength decimalLength = null;
                PropertyInfo[] propertyInfos = typeof(T).GetProperties();
                foreach (PropertyInfo info in propertyInfos)
                {
                    if (info.Name.ToUpper() == fieldName.ToUpper())
                    {
                        string type = info.CustomAttributes.First().NamedArguments.First().TypedValue.ToString();
                        string above = type.Substring(type.IndexOf('(') + 1, type.IndexOf(',') - (type.IndexOf('(') + 1));
                        string below = type.Substring(type.IndexOf(',') + 1, type.IndexOf(')') - (type.IndexOf(',') + 1)).TrimStart();
                        decimalLength = new DecimalLength()
                        {
                            Above = Int32.Parse(above) - Int32.Parse(below),
                            Below = Int32.Parse(below),
                        };
                        break;
                    }
                }
                return decimalLength;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static DecimalLength GetDecimalProp(this string str)
        {
            try
            {
                decimal dec = Decimal.Parse(str);
                int above = (int)Math.Truncate(dec);
                int below = (int)(dec - Math.Truncate(dec));
                DecimalLength decimalLength = new DecimalLength()
                {
                    Above = above.ToString().Length,
                    Below = below.ToString().Length
                };
                return decimalLength;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static DecimalLength GetDecimalProp(this decimal dec)
        {
            try
            {
                int above = (int)Math.Truncate(dec);
                int below = (int)(dec - Math.Truncate(dec));
                DecimalLength decimalLength = new DecimalLength()
                {
                    Above = above.ToString().Length,
                    Below = below.ToString().Length
                };
                return decimalLength;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        public static Guid? GetLeftJoinNull<T>(this T model, Guid guid)
        {
            try
            {
                if (model == null)
                {
                    return null;
                }
                else
                {
                    return guid;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static int GetLeftJoinNull<T>(this T model, int status)
        {
            try
            {
                if (model == null)
                {
                    return 0;
                }
                else
                {
                    return status;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static List<Tvw> MergeModel<Tdb, Tvw>(this List<Tdb> models, Tvw view)
        {
            try
            {
                List<Tvw> result = new List<Tvw>();
                foreach (var item in models)
                {
                    result.Append(item.MergeModel(view));
                }
                return result;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        
        public static List<T> FirstToList<T>(this T first)
        {
            try
            {
                List<T> results = new List<T>();
                if (first != null)
                {
                    results.Add(first);
                }
                return results;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public static string ToReplaceUnUseString(this string txt)
        {
            try
            {
                return txt.Replace(" ", "").Replace("-", "");
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string GetPropStringValue(this object item, PropertyInfo prop)
        {
            try
            {
                if (item == null || prop == null || prop.GetValue(item) == null)
                {
                    return null;
                }

                string value = null;
                if (prop.Name.ToUpper().Contains("DateTime".ToUpper()) ||
                            (prop.Name.ToUpper().Contains("Date".ToUpper()) &&
                             prop.Name.ToUpper().Contains("Time".ToUpper())))
                {
                    if (prop.PropertyType == typeof(DateTime))
                    {
                        value = ((DateTime)prop.GetValue(item)).DateTimeToString();
                    }
                    else if (prop.PropertyType == typeof(DateTime?))
                    {
                        value = ((DateTime?)prop.GetValue(item)).DateTimeNullToString();
                    }
                    else if (prop.PropertyType == typeof(string))
                    {
                        value = (string)prop.GetValue(item);
                    }
                }
                else if (prop.Name.ToUpper().Contains("Date".ToUpper()))
                {
                    if (prop.PropertyType == typeof(DateTime))
                    {
                        value = ((DateTime)prop.GetValue(item)).DateToString();
                    }
                    else if (prop.PropertyType == typeof(DateTime?))
                    {
                        value = ((DateTime?)prop.GetValue(item)).DateNullToString();
                    }
                    else if (prop.PropertyType == typeof(string))
                    {
                        value = (string)prop.GetValue(item);
                    }
                }
                else if (prop.PropertyType == typeof(Guid) ||
                        prop.PropertyType == typeof(Guid?))
                {
                    value = prop.PropertyType == typeof(Guid) ?
                                ((Guid)prop.GetValue(item)).GuidNullToString() :
                                ((Guid?)prop.GetValue(item)).GuidNullToString();
                }
                else if (prop.PropertyType == typeof(string))
                {
                    value = (string)prop.GetValue(item);
                }
                else
                {
                    value = prop.GetValue(item)?.ToString();
                }
                return value;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public static string GetDisplayContentType(this string savedContentType)
        {
            var split1 = savedContentType.Split(new char[] { ':', ';' });
            return split1[1];
        }
        // merge Dictionaries: same key from latter Dictionaries will overwrite the former
        public static IDictionary<K, V> MergeOverwrite<K, V>(this IDictionary<K, V> self, params IDictionary<K, V>[] sources)
        {
            try
            {
                IDictionary<K, V> result = new Dictionary<K, V>();

                if (self == null)
                {
                    return result;
                }
                else
                {
                    if (sources == null || sources.Length == 0)
                    {
                        return self;
                    }
                    else
                    {
                        IDictionary<K, V>[] allSources = new IDictionary<K, V>[sources.Length + 1];
                        allSources[0] = self;
                        Array.Copy(sources, 0, allSources, 1, sources.Length);
                        foreach (IDictionary<K, V> src in allSources)
                        {
                            if (src != null && src.Count() > 0)
                            {
                                foreach (KeyValuePair<K, V> item in src)
                                {
                                    result[item.Key] = item.Value;
                                }
                            }
                        }
                        return result;
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
        #endregion
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
            (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        public static void CheckRowVersion<TInput, TOrig>(this TInput inputModel, TOrig origModel)
        {
            try
            {
                bool inputHasRowVersion = typeof(TInput).GetProperty(TextConstants.RowVersion) != null ? true : false;
                bool origHasRowVersion = typeof(TOrig).GetProperty(TextConstants.RowVersion) != null ? true : false;

                if(inputHasRowVersion && origHasRowVersion)
                {
                    var inputRowVersionProp = typeof(TInput).GetProperty(TextConstants.RowVersion);
                    var origRowVersionProp = typeof(TOrig).GetProperty(TextConstants.RowVersion);

                    if(inputRowVersionProp.PropertyType == typeof(byte[]) && origRowVersionProp.PropertyType == typeof(byte[]))
                    {
                        byte[] inputRowversion = (byte[])inputRowVersionProp.GetValue(inputModel);
                        byte[] origRowversion = (byte[])origRowVersionProp.GetValue(origModel);

                        if(inputRowversion != null && origRowversion != null && !inputRowversion.SequenceEqual(origRowversion))
                        {
                            SmartAppException ex = new SmartAppException("ERROR.ERROR");
                            ex.AddData("ERROR.CONCURRENCY");
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

    }
}
