﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModelHandler
{
    public static class SearchingHandler
    {
        public static SearchPredicate GetSearchPredicate(this SearchParameter search, Type modelType, bool checkDBDuplicate = false)
        {
            try
            {
                var result = new SearchPredicate();
                int counter = 0, conditionLength = 0;
                PropertyInfo[] properties = modelType.GetProperties();
                string keyColumn = string.Empty, dropdownPredicate = string.Empty;
                foreach (PropertyInfo property in properties)
                {
                    var attribute = Attribute.GetCustomAttribute(property, typeof(KeyAttribute))
                        as KeyAttribute;
                    if (attribute != null)
                    {
                        keyColumn = property.Name;
                    }
                }
                search.Conditions = search.Conditions == null ? new List<SearchCondition>() : search.Conditions;
                if (!checkDBDuplicate)
                {
                    search.Conditions.RemoveAll(r => r.Type == ColumnType.VARIABLE || (string.IsNullOrEmpty(r.Value) && r.Values.UnCountOrNull()));
                }
                
                conditionLength = search.Conditions.Count;
                object[] newObj = new object[(conditionLength)];
                // case single item dropdown
                if(conditionLength > 0 && search.Conditions.Any(a => a.ColumnName == TextConstants.BindingSingleItem))
                {
                    search.Conditions = search.Conditions.Where(w => w.ColumnName == TextConstants.BindingSingleItem).ToList();
                }

                if (conditionLength > 0 && 
                    (search.Conditions[0].ColumnName == TextConstants.BindingItem || search.Conditions[0].ColumnName == TextConstants.BindingSingleItem)
                    && search.Conditions[0].Operator == Operators.AND)
                {
                    newObj = new object[1];
                    var itemCondition = search.Conditions.Find(f => (f.ColumnName == TextConstants.BindingItem || f.ColumnName == TextConstants.BindingSingleItem) && 
                                                                    f.Operator == Operators.AND);
                    string op = GetOperatorBracket(Operators.AND, itemCondition.Bracket);
                    string endToken = GetOperatorBracket(" ", itemCondition.Bracket);
                    
                    dropdownPredicate = String.Concat(op, keyColumn, itemCondition.EqualityOperator, "@", counter, endToken);
                    newObj[counter] = itemCondition.Value.ToLower();
                    result.Values = newObj;
                }
                else if ((conditionLength) > 0)
                {
                    search.Conditions.ForEach(cond =>
                    {
                        // bracket
                        cond.Operator = GetOperatorBracket(cond.Operator, cond.Bracket);
                        string endToken = GetOperatorBracket(" ", cond.Bracket);
                        
                        if (cond.ColumnName == TextConstants.BindingItem && !string.IsNullOrEmpty(keyColumn) && cond.Value != "null")
                        {
                            cond.ColumnName = keyColumn;
                            dropdownPredicate = String.Concat(cond.Operator, keyColumn, cond.EqualityOperator, "@", counter, endToken);
                            newObj[counter] = cond.Value.ToLower();
                            counter++;
                        }
                        else if (cond.Type.ToUpper() == ColumnType.BOOLEAN)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, cond.EqualityOperator, "@", counter, endToken);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type.ToUpper() == ColumnType.DATE || cond.Type.ToUpper() == ColumnType.DATETIME)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, cond.EqualityOperator, "@", counter, endToken);
                            if (checkDBDuplicate)
                                newObj[counter] = DateTime.Parse(cond.Value);
                            else
                                newObj[counter] = DateTime.Parse(cond.Value.GetDateString());
                            counter++;
                        }
                        else if (cond.Type.ToUpper() == ColumnType.DATERANGE)
                        {
                            string propertyType = properties.Where(s => s.Name.ToUpper() == cond.ColumnName.ToUpper()).First().PropertyType.Name;
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.EqualityOperator.GetEqualOperatorForBracketedCondition(), cond.Values.GetDateRangePredicate(cond.ColumnName, propertyType), endToken);
                            newObj[counter] = cond.Values.ListStringToString();
                        }
                        else if (cond.Type.ToUpper() == ColumnType.STRING)
                        {
                            if (checkDBDuplicate)
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, cond.EqualityOperator, "@", counter, endToken);
                            else
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.EqualityOperator.GetEqualOperatorForBracketedCondition(), "(", cond.ColumnName, " ?? \"\").Contains(@", counter, ") ", endToken);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type.ToUpper() == ColumnType.INT)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, cond.EqualityOperator, "@", counter, endToken);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type.ToUpper() == ColumnType.MASTER)
                        {
                            if (cond.Value != null && cond.Value != "")
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName.ReplaceFKColumnName(), cond.EqualityOperator, "@", counter, endToken);
                                newObj[counter] = cond.Value.ToLower();
                            }
                            else
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.EqualityOperator.GetEqualOperatorForBracketedCondition(), GetMastersPredicate(cond.Values, cond.ColumnName.ReplaceFKColumnName()), endToken);
                                newObj[counter] = cond.Values.ListStringToString();
                            }
                            counter++;
                        }
                        else if (cond.Type.ToUpper() == ColumnType.MASTERSINGLE)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, cond.EqualityOperator, "@", counter, endToken);
                            newObj[counter] = cond.Value.ToLower();
                        }
                        else if (cond.Type.ToUpper() == ColumnType.ENUM)
                        {
                            if (cond.Value != null && cond.Value != "")
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, cond.EqualityOperator, "@", counter, endToken);
                                newObj[counter] = cond.Value;
                            }
                            else
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.EqualityOperator.GetEqualOperatorForBracketedCondition(), GetMastersPredicate(cond.Values, cond.ColumnName), endToken);
                                newObj[counter] = cond.Values.ListStringToString();
                            }
                            counter++;
                        }
                        else
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, cond.EqualityOperator, "@", counter, endToken);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                    });
                    result.Values = newObj;
                }


                if (search.SortColumns != null)
                {
                    var stringBuilder = new StringBuilder();
                    for (int i = 0; i < search.SortColumns.Count; i++)
                    {
                        stringBuilder.Append(string.Format("{0} {1},", search.SortColumns[i], search.IsAscs[i] ? "ASC" : "DESC"));
                    }
                    result.Sorting = stringBuilder.ToString().Trim(',');
                }
                if (!string.IsNullOrEmpty(dropdownPredicate))
                {
                    result.Predicates = string.Format("({0} {2}) {1}", result.Predicates, dropdownPredicate, TextConstants.BasePredicate);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static SearchPredicate GetPredicate_entity<T>(this SearchParameter search) where T : BaseEntity
        {
            try
            {
                Type modelType = typeof(T);
                string baseType = modelType.BaseType.Name;
                if (baseType != BaseType.BaseEntity.GetAttrCode())
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00645", new object[] { baseType });
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    SearchPredicate result = GetSearchPredicate(search, modelType);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static SearchPredicate GetPredicate_entity<T>(this SearchParameter search, SearchCondition accessLevelCond, string compGUID) where T : CompanyBaseEntity
        {
            try
            {
                if (accessLevelCond != null)
                {
                    if (accessLevelCond.Type == ColumnType.PREDICATE_NOT)
                    {
                        return new SearchPredicate { Predicates = TextConstants.PredicateNot };
                    }
                    else
                    {
                        search.Conditions.Add(accessLevelCond);
                        return GetPredicate_entity<T>(search, compGUID);
                    }
                }
                else
                {
                    return GetPredicate_entity<T>(search, compGUID);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchPredicate GetPredicate_entity<T>(this SearchParameter search, string compGUID) where T : CompanyBaseEntity
        {
            try
            {
                var baseId = new Dictionary<string, string>();
                Type modelType = typeof(T);
                string baseType = modelType.BaseType.Name;
                if (baseType != BaseType.CompanyBase.GetAttrCode())
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00645", new object[] { baseType });
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    SearchPredicate result = GetSearchPredicate(search, modelType);
                    int counter = 0;
                    List<object> values = new List<object>();
                    if (result.Values != null)
                    {
                        counter = result.Values.Count();
                        values = result.Values.ToList();
                    }

                    PropertyInfo[] properties = modelType.GetProperties();
                    var compProp = properties.Where(p => p.Name == TextConstants.CompanyGUID).FirstOrDefault();
                    if (compProp == null || (compGUID.isNull() && search.CompanyBaseGUID.isNull()))
                    {
                        result.Predicates = TextConstants.PredicateNot;
                    }
                    else
                    {
                        baseId.Add(compGUID ?? search.CompanyBaseGUID, compProp.Name);
                    }
                    foreach (KeyValuePair<string, string> entry in baseId)
                    {
                        result.Predicates = result.Predicates.GetBasePredicate(String.Concat(" AND ", entry.Value, " = @", counter, " "));
                        values.Add(entry.Key.ToLower());
                        counter++;
                    }
                    result.Predicates = result.Predicates.RemoveBasePredicate();
                    result.Values = values.ToArray();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static SearchPredicate GetPredicate_entity<T>(this SearchParameter search, SearchCondition accessLevelCond, string compGUID, DateTime effectiveFrom, DateTime? effectiveTo = null) where T : DateEffectiveBaseEntity
        {
            try
            {
                if (accessLevelCond != null)
                {
                    if (accessLevelCond.Type == ColumnType.PREDICATE_NOT)
                    {
                        return new SearchPredicate { Predicates = TextConstants.PredicateNot };
                    }
                    else
                    {
                        search.Conditions.Add(accessLevelCond);
                        return GetPredicate_entity<T>(search, compGUID, effectiveFrom, effectiveTo);
                    }
                }
                else
                {
                    return GetPredicate_entity<T>(search, compGUID, effectiveFrom, effectiveTo);
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchPredicate GetPredicate_entity<T>(this SearchParameter search, string compGUID, DateTime effectiveFrom, DateTime? effectiveTo = null) where T : DateEffectiveBaseEntity
        {
            try
            {
                Type modelType = typeof(T);
                string baseType = modelType.BaseType.Name;
                if (baseType != BaseType.DateEffectiveBase.GetAttrCode())
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00645", new object[] { baseType });
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    SearchPredicate result = GetSearchPredicate(search, modelType);
                    int counter = 0;
                    List<object> values = new List<object>();
                    if (result.Values != null)
                    {
                        counter = result.Values.Count();
                        values = result.Values.ToList();
                    }

                    var baseId = new Dictionary<string, string>();
                    var baseDate = new Dictionary<DateTime, string>();
                    PropertyInfo[] properties = modelType.GetProperties();
                    var compProp = properties.Where(p => p.Name == TextConstants.CompanyGUID).FirstOrDefault();
                    if (compProp == null || (compGUID.isNull() && search.CompanyBaseGUID.isNull()))
                    {
                        result.Predicates = TextConstants.PredicateNot;
                    }
                    else
                    {
                        baseId.Add(compGUID ?? search.CompanyBaseGUID, compProp.Name);
                    }

                    var effFromProp = properties.Where(p => p.Name == TextConstants.EffectiveFrom).FirstOrDefault();
                    if (effFromProp != null)
                        baseDate.Add(effectiveFrom, effFromProp.Name);

                    var effToProp = properties.Where(p => p.Name == TextConstants.EffectiveTo).FirstOrDefault();
                    if (effToProp != null && effectiveTo != null)
                        baseDate.Add(effectiveTo.GetValueOrDefault(), effToProp.Name);

                    foreach (KeyValuePair<string, string> entry in baseId)
                    {
                        result.Predicates = result.Predicates.GetBasePredicate(String.Concat(" AND ", entry.Value, " = @", counter, " "));
                        values.Add(entry.Key.ToLower());
                        counter++;
                    }
                    foreach (KeyValuePair<DateTime, string> entry in baseDate)
                    {
                        result.Predicates = result.Predicates.GetBasePredicate(String.Concat(" AND ", entry.Value, entry.Value == TextConstants.EffectiveFrom ? " >=" : " <=", " @", counter, " "));
                        values.Add(entry.Key);
                        counter++;
                    }
                    result.Predicates = result.Predicates.RemoveBasePredicate();
                    result.Values = values.ToArray();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }

        }
        public static SearchPredicate GetPredicate_entity<T>(this SearchParameter search, SearchCondition accessLevelCond, List<string> branches, string compGUID, string branGUID) where T : BranchCompanyBaseEntity
        {
            try
            {
                if (accessLevelCond != null)
                {
                    if (accessLevelCond.Type == ColumnType.PREDICATE_NOT)
                    {
                        return new SearchPredicate { Predicates = TextConstants.PredicateNot };
                    }
                    else
                    {
                        search.Conditions.Add(accessLevelCond);
                        return GetPredicate_entity<T>(search, branches, compGUID, branGUID);
                    }
                }
                else
                {
                    return GetPredicate_entity<T>(search, branches, compGUID, branGUID);
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static SearchPredicate GetPredicate_entity<T>(this SearchParameter search, List<string> branches, string compGUID, string branGUID) where T : BranchCompanyBaseEntity
        {
            try
            {
                Type modelType = typeof(T);
                string baseType = modelType.BaseType.Name;
                if (baseType != BaseType.BranchCompanyBase.GetAttrCode())
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00645", new object[] { baseType });
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    SearchPredicate result = GetSearchPredicate(search, modelType);
                    int counter = 0;
                    List<object> values = new List<object>();
                    if (result.Values != null)
                    {
                        counter = result.Values.Count();
                        values = result.Values.ToList();
                    }

                    bool hasBranchCondition = false;
                    var baseId = new Dictionary<string, string>();
                    PropertyInfo[] properties = modelType.GetProperties();
                    var compProp = properties.Where(p => p.Name == TextConstants.CompanyGUID).FirstOrDefault();
                    var branchProp = properties.Where(p => p.Name == TextConstants.BranchGUID).FirstOrDefault();
                    if (search.BranchFilterMode == BranchFilterType.SpecificBranch.GetAttrCode())
                    {
                        if ((compProp == null || (compGUID.isNull() && search.CompanyBaseGUID.isNull()))
                        || (branchProp == null || (branGUID.isNull() && search.BranchBaseGUID.isNull())))
                        {
                            result.Predicates = TextConstants.PredicateNot;
                        }
                        else
                        {
                            baseId.Add(compGUID ?? search.CompanyBaseGUID, compProp.Name);
                            baseId.Add(branGUID ?? search.BranchBaseGUID, branchProp.Name);
                        }

                    }
                    else
                    {
                        if ((compProp == null || (compGUID.isNull() && search.CompanyBaseGUID.isNull())))
                        {
                            result.Predicates = TextConstants.PredicateNot;
                        }
                        else
                        {
                            baseId.Add(compGUID ?? search.CompanyBaseGUID, compProp.Name);
                        }
                    }

                    hasBranchCondition = search.Conditions.Any(a => a.ColumnName == TextConstants.BranchGUID && a.Values.UnCountOrNull());
                    foreach (KeyValuePair<string, string> entry in baseId)
                    {
                        result.Predicates = result.Predicates.GetBasePredicate(String.Concat(" AND ", entry.Value, " = @", counter, " "));
                        values.Add(entry.Key.ToLower());
                        counter++;
                    }

                    if (!hasBranchCondition && search.BranchFilterMode == BranchFilterType.AllBranch.GetAttrCode())
                    {
                        result.Predicates = result.Predicates.GetBasePredicate(String.Concat(" AND ", GetMastersPredicate(branches, TextConstants.BranchGUID), " "));
                        values.Add(branches.ListStringToString());
                    }
                    result.Predicates = result.Predicates.RemoveBasePredicate();
                    result.Values = values.ToArray();
                    return result;
                }

            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }

        }


        public static SearchPredicate GetPredicate<T>(this SearchParameter search)
        {
            try
            {
                var result = new SearchPredicate();
                Type modelType = typeof(T);
                search.Conditions = search.Conditions == null ? new List<SearchCondition>() : search.Conditions;
                search.Conditions.RemoveAll(r => r.Type == ColumnType.VARIABLE || (string.IsNullOrEmpty(r.Value) && r.Values.UnCountOrNull()));
                int counter = 0;
                if (search.Conditions.Count > 0)
                {
                    object[] newObj = new object[search.Conditions.Count];
                    search.Conditions.ForEach(cond =>
                    {
                        if (cond.Type == ColumnType.BOOLEAN)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " == @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} = {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value == "true" ? "1" : "0");
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type == ColumnType.DATE)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.SubColumnName, " == @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} = '{2}'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value.GetDateString());
                            newObj[counter] = DateTime.Parse(cond.Value.GetDateString());
                            counter++;
                        }
                        else if (cond.Type == ColumnType.DATERANGE)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.Values.GetDateRangePredicate(cond.SubColumnName));
                            result.TotalPredicates = String.Format(" {0} AND {1} BETWEEN {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Values.ListDateToString());
                            newObj[counter] = cond.Values.ListStringToString();
                        }
                        else if (cond.Type == ColumnType.STRING)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, "String.Concat(", cond.ColumnName, ",\"\").Contains(@", counter, ",1) ");
                            result.TotalPredicates = String.Format(" {0} AND {1} LIKE N'%{2}%'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type == ColumnType.INT)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " == @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} = {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type == ColumnType.MASTER)
                        {
                            if (cond.Value != null && cond.Value != "")
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " = @", counter, " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} LIKE '%{2}%'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                                newObj[counter] = cond.Value;
                            }
                            else
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, GetMastersPredicate(cond.Values, cond.ColumnName), " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} IN ({2})", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Values.ListStringToString());
                                newObj[counter] = cond.Values.ListStringToString();
                            }
                            counter++;
                        }
                        else if (cond.Type == ColumnType.MASTERSINGLE)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " = @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} LIKE '%{2}%'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value.ToLower();
                        }
                        else if (cond.Type == ColumnType.ENUM)
                        {
                            if (cond.Value != null && cond.Value != "")
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " = @", counter, " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} = {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin("", false), cond.Value);
                                newObj[counter] = cond.Value;
                            }
                            else
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, GetMastersPredicate(cond.Values, cond.ColumnName), " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} IN ({2})", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Values.ListStringToString());
                                newObj[counter] = cond.Values.ListStringToString();
                            }
                            counter++;
                        }
                    });
                    result.Values = newObj;
                }
                if (search.SortColumns != null)
                {
                    var stringBuilder = new StringBuilder();
                    for (int i = 0; i < search.SortColumns.Count; i++)
                    {
                        stringBuilder.Append(string.Format("{0} {1},", search.SortColumns[i], search.IsAscs[i] ? "ASC" : "DESC"));
                    }
                    result.Sorting = stringBuilder.ToString().Trim(',');
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }

        }
        public static SearchPredicate GetPredicate<T>(this SearchParameter search, string compGUID, string branGUID = null)
        {
            try
            {
                var result = new SearchPredicate();
                var baseId = new Dictionary<string, string>();
                int counter = 0, conditionLength = 0;
                Type modelType = typeof(T);
                PropertyInfo[] properties = modelType.GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    if (property.Name == TextConstants.CompanyGUID)
                    {
                        baseId.Add(compGUID, property.Name);
                    }
                    if (property.Name == TextConstants.BranchGUID && branGUID != null)
                    {
                        baseId.Add(branGUID, property.Name);
                    }
                }
                search.Conditions = search.Conditions == null ? new List<SearchCondition>() : search.Conditions;
                search.Conditions.RemoveAll(r => r.Type == ColumnType.VARIABLE || (string.IsNullOrEmpty(r.Value) && r.Values.UnCountOrNull()));
                conditionLength = search.Conditions.Count;
                if ((conditionLength + baseId.Count) > 0)
                {
                    object[] newObj = new object[(conditionLength + baseId.Count)];
                    search.Conditions.ForEach(cond =>
                    {
                        if (cond.Type == ColumnType.BOOLEAN)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " == @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} = {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type == ColumnType.DATE)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " = @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} = {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type == ColumnType.STRING)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, "String.Concat(", cond.ColumnName, ",\"\").Contains(@", counter, ",1) ");
                            result.TotalPredicates = String.Format(" {0} AND {1} LIKE N'%{2}%'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type == ColumnType.INT)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " == @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} = {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type == ColumnType.MASTER)
                        {
                            if (cond.Value != null && cond.Value != "")
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " = @", counter, " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} LIKE '%{2}%'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                                newObj[counter] = cond.Value;
                            }
                            else
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, GetMastersPredicate(cond.Values, cond.ColumnName), " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} IN ({2})", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Values.ListStringToString());
                                newObj[counter] = cond.Values.ListStringToString();
                            }
                            counter++;
                        }
                        else if (cond.Type == ColumnType.MASTERSINGLE)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " = @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} LIKE '%{2}%'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value.ToLower();
                        }
                        else if (cond.Type == ColumnType.ENUM)
                        {
                            if (cond.Value != null && cond.Value != "")
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " = @", counter, " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} = {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin("", false), cond.Value);
                                newObj[counter] = cond.Value;
                            }
                            else
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, GetMastersPredicate(cond.Values, cond.ColumnName), " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} IN ({2})", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Values.ListStringToString());
                                newObj[counter] = cond.Values.ListStringToString();
                            }
                            counter++;
                        }
                    });
                    foreach (KeyValuePair<string, string> entry in baseId)
                    {
                        newObj[counter] = entry.Key;
                        result.Predicates = String.Concat(result.Predicates, " AND ", entry.Value, " = @", counter, " ");
                        result.TotalPredicates = String.Format(" {0} AND {1}.{2} LIKE '%{3}%'", result.TotalPredicates, modelType.Name, entry.Value, entry.Key);
                        counter++;
                    }
                    result.Values = newObj;
                }
                if (search.SortColumns != null)
                {
                    var stringBuilder = new StringBuilder();
                    for (int i = 0; i < search.SortColumns.Count; i++)
                    {
                        stringBuilder.Append(string.Format("{0} {1},", search.SortColumns[i], search.IsAscs[i] ? "ASC" : "DESC"));
                    }
                    result.Sorting = stringBuilder.ToString().Trim(',');
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }

        }
        public static SearchPredicate GetPredicate_new<T>(this SearchParameter search, List<string> branches, string compGUID, string branGUID = null)
        {
            try
            {
                var result = new SearchPredicate();
                var baseId = new Dictionary<string, string>();
                int counter = 0, conditionLength = 0;
                bool hasBranchCondition = false;
                Type modelType = typeof(T);
                var baseType = modelType.BaseType.Name;
                PropertyInfo[] properties = modelType.GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    if (property.Name == TextConstants.CompanyGUID)
                    {
                        baseId.Add(compGUID, property.Name);
                    }
                    if (search.BranchFilterMode == BranchFilterType.SpecificBranch.GetAttrCode() && property.Name == TextConstants.BranchGUID)
                    {
                        if (branGUID != null)
                        {
                            baseId.Add(branGUID, property.Name);
                        }
                        else
                        {
                            throw SmartAppUtil.AddStackTrace(new Exception("branGUID is null"));
                        }
                    }
                }
                hasBranchCondition = search.Conditions.Any(a => a.ColumnName == TextConstants.BranchGUID && a.Values.UnCountOrNull());
                search.Conditions = search.Conditions == null ? new List<SearchCondition>() : search.Conditions;
                search.Conditions.RemoveAll(r => r.Type == ColumnType.VARIABLE || (string.IsNullOrEmpty(r.Value) && r.Values.UnCountOrNull()));
                conditionLength = search.Conditions.Count;
                if (!hasBranchCondition)
                {
                    conditionLength++;
                }
                if ((conditionLength + baseId.Count) > 0)
                {
                    object[] newObj = new object[(conditionLength + baseId.Count)];
                    search.Conditions.ForEach(cond =>
                    {
                        if (cond.Type == ColumnType.BOOLEAN)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " == @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} = {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value == "true" ? "1" : "0");
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type == ColumnType.DATE)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " == @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} = '{2}'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value.GetDateString());
                            newObj[counter] = DateTime.Parse(cond.Value.GetDateString());
                            counter++;
                        }
                        else if (cond.Type == ColumnType.DATERANGE)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.Values.GetDateRangePredicate(cond.SubColumnName));
                            result.TotalPredicates = String.Format(" {0} AND {1} BETWEEN {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Values.ListDateToString());
                            newObj[counter] = cond.Values.ListStringToString();
                        }
                        else if (cond.Type == ColumnType.STRING)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, "(", cond.ColumnName, " ?? \"\").Contains(@", counter, ") ");
                            result.TotalPredicates = String.Format(" {0} AND {1} LIKE N'%{2}%'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type == ColumnType.INT)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " == @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} = {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type == ColumnType.MASTER)
                        {
                            if (cond.Value != null && cond.Value != "")
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " = @", counter, " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} LIKE '%{2}%'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                                newObj[counter] = cond.Value.ToLower();
                            }
                            else
                            {
                                if (cond.ColumnName == TextConstants.BranchGUID)
                                {
                                    hasBranchCondition = true;
                                }
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, GetMastersPredicate(cond.Values, cond.ColumnName), " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} IN ({2})", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Values.ListStringToString());
                                newObj[counter] = cond.Values.ListStringToString();
                            }
                            counter++;
                        }
                        else if (cond.Type == ColumnType.MASTERSINGLE)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " = @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} LIKE '%{2}%'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value.ToLower();
                        }
                        else if (cond.Type == ColumnType.ENUM)
                        {
                            if (cond.Value != null && cond.Value != "")
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " = @", counter, " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} = {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin("", false), cond.Value);
                                newObj[counter] = cond.Value;
                            }
                            else
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, GetMastersPredicate(cond.Values, cond.ColumnName), " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} IN ({2})", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Values.ListStringToString());
                                newObj[counter] = cond.Values.ListStringToString();
                            }
                            counter++;
                        }
                    });
                    foreach (KeyValuePair<string, string> entry in baseId)
                    {
                        newObj[counter] = entry.Key.ToLower();
                        result.Predicates = String.Concat(result.Predicates, " AND ", entry.Value, " = @", counter, " ");
                        result.TotalPredicates = String.Format(" {0} AND {1}.{2} LIKE '%{3}%'", result.TotalPredicates, modelType.Name, entry.Value, entry.Key);
                        counter++;
                    }
                    if (!hasBranchCondition && search.BranchFilterMode == BranchFilterType.AllBranch.GetAttrCode() && branGUID != null)
                    {
                        result.Predicates = String.Concat(result.Predicates, " AND ", GetMastersPredicate(branches, TextConstants.BranchGUID), " ");
                        result.TotalPredicates = String.Format(" {0} AND {1} IN ({2})", result.TotalPredicates, TextConstants.BranchGUID.GetColumnJoin(modelType.Name), branches.ListStringToString());
                        newObj[counter] = branches.ListStringToString();
                    }
                    result.Values = newObj;
                }
                if (search.SortColumns != null)
                {
                    var stringBuilder = new StringBuilder();
                    for (int i = 0; i < search.SortColumns.Count; i++)
                    {
                        stringBuilder.Append(string.Format("{0} {1},", search.SortColumns[i], search.IsAscs[i] ? "ASC" : "DESC"));
                    }
                    result.Sorting = stringBuilder.ToString().Trim(',');
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }

        }
        public static SearchPredicate GetPredicate<T>(this SearchParameter search, List<string> branches, string compGUID, string branGUID = null)
        {
            try
            {
                var result = new SearchPredicate();
                var baseId = new Dictionary<string, string>();
                int counter = 0, conditionLength = 0;
                bool hasBranchCondition = false;
                Type modelType = typeof(T);
                PropertyInfo[] properties = modelType.GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    if (property.Name == TextConstants.CompanyGUID)
                    {
                        baseId.Add(compGUID, property.Name);
                    }
                    if (search.BranchFilterMode == BranchFilterType.SpecificBranch.GetAttrCode() && property.Name == TextConstants.BranchGUID)
                    {
                        if (branGUID != null)
                        {
                            baseId.Add(branGUID, property.Name);
                        }
                        else
                        {
                            throw SmartAppUtil.AddStackTrace(new Exception("branGUID is null"));
                        }
                    }
                }
                hasBranchCondition = search.Conditions.Any(a => a.ColumnName == TextConstants.BranchGUID && a.Values.UnCountOrNull());
                search.Conditions = search.Conditions == null ? new List<SearchCondition>() : search.Conditions;
                search.Conditions.RemoveAll(r => r.Type == ColumnType.VARIABLE || (string.IsNullOrEmpty(r.Value) && r.Values.UnCountOrNull()));
                conditionLength = search.Conditions.Count;
                if (!hasBranchCondition)
                {
                    conditionLength++;
                }
                if ((conditionLength + baseId.Count) > 0)
                {
                    object[] newObj = new object[(conditionLength + baseId.Count)];
                    search.Conditions.ForEach(cond =>
                    {
                        if (cond.Type == ColumnType.BOOLEAN)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " == @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} = {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value == "true" ? "1" : "0");
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type == ColumnType.DATE)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.SubColumnName, " == @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} = '{2}'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value.GetDateString());
                            newObj[counter] = DateTime.Parse(cond.Value.GetDateString());
                            counter++;
                        }
                        else if (cond.Type == ColumnType.DATERANGE)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.Values.GetDateRangePredicate(cond.SubColumnName));
                            result.TotalPredicates = String.Format(" {0} AND {1} BETWEEN {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Values.ListDateToString());
                            newObj[counter] = cond.Values.ListStringToString();
                        }
                        else if (cond.Type == ColumnType.STRING)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, "String.Concat(", cond.ColumnName, ",\"\").Contains(@", counter, ",1) ");
                            result.TotalPredicates = String.Format(" {0} AND {1} LIKE N'%{2}%'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type == ColumnType.INT)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " == @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} = {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value;
                            counter++;
                        }
                        else if (cond.Type == ColumnType.MASTER)
                        {
                            if (cond.Value != null && cond.Value != "")
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " = @", counter, " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} LIKE '%{2}%'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                                newObj[counter] = cond.Value.ToLower();
                            }
                            else
                            {
                                if (cond.ColumnName == TextConstants.BranchGUID)
                                {
                                    hasBranchCondition = true;
                                }
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, GetMastersPredicate(cond.Values, cond.ColumnName), " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} IN ({2})", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Values.ListStringToString());
                                newObj[counter] = cond.Values.ListStringToString();
                            }
                            counter++;
                        }
                        else if (cond.Type == ColumnType.MASTERSINGLE)
                        {
                            result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " = @", counter, " ");
                            result.TotalPredicates = String.Format(" {0} AND {1} LIKE '%{2}%'", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Value);
                            newObj[counter] = cond.Value.ToLower();
                        }
                        else if (cond.Type == ColumnType.ENUM)
                        {
                            if (cond.Value != null && cond.Value != "")
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, cond.ColumnName, " = @", counter, " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} = {2}", result.TotalPredicates, cond.ColumnName.GetColumnJoin("", false), cond.Value);
                                newObj[counter] = cond.Value;
                            }
                            else
                            {
                                result.Predicates = String.Concat(result.Predicates, cond.Operator, GetMastersPredicate(cond.Values, cond.ColumnName), " ");
                                result.TotalPredicates = String.Format(" {0} AND {1} IN ({2})", result.TotalPredicates, cond.ColumnName.GetColumnJoin(modelType.Name), cond.Values.ListStringToString());
                                newObj[counter] = cond.Values.ListStringToString();
                            }
                            counter++;
                        }
                    });
                    foreach (KeyValuePair<string, string> entry in baseId)
                    {
                        newObj[counter] = entry.Key.ToLower();
                        result.Predicates = String.Concat(result.Predicates, " AND ", entry.Value, " = @", counter, " ");
                        result.TotalPredicates = String.Format(" {0} AND {1}.{2} LIKE '%{3}%'", result.TotalPredicates, modelType.Name, entry.Value, entry.Key);
                        counter++;
                    }
                    if (!hasBranchCondition && search.BranchFilterMode == BranchFilterType.AllBranch.GetAttrCode())
                    {
                        result.Predicates = String.Concat(result.Predicates, " AND ", GetMastersPredicate(branches, TextConstants.BranchGUID), " ");
                        result.TotalPredicates = String.Format(" {0} AND {1} IN ({2})", result.TotalPredicates, TextConstants.BranchGUID.GetColumnJoin(modelType.Name), branches.ListStringToString());
                        newObj[counter] = branches.ListStringToString();
                    }
                    result.Values = newObj;
                }
                if (search.SortColumns != null)
                {
                    var stringBuilder = new StringBuilder();
                    for (int i = 0; i < search.SortColumns.Count; i++)
                    {
                        stringBuilder.Append(string.Format("{0} {1},", search.SortColumns[i], search.IsAscs[i] ? "ASC" : "DESC"));
                    }
                    result.Sorting = stringBuilder.ToString().Trim(',');
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }

        }
        private static string GetMastersPredicate(List<string> values, string column)
        {
            try
            {
                string result = " ( ";
                foreach (string val in values)
                {
                    result = string.Format("{0} {1} = \"{2}\" OR", result, column, val.ToLower());
                }
                result = string.Format("{0} {1}", result, " )");
                result = result.Replace("OR  )", ")");
                return values == null ? " 1=1 " : result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static string GetDateRangePredicate(this List<string> values, string column, string propertyType = "DATETIME")
        {
            try
            {
                string getDate = (propertyType == ColumnType.NULLABLE) ? ".Value.Date" : ".Date";
                string result = " 1=1 ";
                if (values != null && values[0] == null && values[1] != null)
                {
                    result = string.Format("( {0}{4} >= \"{1}\" AND {2}{4} <= \"{3}\" ) ", column, "0001/01/01", column, values[1].GetDateString(), getDate);
                }
                else if(values != null && values[0] != null && values[1] == null )
                {
                    result = string.Format("( {0}{4} >= \"{1}\" AND {2}{4} <= \"{3}\" ) ", column, values[0].GetDateString(), column, "9999-12-31", getDate);
                }
                else if (values != null && values[0] != null && values[1] != null)
                {
                    result = string.Format("( {0}{4} >= \"{1}\" AND {2}{4} <= \"{3}\" ) ", column, values[0].GetDateString(), column, values[1].GetDateString(), getDate);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static SearchResult<T> SetSearchResult<T>(this List<T> list, int total, SearchParameter param)
        {
            var result = new SearchResult<T>();
            try
            {
                result = new SearchResult<T>
                {
                    Results = list,
                    Paginator = new Paginator
                    {
                        Page = param.Paginator.Page,
                        First = param.Paginator.First,
                        Rows = param.Paginator.Rows,
                        PageCount = total / param.Paginator.Rows,
                        TotalRecord = total
                    }
                };
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }


        }
        public static SearchParameter GetSpecificBranch(this SearchParameter search)
        {
            try
            {
                search.BranchFilterMode = BranchFilterType.SpecificBranch.GetAttrCode();
                return search;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public static SearchParameter GetAllBranch(this SearchParameter search)
        {
            try
            {
                search.BranchFilterMode = BranchFilterType.AllBranch.GetAttrCode();
                return search;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public static SearchParameter GetByCompany(this SearchParameter search)
        {
            try
            {
                search.BranchFilterMode = BranchFilterType.ByCompany.GetAttrCode();
                return search;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static int GetTotal(this string query, SearchPredicate predicate, SmartAppDbContext db)
        {
            try
            {
                int result = 0;
                int begin = query.LastIndexOf("FROM ");

                string from = query.Substring(begin, query.Length - begin);
                string sql = String.Format(" SELECT COUNT(*) {0}", from);
                using (var command = db.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = string.Format("{0} WHERE {1}", sql, predicate.TotalPredicates);
                    db.Database.OpenConnection();
                    using (var total = command.ExecuteReader())
                    {
                        while (total.Read())
                        {
                            result = total.GetInt32(0);
                        }

                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static string GetColumnJoin(this string column, string modelName = "", bool isBase = true)
        {
            try
            {
                if (isBase)
                {
                    string[] arr = column.Split('_');
                    if (arr.Count() > 1)
                    {
                        if (TextConstants.SqlWordingException.Any(a => a.ToUpper() == arr[1].ToUpper()))
                        {
                            arr[1] = string.Format("[{0}]", arr[1]);
                        }
                        return String.Format("{0}.{1}", arr[0], arr[1]);
                    }
                    else
                    {
                        if (TextConstants.SqlWordingException.Any(a => a.ToUpper() == arr[0].ToUpper()))
                        {
                            arr[0] = string.Format("[{0}]", arr[0]);
                        }
                        return String.Format("{0}.{1}", modelName, arr[0]);
                    }
                }
                else
                {
                    return column;
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private static readonly TypeInfo QueryCompilerTypeInfo = typeof(QueryCompiler).GetTypeInfo();

        private static readonly FieldInfo QueryCompilerField = typeof(EntityQueryProvider).GetTypeInfo().DeclaredFields.First(x => x.Name == "_queryCompiler");

        private static readonly FieldInfo QueryModelGeneratorField = QueryCompilerTypeInfo.DeclaredFields.First(x => x.Name == "_queryModelGenerator");

        private static readonly FieldInfo DataBaseField = QueryCompilerTypeInfo.DeclaredFields.Single(x => x.Name == "_database");

        private static readonly PropertyInfo DatabaseDependenciesField = typeof(Database).GetTypeInfo().DeclaredProperties.Single(x => x.Name == "Dependencies");


        public static T GetSearchToModel<T>(this SearchParameter search) where T : new()
        {
            try
            {
                PropertyInfo[] propertyInfos;
                T result = new T();
                propertyInfos = typeof(T).GetProperties();
                foreach (SearchCondition cond in search.Conditions)
                {
                    if (cond.Type == ColumnType.VARIABLE || cond.Type == ColumnType.VARIABLES)
                    {
                        foreach (PropertyInfo propertyInfo in propertyInfos)
                        {
                            if (propertyInfo.Name.ToUpper() == cond.ColumnName.ToUpper() && !string.IsNullOrEmpty(cond.Value))
                            {
                                string type = propertyInfo.PropertyType.FullName.ToUpper();

                                if (type == SystemType.STRING.ToUpper())
                                {
                                    propertyInfo.SetValue(result, cond.Value);
                                }
                                else if (type == SystemType.INT.ToUpper())
                                {
                                    propertyInfo.SetValue(result, Int32.Parse(cond.Value));
                                }
                                else if (type == SystemType.INT32.ToUpper())
                                {
                                    propertyInfo.SetValue(result, Int32.Parse(cond.Value));
                                }
                                else if (type.ToUpper().Contains(SystemType.BOOLEAN.ToUpper()))
                                {
                                    propertyInfo.SetValue(result, Boolean.Parse(cond.Value));
                                }
                                else
                                {
                                    propertyInfo.SetValue(result, cond.Value);
                                }
                            }
                            else if (propertyInfo.Name.ToUpper() == cond.ColumnName.ToUpper() && !cond.Values.UnCountOrNull())
                            {
                                string type = propertyInfo.PropertyType.FullName.ToUpper();

                                if (type.Contains(SystemType.LIST.ToUpper()) && type.Contains(SystemType.STRING.ToUpper()))
                                {
                                    propertyInfo.SetValue(result, cond.Values);
                                }
                                else if (type.Contains(SystemType.LIST.ToUpper()) && type.Contains(SystemType.INT.ToUpper()))
                                {
                                    var ints = new List<int>();
                                    cond.Values.ForEach(item =>
                                    {
                                        ints.Add(Int32.Parse(item));
                                    });
                                    propertyInfo.SetValue(result, ints);
                                }
                                else
                                {
                                    propertyInfo.SetValue(result, cond.Value);
                                }
                            }

                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<T> EnsureModel<T>(this List<T> model)
        {
            try
            {
                if (model == null)
                {
                    return new List<T>();
                }
                else
                {
                    return model;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static IEnumerable<T> EnsureModel<T>(this IEnumerable<T> model)
        {
            try
            {
                if (model == null)
                {
                    return new List<T>();
                }
                else
                {
                    return model;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string GetBasePredicate(this string predicate, string baseCondition)
        {
            try
            {
                predicate = predicate.Replace(TextConstants.BasePredicate, string.Format("{0} {1}", baseCondition, TextConstants.BasePredicate));
                return predicate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static string RemoveBasePredicate(this string predicate)
        {
            try
            {
                predicate = predicate.Replace(TextConstants.BasePredicate, "");
                return predicate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static string ReplaceFKColumnName(this string columnName)
        {
            try
            {
                columnName = columnName.Replace("_Values", "GUID");
                return columnName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static string GetEqualOperatorForBracketedCondition(this string equalityOperator)
        {
            try
            {
                return equalityOperator == Operators.NOT_EQUAL ? Operators.NOT : "";
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private static string GetOperatorBracket(string condOperator, int condBracket)
        {
            try
            {

                switch(condBracket)
                {
                    case (int)BracketType.None: 
                        return condOperator;
                    case (int)BracketType.SingleStart:
                        if (condOperator == Operators.AND) return Brackets.AND_SINGLE;
                        else if (condOperator == Operators.OR) return Brackets.OR_SINGLE;
                        else return condOperator;
                    case (int)BracketType.SingleEnd:
                        if (string.IsNullOrWhiteSpace(condOperator)) return Brackets.SINGLE_END;
                        else return condOperator;
                    case (int)BracketType.DoubleStart:
                        if (condOperator == Operators.AND) return Brackets.AND_DOUBLE;
                        else if (condOperator == Operators.OR) return Brackets.OR_DOUBLE;
                        else return condOperator;
                    case (int)BracketType.DoubleEnd:
                        if (string.IsNullOrWhiteSpace(condOperator)) return Brackets.DOUBLE_END;
                        else return condOperator;
                    default: return condOperator;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static string GetGroupByPredicate(this IEnumerable<PropertyInfo> props)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                if(props.Count() > 1)
                {
                    sb.Append("new (");
                }
                sb.AppendJoin(", ", props.Select(s => s.Name));
                if (props.Count() > 1)
                {
                    sb.Append(")");
                }
                return sb.ToString();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
