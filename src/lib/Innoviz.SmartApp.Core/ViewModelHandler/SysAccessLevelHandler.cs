﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModelHandler
{
    public static class SysAccessLevelHandler
    {
        public static List<T> GetCanDeleteRowAuthorize<T>(this List<T> list, AccessLevelParm deleteAccessLevel, bool skipMethod)
        {
            try
            {
                if (skipMethod)
                {
                    return list;
                }
                if (list.Count == 0)
                {
                    return list;
                }
                var item = list.FirstOrDefault();
                PropertyInfo ownerProp = item.GetType().GetProperty(TextConstants.Owner);
                PropertyInfo ownerBUProp = item.GetType().GetProperty(TextConstants.OwnerBusinessUnitGUID);
                PropertyInfo rowAuthorizeProp = item.GetType().GetProperty(TextConstants.RowAuthorize);
                // case no owner/BU to check
                if (ownerProp == null || ownerBUProp == null)
                {
                    return list;
                }

                if (deleteAccessLevel == null)
                {
                    return list;
                }
                else
                {
                    list = list.Select(s =>
                    {
                        rowAuthorizeProp.SetValue(s, GetCanDeleteRowAuthorize<T>(s, deleteAccessLevel, Convert.ToInt32(rowAuthorizeProp.GetValue(s))));
                        return s;
                    }).ToList();
                    return list;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public static int GetCanDeleteRowAuthorize<T>(T item, AccessLevelParm deleteAccessLevel, int rowAuthorize)
        {
            try
            {
                PropertyInfo ownerProp = item.GetType().GetProperty(TextConstants.Owner);
                PropertyInfo ownerBUProp = item.GetType().GetProperty(TextConstants.OwnerBusinessUnitGUID);
                // case no owner/BU to check
                if (ownerProp == null || ownerBUProp == null)
                {
                    return rowAuthorize;
                }

                if (deleteAccessLevel == null)
                {
                    return rowAuthorize;
                }
                else
                {
                    AccessLevel delete = (AccessLevel)deleteAccessLevel.AccessLevel;
                    int deleteRowAuthorize = AccessMode.Viewer.GetAttrValue();
                    switch (delete)
                    {
                        case AccessLevel.None:
                            deleteRowAuthorize = AccessMode.Viewer.GetAttrValue();
                            break;
                        case AccessLevel.User:
                            deleteRowAuthorize = ownerProp.GetValue(item).GuidNullToString() == deleteAccessLevel.AccessValue ?
                                                    AccessMode.Full.GetAttrValue() : AccessMode.Viewer.GetAttrValue();
                            break;
                        case AccessLevel.BusinessUnit:
                            deleteRowAuthorize = ownerBUProp.GetValue(item).GuidNullToString() == deleteAccessLevel.AccessValue ?
                                                    AccessMode.Full.GetAttrValue() : AccessMode.Viewer.GetAttrValue();
                            break;
                        case AccessLevel.ParentChildBU:
                            deleteRowAuthorize = deleteAccessLevel.AccessValues.Any(a => a == ownerBUProp.GetValue(item).GuidNullToString()) ?
                                                    AccessMode.Full.GetAttrValue() : AccessMode.Viewer.GetAttrValue();
                            break;
                        case AccessLevel.Company:
                            deleteRowAuthorize = AccessMode.Full.GetAttrValue();
                            break;
                        default:
                            break;
                    }
                    return deleteRowAuthorize < rowAuthorize ? deleteRowAuthorize : rowAuthorize;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
