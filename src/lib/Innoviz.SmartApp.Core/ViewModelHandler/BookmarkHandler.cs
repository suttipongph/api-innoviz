﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using Innoviz.SmartApp.Core.Attributes;
using Innoviz.SmartApp.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Core.ViewModelHandler
{
    public static class BookmarkHandler
    {
        #region replace bookmark ext
        public static T GetFirstDescendant<T>(this OpenXmlElement parent) where T : OpenXmlElement
        {
            var descendants = parent.Descendants<T>();

            if (descendants != null)
                return descendants.FirstOrDefault();
            else
                return null;
        }

        public static T GetParent<T>(this OpenXmlElement child) where T : OpenXmlElement
        {
            while (child != null)
            {
                child = child.Parent;

                if (child is T)
                    return (T)child;
            }

            return null;
        }

        public static bool IsEndBookmark(this OpenXmlElement element, BookmarkStart startBookmark)
        {
            return IsEndBookmark(element as BookmarkEnd, startBookmark);
        }

        public static bool IsEndBookmark(this BookmarkEnd endBookmark, BookmarkStart startBookmark)
        {
            if (endBookmark == null)
                return false;

            return endBookmark.Id == startBookmark.Id;
        }
        public static IDictionary<string, string> GetBookmarkMapperKeyValues<T>(this T templateModel)
        {
            try
            {
                if (templateModel == null)
                {
                    return null;
                }

                IDictionary<string, string> result = null;
                // check BookmarkMappingAttributes
                var props = typeof(T).GetProperties()
                                        .Select(prop => new
                                        {
                                            Property = prop,
                                            Attribute = (BookmarkMappingAttribute)Attribute
                                                            .GetCustomAttribute(prop, typeof(BookmarkMappingAttribute))
                                        });
                if (props != null && props.Count() != 0)
                {
                    result = new Dictionary<string, string>();
                    for (int i = 0; i < props.Count(); i++)
                    {
                        var prop = props.ElementAt(i).Property;
                        string value = templateModel.GetPropStringValue(prop);

                        if(prop.PropertyType == typeof(decimal) || prop.PropertyType == typeof(decimal?))
                        {
                            var decVal = prop.GetValue(templateModel);
                            value = decVal != null ? ((decimal)decVal).DecimalToString() : null;
                        }

                        var mappings = props.ElementAt(i).Attribute?.BookmarkNames;
                        if (mappings != null)
                        {
                            for (int j = 0; j < mappings.Length; j++)
                            {
                                result.Add(mappings[j], value);
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion replace bookmark ext
    }
}
