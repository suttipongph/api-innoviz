﻿using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.ViewModelHandler
{
    public static class StatusHandler
    {
        public static void CompareStatus(this Guid newStatus, Guid oldStatus )
        {
            try
            {
                if (newStatus != oldStatus)
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.00843");
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static void IsCompareStatus(this DecisionInformation decisionInformation, Guid newStatus)
        {
            try
            {
                if (decisionInformation.StatusParent != null)
                {
                    CompareStatus(newStatus, new Guid(decisionInformation.StatusParent));
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
    }
}
