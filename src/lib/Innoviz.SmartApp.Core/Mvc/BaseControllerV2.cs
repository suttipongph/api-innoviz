﻿using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Core.Mvc
{
    [Produces("application/json")]
    [Route("api/v2/[controller]")]
    [Authorize(Policy = "roles_required")]
    public class BaseControllerV2 : ControllerBase
    {
        protected readonly SmartAppDbContext db;
        protected readonly UnitOfWork UnitOfWork;
        protected SystemParameter SystemParameter { get; private set; }
        protected readonly ISysTransactionLogService SysTransactionLogService;
        public BaseControllerV2(SmartAppDbContext context, ISysTransactionLogService sysTransactionLogService)
        {
            db = context;
            UnitOfWork = new UnitOfWork(db);
            this.SysTransactionLogService = sysTransactionLogService;
        }

        [OpenApiIgnore]
        public virtual void SetSystemParameter(SystemParameter systemParameter)
        {
            SystemParameter = systemParameter;
            db.SetSystemParameter(systemParameter);
        }
        [OpenApiIgnore]
        public virtual SmartAppDbContext GetSmartAppDbContext()
        {
            return db;
        }
        [OpenApiIgnore]
        protected string GetCompanyFromRequest()
        {
            return SystemParameter?.CompanyGUID;
        }
        [OpenApiIgnore]
        protected int GetSiteHeader()
        {
            return (SystemParameter != null) ? SystemParameter.SiteLogin : -1;
        }
        [OpenApiIgnore]
        protected string GetUserNameFromToken()
        {
            return SystemParameter?.UserName;
        }
        [OpenApiIgnore]
        protected string GetToken()
        {
            return SystemParameter.BearerToken;
        }
        [OpenApiIgnore]
        protected string GetRequestIdFromRequest()
        {
            return SystemParameter.RequestId;
        }
    }


}
