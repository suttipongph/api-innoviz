﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Innoviz.SmartApp.Core.Repositories;
using Microsoft.AspNetCore.Authorization;

namespace Innoviz.SmartApp.Core.Mvc
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [Authorize(Policy = "roles_required")]
    public class BaseControllerV1 : ControllerBase
    {
        protected readonly SmartAppDbContext db;
        protected readonly UnitOfWork UnitOfWork;
        public BaseControllerV1(SmartAppDbContext context)
        {
            db = context;
            UnitOfWork = new UnitOfWork(db);
        }
    }
}
