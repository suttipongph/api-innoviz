﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Linq.Dynamic.Core;


namespace Innoviz.SmartApp.Core
{
    [Serializable]
    public class SmartAppException : Exception
    {
        public readonly IDictionary DataParameters = new Dictionary<string, dynamic>();
        public List<string> MessageList = new List<string>();
        public IDictionary RowParameters = new Dictionary<string, string>();

        public readonly IDictionary BatchLogReference = new Dictionary<string, SmartAppExceptionData>();
        public SmartAppException(string message) : base(message)
        {
            MessageList.Add(SystemStaticData.GetTranslatedMessage(message));
        }

        public SmartAppException(string message, Exception innerException) : base(message, innerException)
        {
            if (innerException != null)
            {
                if (innerException.InnerException != null)
                {
                    if (innerException.InnerException.Data[ExceptionDataKey.MessageList] != null)
                    {
                        MessageList = (List<string>)innerException.InnerException.Data[ExceptionDataKey.MessageList];
                    }
                    else
                    {
                        MessageList.Add(SystemStaticData.GetTranslatedMessage(innerException.InnerException.Message));
                    }

                }
                if (innerException.Data[ExceptionDataKey.MessageList] != null)
                {
                    MessageList.AddRange((List<string>)innerException.Data[ExceptionDataKey.MessageList]);
                }
                else
                {
                    MessageList.Add(SystemStaticData.GetTranslatedMessage(innerException.Message));
                }

                if (innerException.Data != null &&
                    innerException.Data.Keys != null &&
                    innerException.Data.Keys.Count != 0)
                {

                    foreach (var item in innerException.Data.Keys)
                    {
                        if (item.ToString() != ExceptionDataKey.StackTrace &&
                            item.ToString() != ExceptionDataKey.InnerStackTrace &&
                            item.ToString() != ExceptionDataKey.MessageList &&
                            item.ToString() != ExceptionDataKey.BatchLogReference &&
                            !item.ToString().ToLower().StartsWith("helplink"))
                        {
                            AddData(item.ToString(), innerException.Data[item]);
                        }
                    }
                }

            }
            MessageList.Add(SystemStaticData.GetTranslatedMessage(message));
        }

        public SmartAppException(int messageId)
        {

        }

        public SmartAppException(int messageId, Exception innerException)
        {
        }

        public SmartAppException(WebApiErrorResponseMessage errorResponse)
            : base(errorResponse.Message)
        {
            Source = errorResponse.Source;

            if (errorResponse != null)
            {
                AddData(errorResponse.Message);
                SetDataFromWebApiErrorResponse(errorResponse);
            }


            Data.Add(ExceptionDataKey.StackTrace, errorResponse.StackTrace);
        }
        public SmartAppException(string message, WebApiErrorResponseMessage errorResponse)
            : base(message)
        {
            MessageList.Add(SystemStaticData.GetTranslatedMessage(message));
            Source = errorResponse.Source;
            if (errorResponse != null)
            {
                AddData(errorResponse.Message);
                SetDataFromWebApiErrorResponse(errorResponse);
            }

            Data.Add(ExceptionDataKey.StackTrace, errorResponse.StackTrace);
        }
        protected SmartAppException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public void AddData(string errorCode, int row, params object[] args)
        {
            // add error msg to smartAppException.Data
            this.Data.AddMsgCode(errorCode.ToLower(), errorCode);
            this.DataParameters.AddMsgCode(errorCode.ToLower(), args);
            this.RowParameters.AddMsgCode(errorCode.ToLower(), row.ToString());
            
            this.MessageList.Add("{Row "+ row +"}" + SystemStaticData.GetTranslatedMessage(errorCode, args));
        }
        public void AddData(BatchLogReference batchLogReference, string errorCode, params object[] args)
        {
            if(batchLogReference != null)
            {
                // check BatchLogReference Key
                bool keyExists = false;
                if(!string.IsNullOrWhiteSpace(batchLogReference.Key))
                {
                    foreach (var k in BatchLogReference.Keys)
                    {
                        var val = (SmartAppExceptionData)BatchLogReference[k];
                        if(val != null && val.Key?.ToLower() == batchLogReference.Key?.ToLower())
                        {
                            keyExists = true;
                            break;
                        }
                    }
                }
                if(!keyExists)
                {
                    string keyFromInput = !string.IsNullOrWhiteSpace(batchLogReference.Key) ? batchLogReference.Key : Guid.NewGuid().GuidNullToString();
                    SmartAppExceptionData data = new SmartAppExceptionData
                    {
                        Key = keyFromInput,
                        ErrorCode = errorCode,
                        Parameters = args == null ? null : (string[])args,
                        BatchLogReference = batchLogReference
                    };
                    this.BatchLogReference.AddMsgCode(errorCode, data);
                    this.MessageList.Add(SystemStaticData.GetTranslatedMessage(errorCode, args));
                }
            }
        }
        public void AddData(BatchLogReference batchLogReference, string errorCode)
        {
            AddData(batchLogReference, errorCode, null);
            
        }
        public void AddData(string errorCode, params object[] args)
        {
            // add error msg to smartAppException.Data
            this.Data.AddMsgCode(errorCode.ToLower(), errorCode);
            this.DataParameters.AddMsgCode(errorCode.ToLower(), args);

            this.MessageList.Add(SystemStaticData.GetTranslatedMessage(errorCode, args));
        }
        public void AddData(string errorCode, int row)
        {
            // at least 1 arg
            this.Data.AddMsgCode(errorCode.ToLower(), errorCode);
            this.RowParameters.AddMsgCode(errorCode.ToLower(), row.ToString());

            this.MessageList.Add("{Row " + row + "}" + SystemStaticData.GetTranslatedMessage(errorCode));
        }
        public void AddData(string errorCode)
        {
            // add error msg to smartAppException.Data
            if (!this.Data.Contains(errorCode.ToLower()))
            {
                this.Data.Add(errorCode.ToLower(), errorCode);
                this.MessageList.Add(SystemStaticData.GetTranslatedMessage(errorCode));
            }
        }

        
        private bool IsArrayEqual(object[] a, object[] b)
        {
            try
            {
                bool result = true;
                
                if(a != null && b != null)
                {
                    int alen = a.Length;
                    int blen = b.Length;
                    
                    if(alen != blen)
                    {
                        result = false;
                    }
                    else
                    {
                        for (int i = 0; i < alen; i++)
                        {
                            if(a[i]?.ToString() != b[i]?.ToString())
                            {
                                result = false;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    result = a == b;
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void SetDataFromWebApiErrorResponse(WebApiErrorResponseMessage errorResponse)
        {
            try
            {
                if(errorResponse != null && errorResponse.ErrorMessage != null &&
                    errorResponse.ErrorMessage.Keys.Count != 0)
                {
                    foreach (var item in errorResponse.ErrorMessage.Keys)
                    {
                        string msgValue = errorResponse.ErrorMessage[item].ToString();
                        object[] args = null;
                        if (errorResponse.ErrorParameter != null &&
                            errorResponse.ErrorParameter.Keys.Count != 0)
                        {
                            try
                            {
                                var tmp = JsonConvert.SerializeObject(errorResponse.ErrorParameter[item]);
                                args = JsonConvert.DeserializeObject<object[]>(tmp);

                            }
                            catch (KeyNotFoundException)
                            {
                                args = null;
                            }

                        }

                        if(msgValue != "Error" && msgValue != "ERROR.ERROR")
                        {
                            // check if errorMessage value is code
                            string[] split = msgValue.ToString().Split('.');
                            if (split.Length > 1 &&
                                (split[0] == MessageGroup.ERROR ||
                                 split[0] == MessageGroup.LABEL ||
                                 split[0] == MessageGroup.SUCCESS))
                            {
                                AddData(msgValue, args);
                                MessageList.Add(SystemStaticData.GetTranslatedMessage(msgValue, args));
                            }
                            else
                            {
                                string code = SystemStaticData.GetCodeFromMessage(msgValue, args);
                                if (args != null && args.Length != 0)
                                {
                                    AddData(code, args);
                                }
                                else
                                {
                                    AddData(code);
                                }

                            }
                        }
                        
                    }
                }
                
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

    }
    public class SmartAppExceptionData
    {
        public string Key { get; set; }
        public string ErrorCode { get; set; }
        public string[] Parameters { get; set; }
        public int? Row { get; set; }
        public BatchLogReference BatchLogReference { get; set; }
    }
}
